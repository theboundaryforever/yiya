package com.tongdaxing.erban.libcommon.listener;

import android.util.Log;
import android.view.View;

/**
 * 防止过快多次点击
 * Created by zeda on 16/3/2.
 */
public abstract class SingleClickListener implements View.OnClickListener {

    //单次点击的时间间隔（毫秒，此时间间隔内再次点击无效）
    private long clickInterval = 500;
    private long lastClickTime = 0;

    public SingleClickListener(){}

    public SingleClickListener(long clickInterval){
        this.clickInterval = clickInterval;
    }

    @Override
    public synchronized void onClick(View v) {
        long time = System.currentTimeMillis();
        Log.d("避免多次点击","time="+time+",lastclicktime="+lastClickTime+"时间差="+String.valueOf(time-lastClickTime)+",设定的时间="+clickInterval);
        if (time - lastClickTime < clickInterval) {
            return;
        }
        lastClickTime = time;
        singleClick(v);
    }

    public abstract void singleClick(View v);

    public long getClickInterval() {
        return clickInterval;
    }

    public void setClickInterval(long clickInterval) {
        this.clickInterval = clickInterval;
    }
}