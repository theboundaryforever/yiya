package com.tongdaxing.erban.libcommon.listener;

import io.reactivex.disposables.Disposable;

public interface IDisposableAddListener {
    void addDisposable(Disposable disposable);
}
