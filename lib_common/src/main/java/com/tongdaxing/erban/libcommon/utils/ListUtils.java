package com.tongdaxing.erban.libcommon.utils;

import java.util.List;

/**
 * <p>  </p>
 * Created by Administrator on 2017/11/16.
 */

public class ListUtils {

    public static boolean isListEmpty(List list) {
        return (list == null || list.size() == 0);
    }

    public static boolean isNotEmpty(List list) {
        return list != null && list.size() > 0;
    }

}

//public class ListUtils {
//
//    public static boolean isListEmpty(List list) {
//        return (list == null || list.size() == 0);
//    }
//}
