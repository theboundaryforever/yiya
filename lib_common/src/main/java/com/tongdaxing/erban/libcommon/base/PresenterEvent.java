package com.tongdaxing.erban.libcommon.base;

public enum PresenterEvent {

    CREATE,
    START,
    RESUME,
    PAUSE,
    STOP,
    DESTROY

}