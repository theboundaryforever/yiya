package com.tongdaxing.erban.libcommon.net.rxnet;

import com.tongdaxing.xchat_framework.http_image.http.DefaultRequestProcessor;
import com.tongdaxing.xchat_framework.http_image.http.DownloadRequest;
import com.tongdaxing.xchat_framework.http_image.http.ProgressListener;
import com.tongdaxing.xchat_framework.http_image.http.RequestProcessor;
import com.tongdaxing.xchat_framework.http_image.http.ResponseErrorListener;
import com.tongdaxing.xchat_framework.http_image.http.ResponseListener;

import java.util.Map;

/**
 * Http管理器（现只用于下载）
 *
 * @author zhongyongsheng on 14-6-11.
 */
public class RequestManager {

    private volatile static RequestManager mFactory;
    private RequestProcessor mCommonProcessor;

    private RequestManager() {
        mCommonProcessor = new DefaultRequestProcessor(2, "Http_");
        mCommonProcessor.start();
    }

    public static synchronized RequestManager instance() {
        if (mFactory == null) {
            mFactory = new RequestManager();
        }
        return mFactory;
    }

    /**
     * 下载文件请求,调用者保证下载到本地文件的目录已经创建好
     *
     * @param url                 下载服务器路径
     * @param downloadFilePath    下载本地路径
     * @param successListener     成功回调
     * @param errorListener       失败回调
     * @param progressListener    进度回调
     * @param useContinueDownload 是否使用断点续传
     */
    public void submitDownloadRequest(String url,
                                      String downloadFilePath,
                                      ResponseListener<String> successListener,
                                      ResponseErrorListener errorListener,
                                      ProgressListener progressListener,
                                      boolean useContinueDownload) {
        if (url == null
                || downloadFilePath == null
                || successListener == null
                || errorListener == null
                || progressListener == null) {
            return;
        }
        DownloadRequest req = new DownloadRequest(url, downloadFilePath, successListener,
                errorListener, progressListener, useContinueDownload);
        mCommonProcessor.add(req);
    }
}
