package com.netease.nim.uikit;

/**
 * @author Zhangsongzhou
 * @date 2019/7/22
 */
public interface OnCheckResultAttentionListener {
    void onCheckResultAttention(long uid, boolean isLike, boolean online);

    void onDoAttentionResult(long uid);

}
