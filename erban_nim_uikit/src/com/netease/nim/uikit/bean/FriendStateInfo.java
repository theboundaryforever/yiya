package com.netease.nim.uikit.bean;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/27
 * 描述        私聊用户的好友关系
 *
 * @author dell
 */
public class FriendStateInfo {

    /**
     * inRoom : {"uid":33,"type":4,"roomId":10008343}
     * friendId : 0
     * applyFriendGoldNum : 20
     * status : -1
     */

    public InRoomInfo inRoom;
    public int friendId;
    public int applyFriendGoldNum;
    public int status;

    public InRoomInfo getInRoom() {
        return inRoom;
    }

    public int getFriendId() {
        return friendId;
    }

    public int getApplyFriendGoldNum() {
        return applyFriendGoldNum;
    }

    public int getStatus() {
        return status;
    }

    public static class InRoomInfo {
        /**
         * uid : 33
         * type : 4
         * roomId : 10008343
         */

        public int uid;
        public int type;
        public int roomId;

        public int getUid() {
            return uid;
        }

        public int getType() {
            return type;
        }

        public int getRoomId() {
            return roomId;
        }

        @Override
        public String toString() {
            return "InRoomInfo{" +
                    "uid=" + uid +
                    ", type=" + type +
                    ", roomId=" + roomId +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "FriendStateInfo{" +
                "inRoom=" + inRoom +
                ", friendId=" + friendId +
                ", applyFriendGoldNum=" + applyFriendGoldNum +
                ", status=" + status +
                '}';
    }
}
