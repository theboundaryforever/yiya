package com.netease.nim.uikit;

/**
 * @author Zhangsongzhou
 * @date 2019/7/22
 */
public interface OnCheckAttentionListener {
    void onCheckAttention(long uid);

    void onDoAttention(long uid);
}
