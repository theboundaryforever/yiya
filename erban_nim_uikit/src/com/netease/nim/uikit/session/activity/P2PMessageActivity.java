package com.netease.nim.uikit.session.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.OnCheckResultAttentionListener;
import com.netease.nim.uikit.OnlineStateChangeListener;
import com.netease.nim.uikit.R;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.model.ToolBarOptions;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.constant.Extras;
import com.netease.nim.uikit.session.fragment.MessageFragment;
import com.netease.nim.uikit.uinfo.UserInfoHelper;
import com.netease.nim.uikit.uinfo.UserInfoObservable;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.CustomNotification;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.libcommon.listener.SingleClickListener;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Set;


/**
 * 点对点聊天界面
 * <p/>
 * Created by huangjun on 2015/2/1.
 */
public class P2PMessageActivity extends BaseMessageActivity {

    private boolean isResume = false;
    TextView tvToolbarTitle;

    LinearLayout mAttentionLl;
    ImageView mAttentionIv;
    ImageView mAttentionWaveIv;
    SVGAImageView mAttentionWaveSVG;

    public static void start(Context context, String contactId, SessionCustomization customization, IMMessage anchor) {
        Intent intent = new Intent();
        intent.putExtra(Extras.EXTRA_ACCOUNT, contactId);
        intent.putExtra(Extras.EXTRA_CUSTOMIZATION, customization);
        if (anchor != null) {
            intent.putExtra(Extras.EXTRA_ANCHOR, anchor);
        }
        intent.setClass(context, P2PMessageActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP /*| Intent.FLAG_ACTIVITY_CLEAR_TOP*/);

        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "消息-对话页");

        // 单聊特例话数据，包括个人信息，
        requestBuddyInfo();
        displayOnlineState();
        registerObservers(true);
        registerOnlineStateChangeListener(true);

        if (!NimUIKit.isRobotUid(sessionId)) {
            if (NimUIKit.getOnCheckAttenttionListener() != null) {
                NimUIKit.getOnCheckAttenttionListener().onCheckAttention(Long.parseLong(sessionId));
            }
        }


        NimUIKit.setOnCheckResultAttentionListener(new OnCheckResultAttentionListener() {
            @Override
            public void onCheckResultAttention(long uid, boolean isLike, boolean online) {
                if (uid == Long.parseLong(sessionId)) {
                    mAttentionLl.setVisibility(isLike ? View.GONE : View.VISIBLE);
                }
                if (online) {
                    SVGAParser svgaParser = new SVGAParser(P2PMessageActivity.this);
                    svgaParser.decodeFromAssets("microredwave.svga", new SVGAParser.ParseCompletion() {
                        @Override
                        public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                            mAttentionWaveSVG.setVideoItem(svgaVideoEntity);
                            if (!mAttentionWaveSVG.isAnimating()) {
                                mAttentionWaveSVG.startAnimation();
                            }
                        }

                        @Override
                        public void onError() {
                            LogUtil.e("svgaWave onError");
                        }
                    });

                    mAttentionWaveSVG.setVisibility(View.VISIBLE);
                    mAttentionWaveIv.setVisibility(View.GONE);

                } else {
                    mAttentionWaveSVG.setVisibility(View.GONE);
                    mAttentionWaveIv.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onDoAttentionResult(long uid) {
                if (uid == Long.parseLong(sessionId)) {
                    mAttentionLl.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            registerObservers(false);
            registerOnlineStateChangeListener(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        NimUIKit.setOnCheckResultAttentionListener(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResume = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isResume = false;
    }

    private void requestBuddyInfo() {
        // 显示自己的textview并且居中
        tvToolbarTitle = getToolBar().findViewById(R.id.tv_toolbar_title);
        String userTitleName = UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P);
        if (StringUtil.isEmpty(userTitleName)) {
            NimUserInfoCache.getInstance().getUserInfoFromRemote(sessionId, new RequestCallback<NimUserInfo>() {
                @Override
                public void onSuccess(NimUserInfo param) {
                    tvToolbarTitle.setText(param.getName());
                }

                @Override
                public void onFailed(int code) {

                }

                @Override
                public void onException(Throwable exception) {

                }
            });
        }
        tvToolbarTitle.setText(userTitleName);
        tvToolbarTitle.setVisibility(View.VISIBLE);
        setTitle("");

        //是否显示他人资料页入口
        View userInfoView = findViewById(R.id.toolbar_user_info_fl);
        userInfoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NimUIKit.getSessionListener() != null) {
                    NimUIKit.getSessionListener().onCustomClicked(P2PMessageActivity.this, Long.valueOf(sessionId));
                }
            }
        });
        userInfoView.setVisibility(NimUIKit.isRobotUid(sessionId) ? View.INVISIBLE : View.VISIBLE);


        mAttentionLl = (LinearLayout) findViewById(R.id.attention_layout_ll);
        mAttentionIv = (ImageView) findViewById(R.id.attention_iv);
        mAttentionWaveSVG = (SVGAImageView) findViewById(R.id.svga_wave);
        mAttentionWaveIv = (ImageView) findViewById(R.id.svga_wave_iv);

        mAttentionIv.setOnClickListener(new SingleClickListener() {
            @Override
            public void singleClick(View v) {
                if (NimUIKit.getOnCheckAttenttionListener() != null) {
                    NimUIKit.getOnCheckAttenttionListener().onDoAttention(Long.parseLong(sessionId));
                }
            }
        });
    }

    private void registerObservers(boolean register) {
        if (register) {
            registerUserInfoObserver();
        } else {
            unregisterUserInfoObserver();
        }
        NIMClient.getService(MsgServiceObserve.class).observeCustomNotification(commandObserver, register);
//        FriendDataCache.getInstance().registerFriendDataChangedObserver(friendDataChangedObserver, register);
    }

//    FriendDataCache.FriendDataChangedObserver friendDataChangedObserver = new FriendDataCache.FriendDataChangedObserver() {
//        @Override
//        public void onAddedOrUpdatedFriends(List<String> accounts) {
//            setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
//        }
//
//        @Override
//        public void onDeletedFriends(List<String> accounts) {
//            setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
//        }
//
//        @Override
//        public void onAddUserToBlackList(List<String> account) {
//            setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
//        }
//
//        @Override
//        public void onRemoveUserFromBlackList(List<String> account) {
//            setTitle(UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P));
//        }
//    };

    private UserInfoObservable.UserInfoObserver uinfoObserver;

    OnlineStateChangeListener onlineStateChangeListener = new OnlineStateChangeListener() {
        @Override
        public void onlineStateChange(Set<String> accounts) {
            // 更新 toolbar
            if (accounts.contains(sessionId)) {
                // 按照交互来展示
                displayOnlineState();
            }
        }
    };

    private void registerOnlineStateChangeListener(boolean register) {
        if (!NimUIKit.enableOnlineState()) {
            return;
        }
        if (register) {
            NimUIKit.addOnlineStateChangeListeners(onlineStateChangeListener);
        } else {
            NimUIKit.removeOnlineStateChangeListeners(onlineStateChangeListener);
        }
    }

    private void displayOnlineState() {
        if (!NimUIKit.enableOnlineState()) {
            return;
        }
        String detailContent = NimUIKit.getOnlineStateContentProvider().getDetailDisplay(sessionId);
        setSubTitle(detailContent);
    }

    private void registerUserInfoObserver() {
        if (uinfoObserver == null) {
            uinfoObserver = new UserInfoObservable.UserInfoObserver() {
                @Override
                public void onUserInfoChanged(List<String> accounts) {
                    if (accounts.contains(sessionId)) {
                        requestBuddyInfo();
                    }
                }
            };
        }

        UserInfoHelper.registerObserver(uinfoObserver);
    }

    private void unregisterUserInfoObserver() {
        if (uinfoObserver != null) {
            UserInfoHelper.unregisterObserver(uinfoObserver);
        }
    }

    /**
     * 命令消息接收观察者
     */
    Observer<CustomNotification> commandObserver = new Observer<CustomNotification>() {
        @Override
        public void onEvent(CustomNotification message) {
            if (!sessionId.equals(message.getSessionId()) || message.getSessionType() != SessionTypeEnum.P2P) {
                return;
            }
            showCommandMessage(message);
        }
    };

    protected void showCommandMessage(CustomNotification message) {
        if (!isResume) {
            return;
        }

        String content = message.getContent();
        try {
            JSONObject json = JSON.parseObject(content);
            int id = json.getIntValue("id");
            if (id == 1) {
                // 正在输入
                SingleToastUtil.showToast(P2PMessageActivity.this, "对方正在输入...");
            }

        } catch (Exception e) {

        }
    }

    @Override
    protected MessageFragment fragment() {
        MessageFragment fragment = new MessageFragment();
        Bundle arguments = getIntent().getExtras();
        if (arguments != null)
            arguments.putSerializable(Extras.EXTRA_TYPE, SessionTypeEnum.P2P);
        fragment.setArguments(arguments);
        fragment.setContainerId(R.id.message_fragment_container);
        return fragment;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.nim_message_activity;
    }

    @Override
    protected void initToolBar() {
        ToolBarOptions options = new ToolBarOptions();
        setToolBar(R.id.toolbar, options);
    }
}
