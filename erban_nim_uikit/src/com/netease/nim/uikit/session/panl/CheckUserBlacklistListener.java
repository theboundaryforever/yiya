package com.netease.nim.uikit.session.panl;

import android.view.View;

/**
 * @author Zhangsongzhou
 * @date 2019/5/27
 */
public interface CheckUserBlacklistListener {
    void onCheckUserBlacklist(View v);

    void onTxtOperationResult(View view, String content);

    void onTxtOperationResult(String content);
}
