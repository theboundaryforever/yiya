package com.netease.nim.uikit.session.panl;

import android.view.View;

/**
 * @author Zhangsongzhou
 * @date 2019/5/27
 */
public interface InputPanelListener {
    void onInputPanel(View view, String account);

    void onTxtOperation(View view, String account, String content);
}
