package com.netease.nim.uikit.session;

import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;

import java.io.File;

/**
 * @author Zhangsongzhou
 * @date 2019/5/30
 */
public interface CheckSendImageListener {
    void onCheckSendImage(int position);
}
