package com.tongdaxing.xchat_framework.http_image.util;

import android.os.Build;

import com.juxiao.library_utils.JxUuidFactory;
import com.juxiao.safetychecker.AvdChecker;
import com.juxiao.safetychecker.SafetyChecker;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.AppMetaDataUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.NetworkUtils;
import com.tongdaxing.xchat_framework.util.util.TelephonyUtils;
import com.tongdaxing.xchat_framework.util.util.VersionUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CommonParamUtil {

    public static Map<String, String> getDefaultParam() {
        return getDefaultParam(new HashMap<String, String>());
    }

    public static Map<String, String> getDefaultParam(Map<String, String> param) {
        if (param == null) {
            param = new HashMap<>();
        }
        param.put("os", "android");
        param.put("osVersion", Build.VERSION.RELEASE);
        param.put("app", "xchat");
        param.put("ispType", String.valueOf(getIspType()));
        param.put("netType", String.valueOf(getNetworkType()));
        param.put("model", getPhoneModel());
        param.put("appVersion", VersionUtil.getLocalName(BasicConfig.INSTANCE.getAppContext()));
        param.put("appCode", VersionUtil.getVersionCode(BasicConfig.INSTANCE.getAppContext()) + "");
        try {
            param.put("deviceId", JxUuidFactory.getFactory().getJxUuid(BasicConfig.INSTANCE.getAppContext()));
        } catch (IOException e) {
            LogUtil.i("IOException e");
            param.put("deviceId", "0");
            e.printStackTrace();
        } catch (SecurityException e){
            param.put("deviceId", "0");
            LogUtil.i("SecurityException e");
            e.printStackTrace();
        }

        param.put("isSimulator", AvdChecker.getInstance().check(BasicConfig.INSTANCE.getAppContext()) ? "1":"0");
        param.put("channel", AppMetaDataUtil.getChannelID(BasicConfig.INSTANCE.getAppContext()));
        param.put("imei", TelephonyUtils.getImei(BasicConfig.INSTANCE.getAppContext()));
        return param;
    }

    /**
     * 获取是否nettype字段
     *
     * @return
     */
    public static int getNetworkType() {
        if (NetworkUtils.getNetworkType(BasicConfig.INSTANCE.getAppContext()) == NetworkUtils.NET_WIFI) {
            return 2;
        } else {
            return 1;
        }
    }

    /**
     * 获取运营商字段
     *
     * @return
     */
    public static int getIspType() {
        String isp = NetworkUtils.getOperator(BasicConfig.INSTANCE.getAppContext());
        int ispType = 4;
        if (isp.equals(NetworkUtils.ChinaOperator.CMCC)) {
            ispType = 1;
        } else if (isp.equals(NetworkUtils.ChinaOperator.UNICOM)) {
            ispType = 2;
        } else if (isp.equals(NetworkUtils.ChinaOperator.CTL)) {
            ispType = 3;
        }

        return ispType;
    }

    /**
     * 获取手机型号
     *
     * @return
     */
    public static String getPhoneModel() {
        return Build.MODEL;
    }
}
