package com.tongdaxing.xchat_framework.util.config;

/**
 * Created by Administrator on 2018/3/7.
 */

public interface SpEvent {
    String linkedMeShareUid = "linkedMeShareUid";
    String linkedMeChannel = "linkedMeChannel";
    String onKickRoomInfo = "onKickRoomId";

    String roomUid = "roomUid";
    String roomType = "roomType";
    String time = "time";
    String cache_uid = "cache_uid";
    String not_hot_menu = "not_hot_menu";
    String config_key = "config_key";
    String headwearUrl = "headwear_url";
    String search_history = "search_history";
    String first_open = "first_open";

    String cleckLoginTime = "cleck_login_time";

    String isLianMicroUsed = "room_is_lian_micro_used";
    String isCreateRoomClicked = "isCreateRoomClicked";
}