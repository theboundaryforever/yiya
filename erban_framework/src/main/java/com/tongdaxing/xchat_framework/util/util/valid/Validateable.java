package com.tongdaxing.xchat_framework.util.util.valid;

public interface Validateable {

	public abstract boolean validate();
}
