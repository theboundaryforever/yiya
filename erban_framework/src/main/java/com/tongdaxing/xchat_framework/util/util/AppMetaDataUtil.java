package com.tongdaxing.xchat_framework.util.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: crid
 * Date: 9/3/13
 * Time: 9:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class AppMetaDataUtil {

    /**
     * @param
     * @return 渠道名称
     * <p/>
     * IMPORTANT: 需要在AndroidManifest.xml 配置 <meta-data android:name="Channel_ID" android:value="official"/>
     */
    public static String getChannelID(Context context) {
        String channelID = BasicConfig.INSTANCE.getChannel();
     /*   try {
            if (context != null) {
                String pkgName = context.getPackageName();
                ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(pkgName, PackageManager.GET_META_DATA);
                if (appInfo != null && appInfo.metaData != null) {
                    Object channelIdObj = appInfo.metaData.get("UMENG_CHANNEL");
                    if (channelIdObj instanceof Integer) {
                        channelID = String.valueOf(channelIdObj);
                    } else {
                        channelID = channelIdObj.toString();
                    }
                }
            }
        } catch (Exception e) {
            MLog.error("AppMetaDataUtil.getChannelID(Context context)", e);
        }*/
        channelID = channelID == null ? "official" : channelID;
        return channelID;
    }

    public static String getSvnBuildVersion(Context context) {
        return getMetaString(context, "SvnBuildVersion");
    }

    public static String getMetaString(Context context, String key) {
        String value = "";
        try {
            if (context != null) {
                String pkgName = context.getPackageName();
                ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(pkgName, PackageManager.GET_META_DATA);
                value = appInfo.metaData.getString(key);
            }
        } catch (Exception e) {
            MLog.error("AppMetaDataUtil getSvnBuildVersion", e);
        }

        return value;
    }

    public static String getUpdateId(Context context) {
        return getMetaString(context, "UpdateId");
    }

    /**
     * 获取签名的hashcode
     */
    public static int getPackageSignatureHashCode(Context context, String packageName) {
        try {
            android.content.pm.Signature[] signatures = null;
            PackageManager pm = context.getPackageManager();
//            if (Build.VERSION.SDK_INT >= 28) {
//                PackageInfo packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_SIGNING_CERTIFICATES);
//                signatures = packageInfo.signingInfo.getApkContentsSigners();
//            } else {
                List<PackageInfo> apps = pm.getInstalledPackages(PackageManager.GET_SIGNATURES);
                for (PackageInfo app : apps)
                    if (app.packageName.equals(packageName)) {
                        signatures = app.signatures;
                    }
//            }
            if (signatures != null && signatures.length > 0) {
                    LogUtil.d("sign",signatures[0].hashCode()+"");
                return signatures[0].hashCode();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
}
