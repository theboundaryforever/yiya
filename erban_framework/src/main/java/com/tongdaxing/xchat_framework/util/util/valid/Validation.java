package com.tongdaxing.xchat_framework.util.util.valid;



import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 基础断言类
 * 
 * @author <a href="mailto:kuanglingxuan@yy.com">匡凌轩</a> V2.0
 */
public class Validation {
	
	private static final String EXPRESSION_GREATER_EQUAL = ">=";
	private static final String EXPRESSION_EQUAL = "=";
	private static final String EXPRESSION_GREATER = ">";
	private static final String EXPRESSION_LESSER_EQUAL = "<=";
	private static final String EXPRESSION_LESSER = "<";

	/**
	 * 断言非空
	 * @param dataName
	 * @param values
	 */
	public static void checkNull(String dataName, Object... values) {
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		if(values == null){
			throw new IllegalArgumentException(dataName +" cannot be null");
		}
		for (int i = 0; i < values.length; i++) {
			Object value = values[i];
			if(value == null){
				throw new IllegalArgumentException(dataName +" cannot be null at " + dataName + "[" + i + "]");
			}
		}
	}
	
	/**
	 * 
	 * @param dataName
	 * @param expected
	 * @param value
	 */
	public static <T> void checkEquals(String dataName, T expected, T value){
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		if(value == null || !expected.equals(value)){
			if(dataName == null){
				throw new IllegalArgumentException(value + " is expected to be equal to " + expected);
			}else{
				throw new IllegalArgumentException(dataName + " is expected to be equal to " + value + " but in fact is " + expected);
			}
		}
	}
	
	public static <T> void checkEquals(T expected, T value){
		checkEquals(null, expected, value);
	}
	
	public static <T> void checkGreater(String dataName, Comparable<T> value, T expected){
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		if(value == null || value.compareTo(expected) <= 0){
			if(dataName == null){
				throw new IllegalArgumentException(value + " is expected to be greater than " + expected);
			}else{
				throw new IllegalArgumentException(dataName + " is expected to be greater than " + expected + " but in fact is " + value);
			}
		}
	}
	
	public static <T> void checkGreater(Comparable<T> value, T expected){
		checkGreater(null, value, expected);
	}
	
	public static <T> void checkGreaterEquals(String dataName, Comparable<T> value, T expected){
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		if(value == null || value.compareTo(expected) < 0){
			if(dataName == null){
				throw new IllegalArgumentException(value + " is expected to be greater equal than " + expected);
			}else{
				throw new IllegalArgumentException(dataName + " is expected to be greater equal than " + expected + " but in fact is " + value);
			}
		}
	}
	
	public static <T> void checkGreaterEquals(Comparable<T> value, T expected){
		checkGreaterEquals(null, value, expected);
	}
	
	public static <T> void checkLesser(String dataName, Comparable<T> value, T expected){
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		if (value == null ||  value.compareTo(expected) >= 0) {
			if(dataName == null){
				throw new IllegalArgumentException(value + " is expected to be lesser than " + expected);
			}else{
				throw new IllegalArgumentException(dataName + " is expected to be lesser than " + expected + " but in fact is " + value);
			}
		}
	}
	
	public static <T> void checkLesser(Comparable<T> value, T expected){
		checkLesser(null, value, expected);
	}
	
	public static <T> void checkLesserEquals(String dataName, Comparable<T> value, T expected){
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		if (value == null ||  value.compareTo(expected) > 0) {
			if(dataName == null){
				throw new IllegalArgumentException(value + " is expected to be lesser equal than " + expected);
			}else{
				throw new IllegalArgumentException(dataName + " is expected to be lesser equal than " + expected + " but in fact is " + value);
			}
		}
	}
	
	public static <T> void checkLesserEquals(Comparable<T> value, T expected){
		checkLesserEquals(null, value, expected);
	}
	
	public static void checkNumeric(String dataName, Object... values){
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		checkNull(dataName, values);
		for (Object value : values) {
			checkNull(dataName, value);
			checkRegex(dataName, "[-+]?[0-9]+", value.toString());
		}
	}
	
	/**
	 *	表达式组合验证，支持 =,>=,<,>,<=
	 *	Validation.check("=", 6, 6);//x == 6
	 *	Validation.check(">,<", 15, 10, 9, 10);// x > 10 && x < 10
	 */
	public static <T> void check(String expression, Comparable<T>... values){
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		String[] exps = expression.split(",");
		if(values.length % 2 != 0){
			throw new IllegalArgumentException("check failed cause by values is not a pair : "+ values.length);
		}
		for (int i = 0; i < values.length; ) {
			for (String exp : exps) {
				if (BlankUtil.isBlank(exp)) {
					throw new IllegalArgumentException(
							"check failed cause by expression : " + expression);
				}
				Comparable<T> value1 = values[i++];
				T value2 = (T) values[i++];
				if (EXPRESSION_EQUAL.equals(exp)) {
					checkEquals(value1, value2);
				} else if (EXPRESSION_GREATER.equals(exp)) {
					checkGreater(value1, value2);
				} else if (EXPRESSION_GREATER_EQUAL.equals(exp)) {
					checkGreaterEquals(value1, value2);
				} else if (EXPRESSION_LESSER.equals(exp)) {
					checkLesser(value1, value2);
				} else if (EXPRESSION_LESSER_EQUAL.equals(exp)) {
					checkLesserEquals(value1, value2);
				}
			}
		}
	}
	
	/**
	 * 正则表达式验证
	 * @param regex
	 * @param value
	 */
	public static void checkRegex(String dataName, String regex, String value){
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		Pattern pattern = Pattern.compile(regex);
		Matcher isNum = pattern.matcher(value);
		if( !isNum.matches() )
		{
			throw new IllegalArgumentException("check failed cause by " + dataName + " is not number : "+value);
		}
	}
	
	/**
	 * Bean需要实现Validateable的validate方法
	 * @param bean
	 */
	public static void checkBean(Validateable bean){
		if (!BasicConfig.INSTANCE.isDebuggable()) {
			return;
		}

		bean.validate();
	}
}
