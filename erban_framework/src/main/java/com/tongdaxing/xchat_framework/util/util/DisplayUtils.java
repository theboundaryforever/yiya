package com.tongdaxing.xchat_framework.util.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.ViewConfiguration;

import java.lang.reflect.Method;

/**
 * Android 显示工具类
 * px dp 屏幕宽高 状态栏 虚拟键等
 */
public class DisplayUtils {

    //获取虚拟按键的高度
    public static int getNavigationBarHeight(Context context) {
        int result = 0;
        if (hasNavBar(context)) {
            Resources res = context.getResources();
            int resourceId = res.getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = res.getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }

    /**
     * 检查是否存在虚拟按键栏
     *
     * @param context
     * @return
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static boolean hasNavBar(Context context) {
        Resources res = context.getResources();
        int resourceId = res.getIdentifier("config_showNavigationBar", "bool", "android");
        if (resourceId != 0) {
            boolean hasNav = res.getBoolean(resourceId);
            // check override flag
            String sNavBarOverride = getNavBarOverride();
            if ("1".equals(sNavBarOverride)) {
                hasNav = false;
            } else if ("0".equals(sNavBarOverride)) {
                hasNav = true;
            }
            return hasNav;
        } else { // fallback
            return !ViewConfiguration.get(context).hasPermanentMenuKey();
        }
    }

    /**
     * 判断虚拟按键栏是否重写
     *
     * @return
     */
    private static String getNavBarOverride() {
        String sNavBarOverride = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                Class c = Class.forName("android.os.SystemProperties");
                Method m = c.getDeclaredMethod("get", String.class);
                m.setAccessible(true);
                sNavBarOverride = (String) m.invoke(null, "qemu.hw.mainkeys");
            } catch (Throwable e) {
            }
        }
        return sNavBarOverride;
    }


    /**
     * 将px值转换为dip或dp值，保证尺寸大小不变
     *
     * @param pxValue （DisplayMetrics类中属性density）
     * @return
     */
    public static int px2dip(Context context, float pxValue) {
        if (context == null)
            return 0;
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将dip或dp值转换为px值，保证尺寸大小不变
     *
     * @param dipValue （DisplayMetrics类中属性density）
     * @return
     */
    public static int dip2px(Context context, float dipValue) {
        if (context == null)
            return 0;
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * 将px值转换为sp值，保证文字大小不变
     *
     * @param pxValue （DisplayMetrics类中属性scaledDensity）
     * @return
     */
    public static int px2sp(Context context, float pxValue) {
        if (context == null)
            return 0;
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }

    /**
     * 将sp值转换为px值，保证文字大小不变
     *
     * @param spValue （DisplayMetrics类中属性scaledDensity）
     * @return
     */
    public static int sp2px(Context context, float spValue) {
        if (context == null)
            return 0;
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    //获取屏幕高度
    public static int getScreenHeight(@NonNull Context context) {
        if (context == null)
            return 0;
        int height = context.getResources().getDisplayMetrics().heightPixels;
        return height;
    }
    //获取屏幕宽度
    public static int getScreenWidth(@NonNull Context context) {
        if (context == null)
            return 0;
        int width = context.getResources().getDisplayMetrics().widthPixels;
        return width;
    }

    /**
     * 根据屏幕宽度算出实际的高度
     *
     * @param activity
     * @param width
     * @param height
     * @return
     */
    public static int getRealHeightByScreenWidth(Activity activity, int width, int height) {
        int realHeight = 0;
        int screenWidth = getScreenWidth(activity);
        realHeight = (int) (screenWidth * height / (float) width);
        return realHeight;
    }

    /**
     * 根据屏幕宽度算出实际的宽度
     *
     * @param activity
     * @param baseWidth
     * @param cellWidth
     * @return
     */
    public static int getRealWidthByScreenWidth(Activity activity, int baseWidth, int cellWidth) {
        int realWidth = 0;
        int screenWidth = getScreenWidth(activity);
        //realWidth=(int)(baseWidth*screenWidth/(float)cellWidth);
        realWidth = (int) (screenWidth * cellWidth / (float) baseWidth);
        return realWidth;
    }

    /**
     * 根据实际的宽度计算出实际的高度
     *
     * @param baseWidth
     * @param baseHeight
     * @param realWidth
     * @return
     */
    public static int getRealHeightByRealWidth(int baseWidth, int baseHeight, int realWidth) {
        int realHeight = 0;
        realHeight = (int) (realWidth * baseHeight / (float) baseWidth);
        return realHeight;
    }

    /**
     * 计算innerLine的宽度
     *
     * @param width
     * @param height
     * @param cellHeight
     * @return
     */
    public static int getInnerLineWidth(int width, int height, int cellHeight) {
        int cellWidth = 0;
        cellWidth = (int) (width * cellHeight / (float) height);
        return cellWidth;
    }

    /**
     * 获取状态栏高度
     *
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Context context) {
        int statusBarHeight = 0;
        try {
            Class<?> clazz = Class.forName("com.android.internal.R$dimen");
            Object object = clazz.newInstance();
            int height = Integer.parseInt(clazz.getField("status_bar_height")
                    .get(object).toString());
            statusBarHeight = context.getResources().getDimensionPixelSize(height);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusBarHeight;
    }


    public static int getStatusBarH(Context context) {
        int statusBarHeight = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }

    /**
     * @return 返回指定的文字高度
     */
    public static float getFontHeight(Paint paint) {
        Paint.FontMetrics fm = paint.getFontMetrics();
        //文字基准线的下部距离-文字基准线的上部距离 = 文字高度
        return (float) Math.ceil(fm.descent - fm.ascent);
    }
}
