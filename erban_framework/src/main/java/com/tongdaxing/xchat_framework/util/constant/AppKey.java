package com.tongdaxing.xchat_framework.util.constant;

public class AppKey {
    /**
     * 网易云盾图形验证码
     */
    public static String getCaptchaId() {
        return "363581aa96d34ab1b12e023baf48c691";
    }

    /**
     * 友盟Appkey
     */
    public static String getUmengAppkey() {
        return "5cb1ac43570df35816000df9";
    }
}
