package com.tongdaxing.xchat_framework.thirdsdk.umeng;

import android.content.Context;

import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;

/**
 * 友盟事件统计工具类
 * event id不能使用特殊字符，不建议使用中文，且长度不能超过128个字节；
 * map中的key和value 都不能使用特殊字符，key 不能超过128个字节，value 不能超过256个字节。
 * id，ts，du是保留字段，不能作为event id及key的名称。
 * 每个应用至多添加500个自定义事件，每个event 的 key不能超过10个，每个key的取值不能超过1000个。
 * 如需要统计支付金额、使用时长等数值型的连续变量，请使用计算事件（不允许通过key-value结构来统计类似搜索关键词，网页链接等随机生成的字符串信息）。
 */
public class UmengEventUtil {

    private static volatile UmengEventUtil instance;

    private UmengEventUtil() {
    }

    public static UmengEventUtil getInstance() {
        if (instance == null) {
            synchronized (UmengEventUtil.class) {
                if (instance == null) {
                    instance = new UmengEventUtil();
                }
            }
        }
        return instance;
    }

    /**
     * 通用的计数事件。
     * @param context 当前宿主进程的ApplicationContext上下文。
     * @param eventId 为当前统计的事件ID。
     */
    public void onEvent(Context context,String eventId){
        if (context == null)
            return;
        MobclickAgent.onEvent(context, eventId);
    }


    /**
     * 转发型通用的计数事件。
     * 例如：统计微博应用中”转发”事件发生的次数，那么在转发的函数里调用
     * @param context 当前宿主进程的ApplicationContext上下文。
     * @param eventId 为当前统计的事件ID。
     * @param label	事件的标签属性。
     */
    public void onEvent(Context context,String eventId,String label){
        if (context == null)
            return;
        MobclickAgent.onEvent(context, eventId,label);
    }

//  下面的方法是在：点击行为各属性被触发的次数考虑事件在不同属性上的取值：
    /**
     * 例如：统计电商应用中”购买”事件发生的次数，以及购买的商品类型及数量，那么在购买的函数里调用：
     * context	当前宿主进程的ApplicationContext上下文。
     * eventId	为当前统计的事件ID。
     * map	为当前事件的属性和取值（Key-Value键值对）。
     */


    /**
     * 充值金额点击统计
     * @param context
     * @param chargeProdId "chargeProdId": 1,  //充值产品ID
     */
    public void onRechargeAmount(Context context, int chargeProdId,int money) {
        if (context == null)
            return;
        HashMap<String, String> map = new HashMap<>();
        map.put("amount", chargeProdId+"");
        map.put("money",money+"");
        MobclickAgent.onEvent(context, UmengEventId.getRechargeAmount(), map);
    }

}
