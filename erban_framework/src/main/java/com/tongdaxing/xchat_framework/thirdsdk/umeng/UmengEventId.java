package com.tongdaxing.xchat_framework.thirdsdk.umeng;

/**
 * 友盟埋点事件id
 */
public class UmengEventId {


    // ----------------------------- 注册登陆 ---------------------------

    /**
     * 登录注册_补充用户信息
     * @return
     */
    public static String getRegisterUserInfoCommit(){
        return "register_finish_user_info_commit_onclick";
    }


    // ------------------------------ 首页 ------------------------------

    /**
     * 首页_搜索点击
     * @return
     */
    public static String getHomeSearchBtn(){
        return "home_search_onclick";
    }

    // ------------------------------  发现 -----------------------------

    /**
     * 发现_我的房间点击
     * @return
     */
    public static String getFindMyRoom(){
        return "find_my_room_onclick";
    }

    /**
     * 发现_公聊大厅点击
     * @return
     */
    public static String getFindPublicChat(){
        return "find_public_chat_onclick";
    }

    // -----------------------------------  房间 --------------------------------

    /**
     * 房间_点击送礼资料点击
     * @return
     */
    public static String getRoomGiftUserInfo(){
        return "room_gift_userinfo_onclick";
    }

    /**
     * 房间_砸蛋十次点击
     * @return
     */
    public static String getRoomEggTen(){
        return "room_egg_ten_onclick";
    }

    /**
     * 房间_关注点击
     * @return
     */
    public static String getRoomAttention(){
        return "room_attention_onclick";
    }

    /**
     * 房间发言
     * @return
     */
    public static String getRoomShare(){
        return "room_share_onclick";
    }


    /**
     * 房间发言
     * @return
     */
    public static String getRoomSendMsg(){
        return "room_send_msg";
    }

    /**
     * 房间打开砸蛋
     * @return
     */
    public static String getRoomOpenEgg(){
        return "room_open_egg";
    }

    /**
     * 房间红包点击
     * @return
     */
    public static String getRoomRedPackage(){
        return "room_red_package_onclick";
    }

    //------------------------------------- 个人主页 ---------------------

    /**
     * 个人主页_去找Ta点击
     * @return
     */
    public static String getUserHomeFindTa(){
        return "user_home_find_ta_onclick";
    }



    // -------------------------------------  充值 -------------------------

    /**
     * 充值_金额统计
     * @return
     */
    public static String getRechargeAmount(){
        return "recharge_amount_onclick";
    }

}
