package com.tongdaxing.xchat_framework.im;

public interface IMReportRoute {
    //通知
    String ChatRoomMemberBlackRemove = "ChatRoomMemberBlackRemove";
    String ChatRoomMemberBlackAdd = "ChatRoomMemberBlackAdd";
    String chatRoomMemberIn = "chatRoomMemberIn";
    String ChatRoomTip = "ChatRoomTip";
    String kickoff = "kickoff";
    String chatRoomMemberExit = "chatRoomMemberExit";
    String ChatRoomInfoUpdated = "ChatRoomInfoUpdated";
    String ChatRoomMemberKicked = "ChatRoomMemberKicked";
    String QueueMemberUpdateNotice = "QueueMemberUpdateNotice";
    String QueueMicUpdateNotice = "QueueMicUpdateNotice";
    String sendMessageReport = "sendMessageReport";
    String updateQueue = "updateQueue";
    String ChatRoomManagerAdd = "ChatRoomManagerAdd";
    String ChatRoomManagerRemove = "ChatRoomManagerRemove";
    String sendTextReport = "sendTextReport";
    String enterPublicRoom = "enterPublicRoom";
    String sendPublicMsgNotice = "sendPublicMsgNotice";
    String sendPublicMsg = "sendPublicMsg";
    String passmicNotice = "passmicNotice";
    String requestNoRoomUserNotice = "requestNoRoomUserNotice";
    String applyWaitQueueNotice = "applyWaitQueueNotice";
    String chatRoomMemberMute = "ChatRoomMemberMute";
    String chatRoomMemberMuteCancel = "ChatRoomMemberMuteCancel";
    String roomClientMicAgree = "roomClientMicAgree";
    String roomClientMicReject = "roomClientMicReject";
    String roomReceiveSum = "roomReceiveSum";//视屏房收益通知

    String sendUploadLogNotice = "sendUploadLogNotice";
    String roomClientMicApply = "roomClientMicApply";//有嘉宾申请连麦
    String roomClientMicCancel = "roomClientMicCancel";//嘉宾取消连麦

    String ChatRoomMemberWanMesg = "ChatRoomMemberWanMesg";//发送警告给主播
    //    String ChatRoomMemberKicked = "ChatRoomMemberKicked";//踢出主播(强制关播)
    String ChatRoomMemberBlock = "ChatRoomMemberBlock";//直播封禁
    String RoomForceOnLiveNotice = "roomForceOnLiveNotice";//直播封禁

    String activationTreasureChestMessage = "activationTreasureChestMessage";//宝箱激活通知
    String LuckyTreasureChestWinning = "LuckyTreasureChestWinning";//幸运提示
    String LuckyTreasureChestWinningUser = "LuckyTreasureChestWinningUser";//超级宝箱中奖用户通知
    String LuckyTreasureChestWinningRoomUser = "LuckyTreasureChestWinningRoomUser";//超级宝箱中奖主播通知
    String LuckyTreasureChestRoomAddGold = "LuckyTreasureChestRoomAddGold";//超级宝箱追加金币全服通知
    String pushUserExperienceUpdate = "pushUserExperienceUpdate";// 用户等级更新

    // 魅力值PK
    String sendPkInvite = "sendPkInvite";// 发送魅力值pk邀请
    String sendPkResult = "sendPkResult";// 发送魅力值pk结果消息
    String sendPkMsg = "sendPkMsg";// 发送魅力值pk消息

    // 猪猪大作战
    String routePigFightResult = "routePigFightResult";// 猪猪大作战结果
    String openOrClosePigFight = "openOrClosePigFight";// 猪猪大作战结果

    String BarrageMessage = "barrageMessage";//弹幕

    String headlineStarLimit = "headlineStarLimit";//头条之星分数超过两万
    String headlineStarDistance = "headlineStarDistance";//头条之星距离上一名
    String headlineStarPopup = "headlineStarPopup";//头条之星公告
    String seedMelonGoldPool = "seedMelonGoldPool";//种瓜金币池变化消息

}
