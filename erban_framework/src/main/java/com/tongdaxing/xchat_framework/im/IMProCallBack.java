package com.tongdaxing.xchat_framework.im;

abstract public class IMProCallBack extends IMCallBack {
    @Override
    public void onSuccess(String data) {
        IMReportBean imReportBean = new IMReportBean(data);
        onSuccessPro(imReportBean);
    }

    public abstract void onSuccessPro(IMReportBean imReportBean);
}
