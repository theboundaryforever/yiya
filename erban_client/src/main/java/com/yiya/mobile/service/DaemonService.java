package com.yiya.mobile.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.ReUsedSocketManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yiya.mobile.reciever.NotificationClickReceiver;

/**
 * @author chenran
 * @date 2017/11/16
 */

public class DaemonService extends Service {
    private static final String TAG = "DaemonService";
    public static final int NOTICE_ID = 100;
    Notification.Builder builder;

    public static void start(Context context, RoomInfo roomInfo) {
        Intent intent = new Intent(context, DaemonService.class);
        intent.putExtra("title", roomInfo.getTitle());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            context.startForegroundService(intent);
        }else {
            context.startService(intent);
        }
    }

    public static void stop(Context context) {
        Intent intent = new Intent(context, DaemonService.class);
        context.stopService(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        String title = "咿呀";
        if (roomInfo != null) {
            title = roomInfo.getTitle();
            builder = new Notification.Builder(this);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O && manager != null) {
                NotificationChannel notificationChannel = new NotificationChannel(getPackageName(), getResources().getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_LOW);
                notificationChannel.setShowBadge(true);
                //任何情况都显示通知完整内容
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                //删除通知（清除历史）渠道
                manager.deleteNotificationChannel(getPackageName());
                //创建通知渠道
                manager.createNotificationChannel(notificationChannel);
            }
            builder.setSmallIcon(R.mipmap.ic_launcher);
            builder.setContentTitle(title);
            builder.setContentText("点击返回房间");
//            builder.setTicker("正在房间内");
            Intent clickIntent = new Intent(this, NotificationClickReceiver.class);
            PendingIntent contentIntent = PendingIntent.getBroadcast(this.getApplicationContext(), (int) System.currentTimeMillis(), clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(contentIntent);
            startForeground(NOTICE_ID, builder.build());
        }
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        //断开socket
        ReUsedSocketManager.get().destroy();
        //清除房间数据
        AvRoomDataManager.get().release();
        stopForeground(true);
    }
}
