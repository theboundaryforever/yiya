package com.yiya.mobile.constant;

/**
 * 文件描述：app通用常量
 *
 * @auther：zwk
 * @data：2019/1/2
 */
public interface AppConstant {
    //跳转进入QQ群聊的key
    String JOIN_QQ_GROUP_KEY = "0ViP-b7IMxsGzsDmvH7Xd9UkPFr5r7Sr";


    // ----------------------------------------------- 默认浏览器的标题栏主题 ----------------------------------------------
    int WEB_TITLE_TYPE_APP_THEME = 1;

    int WEB_TITLE_TYPE_RNAKING_THEME = 2;

}
