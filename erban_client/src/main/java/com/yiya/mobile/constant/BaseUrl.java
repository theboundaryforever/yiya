package com.yiya.mobile.constant;

import com.tongdaxing.xchat_core.UriProvider;

public class BaseUrl {
    /**
     * app固定跳转H5地址
     */
//    public static final String BASE_APP_TAG = UriProvider.JAVA_WEB_URL + "";

    //用户协议
    public static final String USER_AGREEMENT = UriProvider.JAVA_WEB_URL + "/front/agreement/index.html";
    //常见问题
    public static final String COMMON_PROBLEM = UriProvider.JAVA_WEB_URL + "/front/question/question.html";
    //我的等级
    public static final String MY_LEVEL = UriProvider.JAVA_WEB_URL + "/front/levels/index.html";
    //提现规则
    public static final String WITHDRAW_RULES = UriProvider.IM_SERVER_URL + "/modules/guide/withdraw.html";
    //实名认证
    public static final String REAL_NAME = UriProvider.JAVA_WEB_URL + "/front/realname/index.html";
    //主播认证
    public static final String ANCHOR_AUTHENTICATION = UriProvider.JAVA_WEB_URL + "/front/AnchorAuthentication/index.html";
    //主播记录
    public static final String ANCHOR_RECORD = UriProvider.JAVA_WEB_URL + "/front/anchor_record/index.html";


    //邀请奖励排行榜
    public static final String INVITE_REWARD_RANK = UriProvider.JAVA_WEB_URL + "/front/invitationRank/index.html";
    //我的邀请人数
    public static final String MY_INVITE_PEOPLE = UriProvider.IM_SERVER_URL + "/front/invitation/index.html";
    //我的分成奖励
    public static final String MY_INVITE_PERCENTAGE = UriProvider.IM_SERVER_URL + "/front/percentage/index.html";
    //我的邀请规则说明
    public static final String MY_INVITE_RULES = UriProvider.IM_SERVER_URL + "/front/method/method.html";
    //全服榜单
    public static final String ROOM_RANK = UriProvider.JAVA_WEB_URL + "/front/all_ranks/index.html";

    //圆盘抽奖
    public static final String LUCK_DRAW = UriProvider.JAVA_WEB_URL + "/front/draw/index.html";

    //分享模块的下载页地址
    public static final String SHARE_DOWNLOAD = UriProvider.JAVA_WEB_URL + "/front/download/download.html";
    //分享默认logo图
    public static final String SHARE_DEFAULT_LOGO = UriProvider.JAVA_WEB_URL + "/home/images/logo.png";

    //首页推荐人气厅列表 -- 我要上推荐
    public static final String HOME_UP_RECOMMEND = UriProvider.IM_SERVER_URL + "/front/hotroom/index.html";

    //扭蛋
    public static final String GASHAPON = UriProvider.JAVA_WEB_URL + "/front/gashapon/index.html";

    //TODO userAgent
    public static final String USER_AGENT = "yumengAppAndroid";

    public static final String WITTH_DRAW = UriProvider.JAVA_WEB_URL + "/modules/guide/withdraw.html";

    public static final String HOME_ADD_RECOMMEND = UriProvider.JAVA_WEB_URL + "/front/hotroom/index.html";

    public static final String MENG_COIN_URL = UriProvider.JAVA_WEB_URL + "/front/luck_issue/index.html#/";

    public static final String REAL_NAME_URL = UriProvider.JAVA_WEB_URL + "/front/real_name/index.html";

    //幸运宝箱
//    public static final String LUCKY_BOX = UriProvider.JAVA_WEB_URL + "/front/treasure_box/index.html?abcd_usecache=1";
    public static final String LUCKY_BOX = UriProvider.JAVA_WEB_URL + "/front/treasure_box_new/index.html?abcd_usecache=1";

    //任务中心
    public static final String MISSION_CENTER = UriProvider.JAVA_WEB_URL + "/front/taskCenter/index.html";

    //收不到验证码
    public static final String NOT_GET_SMS_CODE = UriProvider.JAVA_WEB_URL + "/front/code/index.html";

    //种瓜
    public static final String PLANT_CUCUMBER = UriProvider.JAVA_WEB_URL + "/front/seed_melon/index.html";

    //首充特惠
    public static final String FIRST_CHARGE = UriProvider.JAVA_WEB_URL + "/front/newUserGift/index.html";

    // 头条之星
    public static final String HEAD_LINE_STAR = UriProvider.JAVA_WEB_URL + "/front/headline_star/index.html";

    // 银行卡支付
    public static final String BANK_PAY = UriProvider.JAVA_WEB_URL + "/front/formField/successUrl.html";

    //意见反馈
    public static final String FEED_BACK = UriProvider.JAVA_WEB_URL + "/front/suggest/index.html";

    //装扮商场
    public static final String DRESS_MALL = UriProvider.JAVA_WEB_URL + "/front/dress_mall/index.html";

    //贡献榜
    public static final String CONTRIBUTION = UriProvider.JAVA_WEB_URL + "/front/ranks/index.html";

    //个人主页贡献榜
    public static final String USER_INFO_CONTRIBUTION = UriProvider.JAVA_WEB_URL + "/front/rank/index.html";

    //邀请奖励
    public static final String INVITATION = UriProvider.JAVA_WEB_URL + "/front/invitations/index.html";

    //注册、登录帮助
    public static final String HELP = UriProvider.JAVA_WEB_URL + "/front/help/index.html";

}