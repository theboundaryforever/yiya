package com.yiya.mobile.room.egg.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.egg.IPlantRankListView;
import com.yiya.mobile.presenter.egg.PlantRankListPresenter;
import com.yiya.mobile.room.egg.adapter.PlantRankListAdapter;
import com.yiya.mobile.utils.UIHelper;

import java.util.List;

import butterknife.BindView;

/**
 * ProjectName:
 * Description:砸蛋排行榜
 * Created by BlackWhirlwind on 2019/7/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
@CreatePresenter(PlantRankListPresenter.class)
public class PlantRankListFragment extends BaseMvpFragment<IPlantRankListView, PlantRankListPresenter> implements IPlantRankListView, RadioGroup.OnCheckedChangeListener, BaseQuickAdapter.OnItemClickListener {

    @BindView(R.id.rg_rank)
    RadioGroup rgRank;
    @BindView(R.id.rv_egg_rank)
    RecyclerView rvEggRank;
    private long mRoomId;
    private int type = 1;
    private PlantRankListAdapter mRankListAdapter;

    @Override
    public int getRootLayoutId() {
        return R.layout.layout_egg_rank_list;
    }

    @Override
    public void onFindViews() {
        rvEggRank.setLayoutManager(new LinearLayoutManager(getContext()));
        rvEggRank.setHasFixedSize(true);
        mRankListAdapter = new PlantRankListAdapter();
        mRankListAdapter.setEmptyView(R.layout.layout_empty_view, rvEggRank);
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo != null) {
            mRoomId = roomInfo.getUid();
            mRankListAdapter.setRoomType(roomInfo.getType());
        }
        rvEggRank.setAdapter(mRankListAdapter);
    }

    @Override
    public void onSetListener() {
        rgRank.setOnCheckedChangeListener(this);
        mRankListAdapter.setOnItemClickListener(this);
    }

    @Override
    public void initiate() {
        getMvpPresenter().getRankList(mRoomId, type);
    }

    @Override
    public void showRankList(List<UserInfo> list) {
        if (mRankListAdapter != null) {
            mRankListAdapter.setNewData(list);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_tab_day:
                typeChange(1);
                break;
            case R.id.rb_tab_week:
                typeChange(2);
                break;
            case R.id.rb_tab_all:
                typeChange(3);
                break;
            default:
                break;
        }
        getMvpPresenter().getRankList(mRoomId, type);

    }

    private void typeChange(int i) {
        if (i == type) {
            return;
        }
        type = i;
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        List list = adapter.getData();
        if (ListUtils.isListEmpty(list)) {
            return;
        }
        UserInfo userInfo = (UserInfo) list.get(position);
        // 跳去主页
        UIHelper.showUserInfoAct(getContext(), userInfo.getUid());
    }
}
