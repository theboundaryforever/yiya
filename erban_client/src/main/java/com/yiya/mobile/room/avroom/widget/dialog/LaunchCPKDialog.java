package com.yiya.mobile.room.avroom.widget.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.room.model.RoomModel;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.zyyoona7.lib.EasyPopup;
import com.zyyoona7.lib.HorizontalGravity;
import com.zyyoona7.lib.VerticalGravity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zdw
 * @date 2019/7/19
 *
 * @describe 发起魅力值PK dialog
 */

public class LaunchCPKDialog extends DialogFragment implements View.OnClickListener{

    @BindView(R.id.rl_contain)
    RelativeLayout rlContain;

    @BindView(R.id.rl_join_in)
    RelativeLayout rlJoinIn;
    @BindView(R.id.rv_join_in)
    RecyclerView rvJoinJn;

    @BindView(R.id.rl_time_sel)
    RelativeLayout rlTimeSel;
    @BindView(R.id.tv_join_time)
    TextView tvJoinTime;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;

    private EasyPopup joinPopup,timePopup;
    private CPKAdapter cPKAdapter;
    private ArrayList<RoomQueueInfo> selectJoinIns = new ArrayList<>();// 已选择人选
    private int time = 0;// 当前选中比赛时长
    private RoomModel roomModel = new RoomModel();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,R.style.transFullScreenDialog); //dialog全屏
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.dialog_cpk_launch, container);
        ButterKnife.bind(this, mRootView);

        initView();
        setOnListener();

        return mRootView;
    }

    private void setOnListener() {
        rlJoinIn.setOnClickListener(v -> {
            ArrayList<RoomQueueInfo> infos = AvRoomDataManager.get().getRoomQueueMemberInfos();
            if (ListUtils.isListEmpty(infos)){
                SingleToastUtil.showToast(getContext(), "当前麦位上没人", Toast.LENGTH_SHORT);
            } else {
                initJoinPop(infos);
                joinPopup.showAtAnchorView(rlJoinIn,
                        VerticalGravity.BELOW, HorizontalGravity.CENTER, 0, 10);
            }
        });
        rlTimeSel.setOnClickListener(v -> timePopup.showAtAnchorView(rlTimeSel,VerticalGravity.BELOW, HorizontalGravity.CENTER, 0, 10));


        tvConfirm.setOnClickListener(v -> confirm());
        tvConfirm.setClickable(false);

        rlContain.setOnClickListener(v -> dismiss());
    }

    private void initView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvJoinJn.setLayoutManager(mLayoutManager);
//        rvJoinJn.addItemDecoration(new RecyclerView.ItemDecoration() {
//            @Override
//            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//                super.getItemOffsets(outRect, view, parent, state);
//                outRect.left = 10;
//                outRect.right = 10;
//            }
//        });
        cPKAdapter = new CPKAdapter();
        rvJoinJn.setAdapter(cPKAdapter);

        initTimePop();
    }

    // 初始化参与人pop
    private void initJoinPop(ArrayList<RoomQueueInfo> infos) {
        // 参与人选择
        joinPopup = new EasyPopup(getContext())
                .setContentView(R.layout.dialog_cpk_sel_person)
                .setWidth(DisplayUtils.dip2px(getContext(),225))
                .setFocusAndOutsideEnable(true) // 是否允许点击PopupWindow之外的地方消失
                .createPopup();

        for (int i=0; i < infos.size(); i++){
            RoomQueueInfo roomQueueInfo = infos.get(i);
            switch (i){
                case 0:
                    setJoinPeople(joinPopup.getView(R.id.pup_0), joinPopup.getView(R.id.tv_0), roomQueueInfo);
                    break;
                case 1:
                    setJoinPeople(joinPopup.getView(R.id.pup_1), joinPopup.getView(R.id.tv_1), roomQueueInfo);
                    break;
                case 2:
                    setJoinPeople(joinPopup.getView(R.id.pup_2), joinPopup.getView(R.id.tv_2), roomQueueInfo);
                    break;
                case 3:
                    setJoinPeople(joinPopup.getView(R.id.pup_3), joinPopup.getView(R.id.tv_3), roomQueueInfo);
                    break;
                case 4:
                    setJoinPeople(joinPopup.getView(R.id.pup_4), joinPopup.getView(R.id.tv_4), roomQueueInfo);
                    break;
                case 5:
                    setJoinPeople(joinPopup.getView(R.id.pup_5), joinPopup.getView(R.id.tv_5), roomQueueInfo);
                    break;
                case 6:
                    setJoinPeople(joinPopup.getView(R.id.pup_6), joinPopup.getView(R.id.tv_6), roomQueueInfo);
                    break;
                case 7:
                    setJoinPeople(joinPopup.getView(R.id.pup_7), joinPopup.getView(R.id.tv_7), roomQueueInfo);
                    break;
            }
        }
    }

    private void setJoinPeople(LinearLayout ll, TextView textView, RoomQueueInfo roomQueueInfo){
        ll.setOnClickListener(this);
        ll.setVisibility(View.VISIBLE);
        ll.setTag(roomQueueInfo);
        if(roomQueueInfo.mChatRoomMember == null)return;
        textView.setText(roomQueueInfo.mChatRoomMember.getNick());
    }

    // 初始化时间选择pop
    private void initTimePop(){
        timePopup = new EasyPopup(getContext())
                .setContentView(R.layout.dialog_cpk_sel_time)
                .setWidth(DisplayUtils.dip2px(getContext(),225))
                .setFocusAndOutsideEnable(true) // 是否允许点击PopupWindow之外的地方消失
                .createPopup();

        timePopup.getView(R.id.ll_time_1).setOnClickListener(v -> setJoinTime(1, "1分钟"));
        timePopup.getView(R.id.ll_time_2).setOnClickListener(v -> setJoinTime(3, "3分钟"));
        timePopup.getView(R.id.ll_time_3).setOnClickListener(v -> setJoinTime(5, "5分钟"));
        timePopup.getView(R.id.ll_time_4).setOnClickListener(v -> setJoinTime(10, "10分钟"));
    }

    private void setJoinTime(int time, String timeStr){
        timePopup.dismiss();
        Context context = getContext();
        if(context == null){
            SingleToastUtil.showToast(getContext(), "出现异常情况", Toast.LENGTH_SHORT);
        } else {
            tvJoinTime.setTextColor(context.getResources().getColor(R.color.color_383950));
            tvJoinTime.setText(timeStr);
            this.time = time;
            refreshConfirmStatus();
        }
    }

    private void selectPos(RoomQueueInfo roomQueueInfo) {
        if (roomQueueInfo == null)return;
        for (RoomQueueInfo info : selectJoinIns){
            if(info.mRoomMicInfo.getPosition() == roomQueueInfo.mRoomMicInfo.getPosition())
                return;
        }
        selectJoinIns.add(roomQueueInfo);
        cPKAdapter.setNewData(selectJoinIns);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pup_0:
            case R.id.pup_1:
            case R.id.pup_2:
            case R.id.pup_3:
            case R.id.pup_4:
            case R.id.pup_5:
            case R.id.pup_6:
            case R.id.pup_7:
                joinPopup.dismiss();
                RoomQueueInfo roomQueueInfo = (RoomQueueInfo)v.getTag();
                selectPos(roomQueueInfo);
                refreshConfirmStatus();
                break;
            default:
        }
    }

    private void refreshConfirmStatus(){
        List<RoomQueueInfo> data = cPKAdapter.getData();
        if(ListUtils.isListEmpty(data) || time == 0){
            tvConfirm.setBackgroundResource(R.mipmap.ic_cpk_confirm_disable);
            tvConfirm.setClickable(false);
        } else {
            tvConfirm.setBackgroundResource(R.mipmap.ic_cpk_confirm_enable);
            tvConfirm.setClickable(true);
        }
    }

    private void confirm(){
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        List<RoomQueueInfo> data = cPKAdapter.getData();
        if(roomInfo == null || ListUtils.isListEmpty(data) || time <=0 ){
            return;
        }

        StringBuilder uids = new StringBuilder();
        for (RoomQueueInfo roomQueueInfo : data){
            if(roomQueueInfo.mChatRoomMember != null && roomQueueInfo.mChatRoomMember.getAccount() != null){
                uids.append(roomQueueInfo.mChatRoomMember.getAccount());
                uids.append(",");
            } else {
                SingleToastUtil.showToast(getContext(), "存在用户不在麦上，请重新选择", Toast.LENGTH_SHORT);
                return;
            }
        }
        if (StringUtils.isEmpty(uids) || uids.length() < 2) return;

        uids.deleteCharAt(uids.length() - 1);

        roomModel.confirmCharmPK(time*60, CoreManager.getCore(IAuthCore.class).getCurrentUid(), roomInfo.getRoomId(), uids.toString(), new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {
                LogUtil.i("confirmCharmPK : "+response);
            }

            @Override
            public void onFailure(int code, String msg) {
                SingleToastUtil.showToast(msg);
            }
        });

        dismiss();
    }

    public class CPKAdapter extends BaseQuickAdapter<RoomQueueInfo, BaseViewHolder> {

        CPKAdapter() {
            super(R.layout.list_item_cpk_sel);
        }

        @Override
        protected void convert(BaseViewHolder helper, RoomQueueInfo info) {
            if(info == null || info.mChatRoomMember == null || info.getMRoomMicInfo() == null){
                return;
            }
            IMChatRoomMember member = info.mChatRoomMember;
            String nick = member.getNick();

            TextView textView = helper.getView(R.id.tv_nick);
            ImageView ivCancel = helper.getView(R.id.iv_cancel);
            ivCancel.setOnClickListener(v -> {
                for (int i=0; selectJoinIns != null && i<selectJoinIns.size(); i++){
                    RoomQueueInfo info1 = selectJoinIns.get(i);
                    if(info1 != null && info1.mChatRoomMember != null && info1.getMRoomMicInfo() != null){
                        if(info1.getMRoomMicInfo().getPosition() == info.getMRoomMicInfo().getPosition()){
                            selectJoinIns.remove(i);
                        }
                    }
                }
                cPKAdapter.setNewData(selectJoinIns);
                refreshConfirmStatus();
            });

            if(nick != null){
                if(nick.length() > 5){
                    nick = nick.substring(0, 5).concat("...");
                }
                textView.setText(nick);
            }
        }
    }
}


//    public class LaunchCPKDialog extends Dialog implements View.OnClickListener{
//
//    @BindView(R.id.v_placeholder)
//    View vPlaceholder;
//
//    @BindView(R.id.rl_join_in)
//    RelativeLayout rlJoinIn;
//    @BindView(R.id.rv_join_in)
//    RecyclerView rvJoinJn;
//
//    @BindView(R.id.rl_time_sel)
//    RelativeLayout rlTimeSel;
//    @BindView(R.id.tv_join_time)
//    TextView tvJoinTime;
//    @BindView(R.id.tv_confirm)
//    TextView tvConfirm;
//
//    private EasyPopup joinPopup,timePopup;
//    private CPKAdapter cPKAdapter;
//    private ArrayList<RoomQueueInfo> selectJoinIns = new ArrayList<>();// 已选择人选
//    private int time = 0;// 当前选中比赛时长
//    private RoomModel roomModel = new RoomModel();
//
//    public LaunchCPKDialog(Context context) {
//        super(context, R.style.transDialog);
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.dialog_cpk_launch);
//        ButterKnife.bind(this);
//
//        initView();
//
//        setOnListener();
//    }
//
//    private void setOnListener() {
//        rlJoinIn.setOnClickListener(v -> {
//            ArrayList<RoomQueueInfo> infos = AvRoomDataManager.get().getRoomQueueMemberInfos();
//            if (ListUtils.isListEmpty(infos)){
//                SingleToastUtil.showToast("当前麦位上没人");
//            } else {
//                initJoinPop(infos);
//                joinPopup.showAtAnchorView(vPlaceholder, VerticalGravity.BELOW, HorizontalGravity.CENTER, 0, 10);
//            }
//        });
//        rlTimeSel.setOnClickListener(v -> timePopup.showAtAnchorView(vPlaceholder, VerticalGravity.BELOW, HorizontalGravity.CENTER, 0, DisplayUtils.dip2px(getContext(),200)));
//
//        tvConfirm.setOnClickListener(v -> confirm());
//        tvConfirm.setClickable(false);
//    }
//
//    private void initView() {
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
//        rvJoinJn.setLayoutManager(mLayoutManager);
////        rvJoinJn.addItemDecoration(new RecyclerView.ItemDecoration() {
////            @Override
////            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
////                super.getItemOffsets(outRect, view, parent, state);
////                outRect.left = 10;
////                outRect.right = 10;
////            }
////        });
//        cPKAdapter = new CPKAdapter();
//        rvJoinJn.setAdapter(cPKAdapter);
//
//        initTimePop();
//    }
//
//    // 初始化参与人pop
//    private void initJoinPop(ArrayList<RoomQueueInfo> infos) {
//        // 参与人选择
//        joinPopup = new EasyPopup(getContext())
//                .setContentView(R.layout.dialog_cpk_sel_person)
//                .setWidth(DisplayUtils.dip2px(getContext(),225))
//                .setFocusAndOutsideEnable(true) // 是否允许点击PopupWindow之外的地方消失
//                .createPopup();
//
//        for (int i=0; i < infos.size(); i++){
//            RoomQueueInfo roomQueueInfo = infos.get(i);
//            switch (i){
//                case 0:
//                    setJoinPeople(joinPopup.getView(R.id.pup_0), joinPopup.getView(R.id.tv_0), roomQueueInfo);
//                    break;
//                case 1:
//                    setJoinPeople(joinPopup.getView(R.id.pup_1), joinPopup.getView(R.id.tv_1), roomQueueInfo);
//                    break;
//                case 2:
//                    setJoinPeople(joinPopup.getView(R.id.pup_2), joinPopup.getView(R.id.tv_2), roomQueueInfo);
//                    break;
//                case 3:
//                    setJoinPeople(joinPopup.getView(R.id.pup_3), joinPopup.getView(R.id.tv_3), roomQueueInfo);
//                    break;
//                case 4:
//                    setJoinPeople(joinPopup.getView(R.id.pup_4), joinPopup.getView(R.id.tv_4), roomQueueInfo);
//                    break;
//                case 5:
//                    setJoinPeople(joinPopup.getView(R.id.pup_5), joinPopup.getView(R.id.tv_5), roomQueueInfo);
//                    break;
//                case 6:
//                    setJoinPeople(joinPopup.getView(R.id.pup_6), joinPopup.getView(R.id.tv_6), roomQueueInfo);
//                    break;
//                case 7:
//                    setJoinPeople(joinPopup.getView(R.id.pup_7), joinPopup.getView(R.id.tv_7), roomQueueInfo);
//                    break;
//            }
//        }
//    }
//
//    private void setJoinPeople(LinearLayout ll, TextView textView, RoomQueueInfo roomQueueInfo){
//        ll.setOnClickListener(this);
//        ll.setVisibility(View.VISIBLE);
//        ll.setTag(roomQueueInfo);
//        if(roomQueueInfo.mChatRoomMember == null)return;
//        textView.setText(roomQueueInfo.mChatRoomMember.getNick());
//    }
//
//    // 初始化时间选择pop
//    private void initTimePop(){
//        timePopup = new EasyPopup(getContext())
//                .setContentView(R.layout.dialog_cpk_sel_time)
//                .setWidth(DisplayUtils.dip2px(getContext(),225))
//                .setFocusAndOutsideEnable(true) // 是否允许点击PopupWindow之外的地方消失
//                .createPopup();
//
//        timePopup.getView(R.id.ll_time_1).setOnClickListener(v -> setJoinTime(1, "1分钟"));
//        timePopup.getView(R.id.ll_time_2).setOnClickListener(v -> setJoinTime(3, "3分钟"));
//        timePopup.getView(R.id.ll_time_3).setOnClickListener(v -> setJoinTime(5, "5分钟"));
//        timePopup.getView(R.id.ll_time_4).setOnClickListener(v -> setJoinTime(10, "10分钟"));
//    }
//
//    private void setJoinTime(int time, String timeStr){
//        timePopup.dismiss();
//        tvJoinTime.setTextColor(getContext().getResources().getColor(R.color.color_383950));
//        tvJoinTime.setText(timeStr);
//        this.time = time;
//        refreshConfirmStatus();
//    }
//
//    private void selectPos(RoomQueueInfo roomQueueInfo) {
//        if (roomQueueInfo == null)return;
//        for (RoomQueueInfo info : selectJoinIns){
//            if(info.mRoomMicInfo.getPosition() == roomQueueInfo.mRoomMicInfo.getPosition())
//                return;
//        }
//        selectJoinIns.add(roomQueueInfo);
//        cPKAdapter.setNewData(selectJoinIns);
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.pup_0:
//            case R.id.pup_1:
//            case R.id.pup_2:
//            case R.id.pup_3:
//            case R.id.pup_4:
//            case R.id.pup_5:
//            case R.id.pup_6:
//            case R.id.pup_7:
//                joinPopup.dismiss();
//                RoomQueueInfo roomQueueInfo = (RoomQueueInfo)v.getTag();
//                selectPos(roomQueueInfo);
//                refreshConfirmStatus();
//                break;
//            default:
//        }
//    }
//
//    private void refreshConfirmStatus(){
//        List<RoomQueueInfo> data = cPKAdapter.getData();
//        if(ListUtils.isListEmpty(data) || time == 0){
//            tvConfirm.setBackgroundResource(R.mipmap.ic_cpk_confirm_disable);
//            tvConfirm.setClickable(false);
//        } else {
//            tvConfirm.setBackgroundResource(R.mipmap.ic_cpk_confirm_enable);
//            tvConfirm.setClickable(true);
//        }
//    }
//
//    private void confirm(){
//        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
//        List<RoomQueueInfo> data = cPKAdapter.getData();
//        if(roomInfo == null || ListUtils.isListEmpty(data) || time <=0 ){
//            return;
//        }
//
//        StringBuilder uids = new StringBuilder();
//        for (RoomQueueInfo roomQueueInfo : data){
//            if(roomQueueInfo.mChatRoomMember != null && roomQueueInfo.mChatRoomMember.getAccount() != null){
//                uids.append(roomQueueInfo.mChatRoomMember.getAccount());
//                uids.append(",");
//            }
//        }
//        uids.deleteCharAt(uids.length() - 1);
//
//        roomModel.confirmCharmPK(time*30, CoreManager.getCore(IAuthCore.class).getCurrentUid(), roomInfo.getRoomId(), uids.toString(), new HttpRequestCallBack<String>() {
//            @Override
//            public void onSuccess(String message, String response) {
//                LogUtil.i("confirmCharmPK : "+response);
//            }
//
//            @Override
//            public void onFailure(int code, String msg) {
//                SingleToastUtil.showToast(msg);
//            }
//        });
//
//        dismiss();
//    }
//
//    public class CPKAdapter extends BaseQuickAdapter<RoomQueueInfo, BaseViewHolder> {
//
//        CPKAdapter() {
//            super(R.layout.list_item_cpk_sel);
//        }
//
//        @Override
//        protected void convert(BaseViewHolder helper, RoomQueueInfo info) {
//            if(info == null || info.mChatRoomMember == null || info.getMRoomMicInfo() == null){
//                return;
//            }
//            IMChatRoomMember member = info.mChatRoomMember;
//            String nick = member.getNick();
//
//            TextView textView = helper.getView(R.id.tv_nick);
//            ImageView ivCancel = helper.getView(R.id.iv_cancel);
//            ivCancel.setOnClickListener(v -> {
//                for (int i=0; selectJoinIns != null && i<selectJoinIns.size(); i++){
//                    RoomQueueInfo info1 = selectJoinIns.get(i);
//                    if(info1 != null && info1.mChatRoomMember != null && info1.getMRoomMicInfo() != null){
//                        if(info1.getMRoomMicInfo().getPosition() == info.getMRoomMicInfo().getPosition()){
//                            selectJoinIns.remove(i);
//                        }
//                    }
//                }
//                cPKAdapter.setNewData(selectJoinIns);
//                refreshConfirmStatus();
//            });
//
//            if(nick != null){
//                if(nick.length() > 5){
//                    nick = nick.substring(0, 5).concat("...");
//                }
//                textView.setText(nick);
//            }
//        }
//    }
//}
