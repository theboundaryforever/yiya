package com.yiya.mobile.room.presenter;

import com.yiya.mobile.presenter.BaseMvpPresenter;
import com.yiya.mobile.room.model.RoomModel;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * 房间框架的presenter
 *
 * @author zeda
 */
public class RoomFramePresenter extends BaseMvpPresenter<IRoomView> {

    //除RoomModel外，AvRoomDataManager、RtcEngineManager也属于model
    protected RoomModel model;

    public RoomFramePresenter() {
        model = new RoomModel();
    }


    /**
     * 退出房间
     *
     * @param callBack 退出结果回调
     */
    public void exitRoom(CallBack<String> callBack) {
        model.exitRoom(callBack);
    }

    /**
     * 获取当前房间的房间信息
     */
    public RoomInfo getCurrRoomInfo() {
        return model.getCurrentRoomInfo();
    }

    /**
     * 我自己是否是房主
     */
    public boolean isRoomOwnerByMyself() {
        return AvRoomDataManager.get().isRoomOwner();
    }


    /**
     * 刷新房主的用户信息
     */
    public void refreshRoomOwnerUserInfo() {
        RoomInfo roomInfo = getCurrRoomInfo();
        if (roomInfo != null) {
            CoreManager.getCore(IUserCore.class).requestUserInfo(roomInfo.getUid(), null);
        }
    }
}
