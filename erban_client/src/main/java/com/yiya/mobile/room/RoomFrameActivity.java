package com.yiya.mobile.room;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.room.avroom.activity.LivePrepareFragment;
import com.yiya.mobile.room.avroom.activity.LiveRoomOwnerFinishFragment;
import com.yiya.mobile.room.avroom.activity.RoomFinishedActivity;
import com.yiya.mobile.room.avroom.fragment.BaseRoomDetailFragment;
import com.yiya.mobile.room.avroom.fragment.InputPwdDialogFragment;
import com.yiya.mobile.room.avroom.fragment.VideoRoomMasterDetailFragment;
import com.yiya.mobile.room.avroom.fragment.VideoRoomSlaveDetailFragment;
import com.yiya.mobile.room.avroom.fragment.YiYaAudioRoomDetailFragment;
import com.yiya.mobile.room.avroom.other.RoomFrameHandle;
import com.yiya.mobile.room.presenter.IRoomView;
import com.yiya.mobile.room.presenter.RoomFramePresenter;
import com.yiya.mobile.ui.home.HomeGuideDialog;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.ArrayList;

/**
 * 房间框架
 */
@CreatePresenter(RoomFramePresenter.class)
public class RoomFrameActivity extends BaseMvpActivity<IRoomView, RoomFramePresenter> implements IRoomView {

    //TYPE_ROOM_SHOW：显示房间页面，TYPE_ROOM_FINISH：显示房间结束页面，TYPE_PWD_SHOW：显示密码输入页面，TYPE_GUIDE_SHOW：显示认证引导页面，TYPE_LIVE_PREPARE：显示视频预览页面
    public final static int TYPE_ROOM_SHOW = 1, TYPE_ROOM_FINISH = 2, TYPE_PWD_SHOW = 3, TYPE_GUIDE_SHOW = 4, TYPE_LIVE_PREPARE = 5;

    protected long roomUid;
    protected int roomType;
    private int viewType;

    public BaseRoomDetailFragment roomFragment;
    private LivePrepareFragment livePrepareFragment;
    private InputPwdDialogFragment mPwdDialogFragment;

    /**
     * Use {@link RoomFrameHandle#start(Context, long, int)} or {@link RoomFrameActivity#start(Context, long, int, int)}
     */
    @Deprecated
    public static void start(Context activityContext, long roomUid, int roomType) {
        //兼容旧写法
        if (AvRoomDataManager.get().isFirstEnterRoomOrChangeOtherRoom(roomUid, roomType)) {
            //未进入房间，先走进入房间流程
            RoomFrameHandle.start(activityContext, roomUid, roomType);
        } else {
            start(activityContext, roomUid, roomType, TYPE_ROOM_SHOW);
        }
    }

    /**
     * @param viewType 显示的页面类型 TYPE_ROOM_SHOW = 1, TYPE_ROOM_FINISH = 2, TYPE_PWD_SHOW = 3, TYPE_GUIDE_SHOW = 4;
     */
    public static void start(Context activityContext, long roomUid, int roomType, int viewType) {
        Intent intent = new Intent(activityContext, RoomFrameActivity.class);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        intent.putExtra(Constants.ROOM_TYPE, roomType);
        intent.putExtra(Constants.ROOM_VIEW_TYPE, viewType);
        activityContext.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        //保持房间页面不息屏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setSwipeBackEnable(false);

        //注册房间事件监听
        IMNetEaseManager.get().subscribeChatRoomEventObservable(this::onRoomEventReceive, this);

        roomUid = getIntent().getLongExtra(Constants.ROOM_UID, 0);
        roomType = getIntent().getIntExtra(Constants.ROOM_TYPE, 0);
        viewType = getIntent().getIntExtra(Constants.ROOM_VIEW_TYPE, 0);

        showView();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (roomFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(roomFragment).commitAllowingStateLoss();
            roomFragment = null;
        }
        if (livePrepareFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(livePrepareFragment).commitAllowingStateLoss();
            livePrepareFragment = null;
        }
        if (mPwdDialogFragment != null) {
            mPwdDialogFragment.dismiss();
            mPwdDialogFragment = null;
        }

        roomUid = intent.getLongExtra(Constants.ROOM_UID, 0);
        roomType = intent.getIntExtra(Constants.ROOM_TYPE, 0);
        viewType = intent.getIntExtra(Constants.ROOM_VIEW_TYPE, 0);

        showView();
    }

    protected void onRoomEventReceive(RoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
            return;
        }
        int event = roomEvent.getEvent();
        switch (event) {
            case RoomEvent.ROOM_EXIT:
            case RoomEvent.ENTER_ROOM_FAIL:
                //退房后或者进房失败时，如果此页面存在，那么finish
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_container) instanceof LiveRoomOwnerFinishFragment)) {
                    finish();
                }
                break;
            case RoomEvent.RTC_ENGINE_NETWORK_BAD:
                toast("当前网络不稳定，请检查网络");
                break;
            case RoomEvent.RTC_ENGINE_NETWORK_CLOSE:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                break;
            case RoomEvent.PLAY_OR_PUBLISH_NETWORK_ERROR:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                break;
            case RoomEvent.ZEGO_AUDIO_DEVICE_ERROR:
                toast("当前麦克风可能被占用，请检查设备是否可用");
                break;
            default:
                break;
        }
    }

    private void showView() {
        switch (viewType) {
            case TYPE_ROOM_SHOW:
                //展示对应房间页面
                showRoomDetailFragment();
                break;
            case TYPE_ROOM_FINISH:
                //展示房间结束页面
                showRoomFinishedView();
                break;
            case TYPE_PWD_SHOW:
                //展示密码输入页面
                showRoomPwdView();
                break;
            case TYPE_GUIDE_SHOW:
                //展示认证引导页面
                showRoomGuideView();
                break;
            case TYPE_LIVE_PREPARE:
                //展示视频预览页面
                showRoomLivePrepareFragment();
                break;
        }
    }

    /**
     * 显示房间页面
     */
    public void showRoomDetailFragment() {
        RoomInfo roomInfo = getMvpPresenter().getCurrRoomInfo();
        if (roomInfo != null) {
            getMvpPresenter().refreshRoomOwnerUserInfo();//刷新一下房主信息
            if (roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                this.roomFragment = YiYaAudioRoomDetailFragment.newInstance(roomUid);
            } else if (roomInfo.getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE) {
                if (getMvpPresenter().isRoomOwnerByMyself()) {
                    this.roomFragment = VideoRoomMasterDetailFragment.newInstance(roomUid);
                } else {
                    this.roomFragment = VideoRoomSlaveDetailFragment.newInstance(roomUid);
                }
            }
            if (livePrepareFragment != null) {
                livePrepareFragment = null;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, roomFragment).commitAllowingStateLoss();
        } else {
            toast("进房失败 roomInfo is null");
            LogUtil.w(AvRoomDataManager.TAG, "进房失败 roomInfo is null");
            finish();
        }
    }

    /**
     * 显示房间直播结束
     */
    private void showRoomFinishedView() {
        RoomFinishedActivity.start(this, roomUid, roomType);
        finish();
    }

    /**
     * 显示房间密码弹框
     */
    private void showRoomPwdView() {
        if (mPwdDialogFragment != null && mPwdDialogFragment.getDialog() != null && mPwdDialogFragment.getDialog().isShowing()) {
            mPwdDialogFragment.refreshFailText();
            return;
        }
        mPwdDialogFragment = InputPwdDialogFragment.newInstance(getString(R.string.input_pwd), getString(R.string.ok), getString(R.string.cancel));
        try {
            mPwdDialogFragment.show(getSupportFragmentManager(), InputPwdDialogFragment.class.getSimpleName());
            mPwdDialogFragment.setOnDialogBtnClickListener(new InputPwdDialogFragment.OnDialogBtnClickListener() {
                @Override
                public void onBtnConfirm(String pwd) {
                    RoomFrameHandle.start(RoomFrameActivity.this, roomUid, roomType, pwd);
                }

                @Override
                public void onBtnCancel() {
                    mPwdDialogFragment.dismiss();
                    finish();
                }
            });
        } catch (Exception e) {
            //google的bug show有可能exception
            toast("密码输入框弹出异常");
            LogUtil.w(AvRoomDataManager.TAG, String.format("密码输入框弹出异常：%s", e.getMessage()));
            finish();
        }
    }

    /**
     * 显示开房间认证引导dialog
     */
    private void showRoomGuideView() {
        HomeGuideDialog dialog = HomeGuideDialog.newInstance(roomType == RoomInfo.ROOMTYPE_VIDEO_LIVE);
        dialog.setOKCancelDialogListener(new HomeGuideDialog.OKCancelDialogListener() {
            @Override
            public void onOk() {
                if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    //语音房跳去实名认证
                    CommonWebViewActivity.start(RoomFrameActivity.this, BaseUrl.REAL_NAME_URL);
                }
                finish();
            }

            @Override
            public void onCancel() {
                finish();
            }
        });
        dialog.show(getSupportFragmentManager(), HomeGuideDialog.class.getSimpleName());
    }

    /**
     * 显示房间开播预览页面
     */
    private void showRoomLivePrepareFragment() {
        livePrepareFragment = LivePrepareFragment.newInstance();
        livePrepareFragment.setOnLivePrepareListener((coverUrl, title, isCanConnectMic) -> {
            //预览结束，进入房间开播
            RoomFrameHandle.createEnterStart(RoomFrameActivity.this, roomUid, roomType, coverUrl, title, isCanConnectMic);
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, livePrepareFragment).commitAllowingStateLoss();
    }

    /**
     * 显示房主结束房间fragment
     */
    public void showOwnerRoomFinishFragment(long roomId, long roomUid) {
        Fragment fragment = LiveRoomOwnerFinishFragment.newInstance(roomId, roomUid);
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, fragment).commitAllowingStateLoss();
    }

    @Override
    public void dismissDialog() {
        super.dismissDialog();
        if (mPwdDialogFragment != null) {
            mPwdDialogFragment.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        if (roomFragment != null) {
            boolean isIntercept = roomFragment.onBack();
            if (isIntercept) {
                return;
            }
        }
        if (RoomInfo.ROOMTYPE_VIDEO_LIVE == roomType) {
            getMvpPresenter().exitRoom(null);
            finish();//视频房没最小化
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        roomFragment = null;
        livePrepareFragment = null;
        super.onDestroy();
    }

    @Override
    public boolean blackStatusBar() {
        //设置状态栏文本为白色
        return false;
    }


    /**
     * 参考：https://blog.csdn.net/androidxiaojiang/article/details/52870781
     */
    public interface MyTouchListener {
        public void onTouchEvent(MotionEvent event);
    }

    // 保存MyTouchListener接口的列表
    private ArrayList<MyTouchListener> myTouchListeners = new ArrayList<RoomFrameActivity.MyTouchListener>();

    /**
     * 提供给Fragment通过getActivity()方法来注册自己的触摸事件的方法
     *
     * @param listener 回调
     */
    public void registerMyTouchListener(MyTouchListener listener) {
        myTouchListeners.add(listener);
    }

    /**
     * 提供给Fragment通过getActivity()方法来取消注册自己的触摸事件的方法
     *
     * @param listener 回调
     */
    public void unRegisterMyTouchListener(MyTouchListener listener) {
        myTouchListeners.remove(listener);
    }

    /**
     * 分发触摸事件给所有注册了MyTouchListener的接口
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        for (MyTouchListener listener : myTouchListeners) {
            listener.onTouchEvent(ev);
        }
        return super.dispatchTouchEvent(ev);
    }
}

