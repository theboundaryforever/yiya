package com.yiya.mobile.room.presenter;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/6/14.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface IMicroApplyListView extends IMvpBaseView {

    /**
     * 设置连麦开关成功
     *
     * @param isCanConnectMic
     */
    void setRoomConnectMicSuccess(boolean isCanConnectMic);


    /**
     * 显示连麦列表
     *
     * @param roomMicroApplyInfo
     */
    void showMicroApplyList(RoomMicroApplyInfo roomMicroApplyInfo);

}
