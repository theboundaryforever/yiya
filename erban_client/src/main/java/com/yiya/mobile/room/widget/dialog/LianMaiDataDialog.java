package com.yiya.mobile.room.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.juxiao.library_ui.widget.DrawableTextView;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.yiya.mobile.room.avroom.adapter.RoomLianMaiListAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author MadisonRong
 * @date 13/01/2018
 */

public class LianMaiDataDialog extends BaseDialogFragment implements BaseQuickAdapter.OnItemClickListener, RadioGroup.OnCheckedChangeListener,
        View.OnClickListener, BaseQuickAdapter.OnItemChildClickListener {

    @BindView(R.id.user_rank)
    RadioGroup userRank;
    @BindView(R.id.rb_tab_lian_mai)
    RadioButton rbTabLianMai;
    @BindView(R.id.rb_tab_enter_room)
    RadioButton rbTabEnterRoom;
    @BindView(R.id.rv_pay_income_list)
    RecyclerView rvPayIncomeList;
    @BindView(R.id.dt_cancel)
    DrawableTextView dtCancel;
    @BindView(R.id.dt_confirm)
    DrawableTextView dtConfirm;
    Unbinder unbinder;


    private int dataType = 1;
    private long roomId;
    private RoomLianMaiListAdapter roomConsumeListAdapter;
    private View noDataView;

    private int gender;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.gender = arguments.getInt("gender", 1);
        }
    }

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, null);
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();
    }

    /**
     * @param gender 1男，2女
     */
    public static LianMaiDataDialog newInstance(int gender) {
        LianMaiDataDialog listDataDialog = new LianMaiDataDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("gender", gender);
        listDataDialog.setArguments(bundle);
        return listDataDialog;
    }

    public LianMaiDataDialog() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_lian_mai_data, window.findViewById(android.R.id.content), false);
        unbinder = ButterKnife.bind(this, view);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        int width = ScreenUtil.getScreenWidth(getContext()) - ScreenUtil.dip2px(32);
        window.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        setCancelable(true);

        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo != null) {
            roomId = roomInfo.getRoomId();
        }
        noDataView = view.findViewById(R.id.tv_no_data);
        unbinder = ButterKnife.bind(this, view);
        initView();
        initRv();
        return view;
    }

    private void initRv() {
        rvPayIncomeList.setLayoutManager(new LinearLayoutManager(getContext()));
        roomConsumeListAdapter = new RoomLianMaiListAdapter(getContext());
        roomConsumeListAdapter.setOnItemChildClickListener(this);
        roomConsumeListAdapter.setOnItemClickListener(this);
        rvPayIncomeList.setAdapter(roomConsumeListAdapter);
        getData(1);
    }

    private void initView() {
        rbTabLianMai.setSelected(true);

        rbTabLianMai.setOnClickListener(v -> dataTypeChange(1));
        rbTabEnterRoom.setOnClickListener(v -> dataTypeChange(2));

        userRank.setOnCheckedChangeListener(this);
        dtConfirm.setOnClickListener(this);
        dtCancel.setOnClickListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        switch (checkedId) {
            case R.id.rb_tab_lian_mai:
                dataTypeChange(1);
                break;
            case R.id.rb_tab_enter_room:
                dataTypeChange(2);
                break;
            default:
                break;
        }
    }

    private void dataTypeChange(int i) {
        if (i == dataType) {
            return;
        }
        dataType = i;
        if (selectOptionAction != null) {
            selectOptionAction.optionClick();
        }
        getData(dataType);

        dataType = i;

        rbTabLianMai.setSelected(i == 1);
        rbTabEnterRoom.setSelected(i == 2);
    }


    private void getData(int itemType) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("gender", String.valueOf(gender == 1 ? 2 : 1));//这里是过滤性别，转过来
        param.put("roomid", String.valueOf(roomId));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        List<RoomConsumeInfo> list = roomConsumeListAdapter.getData();
        if (ListUtils.isListEmpty(list)) {
            return;
        }
        RoomConsumeInfo roomConsumeInfo = list.get(i);
        YiYaUserInfoDialog.newInstance(roomConsumeInfo.getUid())
                .show(getChildFragmentManager(), null);
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        if (roomConsumeListAdapter != null && ListUtils.isNotEmpty(roomConsumeListAdapter.getData())) {
            RoomConsumeInfo consumeInfo = roomConsumeListAdapter.getData().get(position);
            consumeInfo.setChecked(!consumeInfo.isChecked());
        }
    }

    private SelectOptionAction selectOptionAction;

    public void setSelectOptionAction(SelectOptionAction selectOptionAction) {
        this.selectOptionAction = selectOptionAction;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dt_cancel:
                if (selectOptionAction != null) {
                    selectOptionAction.onCancel();
                }
                dismiss();
                break;
            case R.id.dt_confirm:
                operatorList();
                break;
            default:
                break;
        }
    }

    private void operatorList() {
        List<RoomConsumeInfo> consumeInfos = checkSelect();
        if (ListUtils.isListEmpty(consumeInfos)) {
            SingleToastUtil.showToast("未选择任何用户哦！");
            return;
        }
        if (selectOptionAction != null) {
            selectOptionAction.onConfirm(consumeInfos, dataType);
        }
        dismiss();
    }

    /**
     * 检测 是否选择数据
     */
    private List<RoomConsumeInfo> checkSelect() {
        //清空数据
        initList().clear();
        //保存数据
        if (roomConsumeListAdapter != null && ListUtils.isNotEmpty(roomConsumeListAdapter.getData())) {
            for (RoomConsumeInfo consumeInfo : roomConsumeListAdapter.getData()) {
                if (consumeInfo.isChecked()) {
                    initList().add(consumeInfo);
                }
            }
        }
        return consumeInfos;
    }

    private List<RoomConsumeInfo> consumeInfos;

    private List<RoomConsumeInfo> initList() {
        if (consumeInfos == null) {
            consumeInfos = new ArrayList<>();
        }
        return consumeInfos;
    }

    public static abstract class SelectOptionAction {

        public void optionClick() {

        }

        public void onDataResponse() {

        }

        public abstract void onConfirm(List<RoomConsumeInfo> listConsume, int dataType);

        public void onCancel() {

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

