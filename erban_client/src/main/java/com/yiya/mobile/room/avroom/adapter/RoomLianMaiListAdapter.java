package com.yiya.mobile.room.avroom.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.view.LevelView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

/**
 * <p>房间消费adapter  </p>
 *
 * @author Administrator
 * @date 2017/11/20
 */

public class RoomLianMaiListAdapter extends BaseQuickAdapter<RoomConsumeInfo, BaseViewHolder> {

    private Drawable mManDrawable, mFemaleDrawable;
    private int itemType = 1;

    public RoomLianMaiListAdapter(Context context) {
        super(R.layout.list_item_room_lianmai);
        mManDrawable = context.getResources().getDrawable(R.mipmap.icon_mic_male);
        mFemaleDrawable = context.getResources().getDrawable(R.mipmap.icon_mic_female);
    }

    @Override
    protected void convert(BaseViewHolder holder, RoomConsumeInfo roomConsumeInfo) {
        if (roomConsumeInfo == null) {
            return;
        }
        holder.setText(R.id.tv_nick, roomConsumeInfo.getNick())
                .setText(R.id.tv_age, mContext.getString(R.string.lian_mai_item_age, roomConsumeInfo.getAge()))
                .setText(R.id.tv_height, mContext.getString(R.string.lian_mai_item_height, roomConsumeInfo.getHeight()))
                .setText(R.id.tv_income, mContext.getString(R.string.lian_mai_item_income, StringUtils
                        .isNotEmpty(roomConsumeInfo.getSalary()) ? roomConsumeInfo.getSalary() : "未知"))
                .setImageDrawable(R.id.gender, roomConsumeInfo.getGender() == 1 ? mManDrawable : mFemaleDrawable)
                .setChecked(R.id.ck_user, roomConsumeInfo.isChecked())
                .addOnClickListener(R.id.ck_user);
        ImageLoadUtils.loadAvatar(mContext, roomConsumeInfo.getAvatar(), holder.getView(R.id.avatar), true);
        //设置等级
        LevelView levelView = holder.getView(R.id.level_info_room_user_list);
        levelView.setCharmLevel(roomConsumeInfo.getCharmLevel());
        levelView.setExperLevel(roomConsumeInfo.getExperLevel());
        //设置显示图标
        if (itemType == 1) {
            ImageView ivCoinImg = holder.getView(R.id.iv_coin_img);
            if (roomConsumeInfo.getMicIcon() == 1) {
                if (ivCoinImg.getVisibility() == View.GONE) {
                    ivCoinImg.setVisibility(View.VISIBLE);
                }
                holder.setImageResource(R.id.iv_coin_img, R.mipmap.ic_lianmai_item_gold);
            } else if(roomConsumeInfo.getMicIcon() == 0 && roomConsumeInfo.getGlodIcon() == 1) {
                if (ivCoinImg.getVisibility() == View.GONE) {
                    ivCoinImg.setVisibility(View.VISIBLE);
                }
                holder.setImageResource(R.id.iv_coin_img, R.mipmap.ic_lianmai_item_note);
            } else if(roomConsumeInfo.getMicIcon() == 0 && roomConsumeInfo.getGlodIcon() == 0) {
                if (ivCoinImg.getVisibility() == View.VISIBLE) {
                    ivCoinImg.setVisibility(View.GONE);
                }
            }
        } else {
            holder.setGone(R.id.iv_coin_img, false);
        }
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}