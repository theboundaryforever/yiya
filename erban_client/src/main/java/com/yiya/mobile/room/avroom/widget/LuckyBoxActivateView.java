package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ProjectName:
 * Description:宝箱激活弹窗
 * Created by BlackWhirlwind on 2019/7/10.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class LuckyBoxActivateView extends ConstraintLayout {

    @BindView(R.id.tv_tips)
    TextView tvTips;

    public LuckyBoxActivateView(Context context) {
        this(context, null);
    }

    public LuckyBoxActivateView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LuckyBoxActivateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.layout_lucky_box_activate, this);
        ButterKnife.bind(this);
    }

    public void setTips(int stringId) {
        tvTips.setText("太空探险已开始\n" + getResources().getString(stringId));
    }
}
