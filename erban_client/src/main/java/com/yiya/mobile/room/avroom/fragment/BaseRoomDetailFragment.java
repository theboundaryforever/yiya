package com.yiya.mobile.room.avroom.fragment;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.juxiao.library_utils.log.LogUtil;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGADynamicEntity;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomMemberComeInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.bean.UserExperienceUpdateInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.im.custom.bean.BurstGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LuckyGiftAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.ReUsedSocketManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.BannerEvent;
import com.tongdaxing.xchat_core.room.bean.HeadLineStarLimit;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyBoxAddGold;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyBoxInfo;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyTips;
import com.tongdaxing.xchat_core.room.bean.RoomPlantCucumberInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.ChatUtil;
import com.tongdaxing.xchat_framework.util.util.DESUtils;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.room.avroom.adapter.BannerEventAdapter;
import com.yiya.mobile.room.avroom.other.BgTypeHelper;
import com.yiya.mobile.room.avroom.other.BottomViewListenerWrapper;
import com.yiya.mobile.room.avroom.other.OnMicroItemClickListener;
import com.yiya.mobile.room.avroom.widget.BarrageView;
import com.yiya.mobile.room.avroom.widget.BaseBottomView;
import com.yiya.mobile.room.avroom.widget.BaseMicroView;
import com.yiya.mobile.room.avroom.widget.ComingMsgView;
import com.yiya.mobile.room.avroom.widget.GiftEffectView;
import com.yiya.mobile.room.avroom.widget.GiftMsgView;
import com.yiya.mobile.room.avroom.widget.GiftView;
import com.yiya.mobile.room.avroom.widget.HeadLineStarNoticeView;
import com.yiya.mobile.room.avroom.widget.InputMsgView;
import com.yiya.mobile.room.avroom.widget.LuckyBoxView;
import com.yiya.mobile.room.avroom.widget.LuckyGiftView;
import com.yiya.mobile.room.avroom.widget.MessageView;
import com.yiya.mobile.room.avroom.widget.ScreenNoticeView;
import com.yiya.mobile.room.egg.PlantBeanDialog;
import com.yiya.mobile.room.face.DynamicFaceDialog;
import com.yiya.mobile.room.gift.GiftSelector;
import com.yiya.mobile.room.gift.LackOfCandyDialog;
import com.yiya.mobile.room.pigfight.PigFightDialog;
import com.yiya.mobile.room.presenter.BaseRoomDetailPresenter;
import com.yiya.mobile.room.presenter.IRoomDetailView;
import com.yiya.mobile.room.widget.dialog.RewardGiftDialog;
import com.yiya.mobile.room.widget.dialog.RoomLuckyBoxRewardDialog;
import com.yiya.mobile.room.widget.dialog.YiYaUserInfoDialog;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.home.HomeGuideDialog;
import com.yiya.mobile.ui.home.adpater.BannerAdapter;
import com.yiya.mobile.ui.me.wallet.activity.MyWalletActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.ui.web.LuckyBoxWebViewActivity;
import com.yiya.mobile.ui.widget.Banner;
import com.yiya.mobile.ui.widget.dialog.ShareDialog;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.SessionHelper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_TYPE_BURST_GIFT;

/**
 * 房间fragment基类
 *
 * @author zeda
 */
public abstract class BaseRoomDetailFragment<V extends IRoomDetailView, P extends BaseRoomDetailPresenter<V>> extends BaseMvpFragment<V, P> implements IRoomDetailView {

    protected Disposable mDisposable;

    protected abstract @NonNull
    BaseMicroView getMicroView();

    protected abstract @NonNull
    MessageView getMessageView();

    protected abstract @NonNull
    GiftView getGiftView();

    protected abstract @NonNull
    BaseBottomView getBottomView();

    protected abstract @NonNull
    TextView getTitleView();

    protected abstract @Nullable
    GiftMsgView getGiftMsgView();

    protected abstract @NonNull
    ComingMsgView getComingMsgView();

    protected abstract @NonNull
    ImageView getRoomBgView();

    protected abstract @NonNull
    GiftSelector getGiftSelector();

    protected @NonNull
    TextView getRoomUid() {
        return null;
    }

    protected abstract @NonNull
    InputMsgView getInputMsgView();

    protected abstract @NonNull
    View getMoreOperateBtn();

    protected abstract @NonNull
    ImageView getLotteryBoxView();

    protected abstract @NonNull
    Banner getBannerView();

    protected abstract @Nullable
    LuckyGiftView getLuckyGiftView();

    protected abstract SVGAImageView getLuckyTipsSVGAIv();

    protected abstract @NonNull
    SVGAImageView getLuckyBoxActivateView();

    protected abstract @NonNull
    ScreenNoticeView getLuckyGiftNoticeTips();

    protected abstract @NonNull
    ScreenNoticeView getAddCoinNoticeTips();

    protected abstract @NonNull
    HeadLineStarNoticeView getHeadLineStarLimitNoticeTips();

    protected abstract int getLuckyBoxActivateTips();

    /**
     * 超级宝箱、种瓜轮播banner
     *
     * @return
     */
    protected abstract @NonNull
    Banner getBannerEventView();

    //底部消息点击
    protected abstract void onBottomMsgClick();

    /**
     * 接收房间事件
     */
    public void receiveRoomEvent(RoomEvent roomEvent) {
        if (roomEvent == null) {
            return;
        }
        int event = roomEvent.getEvent();
        switch (event) {
            case RoomEvent.ROOM_MANAGER_ADD://添加管理员权限
            case RoomEvent.ROOM_MANAGER_REMOVE://移除管理员权限
                if (getMvpPresenter() != null && getMvpPresenter().isMyself(roomEvent.getAccount())) {
                    if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_ADD) {
                        toast(R.string.set_room_manager);
                    } else if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_REMOVE) {
                        toast(R.string.remove_room_manager);
                    }
                    refreshMicroView(BaseMicroView.MIC_POSITION_ALL);
                    refreshBottomView();
                }
                break;
            case RoomEvent.ROOM_RECONNECT://IM重连
                refreshMicroView(BaseMicroView.MIC_POSITION_ALL);
                getMvpPresenter().checkAndAutoUpMic();
                refreshMicBtnStatus();
            case RoomEvent.ROOM_INFO_UPDATE:
                if (roomEvent.getRoomInfo() != null) {
                    AvRoomDataManager.get().setRoomInfo(roomEvent.getRoomInfo());
                }
                refreshOnlineCount();
                refreshView();
                break;
            case RoomEvent.RECEIVE_LUCKY_GIFT:
                refreshGiftSelector();
                showLuckyGift(roomEvent.getChatRoomMessage());
                break;
            case RoomEvent.RECEIVE_LUCKY_GIFT_ALL:
                showLuckyGiftAll(roomEvent.getChatRoomMessage());
                break;
            case RoomEvent.MIC_QUEUE_STATE_CHANGE:
                refreshMicroView(roomEvent.getMicPosition());
                refreshMicBtnStatus();
                break;
            case RoomEvent.KICK_DOWN_MIC://
                toast(mContext.getResources().getString(R.string.kick_mic));
                break;
            case RoomEvent.DOWN_MIC://下麦
            case RoomEvent.UP_MIC://上麦
                refreshBottomView();
                refreshMicroView(BaseMicroView.MIC_POSITION_ALL);
                break;
            case RoomEvent.INVITE_UP_MIC:
                getDialogManager().showOkBigTips(getString(R.string.tip_tips), getString(R.string.embrace_on_mic), true, null);
                getMvpPresenter().operateUpMicro(roomEvent.getMicPosition(), true);
                break;
//            case RoomEvent.RECEIVE_MSG:
//                ChatRoomMessage chatRoomMessage = roomEvent.getChatRoomMessage();
//                if (chatRoomMessage != null && IMReportRoute.sendMessageReport.equalsIgnoreCase(chatRoomMessage.getRoute())) {
//                    IMCustomAttachment attachment = chatRoomMessage.getAttachment();
//                    setupBurstGiftMsgAndNotify(attachment);
//                    if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
//                        setupBottomViewSendMsgBtn(true);
//                    } else if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
//                        setupBottomViewSendMsgBtn(false);
//                    }
//                }
//                break;
            case RoomEvent.RECEIVE_BAN_SEND_MSG:
                if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == Integer.parseInt(roomEvent.getAccount())) {
                    toast(roomEvent.isBanSendMsg() ? "你已被禁言" : "你已被取消禁言");
                }
                break;
            case RoomEvent.CODE_USER_EXP_UPDATE:// 用户等级更新
                UserExperienceUpdateInfo userExpInfo = roomEvent.getUserExperienceUpdateInfo();
                IMChatRoomMember mOwnerMember = getMvpPresenter().getMOwnerMember();
                mOwnerMember.setVideo_room_exper_level(userExpInfo.getVideoRoomLevelSeq());// 用户等级
                mOwnerMember.setVideo_room_exper_level_pic(userExpInfo.getVideoRoomLevelPic());// 用户等级图片
                mOwnerMember.setExperLevel(userExpInfo.getExperienceLevelSeq());// 财富等级
                mOwnerMember.setExper_level_pic(userExpInfo.getExperienceLevelPic());// 财富等级图片
                mOwnerMember.setCharmLevel(userExpInfo.getCharmLevelSeq());// 魅力等级
                mOwnerMember.setCharm_level_pic(userExpInfo.getCharmLevelPic());// 魅力等级图片

                getMvpPresenter().setMOwnerMember(mOwnerMember);
                CoreManager.getCore(IUserCore.class).updateCurrentUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                break;
            case RoomEvent.RTC_ENGINE_NETWORK_BAD:
                toast("当前网络不稳定，请检查网络");
                break;
            case RoomEvent.RTC_ENGINE_NETWORK_CLOSE:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                break;
            case RoomEvent.PLAY_OR_PUBLISH_NETWORK_ERROR:
                toast("当前网络异常，与服务器断开连接，请检查网络");
                break;
            case RoomEvent.ZEGO_AUDIO_DEVICE_ERROR:
                toast("当前麦克风可能被占用，请检查设备是否可用");
                break;
            case RoomEvent.ROOM_MEMBER_IN:
                dealUserComingMsg();
                break;
//            case RoomEvent.CODE_LUCKY_BOX_ACTIVATE:
//                //宝箱激活
//                showLuckyBoxActivateView();
//                break;
            case RoomEvent.CODE_BARRAGE_MESSAGE:
                showBarrageMessage(roomEvent);
                break;
//            case RoomEvent.CODE_LUCKY_TIPS:
//                //幸运提示
//                showLuckyTips(roomEvent);
//                //刷新超级宝箱，防止倒计时出问题
//                pollLuckyBoxDetail();
//                break;
            case RoomEvent.CODE_LUCKY_BOX_REWARD_BROADCASTER:
                //全服中奖主播提示
            case RoomEvent.CODE_LUCKY_BOX_REWARD_USER:
                //全服中奖用户提示
                showDialog(RoomLuckyBoxRewardDialog.newInstance(roomEvent.getRoomLuckyRewardInfo(), roomEvent.getEvent()));
                break;
            case RoomEvent.ROOM_PUB_DEL:
                setupBottomViewSendMsgBtn(roomEvent.getPublicChatSwitch() == 0);
                if (roomEvent.getPublicChatSwitch() == 1) {
                    getInputMsgView().hideInput();
                    getMessageView().clear();
                }
                break;
            case RoomEvent.RECEIVE_ADD_COIN:
                showAddCoin(roomEvent.getRoomLuckyBoxAddGold());
                break;
            case RoomEvent.HEAD_LINE_STAR_LIMIT:
                // 头条之星分数超过两万
                showHeadLineStarLimit(roomEvent.getHeadLineStarLimit());
                break;
//            case RoomEvent.ROOM_PLANT_CUCUMBER:
//                //种瓜金币池
//                showPlantCucumberGold(roomEvent.getRoomPlantCucumberInfo());
//                break;
            default:
                break;
        }
    }

    @Override
    public void initiate() {

        getMvpPresenter().saveMinimizeStatus(false);

        subscribeChatRoomEventObservable();
        subscribeChatRoomMsgObservable();

        getMvpPresenter().checkAndAutoUpMic();
        getMvpPresenter().refreshRoomManagerMember();// 刷新房间管理员

        refreshMicroView(BaseMicroView.MIC_POSITION_ALL);
        refreshMicBtnStatus();

        refreshView();

        getMvpPresenter().getActionDialogState(ActionDialogInfo.TYPE_NEW_GIFT);

//        getLuckyBoxDetail();


        CoreManager.getCore(IGiftCore.class).requestGiftInfos();// 更新礼物信息
    }

    /**
     * 头条之星分数超过两万
     */
    private void showHeadLineStarLimit(HeadLineStarLimit limit) {
        if (limit == null) return;
        String temp = "恭喜主播：";
        SpannableStringBuilder ssb = new SpannableStringBuilder(temp);
        setStrColor(ssb, 0, temp.length(), R.color.white);

        String nick = StringUtils.ellipsize(limit.getNick(), 4);
        ssb.append(nick);
        setStrColor(ssb, ssb.toString().length() - nick.length(), ssb.toString().length(), R.color.color_FFF39F);

        String tips0 = " 在本轮头条争霸中影响力超过 ";
        ssb.append(tips0);
        setStrColor(ssb, ssb.toString().length() - tips0.length(), ssb.toString().length(), R.color.white);

        String tips2 = String.valueOf(limit.getOutpaceNum());
        ssb.append(tips2);
        setStrColor(ssb, ssb.toString().length() - tips2.length(), ssb.toString().length(), R.color.color_FFF39F);

        getHeadLineStarLimitNoticeTips().setupView("svga_head_line_start_limit_notice.svga", "wenzi", ssb, limit.getAvatar(), "touxiang");
        getHeadLineStarLimitNoticeTips().setOnClickListener(v -> {
            RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
            if (roomInfo != null && (roomInfo.getUid() != limit.getUid())) {
                RoomFrameActivity.start(getActivity(), limit.getUid(), limit.getRoomType());
            }
        });
    }

    /**
     * 获取超级宝箱信息
     */
    private void getLuckyBoxDetail() {
        if (getMvpPresenter().isLuckyBoxEnabled()) {
            //后台是否开启了超级宝箱(默认0：开 1：关)
            //轮询超级宝箱累计金币
            pollLuckyBoxDetail();
        }
    }

    /**
     * 轮询超级宝箱累计金币
     */
    private void pollLuckyBoxDetail() {
        if (mDisposable != null && !mDisposable.isDisposed()) {
            mDisposable.dispose();
        }
        mDisposable = Observable.interval(Math.round(Math.random()), 10, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong ->
                        getMvpPresenter().getLuckyBoxDetail());
        addDisposable(mDisposable);
    }


    /**
     * 宝箱激活弹窗
     */
    private void showLuckyBoxActivateView() {
        SVGAParser parser = new SVGAParser(getContext());
        parser.decodeFromAssets("luckyboxactivate.svga", new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity videoItem) {
                TextPaint normalTextPaint = new TextPaint();
                normalTextPaint.setColor(Color.WHITE);
                normalTextPaint.setTextSize(26);
                normalTextPaint.setFakeBoldText(true);
                SVGADynamicEntity dynamicEntity = new SVGADynamicEntity();
                dynamicEntity.setDynamicText("太空探险已开始", normalTextPaint, "tihuan");
                dynamicEntity.setDynamicText(getResources().getString(getLuckyBoxActivateTips()), normalTextPaint, "tihuan2");
                SVGADrawable drawable = new SVGADrawable(videoItem, dynamicEntity);
                if (getLuckyBoxActivateView() != null) {
                    getLuckyBoxActivateView().setImageDrawable(drawable);
                    getLuckyBoxActivateView().setLoops(1);
                    getLuckyBoxActivateView().setClearsAfterStop(true);
                    getLuckyBoxActivateView().startAnimation();
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    /**
     * 幸运提示动画
     *
     * @param roomEvent 房间事件
     */
    private void showLuckyTips(RoomEvent roomEvent) {
        SVGAParser parser = new SVGAParser(mContext);
        parser.decodeFromAssets("luckyboxtips.svga", new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                RoomLuckyTips roomLuckyTips = roomEvent.getRoomLuckyTips();
                if (roomLuckyTips == null) {
                    return;
                }
                SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                SVGADynamicEntity dynamicEntity = drawable.getDynamicItem();

                TextPaint normalTextPaint = new TextPaint();
                normalTextPaint.setColor(Color.WHITE);
                normalTextPaint.setTextSize(20);

                String nick = StringUtils.ellipsize(roomLuckyTips.getNick(), 3);
                String golds = String.valueOf(roomLuckyTips.getGolds());
                String roomUserNick = StringUtils.ellipsize(roomLuckyTips.getRoomUserNick(), 3);
//                //恭喜 小鱼儿...成为超级幸运儿获得 6666金币，夏二芬...成为幸运主播
                String tips1 = "恭喜 ";
                String tips2 = tips1.concat(String.format("%s成为探险者独占", nick));
                String tips3 = tips2.concat(String.format("%s金币，%s成为探险主播", golds, roomUserNick));
                SpannableStringBuilder ssb = new SpannableStringBuilder(tips3);
                ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.color_FCFF05)), tips1.length(), tips1.length() + nick.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.color_FCFF05)), tips2.length(), tips2.length() + golds.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.color_FCFF05)), tips3.length() - 6 - roomUserNick.length(), tips3.length() - 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                dynamicEntity.setDynamicText(new StaticLayout(
                        ssb,
                        normalTextPaint,
                        Integer.MAX_VALUE,
                        Layout.Alignment.ALIGN_NORMAL,
                        1,
                        0,
                        false
                ), "img_283");
                //decode是异步的。有可能fragment结束掉才解析完导致npe
                if (getLuckyTipsSVGAIv() != null) {
                    getLuckyTipsSVGAIv().setImageDrawable(drawable);
                    getLuckyTipsSVGAIv().setLoops(1);
                    getLuckyTipsSVGAIv().setClearsAfterStop(true);
                    getLuckyTipsSVGAIv().startAnimation();
                }
            }

            @Override
            public void onError() {
            }
        });
    }

    public abstract OnMicroItemClickListener getOnMicroItemClickListener();

    public abstract void showRoomMoreOperateDialog();

    /**
     * 刷新在线人数显示
     */
    protected abstract void refreshOnlineCount();

    protected abstract void refreshAttention();

    private DynamicFaceDialog dynamicFaceDialog = null;

    /**
     * 防止外部新消息回调过来这边正在执行动画影响显示
     */
    private boolean isShowing;

    @Override
    public void onSetListener() {
        getMicroView().setOnMicroItemClickListener(getOnMicroItemClickListener());
        getInputMsgView().setOnInputClickListener(new InputMsgView.OnInputClickListener() {
            @Override
            public void onClick(boolean enable, String inputContent) {
                if (!enable) {
                    //发送消息
                    getMvpPresenter().sendTextMsg(inputContent);
                    getInputMsgView().setInputText("");
                } else {
                    getMvpPresenter().sendBarrage(inputContent);
                    getInputMsgView().hideKeyBoard();
                }

            }

            @Override
            public void onBarrageClick(boolean value) {

            }
        });
        getBottomView().setBottomViewCommonListener(bottomViewListenerWrapper);

        getMoreOperateBtn().setOnClickListener(v -> showRoomMoreOperateDialog());

        getLotteryBoxView().setOnClickListener(v -> {
            UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomOpenEgg());
            PlantBeanDialog.newInstance()
                    .show(getChildFragmentManager(), null);
        });

        getMessageView().getAdapter().setOnMsgContentClickListener(onMsgContentClickListener);

        getComingMsgView().setSvgaListener(svgUrl -> {
            try {
                final String carUrl = DESUtils.DESAndBase64Decrypt(svgUrl, DESUtils.giftCarSecret);//座驾
                getGiftView().drawSvgaEffect(carUrl, GiftEffectView.CAR);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void showBarrageMessage(RoomEvent roomEvent) {
        IMChatRoomMember roomMember = roomEvent.getChatRoomMember();
        if (roomMember != null) {
            BarrageView.BarrageInfo info = new BarrageView.BarrageInfo();
            info.setMessage(roomEvent.getBarrageMsg());
            info.setAvatar(roomMember.getAvatar());
            info.setName(roomMember.getNick());
            getGiftView().addBarrageInfo(info);
        }
    }

    /**
     * 18934239543 123456
     *
     * @param code
     * @param message
     * @param barrageNum 剩余免费弹幕数
     */
    @Override
    public void onBarrageCallback(int code, String message, int barrageNum) {
        switch (code) {
            case 200:
                getInputMsgView().setInputText("");
                getInputMsgView().setFreeBarrages(barrageNum);
                break;
            case 100702:
                showRechargeTipDialog();
                break;
            default:
                toast(message);
                break;
        }
    }

    /**
     * 显示幸运宝箱
     *
     * @param roomLuckyBoxInfo 幸运宝箱信息
     */
    @Override
    public void showLuckyBoxView(RoomLuckyBoxInfo roomLuckyBoxInfo) {
        if (roomLuckyBoxInfo != null) {
            BannerEventAdapter adapter = (BannerEventAdapter) getBannerEventView().getViewPager().getAdapter();
            if (adapter == null) {
                ArrayList<BannerEvent> bannerEvents = new ArrayList<>();
                BannerEvent bannerEvent = new BannerEvent(BannerEvent.TYPE_LUCKY_BOX);
                bannerEvent.setRoomLuckyBoxInfo(roomLuckyBoxInfo);
                bannerEvents.add(bannerEvent);
                adapter = new BannerEventAdapter(bannerEvents);
                setLuckyBoxCallback(adapter);
                getBannerEventView().setAdapter(adapter);
                getBannerEventView().setPlayDelay(3000);
            } else {
                boolean hasAdded = false;
                for (BannerEvent bannerEvent : adapter.getList()) {
                    if (bannerEvent.getItemType() == BannerEvent.TYPE_LUCKY_BOX) {
                        bannerEvent.setRoomLuckyBoxInfo(roomLuckyBoxInfo);
                        hasAdded = true;
                    }
                }
                if (!hasAdded) {
                    BannerEvent bannerEvent = new BannerEvent(BannerEvent.TYPE_LUCKY_BOX);
                    bannerEvent.setRoomLuckyBoxInfo(roomLuckyBoxInfo);
                    adapter.getList().add(bannerEvent);
                    setLuckyBoxCallback(adapter);
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void setLuckyBoxCallback(BannerEventAdapter adapter) {
        adapter.setOnLuckyBoxCallback(new LuckyBoxView.OnLuckyBoxViewClickListener() {
            @Override
            public void onLuckyBoxViewClick() {
                LuckyBoxWebViewActivity.start(getContext(), BaseUrl.LUCKY_BOX);
            }

            @Override
            public void refreshLuckyBox() {
                //刷新幸运宝箱并重新轮询
                pollLuckyBoxDetail();
            }
        });
    }

    /**
     * 种瓜金币池
     *
     * @param roomPlantCucumberInfo
     */
    private void showPlantCucumberGold(RoomPlantCucumberInfo roomPlantCucumberInfo) {
        if (!getMvpPresenter().isPlantCucumberEnabled()) {
            //后台是否开启房间种瓜
            return;
        }
        if (roomPlantCucumberInfo != null) {
            BannerEventAdapter adapter = (BannerEventAdapter) getBannerEventView().getViewPager().getAdapter();
            if (adapter == null) {
                ArrayList<BannerEvent> bannerEvents = new ArrayList<>();
                BannerEvent bannerEvent = new BannerEvent(BannerEvent.TYPE_PLANT_CUCUMBER);
                bannerEvent.setRoomPlantCucumberInfo(roomPlantCucumberInfo);
                bannerEvents.add(bannerEvent);
                adapter = new BannerEventAdapter(bannerEvents);
                setPlantCucumberCallback(adapter);
                getBannerEventView().setAdapter(adapter);
                getBannerEventView().setPlayDelay(3000);
            } else {
                boolean hasAdded = false;
                for (BannerEvent bannerEvent : adapter.getList()) {
                    if (bannerEvent.getItemType() == BannerEvent.TYPE_PLANT_CUCUMBER) {
                        bannerEvent.setRoomPlantCucumberInfo(roomPlantCucumberInfo);
                        hasAdded = true;
                    }
                }
                if (!hasAdded) {
                    BannerEvent bannerEvent = new BannerEvent(BannerEvent.TYPE_PLANT_CUCUMBER);
                    bannerEvent.setRoomPlantCucumberInfo(roomPlantCucumberInfo);
                    adapter.getList().add(bannerEvent);
                    setPlantCucumberCallback(adapter);
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void setPlantCucumberCallback(BannerEventAdapter adapter) {
        adapter.setOnPlantCucumberCallback(() -> {
            //跳转种瓜H5
            CommonWebViewActivity.start(getContext(), BaseUrl.PLANT_CUCUMBER);
        });
    }

    /**
     * 监听房间事件
     */
    public void subscribeChatRoomEventObservable() {
        IMNetEaseManager.get().subscribeChatRoomEventObservable(this::receiveRoomEvent, this);
    }


    //    /**
//     * 监听房间消息
//     */
    private void subscribeChatRoomMsgObservable() {
        IMNetEaseManager.get().subscribeChatRoomMsgFlowable(true, this::receiveRoomMsg, this);
    }

    /**
     * 接收房间消息
     */
    protected void receiveRoomMsg(List<ChatRoomMessage> messages) {
        if (messages.size() == 0) return;
//        for (int i = 0; i < messages.size(); i++) {
//            ChatRoomMessage chatRoomMessage = messages.get(i);
//            if (chatRoomMessage == null) return;
//            if (IMReportRoute.sendMessageReport.equalsIgnoreCase(chatRoomMessage.getRoute())) {
//                IMCustomAttachment attachment = chatRoomMessage.getAttachment();
//                if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
//                    setupBottomViewSendMsgBtn(true);
//                } else if (attachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
//                    setupBottomViewSendMsgBtn(false);
//                }
//            }
//        }
    }

    protected void refreshView() {
        RoomInfo currentRoomInfo = getMvpPresenter().getCurrRoomInfo();
        if (currentRoomInfo != null) {
            refreshTitleView(currentRoomInfo.getTitle());
            refreshBottomView();
            refreshLotteryBoxView(currentRoomInfo.getGiftDrawEnable());
        }
    }


    /**
     * 显示猪猪大作战
     */
    public void showPigFight() {
        PigFightDialog.newInstance()
                .show(getChildFragmentManager(), null);
    }

    /**
     * 显示幸运礼物
     *
     * @param chatRoomMessage 幸运礼物msg
     */
    private void showLuckyGift(ChatRoomMessage chatRoomMessage) {
        LuckyGiftAttachment luckyGiftAttachment = (LuckyGiftAttachment) chatRoomMessage.getAttachment();
        if (luckyGiftAttachment != null && luckyGiftAttachment.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid()) {
            getLuckyGiftView().setVisibility(View.VISIBLE);
            getLuckyGiftView().setupView(luckyGiftAttachment);
        }
    }

    /**
     * 显示全服幸运通告
     *
     * @param chatRoomMessage
     */
    private void showLuckyGiftAll(ChatRoomMessage chatRoomMessage) {

        LuckyGiftAttachment luckyGiftAttachment = (LuckyGiftAttachment) chatRoomMessage.getAttachment();
        if (luckyGiftAttachment != null && luckyGiftAttachment.getProportion() >= 100) {
            String temp = "返币通知：";
            SpannableStringBuilder ssb = new SpannableStringBuilder(temp);
            setStrColor(ssb, 0, temp.length(), R.color.white);

            String nick = StringUtils.ellipsize(luckyGiftAttachment.getNick(), 6);
            ssb.append(nick);
            setStrColor(ssb, ssb.toString().length() - nick.length(), ssb.toString().length(), R.color.color_FFF39F);

            String giftName = luckyGiftAttachment.getGiftName();
            String tips0 = "送" + giftName + "赢得 ";
            ssb.append(tips0);
            setStrColor(ssb, ssb.toString().length() - tips0.length(), ssb.toString().length(), R.color.white);

            String tips1 = String.valueOf((int) luckyGiftAttachment.getProportion());
            ssb.append(tips1);
            setStrColor(ssb, ssb.toString().length() - tips1.length(), ssb.toString().length(), R.color.color_FFF39F);

            String tips2 = " 倍返币, 获得 ";
            ssb.append(tips2);
            setStrColor(ssb, ssb.toString().length() - tips2.length(), ssb.toString().length(), R.color.white);

            String tips3 = String.valueOf((int) luckyGiftAttachment.getOutputGold());
            ssb.append(tips3);
            setStrColor(ssb, ssb.toString().length() - tips3.length(), ssb.toString().length(), R.color.color_FFF39F);

            String tips4 = "金币";
            ssb.append(tips4);
            setStrColor(ssb, ssb.toString().length() - tips4.length(), ssb.toString().length(), R.color.white);

            getLuckyGiftNoticeTips().setupView("luckygifttips.svga", "fanbi02", ssb);
            getLuckyGiftNoticeTips().setOnClickListener(v -> {
                RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
                if (roomInfo != null && (roomInfo.getUid() != luckyGiftAttachment.getRoomUid())) {
                    RoomFrameActivity.start(getContext(), luckyGiftAttachment.getRoomUid(), luckyGiftAttachment.getRoomType());
                }
            });
        }
    }

    /**
     * 显示添加金币通告
     *
     * @param roomLuckyBoxAddGold
     */
    private void showAddCoin(RoomLuckyBoxAddGold roomLuckyBoxAddGold) {
        if (roomLuckyBoxAddGold == null) return;

        String temp = "恭喜主播：";
        SpannableStringBuilder ssb = new SpannableStringBuilder(temp);
        setStrColor(ssb, 0, temp.length(), R.color.white);

        String nick = StringUtils.ellipsize(roomLuckyBoxAddGold.getRoomOwnerNick(), 6);
        ssb.append(nick);
        setStrColor(ssb, ssb.toString().length() - nick.length(), ssb.toString().length(), R.color.color_FFF39F);

        String tips0 = "给宝箱追加了 ";
        ssb.append(tips0);
        setStrColor(ssb, ssb.toString().length() - tips0.length(), ssb.toString().length(), R.color.white);

        String tips2 = String.valueOf(roomLuckyBoxAddGold.getGolds());
        ssb.append(tips2);
        setStrColor(ssb, ssb.toString().length() - tips2.length(), ssb.toString().length(), R.color.color_FFF39F);

        String tips3 = " 金币，点击前往围观！";
        ssb.append(tips3);
        setStrColor(ssb, ssb.toString().length() - tips3.length(), ssb.toString().length(), R.color.white);

        getAddCoinNoticeTips().setupView("addcointips.svga", "gongxi02", ssb);
        getAddCoinNoticeTips().setOnClickListener(v -> {
            RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
            if (roomInfo != null && (roomInfo.getUid() != roomLuckyBoxAddGold.getRoomUid())) {
                RoomFrameActivity.start(getActivity(), roomLuckyBoxAddGold.getRoomUid(), roomLuckyBoxAddGold.getRoomType());
            }
        });
    }

    protected void setStrColor(SpannableStringBuilder builder, int start, int end, int color) {
        ForegroundColorSpan redSpan = new ForegroundColorSpan(ContextCompat.getColor(mContext, color));
        builder.setSpan(redSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * 刷新砸蛋
     */
    public void refreshLotteryBoxView(int lotteryBoxSwitch) {
        if (lotteryBoxSwitch == 1) {
            getLotteryBoxView().setVisibility(View.VISIBLE);
        } else if (lotteryBoxSwitch == 2) {
            getLotteryBoxView().setVisibility(View.GONE);
        }
    }

    /**
     * 刷新房间标题
     */
    public void refreshTitleView(String title) {
        getTitleView().setText(title);
    }

    /**
     * 设置房间背景
     */
    protected void setupRoomBg(@NonNull RoomInfo roomInfo) {
        if (StringUtils.isNotEmpty(roomInfo.getBackPicUrl())) {
            ImageLoadUtils.loadImage(getContext(), roomInfo.getBackPicUrl(), getRoomBgView(), R.mipmap.bg_room_video_default, R.mipmap.bg_room_video_default);
        } else {
            int bgId = BgTypeHelper.getBgId(roomInfo.getBackPic());
            ImageLoadUtils.loadImageRes(getContext(), bgId, getRoomBgView(), BgTypeHelper.getDefaultBackRes(), new RequestOptions().format(DecodeFormat.PREFER_ARGB_8888));
        }
    }

    /**
     * 刷新麦位
     *
     * @param micPosition 麦位position 刷新所有麦位传MIC_POSITION_ALL
     */
    public void refreshMicroView(int micPosition) {
        getMicroView().refreshMicroView(micPosition);
    }

    /**
     * 刷新底部按钮
     */
    public void refreshBottomView() {
        if (getMvpPresenter().isOnMicByMyself()) {
            getBottomView().showUpMicBottom();

        } else {
            getBottomView().showDownMicBottom();
        }

        refreshMicBtnStatus();

        refreshRemoteMuteBtnStatus();
    }

    /**
     * 刷新静音按钮状态
     */
    public void refreshRemoteMuteBtnStatus() {
        getBottomView().setRemoteMuteOpen(!getMvpPresenter().isRemoteMuteByMyself());
    }

    /**
     * 刷新底部麦位按钮状态
     */
    @Override
    public void refreshMicBtnStatus() {
        if (getMvpPresenter().isAudienceRoleByMyself()) {
            getBottomView().setMicBtnEnable(false);
            getBottomView().setMicBtnOpen(false);
        } else {
            if (getMvpPresenter().isMicMuteByMyself()) {
                getBottomView().setMicBtnEnable(true);
                getBottomView().setMicBtnOpen(false);
            } else {
                getBottomView().setMicBtnEnable(true);
                getBottomView().setMicBtnOpen(true);
            }
        }
    }

    /**
     * 刷新礼物弹框
     */
    public void refreshGiftSelector() {
        if (getGiftSelector().isShowing()) {
            getGiftSelector().refreshGiftInfos();
            getGiftSelector().refreshGold();
        }
    }

    /**
     * 显示开房间认证引导dialog
     */
    @Override
    public void showRoomGuideDialog() {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm == null) return;
        imm.hideSoftInputFromWindow(getBottomView().getWindowToken(), 0);

        HomeGuideDialog dialog = HomeGuideDialog.newInstance(false);// 若这个为true 则跳去另外一种认证
        dialog.setOKCancelDialogListener(new HomeGuideDialog.OKCancelDialogListener() {
            @Override
            public void onOk() {
                //语音房跳去实名认证
                CommonWebViewActivity.start(getContext(), BaseUrl.REAL_NAME_URL);
            }

            @Override
            public void onCancel() {
//                finish();
            }
        });
        dialog.show(getFragmentManager(), HomeGuideDialog.class.getSimpleName());
    }

    /**
     * 显示充值提示弹框
     */
    @Override
    public void showRechargeTipDialog() {
        getDialogManager().dismissDialog();
        getDialogManager().showOkCancelDialog("余额不足，是否充值", true,
                new DialogManager.AbsOkDialogListener() {
                    @Override
                    public void onOk() {
                        MyWalletActivity.start(mContext,1);
                    }
                });
    }

    /**
     * 显示跳转任务中心dialog
     */
    @Override
    public void showLackOfCandyDialog() {
        LackOfCandyDialog.newInstance()
                .show(getChildFragmentManager(), null);
    }

    /**
     * 2: 新手礼包
     *
     * @param actionDialogInfoList 新手礼包队列
     */
    @Override
    public void onGetActionDialog(List<ActionDialogInfo> actionDialogInfoList) {
        if (actionDialogInfoList != null) {
            List<BannerInfo> bannerInfoList = new ArrayList<>();
            for (ActionDialogInfo info : actionDialogInfoList) {
                BannerInfo bannerInfo = new BannerInfo();
                bannerInfo.setBannerName(info.getActName());
                bannerInfo.setSkipType(info.getSkipType());
                bannerInfo.setSkipUri(info.getSkipUrl());
                bannerInfo.setBannerPic(info.getAlertWinPic());
                bannerInfoList.add(bannerInfo);
            }
            refreshNewGiftView(bannerInfoList);
        }
    }

    @Override
    public void onGetActionDialogError(String error) {
        refreshNewGiftView(null);
    }


    private void refreshNewGiftView(@Nullable List<BannerInfo> list) {
        if (list == null || list.size() == 0) {
            getBannerView().setVisibility(View.GONE);
        } else {
            BannerAdapter bannerAdapter = new BannerAdapter(list, mContext);
            getBannerView().setAdapter(bannerAdapter);
            getBannerView().setVisibility(View.VISIBLE);
            getBannerView().setPlayDelay(3000);
        }

    }

    /**
     * 配置底部公屏输入框的发送按钮
     */
    private void setupBottomViewSendMsgBtn(boolean enable) {
        getBottomView().setInputMsgBtnEnable(enable);
    }

    /**
     * 处理谁来了的逻辑
     */
    protected void dealUserComingMsg() {
        if (isShowing) {
            return;
        }

        RoomMemberComeInfo comeInfo = getMvpPresenter().getAndRemoveFirstMemberComeInfo();
        int roomType = getMvpPresenter().getCurrRoomInfo().getType();

        // 显示座驾动画
        if (comeInfo != null) {
            this.isShowing = true;
            getComingMsgView().setupView(comeInfo, roomType);
        }

        //大于等于10级，显示用户进来的弹幕
        if (comeInfo != null &&
                ((comeInfo.getExperLevel() >= 10 && roomType == RoomInfo.ROOMTYPE_HOME_PARTY) || (comeInfo.getVideoRoomExperLevel() >= 10 && roomType == RoomInfo.ROOMTYPE_VIDEO_LIVE))) {
            // 显示座驾动画
            this.isShowing = true;

            //展示动画
            getComingMsgView().startAnimation(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    getComingMsgView().setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    getComingMsgView().setVisibility(View.INVISIBLE);
                    isShowing = false;
                    //队列中还有其他弹幕未处理完
                    if (getMvpPresenter().getMemberComeSize() > 0) {
                        //继续处理
                        dealUserComingMsg();
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        } else {
            this.isShowing = false;
        }
    }


    /**
     * 配置爆出礼物显示 并 通知数量刷新
     *
     * @param imCustomAttachment 消息
     */
    private void setupBurstGiftMsgAndNotify(IMCustomAttachment imCustomAttachment) {
        if (imCustomAttachment.getFirst() == CUSTOM_MSG_TYPE_BURST_GIFT && imCustomAttachment instanceof BurstGiftAttachment) {
            BurstGiftAttachment attachment = (BurstGiftAttachment) imCustomAttachment;
            if (attachment.getUid() == getMvpPresenter().getCurrentUserId()) {
                RewardGiftDialog reward = RewardGiftDialog.newInstance(attachment.getGiftName(), attachment.getGiftNum(), attachment.getPicUrl());
                reward.show(getChildFragmentManager(), "gift_reward");
                GiftInfo giftInfo = getMvpPresenter().getLocalGiftInfoById(attachment.getGiftId());
                if (giftInfo != null) {
                    giftInfo.setUserGiftPurseNum(giftInfo.getUserGiftPurseNum() + attachment.getGiftNum());
                }
                refreshGiftSelector();
            }
        }
    }

    /**
     * 显示礼物弹框
     *
     * @param targetUid 此人的uid
     */
    public abstract void showGiftDialog(long targetUid);

    /**
     * 显示用户信息弹框
     *
     * @param uid 用户uid
     */
    public void showUserInfoDialog(long uid) {
        YiYaUserInfoDialog.newInstance(uid)
                .setOnHandleEventListener(onUserInfoDialogEventListener)
                .show(getChildFragmentManager(), null);
    }

    /**
     * 显示用户操作按钮
     */
    public void showUserMenuOperateDialog(long uid) {
        if (getMvpPresenter().isMyself(String.valueOf(uid))) {
            showUserInfoDialog(uid);
        } else {
            getMvpPresenter().checkUserBlacklist(uid, new HttpRequestCallBack<Json>() {
                @Override
                public void onSuccess(String message, Json response) {
                    boolean value = response.boo("inBlacklist");
//                    handleUserMenuDialogItems(uid, value);
                    if (AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE) {
                        showVideoUserMenuDialogItems(uid, value);
                    } else {
                        showAudioUserMenuDialogItems(uid, value);
                    }
                }

                @Override
                public void onFailure(int code, String msg) {
                    toast(code + " " + msg);
                }
            });
        }
    }

    /**
     * 语音房
     *
     * @param uid   用户UID
     * @param value 是否在自己的黑名单列表
     */
    private void showAudioUserMenuDialogItems(long uid, boolean value) {
        String valueStr = value ? "移除个人黑名单" : "加入个人黑名单";
        List<ButtonItem> buttonItems = new ArrayList<>();
        if (getMvpPresenter().isRoomOwnerByMyself()) { //房主
            boolean isAdmin = getMvpPresenter().isRoomAdmin(uid);
            buttonItems.add(new ButtonItem(isAdmin ? "撤销该管理员" : "设为管理员", () -> getMvpPresenter().operateAddOrRemoveManager(!isAdmin, uid)));
            buttonItems.add(new ButtonItem("加入黑名单", () ->
                    getDialogManager().showOkCancelDialog("是否将该用户拉入黑名单？拉入后他将无法进入此房间", true, new DialogManager.AbsOkDialogListener() {
                        @Override
                        public void onOk() {
                            getMvpPresenter().operateRoomBlackList(uid, true, true);
                        }
                    })));

        } else if (getMvpPresenter().isRoomAdminByMyself()) {   //管理员
            buttonItems.add(new ButtonItem("加入房间黑名单", () ->
                    getDialogManager().showOkCancelDialog("是否将该用户拉入房间黑名单？拉入后他将无法进入此房间", true, new DialogManager.AbsOkDialogListener() {
                        @Override
                        public void onOk() {
                            getMvpPresenter().operateRoomBlackList(uid, true, false);
                        }
                    })));

            buttonItems.add(new ButtonItem(valueStr, () -> {
                if (value) {
                    getMvpPresenter().operateUserBlackList(false, uid);
                } else {
                    getDialogManager().showOkCancelDialog("是否将该用户拉入个人黑名单？加入后他将无法与你私聊", true, new DialogManager.AbsOkDialogListener() {
                        @Override
                        public void onOk() {
                            getMvpPresenter().operateUserBlackList(true, uid);
                        }
                    });
                }
            }));

        } else {
            buttonItems.add(new ButtonItem(valueStr, () -> {
                if (value) {
                    getMvpPresenter().operateUserBlackList(false, uid);
                } else {
                    getDialogManager().showOkCancelDialog("是否将该用户拉入个人黑名单？加入后他将无法与你私聊", true, new DialogManager.AbsOkDialogListener() {
                        @Override
                        public void onOk() {
                            getMvpPresenter().operateUserBlackList(true, uid);
                        }
                    });
                }

            }));

        }
        buttonItems.add(new ButtonItem("举报用户", () -> showReportDialog(1, uid)));
        getDialogManager().showCommonPopupDialog(buttonItems, "取消");
    }

    /**
     * 视频房
     *
     * @param uid   用户UID
     * @param value 是否在自己的黑名单列表
     */
    private void showVideoUserMenuDialogItems(long uid, boolean value) {
        String valueStr = value ? "移除个人黑名单" : "加入个人黑名单";
        List<ButtonItem> buttonItems = new ArrayList<>();
        if (getMvpPresenter().isRoomOwnerByMyself() /*|| getMvpPresenter().isRoomAdminByMyself()*/) {
            checkMuteState(buttonItems, uid, value);
        } else {
            buttonItems.add(new ButtonItem(valueStr, () -> {
                if (value) {
                    getMvpPresenter().operateUserBlackList(false, uid);
                } else {
                    getDialogManager().showOkCancelDialog("是否将该用户拉入个人黑名单？加入后他将无法与你私聊", true, new DialogManager.AbsOkDialogListener() {
                        @Override
                        public void onOk() {
                            getMvpPresenter().operateUserBlackList(true, uid);
                        }
                    });
                }

            }));
            buttonItems.add(new ButtonItem("举报用户", () -> showReportDialog(1, uid)));
            getDialogManager().showCommonPopupDialog(buttonItems, "取消");
        }

    }

    private void checkMuteState(List<ButtonItem> buttonItems, long uid, boolean blacklistState) {
        getMvpPresenter().checkChatRoomMute(uid, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                toast(e.toString());
            }

            @Override
            public void onResponse(Json response) {
                Object[] data = response.arr("data");
                if (data == null) {
                    toast(response.num("code"));
                } else {
                    boolean isMute = data.length > 0;
                    handleMuteDialogItems(buttonItems, uid, blacklistState, isMute);
                }

            }
        });
    }

    private void handleMuteDialogItems(List<ButtonItem> buttonItems, long uid, boolean blacklistState, boolean isMute) {
        String muteStr = isMute ? "解除禁言" : "禁言";
        String valueStr = blacklistState ? "移除个人黑名单" : "加入个人黑名单";

        if (getMvpPresenter().isRoomOwnerByMyself()) { //房主

            buttonItems.add(new ButtonItem("踢出房间", () -> getMvpPresenter().operateKickMemberToRoom(uid)));
            //禁言
            buttonItems.add(new ButtonItem(muteStr, () -> getMvpPresenter().operateMarkChatRoomMute(uid + "", isMute ? 0 : 1)));
            boolean isAdmin = getMvpPresenter().isRoomAdmin(uid);
//            buttonItems.add(new ButtonItem(isAdmin ? "撤销该管理员" : "设为管理员", () -> getMvpPresenter().operateAddOrRemoveManager(!isAdmin, uid)));
            //这里的直播房房间黑名单相当于个人黑名单
            buttonItems.add(new ButtonItem(valueStr, () -> {
                if (blacklistState) {
                    getMvpPresenter().operateRoomBlackList(uid, false, true);
                } else {
                    getDialogManager().showOkCancelDialog("是否将该用户拉入个人黑名单？加入后他将无法与你私聊", true, new DialogManager.AbsOkDialogListener() {
                        @Override
                        public void onOk() {
                            getMvpPresenter().operateRoomBlackList(uid, true, true);
                        }
                    });
                }
            }));
        } else if (getMvpPresenter().isRoomAdminByMyself()) { //管理员
            buttonItems.add(new ButtonItem(valueStr, () -> {
                if (blacklistState) {
                    getMvpPresenter().operateRoomBlackList(uid, false, true);
                } else {
                    getDialogManager().showOkCancelDialog("是否将该用户拉入房间黑名单？拉入后他将无法进入此房间", true, new DialogManager.AbsOkDialogListener() {
                        @Override
                        public void onOk() {
                            getMvpPresenter().operateRoomBlackList(uid, true, true);
                        }
                    });
                }
            }));
        }

        buttonItems.add(new ButtonItem("举报用户", () -> showReportDialog(1, uid)));
        getDialogManager().showCommonPopupDialog(buttonItems, "取消");


    }

    /**
     * 显示举报弹框
     *
     * @param type 1为举报用户 和 2为举报房间
     * @param uid  被举报人的uid，如果是房间，则传房主uid
     */
    public void showReportDialog(int type, long uid) {
        List<ButtonItem> buttons = new ArrayList<>();
        ButtonItem button1 = new ButtonItem("政治敏感", () -> getMvpPresenter().commitReport(type, uid, 1));
        ButtonItem button2 = new ButtonItem("色情低俗", () -> getMvpPresenter().commitReport(type, uid, 2));
        ButtonItem button3 = new ButtonItem("广告骚扰", () -> getMvpPresenter().commitReport(type, uid, 3));
        ButtonItem button4 = new ButtonItem("人身攻击", () -> getMvpPresenter().commitReport(type, uid, 4));
        buttons.add(button1);
        buttons.add(button2);
        buttons.add(button3);
        buttons.add(button4);
        getDialogManager().showCommonPopupDialog(buttons, "取消");
    }

    /**
     * 打开私聊页面
     */
    @Override
    public void startP2PSession(long friendUid) {
        SessionHelper.startP2PSession(getContext(), String.valueOf(friendUid));
    }

    /**
     * 用户信息弹框点击事件监听
     */
    private YiYaUserInfoDialog.OnHandleEventListener onUserInfoDialogEventListener = new YiYaUserInfoDialog.OnHandleEventListener() {

        @Override
        public void onMenuClicked(long uid) {
            showUserMenuOperateDialog(uid);
        }

        @Override
        public void onSendGiftClicked(long uid) {
            showGiftDialog(uid);
        }

    };

    /**
     * 公屏消息内容点击事件监听
     */
    private MessageView.OnMsgContentClickListener onMsgContentClickListener = this::showUserInfoDialog;

    /**
     * 底部按钮点击监听
     */
    private BottomViewListenerWrapper bottomViewListenerWrapper = new BottomViewListenerWrapper() {

        @Override
        public void onMsgBtnClick() {
            onBottomMsgClick();
        }

        @Override
        public void onOpenMicBtnClick() {
            //没有im的时候不给操作
            if (!ReUsedSocketManager.get().isConnect()) {
                toast("网络已断开无法操作");
                return;
            }

            RoomQueueInfo roomQueueInfo = getMvpPresenter().getMicQueueInfoByMicPositionByMyself();
            if (roomQueueInfo == null || roomQueueInfo.mRoomMicInfo == null) return;//我不在麦上
            if (!roomQueueInfo.mRoomMicInfo.isMicMute() && !getMvpPresenter().isAudienceRoleByMyself()) {//我是主播，并且这个麦位不是被禁麦的
                getMvpPresenter().switchMicroMuteStatus();//切换闭麦/开麦状态
                refreshMicBtnStatus();
            }
        }

        @Override
        public void onSendFaceBtnClick() {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            if (AvRoomDataManager.get().isOnMic(myUid) || AvRoomDataManager.get().isRoomOwner()) {
                if (dynamicFaceDialog == null) {
                    dynamicFaceDialog = new DynamicFaceDialog(getContext());
                    dynamicFaceDialog.setOnDismissListener(dialog -> dynamicFaceDialog = null);
                }
                if (!dynamicFaceDialog.isShowing()) {
                    dynamicFaceDialog.show();
                }
            } else {
                toast("上麦才能发表情哦!");
            }
        }

        @Override
        public void onSendMsgBtnClick() {
            if (ChatUtil.checkBanned())
                return;
            getInputMsgView().setVisibility(View.VISIBLE);
            getInputMsgView().setInputText();
            getInputMsgView().showKeyBoard();
        }

        @Override
        public void onSendGiftBtnClick() {
            showGiftDialog(getMvpPresenter().getGiftDefaultSelectMemberUid());
        }

        @Override
        public void onShareBtnClick() {
            UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomShare());
            showShareDialog();
        }

        @Override
        public void onRemoteMuteBtnClick() {
            RtcEngineManager.get().setRemoteMute(!RtcEngineManager.get().isRemoteMute());
            refreshRemoteMuteBtnStatus();
        }

        @Override
        public void onMoreOperateBtnClick() {
            showRoomMoreOperateDialog();
        }

        @Override
        public void onBeautyBtnClick() {
            showRoomFaceuDialog();
        }
    };

    public void showShareDialog() {
        ShareDialog shareDialog = new ShareDialog(getActivity(), true);
        shareDialog.setHasInvite(true);
        shareDialog.show();
    }

    protected void showRoomFaceuDialog() {
        //视频房实现
    }

    /**
     * 礼物弹框里的点击事件监听
     */
    protected GiftSelector.OnGiftSelectotBtnClickListener onGiftSelectorBtnClickListener = new GiftSelector.OnGiftSelectotBtnClickListener() {
        @Override
        public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long targetUid, int number) {
            RoomInfo currentRoomInfo = getMvpPresenter().getCurrRoomInfo();

            if (giftInfo != null && currentRoomInfo != null && (!ListUtils.isListEmpty(micMemberInfos) || targetUid != 0)) {
                List<Long> targetUids = null;
                if (!ListUtils.isListEmpty(micMemberInfos)) {
                    targetUids = new ArrayList<>();
                    for (int i = 0; i < micMemberInfos.size(); i++) {
                        targetUids.add(micMemberInfos.get(i).getUid());
                    }
                }
                getMvpPresenter().sendGift(giftInfo.getGiftId(), targetUid, targetUids, number);
            }
        }

        @Override
        public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long targetUid, int number, long comboId, long comboCount, long comboFrequencyCount) {
            RoomInfo currentRoomInfo = getMvpPresenter().getCurrRoomInfo();

            if (giftInfo != null && currentRoomInfo != null && (!ListUtils.isListEmpty(micMemberInfos) || targetUid != 0)) {
                List<Long> targetUids = null;
                if (!ListUtils.isListEmpty(micMemberInfos)) {
                    targetUids = new ArrayList<>();
                    for (int i = 0; i < micMemberInfos.size(); i++) {
                        targetUids.add(micMemberInfos.get(i).getUid());
                    }
                }
                getMvpPresenter().sendGift(giftInfo.getGiftId(), targetUid, targetUids, number, comboId, comboCount, comboFrequencyCount);
            }
        }

        @Override
        public void onUserInfoClick(long uid) {
            showUserInfoDialog(uid);
        }
    };

    protected void releaseView() {
        getMicroView().release();
        getMessageView().release();
        getGiftView().release();
        getComingMsgView().release();
        releaseBannerEventView();
        getBannerView().pause();
        bottomViewListenerWrapper = null;
    }

    private void releaseBannerEventView() {
        getBannerEventView().pause();
        PagerAdapter adapter = getBannerEventView().getViewPager().getAdapter();
        if (adapter instanceof BannerEventAdapter) {
            List<BannerEvent> list = ((BannerEventAdapter) adapter).getList();
            for (int i = 0; i < list.size(); i++) {
                BannerEvent bannerEvent = list.get(i);
                if (BannerEvent.TYPE_LUCKY_BOX == bannerEvent.getItemType()) {
                    LuckyBoxView view = (LuckyBoxView) ((BannerEventAdapter) adapter).getView(getBannerEventView(), i);
                    view.release();
                }
            }
        }
    }

    /**
     * 返回按钮事件
     *
     * @return 是否拦截
     */
    public abstract boolean onBack();

    @Override
    public void onResume() {
        super.onResume();
        if (getMvpPresenter().getCurrRoomInfo() != null) {
            dealUserComingMsg();//检查一次是否有用户进人房间的消息未处理
            refreshView();
            refreshOnlineCount();
            refreshAttention();

            if (getGiftView() != null) {
                getGiftView().getBarrageView().setLiveState(BarrageView.STATE_RESUME);
            }

        } else {
            LogUtil.w("BaseRoomDetailFragment ----> onResume ---> finish");
            finish();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (getGiftView() != null) {
            getGiftView().getBarrageView().setLiveState(BarrageView.STATE_STOP);
        }
    }

    @Override
    public void onDestroyView() {
        releaseView();
        super.onDestroyView();

    }
}