package com.yiya.mobile.room.audio.widget;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.room.audio.activity.AddMusicListActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.player.IPlayerCore;
import com.tongdaxing.xchat_core.player.IPlayerCoreClient;
import com.tongdaxing.xchat_core.player.bean.LocalMusicInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/29.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class YiYaMusicPlayerDialog extends BaseTransparentDialogFragment implements SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.pack_up)
    ImageView packUp;
    @BindView(R.id.iv_voice)
    ImageView ivVoice;
    @BindView(R.id.music_list_more)
    ImageView musicListMore;
    @BindView(R.id.music_play_pause)
    ImageView musicPlayPause;
    @BindView(R.id.music_play_next)
    ImageView musicPlayNext;
    @BindView(R.id.music_play_pro)
    ImageView musicPlayPro;
    @BindView(R.id.voice_seek)
    SeekBar volumeSeekBar;
    @BindView(R.id.music_name)
    TextView musicName;
    @BindView(R.id.music_box_layout)
    ConstraintLayout musicBoxLayout;

    private String imageBg;

    private static final String KEY = "KEY";

    public static YiYaMusicPlayerDialog newInstance(String imageBg) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY, imageBg);
        YiYaMusicPlayerDialog dialog = new YiYaMusicPlayerDialog();
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.layout_yiya_music_player_view;
    }

    @Override
    protected int getWindowGravity() {
        return Gravity.TOP;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getWindow() != null) {
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            imageBg = getArguments().getString(KEY);
        }

        CoreManager.addClient(this);
        updateView();

        volumeSeekBar.setProgress(CoreManager.getCore(IPlayerCore.class).getCurrentVolume());
        volumeSeekBar.setOnSeekBarChangeListener(this);
    }

    public void updateVoiceValue() {
        if (volumeSeekBar != null) {
            volumeSeekBar.setProgress(CoreManager.getCore(IPlayerCore.class).getCurrentVolume());
        }
    }

    public void release() {
        CoreManager.removeClient(this);
    }

    private void updateView() {
        LocalMusicInfo current = CoreManager.getCore(IPlayerCore.class).getCurrent();
        updateView(current);
    }

    private void updateView(LocalMusicInfo musicInfo) {
        if (musicInfo != null) {
            musicName.setText(musicInfo.getSongName());
            int state = CoreManager.getCore(IPlayerCore.class).getState();
            if (state == IPlayerCore.STATE_PLAY) {
                musicPlayPause.setImageResource(R.drawable.icon_music_play);
            } else {
                musicPlayPause.setImageResource(R.drawable.icon_music_pause_small);
            }
        } else {
            musicName.setText("暂无歌曲播放");
            musicPlayPause.setImageResource(R.drawable.icon_music_pause_small);
        }
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPlaying(LocalMusicInfo localMusicInfo) {
        updateView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicPause(LocalMusicInfo localMusicInfo) {
        updateView();

    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onMusicStop() {
        updateView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onCurrentMusicUpdate(LocalMusicInfo localMusicInfo) {
        updateView();
    }

    @CoreEvent(coreClientClass = IPlayerCoreClient.class)
    public void onRefreshPlayerList(List<LocalMusicInfo> playerListMusicInfoList) {
        LocalMusicInfo current = CoreManager.getCore(IPlayerCore.class).getCurrent();
        if (current == null && !ListUtils.isListEmpty(playerListMusicInfoList)) {
            updateView(playerListMusicInfoList.get(0));
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        CoreManager.getCore(IPlayerCore.class).seekVolume(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        release();
    }

    @OnClick({R.id.fl_root, R.id.pack_up, R.id.music_list_more, R.id.music_play_pause, R.id.music_play_next, R.id.music_play_pro})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fl_root:
            case R.id.pack_up:
                dismiss();
                break;
            case R.id.music_list_more:
                AddMusicListActivity.start(getContext(), imageBg);
                break;
            case R.id.music_play_pause:
                List<LocalMusicInfo> localMusicInfoList = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
                if (localMusicInfoList != null && localMusicInfoList.size() > 0) {
                    int state = CoreManager.getCore(IPlayerCore.class).getState();
                    if (state == IPlayerCore.STATE_PLAY) {
                        CoreManager.getCore(IPlayerCore.class).pause();
                    } else if (state == IPlayerCore.STATE_PAUSE) {
                        CoreManager.getCore(IPlayerCore.class).play(null);
                    } else {
                        int result = CoreManager.getCore(IPlayerCore.class).playNext();
                        if (result < 0) {
                            if (result == -3) {
                                ((BaseMvpActivity) getContext()).toast("播放列表中还没有歌曲哦！");
                            } else {
                                ((BaseMvpActivity) getContext()).toast("播放失败，文件异常");
                            }
                        }
                    }
                } else {
                    AddMusicListActivity.start(getContext(), imageBg);
                }
                break;
            case R.id.music_play_next:
                List<LocalMusicInfo> localMusicInfoList1 = CoreManager.getCore(IPlayerCore.class).getPlayerListMusicInfos();
                if (localMusicInfoList1 != null && localMusicInfoList1.size() > 0) {
                    int result = CoreManager.getCore(IPlayerCore.class).playNext();
                    if (result < 0) {
                        if (result == -3) {
                            ((BaseMvpActivity) getContext()).toast("播放列表中还没有歌曲哦！");
                        } else {
                            ((BaseMvpActivity) getContext()).toast("播放失败，文件异常");
                        }
                    }
                } else {
                    AddMusicListActivity.start(getContext(), imageBg);
                }
                break;
        }
    }
}
