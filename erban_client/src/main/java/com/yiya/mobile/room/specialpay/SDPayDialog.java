package com.yiya.mobile.room.specialpay;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.fragment.BaseMvpDialogFragment;
import com.yiya.mobile.presenter.specialpay.SpecialPayPresenter;
import com.yiya.mobile.presenter.specialpay.SpecialPayView;
import com.juxiao.library_utils.log.LogUtil;
import com.pingplusplus.android.Pingpp;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.count.IChargeClient;
import com.tongdaxing.xchat_core.initial.ClientConfigure;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.DiscountGift;
import com.tongdaxing.xchat_core.room.bean.DiscountGiftInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.log.MLog;
import com.yiya.mobile.ui.me.wallet.activity.WalletActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;

import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_HC;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_HJ;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_PPP;

/**
 * @author zdw
 * @date 2019/9/2
 * @describe 特惠支付的v
 */

@CreatePresenter(SpecialPayPresenter.class)
public class SDPayDialog extends BaseMvpDialogFragment<SpecialPayView, SpecialPayPresenter> implements SpecialPayView {

    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.tv_limit)
    TextView tvLimit;
    @BindView(R.id.tv_wx_pay)
    TextView tvWXPay;
    @BindView(R.id.tv_ali_pay)
    TextView tvAliPay;


    private static final int POLLCOUNT= 4;
    private int mPollCount = 0;
    private static final int POLL_CODE = 2;

    private DiscountGiftInfo mDiscountGiftInfo;

    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View rootView = inflater.inflate(R.layout.dialog_sd_pay, window.findViewById(android.R.id.content), false);
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setWindowAnimations(R.style.Dialog_sd_Pay_Style);
        window.setGravity(Gravity.CENTER);

        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    public int getRootLayoutId() {
        return 0;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshView(mDiscountGiftInfo);
    }

    public void setData(DiscountGiftInfo discountGiftInfo) {
        mDiscountGiftInfo = discountGiftInfo;
    }

    public void refreshView(DiscountGiftInfo discountGiftInfo) {
        if (discountGiftInfo == null) {
            dismiss();
//            AvRoomDataManager.get().setDialogShow(false);
            return;
        }

        String content = "";
        SpannableStringBuilder builder = new SpannableStringBuilder(content);
        String num = discountGiftInfo.getDiscountPriceRMB();
        builder.append(num);
        setStrSize(builder, builder.toString().length() - num.length(), builder.toString().length());

        String unit = "元";
        builder.append(unit);
        setStrColor(builder, builder.toString().length() - unit.length(), builder.toString().length(), R.color.color_383950);
        tvPrice.setText(builder);

        builder.clear();
        String total = String.valueOf(discountGiftInfo.getMaxBuyCount());
        String desFront = "每天限购" + total + "次(";
        builder.append(desFront);
        setStrColor(builder, builder.toString().length() - desFront.length(), builder.toString().length(), R.color.color_A8A8A8);

        String hasTimes = String.valueOf(discountGiftInfo.getUserBuyCount());
        builder.append(hasTimes);
        if (discountGiftInfo.getUserBuyCount() <= 0) {
            setStrColor(builder, builder.toString().length() - hasTimes.length(), builder.toString().length(), R.color.color_A8A8A8);
        } else {
            setStrColor(builder, builder.toString().length() - hasTimes.length(), builder.toString().length(), R.color.black);
        }

        String desBack = "/" + total + ")";
        builder.append(desBack);
        setStrColor(builder, builder.toString().length() - desBack.length(), builder.toString().length(), R.color.color_A8A8A8);

        tvLimit.setText(builder);
    }

    /**
     * 文字颜色
     */
    private void setStrColor(SpannableStringBuilder builder, int start, int end, int color) {
        ForegroundColorSpan redSpan = new ForegroundColorSpan(BasicConfig.INSTANCE.getAppContext().getResources().getColor(color));
        builder.setSpan(redSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * 文字颜色
     */
    private void setStrSize(SpannableStringBuilder builder, int start, int end) {
        RelativeSizeSpan sizeSpan = new RelativeSizeSpan(1.5f);
        builder.setSpan(sizeSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

        tvAliPay.setOnClickListener(v -> {
            if (mDiscountGiftInfo == null) return;
            RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
            if (roomInfo == null) return;
//            mPollCount = POLLCOUNT;
            ClientConfigure clientConfigure = DemoCache.readClientConfigure();
            if ((clientConfigure != null) && (clientConfigure.getAlipayChannel() == 2)) {//支付宝 1,ping++;2,汇潮
                if (!checkAliPayInstalled(mContext)){
                    toast("未安装支付宝");
                    return;
                }
                getMvpPresenter().requestCharge(BasicConfig.INSTANCE.getAppContext(), null, roomInfo.getRoomId(), mDiscountGiftInfo.getGiftId(), Constants.CHARGE_HC_ALIPAY, CHARGE_TYPE_HC);
            } else {
                getMvpPresenter().requestCharge(BasicConfig.INSTANCE.getAppContext(), null, roomInfo.getRoomId(), mDiscountGiftInfo.getGiftId(), Constants.CHARGE_ALIPAY, CHARGE_TYPE_PPP);
            }
            dismiss();
        });

        tvWXPay.setOnClickListener(v -> {
            Platform wechat = ShareSDK.getPlatform(Wechat.NAME);
            if (!wechat.isClientValid()) {
                toast("未安装微信");
                return;
            }

            if (mDiscountGiftInfo == null) return;
            RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
            if (roomInfo == null) return;
            ClientConfigure clientConfigure = DemoCache.readClientConfigure();
//            mPollCount = POLLCOUNT;
            if ((clientConfigure != null) && (clientConfigure.getPayChannel() == 2)) {//微信 1,ping++;2,汇聚
                getMvpPresenter().requestCharge(BasicConfig.INSTANCE.getAppContext(), null, roomInfo.getRoomId(), mDiscountGiftInfo.getGiftId(), Constants.CHARGE_HJ_WX, CHARGE_TYPE_HJ);
            } else {
                getMvpPresenter().requestCharge(BasicConfig.INSTANCE.getAppContext(), null, roomInfo.getRoomId(), mDiscountGiftInfo.getGiftId(), Constants.CHARGE_WX, CHARGE_TYPE_PPP);
            }
            dismiss();
        });
    }

    @Override
    public void initiate() {

    }

    @Override
    public void getSpecialDiscountGiftSuccess(List<DiscountGift> discountGifts) {
        if (AvRoomDataManager.get().getRoomInfo() == null || ListUtils.isListEmpty(discountGifts)) return;
        boolean hasSend = false;
        for (DiscountGift discountGift: discountGifts){
            if (discountGift.getGiftNum() <= 0)continue;
            getMvpPresenter().sendGift(AvRoomDataManager.get().getRoomInfo().getRoomId(), discountGift.getGiftId(),
                    AvRoomDataManager.get().getRoomInfo().getUid(), null, 1,
                    System.currentTimeMillis(), 1, 1);
            hasSend = true;
        }

        if (hasSend){
            SingleToastUtil.showToast("你已成功购买礼物");
            mPollCount = 0;
            dismiss();
        }
    }

    @Override
    public void getSpecialDiscountGiftFail(String error) {
        SingleToastUtil.showToast(error);
    }

    @Override
    public void getChargeOrOrderInfo(String data, int payType) {
        if (data != null) {
            switch (payType) {
                case CHARGE_TYPE_PPP:
                    Pingpp.createPayment(this, data);
                    break;
                case CHARGE_TYPE_HC: //汇潮支付
                    try {
                        String payUrl = new Json(data).str("payUrl");
                        LogUtil.e("payUrl == " + payUrl);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("alipays://platformapi/startApp?appId=10000011&url=" + URLEncoder.encode(payUrl, "UTF-8")));
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        toast("发起充值失败，请联系客服人员");
                    }
                    break;
                case CHARGE_TYPE_HJ: //汇聚支付
                    getMvpPresenter().joinPay(getActivity(), data);
                    break;
            }
        }
    }

    @Override
    public void getChargeOrOrderInfoFail(String error) {
        toast("发起充值失败" + error);
        LogUtil.i("getChargeOrOrderInfoFail", error);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: ");
        //支付页面返回处理
        if (requestCode == Pingpp.REQUEST_CODE_PAYMENT) {
            if (data != null && data.getExtras() != null) {
                String result = data.getExtras().getString("pay_result");
                if (result != null) {

                    /* 处理返回值
                     * "success" - 支付成功
                     * "fail"    - 支付失败
                     * "cancel"  - 取消支付
                     * "invalid" - 支付插件未安装（一般是微信客户端未安装的情况）
                     * "unknown" - app进程异常被杀死(一般是低内存状态下,app进程被杀死)
                     */
                    // 错误信息
                    String errorMsg = data.getExtras().getString("error_msg");
                    String extraMsg = data.getExtras().getString("extra_msg");
                    MLog.error(TAG, "errorMsg:" + errorMsg + "extraMsg:" + extraMsg);
                    CoreManager.notifyClients(IChargeClient.class, IChargeClient.chargeAction, result);
                    LogUtil.i("onResultFromPay", result);

                    if ("success".equals(result)) {
                        WalletActivity.isRefresh = true;
                        //间隔1200毫秒请求数据
                        handler.sendMessageDelayed(handler.obtainMessage(), 1200);
//                        if (mSelectChargeBean != null) {
//                            handleGrowing(mSelectChargeBean.money);
//                        }
                    } else if ("cancel".equals(result)) {
                        mPollCount = 0;
                        toast("支付被取消！");
                    } else {
                        mPollCount = 0;
                        toast("支付失败！");
                    }
                }
                dismissDialog();
            }
        }
    }

    @Override
    public void finish() {

    }

    @Override
    public void dismissDialog() {
//        AvRoomDataManager.get().setDialogShow(false);
    }

    @Override
    public void onResume() {
        super.onResume();
//        dismiss();
//        if (mPollCount > 0) {
//            handler.sendEmptyMessage(POLL_CODE);
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    private WalletInfoHandler handler = new WalletInfoHandler(this);

    private static class WalletInfoHandler extends Handler {

        private WeakReference<SDPayDialog> mReference;

        WalletInfoHandler(SDPayDialog context) {
            mReference = new WeakReference<>(context);
        }

        @Override
        public void handleMessage(Message msg) {
            if (mReference == null || mReference.get() == null) {
                return;
            }

            try {
                if (--mReference.get().mPollCount > 0) {
                    RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
                    if (roomInfo == null) return;

                    mReference.get().getMvpPresenter().getPreferentialGiftList(roomInfo.getRoomId());// 查询特惠礼物2s*3次
                    sendEmptyMessageDelayed(POLL_CODE, 2 * 1000);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * ------------------------ Growing
     */

    private void handleGrowing(int value) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("jine", value);
            GrowingIO.getInstance().track("chongzhichenggong", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean checkAliPayInstalled(Context context) {

        Uri uri = Uri.parse("alipays://platformapi/startApp");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        return componentName != null;
    }
}
