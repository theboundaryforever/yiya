package com.yiya.mobile.room.avroom.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.Map;

public class RoomPlayTipActivity extends BaseActivity {
    private EditText etPlay;
    private int MAXLINES = 15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_play_tip);
        initTitleBar("设置进入房间提示");
        initView();
    }

    private void initView() {
        etPlay = findView(R.id.et_room_play);
        GrowingIO.getInstance().trackEditText(etPlay);
        if (StringUtils.isNotEmpty(AvRoomDataManager.get().getRoom_rule()))
            etPlay.setText(AvRoomDataManager.get().getRoom_rule());
        TextView rightAction = new TextView(this);
        rightAction.setTextColor(Color.parseColor("#1a1a1a"));
        rightAction.setText("保存");
        rightAction.setOnClickListener(v -> save());
        if (mTitleBar != null)
            mTitleBar.mRightLayout.addView(rightAction);
    }

    private void save() {
        String playInfo = etPlay.getText().toString();
        if (TextUtils.isEmpty(playInfo)) {
            toast("房间玩法不能为空");
            return;
        }
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo == null)
            return;
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        Map<String,String> param = CommonParamUtil.getDefaultParam();
        param.put("playInfo", playInfo);
        param.put("tagId",roomInfo.tagId+"");
        param.put("uid", uid + "");
        param.put("ticket", ticket);
        param.put("roomUid", roomInfo.getUid() + "");
        param.put("type", String.valueOf(RoomInfo.ROOMTYPE_HOME_PARTY));

//        param.put("roomDesc", null);
//        param.put("roomNotice", null);
//        param.put("title", null);
//        param.put("backPic", null);
//        param.put("roomPwd", null);
//        param.put("giftEffectSwitch", null);
//        param.put("publicChatSwitch", null);

        String url = "";
        if (AvRoomDataManager.get().isRoomOwner()) {
            url = UriProvider.updateRoomInfo();
        } else if (AvRoomDataManager.get().isRoomAdmin()) {
            url = UriProvider.updateByAdimin();
        } else {
            return;
        }
        OkHttpManager.getInstance().doPostRequest(url,param, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                toast("网络异常");
            }

            @Override
            public void onResponse(Json json) {
                if (json.num("code") == 200) {
                    AvRoomDataManager.get().setRoom_rule(playInfo);
                    toast("保存成功");
                    finish();
                } else {
                    toast(json.str("message", "网络异常"));
                }

            }
        });
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, RoomPlayTipActivity.class);
        context.startActivity(intent);
    }
}
