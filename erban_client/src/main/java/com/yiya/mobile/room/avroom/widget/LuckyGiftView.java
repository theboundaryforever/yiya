package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.im.custom.bean.LuckyGiftAttachment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LuckyGiftView extends RelativeLayout {
    private Context context;

    @BindView(R.id.rlLuckyHightGift)
    ConstraintLayout rlLuckyHightGift;
    @BindView(R.id.tv_high_output_gold)
    TextView tvHighOutputGold;
    @BindView(R.id.tv_high_proportion)
    TextView tvHighProportion;

    @BindView(R.id.rlLuckyLowGift)
    RelativeLayout rlLuckyLowGift;
    @BindView(R.id.tv_low_output_gold)
    TextView tvLowOutputGold;
    @BindView(R.id.tv_low_proportion)
    TextView tvLowProportion;

    private double curProportion;

    public LuckyGiftView(Context context) {
        super(context);
        init(context);
    }

    public LuckyGiftView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LuckyGiftView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_lucky_gift_view, this, true);
        ButterKnife.bind(this, view);
    }

    public void setupView(LuckyGiftAttachment luckyGiftAttachment) {
        if (luckyGiftAttachment == null) return;
        int outputGold = (int)luckyGiftAttachment.getOutputGold();
        int  proportion = (int)luckyGiftAttachment.getProportion();
        curProportion = proportion;
        if (proportion < 50) {
            rlLuckyLowGift.setVisibility(VISIBLE);
            rlLuckyHightGift.setVisibility(INVISIBLE);
            tvLowOutputGold.setText(outputGold + "金币");
            tvLowProportion.setText("恭喜获得" + proportion + "倍返币");
        } else {
            rlLuckyLowGift.setVisibility(INVISIBLE);
            rlLuckyHightGift.setVisibility(VISIBLE);
            tvHighOutputGold.setText("获得"+outputGold + "金币");
            tvHighProportion.setText(proportion+"");
        }

        startAnimation();
    }

    public void startAnimation() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(
                1.0f, 0.8f, 1.0f, 0.8f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f
        );
        scaleAnimation.setDuration(2000);
        scaleAnimation.setInterpolator(new AnticipateOvershootInterpolator());
        //动画执行完毕后是否停在结束时的状态
//        animation.setFillAfter(true);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rlLuckyHightGift.setVisibility(View.GONE);
                rlLuckyLowGift.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        if (curProportion < 50) {
            rlLuckyLowGift.startAnimation(scaleAnimation);
        } else {
            rlLuckyHightGift.startAnimation(scaleAnimation);
        }
    }
}
