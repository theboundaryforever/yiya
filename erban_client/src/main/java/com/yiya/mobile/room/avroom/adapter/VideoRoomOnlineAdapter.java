package com.yiya.mobile.room.avroom.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/9
 * 描述        显示在线用户列表
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */

public class VideoRoomOnlineAdapter extends BaseQuickAdapter<IMChatRoomMember, BaseViewHolder> {

    public VideoRoomOnlineAdapter() {
        super(R.layout.video_room_online_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, IMChatRoomMember item) {
        int adapterPosition = helper.getAdapterPosition();
        ImageLoadUtils.loadAvatar(mContext, item.getAvatar(), helper.getView(R.id.avatar), true);

        switch (adapterPosition){
            case 0 :
                ImageLoadUtils.loadImageRes(mContext, R.mipmap.ic_avatar_hat_1, helper.getView(R.id.item_avatar_ranking_iv));
                break;
            case 1 :
                ImageLoadUtils.loadImageRes(mContext, R.mipmap.ic_avatar_hat_2, helper.getView(R.id.item_avatar_ranking_iv));
                break;
            case 2 :
                ImageLoadUtils.loadImageRes(mContext, R.mipmap.ic_avatar_hat_3, helper.getView(R.id.item_avatar_ranking_iv));
                break;
            default:
                break;
        }
    }

}
