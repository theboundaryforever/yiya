package com.yiya.mobile.room.avroom.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yiya.mobile.room.avroom.holder.BaseAudioMicroViewHolder;
import com.yiya.mobile.room.avroom.holder.YiYaAudioBossMicroViewHolder;
import com.yiya.mobile.room.avroom.holder.YiYaAudioMicroViewHolder;
import com.yiya.mobile.room.avroom.other.OnMicroItemClickListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomCharmInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;

import org.jetbrains.annotations.NotNull;

/**
 * 语音房麦位adapter
 *
 * @author zeda
 */

public class YiYaAudioMicroViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_BOSS = 1;
    private static final int TYPE_NORMAL = 0;
    public static final int TYPE_INVALID = -2;
    private final static int MICRO_COUNT = 9;

    private Context context;

    private OnMicroItemClickListener onMicroItemClickListener;
    private int spanCount;

    public YiYaAudioMicroViewAdapter(Context context, int spanCount) {
        this.spanCount = spanCount;
        this.context = context;
    }

    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        this.onMicroItemClickListener = onMicroItemClickListener;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View item;
        if (viewType == TYPE_BOSS) {
            item = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_audio_room_owner, parent, false);
            return new YiYaAudioBossMicroViewHolder(context, item);
        } else {
            item = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_audio_room_guest, parent, false);
            return new YiYaAudioMicroViewHolder(context, item);
        }
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder viewHolder, final int position) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(AvRoomDataManager.getMicPositionByMicroViewPosition(position));
        BaseAudioMicroViewHolder holder = (BaseAudioMicroViewHolder) viewHolder;
        holder.setOnMicroItemClickListener(new OnMicroItemClickListener() {
            @Override
            public void onAvatarBtnClick(int position, @Nullable IMChatRoomMember chatRoomMember) {
                if (onMicroItemClickListener != null) {
                    onMicroItemClickListener.onAvatarBtnClick(position, chatRoomMember);
                }
            }

            @Override
            public void onUpMicBtnClick(int position) {
                if (onMicroItemClickListener != null) {
                    onMicroItemClickListener.onUpMicBtnClick(position);
                }
            }

            @Override
            public void onLockBtnClick(int position) {
                if (onMicroItemClickListener != null) {
                    onMicroItemClickListener.onLockBtnClick(position);
                }
            }

            @Override
            public void onAvatarSendMsgClick(int position) {
                if (onMicroItemClickListener != null) {
                    onMicroItemClickListener.onAvatarSendMsgClick(position);
                }
            }

            @Override
            public void onDownMicBtnClick(int position) {
                if (onMicroItemClickListener != null) {
                    onMicroItemClickListener.onDownMicBtnClick(position);
                }
            }

            @Override
            public void onGiftBtnClick(@NonNull IMChatRoomMember chatRoomMember) {
                if (onMicroItemClickListener != null) {
                    onMicroItemClickListener.onGiftBtnClick(chatRoomMember);
                }
            }

            @Override
            public void onNoteGiftClick(@NonNull IMChatRoomMember chatRoomMember) {
                if (onMicroItemClickListener != null) {
                    onMicroItemClickListener.onNoteGiftClick(chatRoomMember);
                }
            }
        });

        holder.bind(roomQueueInfo, position - 1);
    }

    /**
     * 更新麦位魅力值
     *
     * @param roomCharmAttachment
     */
    public void updateCharmData(RoomCharmAttachment roomCharmAttachment) {
        if (roomCharmAttachment == null || AvRoomDataManager.get().mMicQueueMemberMap == null) {
            return;
        }
        if (roomCharmAttachment.getLatestCharm() != null
                && roomCharmAttachment.getLatestCharm().size() > 0
                && roomCharmAttachment.getTimestamps() > AvRoomDataManager.get().getCharmTimestamps()) {
            boolean isFirst = AvRoomDataManager.get().getCharmTimestamps() == 0;
            AvRoomDataManager.get().setCharmTimestamps(roomCharmAttachment.getTimestamps());
            for (int i = 1; i < AvRoomDataManager.get().mMicQueueMemberMap.size(); i++) {
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                    if (roomQueueInfo.mChatRoomMember.getAccount() != null) {
                        RoomCharmInfo roomCharmInfo = roomCharmAttachment.getLatestCharm().get(roomQueueInfo.mChatRoomMember.getAccount());
                        if (!isFirst && roomCharmInfo != null) {
                            notifyItemChanged(roomQueueInfo.mRoomMicInfo.getPosition() + 1);
                        }
                    }
                }
            }
            if (isFirst) {
                notifyDataSetChanged();
            }
        }
    }


    @Override
    public void onAttachedToRecyclerView(@NotNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (getItemViewType(position) == TYPE_BOSS) {
                        return spanCount;
                    } else {
                        return 1;
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return MICRO_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? TYPE_BOSS : TYPE_NORMAL;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
