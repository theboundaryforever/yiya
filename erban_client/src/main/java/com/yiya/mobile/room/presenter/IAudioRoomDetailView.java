package com.yiya.mobile.room.presenter;

import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.bean.RecommendWorldsInfo;
import com.tongdaxing.xchat_core.bean.RoomRedPackageInfo;
import com.tongdaxing.xchat_core.bean.SendRedPackageInfo;

import java.util.LinkedList;
import java.util.List;

public interface IAudioRoomDetailView extends IRoomDetailView {
    void refreshRich(List<RankingXCInfo.ListBean> data);

    void refreshCharm(List<RankingXCInfo.ListBean> data);

    void showReceivedRedPackageView(SendRedPackageInfo sendRedPackageInfo);

    void refreshRedPackageView(LinkedList<RoomRedPackageInfo> roomRedPackageInfos);

    void startCPKCountDown(long expireSeconds);

    void showRecommendWorlds(RecommendWorldsInfo info);
}
