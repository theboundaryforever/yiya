package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;

import org.jetbrains.annotations.NotNull;


/**
 * @author Zhangsongzhou
 * @date 2019/6/5
 */
public class FaceuCheckBox extends LinearLayout {

    private Context mContext;

    public FaceuCheckBox(Context context) {
        this(context, null);
    }

    public FaceuCheckBox(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FaceuCheckBox(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        LayoutInflater.from(mContext).inflate(R.layout.checkbox_faceu, this);

        mSrcImageView = findViewById(R.id.check_box_src_iv);
        mSvgaImageView = findViewById(R.id.check_box_select_siv);
        mSelectIv = findViewById(R.id.check_box_select_iv);
        mTitleNameTv = findViewById(R.id.check_box_name_tv);

        TypedArray typedArray = mContext.obtainStyledAttributes(attrs, R.styleable.FaceuCheckBox, defStyleAttr, 0);

        mSrcDrawable = typedArray.getDrawable(R.styleable.FaceuCheckBox_check_box_src);
        mTitleName = typedArray.getString(R.styleable.FaceuCheckBox_check_box_text);
        mSVGASource = typedArray.getString(R.styleable.FaceuCheckBox_check_box_select_source);

        mSrcImageView.setImageDrawable(mSrcDrawable);
        mTitleNameTv.setText(mTitleName);

        initSAVG(mSVGASource);
    }

    private ImageView mSrcImageView;

    private SVGAImageView mSvgaImageView;
    private ImageView mSelectIv;
    private TextView mTitleNameTv;


    private Drawable mSrcDrawable;

    private String mTitleName;
    private String mSVGASource;

    private boolean isCheck;


    public void setCheck(boolean value) {
        isCheck = value;
//        if (isCheck) {
//            if (!TextUtils.isEmpty(mSVGASource)) {
//                mSvgaImageView.setClearsAfterStop(false);
//                mSvgaImageView.startAnimation();
//            }
//        } else {
//            if (!TextUtils.isEmpty(mSVGASource)) {
//                mSvgaImageView.setClearsAfterStop(true);
//                mSvgaImageView.stopAnimation();
//            }
//
//        }

        if (isCheck) {
            mSelectIv.setImageResource(R.mipmap.icon_beauty_select);
        } else {
            mSelectIv.setImageDrawable(null);
        }
    }

    private void initSAVG(String source) {
        if (!TextUtils.isEmpty(source)) {
            SVGAParser parser = new SVGAParser(mContext);
            parser.decodeFromAssets(source, new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                    SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                    mSvgaImageView.setImageDrawable(drawable);
                    mSvgaImageView.setClearsAfterStop(false);
                }

                @Override
                public void onError() {

                }
            });
        }
    }

    public boolean isCheck() {
        return isCheck;
    }


}
