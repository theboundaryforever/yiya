package com.yiya.mobile.room.avroom.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

import com.tongdaxing.erban.R;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/22.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MicContainerView extends FrameLayout {

    private Disposable disposable;

    public MicContainerView(@NonNull Context context) {
        this(context, null);
    }

    public MicContainerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MicContainerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.layout_mic_apply_tips_container, this);
    }

    public void addView(String avatar) {
        MicApplyTipsView micApplyTipsView = new MicApplyTipsView(getContext());
        micApplyTipsView.setAvatar(avatar);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.gravity = Gravity.END;
        addView(micApplyTipsView, params);
        micApplyTipsView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                micApplyTipsView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ObjectAnimator animator = ObjectAnimator.ofFloat(micApplyTipsView, "translationX", micApplyTipsView.getWidth(), 0);
                animator.setDuration(500).start();
                animator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        disposable = Observable.timer(2, TimeUnit.SECONDS)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(aLong -> removeView(micApplyTipsView));
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
            }
        });
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
