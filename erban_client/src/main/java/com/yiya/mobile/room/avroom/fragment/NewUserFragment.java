package com.yiya.mobile.room.avroom.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RadioGroup;

import com.yiya.mobile.base.fragment.BaseFragment;
import com.yiya.mobile.ui.home.adpater.NewUserAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.bean.NewUserBean;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;

/**
 * @author zdw
 * @date 2019/9/2
 * @describe 语聊房萌新功能列表
 */

public class NewUserFragment extends BaseFragment implements OnRefreshListener, OnLoadmoreListener {
    private SmartRefreshLayout srlRefresh;
    private RadioGroup rgMengXin;
    private RecyclerView rvMengXin;
    private NewUserAdapter mAdapter;
    private int mPage = Constants.PAGE_START;
    private String gender = "";
    private boolean isLoading = false;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_meng_xin;
    }

    @Override
    public void onFindViews() {
        rvMengXin = mView.findViewById(R.id.rv_meng_xin);
        srlRefresh = mView.findViewById(R.id.swipe_refresh);
        mAdapter = new NewUserAdapter(R.layout.item_rv_new_user);
        mAdapter.setEnableLoadMore(false);
        rvMengXin.setLayoutManager(new LinearLayoutManager(getContext()));
        rvMengXin.setAdapter(mAdapter);
    }

    @Override
    public void onSetListener() {
        srlRefresh.setOnRefreshListener(this);
        srlRefresh.setOnLoadmoreListener(this);
    }

    @Override
    public void initiate() {
        showLoading();
        initData();
    }

    private void initData() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("gender", gender);
        params.put("pageNum", mPage + "");
        params.put("pageSize", 20 + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.getMengXin(), params, new OkHttpManager.MyCallBack<ServiceResult<List<NewUserBean>>>() {
            @Override
            public void onError(Exception e) {
                isLoading = false;
                if (mPage == Constants.PAGE_START) {
                    showNetworkErr();
                    srlRefresh.finishRefresh();
                } else {
                    mPage--;
                    srlRefresh.finishLoadmore();
                }
            }

            @Override
            public void onResponse(ServiceResult<List<NewUserBean>> response) {
                isLoading = false;
                if (response != null && response.isSuccess() && !ListUtils.isListEmpty(response.getData())) {
                    hideStatus();
                    if (mPage == Constants.PAGE_START) {//
                        mAdapter.setNewData(response.getData());
                        srlRefresh.finishRefresh();
                    } else {
                        mAdapter.addData(response.getData());
                        srlRefresh.finishLoadmore();
                    }

                } else {
                    if (mPage == Constants.PAGE_START) {
                        showNoData("暂无该分类萌新数据");
                        srlRefresh.finishRefresh();
                    } else {
                        srlRefresh.finishLoadmoreWithNoMoreData();
                    }
                }
            }
        });
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        mPage = Constants.PAGE_START;
        srlRefresh.setLoadmoreFinished(false);
        initData();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        isLoading = true;
        mPage++;
        initData();
    }
}
