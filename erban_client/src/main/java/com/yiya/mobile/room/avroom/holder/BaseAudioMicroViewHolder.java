package com.yiya.mobile.room.avroom.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.yiya.mobile.room.avroom.other.OnMicroItemClickListener;
import com.yiya.mobile.ui.widget.HeadWearImageView;
import com.juxiao.library_ui.widget.WaveView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomCharmInfo;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;

import java.util.HashMap;

import static com.yiya.mobile.room.avroom.adapter.AudioMicroViewAdapter.TYPE_INVALID;

public class BaseAudioMicroViewHolder extends RecyclerView.ViewHolder {

    WaveView waveView;
    HeadWearImageView ivHeadWear;
    Context context;
    ImageView charmHatIv;

    OnMicroItemClickListener onMicroItemClickListener;

    public static final String KEY_GENDER = "KEY_GENDER";
    public static final String KEY_CHARM = "KEY_CHARM";


    public BaseAudioMicroViewHolder(Context context, View itemView) {
        super(itemView);
        this.context = context;
        waveView = itemView.findViewById(R.id.wave_view);
        ivHeadWear = itemView.findViewById(R.id.avatar);
        charmHatIv = itemView.findViewById(R.id.iv_charm_hat);
    }

    protected RoomQueueInfo info;
    protected int position = TYPE_INVALID;

    public void clear() {
        info = null;
        onMicroItemClickListener = null;
        position = TYPE_INVALID;
        waveView.stop();
        ivHeadWear.setVisibility(View.GONE);
    }

    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        this.onMicroItemClickListener = onMicroItemClickListener;
    }

    public void bind(RoomQueueInfo info, int position) { // 传过来的是麦位
        this.info = info;
        if (info == null) {
            return;
        }
        this.position = position;
        RoomMicInfo roomMicInfo = info.mRoomMicInfo;
        IMChatRoomMember chatRoomMember = info.mChatRoomMember;
        // 清除动画
        waveView.stop();
        if (roomMicInfo == null) {
            ivHeadWear.setVisibility(View.GONE);
            return;
        }

        //显示，先展示人，无视麦的锁
        if (chatRoomMember != null) {
            if (!TextUtils.isEmpty(chatRoomMember.getAccount()) && JavaUtil.str2long(chatRoomMember.getAccount()) > 0) {
                ivHeadWear.setVisibility(View.VISIBLE);

                if ((AvRoomDataManager.get().getRoomInfo() != null && AvRoomDataManager.get().getRoomInfo().getCharmEnable() == 2) || AvRoomDataManager.get().isHasCharm()) {

                    if (AvRoomDataManager.get().getMicCharmInfo() != null) {
                        RoomCharmInfo roomCharmInfo = AvRoomDataManager.get().getMicCharmInfo().get(chatRoomMember.getAccount());
                        if (roomCharmInfo != null) {
                            HashMap<String, Integer> map = new HashMap<>();
                            map.put(KEY_GENDER, chatRoomMember.getGender());
                            map.put(KEY_CHARM, roomCharmInfo.getValue());
                            updateCharmValue(map);
                            if (roomCharmInfo.isWithHat()) {
//                                if (roomCharmInfo.getType() == 1) {
//                                    charmHatIv.setImageResource(R.mipmap.icon_margin_hat);
//                                } else if (roomCharmInfo.getType() == 2) {
//                                    charmHatIv.setImageResource(R.mipmap.icon_poison_hat);
//                                }
                                charmHatIv.setVisibility(View.VISIBLE);
                            } else {
                                charmHatIv.setVisibility(View.GONE);
                            }
                        } else {
                            updateCharmValue(null);
                            charmHatIv.setVisibility(View.GONE);
                        }
                    } else {
                        updateCharmValue(null);
                        charmHatIv.setVisibility(View.GONE);
                    }
                } else {
                    charmHatIv.setVisibility(View.GONE);
                }

                ivHeadWear.setAvatar(chatRoomMember.getAvatar());

                //--------头饰-----------
                String headwearUrl = chatRoomMember.getHeadwear_url();
                ivHeadWear.setHeadWear(headwearUrl);
                //--------头饰-----------
            } else {
                ivHeadWear.setVisibility(View.INVISIBLE);
            }
        } else {
            ivHeadWear.setVisibility(View.INVISIBLE);

        }
    }

    protected void updateCharmValue(HashMap<String, Integer> map) {
    }

}
