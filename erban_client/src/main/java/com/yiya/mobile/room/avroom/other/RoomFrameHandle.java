package com.yiya.mobile.room.avroom.other;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.yiya.mobile.ChatApplicationLike;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.room.model.RoomModel;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.ReUsedSocketManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.im.IMError;
import com.tongdaxing.xchat_framework.im.IMProCallBack;
import com.tongdaxing.xchat_framework.im.IMReportBean;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.lang.ref.WeakReference;
import java.util.Stack;

import static com.yiya.mobile.room.model.RoomModel.ERROR_CODE_AUDIO_CHANNEL_INIT;
import static com.yiya.mobile.room.model.RoomModel.ERROR_CODE_DEFAULT;
import static com.yiya.mobile.room.model.RoomModel.ERROR_CODE_VIDEO_LIVE_FINISH;

/**
 * 房间框架Handle
 */
public class RoomFrameHandle {
    /**
     * 进房等待弹框 在进房流程等了多长时间后弹出
     */
    private static final int ENTER_ROOM_WAIT_TIME_TO_SHOW_DIALOG = 1000;

    private volatile static RoomFrameHandle frameHandle;
    private WeakReference<Context> weakReference;
    private DialogManager dialogManager;

    private int currCallBackId;

    private long roomUid;
    private int roomType;

    private RoomModel model;

    /**
     * 是否进房完成
     */
    private boolean isEnterRoomFinish = false;

    private Handler waitHandler = new Handler(Looper.getMainLooper());

    private static RoomFrameHandle getInstance() {
        if (frameHandle == null) {
            synchronized (RoomFrameHandle.class) {
                if (frameHandle == null) {
                    frameHandle = new RoomFrameHandle();
                }
            }
        }
        return frameHandle;
    }

    /**
     * 创建并进入房间
     *
     * @param activityContext activity的context
     * @param roomUid         房主uid
     * @param roomType        房间类型
     * @param cover           房间封面（房主开播时传）
     * @param title           房间title（房主开播时传）
     * @param canConnectMic   是否允许连麦（房主开播时传）
     */
    public static void createEnterStart(Context activityContext, long roomUid, int roomType, String cover, String title, boolean canConnectMic) {
        getInstance().handle(activityContext, roomUid, roomType, true, null, cover, title, canConnectMic);
    }

    /**
     * 进入房间
     *
     * @param activityContext activity的context
     * @param roomUid         房主uid
     * @param roomType        房间类型
     */
    public static void start(Context activityContext, long roomUid, int roomType) {
        getInstance().handle(activityContext, roomUid, roomType, false, null, null, null, false);
    }

    /**
     * 进入房间
     *
     * @param activityContext activity的context
     * @param roomUid         房主uid
     * @param roomType        房间类型
     * @param pwd             房间密码
     */
    public static void start(Context activityContext, long roomUid, int roomType, String pwd) {
        getInstance().handle(activityContext, roomUid, roomType, false, pwd, null, null, false);
    }

    private void handle(Context activityContext, long roomUid, int roomType, boolean createAndEnter, String pwd, String cover, String title, boolean canConnectMic) {
        if (AvRoomDataManager.get().checkVideoRoomStart()) {
            SingleToastUtil.showToast("你已开播，无法进行房间跳转");
            return;
        }
        //如果正在进房中，先结束
        if (!isEnterRoomFinish) {
            release();
        }
        if (AvRoomDataManager.get().isFirstEnterRoomOrChangeOtherRoom(roomUid, roomType)) {
            //第一次进来
            if (!createAndEnter && roomUid == CoreManager.getCore(IAuthCore.class).getCurrentUid() & roomType == RoomInfo.ROOMTYPE_VIDEO_LIVE) {
                //如果是视频房房主，未预览，先进入房间预览页面
                RoomFrameActivity.start(activityContext, roomUid, roomType, RoomFrameActivity.TYPE_LIVE_PREPARE);
            } else {
                weakReference = new WeakReference<>(activityContext);
                this.roomUid = roomUid;
                this.roomType = roomType;
                if (model == null) {
                    model = new RoomModel();
                }
                enterRoom(roomUid, roomType, pwd, cover, title, canConnectMic);
            }
        } else {
            //直接进入房间显示页面
            RoomFrameActivity.start(activityContext, roomUid, roomType, RoomFrameActivity.TYPE_ROOM_SHOW);
        }
    }


    /**
     * 进入房间
     *
     * @param roomUid       房主uid
     * @param roomType      房间类型
     * @param roomPwd       房间密码
     * @param cover         房间封面（房主开播时传）
     * @param title         房间title（房主开播时传）
     * @param canConnectMic 是否允许连麦（房主开播时传）
     */
    private void enterRoom(long roomUid, int roomType, String roomPwd, String cover, String title, boolean canConnectMic) {
        if (AvRoomDataManager.get().getRoomInfo() != null) {
            AvRoomDataManager.get().release();
            IMNetEaseManager.get().getChatRoomEventObservable()
                    .onNext(new RoomEvent().setEvent(RoomEvent.ROOM_EXIT));
        }
        isEnterRoomFinish = false;
        startWaitEnterRoomHandle();
        int reconnect = 0;// 重连为1 非重连为0
        LogUtil.i(AvRoomDataManager.TAG, "enterWithOpenChatRoom, roomUid = " + roomUid + " roomType = " + roomType);
        IMProCallBack enterRoomCallBack = new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                if (currCallBackId == getCallbackId()) {
                    isEnterRoomFinish = true;
                    finishWaitEnterRoomHandle();
                    IMReportBean.ReportData reportData = imReportBean.getReportData();
                    Json data = reportData.data;
                    String agora = data.str("agora");
                    String key = null;
                    try {
                        Json json = new Json(agora);
                        key = json.str("key");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (key != null) {
                        onEnterRoomSuccess(key);
                    } else {
                        onEnterRoomFail(ERROR_CODE_DEFAULT, "获取key失败");
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (currCallBackId == getCallbackId()) {
                    isEnterRoomFinish = true;
                    finishWaitEnterRoomHandle();
                    onEnterRoomFail(errorCode, errorMsg);
                }
            }
        };
        currCallBackId = enterRoomCallBack.getCallbackId();
        ReUsedSocketManager.get().enterWithOpenChatRoom(roomUid, roomType, roomPwd, reconnect, cover, title, canConnectMic, enterRoomCallBack);
    }

    private void onEnterRoomSuccess(String key) {
        LogUtil.i(AvRoomDataManager.TAG, "enterRoom success");
        dismissDialog();

        Context activityContext = getActivityContext();
        if (activityContext != null) {
            RoomInfo currRoomInfo = AvRoomDataManager.get().getRoomInfo();
            if (currRoomInfo != null) {
                int roomType = currRoomInfo.getType();
                if (!AvRoomDataManager.get().isRoomOwner() || roomType != RoomInfo.ROOMTYPE_VIDEO_LIVE) {
                    //非视频房房主在这里配置，视频房房主在预览的时候配置了
                    RtcEngineManager.get().configEngine(roomType, false);
                }
                boolean isJoinSuccess = joinRtcEngineChannel(key);
                if (isJoinSuccess) {
                    RoomFrameActivity.start(activityContext, roomUid, roomType, RoomFrameActivity.TYPE_ROOM_SHOW);
                } else {
                    onEnterRoomFail(ERROR_CODE_DEFAULT, "进房初始化失败");
                }
            } else {
                onEnterRoomFail(ERROR_CODE_DEFAULT, "进房失败 roomInfo is null");
            }
        }
        release();
    }

    private void onEnterRoomFail(int code, String error) {
        LogUtil.i(AvRoomDataManager.TAG, "onEnterRoomFail ---> enterRoom fail code =" + code + " error =" + error);
        dismissDialog();
        Context activityContext = getActivityContext();
        if (activityContext != null) {
            switch (code) {
                case IMError.IM_ROOM_PWD://请输入房间密码
                case IMError.IM_ROOM_RE_PWS://房间密码错误，请输入正确的密码
                    RoomFrameActivity.start(activityContext, roomUid, roomType, RoomFrameActivity.TYPE_PWD_SHOW);
                    break;
                case IMError.IM_ROOM_FINISHED://房间已结束
                case ERROR_CODE_VIDEO_LIVE_FINISH://房间已结束
                case IMError.IM_ROOM_NOT_EXIST://房间不存在
                    RoomFrameActivity.start(activityContext, roomUid, roomType, RoomFrameActivity.TYPE_ROOM_FINISH);
                    break;
                case IMError.USER_REAL_NAME_NEED_VERIFIED://"该功能需要进行实名验证"
                case IMError.USER_REAL_NAME_NEED_ROLE:// 实名认证
                    RoomFrameActivity.start(activityContext, roomUid, roomType, RoomFrameActivity.TYPE_GUIDE_SHOW);
                    break;
                case IMError.USER_REAL_NAME_AUDITING:// "您的实名认证信息正在审核中……"
                    toast(error);
                    break;
                case IMError.USER_REAL_NAME_NEED_PHONE://"该功能需要手机绑定"
                    break;
                case IMError.IM_ROOM_USE_WEB:// 请使用网页端开播
                case IMError.IM_USER_IN_ROOM_BLACK_LIST:// 用户在黑名单中
                    AvRoomDataManager.get().release();
                    toast(error);
                    break;
                case IMError.IM_JSON_PARSE:
                case ERROR_CODE_AUDIO_CHANNEL_INIT:
                case IMError.IM_LOGIN_FAIL://"im登录鉴权失败
                case IMError.IM_GET_USER_INFO_FAIL://获取用户信息失败
                case IMError.IM_GET_ROOM_INFO_FAIL://获取用户信息失败
                case IMError.IM_ROOM_SOCKET_ID_NOT_EXIST://用户房间socketId不存在
                    model.exitRoom(null);
                    toast(error);
                    break;
                case IMError.IM_ROOM_HAD_LIVE://您已经使开播不能进入他人房间
                    toast(error);
                    break;
                default:
                    AvRoomDataManager.get().release();
                    toast(error);
                    break;
            }
        }
        release();
        IMNetEaseManager.get().getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ENTER_ROOM_FAIL));
    }

    private boolean joinRtcEngineChannel(String key) {
        LogUtil.i(AvRoomDataManager.TAG, "enterRoom ---> joinRtcEngineChannel");
        RoomInfo curRoomInfo = AvRoomDataManager.get().getRoomInfo();
        if (curRoomInfo != null) {
            return RtcEngineManager.get().joinChannel(CoreManager.getCore(IAuthCore.class).getCurrentUid(), key, curRoomInfo);
        } else {
            return false;
        }
    }

    private void startWaitEnterRoomHandle() {
        waitHandler.postDelayed(this::showWaitEnterRoomDialog, ENTER_ROOM_WAIT_TIME_TO_SHOW_DIALOG);
    }

    private void finishWaitEnterRoomHandle() {
        waitHandler.removeCallbacksAndMessages(null);
    }

    /**
     * 弹出进房等待弹框
     */
    private void showWaitEnterRoomDialog() {
        Context activityContext = getActivityContext();
        if (activityContext != null) {
            getDialogManager(activityContext).showProgressDialog(activityContext, "正在进房...", true, true, dialog -> {
                if (!isEnterRoomFinish) {
                    //如果还没进房完成就结束了弹框（属于用户主动结束弹框，不愿意等待了），那么直接退出去
                    if (RoomInfo.ROOMTYPE_VIDEO_LIVE == roomType) {
                        //视频房没最小化，直接退房
                        model.exitRoom(null);
                    }
                    release();
                }
            });
        } else {
            release();
        }
    }

    private @Nullable
    Context getActivityContext() {
        if (weakReference != null && weakReference.get() != null) {
            return weakReference.get();
        } else {
            //如果activity被回收了，那么就尝试从堆栈里面拿
            Stack<Activity> activityStack = ChatApplicationLike.getApplicationLike().getActivityStack();
            if (activityStack != null && !activityStack.isEmpty()) {
                return activityStack.peek();
            }
        }
        return null;
    }

    private DialogManager getDialogManager(@NonNull Context context) {
        dialogManager = new DialogManager(context);
        return dialogManager;
    }

    private void dismissDialog() {
        if (dialogManager != null) {
            dialogManager.dismissDialog();
        }
    }

    private void toast(String toast) {
        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), toast, Toast.LENGTH_SHORT);
    }

    private void release() {
        isEnterRoomFinish = true;
        if (weakReference != null) {
            weakReference.clear();
            weakReference = null;
        }
        dismissDialog();
        dialogManager = null;
        finishWaitEnterRoomHandle();
    }
}
