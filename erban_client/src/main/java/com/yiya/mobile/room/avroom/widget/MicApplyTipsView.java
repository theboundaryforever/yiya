package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.makeramen.roundedimageview.RoundedImageView;
import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/22.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MicApplyTipsView extends ConstraintLayout {

    @BindView(R.id.iv_avatar)
    RoundedImageView ivAvatar;

    public MicApplyTipsView(Context context) {
        this(context, null);
    }

    public MicApplyTipsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MicApplyTipsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.layout_mic_apply_tips, this);
        ButterKnife.bind(this);
    }

    public void setAvatar(String avatar) {
        ImageLoadUtils.loadAvatar(getContext(), avatar, ivAvatar);
    }
}
