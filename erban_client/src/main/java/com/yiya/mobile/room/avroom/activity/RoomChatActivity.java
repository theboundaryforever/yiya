package com.yiya.mobile.room.avroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.presenter.find.FindSquarePresenter;
import com.yiya.mobile.presenter.find.IFindSquareView;
import com.yiya.mobile.room.avroom.fragment.PrivateChatFragment;
import com.yiya.mobile.room.avroom.fragment.RoomChatFragment;
import com.netease.nim.uikit.session.constant.Extras;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

/**
 * 文件描述：
 * 房间聊天相关功能模块入口：私聊
 *
 * @auther：zwk
 * @data：2019/1/2
 */
@CreatePresenter(FindSquarePresenter.class)
public class RoomChatActivity extends BaseMvpActivity<IFindSquareView, FindSquarePresenter> implements IFindSquareView {
    private FragmentManager fm;
    private RoomChatFragment chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_chat);
        initView();
    }

    public static void startDialogActivity(Context mContext) {
        Intent intent = new Intent(mContext, RoomChatActivity.class);
        mContext.startActivity(intent);
    }

    public static void startActivityWithSquare(Context context) {
        Intent intent = new Intent(context, RoomChatActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(RoomChatFragment.SHOW_SQUARE, true);
        bundle.putBoolean(RoomChatFragment.CHARM_LEVEL, true);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    private void initView() {
        setSwipeBackEnable(false);
        findViewById(R.id.title).setVisibility(View.GONE);
        //窗口对齐屏幕宽度
        Window win = this.getWindow();
        win.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = win.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = DisplayUtils.dip2px(this, 340);
        //设置对话框在底部
        lp.gravity = Gravity.BOTTOM;
        win.setAttributes(lp);
        chat = new RoomChatFragment();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            chat.setArguments(bundle);
        }

        fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fl_room_chat_container, chat);
        ft.commitAllowingStateLoss();

        IMNetEaseManager.get().subscribeChatRoomEventObservable(this::receiveRoomEvent, this);
    }

    /**
     * 接收房间事件
     */
    public void receiveRoomEvent(RoomEvent roomEvent) {
        if (roomEvent != null && roomEvent.getEvent() == RoomEvent.ROOM_EXIT) {
            finish();
        }
    }

    /**
     * 切换到私聊的frgment页面
     *
     * @param sessionId
     */
    public void showPrivateChat(String sessionId) {
        if (fm != null) {
            FragmentTransaction ft = fm.beginTransaction();
            PrivateChatFragment privateChatFragment = new PrivateChatFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Extras.EXTRA_ACCOUNT, sessionId);
            privateChatFragment.setArguments(bundle);
            ft.add(R.id.fl_room_chat_container, privateChatFragment);
            ft.addToBackStack(null);
            if (chat != null) {
                ft.hide(chat);
            }
            ft.commitAllowingStateLoss();
        }
    }

    /**
     * 返回上一级fragment页面
     */
    public void backToMainChatList() {
        // 不能在 saveState时调用，否则Can not perform this action after onSaveInstanceState
        if (fm != null && !fm.isStateSaved()) {
            try {//if (mNoTransactionsBecause != null) {throw new IllegalStateException("Can not perform this action inside of " + mNoTransactionsBecause);}
                fm.popBackStackImmediate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
