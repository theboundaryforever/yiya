package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.netease.nim.uikit.glide.GlideApp;
import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGADynamicEntity;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

import lombok.AllArgsConstructor;
import lombok.Data;

public class HeadLineStarNoticeView extends RelativeLayout implements SVGACallback {

    public static final String TAG = "ScreenNoticeView";

    private SVGAImageView svgaScreenNotice;// 通知svga

    private boolean isAnim = false;//门槛svga 是否在播放

    private Context mContext;

    private LinkedList<NoticeBean> giftVoInfos = new LinkedList<>();// 本地缓存

    public HeadLineStarNoticeView(Context context) {
        super(context);
        init(context);
    }

    public HeadLineStarNoticeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HeadLineStarNoticeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_head_line_start_notice, this, true);

        giftVoInfos.clear();

        mContext = context;

        svgaScreenNotice = (SVGAImageView) findViewById(R.id.svga_screen_notice);

        svgaScreenNotice.setCallback(this);
    }

    public boolean isAnim() {
        return isAnim;
    }

    public void setAnim(boolean anim) {
        isAnim = anim;
    }

    public void setupView(String source, String key, SpannableStringBuilder ssb, String url, String urlKey) {
        giftVoInfos.addLast(new NoticeBean(source, key ,ssb, url, urlKey));
        try {
            draw();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void draw() {
        if (isAnim || ListUtils.isListEmpty(giftVoInfos)) {
            return;
        }

        NoticeBean bean = giftVoInfos.removeFirst();
        if (bean == null) {
            isAnim = false;
            return;
        }

        SVGAParser parser = new SVGAParser(getContext());
        parser.decodeFromAssets(bean.getAssetsSource(), new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                SVGADynamicEntity dynamicEntity = drawable.getDynamicItem();

                if (bean.getUrl() != null){
                    GlideApp.with(mContext)
                            .asBitmap()
                            .load(bean.getUrl())
                            .apply(RequestOptions.circleCropTransform())
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                    //头像
                                    if (resource != null) {
                                        dynamicEntity.setDynamicImage(resource, bean.getUrlKey());
                                    }

                                    TextPaint normalTextPaint = new TextPaint();
                                    normalTextPaint.setColor(Color.WHITE);
                                    normalTextPaint.setTextSize(20);

                                    dynamicEntity.setDynamicText(new StaticLayout(
                                            bean.getSsb(),
                                            normalTextPaint,
                                            Integer.MAX_VALUE,
                                            Layout.Alignment.ALIGN_NORMAL,
                                            1,
                                            0,
                                            false
                                    ), bean.getKey());

                                    svgaScreenNotice.setLoops(1);
                                    svgaScreenNotice.setImageDrawable(drawable);
                                    svgaScreenNotice.setClearsAfterStop(true);
                                    svgaScreenNotice.startAnimation();

                                    isAnim = true;
                                }
                            });
                }
            }

            @Override
            public void onError() {

            }

        });
    }

    @Override
    public void onFinished() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onRepeat() {

    }

    @Override
    public void onStep(int i, double v) {

    }

    @Data
    @AllArgsConstructor
    private class NoticeBean {
        private String assetsSource;
        private String key;
        private SpannableStringBuilder ssb;

        private String url;
        private String urlKey;
    }
}
