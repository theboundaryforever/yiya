package com.yiya.mobile.room.avroom.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.room.avroom.adapter.OnlineUserAdapter;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.common.util.sys.NetworkUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.HomePartyUserListPresenter;
import com.tongdaxing.xchat_core.room.view.IHomePartyUserListView;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>  在线用户列表 </p>
 *
 * @author Administrator
 * @date 2017/12/4
 */

@CreatePresenter(HomePartyUserListPresenter.class)
public class OnlineUserFragment extends BaseMvpFragment<IHomePartyUserListView, HomePartyUserListPresenter> implements
        BaseQuickAdapter.OnItemClickListener, IHomePartyUserListView, OnlineUserAdapter.OnRoomOnlineNumberChangeListener,
        BaseQuickAdapter.OnItemChildClickListener {

    private RecyclerView mRecyclerView;
    private SmartRefreshLayout mRefreshLayout;

    private OnlineUserAdapter mOnlineUserAdapter;
    private int mPage = Constants.PAGE_START;
    private OnLineUserCallback onLineUserCallback;
    private boolean isVideoRoom = false;


    @Override
    public void onDestroy() {
        if (mOnlineUserAdapter != null) {
            mOnlineUserAdapter.release();
        }
        super.onDestroy();
    }

    @Override
    public void onFindViews() {
        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mRefreshLayout = mView.findViewById(R.id.refresh_layout);
    }

    @Override
    public void onSetListener() {
        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(mContext)) {
                    mRefreshLayout.finishLoadMore();
                    return;
                }
                List<OnlineChatMember> data = mOnlineUserAdapter.getData();
                if (ListUtils.isListEmpty(data)) {
                    mRefreshLayout.finishLoadMore();
                    return;
                }
                loadData(data.size(), mPage + 1);
            }

            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                if (!NetworkUtil.isNetAvailable(mContext)) {
                    mRefreshLayout.finishRefresh();
                    return;
                }
                firstLoad();
            }
        });
    }

    private List<OnlineChatMember> fitUser(List<OnlineChatMember> user) {
        if (ListUtils.isListEmpty(user)) {
            return user;
        }
        List<OnlineChatMember> data = new ArrayList<>();
        for (int i = 0; i < user.size(); i++) {
            if (user.get(i).isOnMic) {
                data.add(user.get(i));
            }
        }
        return data;
    }

    @Override
    public void initiate() {
        if (getArguments() != null) {
            isVideoRoom = getArguments().getBoolean("isVideoRoom", false);
        }
        mRefreshLayout.setEnableOverScrollDrag(false);
        mRefreshLayout.setEnableOverScrollBounce(false);
        mRefreshLayout.setEnableHeaderTranslationContent(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mOnlineUserAdapter = new OnlineUserAdapter(isVideoRoom);
        mRecyclerView.setAdapter(mOnlineUserAdapter);
        mOnlineUserAdapter.setOnItemClickListener(this);
        mOnlineUserAdapter.setOnItemChildClickListener(this);
        mOnlineUserAdapter.setListener(this);
    }


    @Override
    public int getRootLayoutId() {
        return R.layout.common_refresh_recycler_view;
    }

    public void firstLoad() {
        loadData(0, Constants.PAGE_START);
    }

    private void loadData(int index, int page) {
        getMvpPresenter().requestChatMemberByIndex(page, index,
                mOnlineUserAdapter == null ? null : mOnlineUserAdapter.getData());
    }

    @Override
    public synchronized void onRequestChatMemberByPageSuccess(List<OnlineChatMember> chatRoomMemberList, int page) {
        mPage = page;
        if (ListUtils.isNotEmpty(chatRoomMemberList)) {
            mOnlineUserAdapter.setNewData(chatRoomMemberList);
            if (mPage == Constants.PAGE_START) {
                mRefreshLayout.finishRefresh();
            } else {
                mRefreshLayout.finishLoadMore();
            }
        } else {
            if (mPage == Constants.PAGE_START) {
                mRefreshLayout.finishRefresh();
            } else {
                mRefreshLayout.finishLoadMore();
            }
        }
    }

    @Override
    public void onRequestChatMemberByPageFail(String errorStr, int page) {
        LogUtil.w("room", "getOnlineMembers fail page=" + page + " errorStr=" + errorStr);
        if (page == Constants.PAGE_START) {
            mRefreshLayout.finishRefresh();
        } else {
            mRefreshLayout.finishLoadMore();
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int position) {
        if (onLineUserCallback != null) {
            onLineUserCallback.onDismiss();
        }
        RoomInfo currentRoom = AvRoomDataManager.get().getRoomInfo();
        if (currentRoom != null) {
            List<OnlineChatMember> chatRoomMembers = mOnlineUserAdapter.getData();
            if (ListUtils.isListEmpty(chatRoomMembers)) {
                return;
            }
            if (chatRoomMembers.size() <= position) {
                return;
            }
            OnlineChatMember chatRoomMember = chatRoomMembers.get(position);
            if (onlineItemClick != null) {
                onlineItemClick.onItemClick(chatRoomMember);
            }
        }
    }

    @Override
    public void onMemberIn(String account, List<OnlineChatMember> dataList, ChatRoomMessage chatRoomMessage) {
        getMvpPresenter().onMemberInRefreshData(account, dataList, mPage, chatRoomMessage);
    }

    @Override
    public void onMemberDownUpMic(String account, boolean isUpMic, List<OnlineChatMember> dataList) {
        getMvpPresenter().onMemberDownUpMic(account, isUpMic, dataList, mPage);
    }

    @Override
    public void onUpdateMemberManager(String account, boolean isRemoveManager, List<OnlineChatMember> dataList) {
        getMvpPresenter().onUpdateMemberManager(account, dataList, isRemoveManager, mPage);
    }

    @Override
    public void addMemberBlack() {
    }

    public void setOnLineUserCallback(OnLineUserCallback onLineUserCallback) {
        this.onLineUserCallback = onLineUserCallback;
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        if (onLineUserCallback != null) {
            onLineUserCallback.onDismiss();
        }
        RoomInfo currentRoom = AvRoomDataManager.get().getRoomInfo();
        if (currentRoom != null) {
            List<OnlineChatMember> chatRoomMembers = mOnlineUserAdapter.getData();
            if (ListUtils.isListEmpty(chatRoomMembers)) {
                return;
            }
            if (chatRoomMembers.size() <= position) {
                return;
            }
            OnlineChatMember chatRoomMember = chatRoomMembers.get(position);
            if (onlineItemClick != null) {
                onlineItemClick.onHoldMicroClick(chatRoomMember);
            }
        }
    }

    public interface OnLineUserCallback {
        void onDismiss();
    }

    private OnlineItemClick onlineItemClick;

    public void setOnlineItemClick(OnlineItemClick onlineItemClick) {
        this.onlineItemClick = onlineItemClick;
    }


    public interface OnlineItemClick {

        default void onItemClick(OnlineChatMember chatRoomMember) {

        }

        default void onHoldMicroClick(OnlineChatMember chatRoomMember) {

        }
    }
}
