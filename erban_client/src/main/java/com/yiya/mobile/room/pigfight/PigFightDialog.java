package com.yiya.mobile.room.pigfight;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.yiya.mobile.base.fragment.BaseMvpDialogFragment;
import com.yiya.mobile.presenter.pigfight.IPigFightView;
import com.yiya.mobile.room.pigfight.dialog.PigFightCommonListDialog;
import com.yiya.mobile.room.pigfight.dialog.PigFightRecordDialog;
import com.yiya.mobile.room.presenter.PigFightPresenter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.PigRecordInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author zdw
 * @date 2019/8/12
 * @describe 猪猪大作战
 */

@CreatePresenter(PigFightPresenter.class)
public class PigFightDialog extends BaseMvpDialogFragment<IPigFightView, PigFightPresenter> implements IPigFightView {
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.iv_play)
    ImageView ivPlay;
    @BindView(R.id.iv_rule)
    ImageView ivRule;
    @BindView(R.id.iv_enter)
    ImageView ivEnter;

    private Unbinder unbinder;

    public static PigFightDialog newInstance() {
        return new PigFightDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View rootView = inflater.inflate(R.layout.dialog_pig_fight, window.findViewById(android.R.id.content), false);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setWindowAnimations(R.style.WindowBottomAnimationStyle);
        window.setGravity(Gravity.BOTTOM);

        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public int getRootLayoutId() {
        return 0;
    }


    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {
        ivClose.setOnClickListener(v -> dismiss());
        ivPlay.setOnClickListener(v -> {
            getMvpPresenter().startPlay();
            dismiss();
        });
        ivRule.setOnClickListener(v -> {
            PigFightCommonListDialog.newInstance(PigFightCommonListDialog.TYPE_RULE_LIST)
                    .show(getChildFragmentManager(), null);
        });
        ivEnter.setOnClickListener(v -> getMvpPresenter().getPigFightRecordList());
    }

    @Override
    public void initiate() {
        if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
            ivPlay.setVisibility(View.VISIBLE);
        } else {
            ivPlay.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void finish() {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void showPlaySuccess(List<?> infos) {

    }

    @Override
    public void showPlayFail(String error) {
        toast(error);
    }

    /**
     * 猪猪大作战记录
     *
     * @param list
     */
    @Override
    public void showPigFightRecord(List<PigRecordInfo> list) {
        PigFightRecordDialog.newInstance(list)
                .show(getChildFragmentManager(), null);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }
}
