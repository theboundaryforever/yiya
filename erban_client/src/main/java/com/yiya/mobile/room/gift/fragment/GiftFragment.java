package com.yiya.mobile.room.gift.fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.yiya.mobile.base.fragment.BaseFragment;
import com.yiya.mobile.room.gift.adapter.GiftAdapter;

import java.util.List;

import butterknife.BindView;

import static com.yiya.mobile.room.gift.GiftSelector.NORMALPAGE;
import static com.yiya.mobile.room.gift.GiftSelector.PACKETPAGE;

/**
 * @author Zhangsongzhou
 * @date 2019/7/15
 */
public class GiftFragment extends BaseFragment {
    public interface OnItemClickListener {
        void onItemClick(int type, int position, GiftInfo info);
    }


    private OnItemClickListener mItemClickListener;

    private int mType = 0;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.ll_gift_empty)
    LinearLayout llGiftEmpty;
    private List<GiftInfo> normalGiftInfos;
    private GiftAdapter mAdapter;

    /**
     * R.layout.fragment_gift_normal
     *
     * @return
     */
    @Override
    public int getRootLayoutId() {
        return getArguments().getInt("viewId");
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    public void setItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    @Override
    public void initiate() {
        mType = getArguments().getInt("type");
        mAdapter = new GiftAdapter(mContext);

        refreshGiftList();
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
        mRecyclerView.setAdapter(mAdapter);


        mAdapter.setIndex(0);

        mAdapter.setOnItemClickListener(new GiftAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mAdapter.setIndex(position);
                mAdapter.notifyDataSetChanged();
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(mType, position, mAdapter.getIndexGift());
                }
            }
        });
    }

    private void refreshGiftList() {
        switch (mType) {
            case NORMALPAGE:
                normalGiftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType2And4();
                break;
            case PACKETPAGE:
                normalGiftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(3);
                break;
//            case MAGICPAGE:
//                normalGiftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(IGiftCore.MAGIC_LIST);
//                break;
//            case LUCKYPAGE:
//                normalGiftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(IGiftCore.LUCKY_LIST);
//                break;
        }
        llGiftEmpty.setVisibility(ListUtils.isListEmpty(normalGiftInfos) ? View.VISIBLE : View.GONE);
        mRecyclerView.setVisibility(ListUtils.isNotEmpty(normalGiftInfos) ? View.VISIBLE : View.GONE);
        mAdapter.setGiftInfoList(normalGiftInfos);
    }

    public GiftAdapter getAdapter() {
        return mAdapter;
    }

    public List<GiftInfo> getNormalGiftInfos() {
        return normalGiftInfos;
    }


    public void updateUI() {
        refreshGiftList();
        mAdapter.notifyDataSetChanged();
    }

}