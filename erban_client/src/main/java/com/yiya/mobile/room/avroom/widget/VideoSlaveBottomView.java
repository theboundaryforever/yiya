package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yiya.mobile.room.avroom.other.BottomViewListenerWrapper;
import com.opensource.svgaplayer.SVGAImageView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoSlaveBottomView extends BaseBottomView {
    private BottomViewListenerWrapper wrapper;

    @BindView(R.id.rl_room_operate)
    RelativeLayout rlRoomOperate;
    @BindView(R.id.icon_room_send_msg)
    LinearLayout llSendMsg;
    @BindView(R.id.iv_room_send_msg)
    TextView tvSendMsg;
    @BindView(R.id.icon_room_send_gift)
    ImageView ivSendGift;
    @BindView(R.id.icon_room_share)
    ImageView ivShare;
//    @BindView(R.id.iv_room_operate)
//    ImageView ivRoomOperate;
//    @BindView(R.id.rl_room_msg)
//    RelativeLayout rlRoomMsg;
    @BindView(R.id.iv_room_msg_mark)
    ImageView ivMsgMark;

    @BindView(R.id.rl_room_lian_micro)
    View lianMicroRootView;
    @BindView(R.id.iv_room_lian_micro_new)
    ImageView lianMicroNewIconIv;
    @BindView(R.id.iv_room_lian_micro)
    ImageView lianMicroIv;
    @BindView(R.id.svga_room_lian_micro)
    SVGAImageView lianMicroAnimView;

    public VideoSlaveBottomView(Context context) {
        super(context);
        init();
    }

    public VideoSlaveBottomView(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    public VideoSlaveBottomView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init();
    }

    private void init() {
        View inflate = inflate(getContext(), R.layout.layout_room_video_slave_bottom_view, this);
        ButterKnife.bind(inflate);
        setGravity(Gravity.CENTER_VERTICAL);
        setOrientation(HORIZONTAL);

        llSendMsg.setOnClickListener(v -> {
            if (wrapper != null) {
                wrapper.onSendMsgBtnClick();
            }
        });
        ivSendGift.setOnClickListener(v -> {
            if (wrapper != null) {
                wrapper.onSendGiftBtnClick();
            }
        });
        ivShare.setOnClickListener(v -> {
            if (wrapper != null) {
                wrapper.onShareBtnClick();
            }
        });
//        rlRoomMsg.setOnClickListener(v -> {
//            if (wrapper != null) {
//                wrapper.onMsgBtnClick();
//            }
//        });
        rlRoomOperate.setOnClickListener(v -> {
            if (wrapper != null) {
                wrapper.onMoreOperateBtnClick();
            }
        });
        lianMicroRootView.setOnClickListener(v -> {
            if (wrapper != null) {
                wrapper.onLianMicroBtnClick();
            }
        });

        RoomInfo currentRoomInfo = AvRoomDataManager.get().getRoomInfo();
        if (currentRoomInfo != null) {
            //公屏聊天开关 0开
            if (currentRoomInfo.getPublicChatSwitch() == 0) {
                setInputMsgBtnEnable(true);
            } else {
                setInputMsgBtnEnable(false);
            }
        }
    }

    public void setBottomViewCommonListener(BottomViewListenerWrapper wrapper) {
        this.wrapper = wrapper;
    }

    /**
     * 新私聊消息提示
     *
     * @param isShow true 显示 false不显示
     */
    public void showMsgMark(boolean isShow) {
        if (ivMsgMark != null)
            ivMsgMark.setVisibility(isShow ? VISIBLE : GONE);
    }

    /**
     * 配置连麦按钮
     *
     * @param isNewEnable         new图标是否启用
     * @param isLianMicroSelected 连麦按钮是否选中
     */
    public void setupLianMicroView(boolean isNewEnable, boolean isLianMicroSelected, OnClickListener onLianMicroClickListener) {
        lianMicroNewIconIv.setVisibility(isNewEnable ? VISIBLE : GONE);

        if (isLianMicroSelected) {
            lianMicroIv.setSelected(true);
            lianMicroAnimView.startAnimation();
        } else {
            lianMicroIv.setSelected(false);
            lianMicroAnimView.stopAnimation();
        }
        lianMicroRootView.setOnClickListener(onLianMicroClickListener);
    }
}
