package com.yiya.mobile.room.avroom.adapter;

import android.text.TextUtils;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo.MicroApplyInfo;

/**
 * ProjectName:
 * Description:视频房连麦列表adapter
 * Created by BlackWhirlwind on 2019/6/6.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MicroApplyListAdapter extends BaseMultiItemQuickAdapter<MicroApplyInfo, BaseViewHolder> {

    public MicroApplyListAdapter() {
        super(null);
        addItemType(MicroApplyInfo.MICRO_APPLY_NORMAL, R.layout.item_micro_apply_normal);
        addItemType(MicroApplyInfo.MICRO_APPLY_CONNECTING, R.layout.item_micro_apply_connecting);
    }

    @Override
    protected void convert(BaseViewHolder helper, MicroApplyInfo item) {
        if (!TextUtils.isEmpty(item.getAvatar())) {
            ImageLoadUtils.loadAvatar(mContext, item.getAvatar(), helper.getView(R.id.civ_avatar));
        }
        if (!TextUtils.isEmpty(item.getNick())) {
            helper.setText(R.id.tv_nick, item.getNick());
        }
        switch (item.getItemType()) {
            case MicroApplyInfo.MICRO_APPLY_CONNECTING:
                //互动中
                helper.addOnClickListener(R.id.iv_shut_down);
                break;
            case MicroApplyInfo.MICRO_APPLY_NORMAL:
                //待处理
                helper.setText(R.id.tv_status, String.format("想跟你%s连麦！", item.isAudioType() ? "语音" : "视频"))
                        .setImageResource(R.id.iv_type, item.isAudioType() ? R.mipmap.icon_audio : R.mipmap.icon_video)
                        .addOnClickListener(R.id.iv_accept).addOnClickListener(R.id.iv_reject);
                break;
        }
    }
}
