package com.yiya.mobile.room.avroom.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.yiya.mobile.room.avroom.widget.LuckyBoxView;
import com.yiya.mobile.room.avroom.widget.PlantCucumberView;
import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.tongdaxing.xchat_core.room.bean.BannerEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/4.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class BannerEventAdapter extends StaticPagerAdapter {

    private List<BannerEvent> list;
    private LuckyBoxView.OnLuckyBoxViewClickListener mLuckyBoxCallback;
    private PlantCucumberView.Callback mPlantCucumberCallback;

    public BannerEventAdapter(List<BannerEvent> list) {
        this.list = list == null ? new ArrayList<>() : list;
    }

    @Override
    public View getView(ViewGroup container, int position) {
        BannerEvent bannerEvent = list.get(position);
        switch (bannerEvent.getItemType()) {
            case BannerEvent.TYPE_LUCKY_BOX:
                //超级宝箱
                LuckyBoxView luckyBoxView = new LuckyBoxView(container.getContext());
                luckyBoxView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                if (mLuckyBoxCallback != null) {
                    luckyBoxView.setOnLuckyBoxViewClickListener(mLuckyBoxCallback);
                }
                luckyBoxView.setData(bannerEvent.getRoomLuckyBoxInfo());
                return luckyBoxView;
            case BannerEvent.TYPE_PLANT_CUCUMBER:
                //种瓜
                PlantCucumberView plantCucumberView = new PlantCucumberView(container.getContext());
                plantCucumberView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                if (mPlantCucumberCallback != null) {
                    plantCucumberView.setOnCallback(mPlantCucumberCallback);
                }
                plantCucumberView.setData(bannerEvent.getRoomPlantCucumberInfo());
                return plantCucumberView;
            default:
                return null;
        }
    }

    public void setOnLuckyBoxCallback(LuckyBoxView.OnLuckyBoxViewClickListener callback) {
        mLuckyBoxCallback = callback;
    }

    public void setOnPlantCucumberCallback(PlantCucumberView.Callback callback) {
        mPlantCucumberCallback = callback;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public List<BannerEvent> getList() {
        return list;
    }
}
