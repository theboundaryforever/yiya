package com.yiya.mobile.room.egg.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/19.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantRewardRecordAdapter extends BaseQuickAdapter<EggGiftInfo, BaseViewHolder> {

    public PlantRewardRecordAdapter(@Nullable List<EggGiftInfo> data) {
        super(R.layout.item_plant_bean_reward_record, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, EggGiftInfo item) {
        if (item == null) {
            return;
        }
        ImageLoadUtils.loadImage(mContext, item.getPicUrl(), helper.getView(R.id.iv_pic));
        helper.setText(R.id.tv_num, "x" + item.getGiftNum());
    }
}
