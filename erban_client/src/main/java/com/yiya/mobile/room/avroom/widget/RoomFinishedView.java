package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yiya.mobile.ui.home.adpater.LiveStreamRecyclerAdapter;
import com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 房间直播结束后的view
 *
 * @author zeda
 */
public class RoomFinishedView extends RelativeLayout {

    private OnViewClickListener onViewClickListener;

    private TextView nickTv;
    private TextView tv_uid;
    private ImageView avatarIv;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private Context mContext;
    private List<HomeLiveStreamFragment.LiveStreamInfo> mData = new ArrayList<>();
    private LiveStreamRecyclerAdapter mAdapter;
    private GridLayoutManager mLayoutManager;

    public RoomFinishedView(@NonNull Context context) {
        this(context, null);
        init(context);
    }

    public RoomFinishedView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
        init(context);
    }

    public RoomFinishedView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        this.mContext = context;

        inflate(getContext(), R.layout.layout_room_live_finish, this);

        avatarIv = findViewById(R.id.avatar);
        nickTv = findViewById(R.id.nick);
        tv_uid = findViewById(R.id.tv_uid);

        findViewById(R.id.home_page_btn).setOnClickListener(v -> {
            if (onViewClickListener != null) {
                onViewClickListener.onHomeBtnClick();
            }
        });

//        mLayoutManager = new GridLayoutManager(mContext, 2);
//        mAdapter = new LiveStreamRecyclerAdapter(mContext, null);
//        mAdapter.setSpanSizeLookup(new com.chad.library.adapter.base.BaseQuickAdapter.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
//                int value = 1;
//                try {
//                    if (mAdapter.getData().get(position).getItemType() == HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_BANNER) {
//                        value = 2;
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                return value;
//            }
//        });
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.setAdapter(mAdapter);
//
//        mAdapter.setOnItemClickListener((adapter, view, position) -> {
//            HomeLiveStreamFragment.LiveStreamInfo streamInfo = mAdapter.getItem(position);
//            if (streamInfo != null) {
//                if (streamInfo.getItemType() == ITEM_TYPE_LIVE) {
//                    RoomFrameActivity.start(mContext, streamInfo.getRoomListBean().getUid(), streamInfo.getRoomListBean().getType());
//                }
//            }
//        });

//        if(mContext instanceof RoomFrameActivity && ((RoomFrameActivity) mContext).roomFragment != null){
//            if(((RoomFrameActivity) mContext).roomFragment instanceof VideoRoomDetailFragment){
//                ((RoomFrameActivity) mContext).roomFragment.getMvpPresenter()
//            }
//        }



    }

    public void setOnViewClickListener(OnViewClickListener onViewClickListener) {
        this.onViewClickListener = onViewClickListener;
    }

    public void setupView(UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }
        ImageLoadUtils.loadCircleImage(getContext(), userInfo.getAvatar(), avatarIv, R.drawable.nim_avatar_default);
        nickTv.setText(userInfo.getNick());
        tv_uid.setText(String.valueOf(userInfo.getUid()));
    }

    public interface OnViewClickListener {
        void onHomeBtnClick();
    }
}
