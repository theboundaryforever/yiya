package com.yiya.mobile.room.avroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.donkingliang.labels.LabelsView;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.kyleduo.switchbutton.SwitchButton;
import com.netease.nimlib.sdk.StatusCode;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.RoomSettingPresenter;
import com.tongdaxing.xchat_core.room.view.IRoomSettingView;
import com.tongdaxing.xchat_core.utils.StringUtils;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenran
 * @date 2017/9/26
 */
@CreatePresenter(RoomSettingPresenter.class)
public class RoomSettingActivity extends BaseMvpActivity<IRoomSettingView, RoomSettingPresenter>
        implements LabelsView.OnLabelClickListener, View.OnClickListener, IRoomSettingView, CompoundButton.OnCheckedChangeListener {
    private EditText nameEdit;
    private EditText topicEdit;
    private EditText pwdEdit;
    private LabelsView labelsView;
    private RoomInfo roomInfo;
    private List<String> labels;
    private String selectLabel;
    private RelativeLayout managerLayout;
    private RelativeLayout blackLayout;
    private RelativeLayout rlRoomPlay;
    private LinearLayout mLabelLayout;

    private TabInfo mSelectTabInfo;
    private List<TabInfo> mTabInfoList;
    private RelativeLayout bgLayout;
    private String mBackPic;
    private String mBackPicUrl = "";//新增字段网络背景图片地址
    //    private TextView bgName;
    private RelativeLayout topicLayout;//房间话题
    private RelativeLayout smallEffect;//小礼物特效
    private SwitchButton sbSetting;//小礼物特效开关
    private SwitchButton sbBlockGiftMsg;//公屏礼物信息开关
    private int changeGiftEffect = -1;

    public static void start(Context context, RoomInfo roomInfo) {
        Intent intent = new Intent(context, RoomSettingActivity.class);
        intent.putExtra("roomInfo", roomInfo);
        context.startActivity(intent);
    }

    public static void start(Context context, RoomInfo roomInfo, int isPermitRoom) {
        Intent intent = new Intent(context, RoomSettingActivity.class);
        intent.putExtra("roomInfo", roomInfo);
        intent.putExtra("isPermitRoom", isPermitRoom);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_setting);
        GrowingIO.getInstance().setPageName(this, "语音-房间设置页");
        initTitleBar(getString(R.string.room_setting));
        ImageView ivSave = new ImageView(this);
        ivSave.setImageResource(R.drawable.bg_room_setting_save);
        ivSave.setOnClickListener(v -> save());
//        TextView tvRight = new TextView(this);
//        tvRight.setText("保存");
//        tvRight.setTextSize(17);
//        tvRight.setTextColor(ContextCompat.getColor(this, R.color.color_theme));
//        tvRight.setOnClickListener(v -> save());
        mTitleBar.mRightLayout.addView(ivSave);
        roomInfo = (RoomInfo) getIntent().getSerializableExtra("roomInfo");
        int isPermitRoom = getIntent().getIntExtra("isPermitRoom", -2);
        if (isPermitRoom != -2) {
            roomInfo.setIsPermitRoom(isPermitRoom);
        }
        //初始化界面要在获取数据之后
        initView();

        mBackPic = roomInfo.getBackPic() + "";
//        bgName.setText(BgTypeHelper.getBgName(mBackPic));

        getMvpPresenter().requestTagAll();
        if (!AvRoomDataManager.get().isRoomOwner(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()))) {
            managerLayout.setVisibility(View.GONE);
        } else {
            managerLayout.setVisibility(View.VISIBLE);
        }
        labelsView.setOnLabelClickListener(this);
    }

    /**
     * 隐藏虚拟键盘
     */
    public void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && imm.isActive()) {
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);

        }
    }

    private void initView() {
        nameEdit = (EditText) findViewById(R.id.name_edit);
        GrowingIO.getInstance().trackEditText(nameEdit);
        topicEdit = (EditText) findViewById(R.id.topic_edit);
        GrowingIO.getInstance().trackEditText(topicEdit);
        pwdEdit = (EditText) findViewById(R.id.pwd_edit);
        GrowingIO.getInstance().trackEditText(pwdEdit);
        labelsView = (LabelsView) findViewById(R.id.labels_view);
        managerLayout = (RelativeLayout) findViewById(R.id.manager_layout);
        bgLayout = (RelativeLayout) findViewById(R.id.bg_layout);
        blackLayout = (RelativeLayout) findViewById(R.id.black_layout);
        mLabelLayout = (LinearLayout) findViewById(R.id.label_layout);
//        bgName = (TextView) findViewById(R.id.tv_bg_name);
        topicLayout = (RelativeLayout) findViewById(R.id.topic_layout);
        rlRoomPlay = (RelativeLayout) findViewById(R.id.rl_room_tip);
        smallEffect = (RelativeLayout) findViewById(R.id.rl_small_gift_effect);
        sbSetting = (SwitchButton) findViewById(R.id.sb_small_gift_effect);
        sbBlockGiftMsg = (SwitchButton) findViewById(R.id.sb_block_gift_msg);
        changeGiftEffect = roomInfo.getGiftEffectSwitch();
        sbSetting.setChecked(roomInfo.getGiftEffectSwitch() == 0);
        sbSetting.setOnCheckedChangeListener(this);
        sbBlockGiftMsg.setChecked(!roomInfo.isBlockGiftMsg());
        rlRoomPlay.setOnClickListener(this);
        managerLayout.setOnClickListener(this);
        blackLayout.setOnClickListener(this);
        bgLayout.setOnClickListener(this);
        topicLayout.setOnClickListener(this);

        findViewById(R.id.tv_save).setOnClickListener(this);

        if (roomInfo != null) {
            nameEdit.setText(roomInfo.getTitle());
            topicEdit.setText(roomInfo.getRoomDesc());
            pwdEdit.setText(roomInfo.getRoomPwd());
            selectLabel = roomInfo.getRoomTag();
        }

        if (selectLabel == null) {
            selectLabel = "";
        }
    }

    private void save() {
        String name = null;
        String desc = null;
        String pwd = null;
        String label = null;
        String backPic = null;
        if (!nameEdit.getText().toString().equals(roomInfo.getTitle())) {
            name = nameEdit.getText().toString();
        }
        if (!topicEdit.getText().toString().equals(roomInfo.getRoomDesc())) {
            desc = topicEdit.getText().toString();
        }
        if (!pwdEdit.getText().toString().equals(roomInfo.getRoomPwd())) {
            pwd = pwdEdit.getText().toString();
        }
        if (!selectLabel.equals(roomInfo.getRoomTag())) {
            label = selectLabel;
        }
        if (!mBackPic.equals(roomInfo.getBackPic())) {
            backPic = mBackPic;
        }
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo != null) {
            if (name == null && desc == null && pwd == null && label == null && backPic == null && changeGiftEffect == roomInfo.getGiftEffectSwitch() && sbBlockGiftMsg.isChecked() != roomInfo.isBlockGiftMsg()) {
                toast("暂无更改");
            } else {
                hideKeyboard(nameEdit);
                getDialogManager().showProgressDialog(this, "请稍后...");
                int id = roomInfo.tagId;
                if (mSelectTabInfo != null) {
                    id = mSelectTabInfo.getId();
                }

                int isBlockGiftMsg = sbBlockGiftMsg.isChecked() ? 1 : 2;
                if (AvRoomDataManager.get().isRoomOwner()) {
                    getMvpPresenter().updateRoomInfo(roomInfo.getType(), name, desc, pwd, label, id, backPic, changeGiftEffect, isBlockGiftMsg);
                } else if (AvRoomDataManager.get().isRoomAdmin()) {
                    getMvpPresenter().updateByAdmin(roomInfo.getType(), roomInfo.getUid(), name, desc, pwd, label, id, backPic, changeGiftEffect, isBlockGiftMsg);
                }
            }
        }
    }

    @Override
    public void onLabelClick(View label, String labelText, int position) {
        if (!ListUtils.isListEmpty(mTabInfoList)) {
            mSelectTabInfo = mTabInfoList.get(position);
        }
        selectLabel = labelText;
        labelsView.setSelects(position);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.manager_layout:
                RoomManagerListActivity.start(this);
                break;
            case R.id.rl_room_tip://设置进入房间提示
                RoomPlayTipActivity.start(this);
                break;
            case R.id.tv_save:

                break;
            case R.id.black_layout:
                RoomBlackListActivity.start(this);
                break;
            case R.id.bg_layout:
                if (roomInfo == null)
                    return;
//                LogUtil.i("roomInfo.getIsPermitRoom()", roomInfo + "");
//                if (roomInfo.getIsPermitRoom() != 2) {
                Intent intent = new Intent(this, RoomSelectBgActivity.class);
                intent.putExtra("backPic", roomInfo.getBackPic());
                startActivityForResult(intent, 2);
//                } else {
//                    toast("只有牌照厅才支持更换背景哦");
//                }
                break;
            case R.id.topic_layout:
                RoomTopicActivity.start(this);
                break;
            default:
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            if (resultCode == 2) {
                if (data != null) {
                    String selectIndex = data.getStringExtra("selectIndex");
//                    String selectName = data.getStringExtra("selectName");
                    mBackPicUrl = data.getStringExtra("selectUrl");
                    mBackPic = StringUtils.isEmpty(selectIndex) ? "0" : selectIndex;
//                    bgName.setText(selectName);
//                    toast(mBackPic);
                }
            }
        }

    }

    @Override
    public void onResultRequestTagAllSuccess(List<TabInfo> tabInfoList) {
        mTabInfoList = tabInfoList;
//        if (ListUtils.isListEmpty(tabInfoList)) {
//            mLabelLayout.setVisibility(View.GONE);
//            return;
//        }
//        mLabelLayout.setVisibility(View.VISIBLE);

        labels = new ArrayList<>();
        for (TabInfo tabInfo : tabInfoList) {
            labels.add(tabInfo.getName());
        }

        labelsView.setLabels((ArrayList<String>) labels);
        if (roomInfo != null && !TextUtils.isEmpty(roomInfo.getRoomTag())) {
            if (labels.contains(roomInfo.getRoomTag())) {
                labelsView.setSelects(labels.indexOf(roomInfo.getRoomTag()));
            }
        }
    }

    @Override
    public void onResultRequestTagAllFail(String error) {
        toast(error);
    }

    @Override
    public void updateRoomInfoSuccess(RoomInfo roomInfo) {
        toast("保存成功");
        getDialogManager().dismissDialog();
        finish();
    }

    @Override
    public void updateRoomInfoFail(String error) {
        toast("保存失败");
        getDialogManager().dismissDialog();
        toast(error);
    }


    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        finish();
    }

    @Override
    protected void onReceiveChatRoomEvent(RoomEvent roomEvent) {
        super.onReceiveChatRoomEvent(roomEvent);
        int event = roomEvent.getEvent();
        switch (event) {
            case RoomEvent.KICK_OUT_ROOM:
                finish();
                break;
            case RoomEvent.ROOM_MANAGER_REMOVE:
                long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                if (!AvRoomDataManager.get().isRoomOwner(uid)) {
                    toast(R.string.remove_room_manager);
                    finish();
                }
            default:
        }
    }

    //小礼物特效开关
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked) {
            changeGiftEffect = 1;
        } else {
            changeGiftEffect = 0;
        }
    }
}
