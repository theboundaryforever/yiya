package com.yiya.mobile.room.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.ui.me.wallet.activity.MyWalletActivity;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.bean.HeadLineStarAnnounce;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

/**
 * @author zdw
 * @date 2019/9/2
 *
 * @describe 头条之星
 */

public class HeadLineStarDialog extends Dialog {

    private Context mContext;
    ImageView ivGift;
    TextView  tvGiftName;
    ImageView ivRecharge;
    FrameLayout flClose;

    public HeadLineStarDialog(Context context) {
        super(context, R.style.transDialog);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_head_start);

        ivGift = findViewById(R.id.iv_gift);
        tvGiftName = findViewById(R.id.tv_gift_name);
        ivRecharge = findViewById(R.id.iv_recharge);
        flClose = findViewById(R.id.fl_close);

        ivRecharge.setOnClickListener(v -> {
            MyWalletActivity.start(getContext(),1);
//            AvRoomDataManager.get().setDialogShow(false);
            dismiss();
        });

        flClose.setOnClickListener(v -> {
//            AvRoomDataManager.get().setDialogShow(false);
            dismiss();
        });
    }

    public void setData(HeadLineStarAnnounce announce){
        if (announce == null)return;
        ImageLoadUtils.loadImage(mContext, announce.getGiftPic(), ivGift, R.drawable.nim_avatar_default);
        tvGiftName.setText(StringUtils.isEmpty(announce.getGiftName()) ? "": announce.getGiftName());
    }

    @Override
    public void dismiss() {
        super.dismiss();
//        AvRoomDataManager.get().setDialogShow(false);
    }
}
