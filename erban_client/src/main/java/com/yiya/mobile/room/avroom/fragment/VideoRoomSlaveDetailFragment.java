package com.yiya.mobile.room.avroom.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.yiya.mobile.room.avroom.activity.RoomChatActivity;
import com.yiya.mobile.room.avroom.adapter.VideoRoomOnlineAdapter;
import com.yiya.mobile.room.avroom.other.ButtonItemFactory;
import com.yiya.mobile.room.avroom.widget.BaseBottomView;
import com.yiya.mobile.room.avroom.widget.BaseMicroView;
import com.yiya.mobile.room.avroom.widget.ComingMsgView;
import com.yiya.mobile.room.avroom.widget.GiftMsgView;
import com.yiya.mobile.room.avroom.widget.GiftView;
import com.yiya.mobile.room.avroom.widget.HeadLineStarNoticeView;
import com.yiya.mobile.room.avroom.widget.InputMsgView;
import com.yiya.mobile.room.avroom.widget.LuckyGiftView;
import com.yiya.mobile.room.avroom.widget.MessageView;
import com.yiya.mobile.room.avroom.widget.MicroUserInfoView;
import com.yiya.mobile.room.avroom.widget.ScreenNoticeView;
import com.yiya.mobile.room.avroom.widget.VideoMicroView;
import com.yiya.mobile.room.avroom.widget.VideoSlaveBottomView;
import com.yiya.mobile.room.avroom.widget.dialog.RoomFunctionDialog;
import com.yiya.mobile.room.gift.GiftSelector;
import com.yiya.mobile.room.presenter.VideoRoomDetailPresenter;
import com.yiya.mobile.room.widget.dialog.LianMicroDialogFactory;
import com.yiya.mobile.room.widget.dialog.ListDataDialog;
import com.yiya.mobile.ui.me.setting.activity.FeedbackActivity;
import com.yiya.mobile.ui.rank.activity.RankingListActivity;
import com.yiya.mobile.ui.widget.Banner;
import com.juxiao.library_utils.DisplayUtils;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGADynamicEntity;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.LianMicroStatusInfo;
import com.tongdaxing.xchat_core.room.bean.RoomFunctionEnum;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyTips;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SpUtils;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.yiya.mobile.room.avroom.widget.dialog.RoomFunctionModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

@CreatePresenter(VideoRoomDetailPresenter.class)
public class VideoRoomSlaveDetailFragment extends VideoRoomDetailFragment {
    @BindView(R.id.rl_Top)
    RelativeLayout rlTop;
    @BindView(R.id.micro_container)
    VideoMicroView microView;
    @BindView(R.id.message_view)
    MessageView messageView;
    @BindView(R.id.gift_view)
    GiftView giftView;
    @BindView(R.id.bottom_view)
    VideoSlaveBottomView bottomView;
    @BindView(R.id.iv_room_bg)
    ImageView roomBgIv;
    @BindView(R.id.iv_close)
    ImageView iv_close;
    @BindView(R.id.tv_follow)
    TextView tv_follow;
    @BindView(R.id.avatar)
    ImageView avatar;
    @BindView(R.id.rv_online_count)
    RecyclerView rvOnlineList;
    @BindView(R.id.input_layout)
    InputMsgView inputMsgView;
    @BindView(R.id.rl_room_operate)
    RelativeLayout moreOperateBtn;
    @BindView(R.id.tv_room_title)
    TextView tv_title;
    @BindView(R.id.tv_room_num)
    TextView tv_room_num;
    @BindView(R.id.ll_contribute)
    LinearLayout ll_contribute;
    @BindView(R.id.tv_contribute_num)
    TextView mReceiveSumTv;
    @BindView(R.id.miv_info)
    MicroUserInfoView microUserInfoView;
    @BindView(R.id.cmv_msg)
    ComingMsgView cmv_msg;
    @BindView(R.id.gift_selector)
    GiftSelector giftSelector;
    @BindView(R.id.icon_room_lottery_box)
    ImageView icon_room_lottery_box;
    @BindView(R.id.ic_room_banner)
    Banner banner;
    @BindView(R.id.lgLuckyView)
    LuckyGiftView lgLuckyView;
    @BindView(R.id.svga_lucky_box_activate)
    SVGAImageView svgaLuckyBoxActivate;
    @BindView(R.id.svga_lucky_tips)
    SVGAImageView svgaLuckyTips;
    @BindView(R.id.svga_lucky_room)
    SVGAImageView svgaLuckyRoom;
    @BindView(R.id.svga_lucky_gift_notice)
    ScreenNoticeView svgaLuckyGiftNotice;
    @BindView(R.id.svga_add_coin_notice)
    ScreenNoticeView svgaAddCoinNotice;
    @BindView(R.id.svga_head_line_start_limit_notice)
    HeadLineStarNoticeView svgaHeadLineStartLimitNotice;
    @BindView(R.id.ll_distance)
    LinearLayout llDistance;
    @BindView(R.id.tv_distance)
    TextView tvDistance;
    @BindView(R.id.svga_add_gold)
    SVGAImageView svgaAddGold;
    @BindView(R.id.iv_sd)
    ImageView ivSpecialDiscount;
    @BindView(R.id.banner_event)
    Banner bannerEvent;

    VideoRoomOnlineAdapter onlineAdapter;

    private BaseDialogFragment lianMicroApplyDialog;

    @NonNull
    @Override
    protected BaseMicroView getMicroView() {
        return microView;
    }

    @NonNull
    @Override
    protected MessageView getMessageView() {
        return messageView;
    }

    @NonNull
    @Override
    protected GiftView getGiftView() {
        return giftView;
    }

    @NonNull
    @Override
    protected BaseBottomView getBottomView() {
        return bottomView;
    }

    @NonNull
    @Override
    protected TextView getTitleView() {
        return tv_title;
    }

    @Nullable
    @Override
    protected GiftMsgView getGiftMsgView() {
        return null;
    }

    @NonNull
    @Override
    protected ComingMsgView getComingMsgView() {
        return cmv_msg;
    }

    @NonNull
    @Override
    protected ImageView getRoomBgView() {
        return roomBgIv;
    }

    @NonNull
    @Override
    protected GiftSelector getGiftSelector() {
        return giftSelector;
    }

    @NonNull
    @Override
    protected InputMsgView getInputMsgView() {
        return inputMsgView;
    }

    @NonNull
    @Override
    protected View getMoreOperateBtn() {
        return moreOperateBtn;
    }

    @NonNull
    @Override
    protected ImageView getLotteryBoxView() {
        return icon_room_lottery_box;
    }

    @NonNull
    @Override
    protected Banner getBannerView() {
        return banner;
    }

    @NonNull
    @Override
    protected LuckyGiftView getLuckyGiftView() {
        return lgLuckyView;
    }

    @NonNull
    @Override
    protected SVGAImageView getLuckyTipsSVGAIv() {
        return svgaLuckyTips;
    }

    @NonNull
    @Override
    protected SVGAImageView getLuckyBoxActivateView() {
        return svgaLuckyBoxActivate;
    }

    @NonNull
    @Override
    protected ScreenNoticeView getLuckyGiftNoticeTips() {
        return svgaLuckyGiftNotice;
    }

    @NonNull
    @Override
    protected ScreenNoticeView getAddCoinNoticeTips() {
        return svgaAddCoinNotice;
    }

    @NonNull
    @Override
    protected TextView getOnlineNumView() {
        return tv_room_num;
    }

    @Override
    protected TextView getFollowView() {
        return tv_follow;
    }

    @Override
    protected SVGAImageView getAddGoldSvga() {
        return svgaAddGold;
    }

    @Override
    protected View getReceiveSumView() {
        return mReceiveSumTv;
    }

    @NonNull
    @Override
    protected ImageView getAvatarView() {
        return avatar;
    }

    @NonNull
    @Override
    protected ImageView getRoomCloseView() {
        return iv_close;
    }

    @NonNull
    @Override
    protected LinearLayout getDistanceView() {
        return llDistance;
    }

    @NonNull
    @Override
    protected HeadLineStarNoticeView getHeadLineStarLimitNoticeTips() {
        return svgaHeadLineStartLimitNotice;
    }

    @NonNull
    @Override
    protected TextView getDistanceTextView() {
        return tvDistance;
    }

    @NonNull
    @Override
    protected ImageView getSpecialDiscountView() {
        return ivSpecialDiscount;
    }

    @NonNull
    @Override
    protected Banner getBannerEventView() {
        return bannerEvent;
    }

    /**
     * 宝箱激活文案
     *
     * @return
     */
    @Override
    protected int getLuckyBoxActivateTips() {
        return R.string.lucky_box_activate_audience;
    }



    public static VideoRoomSlaveDetailFragment newInstance(long roomUid) {
        VideoRoomSlaveDetailFragment roomDetailFragment = new VideoRoomSlaveDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.ROOM_UID, roomUid);
        roomDetailFragment.setArguments(bundle);
        return roomDetailFragment;
    }


    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_video_slave_room_detail;
    }

    @Override
    public void onSetListener() {
        super.onSetListener();

        ll_contribute.setOnClickListener(view -> RankingListActivity.start(getContext(), CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", getMvpPresenter().getCurrRoomInfo().getUid() + ""));

        mView.findViewById(R.id.tv_room_num).setOnClickListener(v -> {
            ListDataDialog listDataDialog = ListDataDialog.newOnlineUserListInstance(getActivity(), true);
            listDataDialog.setOnlineItemClick(onlineItemClick);
            showDialog(listDataDialog);

            Log.i(TAG, "onSetListener: " + ll_contribute.getBottom());
        });

        tv_follow.setOnClickListener(v -> {
            getMvpPresenter().roomAttention(getMvpPresenter().getCurrRoomInfo().getRoomId(), true);
        });
    }

    @Override
    public void initiate() {
        super.initiate();
//        GrowingIO.getInstance().setPageName(this, "视频-游客页");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "视频房");

        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, DisplayUtils.getStatusBarHeight(mContext), 0, 0);
        rlTop.setLayoutParams(params);

        if (roomInfo != null && roomInfo.getRoomUserOnLiveClient() == 0) { //主播是web端
            webLayout();
        }

        if (AvRoomDataManager.get().isRoomOwner()) {
            tv_follow.setVisibility(View.INVISIBLE);
        } else {
            tv_follow.setVisibility(View.VISIBLE);
        }

        onlineAdapter = new VideoRoomOnlineAdapter();
        rvOnlineList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvOnlineList.setAdapter(onlineAdapter);

        onlineAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (onlineAdapter != null && ListUtils.isNotEmpty(onlineAdapter.getData())) {
                IMChatRoomMember member = onlineAdapter.getData().get(position);
                showUserInfoDialog(Integer.valueOf(member.getAccount()));
            }
        });

        microUserInfoView.setMicroUserInfo(roomInfo);
        refreshReceiveSum(roomInfo);
        refreshLianMicroViewStatus();
    }

    @Override
    public void receiveRoomEvent(RoomEvent roomEvent) {
        if (roomEvent != null && getMvpPresenter() != null) {
            switch (roomEvent.getEvent()) {
                case RoomEvent.ROOM_RECONNECT://重连
                    getMvpPresenter().getLianMicroStatus(lianMicroApplyDialog != null && lianMicroApplyDialog.isShowing());
                    break;
                case RoomEvent.RECEIVE_LIAN_MICRO_SEND_MSG:
                    boolean isAgree = roomEvent.isLianMicroAgree();
                    String message = roomEvent.getReason_msg();
                    if (!TextUtils.isEmpty(message)) {
                        toast(message);
                    }
                    getMvpPresenter().setApplyLianMicro(false);
                    getMvpPresenter().setInLianMicro(isAgree);
                    dismissLianMicroApplyDialog();
                    refreshLianMicroViewStatus();
                    break;
                case RoomEvent.DOWN_MIC:
                case RoomEvent.KICK_DOWN_MIC:
                    getMvpPresenter().setApplyLianMicro(false);
                    break;
                case RoomEvent.KICK_OUT_ROOM:
                    LogUtil.i(TAG, "KICK_OUT_ROOM");
                    if (StringUtils.isNotEmpty(roomEvent.getReason_msg())) {
                        toast(roomEvent.getReason_msg());
                    }
                    finish();
                    break;
                case RoomEvent.CODE_LUCKY_TIPS:
                    //幸运提示
                    showLuckyRoomTips(roomEvent.getRoomLuckyTips());
                    break;
            }
        }
        super.receiveRoomEvent(roomEvent);
    }

    // web端主播开播布局
    public void webLayout() {
        int canvasMarginTop = getResources().getDimensionPixelSize(R.dimen.video_micro_surface_margin_top);// canvas相对顶部的高度
        int screenHeight = (int) (com.tongdaxing.xchat_framework.util.util.DisplayUtils.getScreenWidth(mContext) * 3.0 / 4);// 4:3 时的屏幕高度

        // 宝箱的位置
        RelativeLayout.LayoutParams luckBoxLP = (RelativeLayout.LayoutParams) getBannerEventView().getLayoutParams();
        luckBoxLP.addRule(RelativeLayout.BELOW, 0);
        luckBoxLP.setMargins(DisplayUtils.dip2px(mContext, 18),
                canvasMarginTop + DisplayUtils.dip2px(mContext, 8),
                0, 0);
        getBannerEventView().setLayoutParams(luckBoxLP);

        // banner的位置
        RelativeLayout.LayoutParams bannerLP = (RelativeLayout.LayoutParams) getBannerView().getLayoutParams();
        bannerLP.addRule(RelativeLayout.BELOW, 0);
        bannerLP.setMargins(DisplayUtils.dip2px(mContext, 18),
                canvasMarginTop + DisplayUtils.dip2px(mContext, 8),
                0, 0);
        getBannerView().setLayoutParams(bannerLP);

        // 谁来了飘屏的位置
        int cmvHeight = DisplayUtils.dip2px(mContext, 34);// 谁来了控件的高度
        int layoutMarginTop = canvasMarginTop + screenHeight - cmvHeight - 4;// 谁来了相对顶部的高度
        RelativeLayout.LayoutParams cmvLP = (RelativeLayout.LayoutParams) getComingMsgView().getLayoutParams();
        cmvLP.addRule(RelativeLayout.ABOVE, 0);
        cmvLP.setMargins(0,
                layoutMarginTop,
                0, 0);
        getComingMsgView().setLayoutParams(cmvLP);

        // 公屏位置
        RelativeLayout.LayoutParams mvLP = (RelativeLayout.LayoutParams) getMessageView().getLayoutParams();
        mvLP.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
        mvLP.setMargins(0,
                canvasMarginTop + screenHeight + DisplayUtils.dip2px(mContext, 8),
                0, 0);

    }

    /**
     * 幸运房间特效
     *
     * @param roomLuckyTips
     */
    private void showLuckyRoomTips(RoomLuckyTips roomLuckyTips) {
        RoomInfo roomInfo = getMvpPresenter().getCurrRoomInfo();
        if (roomInfo == null || roomLuckyTips == null) {
            return;
        }
        //本房间才展示
        if (roomInfo.getRoomId() != roomLuckyTips.getRoomId()) {
            return;
        }
        //幸运儿自己不展示
        if (roomLuckyTips.getNotShowUids().contains(getMvpPresenter().getCurrentUserId())) {
            return;
        }
        SVGAParser parser = new SVGAParser(mContext);
        parser.decodeFromAssets("luckyroom.svga", new SVGAParser.ParseCompletion() {

            @Override
            public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                GlideApp.with(mContext)
                        .asBitmap()
                        .load(roomLuckyTips.getAvatar())
                        .apply(RequestOptions.circleCropTransform())
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                setLuckyRoomTips(resource, roomLuckyTips, svgaVideoEntity);
                            }
                        });
            }

            @Override
            public void onError() {

            }

        });
    }

    private void setLuckyRoomTips(Bitmap bitmap, RoomLuckyTips roomLuckyTips, SVGAVideoEntity svgaVideoEntity) {
        SVGADynamicEntity dynamicEntity = new SVGADynamicEntity();
        //头像
        if (bitmap != null) {
            dynamicEntity.setDynamicImage(bitmap, "touxiang");
        }

        TextPaint normalTextPaint = new TextPaint();
        normalTextPaint.setColor(Color.WHITE);
        normalTextPaint.setTextSize(20);
        //昵称
        if (!TextUtils.isEmpty(roomLuckyTips.getNick())) {
            dynamicEntity.setDynamicText(roomLuckyTips.getNick(), normalTextPaint, "mingzi01");
        }
        //文案
        String nick = StringUtils.ellipsize(roomLuckyTips.getNick(), 3);
        String tips1 = "恭喜本房间的";
        SpannableStringBuilder ssb = new SpannableStringBuilder(String.format(tips1.concat("%s夺得太空宝藏奖励"), nick));
        ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.color_FCFF05)), tips1.length(), tips1.length() + nick.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        dynamicEntity.setDynamicText(new StaticLayout(
                ssb,
                normalTextPaint,
                Integer.MAX_VALUE,
                Layout.Alignment.ALIGN_CENTER,
                1,
                0,
                false
        ), "mingzi02");

        SVGADrawable drawable = new SVGADrawable(svgaVideoEntity, dynamicEntity);
        if (svgaLuckyRoom != null) {
            svgaLuckyRoom.setVisibility(View.VISIBLE);
            svgaLuckyRoom.setImageDrawable(drawable);
            svgaLuckyRoom.setLoops(1);
            svgaLuckyRoom.startAnimation();
        }
    }

    private void refreshReceiveSum(RoomInfo roomInfo) {
        if (roomInfo == null) return;
        if (roomInfo.getReceiveSum() == null) {
            roomInfo.setReceiveSum(0L);
        }
        mReceiveSumTv.setText(roomInfo.getReceiveSum() + "");
    }

    @Override
    public void refreshRoomAttentionStatus(Boolean status) {
        microUserInfoView.setAttention(status);
    }

    /**
     * 刷新在线人员列表view 注意这里请求的是在线头像人数列表
     */
    @Override
    public void refreshOnlineView(List<IMChatRoomMember> memberList) {
        if (ListUtils.isListEmpty(memberList) || memberList.size() <= 0) return;
        onlineAdapter.setNewData(memberList);
    }

    /**
     * 刷新关注标签
     */
    @Override
    protected void refreshAttention() {
        microUserInfoView.tv_follow.setSelected(true);
        getMvpPresenter().checkAttention(getMvpPresenter().getCurrRoomInfo().getRoomId());
    }

    /**
     * 显示房间更多功能弹框
     */
    // TODO: 2019/3/23 可能要弱引用
    @Override
    public void showRoomMoreOperateDialog() {

        RoomFunctionDialog functionDialog = new RoomFunctionDialog(mContext);
        functionDialog.setFunctionType(RoomFunctionModel.ROOM_VIDEO_DEFAULT);
        functionDialog.setOnFunctionClickListener(bean -> {
            if (bean.getFunctionType() == RoomFunctionEnum.ROOM_REPORT) {
                List<ButtonItem> buttons = new ArrayList<>();
                ButtonItem button1 = new ButtonItem("政治敏感", () -> {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        ButtonItemFactory.reportCommit(mContext, 2, 1, AvRoomDataManager.get().getRoomInfo().getUid());
                    }
                });
                ButtonItem button2 = new ButtonItem("色情低俗", () -> {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        ButtonItemFactory.reportCommit(mContext, 2, 2, AvRoomDataManager.get().getRoomInfo().getUid());
                    }
                });
                ButtonItem button3 = new ButtonItem("广告骚扰", () -> {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        ButtonItemFactory.reportCommit(mContext, 2, 3, AvRoomDataManager.get().getRoomInfo().getUid());
                    }
                });
                ButtonItem button4 = new ButtonItem("人身攻击", () -> {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        ButtonItemFactory.reportCommit(mContext, 2, 4, AvRoomDataManager.get().getRoomInfo().getUid());
                    }
                });
                buttons.add(button1);
                buttons.add(button2);
                buttons.add(button3);
                buttons.add(button4);
                getDialogManager().showCommonPopupDialog(buttons, "取消");
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_FEEDBACK) {
                startActivity(new Intent(mContext, FeedbackActivity.class));
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_SHARE) {
                UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomShare());
                showShareDialog();
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_QUIT) {
                getMvpPresenter().exitRoom(null);
                finish();
            } else if (RoomFunctionEnum.ROOM_PLANT_BEAN == bean.getFunctionType()) {
                //种豆消息
                boolean isPlantBeanMsgEnabled = (boolean) SpUtils.get(BasicConfig.INSTANCE.getAppContext(), Constants.KEY_PLANT_BEAN_MSG, true);
                SpUtils.put(BasicConfig.INSTANCE.getAppContext(), Constants.KEY_PLANT_BEAN_MSG, !isPlantBeanMsgEnabled);
                bean.setImgRes(!isPlantBeanMsgEnabled ? R.mipmap.ic_plant_bean_msg_setting_enable : R.mipmap.ic_plant_bean_msg_setting_disable);
                functionDialog.getAdapter().notifyDataSetChanged();
            } else if (RoomFunctionEnum.ROOM_PRIVATE_CHAT == bean.getFunctionType()) {
                RoomChatActivity.startDialogActivity(getContext());
            }
        });

        functionDialog.show();
        functionDialog.initData();
    }

    /**
     * 显示连麦申请弹框
     *
     * @param statusInfo 连麦状态信息
     */
    @Override
    public void showLianMicroApplyDialog(@NonNull LianMicroStatusInfo statusInfo) {
        dismissLianMicroApplyDialog();
        BaseDialogFragment dialog = LianMicroDialogFactory.create(statusInfo, onLianMicroDialogClickListener);
        showDialog(dialog);
        lianMicroApplyDialog = dialog;
    }

    /**
     * 连麦弹框点击事件监听
     */
    private LianMicroDialogFactory.OnLianMicroDialogClickListener onLianMicroDialogClickListener = new LianMicroDialogFactory.OnLianMicroDialogClickListener() {
        @Override
        public void onVideoBtnClick() {
            //视频连麦
            getDialogManager().showProgressDialog(mContext);
            getMvpPresenter().applyLianMicro(AvRoomDataManager.MIC_TYPE_VIDEO);

        }

        @Override
        public void onAudioBtnClick() {
            //语音连麦
            getDialogManager().showProgressDialog(mContext);
            getMvpPresenter().applyLianMicro(AvRoomDataManager.MIC_TYPE_AUDIO);
        }

        @Override
        public void onCancelBtnClick() {
            getDialogManager().showProgressDialog(mContext);
            getMvpPresenter().cancelLianMicroApply();
        }
    };


    /**
     * 关闭连麦申请弹框
     */
    @Override
    public void dismissLianMicroApplyDialog() {
        if (lianMicroApplyDialog != null) {
            lianMicroApplyDialog.dismiss();
            lianMicroApplyDialog = null;
        }
    }

    /**
     * 更新连麦按钮状态
     */
    @Override
    public void refreshLianMicroViewStatus() {
        RoomInfo currRoomInfo = getMvpPresenter().getCurrRoomInfo();
        if (currRoomInfo != null) {
            //是否用过（点击过）连麦功能
            boolean isLianMicroUsed = getMvpPresenter().isLianMicroUsed();
            //是否已经申请连麦或者在连麦中
            boolean isInLianMicroOrApply = getMvpPresenter().isApplyLianMicro() || getMvpPresenter().isInLianMicro();
            ((VideoSlaveBottomView) getBottomView()).setupLianMicroView(!isLianMicroUsed, isInLianMicroOrApply, onLianMicroBtnClickListener);
        }
    }

    /**
     * 显示退出提醒dialog
     */
    protected void showExitTipDialog() {
        //1.5.0需求：点击关闭按钮直接关闭视频房，无需toast确认窗口
        getMvpPresenter().exitRoom(null);
        finish();
    }

    @Override
    public boolean onBack() {
        if (isResumed()) {
            if (getGiftSelector().isShowing()) {
                getGiftSelector().hide();
            } else {
                showExitTipDialog();
            }
            return true;
        } else {
            return false;
        }
    }

    protected void changeState() {
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        if (unreadCount > 0) {
            bottomView.showMsgMark(true);
        } else {
            bottomView.showMsgMark(false);
        }
    }

    /**
     * 连麦按钮点击监听
     */
    @Override
    protected void onMicroBtnClick(View v) {
        //嘉宾
        getMvpPresenter().getLianMicroStatus(true);
    }

}
