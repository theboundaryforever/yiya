package com.yiya.mobile.room.avroom.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.opensource.svgaplayer.SVGAImageView;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.bean.RecommendWorldsInfo;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.bean.RoomRedPackageInfo;
import com.tongdaxing.xchat_core.bean.SendRedPackageInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmAttachment;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.CPKInviteBean;
import com.tongdaxing.xchat_core.room.bean.CPKMsgBean;
import com.tongdaxing.xchat_core.room.bean.CPKResultBean;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.PigFightBean;
import com.tongdaxing.xchat_core.room.bean.RoomFunctionEnum;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.im.IMReportRoute;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.ChatUtil;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.SpUtils;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.yiya.mobile.room.audio.widget.YiYaMusicPlayerDialog;
import com.yiya.mobile.room.avroom.activity.RoomChatActivity;
import com.yiya.mobile.room.avroom.activity.RoomInviteActivity;
import com.yiya.mobile.room.avroom.activity.RoomSettingActivity;
import com.yiya.mobile.room.avroom.activity.RoomTopicActivity;
import com.yiya.mobile.room.avroom.adapter.RoomGiftRecordAdapter;
import com.yiya.mobile.room.avroom.other.ButtonItemFactory;
import com.yiya.mobile.room.avroom.other.OnMicroItemClickListener;
import com.yiya.mobile.room.avroom.widget.AudioBottomView;
import com.yiya.mobile.room.avroom.widget.AudioMicroView;
import com.yiya.mobile.room.avroom.widget.BaseBottomView;
import com.yiya.mobile.room.avroom.widget.BaseMicroView;
import com.yiya.mobile.room.avroom.widget.ComingMsgView;
import com.yiya.mobile.room.avroom.widget.GiftMsgView;
import com.yiya.mobile.room.avroom.widget.GiftView;
import com.yiya.mobile.room.avroom.widget.HeadLineStarNoticeView;
import com.yiya.mobile.room.avroom.widget.InputMsgView;
import com.yiya.mobile.room.avroom.widget.LuckyGiftView;
import com.yiya.mobile.room.avroom.widget.MessageGuideView;
import com.yiya.mobile.room.avroom.widget.MessageView;
import com.yiya.mobile.room.avroom.widget.PigFightView;
import com.yiya.mobile.room.avroom.widget.ScreenNoticeView;
import com.yiya.mobile.room.avroom.widget.YiYaAudioMicroView;
import com.yiya.mobile.room.avroom.widget.dialog.CPKResultDialog;
import com.yiya.mobile.room.avroom.widget.dialog.LaunchCPKDialog;
import com.yiya.mobile.room.avroom.widget.dialog.RoomFunctionDialog;
import com.yiya.mobile.room.avroom.widget.dialog.RoomFunctionModel;
import com.yiya.mobile.room.avroom.widget.dialog.RoomTopicDialog;
import com.yiya.mobile.room.gift.GiftSelector;
import com.yiya.mobile.room.presenter.AudioRoomDetailPresenter;
import com.yiya.mobile.room.presenter.IAudioRoomDetailView;
import com.yiya.mobile.room.widget.dialog.GiftRecordDialog;
import com.yiya.mobile.room.widget.dialog.ListDataDialog;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.find.activity.SendRedPackageActivity;
import com.yiya.mobile.ui.find.view.PublicRedPackageDialog;
import com.yiya.mobile.ui.me.setting.activity.FeedbackActivity;
import com.yiya.mobile.ui.widget.Banner;
import com.yiya.mobile.ui.widget.avatarStack.AvatarStackView;
import com.yiya.mobile.ui.widget.avatarStack.WhiteAvatarStackAdapter;
import com.yiya.mobile.utils.UIHelper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

import static com.tongdaxing.xchat_core.room.bean.RoomInfo.ROOMTYPE_HOME_PARTY;

/**
 * 语音房的房间fragment
 *
 * @author zeda
 */
@CreatePresenter(AudioRoomDetailPresenter.class)
public class YiYaAudioRoomDetailFragment extends BaseRoomDetailFragment<IAudioRoomDetailView, AudioRoomDetailPresenter> implements IAudioRoomDetailView {

    @BindView(R.id.micro_container)
    YiYaAudioMicroView microView;
    @BindView(R.id.message_view)
    MessageView messageView;
    @BindView(R.id.gift_view)
    GiftView giftView;
    @BindView(R.id.bottom_view)
    AudioBottomView bottomView;
    @BindView(R.id.msg_container)
    GiftMsgView giftMsgView;
    @BindView(R.id.room_title)
    TextView titleTv;
    @BindView(R.id.iv_room_bg)
    ImageView roomBgIv;
    @BindView(R.id.tv_online_num)
    TextView tv_online_num;
    @BindView(R.id.input_layout)
    InputMsgView inputMsgView;
    @BindView(R.id.cmv_msg)
    ComingMsgView cmvMsgView;
    @BindView(R.id.tv_room_uid)
    TextView tv_room_uid;
    @BindView(R.id.giftSelector)
    GiftSelector giftSelector;
    @BindView(R.id.iv_room_owner)
    ImageView ivRoomOwner;
    @BindView(R.id.iv_people_num)
    ImageView ivPeopleNum;
    @BindView(R.id.iv_follow)
    ImageView ivFollow;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.iv_share)
    ImageView ivShare;
    @BindView(R.id.iv_music)
    ImageView ivMusic;
    @BindView(R.id.icon_room_lottery_box)
    ImageView icon_room_lottery_box;
    @BindView(R.id.ic_room_banner)
    Banner icon_room_new_gift;
    @BindView(R.id.lgLuckyView)
    LuckyGiftView lgLuckyView;
    @BindView(R.id.svga_lucky_box_activate)
    SVGAImageView svgaLuckyBoxActivate;
    @BindView(R.id.svga_lucky_tips)
    SVGAImageView svgaLuckyTips;
    @Nullable
    @BindView(R.id.iv_time_count)
    TextView ivTimeCount;
    @BindView(R.id.iv_room_pig_fight)
    ImageView ivRoomPigFight;

    @BindView(R.id.ll_MessageGuide)
    LinearLayout ll_MessageGuide;
    @BindView(R.id.message_guide_view)
    MessageGuideView messageGuideView;
    @BindView(R.id.fl_mg_close)
    FrameLayout flMgClose;
    @BindView(R.id.iv_room_operate)
    ImageView moreOperateBtn;
    @BindView(R.id.pig_fight_view)
    PigFightView pigFightView;
    @BindView(R.id.svga_lucky_gift_notice)
    ScreenNoticeView svgaLuckyGiftNotice;

    @BindView(R.id.svga_add_coin_notice)
    ScreenNoticeView svgaAddCoinNotice;
    @BindView(R.id.svga_head_line_start_limit_notice)
    HeadLineStarNoticeView svgaHeadLineStartLimitNotice;
    @BindView(R.id.banner_event)
    Banner bannerEvent;
    @BindView(R.id.group_lock)
    Group groupLock;
    @BindView(R.id.asv_contributors)
    AvatarStackView asvContributors;
    @BindView(R.id.ll_contributors)
    LinearLayout llContributors;

    private List<ChatRoomMessage> giftMsgList;
    protected GiftRecordDialog giftRecordDialog;

    private CountDownTimer timer = null;
    private YiYaMusicPlayerDialog musicPlayerDialog;

    public static YiYaAudioRoomDetailFragment newInstance(long roomUid) {
        YiYaAudioRoomDetailFragment roomDetailFragment = new YiYaAudioRoomDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.ROOM_UID, roomUid);
        roomDetailFragment.setArguments(bundle);
        return roomDetailFragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_audio_room;
    }

    @Override
    public void onFindViews() {
    }


    @Override
    protected void onBottomMsgClick() {
        RoomChatActivity.startActivityWithSquare(getContext());
    }

    @Override
    public void initiate() {
        super.initiate();
//        GrowingIO.getInstance().setPageName(this, "语音-房间");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "语聊房");
        //获取礼物赠送记录
        getMvpPresenter().getRecommendWorlds();
        giftMsgList = getMvpPresenter().getGiftMsgList();
        if (!ListUtils.isListEmpty(giftMsgList)) {
            setupGiftSendMsg(giftMsgList.get(0));//显示最近的一个礼物
        } else {
            giftMsgList = new ArrayList<>();
        }
        //初始化播放器的状态
        setupMusicPlayView();
        if (AvRoomDataManager.get().isRoomOwner()) {
            ivFollow.setVisibility(View.GONE);
        }

        // 关闭小礼物特效
        giftView.setSmallGift(true);
        //最小化检测
        changeState();

        //获取房间是否有魅力值pk倒计时
        getMvpPresenter().getCharmCountDown();
    }

    @NonNull
    @Override
    protected SVGAImageView getLuckyBoxActivateView() {
        return svgaLuckyBoxActivate;
    }

    @NonNull
    @Override
    protected ScreenNoticeView getLuckyGiftNoticeTips() {
        return svgaLuckyGiftNotice;
    }

    @NonNull
    @Override
    protected ScreenNoticeView getAddCoinNoticeTips() {
        return svgaAddCoinNotice;
    }

    @NonNull
    @Override
    protected HeadLineStarNoticeView getHeadLineStarLimitNoticeTips() {
        return svgaHeadLineStartLimitNotice;
    }

    @NonNull
    @Override
    protected BaseMicroView getMicroView() {
        return microView;
    }

    @NonNull
    @Override
    protected MessageView getMessageView() {
        return messageView;
    }

    @NonNull
    @Override
    protected GiftView getGiftView() {
        return giftView;
    }

    @NonNull
    @Override
    protected BaseBottomView getBottomView() {
        return bottomView;
    }

    @NonNull
    @Override
    protected TextView getTitleView() {
        titleTv.setFocusable(true);
        titleTv.setFocusableInTouchMode(true);
        titleTv.requestFocus();
        return titleTv;
    }

    @NonNull
    protected GiftMsgView getGiftMsgView() {
        return giftMsgView;
    }

    @NonNull
    @Override
    protected ComingMsgView getComingMsgView() {
        return cmvMsgView;
    }

    @NonNull
    @Override
    protected ImageView getRoomBgView() {
        return roomBgIv;
    }

    @NonNull
    @Override
    protected GiftSelector getGiftSelector() {
        return giftSelector;
    }

    @NonNull
    @Override
    protected InputMsgView getInputMsgView() {
        return inputMsgView;
    }

    @NonNull
    @Override
    protected View getMoreOperateBtn() {
        return moreOperateBtn;
    }

    @NonNull
    @Override
    protected ImageView getLotteryBoxView() {
        return icon_room_lottery_box;
    }


    @NonNull
    @Override
    protected Banner getBannerView() {
        return icon_room_new_gift;
    }

    @NonNull
    @Override
    protected LuckyGiftView getLuckyGiftView() {
        return lgLuckyView;
    }

    @Override
    protected SVGAImageView getLuckyTipsSVGAIv() {
        return svgaLuckyTips;
    }

    @NonNull
    @Override
    protected Banner getBannerEventView() {
        return bannerEvent;
    }


    /**
     * 显示房间更多功能弹框
     */
    @Override
    public void showRoomMoreOperateDialog() {
        RoomFunctionDialog functionDialog = new RoomFunctionDialog(mContext);

        if (AvRoomDataManager.get().isRoomOwner() | AvRoomDataManager.get().isRoomAdmin()) {
            functionDialog.setFunctionType(RoomFunctionModel.ROOM_AUDIO_MANAGE);
        } else {
            functionDialog.setFunctionType(RoomFunctionModel.ROOM_AUDIO_NORMAL);
        }

        functionDialog.setOnFunctionClickListener(bean -> {
            if (bean.getFunctionType() == RoomFunctionEnum.ROOM_REPORT) {
                List<ButtonItem> buttons = new ArrayList<>();
                ButtonItem button1 = new ButtonItem("政治敏感", () -> {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        ButtonItemFactory.reportCommit(mContext, 2, 1, AvRoomDataManager.get().getRoomInfo().getUid());
                    }
                });
                ButtonItem button2 = new ButtonItem("色情低俗", () -> {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        ButtonItemFactory.reportCommit(mContext, 2, 2, AvRoomDataManager.get().getRoomInfo().getUid());
                    }
                });
                ButtonItem button3 = new ButtonItem("广告骚扰", () -> {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        ButtonItemFactory.reportCommit(mContext, 2, 3, AvRoomDataManager.get().getRoomInfo().getUid());
                    }
                });
                ButtonItem button4 = new ButtonItem("人身攻击", () -> {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        ButtonItemFactory.reportCommit(mContext, 2, 4, AvRoomDataManager.get().getRoomInfo().getUid());
                    }
                });
                buttons.add(button1);
                buttons.add(button2);
                buttons.add(button3);
                buttons.add(button4);
                getDialogManager().showCommonPopupDialog(buttons, "取消");
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_MINIMIZE) {
                AvRoomDataManager.get().setMinimize(true);
                getActivity().finish();
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_SHARE) {
                UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomShare());
                showShareDialog();
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_PUBLIC_ENABLE) {
                IAuthCore core = CoreManager.getCore(IAuthCore.class);
                if (core == null)
                    return;
                String ticket = core.getTicket();
                long currentUid = core.getCurrentUid();
                RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
                if (roomInfo != null) {
                    if (roomInfo.getPublicChatSwitch() == 1) {
                        CoreManager.getCore(IAVRoomCore.class).changeRoomMsgFilter(roomInfo.getType(), AvRoomDataManager.get().isRoomOwner(), 0, ticket, currentUid);
                    } else {
                        CoreManager.getCore(IAVRoomCore.class).changeRoomMsgFilter(roomInfo.getType(), AvRoomDataManager.get().isRoomOwner(), 1, ticket, currentUid);
                    }
                }
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_FEEDBACK) {
                startActivity(new Intent(mContext, FeedbackActivity.class));
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_QUIT) {
                getMvpPresenter().exitRoom(null);
                getActivity().finish();
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_SETTING) {
                int isPermitRoom = 2;
                if (AvRoomDataManager.get().getRoomInfo() != null)
                    isPermitRoom = AvRoomDataManager.get().getRoomInfo().getIsPermitRoom();
                RoomSettingActivity.start(getContext(), AvRoomDataManager.get().getRoomInfo(), isPermitRoom);
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_PLAY_MUSIC) {
                if (AvRoomDataManager.get().isOwnerOnMic()) {// 注意：用户不上麦，是不会初始化这个view的
                    showMusicPlayerDialog();
                } else {
                    toast("请上麦");
                }

            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_RED_PACKET) {
                SendRedPackageActivity.start(mContext, AvRoomDataManager.get().getRoomInfo().getRoomId());
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_CPK) {// 魅力值PK
                FragmentManager fragmentManager = getFragmentManager();
                if (fragmentManager != null) {
                    LaunchCPKDialog launchCPKDialog = new LaunchCPKDialog();
                    launchCPKDialog.show(fragmentManager, InputPwdDialogFragment.class.getSimpleName());
                }
            } else if (RoomFunctionEnum.ROOM_PLANT_BEAN == bean.getFunctionType()) {
                //种豆消息
                boolean isPlantBeanMsgEnabled = (boolean) SpUtils.get(BasicConfig.INSTANCE.getAppContext(), Constants.KEY_PLANT_BEAN_MSG, true);
                SpUtils.put(BasicConfig.INSTANCE.getAppContext(), Constants.KEY_PLANT_BEAN_MSG, !isPlantBeanMsgEnabled);
                bean.setImgRes(!isPlantBeanMsgEnabled ? R.mipmap.ic_plant_bean_msg_setting_enable : R.mipmap.ic_plant_bean_msg_setting_disable);
                functionDialog.getAdapter().notifyDataSetChanged();
            } else if (RoomFunctionEnum.ROOM_PIG_FIGHT == bean.getFunctionType()) {// 猪猪大作战
                if (AvRoomDataManager.get().getRoomInfo() != null) {
                    if (AvRoomDataManager.get().getRoomInfo().getOpenPigFight() == 1) {
                        getMvpPresenter().openOrClosePigFight(RoomInfo.GameBean.GAME_GAME_OFF);
                    } else {
                        getMvpPresenter().openOrClosePigFight(RoomInfo.GameBean.GAME_GAME_ON);
                    }
                }
            }
        });

        functionDialog.show();

        if (AvRoomDataManager.get().getRoomInfo() != null) {
            List<RoomFunctionEnum> tmp = new ArrayList<>();

            if (AvRoomDataManager.get().getRoomInfo().getCharmEnable() != 2) {// 关闭魅力值PK
                tmp.add(RoomFunctionEnum.ROOM_CPK);
            }

            RoomInfo.GameBean game = getMvpPresenter().getGame(RoomInfo.GameBean.GAME_TYPE_PIG);
            if (game == null) {// 猪猪大作战
                tmp.add(RoomFunctionEnum.ROOM_PIG_FIGHT);
            } else if (game.getIsOpen() == RoomInfo.GameBean.GAME_GAME_OFF) {
                tmp.add(RoomFunctionEnum.ROOM_PIG_FIGHT);
            }

            if (getMvpPresenter().getClientConfigure() != null && getMvpPresenter().getClientConfigure().getLishi_flag().equals("1")) {// 禁用redpack
                tmp.add(RoomFunctionEnum.ROOM_RED_PACKET);
            }

            functionDialog.initData(tmp);
        } else {
            functionDialog.initData();
        }
    }

    @NonNull
    @Override
    protected TextView getRoomUid() {
        return tv_room_uid;
    }

    @Override
    public void receiveRoomEvent(RoomEvent roomEvent) {
        super.receiveRoomEvent(roomEvent);
        if (roomEvent != null && getMvpPresenter() != null) {
            int event = roomEvent.getEvent();
            switch (event) {
                case RoomEvent.ROOM_MEMBER_IN:
                case RoomEvent.ROOM_MEMBER_EXIT:
                    if (roomEvent.getChatRoomMessage() != null
                            && roomEvent.getChatRoomMessage().getImChatRoomMember() != null
                            && roomEvent.getChatRoomMessage().getImChatRoomMember().getTimestamp() > getMvpPresenter().getTimestampOnMemberChanged()) {
                        //当房间人员变化的时候更新本地房间人数
                        int currOnLineNum = roomEvent.getChatRoomMessage().getImChatRoomMember().getOnline_num();
                        getMvpPresenter().getCurrRoomInfo().setOnlineNum(currOnLineNum);
                        //当房间人员变化的时候记录一下变化的时间戳
                        getMvpPresenter().setTimestampOnMemberChanged(roomEvent.getChatRoomMessage().getImChatRoomMember().getTimestamp());
                        //刷新在线人数显示
                        refreshOnlineCount();
                    }
                    break;
                case RoomEvent.DOWN_MIC://下麦
                case RoomEvent.UP_MIC://上麦
                    setupMusicPlayView();
                    break;
                case RoomEvent.RECEIVE_RED_PACKET://接收到红包
                    getMvpPresenter().insertRoomRedPackage(roomEvent.getRoomRedPackageInfo());
                    break;
                case RoomEvent.KICK_OUT_ROOM:
                    toast(roomEvent.getReason_no() + ":" + roomEvent.getReason_msg());
                    finish();
                    break;
                case RoomEvent.ROOM_RECONNECT://IM重连
                    //管理员列表是本地的，要及时更新
                    getMvpPresenter().refreshRoomManagerMember();
                    break;
                case RoomEvent.ROOM_INFO_UPDATE:
                    //魅力值刷新的时候
                    updateCharmFromRoom();
                    break;
                case RoomEvent.ROOM_CHARM:
                    updateCharmData(roomEvent.getRoomCharmAttachment());
                    break;
                case RoomEvent.ROOM_CPK_INVITE:// 魅力值PK邀请
                    showInviteCPKDialog(roomEvent.getCpkInviteBean());
                    break;
                case RoomEvent.ROOM_CPK_RESULT:// 魅力值PK结果
                    showCPKResultDialog(roomEvent.getCPKResultBean());
                    break;
                case RoomEvent.ROOM_CPK_MSG:// 魅力值PK消息
                    dealCPKMsg(roomEvent.getCpkMsgBean());
                    break;
                case RoomEvent.ROOM_PF_RESULT:// 猪猪大作战结果
                    dealPFResult(roomEvent.getPigFightBean());
                    break;
                case RoomEvent.ROOM_PF_CTR:// 猪猪大作战ICON控制
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        AvRoomDataManager.get().getRoomInfo().setOpenPigFight(roomEvent.getPigFightCrtBean().getType() == 1 ? 1 : 0);
                    }

                    refreshPigFight();
                    break;
                default:
            }
        }
    }

    @Override
    protected void receiveRoomMsg(List<ChatRoomMessage> messages) {
        super.receiveRoomMsg(messages);
        if (messages.size() == 0) return;
        //是否有新的礼物消息更新礼物记录
        boolean hasChange = false;
        for (int i = 0; i < messages.size(); i++) {
            ChatRoomMessage chatRoomMessage = messages.get(i);
            if (chatRoomMessage == null) return;
            if (IMReportRoute.sendMessageReport.equalsIgnoreCase(chatRoomMessage.getRoute())) {
                IMCustomAttachment attachment = chatRoomMessage.getAttachment();
                if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT || attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                    hasChange = true;
                    giftMsgList.add(0, chatRoomMessage);
                    setupGiftSendMsg(chatRoomMessage);
                }
            }
        }
        //如果当前正弹着礼物记录弹框，则刷新下礼物记录弹框的数据
        if (hasChange && giftRecordDialog != null && giftRecordDialog.isShowing()) {
            giftRecordDialog.loadData(giftMsgList);
        }
    }

    @Override
    protected void refreshView() {
        super.refreshView();
        RoomInfo currentRoomInfo = getMvpPresenter().getCurrRoomInfo();
        if (currentRoomInfo == null) return;

        setupRoomBg(currentRoomInfo);
        getRoomUid().setText(String.format("ID:%d", currentRoomInfo.getErbanNo()));

//        tv_room_desc.setText(TextUtils.isEmpty(currentRoomInfo.getRoomDesc()) ? "房间玩法" : currentRoomInfo.getRoomDesc());
        //是否密码房
        groupLock.setVisibility(TextUtils.isEmpty(currentRoomInfo.getRoomPwd()) ? View.GONE : View.VISIBLE);

        refreshPigFight();
    }

    // 刷新猪猪大作战UI
    public void refreshPigFight() {
//        RoomInfo roomInfo = getMvpPresenter().getCurrRoomInfo();
//        if (roomInfo == null) return;
//        List<RoomInfo.GameBean> roomGameConfig = roomInfo.getRoomGameConfig();
//        if (roomGameConfig == null) {
//            ivRoomPigFight.setVisibility(View.INVISIBLE);
//            return;
//        } else {
//            RoomInfo.GameBean pigGame = getMvpPresenter().getGame(RoomInfo.GameBean.GAME_TYPE_PIG);
//            if (pigGame == null) {
//                ivRoomPigFight.setVisibility(View.INVISIBLE);
//            } else if (pigGame.getIsOpen() == RoomInfo.GameBean.GAME_GAME_ON && roomInfo.getOpenPigFight() == 1) {
//                ivRoomPigFight.setVisibility(View.VISIBLE);
//            } else {
//                ivRoomPigFight.setVisibility(View.INVISIBLE);
//            }
//        }
    }

    @Override
    public void showGiftDialog(long targetUid) {
        if (getMvpPresenter().isRoomOwner(targetUid) || getMvpPresenter().isOnMic(targetUid)) {
            getGiftSelector().setupView(AvRoomDataManager.get().mMicQueueMemberMap, targetUid);
        } else {//如果对方不在麦上，则礼物弹框中只出现他自己就好了
            getGiftSelector().setupView(targetUid);
        }

        getGiftSelector().setGiftSelectotBtnClickListener(onGiftSelectorBtnClickListener);
        getGiftSelector().show();
    }

    // 猪猪大作战结果处理
    private void dealPFResult(PigFightBean pigFightBean) {
//        if (pigFightBean == null) return;
//        pigFightView.setupView(pigFightBean);
    }

    // 魅力值消息处理
    private void dealCPKMsg(CPKMsgBean cpkMsg) {
        if (cpkMsg == null)
            return;

        if (cpkMsg.getType() == CPKMsgBean.CPKMSG_TYPE_START) {
            startCPKCountDown(cpkMsg.getExpireSeconds());
        }
    }

    @Override
    public void startCPKCountDown(long expireSeconds) {
//        if (expireSeconds <= 0) {
//            return;
//        }
//        ivTimeCount.setVisibility(View.VISIBLE);
//        if (timer != null) {
//            timer.cancel();
//        }
//        timer = new CountDownTimer(expireSeconds * 1000, 1000) {
//            @Override
//            public void onTick(long l) {
//                SimpleDateFormat formatter = new SimpleDateFormat("mm:ss", Locale.getDefault());
//                String ss = formatter.format(l);
//                ivTimeCount.setText(ss);
//            }
//
//            @Override
//            public void onFinish() {
//                ivTimeCount.setVisibility(View.INVISIBLE);
//            }
//        };
//        timer.start();
    }

    // 显示魅力值PK结果弹框
    private void showCPKResultDialog(CPKResultBean bean) {
        if (bean == null) {
            LogUtil.i(TAG, "CPKResultBean is null");
            return;
        }

        if (bean.getType() == CPKResultBean.CPKRESULT_WIN || bean.getType() == CPKResultBean.CPKRESULT_DRAW) {
            CPKResultDialog cpkResultDialog = new CPKResultDialog(getContext());
            cpkResultDialog.show();
            cpkResultDialog.setUpView(bean);
        } else {
            SingleToastUtil.showToast("魅力值PK结果为：" + bean.getType());
        }
    }

    // 显示魅力值PK被邀请弹框
    private void showInviteCPKDialog(CPKInviteBean bean) {
        if (bean == null || bean.getNick() == null) {
            LogUtil.i(TAG, "CPKInviteBean or Nick is null");
            return;
        }
        String nick = bean.getNick();
        if (nick.length() > 6)
            nick = bean.getNick().substring(0, 6).concat("...");

        getDialogManager().showOkCancelDialog(nick + "邀请你参加魅力值PK，是否参加", "参加", "拒绝", true, new DialogManager.OkCancelDialogListener() {
            @Override
            public void onCancel() {

            }

            @Override
            public void onOk() {
                getDialogManager().dismissDialog();
                getMvpPresenter().agreeCharmPK(bean.getPkId(), bean.getRoomId(), bean.getUid());
            }
        });
    }

    /**
     * 宝箱激活文案
     *
     * @return
     */
    @Override
    protected int getLuckyBoxActivateTips() {
        return AvRoomDataManager.get().isOwnerOnMic() ? R.string.lucky_box_activate_broadcaster : R.string.lucky_box_activate_audience;
    }

    @Override
    public void refreshRoomAttentionStatus(Boolean status) {
        ivFollow.setVisibility(status ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void refreshAttention() {
        ivFollow.setVisibility(View.GONE);
        getMvpPresenter().checkAttention(getMvpPresenter().getCurrRoomInfo().getRoomId());
    }

    /**
     * 设置显示的赠送礼物消息
     * （XXX赠送给XXX YYY礼物）
     */
    private void setupGiftSendMsg(ChatRoomMessage chatRoomMessage) {
        boolean isMulti = chatRoomMessage.getAttachment().getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT;//是否是全麦赠送
        GiftAttachment attachment = (GiftAttachment) chatRoomMessage.getAttachment();
        GiftReceiveInfo giftReceiveInfo = attachment.getGiftRecieveInfo();
        if (giftReceiveInfo == null) {
            return;
        }

        if (getMvpPresenter().getCurrRoomInfo().getType() == ROOMTYPE_HOME_PARTY) {// 语音房
            GiftMsgView giftMsgView = getGiftMsgView();
            if (giftMsgView.getVisibility() != View.VISIBLE) {
                giftMsgView.setVisibility(View.VISIBLE);
                giftMsgView.setOnViewClickListener(onGiftMsgViewClickListener);
            }
            giftMsgView.setupView(isMulti, giftReceiveInfo);
        }
    }

    private void updateCharmData(RoomCharmAttachment roomCharmAttachment) {
        ((YiYaAudioMicroView) getMicroView()).getMicroViewAdapter().updateCharmData(roomCharmAttachment);

    }

    private void updateCharmFromRoom() {
        int value = 0;
        if (AvRoomDataManager.get().getRoomInfo() != null) {
            value = AvRoomDataManager.get().getRoomInfo().getCharmEnable();
        }
        if (value == 2) {
            AvRoomDataManager.get().setHasCharm(true);
        } else if (value == 1) {
            AvRoomDataManager.get().setHasCharm(false);
        }
        ((AudioMicroView) getMicroView()).getMicroViewAdapter().notifyDataSetChanged();
    }


    /**
     * 麦位点击监听
     */
    @Override
    public OnMicroItemClickListener getOnMicroItemClickListener() {
        return new OnMicroItemClickListener() {
            @Override
            public void onAvatarBtnClick(int micPosition, @Nullable IMChatRoomMember chatRoomMember) {
                final RoomInfo currentRoom = getMvpPresenter().getCurrRoomInfo();
                if (currentRoom == null || getMvpPresenter() == null) {
                    return;
                }

                // 麦上的人员信息,麦上的坑位信息
                RoomQueueInfo roomQueueInfo = getMvpPresenter().getMicQueueInfoByMicPosition(micPosition);
                RoomMicInfo roomMicInfo = roomQueueInfo.mRoomMicInfo;
                if (chatRoomMember == null && micPosition == AvRoomDataManager.MIC_POSITION_BY_OWNER) {
                    chatRoomMember = getMvpPresenter().getRoomOwnerDefaultMemberInfo();//房主的话，创建一下默认信息
                    chatRoomMember.setDefaultInfo(true);
                }
                if (chatRoomMember == null || roomMicInfo == null) {
                    return;
                }
                String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                boolean isMySelf = Objects.equals(currentUid, chatRoomMember.getAccount());
                boolean isTargetRoomAdmin = getMvpPresenter().isRoomAdmin(Long.valueOf(chatRoomMember.getAccount()));
                boolean isTargetRoomOwner = getMvpPresenter().isRoomOwner(Long.valueOf(chatRoomMember.getAccount()));
                final IMChatRoomMember finalChatRoomMember = chatRoomMember;

                List<ButtonItem> buttonItems = new ArrayList<>();
                if (getMvpPresenter().isRoomOwnerByMyself()) {//房主操作
                    if (!isMySelf) {
                        //点击不是自己
                        buttonItems.add(new ButtonItem("送礼物",
                                () -> showGiftDialog(Long.valueOf(finalChatRoomMember.getAccount()))));
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> showUserInfoDialog(Long.valueOf(finalChatRoomMember.getAccount()))));

                        buttonItems.add(ButtonItemFactory.createKickDownMicItem(chatRoomMember.getAccount()));//抱他下麦
                        buttonItems.add(roomMicInfo.isMicMute() ?//禁麦/解麦操作
                                ButtonItemFactory.createFreeMicItem(micPosition,
                                        () -> getMvpPresenter().operateMicroOpenOrClose(micPosition, true)) :
                                ButtonItemFactory.createLockMicItem(micPosition,
                                        () -> getMvpPresenter().operateMicroOpenOrClose(micPosition, false)));
                        buttonItems.add(ButtonItemFactory.createKickOutRoomItem(mContext, chatRoomMember,
                                String.valueOf(currentRoom.getRoomId()), chatRoomMember.getAccount()));// 踢出房间
                        buttonItems.add(ButtonItemFactory
                                .createMarkManagerListItem(String.valueOf(currentRoom.getRoomId()),// 设置管理员
                                        chatRoomMember.getAccount(), !isTargetRoomAdmin));
                        buttonItems.add(ButtonItemFactory.createAddRoomBlackListItem(getContext(), chatRoomMember, true));// 加入房间黑名单
                        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
                    } else {
                        showUserInfoDialog(JavaUtil.str2long(chatRoomMember.getAccount()));
                    }
                } else if (getMvpPresenter().isRoomAdminByMyself()) { //管理员操作
                    if (isMySelf) {//点击自己
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> showUserInfoDialog(Long.valueOf(finalChatRoomMember.getAccount()))));
                        buttonItems.add(ButtonItemFactory.createDownMicItem());//下麦旁听
                        buttonItems.add(roomMicInfo.isMicMute() ?//禁麦/解麦操作
                                ButtonItemFactory.createFreeMicItem(micPosition,
                                        () -> getMvpPresenter().operateMicroOpenOrClose(micPosition, true)) :
                                ButtonItemFactory.createLockMicItem(micPosition,
                                        () -> getMvpPresenter().operateMicroOpenOrClose(micPosition, false)));
                    } else {
                        buttonItems.add(new ButtonItem("送礼物",
                                () -> showGiftDialog(Long.valueOf(finalChatRoomMember.getAccount()))));
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> showUserInfoDialog(Long.valueOf(finalChatRoomMember.getAccount()))));
                        if (!isTargetRoomAdmin && !isTargetRoomOwner) {//他非房主或管理员
                            buttonItems.add(ButtonItemFactory.createKickDownMicItem(chatRoomMember.getAccount()));//抱他下麦
                            buttonItems.add(roomMicInfo.isMicMute() ?//禁麦/解麦操作
                                    ButtonItemFactory.createFreeMicItem(micPosition,
                                            () -> getMvpPresenter().operateMicroOpenOrClose(micPosition, true)) :
                                    ButtonItemFactory.createLockMicItem(micPosition,
                                            () -> getMvpPresenter().operateMicroOpenOrClose(micPosition, false)));
                            buttonItems.add(ButtonItemFactory.createKickOutRoomItem(mContext, chatRoomMember,
                                    String.valueOf(currentRoom.getRoomId()), chatRoomMember.getAccount()));//踢出房间
                            buttonItems.add(ButtonItemFactory.createAddRoomBlackListItem(getContext(), chatRoomMember, false));// 加入房间黑名单
                            buttonItems.add(ButtonItemFactory.createAddUserBlackListItem(getContext(), chatRoomMember,
                                    String.valueOf(currentRoom.getRoomId())));// 加入用户黑名单
                        } else {
                            if (!roomMicInfo.isMicMute() && !isTargetRoomOwner) {
                                buttonItems.add(ButtonItemFactory.createLockMicItem(micPosition,
                                        () -> getMvpPresenter().operateMicroOpenOrClose(micPosition, false)));
                            }
                            if (!isTargetRoomOwner) {
                                buttonItems.add(ButtonItemFactory.createKickDownMicItem(chatRoomMember.getAccount()));
                            }
                            if (roomMicInfo.isMicMute()) {// 对于管理员和房主,只有解麦功能
                                buttonItems.add(ButtonItemFactory.createFreeMicItem(micPosition,
                                        () -> getMvpPresenter().operateMicroOpenOrClose(micPosition, true)));
                            }
                        }
                    }
                    getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
                } else {//游客操作
                    if (isMySelf) {
                        buttonItems.add(new ButtonItem("查看资料",
                                () -> showUserInfoDialog(Long.valueOf(finalChatRoomMember.getAccount()))));
                        buttonItems.add(ButtonItemFactory.createDownMicItem()); //下麦旁听
                        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
                    } else {
                        showGiftDialog(Long.valueOf(chatRoomMember.getAccount()));
                    }
                }
            }

            @Override
            public void onUpMicBtnClick(int micPosition) {
                RoomQueueInfo roomQueueInfo = getMvpPresenter().getMicQueueInfoByMicPosition(micPosition);
                if (roomQueueInfo != null && roomQueueInfo.mRoomMicInfo != null && getMvpPresenter() != null) {
                    if (getMvpPresenter().isRoomOwnerByMyself() || getMvpPresenter().isRoomAdminByMyself()) {//管理员或者房主
                        List<ButtonItem> buttonItems = new ArrayList<>(4);
                        buttonItems.add(new ButtonItem(getString(R.string.embrace_up_mic),
                                () -> RoomInviteActivity.openActivity(getActivity(), micPosition)));
                        buttonItems.add(new ButtonItem(
                                roomQueueInfo.mRoomMicInfo.isMicMute() ? getString(R.string.no_forbid_mic)
                                        : getString(R.string.forbid_mic), () -> {
                            getMvpPresenter().operateMicroOpenOrClose(micPosition,
                                    roomQueueInfo.mRoomMicInfo.isMicMute());
                        }));
                        buttonItems.add(
                                new ButtonItem(roomQueueInfo.mRoomMicInfo.isMicLock() ? getString(R.string.unlock_mic) :
                                        getString(R.string.lock_mic), () -> getMvpPresenter()
                                        .operateMicroLockOrUnLock(micPosition,
                                                !roomQueueInfo.mRoomMicInfo.isMicLock())));
                        if (!getMvpPresenter().isRoomOwnerByMyself()) {
                            buttonItems.add(new ButtonItem("移到此座位",
                                    () -> getMvpPresenter().operateUpMicro(micPosition, false)));
                        }
                        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel));
                    } else {
                        getMvpPresenter().operateUpMicro(micPosition, false);
                    }
                }
            }

            @Override
            public void onLockBtnClick(int micPosition) {
                getMvpPresenter().operateMicroLockOrUnLock(micPosition, false);
            }

            @Override
            public void onAvatarSendMsgClick(int micPosition) {
                if (ChatUtil.checkBanned() || getMvpPresenter() == null || inputMsgView == null) {
                    return;
                }
                RoomQueueInfo roomQueueInfo = getMvpPresenter().getMicQueueInfoByMicPosition(micPosition);
                if (roomQueueInfo == null) {
                    return;
                }
                IMChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
                if (chatRoomMember == null) {
                    return;
                }
                String str = "@" + chatRoomMember.getNick() + " ";
                if (inputMsgView.getVisibility() == View.VISIBLE) {
                    if (!TextUtils.isEmpty(inputMsgView.getInputText())) {
                        str = inputMsgView.getInputText() + str;
                    }
                    inputMsgView.setInputText(str);
                } else {
                    inputMsgView.setVisibility(View.VISIBLE);
                    inputMsgView.setInputText(str);
                    inputMsgView.showKeyBoard();
                }
            }
        };
    }

    /**
     * 点击赠送礼物消息监听
     */
    private GiftMsgView.OnViewClickListener onGiftMsgViewClickListener = new GiftMsgView.OnViewClickListener() {
        @Override
        public void onNickNameViewClick(long clickUserUid) {
            showUserInfoDialog(clickUserUid);
        }

        @Override
        public void onParentViewClick() {
            showGiftRecordDialog();
        }
    };

    /**
     * 显示礼物记录弹框
     */
    private void showGiftRecordDialog() {
        if (giftMsgList != null && !giftMsgList.isEmpty()) {
            giftRecordDialog = new GiftRecordDialog(mContext);
            giftRecordDialog.show();
            giftRecordDialog.setOnGiftRecordItemClickListener(onGiftRecordItemClickListener);
            giftRecordDialog.loadData(giftMsgList);
        }
    }

    /**
     * 礼物记录点击监听
     */
    private RoomGiftRecordAdapter.OnGiftRecordItemClickListener onGiftRecordItemClickListener = this::showUserInfoDialog;

    @Override
    public void onSetListener() {
        super.onSetListener();

//        fl_finish.setOnClickListener(v -> {
//            AvRoomDataManager.get().setMinimize(true);
//            getActivity().finish();
//        });

//        tv_room_desc.setOnClickListener(view -> {
//            if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
//                RoomTopicActivity.start(getContext());
//            } else {
//                RoomTopicDialog roomTopicDIalog = new RoomTopicDialog();
//                roomTopicDIalog.show(getChildFragmentManager());
//            }
//        });

        tv_online_num.setOnClickListener(v -> {
            ListDataDialog listDataDialog = ListDataDialog.newOnlineUserListInstance(getActivity(), false);
            listDataDialog.setOnlineItemClick(onlineItemClick);
            showDialog(listDataDialog);
        });

//        ivFollow.setOnClickListener(v -> {
//            if (ivFollow.isSelected()) {
//                getMvpPresenter().deleteAttention(getMvpPresenter().getCurrRoomInfo().getRoomId());
//            } else {
//                getMvpPresenter().roomAttention(getMvpPresenter().getCurrRoomInfo().getRoomId(), true);
//            }
//        });

//        rl_contribute.setOnClickListener(view -> RankingChatListActivity.start(getContext(), getMvpPresenter().getCurrRoomInfo().getUid() + "", getMvpPresenter().getCurrRoomInfo().getRoomId() + ""));

//        ivRoomPigFight.setOnClickListener(v -> showPigFight());

        messageGuideView.setMGuideClickListener((view, info, position) -> {
            getMvpPresenter().sendTextMsg(info.getText());
            getInputMsgView().setInputText("");
            AvRoomDataManager.get().setSendNewHandMsg(true);
            ll_MessageGuide.setVisibility(View.GONE);
        });

        flMgClose.setOnClickListener(v -> {
            ll_MessageGuide.setVisibility(View.GONE);
        });
    }

    @Override
    protected void refreshOnlineCount() {
        if (getMvpPresenter() == null || getMvpPresenter().getCurrRoomInfo() == null) {
            return;
        }

        int onlineNum = getMvpPresenter().getCurrRoomInfo().getOnlineNum();
        String result;
        if (onlineNum < 10000) {
            result = onlineNum + "人";
        } else {
            result = StringUtils.transformToW(onlineNum) + "万";
        }
        tv_online_num.setText(result);
    }

    /**
     * 配置音乐播放view
     */
    private void setupMusicPlayView() {
        //获取自己是否在麦上
        boolean isOnMic = AvRoomDataManager.get().isOwnerOnMic();
        ivMusic.setVisibility(isOnMic ? View.VISIBLE : View.GONE);
    }

    private void showMusicPlayerDialog() {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().getRoomInfo();
        if (currentRoomInfo == null) {
            return;
        }
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(currentRoomInfo.getUid());
        if (userInfo != null) {
            musicPlayerDialog = YiYaMusicPlayerDialog.newInstance(userInfo.getAvatar());
            musicPlayerDialog.show(getChildFragmentManager(), null);
        }
    }

    @Override
    public void refreshRich(List<RankingXCInfo.ListBean> data) {
        WhiteAvatarStackAdapter adapter = new WhiteAvatarStackAdapter();
        asvContributors.setAdapter(adapter);
        if (ListUtils.isListEmpty(data)) {
            ArrayList<String> list = new ArrayList<>();
            list.add("");
            list.add("");
            list.add("");
            asvContributors.setAvatarList(list);
            return;
        }
        addDisposable(Observable.fromIterable(data)
                .take(3)
                .map(RankingXCInfo.ListBean::getAvatar)
                .toList()
                .subscribe((strings, throwable) -> {
                    asvContributors.setAvatarList(strings);
                })
        );
    }

    @Override
    public void refreshCharm(List<RankingXCInfo.ListBean> data) {
        if (data == null) return;
//        ImageLoadUtils.loadAvatar(mContext, data.get(0).getAvatar(), civ_rank_right, true);
    }

    /**
     * 显示房间新手快捷语
     *
     * @param info
     */
    @Override
    public void showRecommendWorlds(RecommendWorldsInfo info) {
        if (info != null && ListUtils.isNotEmpty(info.getRecommendWorlds()) && !AvRoomDataManager.get().isSendNewHandMsg()) {
            messageGuideView.setData(info.getRecommendWorlds());
            ll_MessageGuide.setVisibility(View.VISIBLE);
        } else {
            ll_MessageGuide.setVisibility(View.GONE);
        }
    }


    @Override
    public void showReceivedRedPackageView(SendRedPackageInfo sendRedPackageInfo) {
        if (sendRedPackageInfo.getGetCoin() > 0) {//领取成功
            PublicRedPackageDialog publicRedPackageDialog = new PublicRedPackageDialog();
            Bundle bundle = new Bundle();
            bundle.putSerializable("sendRedPackageInfo", sendRedPackageInfo);
            publicRedPackageDialog.setArguments(bundle);
            publicRedPackageDialog.show(getChildFragmentManager(), "publicRedPackageDialog");
        } else {//红包已领完
            toast("红包已领完");
        }
    }

    @Override
    public void refreshRedPackageView(LinkedList<RoomRedPackageInfo> roomRedPackageInfos) {

    }

    /**
     * 在线列表点击事件监听
     */
    private OnlineUserFragment.OnlineItemClick onlineItemClick = new OnlineUserFragment.OnlineItemClick() {

        @Override
        public void onItemClick(OnlineChatMember chatRoomMember) {
            if (null != chatRoomMember && null != chatRoomMember.chatRoomMember) {
                IMChatRoomMember member = chatRoomMember.chatRoomMember;
                showUserInfoDialog(JavaUtil.str2long(member.getAccount()));
            }
        }

        @Override
        public void onHoldMicroClick(OnlineChatMember chatRoomMember) {
            if (chatRoomMember != null && chatRoomMember.chatRoomMember != null) {
                getMvpPresenter().holdUpMicro(Long.valueOf(chatRoomMember.chatRoomMember.getAccount()));
            }
        }
    };

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceiveRecentContactChanged(List<RecentContact> imMessages) {
        changeState();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        changeState();
    }

    private void changeState() {
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        if (unreadCount > 0) {
            bottomView.showMsgMark(true);
        } else {
            bottomView.showMsgMark(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //更新播放器状态
        if (musicPlayerDialog != null) {
            musicPlayerDialog.updateVoiceValue();
        }

        getMvpPresenter().refleshContribute();
        getMvpPresenter().getSendRedPackageRecord();
    }

    @Override
    public void onPause() {
        super.onPause();
        getMvpPresenter().clear();
    }

    @Override
    protected void releaseView() {
        super.releaseView();
        onGiftMsgViewClickListener = null;
        giftMsgList = null;
    }

    @Override
    public boolean onBack() {
        if (isResumed() && getGiftSelector().isShowing()) {
            getGiftSelector().hide();
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 当 Activity 关闭时，如果 CountDownTimer 没有倒计时完毕的话，就会在后台一直执行，
        // 并且持有 Activity 的引用，导致不必要的内存泄漏，甚至回调处理时发生空值异常错误。
        if (timer != null) {
            timer.cancel();
        }
    }


    @OnClick({R.id.iv_close, R.id.iv_share, R.id.iv_music, R.id.iv_notice, R.id.iv_contribution_more, R.id.ll_contributors, R.id.iv_follow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_close:
                //关闭
                ButtonItem exitRoomItem = new ButtonItem("退出房间", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        getMvpPresenter().exitRoom(null);
                        getActivity().finish();
                    }
                });
                ButtonItem miniItem = new ButtonItem("最小化", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        AvRoomDataManager.get().setMinimize(true);
                        getActivity().finish();
                    }
                });
                ButtonItem reportItem = new ButtonItem("举报", new ButtonItem.OnClickListener() {
                    @Override
                    public void onClick() {
                        List<ButtonItem> buttons = new ArrayList<>();
                        ButtonItem button1 = new ButtonItem("政治敏感", () -> {
                            if (AvRoomDataManager.get().getRoomInfo() != null) {
                                ButtonItemFactory.reportCommit(mContext, 2, 1, AvRoomDataManager.get().getRoomInfo().getUid());
                            }
                        });
                        ButtonItem button2 = new ButtonItem("色情低俗", () -> {
                            if (AvRoomDataManager.get().getRoomInfo() != null) {
                                ButtonItemFactory.reportCommit(mContext, 2, 2, AvRoomDataManager.get().getRoomInfo().getUid());
                            }
                        });
                        ButtonItem button3 = new ButtonItem("广告骚扰", () -> {
                            if (AvRoomDataManager.get().getRoomInfo() != null) {
                                ButtonItemFactory.reportCommit(mContext, 2, 3, AvRoomDataManager.get().getRoomInfo().getUid());
                            }
                        });
                        ButtonItem button4 = new ButtonItem("人身攻击", () -> {
                            if (AvRoomDataManager.get().getRoomInfo() != null) {
                                ButtonItemFactory.reportCommit(mContext, 2, 4, AvRoomDataManager.get().getRoomInfo().getUid());
                            }
                        });
                        buttons.add(button1);
                        buttons.add(button2);
                        buttons.add(button3);
                        buttons.add(button4);
                        getDialogManager().showCommonPopupDialog(buttons, "取消");
                    }
                });
                List<ButtonItem> list = new ArrayList<>();
                list.add(exitRoomItem);
                list.add(miniItem);
                list.add(reportItem);
                getDialogManager().showCommonPopupDialog(list, "取消", true);
                break;
            case R.id.iv_share:
                //分享
                UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomShare());
                showShareDialog();
                break;
            case R.id.iv_music:
                showMusicPlayerDialog();
                break;
            case R.id.iv_notice:
                //房间玩法
                if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
                    RoomTopicActivity.start(getContext());
                } else {
                    RoomTopicDialog roomTopicDIalog = new RoomTopicDialog();
                    roomTopicDIalog.show(getChildFragmentManager());
                }
                break;
            case R.id.ll_contributors:
                //贡献榜
                UIHelper.showContributionList(getContext());
                break;
            case R.id.iv_follow:
                //关注
                getMvpPresenter().roomAttention(getMvpPresenter().getCurrRoomInfo().getRoomId(), true);
                break;
        }
    }
}


