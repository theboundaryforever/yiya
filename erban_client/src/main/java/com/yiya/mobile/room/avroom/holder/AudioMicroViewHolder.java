package com.yiya.mobile.room.avroom.holder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.yiya.mobile.room.avroom.other.OnMicroItemClickListener;
import com.yiya.mobile.ui.common.widget.CircleImageView;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.juxiao.library_ui.widget.WaveView;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomCharmInfo;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import static com.yiya.mobile.room.avroom.adapter.AudioMicroViewAdapter.TYPE_INVALID;

public class AudioMicroViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
    OnMicroItemClickListener onMicroItemClickListener;

    TextView tvNick;
    TextView tv_mic_position;
    ImageView ivUpImage;
    ImageView ivLockImage;
    ImageView ivMuteImage;
    ImageView ivGender;
    CircleImageView ivAvatar;
    FrameLayout rlMicLayout;
    WaveView waveView;
    View avatarBg;
    ImageView ivHeadWear;
    Context context;
    LinearLayout charmLl;
    TextView charmTv;
    ImageView charmHatIv;

    public AudioMicroViewHolder(Context context, View itemView) {
        super(itemView);
        this.context = context;
        waveView = itemView.findViewById(R.id.wave_view);
        rlMicLayout = itemView.findViewById(R.id.micro_layout);
        ivGender = itemView.findViewById(R.id.iv_mic_gender);
        tv_mic_position = itemView.findViewById(R.id.tv_mic_position);
        ivUpImage = itemView.findViewById(R.id.up_image);
        ivLockImage = itemView.findViewById(R.id.lock_image);
        ivMuteImage = itemView.findViewById(R.id.mute_image);
        ivAvatar = itemView.findViewById(R.id.avatar);
        tvNick = itemView.findViewById(R.id.nick);
        avatarBg = itemView.findViewById(R.id.avatar_bg);
        ivHeadWear = itemView.findViewById(R.id.iv_headwear);
        charmHatIv = itemView.findViewById(R.id.iv_charm_hat);

        charmLl = itemView.findViewById(R.id.charm_ll);
        charmTv = itemView.findViewById(R.id.charm_tv);

        ivUpImage.setOnClickListener(this);
        ivLockImage.setOnClickListener(this);
        ivAvatar.setOnLongClickListener(this);
    }

    private RoomQueueInfo info;
    private int position = TYPE_INVALID;

    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        this.onMicroItemClickListener = onMicroItemClickListener;
    }

    public void clear() {
        onMicroItemClickListener = null;
        info = null;
        position = TYPE_INVALID;
        rlMicLayout.setBackground(null);
        rlMicLayout.clearAnimation();
        waveView.stop();
        ivUpImage.setVisibility(View.VISIBLE);
        ivLockImage.setVisibility(View.GONE);
        ivMuteImage.setVisibility(View.GONE);
        ivAvatar.setVisibility(View.GONE);
        avatarBg.setVisibility(View.GONE);
        ivGender.setVisibility(View.GONE);
        tvNick.setText("");
        tv_mic_position.setText("");


    }

    public void bind(RoomQueueInfo info, int position) { // 传过来的是麦位
        this.info = info;
        if (info == null) {
            return;
        }
        this.position = position;
        RoomMicInfo roomMicInfo = info.mRoomMicInfo;
        IMChatRoomMember chatRoomMember = info.mChatRoomMember;
        // 清除动画
        waveView.stop();
        rlMicLayout.setBackground(null);
        rlMicLayout.clearAnimation();
        if (roomMicInfo == null) {
            ivUpImage.setVisibility(View.VISIBLE);
            ivLockImage.setVisibility(View.GONE);
            ivMuteImage.setVisibility(View.GONE);
            ivAvatar.setVisibility(View.GONE);
            avatarBg.setVisibility(View.GONE);
            tvNick.setText("");
            tv_mic_position.setText("");
            ivGender.setVisibility(View.GONE);
            return;
        }

        //显示，先展示人，无视麦的锁
        if (chatRoomMember != null) {
            ivLockImage.setVisibility(View.GONE);
            ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
            tv_mic_position.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(chatRoomMember.getAccount()) && JavaUtil.str2long(chatRoomMember.getAccount()) > 0) {
                ivUpImage.setVisibility(View.GONE);
                ivAvatar.setVisibility(View.VISIBLE);
                ivAvatar.setOnClickListener(v -> onMicroItemClickListener.onAvatarBtnClick(position, chatRoomMember));
                avatarBg.setVisibility(View.VISIBLE);
                tvNick.setVisibility(View.VISIBLE);
                ivGender.setVisibility(View.VISIBLE);

                if ((AvRoomDataManager.get().getRoomInfo() != null && AvRoomDataManager.get().getRoomInfo().getCharmEnable() == 2) || AvRoomDataManager.get().isHasCharm()) {
                    if (this instanceof AudioBossMicroViewHolder) {
                        charmLl.setVisibility(View.GONE);
                    } else {
                        charmLl.setVisibility(View.VISIBLE);
                    }

                    if (AvRoomDataManager.get().getMicCharmInfo() != null) {
                        RoomCharmInfo roomCharmInfo = AvRoomDataManager.get().getMicCharmInfo().get(chatRoomMember.getAccount());
                        if (roomCharmInfo != null) {
                            charmTv.setText(StringUtils.charmCalculate(roomCharmInfo.getValue()));
                            if (roomCharmInfo.isWithHat()) {
                                if (roomCharmInfo.getType() == 1) {
                                    charmHatIv.setImageResource(R.mipmap.icon_margin_hat);
                                } else if (roomCharmInfo.getType() == 2) {
                                    charmHatIv.setImageResource(R.mipmap.icon_poison_hat);
                                }
                                charmHatIv.setVisibility(View.VISIBLE);
                            } else {
                                charmHatIv.setVisibility(View.GONE);
                            }
                        } else {
                            charmTv.setText("0");
                            charmHatIv.setVisibility(View.GONE);
                        }
                    } else {
                        charmTv.setText("0");
                        charmHatIv.setVisibility(View.GONE);
                    }
                } else {
                    charmLl.setVisibility(View.INVISIBLE);
                    charmHatIv.setVisibility(View.GONE);
                }

                if (chatRoomMember.getGender() == 1) { // 男
                    ivGender.setImageDrawable(context.getResources().getDrawable((R.mipmap.ic_room_male)));
                } else {
                    ivGender.setImageDrawable(context.getResources().getDrawable((R.mipmap.ic_room_female)));
                }

                tvNick.setText(chatRoomMember.getNick());

                //房主位不需要设置
//                int p = (position + 1);
//                if (p != 0) {
                tv_mic_position.setText(String.valueOf(position));
//                }

                ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), chatRoomMember.getAvatar(), ivAvatar);

                //--------头饰-----------
                String headwearUrl = chatRoomMember.getHeadwear_url();
                if (!TextUtils.isEmpty(headwearUrl)) {
                    ImageLoadUtils.loadImage(context, headwearUrl, ivHeadWear);
                    ivHeadWear.setVisibility(View.VISIBLE);
                } else {
                    ivHeadWear.setVisibility(View.GONE);
                }
                //--------头饰-----------
//
                GlideApp.with(context)
                        .load(chatRoomMember.getAvatar())
                        .placeholder(R.drawable.nim_avatar_default)
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object o,
                                                        Target<Drawable> target, boolean b) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable drawable, Object o,
                                                           Target<Drawable> target,
                                                           DataSource dataSource, boolean b) {
                                ivAvatar.setImageDrawable(drawable);
                                return true;
                            }
                        })
                        .into(ivAvatar);
            } else {
                ivUpImage.setVisibility(View.VISIBLE);
                ivAvatar.setVisibility(View.GONE);
                avatarBg.setVisibility(View.GONE);
                tvNick.setText("");
                tv_mic_position.setText("");
                ivGender.setVisibility(View.GONE);

                charmLl.setVisibility(View.INVISIBLE);
            }
        } else {
            tv_mic_position.setVisibility(View.GONE);
            charmLl.setVisibility(View.INVISIBLE);
            //锁麦
            if (roomMicInfo.isMicLock()) {
                ivUpImage.setVisibility(View.GONE);
                ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
                ivLockImage.setVisibility(View.VISIBLE);
                ivAvatar.setVisibility(View.GONE);
                avatarBg.setVisibility(View.GONE);
            } else {
                ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
                ivUpImage.setVisibility(View.VISIBLE);
                ivAvatar.setVisibility(View.GONE);
                avatarBg.setVisibility(View.GONE);
                ivLockImage.setVisibility(View.GONE);
            }

            tvNick.setText(position + "号麦位");


            ivGender.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {
        if (info == null || position == TYPE_INVALID || onMicroItemClickListener == null)
            return;
        if (v.getId() == R.id.up_image || v.getId() == R.id.lock_image) {
//            if (position == -1) return;
            onMicroItemClickListener.onUpMicBtnClick(position);
        } else if (v.getId() == R.id.lock_image) {
//            if (position == -1) return;
            onMicroItemClickListener.onLockBtnClick(position);
        }
//        else if (v.getId() == R.id.tv_room_desc) {
//            onMicroItemClickListener.onRoomSettingsClick();
//
//        }
//        else if (v.getId() == R.id.tv_room_people) {
//            onMicroItemClickListener.onOnlinePeopleClick();
//        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (onMicroItemClickListener != null) {
            onMicroItemClickListener.onAvatarSendMsgClick(position);
        }
        return true;
    }
}
