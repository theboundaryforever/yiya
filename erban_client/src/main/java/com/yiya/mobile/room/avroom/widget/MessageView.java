package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.yiya.mobile.room.avroom.adapter.MessageAdapter;
import com.yiya.mobile.room.avroom.other.ScrollSpeedLinearLayoutManger;
import com.yiya.mobile.ui.widget.itemdecotion.DividerItemDecoration;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.im.IMReportRoute;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.SpUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * 直播间消息界面
 *
 * @author chenran
 * @date 2017/7/26
 */
public class MessageView extends FrameLayout {
    private RecyclerView messageListView;
    private TextView tvBottomTip;
    private MessageAdapter mMessageAdapter;
    private List<ChatRoomMessage> tempMessages;
    private ScrollSpeedLinearLayoutManger layoutManger;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public MessageView(Context context) {
        this(context, null);
    }

    public MessageView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public MessageView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (compositeDisposable == null)
            return;
        compositeDisposable.add(IMNetEaseManager.get().getChatRoomMsgFlowable()
                .subscribe(messages -> {
                    if (messages.size() == 0) return;
                    ChatRoomMessage chatRoomMessage = messages.get(0);
//                    if (checkNoNeedMsg(chatRoomMessage)) return;
                    onCurrentRoomReceiveNewMsg(messages);
                }));
        compositeDisposable.add(IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(roomEvent -> {
                    if (roomEvent == null ||
                            roomEvent.getEvent() != RoomEvent.RECEIVE_MSG) return;
                    ChatRoomMessage chatRoomMessage = roomEvent.getChatRoomMessage();

//                    if (checkNoNeedMsg(chatRoomMessage)) return;

                    tempMessages.clear();
                    tempMessages.add(chatRoomMessage);
                    onCurrentRoomReceiveNewMsg(tempMessages);
                }));
    }

    /**
     * 检查是否是公屏需要的消息，送礼物和谁来了的消息不加入公屏
     *
     * @param chatRoomMessage
     * @return
     */
    private boolean checkNoNeedMsg(ChatRoomMessage chatRoomMessage) {
//        if (chatRoomMessage == null) return true;
//        if (chatRoomMessage.getMsgType() == MsgTypeEnum.notification) return true;
        if (IMReportRoute.sendMessageReport.equalsIgnoreCase(chatRoomMessage.getRoute())) {
            IMCustomAttachment attachment = (IMCustomAttachment) chatRoomMessage.getAttachment();
            //增加second判断避免将房间分享消息屏蔽掉（问题：分享成功后没显示分享消息，最小化后重新进入又出现）
            if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_ROOM_TIP
                    && attachment.getSecond() == IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_ATTENTION_ROOM_OWNER) {
                return true;
            }
            if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    public void clear() {
        if (mMessageAdapter != null) {
            mMessageAdapter.getData().clear();
            mMessageAdapter.notifyDataSetChanged();
        }
    }

    private void init(Context context) {
        // 内容区域
        layoutManger = new ScrollSpeedLinearLayoutManger(context);
        FrameLayout.LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.rightMargin = DisplayUtils.dip2px(context, 110);
        messageListView = new RecyclerView(context);
        messageListView.setLayoutParams(params);
        messageListView.setOverScrollMode(OVER_SCROLL_NEVER);
        messageListView.setHorizontalScrollBarEnabled(false);
        addView(messageListView);
        messageListView.setLayoutManager(layoutManger);
        messageListView.addItemDecoration(new DividerItemDecoration(context, layoutManger.getOrientation(), 3, R.color.transparent));
        mMessageAdapter = new MessageAdapter();
        messageListView.setAdapter(mMessageAdapter);

        // 底部有新消息
        tvBottomTip = new TextView(context);
        FrameLayout.LayoutParams params1 = new LayoutParams(DisplayUtils.dip2px(context, 165F), DisplayUtils.dip2px(context, 35));
        params1.gravity = Gravity.BOTTOM | Gravity.CENTER;
        params1.bottomMargin = DisplayUtils.dip2px(context, 0);
        tvBottomTip.setBackgroundResource(R.drawable.bg_pink_bottom_new_msg);
        tvBottomTip.setLayoutParams(params1);
        tvBottomTip.setVisibility(GONE);
        tvBottomTip.setOnClickListener(v -> {
            tvBottomTip.setVisibility(GONE);
//            messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);// 底部显示不全
            layoutManger.scrollToPositionWithOffset(mMessageAdapter.getItemCount() - 1, 0);
        });
        addView(tvBottomTip);

        tempMessages = new ArrayList<>();
        initData();

        messageListView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (needUpdateDataSet && newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    needUpdateDataSet = false;
                    // 如果用户在滑动,并且显示的是tip,则需要跟新
                    mMessageAdapter.notifyDataSetChanged();
                }
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (layoutManger.findLastVisibleItemPosition() == mMessageAdapter.getItemCount() - 1) {
                        tvBottomTip.setVisibility(GONE);
                    }
                }
            }
        });

    }

    public MessageAdapter getAdapter() {
        return mMessageAdapter;
    }

    private void initData() {
        List<ChatRoomMessage> messages = IMNetEaseManager.get().messages;
        mMessageAdapter.setNewData(new ArrayList<>());
        if (!ListUtils.isListEmpty(messages)) {
            messages = msgFilter(messages);
            mMessageAdapter.addData(messages);
            messageListView.scrollToPosition(messages.size() - 1);
        }
    }

    public void onCurrentRoomReceiveNewMsg(List<ChatRoomMessage> messages) {
        if (ListUtils.isListEmpty(messages)) return;
        showTipsOrScrollToBottom(messages);
    }

    private boolean needUpdateDataSet = false;

    private List<ChatRoomMessage> msgFilter(List<ChatRoomMessage> chatRoomMessages) {
        List<ChatRoomMessage> messages = new ArrayList<>();
        for (ChatRoomMessage message : chatRoomMessages) {
            IMCustomAttachment attachment = message.getAttachment();
            if (attachment != null) {
//                if (((IMCustomAttachment) message.getAttachment()).getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM
//                        || ((IMCustomAttachment) message.getAttachment()).getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
//                        || ((IMCustomAttachment) message.getAttachment()).getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                if (((IMCustomAttachment) attachment).getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM) {
                    continue;
                }
                if (((IMCustomAttachment) attachment).getSecond() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_SECOND_ADD) {//pk投票消息
                    continue;
                }
                //是否屏蔽公屏种豆消息
                if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_LOTTERY_BOX) {
                    if (attachment instanceof LotteryBoxAttachment) {
                        boolean isPlantBeanMsgEnabled = (boolean) SpUtils.get(BasicConfig.INSTANCE.getAppContext(), Constants.KEY_PLANT_BEAN_MSG, true);
                        if (!isPlantBeanMsgEnabled && CoreManager.getCore(IAuthCore.class).getCurrentUid() != ((LotteryBoxAttachment) attachment).getUid()) {
                            //过滤他人种豆公屏消息
                            continue;
                        }
                    }
                }
                //语聊房是否屏蔽礼物消息
                RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
                if (roomInfo != null && roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                    if (roomInfo.isBlockGiftMsg()) {
                        if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT//公屏赠送礼物
                                || attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {//全麦赠送
                            continue;
                        }
                    }
                }
            }
            messages.add(message);
        }
        return messages;
    }

    private void showTipsOrScrollToBottom(List<ChatRoomMessage> messages) {
        // 最后一个item是否显示出来
        int lastCompletelyVisibleItemPosition = layoutManger.findLastVisibleItemPosition();
        boolean needScroll = (lastCompletelyVisibleItemPosition == mMessageAdapter.getItemCount() - 1);
        messages = msgFilter(messages);
        mMessageAdapter.addData(messages);

        if (needScroll) {
            needUpdateDataSet = false;
            tvBottomTip.setVisibility(GONE);
//            messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);//偶显拉倒底部显示不全
            layoutManger.scrollToPositionWithOffset(mMessageAdapter.getItemCount() - 1, 0);
        } else {
            needUpdateDataSet = true;
            tvBottomTip.setVisibility(VISIBLE);
        }
    }

    public void release() {
    }

    public interface OnMsgContentClickListener {
        /**
         * @param uid 被点击内容的发送者uid
         */
        void onClick(long uid);
    }
}

