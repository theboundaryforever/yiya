package com.yiya.mobile.room.avroom.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;

import com.yiya.mobile.base.adapter.BaseIndicatorAdapter;
import com.yiya.mobile.base.fragment.BaseFragment;
import com.yiya.mobile.constant.Extras;
import com.yiya.mobile.ui.home.adpater.YiYaRoomChatIndicatorAdapter;
import com.yiya.mobile.ui.message.fragment.RecentListFragment;
import com.yiya.mobile.ui.square.fragment.SquareFragment;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件描述：
 * 房间聊天相关功能模块入口：消息和大厅
 *
 * @auther：zwk
 * @data：2019/1/2
 */
public class RoomChatFragment extends BaseFragment implements YiYaRoomChatIndicatorAdapter.OnItemSelectListener {

    public static final String SHOW_SQUARE = "SHOW_SQUARE";
    public static final String CHARM_LEVEL = "CHARM_LEVEL";

    private MagicIndicator indicator;

    private ViewPager vpRoomChat;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_room_chat;
    }

    @Override
    public void onFindViews() {
        indicator = (MagicIndicator) mView.findViewById(R.id.mi_room_chat);
        vpRoomChat = (ViewPager) mView.findViewById(R.id.vp_room_chat);
        List<Fragment> fragments = new ArrayList<>();
        RecentListFragment recentList = new RecentListFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Extras.EXTRA_ROOM_PRIVATE, true);

        recentList.setArguments(bundle);
        fragments.add(recentList);
        SquareFragment square = new SquareFragment();
        List<TabInfo> tabInfoList = new ArrayList<>();
//        NewUserFragment newUserFragment = new NewUserFragment();

        tabInfoList.add(new TabInfo(-2, "私信"));
        if (getArguments() != null) {
            boolean aBoolean = getArguments().getBoolean(SHOW_SQUARE, false);
            if (aBoolean) {
                tabInfoList.add(new TabInfo(-1, "公聊"));
                fragments.add(square);
//                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCurrentUserInfo();
//                if (userInfo != null && userInfo.getCharmLevel() >= 5){
//                    tabInfoList.add(new TabInfo(-3, "萌新"));
//                    fragments.add(newUserFragment);
//                }
            }
        }

        YiYaRoomChatIndicatorAdapter mMsgIndicatorAdapter = new YiYaRoomChatIndicatorAdapter(mContext, tabInfoList);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        //等分
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        indicator.setNavigator(commonNavigator);
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        vpRoomChat.setAdapter(new BaseIndicatorAdapter(getChildFragmentManager(), fragments));
        ViewPagerHelper.bind(indicator, vpRoomChat);
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {

    }

    @Override
    public void onItemSelect(int position) {
        vpRoomChat.setCurrentItem(position);
    }

}
