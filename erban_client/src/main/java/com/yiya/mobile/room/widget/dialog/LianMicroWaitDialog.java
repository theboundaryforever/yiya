package com.yiya.mobile.room.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.bean.LianMicroStatusInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import org.jetbrains.annotations.NotNull;

/**
 * 连麦申请弹框
 *
 * @author zeda
 */
public class LianMicroWaitDialog extends BaseDialogFragment {

    private LianMicroStatusInfo statusInfo;

    public static LianMicroWaitDialog newInstance(@NonNull LianMicroStatusInfo statusInfo, @Nullable LianMicroDialogFactory.OnLianMicroDialogClickListener onLianMicroDialogClickListener) {
        LianMicroWaitDialog lianMicroApplyDialog = new LianMicroWaitDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("status_info", statusInfo);
        lianMicroApplyDialog.setArguments(bundle);
        lianMicroApplyDialog.setOnLianMicroDialogClickListener(onLianMicroDialogClickListener);
        return lianMicroApplyDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            statusInfo = (LianMicroStatusInfo) arguments.getSerializable("status_info");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View rootView = inflater.inflate(R.layout.dialog_lian_micro_wait, window.findViewById(android.R.id.content), false);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setWindowAnimations(R.style.WindowBottomAnimationStyle);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View rootView, @Nullable Bundle savedInstanceState) {
        rootView.findViewById(R.id.btn_room_lian_micro_cancel).setOnClickListener(v -> {
            if (onLianMicroDialogClickListener != null) {
                onLianMicroDialogClickListener.onCancelBtnClick();
            }
        });
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            ImageLoadUtils.loadMediumAvatar(getContext(), userInfo.getAvatar(), rootView.findViewById(R.id.iv_room_lian_micro_avatar), true);
        }
        //连麦类型
        if (statusInfo != null) {
            ((TextView) rootView.findViewById(R.id.tv_content)).setText(String.format("%s连麦申请中", statusInfo.isAudioMicroType() ? "语音" : "视频"));
        }
    }

    private LianMicroDialogFactory.OnLianMicroDialogClickListener onLianMicroDialogClickListener;

    public void setOnLianMicroDialogClickListener(LianMicroDialogFactory.OnLianMicroDialogClickListener onLianMicroDialogClickListener) {
        this.onLianMicroDialogClickListener = onLianMicroDialogClickListener;
    }

    @Override
    public void onDestroyView() {
        onLianMicroDialogClickListener = null;
        super.onDestroyView();
    }
}
