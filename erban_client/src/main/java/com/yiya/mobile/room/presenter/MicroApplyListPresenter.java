package com.yiya.mobile.room.presenter;

import android.annotation.SuppressLint;

import com.yiya.mobile.room.model.RoomModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo.MicroApplyInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/6/14.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MicroApplyListPresenter extends AbstractMvpPresenter<IMicroApplyListView> {

    //除RoomModel外，AvRoomDataManager、RtcEngineManager也属于model
    protected RoomModel model;

    public MicroApplyListPresenter() {
        model = new RoomModel();
    }

    /**
     * 获取连麦列表
     */
    public void getMicApplyList() {
        model.getMicApplyList(6, new HttpRequestCallBack<RoomMicroApplyInfo>() {
            @Override
            public void onFinish() {
                if (getMvpView() != null) {
                    getMvpView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(String message, RoomMicroApplyInfo roomMicroApplyInfo) {
                //显示连麦列表
                if (getMvpView() != null && roomMicroApplyInfo != null) {
                    getMvpView().showMicroApplyList(roomMicroApplyInfo);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                    getMvpView().showMicroApplyList(null);
                }
            }
        });
    }

    /**
     * 同意连麦
     */
    public void agreeMicroApply(MicroApplyInfo item) {
        model.agreeMicroApply(item.getUid(), new HttpRequestCallBack<RoomMicroApplyInfo>() {
            @Override
            public void onFinish() {
                if (getMvpView() != null) {
                    getMvpView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(String message, RoomMicroApplyInfo roomMicroApplyInfo) {
                if (getMvpView() != null && roomMicroApplyInfo != null) {
                    getMvpView().showMicroApplyList(roomMicroApplyInfo);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 拒绝连麦
     *
     * @param item
     * @param position
     */
    public void rejectMicroApply(MicroApplyInfo item, int position) {
        List<MicroApplyInfo> list = new LinkedList<>();
        list.add(item);
        rejectMicroApply(list);
    }

    /**
     * 拒绝所有连麦
     */
    @SuppressLint("CheckResult")
    public void rejectMicroApply(List<MicroApplyInfo> list) {
        Observable.fromIterable(list)
                .subscribeOn(Schedulers.io())
                .compose(bindToLifecycle())
                .map(MicroApplyInfo -> String.valueOf(MicroApplyInfo.getUid()))
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((tgUidList, throwable) -> {
                    String tgUid = StringUtils.join(tgUidList, ",");
                    model.rejectMicroApply(tgUid, new HttpRequestCallBack<RoomMicroApplyInfo>() {
                        @Override
                        public void onFinish() {
                            if (getMvpView() != null) {
                                getMvpView().dismissDialog();
                            }
                        }

                        @Override
                        public void onSuccess(String message, RoomMicroApplyInfo roomMicroApplyInfo) {
                            if (getMvpView() != null) {
                                getMvpView().showMicroApplyList(roomMicroApplyInfo);
                            }
                        }

                        @Override
                        public void onFailure(int code, String msg) {
                            if (getMvpView() != null) {
                                getMvpView().toast(msg);
                            }
                        }
                    });
                });
    }

    /**
     * 设置连麦申请开关
     *
     * @param isCanConnectMic
     */
    public void setRoomConnectMic(boolean isCanConnectMic) {
        model.setRoomConnectMic(isCanConnectMic ? 1 : 0, 6, new HttpRequestCallBack<RoomInfo>() {

            @Override
            public void onFinish() {
                if (getMvpView() != null) {
                    getMvpView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(String message, RoomInfo roomInfo) {
                if (roomInfo == null) {
                    return;
                }
                if (AvRoomDataManager.get().getRoomInfo() != null) {
                    AvRoomDataManager.get().getRoomInfo().setIsCanConnectMic(roomInfo.getIsCanConnectMic());
                }
                if (getMvpView() != null) {
                    getMvpView().setRoomConnectMicSuccess(roomInfo.getIsCanConnectMic() == 1);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 挂断
     *
     * @param microPosition
     * @param callBack
     */
    public void downMicro(int microPosition, CallBack<String> callBack) {
        IMNetEaseManager.get().downMicroPhoneBySdk(microPosition, callBack);
    }
}
