package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.listener.SingleClickListener;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyBoxInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ProjectName:
 * Description:幸运宝箱view
 * Created by BlackWhirlwind on 2019/7/9.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class LuckyBoxView extends ConstraintLayout {

    @BindView(R.id.iv_lucky_box)
    ImageView ivLuckyBox;
    @BindView(R.id.tv_gold)
    TextView tvGold;
    @BindView(R.id.tv_count_down)
    TextView tvCountDown;
    private CountDownTimer mCountDownTimer;
    private OnLuckyBoxViewClickListener mListener;

    public LuckyBoxView(Context context) {
        this(context, null);
    }

    public LuckyBoxView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LuckyBoxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.layout_lucky_box, this);
        ButterKnife.bind(this);
    }

    public void setData(RoomLuckyBoxInfo roomLuckyBoxInfo) {
        if (roomLuckyBoxInfo == null) {
            return;
        }
        release();

        //是否已激活
        boolean isActivate = roomLuckyBoxInfo.isActivate();
        //宝箱背景
        ivLuckyBox.setBackgroundResource(isActivate ? R.drawable.bg_lucky_box_next : R.drawable.bg_lucky_box);

        String currentTreasure = StringUtils.transformToW(roomLuckyBoxInfo.getCurrentTreasure());
        String nextTreasure = StringUtils.transformToW(roomLuckyBoxInfo.getNextTreasure());
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.color_F1FF11));

        if (isActivate) {
            //已激活
            SpannableStringBuilder ssb = new SpannableStringBuilder(currentTreasure);
            ssb.setSpan(colorSpan, 0, ssb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvGold.setText(ssb.append("金币"));
//            tvCountDown.setText(String.format("下一轮:%s金币", nextTreasure));
            tvCountDown.setText("探险夺宝中");
        } else {
            //待激活
            SpannableStringBuilder ssb = new SpannableStringBuilder(nextTreasure);
            ssb.setSpan(colorSpan, 0, ssb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvGold.setText(ssb.append("金币"));
            startCountDown(roomLuckyBoxInfo);
        }
    }

    private void startCountDown(RoomLuckyBoxInfo roomLuckyBoxInfo) {
        release();
        mCountDownTimer = new CountDownTimer(roomLuckyBoxInfo.getActivateTime(), 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
                String time = sdf.format(new Date(millisUntilFinished));
                tvCountDown.setText(time);
            }

            @Override
            public void onFinish() {
                if (mListener != null) {
                    mListener.refreshLuckyBox();
                }
            }
        };
        mCountDownTimer.start();
    }

    public void setOnLuckyBoxViewClickListener(OnLuckyBoxViewClickListener listener) {
        if (listener != null) {
            mListener = listener;
            setOnClickListener(new SingleClickListener() {
                @Override
                public void singleClick(View v) {
                    listener.onLuckyBoxViewClick();
                }
            });
        }
    }

    public void release() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    public interface OnLuckyBoxViewClickListener {

        void onLuckyBoxViewClick();

        void refreshLuckyBox();
    }

}
