package com.yiya.mobile.room.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.widget.HeadWearImageView;
import com.yiya.mobile.utils.HttpUtil;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;
import com.yiya.mobile.view.LevelView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author xiaoyu
 * @date 2017/12/13
 */

public class UserInfoDialog extends Dialog implements View.OnClickListener {
    private static final String TAG = "UserInfoDialog";
    private Context mContext;

    private FrameLayout fl_close;
    private HeadWearImageView avatar;
    private TextView nick;
    private TextView erbanId;
    private TextView tv_private_msg;
    private TextView fansNumber;
    private TextView attentionNumber;
    private TextView tv_follow;
    private TextView tv_send_gift;
    private TextView tv_home;
    private ImageView iv_sex;
    private FrameLayout fl_more;
    private FrameLayout fl_manage;
    private LevelView mLevelView;

    private LinearLayout bottom_layout;

    @BindView(R.id.iv_user_level)
    ImageView ivUserLevel;
    @BindView(R.id.iv_exper_level)
    ImageView ivExperLevel;
    @BindView(R.id.iv_charm_level)
    ImageView ivCharmLevel;

    private long attentionCount = 0;
    private long fansCount = 0;
    private long myUid;
    private long uid;
    private int roomType;
    private boolean isAttention;
    private RoomInfo roomInfo;
    private UserInfo userInfo;

    public UserInfoDialog(Context context, long uid) {
        super(context, R.style.transDialog);
        this.mContext = context;
        this.uid = uid;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
        setContentView(R.layout.dialog_user_info);
        ButterKnife.bind(this);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        fl_close = (FrameLayout) findViewById(R.id.fl_close);
        avatar = findViewById(R.id.avatar);
        nick = (TextView) findViewById(R.id.nick);
        erbanId = (TextView) findViewById(R.id.tv_erban_id);
        fansNumber = (TextView) findViewById(R.id.fans_number);
        attentionNumber = findViewById(R.id.attention_number);
        iv_sex = (ImageView) findViewById(R.id.iv_sex);
        fl_more = (FrameLayout) findViewById(R.id.fl_more);
        fl_manage = (FrameLayout) findViewById(R.id.fl_manage);
        tv_private_msg = (TextView) findViewById(R.id.tv_private_msg);
        tv_send_gift = (TextView) findViewById(R.id.tv_send_gift);
        tv_follow = (TextView) findViewById(R.id.tv_follow);
        tv_home = findViewById(R.id.tv_home);
        mLevelView = (LevelView)findViewById(R.id.iv_lv);
        bottom_layout = (LinearLayout)findViewById(R.id.bottom_layout);

        tv_follow.setOnClickListener(this);
        tv_private_msg.setOnClickListener(this);
        tv_home.setOnClickListener(this);
        tv_send_gift.setOnClickListener(this);
        fl_more.setOnClickListener(this);
        fl_close.setOnClickListener(this);
        avatar.setOnClickListener(this);
        fl_manage.setOnClickListener(this);

        Window window = getWindow();
        if (window != null)
            window.setGravity(Gravity.CENTER);
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid, true);
        myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

        this.roomInfo = AvRoomDataManager.get().getRoomInfo();
        this.roomType = roomInfo.getType();

        if(roomType == RoomInfo.ROOMTYPE_HOME_PARTY){ // 语音房
            if (uid == myUid) { // 自己
                bottom_layout.setVisibility(View.INVISIBLE);
            } else {
                if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin()) {
                    fl_manage.setVisibility(View.VISIBLE);
                } else {
                    fl_more.setVisibility(View.VISIBLE);
                }
            }
        } else if(roomType == RoomInfo.ROOMTYPE_VIDEO_LIVE) {// 视频房
            if (uid == myUid) { // 自己
                bottom_layout.setVisibility(View.INVISIBLE);
            } else {
                fl_more.setVisibility(View.VISIBLE);
                tv_send_gift.setVisibility(View.GONE);
            }
        }

        if (uid != myUid){
            CoreManager.getCore(IPraiseCore.class).isPraised(myUid, uid);
        }
    }

    private void updateView() {
        if (userInfo != null) {
            //头像
            avatar.setAvatar(userInfo.getAvatar());
            //头饰
            avatar.setHeadWear(userInfo.getHeadwearUrl());
            nick.setText(userInfo.getNick());
            erbanId.setText(String.format(Locale.getDefault(),
                    getContext().getString(R.string.me_user_id), userInfo.getErbanNo()));
            Drawable drawable;
            if (userInfo.getGender() == 1) {
                drawable = mContext.getResources().getDrawable(R.mipmap.ic_ranking_male);
            } else {
                drawable = mContext.getResources().getDrawable(R.mipmap.ic_ranking_female);
            }
            iv_sex.setImageDrawable(drawable);
            //设置星座
            //设置关注数量
            attentionCount = userInfo.getFollowNum();
            attentionNumber.setText(String.valueOf(attentionCount));
            //设置粉丝数量
            fansCount = userInfo.getFansNum();
            fansNumber.setText(String.valueOf(fansCount));

            if(roomType == RoomInfo.ROOMTYPE_VIDEO_LIVE){// 直播房
                //用户等级
                if(ivUserLevel != null){
                    if (StringUtils.isNotEmpty(userInfo.getVideoRoomExperLevelPic())) {
                        ivUserLevel.setVisibility(View.VISIBLE);
                        ImageLoadUtils.loadImage(mContext, userInfo.getVideoRoomExperLevelPic(), ivUserLevel);
                    } else {
                        ivUserLevel.setVisibility(View.GONE);
                    }
                }
            } else{// 语音房
                // 财富等级
                if(ivExperLevel != null){
                    if (StringUtils.isNotEmpty(userInfo.getExperLevelPic())) {
                        ivExperLevel.setVisibility(View.VISIBLE);
                        ImageLoadUtils.loadImage(mContext, userInfo.getExperLevelPic(), ivExperLevel);
                    } else {
                        ivExperLevel.setVisibility(View.GONE);
                    }
                }
            }

            // 魅力等级
            if(ivCharmLevel != null){
                if (StringUtils.isNotEmpty(userInfo.getCharmLevelPic())) {
                    ivCharmLevel.setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(mContext, userInfo.getCharmLevelPic(), ivCharmLevel);
                } else {
                    ivCharmLevel.setVisibility(View.GONE);
                }
            }
        }
    }

    //------------------------------IPraiseClient--------------------------------
    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(Boolean islike, long uid) {
        isAttention = islike;
        if (islike) {
            tv_follow.setText("已关注");
            attentionNumber.setText(String.valueOf(attentionCount));
        } else {
            tv_follow.setText("关注");
            attentionNumber.setText(String.valueOf(attentionCount));
        }
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        isAttention = true;
        tv_follow.setText("已关注");
        fansCount++;
        fansNumber.setText(String.valueOf(fansCount));
        ((BaseActivity) getContext()).toast("关注成功，相互关注可成为好友哦！");
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long uid, boolean showNotice) {
        isAttention = false;
        tv_follow.setText("关注");
        if (fansCount > 0)
            fansCount--;
        fansNumber.setText(String.valueOf(fansCount));
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == uid) {
            this.userInfo = info;
            updateView();
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_follow:
                if (userInfo != null) {
                    if (isAttention) {
                        CoreManager.getCore(IPraiseCore.class).cancelPraise(userInfo.getUid(), true);
                    } else {
                        CoreManager.getCore(IPraiseCore.class).praise(userInfo.getUid());
                    }
                }
                break;
            case R.id.fl_close:
                dismiss();
                break;
            case R.id.tv_send_gift:
                if (listener != null) {
                    listener.onSendGiftClicked(uid);
                }
                dismiss();
                break;
            case R.id.fl_manage:
                if (listener != null) {
                    listener.onMenuClicked(uid);
                }
                dismiss();
                break;
            case R.id.fl_more:
                if (listener != null) {
                    listener.onMenuClicked(uid);
                }
                dismiss();
                break;
            case R.id.tv_home:
                if (userInfo != null) {
                    UIHelper.showUserInfoAct(mContext, userInfo.getUid());
                    dismiss();
                }
                break;
            case R.id.tv_private_msg:// 私聊
                if (userInfo != null){
                    boolean isFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(userInfo.getUid() + "");
                    HttpUtil.checkUserIsDisturb(getContext(), isFriend, userInfo.getUid());
                    dismiss();
                }
                break;
            default:
        }
    }

    private OnHandleEventListener listener;

    public void setOnHandleEventListener(OnHandleEventListener listener) {
        this.listener = listener;
    }

    public interface OnHandleEventListener {

        /**
         * 更多菜单
         */
        default void onMenuClicked(long uid) {

        }

        /**
         * 送礼物
         */
        default void onSendGiftClicked(long uid) {

        }

    };

    //处理Unable to add window -- token android.os.BinderProxy@f9e7485 is not valid; is your activity running?
    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //not attached to window manager
    @Override
    public void dismiss() {
        try {
            super.dismiss();
            roomInfo = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
