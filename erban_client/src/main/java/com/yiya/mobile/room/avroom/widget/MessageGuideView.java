package com.yiya.mobile.room.avroom.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.juxiao.library_ui.widget.DrawableTextView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.RecommendWorldsInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/12
 */
public class MessageGuideView extends LinearLayout {

//    private static final String TAG = "MessageGuideView";

    public interface OnItemMGuideClickListener {
        void onItemMGuideClick(View view, RecommendWorldsInfo.RecommendWorldsBean info, int position);
    }

    private Context mContext;

    public MessageGuideView(Context context) {
        super(context);
        initChildView(context);
    }

    public MessageGuideView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initChildView(context);
    }

    public MessageGuideView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initChildView(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MessageGuideView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initChildView(context);
    }

    private RecyclerView mRecyclerView;

    private MyRecyclerAdapter mAdapter;

    private List<RecommendWorldsInfo.RecommendWorldsBean> mData;

    private OnItemMGuideClickListener mListener;

    private void initChildView(Context context) {
        mContext = context;

        mData = new ArrayList<>();

        mRecyclerView = new RecyclerView(mContext);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mRecyclerView.setLayoutParams(layoutParams);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        addView(mRecyclerView);
        mAdapter = new MyRecyclerAdapter();

        mRecyclerView.setAdapter(mAdapter);

        mAdapter.notifyDataSetChanged();
    }

    public void setData(List<RecommendWorldsInfo.RecommendWorldsBean> data) {
        if (data != null) {
            mData.clear();
            mData.addAll(data);
        }
        mAdapter.notifyDataSetChanged();
    }

    public void setMGuideClickListener(OnItemMGuideClickListener listener) {
        mListener = listener;
    }


    private class MyRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.list_item_room_message_guide, null));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            RecommendWorldsInfo.RecommendWorldsBean info = mData.get(position);
            ItemViewHolder viewHolder = (ItemViewHolder) holder;
//            viewHolder.itemTextView.changeSoildColor(Color.parseColor(info.getColor()));
            viewHolder.itemTextView.setTextColor(Color.parseColor(info.getColor()));
            viewHolder.itemTextView.setText(info.getText());
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        private class ItemViewHolder extends RecyclerView.ViewHolder {

            private DrawableTextView itemTextView;

            public ItemViewHolder(View itemView) {
                super(itemView);
                itemTextView = (DrawableTextView) itemView.findViewById(R.id.dtv_item_title);
                itemTextView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = getAdapterPosition();
                        if (pos >= 0 && mListener != null) {
                            mListener.onItemMGuideClick(v, mData.get(pos), pos);
                        }
                    }
                });

            }
        }
    }

}
