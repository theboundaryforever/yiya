package com.yiya.mobile.room.presenter;

import com.yiya.mobile.room.model.RoomModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.xchat_core.audio.IAudioRecordView;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2018/1/4
 */
public class AudioRecordPresenter extends AbstractMvpPresenter<IAudioRecordView> {

    private final RoomModel roomModel;

    public AudioRecordPresenter() {
        roomModel = new RoomModel();
    }

    public void exitRoom() {
        roomModel.exitRoom(null);
    }
}
