package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * @author zdw
 * @date 2019/8/22
 * @describe 门槛动画 当送礼数达到一定数量就会触发
 */

public class GiftThresholdView extends RelativeLayout implements SVGACallback {
    public static final String TAG = "GiftThresholdView";

    private SVGAImageView svgaThreshold;// 门槛svga

    private boolean isAnim = false;//门槛svga 是否在播放

    private LinkedList<GiftReceiveInfo> giftVoInfos = new LinkedList<>();// 本地缓存

    public GiftThresholdView(Context context) {
        super(context);
        init();
    }

    public GiftThresholdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GiftThresholdView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_gift_threshold, this, true);

        giftVoInfos.clear();

        svgaThreshold = (SVGAImageView) findViewById(R.id.svga_threshold);

        svgaThreshold.setCallback(this);
    }

    public boolean isAnim() {
        return isAnim;
    }

    public void setAnim(boolean anim) {
        isAnim = anim;
    }

    public void setupView(GiftReceiveInfo receiveInfo) {
        giftVoInfos.addLast(receiveInfo);
        try {
            draw();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void draw() {
        if (isAnim || ListUtils.isListEmpty(giftVoInfos)) {
            return;
        }

        GiftReceiveInfo giftReceiveInfo = giftVoInfos.removeFirst();
        if (giftReceiveInfo == null){
            isAnim = false;
            return;
        }

        if (giftReceiveInfo.getGiftNum() > 1 ) {
            isAnim = false;
            return;
        }

        long comboFrequencyCount = giftReceiveInfo.getComboFrequencyCount();

        String source = null;
        Log.d(TAG, "draw: comboFrequencyCount = " + comboFrequencyCount);
        if (comboFrequencyCount == 10) {
            source = "10.svga";
        } else if (comboFrequencyCount == 66) {
            source = "66.svga";
        } else if (comboFrequencyCount == 99) {
            source = "99.svga";
        } else if (comboFrequencyCount == 188) {
            source = "188.svga";
        } else if (comboFrequencyCount == 520) {
            source = "520.svga";
        } else if (comboFrequencyCount == 1314) {
            source = "1314.svga";
        }
        if (StringUtils.isEmpty(source)) {
            isAnim = false;
            return;
        }

        SVGAParser parser = new SVGAParser(getContext());
        parser.decodeFromAssets(source, new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                svgaThreshold.setLoops(1);
                svgaThreshold.setImageDrawable(drawable);
                svgaThreshold.setClearsAfterStop(true);
                svgaThreshold.startAnimation();

                isAnim = true;
            }

            @Override
            public void onError() {

            }

        });
    }

    @Override
    public void onFinished() {
        isAnim = false;
        if (ListUtils.isNotEmpty(giftVoInfos)) {
            draw();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onRepeat() {

    }

    @Override
    public void onStep(int i, double v) {

    }
}
