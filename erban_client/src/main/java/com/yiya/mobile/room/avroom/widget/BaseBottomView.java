package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.yiya.mobile.room.avroom.other.BottomViewListenerWrapper;

public abstract class BaseBottomView extends LinearLayout {
    public BaseBottomView(Context context) {
        super(context);
    }

    public BaseBottomView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseBottomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void showUpMicBottom(){};

    public void showDownMicBottom(){};

    public abstract void setBottomViewCommonListener(BottomViewListenerWrapper wrapper);

    public void setRemoteMuteOpen(boolean isOpen){};

    public void setMicBtnEnable(boolean enable){};

    public void setMicBtnOpen(boolean isOpen){};

    public void setInputMsgBtnEnable(boolean enable){};
}
