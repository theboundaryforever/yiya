package com.yiya.mobile.room.widget.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.ui.widget.FlexUserInfoView;
import com.yiya.mobile.ui.widget.HeadWearImageView;
import com.yiya.mobile.utils.HttpUtil;
import com.yiya.mobile.utils.UIHelper;

import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author xiaoyu
 * @date 2017/12/13
 */

public class YiYaUserInfoDialog extends BaseTransparentDialogFragment {
    @BindView(R.id.avatar)
    HeadWearImageView avatar;
    @BindView(R.id.iv_more)
    ImageView ivMore;
    @BindView(R.id.nick)
    TextView nick;
    @BindView(R.id.tv_erban_id)
    TextView erbanId;
    @BindView(R.id.tv_home)
    TextView tv_home;
    @BindView(R.id.tv_send_gift)
    TextView tv_send_gift;
    @BindView(R.id.tv_private_msg)
    TextView tv_private_msg;
    @BindView(R.id.tv_follow)
    TextView tv_follow;
    @BindView(R.id.group_bottom)
    Group groupBottom;
    @BindView(R.id.flex_user_view)
    FlexUserInfoView flexUserView;

    private long attentionCount = 0;
    private long fansCount = 0;
    private long myUid;
    private long uid;
    private int roomType;
    private boolean isAttention;
    private RoomInfo roomInfo;
    private UserInfo userInfo;

    private static final String KEY = "KEY";

    public static YiYaUserInfoDialog newInstance(long uid) {
        Bundle bundle = new Bundle();
        bundle.putLong(KEY, uid);
        YiYaUserInfoDialog dialog = new YiYaUserInfoDialog();
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_yiya_user_info;
    }


    @Override
    protected int getWindowGravity() {
        return Gravity.BOTTOM;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            uid = getArguments().getLong(KEY);
        }

        CoreManager.addClient(this);
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(uid, true);
        myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

        this.roomInfo = AvRoomDataManager.get().getRoomInfo();
        this.roomType = roomInfo.getType();

        if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) { // 语音房
            if (uid == myUid) { // 自己
                groupBottom.setVisibility(View.GONE);
            } else {
                ivMore.setVisibility(View.VISIBLE);
            }
        } else if (roomType == RoomInfo.ROOMTYPE_VIDEO_LIVE) {// 视频房
            if (uid == myUid) { // 自己
                groupBottom.setVisibility(View.GONE);
            } else {
                ivMore.setVisibility(View.VISIBLE);
                tv_send_gift.setVisibility(View.GONE);
            }
        }

        if (uid != myUid) {
            CoreManager.getCore(IPraiseCore.class).isPraised(myUid, uid);
        }
    }

    private void updateView() {
        if (userInfo != null) {
            //头像
            avatar.setAvatar(userInfo.getAvatar());
            //头饰
            avatar.setHeadWear(userInfo.getHeadwearUrl());
            nick.setText(userInfo.getNick());
            erbanId.setText(String.format(Locale.getDefault(),
                    getContext().getString(R.string.me_user_id), userInfo.getErbanNo()));
            if (userInfo.getIsPrettyErbanNo()) {
                erbanId.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_user_pretty_num, 0, 0, 0);
            }
            flexUserView.setData(userInfo);
        }
    }

    //------------------------------IPraiseClient--------------------------------
    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(Boolean islike, long uid) {
        updateFollowUI(islike);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        updateFollowUI(true);
        SingleToastUtil.showShortToast("关注成功，相互关注可成为好友哦！");
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long uid, boolean showNotice) {
        updateFollowUI(false);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == uid) {
            this.userInfo = info;
            updateView();
        }
    }

    private void updateFollowUI(Boolean islike) {
        isAttention = islike;
        tv_follow.setText(islike ? "已关注" : "+ 关注");
        tv_follow.setSelected(islike);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        CoreManager.removeClient(this);
    }

    @OnClick({R.id.iv_more, R.id.nick, R.id.tv_erban_id, R.id.tv_home, R.id.tv_send_gift, R.id.tv_private_msg, R.id.tv_follow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_follow:
                if (userInfo != null) {
                    if (isAttention) {
                        CoreManager.getCore(IPraiseCore.class).cancelPraise(userInfo.getUid(), true);
                    } else {
                        CoreManager.getCore(IPraiseCore.class).praise(userInfo.getUid());
                    }
                }
                break;
            case R.id.tv_send_gift:
                if (listener != null) {
                    listener.onSendGiftClicked(uid);
                }
                dismiss();
                break;
            case R.id.iv_more:
                if (listener != null) {
                    listener.onMenuClicked(uid);
                }
                dismiss();
                break;
            case R.id.tv_home:
                if (userInfo != null) {
                    UIHelper.showUserInfoAct(getContext(), userInfo.getUid());
                    dismiss();
                }
                break;
            case R.id.tv_private_msg:// 私聊
                if (userInfo != null) {
                    boolean isFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(userInfo.getUid() + "");
                    HttpUtil.checkUserIsDisturb(getContext(), isFriend, userInfo.getUid());
                    dismiss();
                }
                break;
        }
    }

    private OnHandleEventListener listener;

    public YiYaUserInfoDialog setOnHandleEventListener(OnHandleEventListener listener) {
        this.listener = listener;
        return this;
    }

    public interface OnHandleEventListener {

        /**
         * 更多菜单
         */
        default void onMenuClicked(long uid) {

        }

        /**
         * 送礼物
         */
        default void onSendGiftClicked(long uid) {

        }

    }

    ;


}
