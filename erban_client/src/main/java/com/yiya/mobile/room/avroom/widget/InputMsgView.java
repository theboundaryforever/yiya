package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.room.model.RoomModel;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.listener.SingleClickListener;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.room.bean.FreeBarragesBean;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

/**
 * 房间消息输入view
 *
 * @author zeda
 */
public class InputMsgView extends RelativeLayout {

    private RoomModel roomModel = new RoomModel();

    private EditText inputEt;
    private View sendBtn;
    private ImageView mBarrageIv;
    private TextView tvFreeBarrages;
    private RelativeLayout rlBarrage;

    private boolean enableBarrage;

    public InputMsgView(Context context) {
        this(context, null);
    }

    public InputMsgView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InputMsgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_input_msg, this);
        inputEt = findViewById(R.id.input_edit);
        GrowingIO.getInstance().trackEditText(inputEt);
        sendBtn = findViewById(R.id.input_send);
        mBarrageIv = findViewById(R.id.input_barrage_iv);
        tvFreeBarrages = findViewById(R.id.tv_free_barrages);
        rlBarrage = findViewById(R.id.rl_barrage);

        setOnTouchListener((v, event) -> {
            inputEt.clearFocus();
            setVisibility(View.GONE);
            hideKeyBoard();
            return false;
        });
        sendBtn.setOnClickListener(new SingleClickListener() {
            @Override
            public void singleClick(View v) {
                if (onInputClickListener != null) {
                    onInputClickListener.onClick(enableBarrage, getInputText());
                }
            }
        });
        inputEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    inputEt.setText(inputEt.getText().toString() + " ");
                    inputEt.setSelection(inputEt.getText().toString().length());
                    return true;
                }
                return false;
            }
        });

        mBarrageIv.setOnClickListener(v -> {
            enableBarrage = !enableBarrage;
            if (enableBarrage) {
                mBarrageIv.setImageResource(R.mipmap.icon_barrage_open);
                inputEt.setHint("直播间发送弹幕（1金币）");
                refreshData();
//                inputEt.setSingleLine(true);
//                inputEt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
            } else {
                rlBarrage.setVisibility(GONE);
                mBarrageIv.setImageResource(R.mipmap.icon_barrage_close);
                inputEt.setHint("输入你想说的话");
//                inputEt.setSingleLine(false);
//                inputEt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(500)});
            }
            if (onInputClickListener != null) {
                onInputClickListener.onBarrageClick(enableBarrage);
            }
        });
    }

    /**
     * 打开软键盘并显示头布局
     */
    public void showKeyBoard() {
        postDelayed(() -> {
            InputMethodManager imm = null;
            if (getContext() != null) {
                imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            }
            if (imm == null) return;
            imm.showSoftInput(inputEt, InputMethodManager.SHOW_FORCED);
        }, 80);
    }

    /**
     * 隐藏软键盘并隐藏头布局
     */
    public void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm == null) return;
        imm.hideSoftInputFromWindow(inputEt.getWindowToken(), 0);
    }

    public String getInputText() {
        return inputEt.getText().toString();
    }

    public void setInputText(String inputText) {
        if (inputText == null) {
            inputText = "";
        }
        inputEt.setText(inputText);
        inputEt.setSelection(inputText.length());
        inputEt.setFocusable(true);
        inputEt.setFocusableInTouchMode(true);
        inputEt.requestFocus();
    }

    public void setInputText() {
        inputEt.setFocusable(true);
        inputEt.setFocusableInTouchMode(true);
        inputEt.requestFocus();
    }

    public void setFreeBarrages(int barrageNum) {
        String tmp;
        if (barrageNum <= 0) {
            rlBarrage.setVisibility(GONE);
            return;
        } else {
            rlBarrage.setVisibility(VISIBLE);
        }

        if (barrageNum > 99) {
            tmp = "99+";
        } else {
            tmp = String.valueOf(barrageNum);
        }
        tvFreeBarrages.setText(tmp);
    }

    private void refreshData() {
        roomModel.getFreeBarrages(new HttpRequestCallBack<FreeBarragesBean>() {
            @Override
            public void onSuccess(String message, FreeBarragesBean bean) {
                int barrageNum = bean.getBarrageNum();
                setFreeBarrages(barrageNum);
            }

            @Override
            public void onFailure(int code, String msg) {
                SingleToastUtil.showToast(msg);
            }
        });
    }

    private OnInputClickListener onInputClickListener;

    public void setOnInputClickListener(OnInputClickListener listener) {
        this.onInputClickListener = listener;
    }

    public interface OnInputClickListener {
        void onClick(boolean enable, String inputContent);

        void onBarrageClick(boolean value);
    }

    public void hideInput(){
        inputEt.clearFocus();
        setVisibility(View.GONE);
        hideKeyBoard();
    }
}
