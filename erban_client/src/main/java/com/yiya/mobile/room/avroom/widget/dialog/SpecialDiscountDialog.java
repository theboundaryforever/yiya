package com.yiya.mobile.room.avroom.widget.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.room.avroom.fragment.AudioRoomDetailFragment;
import com.yiya.mobile.room.specialpay.SDPayDialog;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.DiscountGiftInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zdw
 * @date 2019/9/2
 * @describe 特惠礼物
 */


public class SpecialDiscountDialog extends DialogFragment {

    private DiscountGiftInfo mDiscountGiftInfo;
    private SpecialDiscountDialogListener listener;

    @BindView(R.id.cl_contain)
    ConstraintLayout clContain;
    @BindView(R.id.tv_des)
    TextView tvDes;
    @BindView(R.id.iv_gift)
    ImageView ivGift;
    @BindView(R.id.tv_gift_name)
    TextView tvGiftName;
    @BindView(R.id.iv_buy)
    ImageView ivBuy;
    @BindView(R.id.iv_close)
    ImageView ivClose;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.transFullScreenDialog); //dialog全屏
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.dialog_special_discount, container);
        ButterKnife.bind(this, mRootView);

        initView();
        setOnListener();
        return mRootView;
    }

    private void initView() {
        getData();
    }

    private void refreshView(DiscountGiftInfo discountGiftInfo) {
        String content = "";
        SpannableStringBuilder builder = new SpannableStringBuilder(content);
        String tips0 = "特惠体验只需";
        builder.append(tips0);
        setStrColor(builder, builder.toString().length() - tips0.length(), builder.toString().length(), R.color.white);

        String price = discountGiftInfo.getDiscountPriceRMB();
        builder.append(price);
        setStrSize(builder, builder.toString().length() - price.length(), builder.toString().length());
        setStrColor(builder, builder.toString().length() - price.length(), builder.toString().length(), R.color.color_FF4B2D);

        String unit = "元";
        builder.append(unit);
        setStrColor(builder, builder.toString().length() - unit.length(), builder.toString().length(), R.color.white);
        tvDes.setText(builder);

        if (discountGiftInfo.getPicUrl() != null) {
            ImageLoadUtils.loadImage(getContext(), discountGiftInfo.getPicUrl(), ivGift);
        }

        tvGiftName.setText(discountGiftInfo.getGiftName());
    }

    private void getData() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getGiftPreferential(), params, new HttpRequestCallBack<DiscountGiftInfo>() {
            @Override
            public void onSuccess(String message, DiscountGiftInfo response) {
                if (response == null) return;
                mDiscountGiftInfo = response;
                if (listener != null && (response.getUserBuyCount() > response.getMaxBuyCount())) {
                    listener.hideEnter();
                }
                refreshView(response);
            }

            @Override
            public void onFailure(int code, String msg) {
                SingleToastUtil.showToast(msg);
            }
        });
    }

    /**
     * 文字颜色
     */
    private void setStrColor(SpannableStringBuilder builder, int start, int end, int color) {
        ForegroundColorSpan redSpan = new ForegroundColorSpan(getResources().getColor(color));
        builder.setSpan(redSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * 文字颜色
     */
    private void setStrSize(SpannableStringBuilder builder, int start, int end) {
        RelativeSizeSpan sizeSpan = new RelativeSizeSpan(1.5f);
        builder.setSpan(sizeSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    private void setOnListener() {
        ivBuy.setOnClickListener(v -> {
            if (mDiscountGiftInfo.getUserBuyCount() >= mDiscountGiftInfo.getMaxBuyCount()){
                SingleToastUtil.showToast("今日特惠已到达次数上限");
            } else {
                SDPayDialog sdPayDialog = new SDPayDialog();
                sdPayDialog.show(getFragmentManager(), AudioRoomDetailFragment.class.getSimpleName());
                sdPayDialog.setData(mDiscountGiftInfo);
            }
            dismiss();
        });

        ivClose.setOnClickListener(v -> {
//            AvRoomDataManager.get().setDialogShow(false);
            dismiss();
        });
    }

    public void setListener(SpecialDiscountDialogListener listener) {
        this.listener = listener;
    }

    public interface SpecialDiscountDialogListener {
        // 隐藏入口
        void hideEnter();
    }
}
