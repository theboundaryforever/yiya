package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.R;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * 直播预览的分享view
 *
 * @author zeda
 */
public class LivePrepareShareView extends RelativeLayout {
    FrameLayout flDesc;
    TextView tvDesc;

    private int[] shareBtnResList = {R.id.iv_qq, R.id.iv_wx, R.id.iv_wx_friend, R.id.iv_qzone};
    private String[] shareTipDesc = {"开播将分享至QQ", "开播将分享至微信", "开播将分享至微信朋友圈", "开播将分享至QQ空间"};
    private String[] sharePlatformNames = {QQ.NAME, Wechat.NAME, WechatMoments.NAME, QZone.NAME};

    private int selectShareBtnIndex = -1;
    private Platform selectSharePlatform = null;

    private ImageView[] shareBtnList;
    private int[] shareTipDesPointXList;

    public LivePrepareShareView(Context context) {
        super(context);
    }

    public LivePrepareShareView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LivePrepareShareView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.layout_live_prepare_share, this);
        flDesc = findViewById(R.id.fl_desc);
        tvDesc = findViewById(R.id.tv_desc);
        shareBtnList = new ImageView[shareBtnResList.length];
        for (int i = 0; i < shareBtnResList.length; i++) {
            shareBtnList[i] = findViewById(shareBtnResList[i]);
            shareBtnList[i].setTag(i);
            shareBtnList[i].setOnClickListener(v -> {
                int index = (int) v.getTag();
                if (selectShareBtnIndex != -1 && selectShareBtnIndex == index) {
                    //点中当前已选中的，做取消选中处理
                    v.setSelected(false);
                    selectSharePlatform = null;
                    selectShareBtnIndex = -1;
                    flDesc.setVisibility(View.INVISIBLE);
                } else {
                    //点中非当前已选中的/当前没有已选中的，做选中处理
                    v.setSelected(true);
                    selectSharePlatform = ShareSDK.getPlatform(sharePlatformNames[index]);
                    if (selectShareBtnIndex != -1) {
                        shareBtnList[selectShareBtnIndex].setSelected(false);
                    }
                    selectShareBtnIndex = index;
                    tvDesc.setText(shareTipDesc[index]);
                    showShareTipDescViewToPosition();
                }
            });
        }
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                postDelayed(() -> {
                    calculateShareTipDesPointX();
                    if (selectShareBtnIndex != -1) {
                        //如果当前已选中了分享按钮，那么执行显示
                        showShareTipDescViewToPosition();
                    }
                }, 200);
                getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    /**
     * 计算分享提示view相对每个分享子view的X位置
     */
    private void calculateShareTipDesPointX() {
        shareTipDesPointXList = new int[shareBtnResList.length];
        int desWidth = getResources().getDimensionPixelSize(R.dimen.live_prepare_share_view_des_width);
        for (int i = 0; i < shareBtnResList.length; i++) {
            View shareBtn = shareBtnList[i];
            int[] location = new int[2];
            shareBtn.getLocationInWindow(location);
            int shareTipDesPointX = location[0] + Math.round(shareBtn.getWidth() / 2f) - Math.round(desWidth / 2f);
            shareTipDesPointXList[i] = shareTipDesPointX;
        }
    }

    /**
     * 显示分享提示view到对应的位置
     */
    private void showShareTipDescViewToPosition() {
        //如果界面还未渲染shareTipDesPointXList为null，则等渲染后会主动再执行此方法一次
        if (shareTipDesPointXList != null) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) flDesc.getLayoutParams();
            layoutParams.leftMargin = shareTipDesPointXList[selectShareBtnIndex];
            flDesc.setLayoutParams(layoutParams);
            flDesc.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 获取选中的分享渠道
     * <p>
     * null代表没选中分享渠道
     */
    public @Nullable
    Platform getSelectSharePlatform() {
        return selectSharePlatform;
    }
}
