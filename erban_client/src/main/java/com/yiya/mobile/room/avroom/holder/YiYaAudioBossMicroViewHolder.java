package com.yiya.mobile.room.avroom.holder;

import android.content.Context;
import android.view.View;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

public class YiYaAudioBossMicroViewHolder extends BaseAudioMicroViewHolder {

    private final View groupState;
    /**
     * 主席位特有
     */
    private String avatarPicture = "";
    private String userName = "";

    public YiYaAudioBossMicroViewHolder(Context context, View itemView) {
        super(context, itemView);
        groupState = itemView.findViewById(R.id.group_state);
    }

    @Override
    public void bind(RoomQueueInfo info, int position) {
        super.bind(info, position);
        if (info == null || AvRoomDataManager.get().getRoomInfo() == null) {
            return;
        }
        try {
            RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(AvRoomDataManager.MIC_POSITION_BY_OWNER);
            if (roomQueueInfo == null || roomQueueInfo.mChatRoomMember != null) {
                groupState.setVisibility(View.GONE);
            } else {
                groupState.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            groupState.setVisibility(View.VISIBLE);
        }

//        ivGender.setVisibility(View.GONE);
//        tvNick.setVisibility(View.VISIBLE);
//        ivMuteImage.setVisibility(View.GONE);
//        ivUpImage.setVisibility(View.GONE);
        ivHeadWear.setVisibility(View.VISIBLE);
//        avatarBg.setVisibility(View.VISIBLE);
        ivHeadWear.setOnClickListener(v -> onMicroItemClickListener.onAvatarBtnClick(position, null));
        if (StringUtil.isEmpty(avatarPicture)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(AvRoomDataManager.get().getRoomInfo().getUid());
            if (userInfo != null) {
                if (userInfo.getAvatar() != null) {
                    if (!userInfo.getAvatar().equals(avatarPicture)) {
                        avatarPicture = userInfo.getAvatar();
//                        userName = userInfo.getNick();
//                        tvNick.setText(userInfo.getNick());
                        ivHeadWear.setAvatar(avatarPicture);
//                        ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), avatarPicture, ivAvatar);
                    }
                }
            } else {
                CoreManager.getCore(IUserCore.class).requestUserInfo(AvRoomDataManager.get().getRoomInfo().getUid(), new HttpRequestCallBack<UserInfo>() {
                    @Override
                    public void onSuccess(String message, UserInfo response) {
                        if (response != null && ivHeadWear != null) {
                            avatarPicture = response.getAvatar();
//                            tvNick.setText(response.getNick());
//                            userName = response.getNick();
                            ivHeadWear.setAvatar(avatarPicture);
//                            ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), avatarPicture, ivAvatar);
                        }
                    }

                    @Override
                    public void onFailure(int code, String msg) {

                    }
                });
            }
        } else {
            ivHeadWear.setAvatar(avatarPicture);
//            ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), avatarPicture, ivAvatar);
//            tvNick.setText(userName);
        }
    }
}