package com.yiya.mobile.room.gift.widget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

public class GiftAllMicView extends RelativeLayout {
    private TextView tvAll;
    private CheckBox cbSelect;
    private boolean isSelect = false;
    private OnAllMicSelectListener onAllMicSelectListener;

    public GiftAllMicView(Context context) {
        super(context);
        inflate(context,R.layout.layout_gift_all_mic, this);
        ViewGroup.LayoutParams vl = getLayoutParams();
        vl.width = DisplayUtils.dip2px(context,48);
        vl.height = DisplayUtils.dip2px(context,51);
        setLayoutParams(vl);
        tvAll = findViewById(R.id.tv_gift_all);
        cbSelect = findViewById(R.id.cb_gift_all);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbSelect.isChecked()) {
                    setCheck(false);
                } else {
                    setCheck(true);
                }
                if (onAllMicSelectListener != null)
                    onAllMicSelectListener.onCheck(isSelect);
            }
        });

    }

    public void setCheck(boolean state) {
        if (cbSelect != null)
            cbSelect.setChecked(state);
        if (tvAll != null)
            tvAll.setSelected(state);
        isSelect = state;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public interface OnAllMicSelectListener {
        void onCheck(boolean isAllMic);
    }

    public void setOnAllMicSelectListener(OnAllMicSelectListener onAllMicSelectListener) {
        this.onAllMicSelectListener = onAllMicSelectListener;
    }
}
