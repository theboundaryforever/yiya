package com.yiya.mobile.room.avroom.activity;

/**
 * Created by zhouxiangfeng on 2017/6/1.
 */

//public class OpenRoomActivity extends TakePhotoActivity implements View.OnClickListener {
//
//    private static final String TAG = "OpenRoomActivity";
//    /**
//     * 基本权限管理
//     */
//    private final String[] BASIC_PERMISSIONS = new String[]{
//            Manifest.permission.CAMERA,
//            Manifest.permission.RECORD_AUDIO
//    };
//
//    // Content View Elements
//    private ImageView mIv_close;
//    private ImageView mIv_cover;
//    private EditText mEt_title;
//    private EditText mEt_noti;
//    private TextView pageTitle;
//    private Button mBtn_open_room;
//    private String roomCoverUrl;
//    private int type;
//
//    //开启竞拍房
//    public static void startAuction(Context context) {
//        Intent intent = new Intent(context, OpenRoomActivity.class);
//        intent.putExtra("type", RoomInfo.ROOMTYPE_AUCTION);
//        context.startActivity(intent);
//
//    }
//
//    //开启轻聊房
//    public static void startHomeParty(Context context) {
//        Intent intent = new Intent(context, OpenRoomActivity.class);
//        intent.putExtra("type", RoomInfo.ROOMTYPE_LIGHT_CHAT);
//        context.startActivity(intent);
//    }
//
//    //开启游戏房
//    public static void startGame(Context context) {
//        Intent intent = new Intent(context, OpenRoomActivity.class);
//        intent.putExtra("type", RoomInfo.ROOMTYPE_HOME_PARTY);
//        context.startActivity(intent);
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_open_room);
//        bindViews();
//        onSetListener();
//        init();
//    }
//
//    private void init() {
//        type = getIntent().getIntExtra("type", 0);
//        if (type == RoomInfo.ROOMTYPE_AUCTION) {
//            pageTitle.setText("竞拍房");
//        } else if (type == RoomInfo.ROOMTYPE_LIGHT_CHAT) {
//            pageTitle.setText("轻聊房");
//        } else if (type == RoomInfo.ROOMTYPE_HOME_PARTY) {
//            pageTitle.setText("轰趴房");
//        }
//    }
//
//    private void onSetListener() {
//        mBtn_open_room.setOnClickListener(this);
//        mIv_close.setOnClickListener(this);
//        mIv_cover.setOnClickListener(this);
//    }
//
//    // End Of Content View Elements
//    private void bindViews() {
//        mIv_close = (ImageView) findViewById(R.id.iv_close);
//        mIv_cover = (ImageView) findViewById(R.id.iv_cover);
//        mEt_title = (EditText) findViewById(R.id.et_title);
//        mEt_noti = (EditText) findViewById(R.id.et_noti);
//        mBtn_open_room = (Button) findViewById(R.id.btn_open_room);
//        pageTitle = (TextView) findViewById(R.id.title);
//    }
//
//    private void checkPermissionAndStartCamera() {
//        //低版本授权检查
//        checkPermission(checkPermissionListener, R.string.ask_camera, android.Manifest.permission.CAMERA);
////        checkPermission(checkPermissionListener, R.string.ask_again, android.Manifest.permission.CAMERA);
//    }
//
//    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
//        @Override
//        public void superPermission() {
//            takePhoto();
//        }
//    };
//
//    private void takePhoto() {
//        File cameraOutFile = JXFileUtils.getTempFile(this, "picture_" + System.currentTimeMillis() + ".jpg");
//        if (!cameraOutFile.getParentFile().exists()) {
//            cameraOutFile.getParentFile().mkdirs();
//        }
//        Uri uri = Uri.fromFile(cameraOutFile);
//        CompressConfig compressConfig = new CompressConfig.Builder().create();
//        getTakePhoto().onEnableCompress(compressConfig, false);
//        getTakePhoto().onPickFromCapture(uri);
//    }
//
//    /**
//     * 开房权限检查
//     */
//    PermissionActivity.CheckPermListener loginCheckPermissionListener = new PermissionActivity.CheckPermListener() {
//        @Override
//        public void superPermission() {
//            getDialogManager().showProgressDialog(OpenRoomActivity.this, "进入房间...");
//            CoreManager.getCore(IRoomCore.class).openRoom(CoreManager.getCore(IAuthCore.class).getCurrentUid(), type,
//                    mEt_title.getText().toString(), mEt_noti.getText().toString(), roomCoverUrl, null);
//        }
//    };
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.btn_open_room:
//                checkPermission(loginCheckPermissionListener, R.string.ask_again, Manifest.permission.RECORD_AUDIO);
//                break;
//            case R.id.iv_close:
//                finish();
//                break;
//            case R.id.iv_cover:
//                ButtonItem upItem = new ButtonItem("拍照上传", new ButtonItem.OnClickListener() {
//                    @Override
//                    public void onClick() {
//                        checkPermissionAndStartCamera();
//                    }
//                });
//                ButtonItem localItem = new ButtonItem("本地相册", new ButtonItem.OnClickListener() {
//                    @Override
//                    public void onClick() {
//                        CompressConfig compressConfig = new CompressConfig.Builder().create();
//                        getTakePhoto().onEnableCompress(compressConfig, true);
//                        getTakePhoto().onPickFromGallery();
//                    }
//                });
//                List<ButtonItem> buttonItemList = new ArrayList<>();
//                buttonItemList.add(upItem);
//                buttonItemList.add(localItem);
//                getDialogManager().showCommonPopupDialog(buttonItemList, "取消", false);
//                break;
//            default:
//        }
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onGetRoomInfo(RoomInfo roomInfo) {
//        getDialogManager().dismissDialog();
//        mEt_title.setText(roomInfo.getTitle());
//        mEt_noti.setText(roomInfo.getRoomDesc());
//
//        if (!StringUtil.isEmpty(roomInfo.getBackPic())) {
//            roomCoverUrl = roomInfo.getBackPic();
//            ImageLoadUtils.loadAvatar(this, roomCoverUrl, mIv_cover);
//        }
//
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onGetRoomInfoFail(int errorno,String error,int pageType) {
//        getDialogManager().dismissDialog();
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onOpenRoom(RoomInfo roomInfo) {
//        getDialogManager().dismissDialog();
//        RoomFrameActivity.start(this, roomInfo.getUid());
//        finish();
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onOpenRoomFail(String error) {
//        toast(error);
//        getDialogManager().dismissDialog();
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onAlreadyOpenedRoom() {
//        getDialogManager().showOkCancelDialog("您的房间已开启，是否立即进入？", true, new DialogManager.OkCancelDialogListener() {
//            @Override
//            public void onCancel() {
//
//            }
//
//            @Override
//            public void onOk() {
////                boolean isImLogin = CoreManager.getCore(IIMLoginCore.class).isImLogin();
////                if (isImLogin) {
//                long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
//                RoomFrameActivity.start(OpenRoomActivity.this, uid);
//                finish();
////                } else {
////                    toast("进入房间失败，请检查网络");
////                }
//            }
//        });
//    }
//
//    @CoreEvent(coreClientClass = IFileCoreClient.class)
//    public void onUpload(String url) {
//        roomCoverUrl = url;
//        Log.d(TAG, "onUpload: 这是开房间的上传");
////        Glide.with(OpenRoomActivity.this).load(roomCoverUrl).into(mIv_cover);
//        ImageLoadUtils.loadImage(this, roomCoverUrl, mIv_cover, 0);
//        getDialogManager().dismissDialog();
//    }
//
//    @CoreEvent(coreClientClass = IFileCoreClient.class)
//    public void onUploadFail() {
//        toast("上传失败");
//        getDialogManager().dismissDialog();
//    }
//
//    @Override
//    public void takeSuccess(TResult result) {
//        getDialogManager().showProgressDialog(OpenRoomActivity.this, "正在上传请稍后...");
//        CoreManager.getCore(IFileCore.class).upload(new File(result.getImage().getCompressPath()));
//    }
//
//    @Override
//    public void takeFail(TResult result, String msg) {
//        toast(msg);
//    }
//
//    @Override
//    public void takeCancel() {
//    }
//
//}
