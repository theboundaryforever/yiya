package com.yiya.mobile.room.avroom.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.faceunity.FURenderer;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.room.avroom.activity.RoomChatActivity;
import com.yiya.mobile.room.avroom.adapter.VideoRoomOnlineAdapter;
import com.yiya.mobile.room.avroom.widget.BaseBottomView;
import com.yiya.mobile.room.avroom.widget.BaseMicroView;
import com.yiya.mobile.room.avroom.widget.ComingMsgView;
import com.yiya.mobile.room.avroom.widget.GiftMsgView;
import com.yiya.mobile.room.avroom.widget.GiftView;
import com.yiya.mobile.room.avroom.widget.HeadLineStarNoticeView;
import com.yiya.mobile.room.avroom.widget.InputMsgView;
import com.yiya.mobile.room.avroom.widget.LuckyGiftView;
import com.yiya.mobile.room.avroom.widget.MessageView;
import com.yiya.mobile.room.avroom.widget.MicContainerView;
import com.yiya.mobile.room.avroom.widget.MicroUserInfoView;
import com.yiya.mobile.room.avroom.widget.ScreenNoticeView;
import com.yiya.mobile.room.avroom.widget.VideoMasterBottomView;
import com.yiya.mobile.room.avroom.widget.VideoMicroView;
import com.yiya.mobile.room.avroom.widget.dialog.LiveRoomFaceuDialog;
import com.yiya.mobile.room.avroom.widget.dialog.RoomFunctionDialog;
import com.yiya.mobile.room.gift.GiftSelector;
import com.yiya.mobile.room.presenter.VideoRoomDetailPresenter;
import com.yiya.mobile.room.widget.dialog.ListDataDialog;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.me.setting.activity.FeedbackActivity;
import com.yiya.mobile.ui.rank.activity.RankingListActivity;
import com.yiya.mobile.ui.widget.Banner;
import com.yiya.mobile.ui.widget.dialog.MicroApplyListDialog;
import com.juxiao.library_utils.DisplayUtils;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.common.ui.dialog.EasyAlertDialogHelper;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomFunctionEnum;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.SpUtils;
import com.yiya.mobile.room.avroom.widget.dialog.RoomFunctionModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;

/**
 * 主播视频房fragment
 */
@CreatePresenter(VideoRoomDetailPresenter.class)
public class VideoRoomMasterDetailFragment extends VideoRoomDetailFragment {
    @BindView(R.id.rl_Top)
    RelativeLayout rlTop;
    @BindView(R.id.micro_container)
    VideoMicroView microView;
    @BindView(R.id.message_view)
    MessageView messageView;
    @BindView(R.id.gift_view)
    GiftView giftView;
    @BindView(R.id.bottom_view)
    VideoMasterBottomView bottomView;
    @BindView(R.id.iv_room_bg)
    ImageView roomBgIv;
    @BindView(R.id.iv_close)
    ImageView iv_close;
    @BindView(R.id.tv_follow)
    TextView tv_follow;
    @BindView(R.id.avatar)
    ImageView avatar;
    @BindView(R.id.rv_online_count)
    RecyclerView rvOnlineList;
    @BindView(R.id.input_layout)
    InputMsgView inputMsgView;
    @BindView(R.id.rl_room_operate)
    RelativeLayout moreOperateBtn;
    @BindView(R.id.tv_room_title)
    TextView tv_title;
    @BindView(R.id.tv_room_num)
    TextView tv_room_num;
    @BindView(R.id.ll_contribute)
    LinearLayout ll_contribute;
    @BindView(R.id.tv_contribute_num)
    TextView mReceiveSumTv;
    @BindView(R.id.miv_info)
    MicroUserInfoView microUserInfoView;
    @BindView(R.id.cmv_msg)
    ComingMsgView cmv_msg;
    @BindView(R.id.gift_selector)
    GiftSelector giftSelector;
    @BindView(R.id.icon_room_lottery_box)
    ImageView icon_room_lottery_box;
    @BindView(R.id.ic_room_banner)
    Banner icon_room_new_gift;
    @BindView(R.id.svga_downcount)
    SVGAImageView svgaDownCount;
    @BindView(R.id.chronometer)
    Chronometer chronometer;
    @BindView(R.id.svga_lucky_box_activate)
    SVGAImageView svgaLuckyBoxActivate;
    @BindView(R.id.svga_lucky_tips)
    SVGAImageView svgaLuckyTips;
    @BindView(R.id.mic_container_view)
    MicContainerView micContainerView;
    @BindView(R.id.svga_lucky_gift_notice)
    ScreenNoticeView svgaLuckyGiftNotice;
    @BindView(R.id.svga_add_coin_notice)
    ScreenNoticeView svgaAddCoinNotice;
    @BindView(R.id.svga_head_line_start_limit_notice)
    HeadLineStarNoticeView svgaHeadLineStartLimitNotice;
    @BindView(R.id.ll_distance)
    LinearLayout llDistance;
    @BindView(R.id.tv_distance)
    TextView tvDistance;
    @BindView(R.id.svga_add_gold)
    SVGAImageView svgaAddGold;
    @BindView(R.id.iv_sd)
    ImageView ivSpecialDiscount;
    @BindView(R.id.banner_event)
    Banner bannerEvent;

    VideoRoomOnlineAdapter onlineAdapter;

    private boolean isDoInitResume = false;//onCreate 和 onResume 都会走一次初始化，开始的时候，用一个就行了

    @NonNull
    @Override
    protected BaseMicroView getMicroView() {
        return microView;
    }

    @NonNull
    @Override
    protected MessageView getMessageView() {
        return messageView;
    }

    @NonNull
    @Override
    protected GiftView getGiftView() {
        return giftView;
    }

    @NonNull
    @Override
    protected BaseBottomView getBottomView() {
        return bottomView;
    }

    @NonNull
    @Override
    protected TextView getTitleView() {
        return tv_title;
    }

    @Nullable
    @Override
    protected GiftMsgView getGiftMsgView() {
        return null;
    }

    @NonNull
    @Override
    protected ComingMsgView getComingMsgView() {
        return cmv_msg;
    }

    @NonNull
    @Override
    protected ImageView getRoomBgView() {
        return roomBgIv;
    }

    @NonNull
    @Override
    protected GiftSelector getGiftSelector() {
        return giftSelector;
    }

    @NonNull
    @Override
    protected InputMsgView getInputMsgView() {
        return inputMsgView;
    }

    @NonNull
    @Override
    protected View getMoreOperateBtn() {
        return moreOperateBtn;
    }

    @NonNull
    @Override
    protected ImageView getLotteryBoxView() {
        return icon_room_lottery_box;
    }

    @NonNull
    @Override
    protected Banner getBannerView() {
        return icon_room_new_gift;
    }

    @Nullable
    @Override
    protected LuckyGiftView getLuckyGiftView() {
        return null;
    }

    @NonNull
    @Override
    protected SVGAImageView getLuckyTipsSVGAIv() {
        return svgaLuckyTips;
    }

    @NonNull
    @Override
    protected SVGAImageView getLuckyBoxActivateView() {
        return svgaLuckyBoxActivate;
    }

    @NonNull
    @Override
    protected ScreenNoticeView getLuckyGiftNoticeTips() {
        return svgaLuckyGiftNotice;
    }

    @NonNull
    @Override
    protected ScreenNoticeView getAddCoinNoticeTips() {
        return svgaAddCoinNotice;
    }

    @NotNull
    @Override
    protected TextView getOnlineNumView() {
        return tv_room_num;
    }

    @Override
    protected TextView getFollowView() {
        return null;
    }

    @Override
    protected SVGAImageView getAddGoldSvga() {
        return svgaAddGold;
    }

    @Override
    protected View getReceiveSumView() {
        return mReceiveSumTv;
    }

    @NonNull
    @Override
    protected ImageView getAvatarView() {
        return avatar;
    }

    @NonNull
    @Override
    protected ImageView getRoomCloseView() {
        return iv_close;
    }

    @NonNull
    @Override
    protected LinearLayout getDistanceView() {
        return llDistance;
    }

    @NonNull
    @Override
    protected HeadLineStarNoticeView getHeadLineStarLimitNoticeTips() {
        return svgaHeadLineStartLimitNotice;
    }

    @NonNull
    @Override
    protected TextView getDistanceTextView() {
        return tvDistance;
    }

    @NonNull
    @Override
    protected ImageView getSpecialDiscountView() {
        return ivSpecialDiscount;
    }

    @NonNull
    @Override
    protected Banner getBannerEventView() {
        return bannerEvent;
    }

    /**
     * 宝箱激活文案
     *
     * @return
     */
    @Override
    protected int getLuckyBoxActivateTips() {
        return R.string.lucky_box_activate_broadcaster;
    }

    public static VideoRoomMasterDetailFragment newInstance(long roomUid) {
        VideoRoomMasterDetailFragment roomDetailFragment = new VideoRoomMasterDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.ROOM_UID, roomUid);
        roomDetailFragment.setArguments(bundle);
        return roomDetailFragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_video_master_room_detail;
    }

    @Override
    public void onSetListener() {
        super.onSetListener();

        ll_contribute.setOnClickListener(view -> RankingListActivity.start(getContext(), CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", getMvpPresenter().getCurrRoomInfo().getUid() + ""));

        mView.findViewById(R.id.tv_room_num).setOnClickListener(v -> {
            ListDataDialog listDataDialog = ListDataDialog.newOnlineUserListInstance(getActivity(), true);
            listDataDialog.setOnlineItemClick(onlineItemClick);
            showDialog(listDataDialog);
        });
    }

    @Override
    public void initiate() {
        super.initiate();
//        GrowingIO.getInstance().setPageName(this, "视频-开播页");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "主播直播页");

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, DisplayUtils.getStatusBarHeight(mContext), 0, 0);
        rlTop.setLayoutParams(params);

        if (AvRoomDataManager.get().isRoomOwner()) {
            tv_follow.setVisibility(View.GONE);
        } else {
            tv_follow.setVisibility(View.VISIBLE);
        }

        onlineAdapter = new VideoRoomOnlineAdapter();
        rvOnlineList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvOnlineList.setAdapter(onlineAdapter);

        onlineAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (onlineAdapter != null && ListUtils.isNotEmpty(onlineAdapter.getData())) {
                IMChatRoomMember member = onlineAdapter.getData().get(position);
                showUserInfoDialog(Integer.valueOf(member.getAccount()));
            }
        });

        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        microUserInfoView.setMicroUserInfo(roomInfo);
        refreshReceiveSum(roomInfo);

        refreshLianMicroViewStatus();

        startDownCount();
        startTimeCount();
    }

    @Override
    public void receiveRoomEvent(RoomEvent roomEvent) {
        if (roomEvent != null && getMvpPresenter() != null) {
            switch (roomEvent.getEvent()) {
                case RoomEvent.ROOM_RECONNECT://IM重连
                case RoomEvent.ENTER_ROOM://进房
                case RoomEvent.DOWN_MIC://下麦
                    getMvpPresenter().getMicApplyList();
                    break;
                case RoomEvent.CODE_GUEST_APPLY_MICRO://嘉宾申请连麦
                    showApplyTipsView(roomEvent.getRoomMicroApplyInfo());
                case RoomEvent.CODE_GUEST_CANCEL_MICRO://嘉宾取消连麦申请
                case RoomEvent.CODE_BROADCASTER_REFRESH_MICRO_APPLY://主播接受连麦
                    refreshLianMicroViewStatus(roomEvent.getRoomMicroApplyInfo());
                    break;
                case RoomEvent.CODE_BROADCASTER_WARN:
                    //警告主播
                    EasyAlertDialogHelper
                            .showOneButtonDiolag(getContext(),
                                    null,
                                    roomEvent.getReason_msg(),
                                    "确定",
                                    true,
                                    null);
                    break;
                case RoomEvent.KICK_OUT_ROOM:
                    //踢出房间
                case RoomEvent.CODE_BROADCASTER_BAN:
                    //封禁主播
                    EasyAlertDialogHelper
                            .showOneButtonDiolag(getContext(),
                                    null,
                                    roomEvent.getReason_msg(),
                                    "确定",
                                    false,
                                    v -> exitRoomWithIncome(Long.parseLong(roomEvent.getRoomId()), Long.parseLong(roomEvent.getAccount())));
                    break;
                case RoomEvent.CODE_FORCE_ON_LIVE:
                    String roomId = roomEvent.getRoomId();
                    String clientType = roomEvent.getClientType();
                    if (Long.valueOf(roomId) == AvRoomDataManager.get().getRoomInfo().getRoomId()) {
                        if (Long.valueOf(clientType) == 1) {// 1 web端 2手机端
                            // 为1说明是手机端踢出
                            LogUtil.i(TAG, "CODE_FORCE_ON_LIVE clientType : " + clientType);
                            getMvpPresenter().exitRoom(null);
                            finish();
                        }
                    }
                    break;

                case RoomEvent.CODE_DISCONNECT_TIME_OUT:
                    toast("连接失败，已退出");
                    //这里要先把roomInfo拿出来，exitRoom的时候会清空
                    RoomInfo currRoomInfo = getMvpPresenter().getCurrRoomInfo();
                    getMvpPresenter().exitRoom(null);
                    if (currRoomInfo != null) {
                        exitRoomWithIncome(currRoomInfo.getRoomId(), currRoomInfo.getUid());
                    } else {
                        finish();
                    }
                    break;
            }
        }
        super.receiveRoomEvent(roomEvent);
    }

    private void refreshReceiveSum(RoomInfo roomInfo) {
        if (roomInfo == null) return;
        if (roomInfo.getReceiveSum() == null) {
            roomInfo.setReceiveSum(0L);
        }
        mReceiveSumTv.setText(roomInfo.getReceiveSum() + "");
    }

    /**
     * 显示退出提醒dialog
     */
    protected void showExitTipDialog() {
        getDialogManager().showRoomExitDialog(
                AvRoomDataManager.get().isRoomOwner(), false, false, RoomInfo.ROOMTYPE_VIDEO_LIVE,
                new DialogManager.ExitRoomDialogListener() {
                    @Override
                    public void onExitAndFollow() {
                        getMvpPresenter().roomAttention(getMvpPresenter().getCurrRoomInfo().getRoomId(), false);
                        getMvpPresenter().exitRoom(null);
                        finish();
                    }

                    @Override
                    public void onMinimize() {
                        AvRoomDataManager.get().setMinimize(true);
                        finish();
                    }

                    @Override
                    public void onExit() {
                        //这里要先把roomInfo拿出来，exitRoom的时候会清空
                        RoomInfo currRoomInfo = getMvpPresenter().getCurrRoomInfo();
                        getMvpPresenter().exitRoom(null);
                        if (currRoomInfo != null) {
                            exitRoomWithIncome(currRoomInfo.getRoomId(), currRoomInfo.getUid());
                        } else {
                            finish();
                        }
                    }
                });
    }

    /**
     * 连麦按钮点击监听
     */
    @Override
    protected void onMicroBtnClick(View v) {
        //房主
        showMicApplyListDialog();
    }

    private void showMicApplyListDialog() {
        MicroApplyListDialog.newInstance().show(getChildFragmentManager(), null);
    }

    @Override
    public void refreshRoomAttentionStatus(Boolean status) {
        microUserInfoView.setAttention(status);
    }

    /**
     * 刷新在线人员列表view 注意这里请求的是在线头像人数列表
     */
    @Override
    public void refreshOnlineView(List<IMChatRoomMember> memberList) {
        if (ListUtils.isListEmpty(memberList) || memberList.size() <= 0) return;
        onlineAdapter.setNewData(memberList);
    }

    /**
     * 刷新关注标签
     */
    @Override
    protected void refreshAttention() {
        microUserInfoView.tv_follow.setSelected(true);
        getMvpPresenter().checkAttention(getMvpPresenter().getCurrRoomInfo().getRoomId());
    }

    /**
     * 开启直播计时
     */
    private void startTimeCount() {
        chronometer.setBase(SystemClock.elapsedRealtime());// MM:SS
        int hour = (int) ((SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000 / 60);
        chronometer.setFormat("0" + String.valueOf(hour) + ":%s");
        chronometer.start();
    }

    /**
     * 开启倒计时
     */
    private void startDownCount() {
        String source = "countdown.svga";
        SVGAParser parser = new SVGAParser(mContext);
        parser.decodeFromAssets(source, new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                svgaDownCount.setLoops(1);
                svgaDownCount.setImageDrawable(drawable);
                svgaDownCount.setClearsAfterStop(true);
                svgaDownCount.startAnimation();
            }

            @Override
            public void onError() {

            }
        });
    }

    /**
     * 显示房间更多功能弹框
     */
    @Override
    public void showRoomMoreOperateDialog() {
        RoomFunctionDialog functionDialog = new RoomFunctionDialog(mContext);
        functionDialog.setFunctionType(RoomFunctionModel.ROOM_LIVE_OWNER);
        functionDialog.setOnFunctionClickListener(bean -> {
            if (bean.getFunctionType() == RoomFunctionEnum.ROOM_FEEDBACK) {// 问题反馈
                startActivity(new Intent(mContext, FeedbackActivity.class));
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_QUIT) {// 退出房间
                getMvpPresenter().exitRoom(null);
                finish();
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_SPEAK) {// 发言
                getInputMsgView().setVisibility(View.VISIBLE);
                getInputMsgView().setInputText("");
                getInputMsgView().showKeyBoard();
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_REVERSAL) {// 镜头反转
                microView.switchCameraFacing();
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_SOUND) {// 静音
                getMvpPresenter().switchMicroMuteStatus();
            } else if (bean.getFunctionType() == RoomFunctionEnum.ROOM_MIRRORING) {// 镜像

            } else if (RoomFunctionEnum.ROOM_PLANT_BEAN == bean.getFunctionType()) {
                //种豆消息
                boolean isPlantBeanMsgEnabled = (boolean) SpUtils.get(BasicConfig.INSTANCE.getAppContext(), Constants.KEY_PLANT_BEAN_MSG, true);
                SpUtils.put(BasicConfig.INSTANCE.getAppContext(), Constants.KEY_PLANT_BEAN_MSG, !isPlantBeanMsgEnabled);
                bean.setImgRes(!isPlantBeanMsgEnabled ? R.mipmap.ic_plant_bean_msg_setting_enable : R.mipmap.ic_plant_bean_msg_setting_disable);
                functionDialog.getAdapter().notifyDataSetChanged();
            } else if (RoomFunctionEnum.ROOM_SHARE == bean.getFunctionType()) {
                UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomShare());
                showShareDialog();
            } else if (RoomFunctionEnum.ROOM_PRIVATE_CHAT == bean.getFunctionType()) {// 私聊
                RoomChatActivity.startDialogActivity(getContext());
            }
        });

        functionDialog.show();
        functionDialog.initData();
    }

    /**
     * 更新连麦按钮状态
     */
    @Override
    public void refreshLianMicroViewStatus(RoomMicroApplyInfo roomMicroApplyInfo) {
        if (roomMicroApplyInfo != null) {
            //是否用过（点击过）连麦功能
            boolean isLianMicroUsed = getMvpPresenter().isLianMicroUsed();
            boolean isInLianMicroOrApply = roomMicroApplyInfo.getTotal() > 0;
            bottomView.setupLianMicroView(!isLianMicroUsed, isInLianMicroOrApply, onLianMicroBtnClickListener);
        }
    }

    /**
     * 嘉宾申请连麦强提示
     *
     * @param roomMicroApplyInfo
     */
    private void showApplyTipsView(RoomMicroApplyInfo roomMicroApplyInfo) {
        if (roomMicroApplyInfo == null || roomMicroApplyInfo.getUserInfo() == null) {
            return;
        }
        micContainerView.setOnClickListener(v -> showMicApplyListDialog());
        micContainerView.addView(roomMicroApplyInfo.getUserInfo().getAvatar());
    }

    /**
     * 退出房间并查看收益
     */
    private void exitRoomWithIncome(long roomId, long roomUid) {
        microView.setRoomOwnerAgoraViewBackground(getResources().getColor(R.color.video_micro_view_bg));
        if (getActivity() instanceof RoomFrameActivity) {
            ((RoomFrameActivity) getActivity()).showOwnerRoomFinishFragment(roomId, roomUid);
        } else {
            finish();
        }
    }

    @Override
    protected void showRoomFaceuDialog() {
        LiveRoomFaceuDialog dialog = new LiveRoomFaceuDialog(getContext());
        FURenderer fuRender = microView.getFURenderer();
        if (fuRender == null) {
            LogUtil.i("fuRender is null");
            SingleToastUtil.showToast("视屏渲染出错咯");
        } else {
            dialog.setFaceUnityControlListener(fuRender);
            dialog.show();
        }
    }

    protected void changeState() {
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        if (unreadCount > 0) {
            bottomView.showMsgMark(true);
        } else {
            bottomView.showMsgMark(false);
        }
    }

    @Override
    public boolean onBack() {
        if (isResumed()) {
            if (getGiftSelector().isShowing()) {
                getGiftSelector().hide();
            } else {
                showExitTipDialog();
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isDoInitResume) {
            microView.onResume();
        } else {
            isDoInitResume = true;
        }
    }

    // 主播房间操作不暂停直播
    @Override
    public void onStop() {
        super.onStop();
        microView.onPause();
    }

}
