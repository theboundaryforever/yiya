package com.yiya.mobile.room.egg.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.room.egg.adapter.PlantRewardRecordAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.yiya.mobile.room.egg.PlantBeanDialog.LOTTERYTYPE_1;
import static com.yiya.mobile.room.egg.PlantBeanDialog.LOTTERYTYPE_10;
import static com.yiya.mobile.room.egg.PlantBeanDialog.LOTTERYTYPE_100;
import static com.yiya.mobile.room.egg.PlantBeanDialog.LOTTERYTYPE_50;

/**
 * ProjectName:
 * Description:种豆获得奖品dialog
 * Created by BlackWhirlwind on 2019/7/19.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantResultDialog extends BaseTransparentDialogFragment {

    private static final String KEY_LIST = "KEY_LIST";
    private static final String KEY_TYPE = "KEY_TYPE";
    @BindView(R.id.rv_reward)
    RecyclerView rvReward;
    @BindView(R.id.iv_nutrition)
    ImageView ivNutrition;
    private OnNutritionClickListener mListener;
    private PlantRewardRecordAdapter mAdapter;

    public static PlantResultDialog newInstance(List<EggGiftInfo> list, int lotteryType) {
        PlantResultDialog dialog = new PlantResultDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_LIST, (Serializable) list);
        bundle.putInt(KEY_TYPE, lotteryType);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_plant_result_list;
    }

    @Override
    protected int getWindowAnimationStyle() {
        return R.style.WindowBottomAnimationStyle;
    }

    @Override
    protected int getWindowGravity() {
        return Gravity.BOTTOM;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            List<EggGiftInfo> list = (List<EggGiftInfo>) getArguments().getSerializable(KEY_LIST);
            if (ListUtils.isNotEmpty(list)) {
                rvReward.setLayoutManager(new GridLayoutManager(getContext(), 3));
                rvReward.setHasFixedSize(true);
                mAdapter = new PlantRewardRecordAdapter(list);
                rvReward.setAdapter(mAdapter);
            }
            int type = getArguments().getInt(KEY_TYPE);
            int resDrawable = R.drawable.bg_plant_bean_nutrition_once;
            switch (type) {
                case LOTTERYTYPE_1:
                    resDrawable = R.drawable.bg_plant_bean_nutrition_once;
                    break;
                case LOTTERYTYPE_10:
                    resDrawable = R.drawable.bg_plant_bean_10_times_normal;
                    break;
                case LOTTERYTYPE_50:
                    resDrawable = R.drawable.bg_plant_bean_50_times_normal;
                    break;
                case LOTTERYTYPE_100:
                    resDrawable = R.drawable.bg_plant_bean_100_times_normal;
                    break;
            }
            ivNutrition.setImageResource(resDrawable);
        }
    }

    public PlantResultDialog setOnNutritionClickListener(OnNutritionClickListener listener) {
        mListener = listener;
        return this;
    }

    @OnClick({R.id.iv_confirm, R.id.iv_nutrition})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_confirm:
                dismiss();
                break;
            case R.id.iv_nutrition:
                if (mListener != null) {
                    mListener.onNutritionClick();
                }
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mListener != null) {
            mListener.onDismiss();
        }
    }

    public void setData(List<EggGiftInfo> eggGiftInfos) {
        if (ListUtils.isNotEmpty(eggGiftInfos)) {
            mAdapter.setNewData(eggGiftInfos);
        }
    }

    public interface OnNutritionClickListener {

        void onNutritionClick();

        void onDismiss();
    }
}
