package com.yiya.mobile.room.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.R;

import org.jetbrains.annotations.NotNull;

/**
 * 连麦申请弹框
 *
 * @author zeda
 */
public class LianMicroCloseDialog extends BaseDialogFragment {

    private static final String MESSAGE = "MESSAGE";

    private String mMessage;

    public static LianMicroCloseDialog newInstance(String message) {
        LianMicroCloseDialog closeDialog = new LianMicroCloseDialog();
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, message);
        closeDialog.setArguments(bundle);
        return closeDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            mMessage = arguments.getString(MESSAGE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View rootView = inflater.inflate(R.layout.dialog_lian_micro_close, window.findViewById(android.R.id.content), false);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setWindowAnimations(R.style.WindowBottomAnimationStyle);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        TextView tvContent = view.findViewById(R.id.tv_content);
        if (!TextUtils.isEmpty(mMessage)) {
            tvContent.setText(mMessage);
        }
    }

}
