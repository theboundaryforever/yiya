package com.yiya.mobile.room.avroom.widget.dialog;

//import android.app.Dialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.os.Bundle;
//import android.view.Gravity;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//
//import BaseMvpActivity;
//import RoomFrameActivity;
//import com.yiya.mobile.room.avroom.activity.OpenRoomActivity;
//import DialogManager;
//import com.tongdaxing.erban.R;
//import com.tongdaxing.xchat_core.auth.IAuthCore;
//import com.tongdaxing.xchat_core.room.IRoomCore;
//import com.tongdaxing.xchat_core.room.IRoomCoreClient;
//import com.tongdaxing.xchat_core.room.bean.RoomInfo;
//import com.tongdaxing.xchat_core.user.IUserCore;
//import com.tongdaxing.xchat_core.user.bean.UserInfo;
//import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
//import com.tongdaxing.xchat_framework.coremanager.CoreManager;
//
//import static com.tongdaxing.xchat_core.user.bean.UserInfo.USER_TYPE_OFFICIAL;

/**
 * 打开房间类型dialog
 *
 * @author chenran
 * @date 2017/8/3
 */

//public class RoomTypeSelectDialog extends Dialog implements View.OnClickListener {
//    private Context context;
//    private int roomType;
//
//    public RoomTypeSelectDialog(Context context, boolean cancelable,
//                                DialogInterface.OnCancelListener cancelListener) {
//        super(context, cancelable, cancelListener);
//        this.context = context;
//    }
//
//    public RoomTypeSelectDialog(Context context) {
//        super(context, R.style.BottomSelectDialogNotTransparent);
//        this.context = context;
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        CoreManager.addClient(this);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.dialog_bottom_type_select);
//        findViewById(R.id.open_chat_light_layout).setOnClickListener(this);
//        findViewById(R.id.open_auction_layout).setOnClickListener(this);
//        findViewById(R.id.open_home_party_layout).setOnClickListener(this);
//        findViewById(R.id.close_image).setOnClickListener(this);
//
//        setDialogShowAttributes(getContext(), this);
//    }
//
//    public static void setDialogShowAttributes(Context context, Dialog dialog) {
//
//        WindowManager winManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
//        int mScreenWidth = winManager.getDefaultDisplay().getWidth();
//        Window mWindow = dialog.getWindow();
//        mWindow.getDecorView().setPadding(0, 0, 0, 0);
//        WindowManager.LayoutParams Params = mWindow.getAttributes();
//        Params.windowAnimations = R.style.bottom_dialog_anim_style;
//        Params.width = WindowManager.LayoutParams.MATCH_PARENT;
//        Params.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        mWindow.setAttributes(Params);
//        mWindow.setGravity(Gravity.BOTTOM);
//    }
//
//    @Override
//    public void onClick(View v) {
//        String waitingText = context.getString(R.string.waiting_text);
//        switch (v.getId()) {
//            case R.id.open_auction_layout:
//                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//                if (userInfo != null) {
//                    if (userInfo.getDefUser() == USER_TYPE_OFFICIAL) {
//                        ((BaseMvpActivity) context).getDialogManager().showProgressDialog(context, waitingText);
//                        roomType = RoomInfo.ROOMTYPE_AUCTION;
//                        CoreManager.getCore(IRoomCore.class).requestRoomInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid(), 0);
//                    } else {
//                        ((BaseMvpActivity) context).toast("您暂无权限开竞拍房");
//                    }
//                }
//                break;
//            case R.id.open_home_party_layout:
//                ((BaseMvpActivity) context).getDialogManager().showProgressDialog(context, waitingText);
//                roomType = RoomInfo.ROOMTYPE_HOME_PARTY;
//                CoreManager.getCore(IRoomCore.class).requestRoomInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid(), 0);
//                break;
//            case R.id.open_chat_light_layout:
//                roomType = RoomInfo.ROOMTYPE_LIGHT_CHAT;
//                ((BaseMvpActivity) context).getDialogManager().showProgressDialog(context, waitingText);
//                CoreManager.getCore(IRoomCore.class).requestRoomInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid(), 0);
//                break;
//            case R.id.close_image:
//                dismiss();
//                break;
//            default:
//        }
//    }
//
//    private void openRoom() {
//        if (roomType == RoomInfo.ROOMTYPE_HOME_PARTY) {
//            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//            CoreManager.getCore(IRoomCore.class).openRoom(userInfo.getUid(), roomType);
//        } else if (roomType == RoomInfo.ROOMTYPE_AUCTION) {
//            ((BaseMvpActivity) context).getDialogManager().dismissDialog();
//            OpenRoomActivity.startAuction(context);
//            dismiss();
//        } else {
//            ((BaseMvpActivity) context).getDialogManager().dismissDialog();
//            OpenRoomActivity.startHomeParty(context);
//            dismiss();
//        }
//    }
//
//    @Override
//    public void onDetachedFromWindow() {
//        super.onDetachedFromWindow();
//        CoreManager.removeClient(this);
//        context = null;
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onGetRoomInfo(RoomInfo roomInfo, int pageType) {
//        if (roomInfo != null) {
//            if (roomInfo.isValid()) {
//                if (roomInfo.getType() != roomType) {
//                    ((BaseMvpActivity) context).getDialogManager().dismissDialog();
//                    String roomName = "";
//                    if (roomInfo.getType() == RoomInfo.ROOMTYPE_AUCTION) {
//                        roomName = "竞拍房";
//                    } else if (roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
//                        roomName = "轰趴房";
//                    } else {
//                        roomName = "轻聊房";
//                    }
//                    ((BaseMvpActivity) context).getDialogManager().showOkCancelDialog(
//                            "当前正在" + roomName + "中，是否关闭当前房间？", true,
//                            new DialogManager.OkCancelDialogListener() {
//                                @Override
//                                public void onCancel() {
//
//                                }
//
//                                @Override
//                                public void onOk() {
//                                    ((BaseMvpActivity) context).getDialogManager().showProgressDialog(context, "请稍后...");
//                                    CoreManager.getCore(IRoomCore.class).closeRoomInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
//                                }
//                            });
//                } else {
//                    ((BaseMvpActivity) context).getDialogManager().dismissDialog();
//                    RoomFrameActivity.start(context, roomInfo.getUid());
//                    dismiss();
//                }
//            } else {
//                openRoom();
//            }
//
//        } else {
//            openRoom();
//        }
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onGetRoomInfoFail(int errorno,String error, int pageType) {
//        ((BaseMvpActivity) context).getDialogManager().dismissDialog();
//        ((BaseMvpActivity) context).toast(error);
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onCloseRoomInfo() {
//        ((BaseMvpActivity) context).getDialogManager().dismissDialog();
//        openRoom();
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onCloseRoomInfoFail(String error) {
//        ((BaseMvpActivity) context).getDialogManager().dismissDialog();
//        ((BaseMvpActivity) context).toast(error);
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onOpenRoom(RoomInfo roomInfo) {
//        ((BaseMvpActivity) context).getDialogManager().dismissDialog();
//        RoomFrameActivity.start(context, roomInfo.getUid());
//        dismiss();
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onOpenRoomFail(String error) {
//        ((BaseMvpActivity) context).getDialogManager().dismissDialog();
//        ((BaseMvpActivity) context).toast(error);
//    }
//
//    @CoreEvent(coreClientClass = IRoomCoreClient.class)
//    public void onAlreadyOpenedRoom() {
//        ((BaseMvpActivity) context).getDialogManager().showProgressDialog(context, "请稍后...");
//        CoreManager.getCore(IRoomCore.class).closeRoomInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
//    }
//}
