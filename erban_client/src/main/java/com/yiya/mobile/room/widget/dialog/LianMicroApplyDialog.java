package com.yiya.mobile.room.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.bean.LianMicroStatusInfo;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

/**
 * 连麦申请弹框
 *
 * @author zeda
 */
public class LianMicroApplyDialog extends BaseDialogFragment {

    private LianMicroStatusInfo statusInfo;

    public static LianMicroApplyDialog newInstance(@NonNull LianMicroStatusInfo statusInfo, LianMicroDialogFactory.OnLianMicroDialogClickListener onLianMicroDialogClickListener) {
        LianMicroApplyDialog lianMicroApplyDialog = new LianMicroApplyDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("status_info", statusInfo);
        lianMicroApplyDialog.setArguments(bundle);
        lianMicroApplyDialog.setOnLianMicroDialogClickListener(onLianMicroDialogClickListener);
        return lianMicroApplyDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            statusInfo = (LianMicroStatusInfo) arguments.getSerializable("status_info");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View rootView = inflater.inflate(R.layout.dialog_lian_micro_apply, window.findViewById(android.R.id.content), false);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setWindowAnimations(R.style.WindowBottomAnimationStyle);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);

        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        rootView.findViewById(R.id.btn_room_lian_micro_video).setOnClickListener(v -> {
            if (onLianMicroDialogClickListener != null) {
                onLianMicroDialogClickListener.onVideoBtnClick();
            }
        });
        rootView.findViewById(R.id.btn_room_lian_micro_audio).setOnClickListener(v -> {
            if (onLianMicroDialogClickListener != null) {
                onLianMicroDialogClickListener.onAudioBtnClick();
            }
        });
        TextView onlineNumTv = rootView.findViewById(R.id.tv_room_lian_micro_online_num);

        onlineNumTv.setText(String.format(Locale.getDefault(), "当前申请:%d", statusInfo.getConnectNum()));
    }

    private LianMicroDialogFactory.OnLianMicroDialogClickListener onLianMicroDialogClickListener;

    public void setOnLianMicroDialogClickListener(LianMicroDialogFactory.OnLianMicroDialogClickListener onLianMicroDialogClickListener) {
        this.onLianMicroDialogClickListener = onLianMicroDialogClickListener;
    }

    @Override
    public void onDestroyView() {
        onLianMicroDialogClickListener = null;
        super.onDestroyView();
    }
}
