package com.yiya.mobile.room.avroom.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.tongdaxing.xchat_core.gift.ComboGiftVoInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.utils.ThreadUtil;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.LinkedList;

/**
 * Created by Administrator on 2019/4/7.
 */

public class ComboGiftView extends LinearLayout {
    public final static String TAG = ComboGiftView.class.getSimpleName();

    private Handler mHandler;

    private int mMaxItem = 2;

    private LinkedList<ComboGiftVoInfo> giftVoInfos = new LinkedList<>();// 本地缓存

    //存活的时间
    private static final int LOADING_TIME = 2000;

    GiftLiveRunnable giftLiveRunnable;

    private volatile boolean isLoopQuit = false;

    public ComboGiftView(Context context) {
        super(context);
        init();
    }

    public ComboGiftView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ComboGiftView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER);

        mHandler = new Handler();

        giftVoInfos.clear();

        giftLiveRunnable = new GiftLiveRunnable();
        ThreadUtil.runInThread(giftLiveRunnable);
    }

    /**
     * 检查view的生命周期
     */
    private class GiftLiveRunnable implements Runnable {
        @Override
        public void run() {
            while (!isLoopQuit) {
                try {
                    long currentTime = System.currentTimeMillis();
                    int viewCount = getChildCount();
                    for (int i = 0; i < viewCount; i++) {
                        ComboGiftItemView childView = (ComboGiftItemView) getChildAt(i);
                        ComboGiftVoInfo childInfo = childView.getComboGiftVoInfo();
                        if (childInfo != null && currentTime - childInfo.getStartTime() >= LOADING_TIME) {
                            if (childInfo.getGiftAnim() == ComboGiftVoInfo.GiftAnim.LOADING) {
                                removeView(i);
                            }
                        }
                    }
                    Thread.sleep(200);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 添加giftInfo给本地缓存队列
     *
     * @param giftReceiveInfo
     * @return
     */
    public void addGiftInfo(GiftReceiveInfo giftReceiveInfo) {
        ComboGiftVoInfo voInfo = assembleComboGiftVo(giftReceiveInfo);
        showComboItemView(voInfo);
    }

    public void showComboItemView(ComboGiftVoInfo voInfo){
        if (voInfo == null)return;
        ComboGiftItemView itemView = findViewByComboId(voInfo.getComboId());
        int currentCount = getChildCount();
        if (itemView == null) { // 当前没有找到活动的item
            if (currentCount < mMaxItem) { // 当前活动的itemview少于两个
                ComboGiftItemView comboGiftItemView = new ComboGiftItemView(getContext());
                addView(comboGiftItemView);
                comboGiftItemView.updateCombo(voInfo);
            } else {
                giftVoInfos.addLast(voInfo);
            }
        } else { // 找到活动的item
            itemView.updateCombo(voInfo);
        }
    }

    /**
     * // 组装Vo
     *
     * @param giftReceiveInfo
     * @return
     */
    private ComboGiftVoInfo assembleComboGiftVo(GiftReceiveInfo giftReceiveInfo) {
        ComboGiftVoInfo Vo = new ComboGiftVoInfo();
        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftReceiveInfo.getGiftId());
        if (giftInfo == null)return null;

        Vo.setCurrentNum(giftReceiveInfo.getGiftNum());
        Vo.setComboCount(giftReceiveInfo.getComboCount());
        Vo.setComboFrequencyCount(giftReceiveInfo.getComboFrequencyCount());

        Vo.setAvatar(giftReceiveInfo.getAvatar());
        Vo.setNick(giftReceiveInfo.getNick());
        Vo.setContent(giftInfo.getGiftName());
        Vo.setGiftId(giftInfo.getGiftId());
        Vo.setGiftUrl(giftInfo.getGiftUrl());

        Vo.setComboId(giftReceiveInfo.getUid() + "-" + giftReceiveInfo.getGiftId() + "-" + giftReceiveInfo.getComboId());
        Log.d(TAG, "GiftDialog assembleComboGiftVo: ComboId = " + Vo.getComboId());
        return Vo;
    }

    /**
     * 找到对应ComboId的ComboGiftItemView
     *
     * @param comboId
     * @return
     */
    public ComboGiftItemView findViewByComboId(String comboId) {
        ComboGiftItemView giftView = null;
        for (int i = 0; i < getChildCount(); i++) {
            View childView = getChildAt(i);
            if (childView instanceof ComboGiftItemView) {
                giftView = (ComboGiftItemView) childView;
                ComboGiftVoInfo childInfo = giftView.getComboGiftVoInfo();
//                if (childInfo.getComboId() == comboId && childInfo.getGiftAnim() == ComboGiftVoInfo.GiftAnim.LOADING) {
                if (childInfo.getComboId().equals(comboId) && childInfo.getGiftAnim() == ComboGiftVoInfo.GiftAnim.LOADING) {
                    return giftView;
                }
            }
        }
        return null;
    }

    private void removeView(int index) {
        if (index >= getChildCount()) {
            return;
        }
        Log.d(TAG, "removeView: ------>");
        final ComboGiftItemView comboGiftItemView = (ComboGiftItemView) getChildAt(index);
        comboGiftItemView.getComboGiftVoInfo().setGiftAnim(ComboGiftVoInfo.GiftAnim.DISAPPEARING);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(comboGiftItemView, "translationX", 0, -comboGiftItemView.getMeasuredWidth());
                objectAnimator.setDuration(400);
                objectAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        comboGiftItemView.getComboGiftVoInfo().setGiftAnim(ComboGiftVoInfo.GiftAnim.DISAPPEARED);
                        comboGiftItemView.setVisibility(View.GONE);
                        removeView(comboGiftItemView);
                    }
                });
                objectAnimator.start();
            }
        });
    }

    public void clear() {
        isLoopQuit = true;
        ThreadUtil.cancelThread(giftLiveRunnable);
        giftLiveRunnable = null;
        mHandler.removeCallbacksAndMessages(null);
        removeAllViews();
        giftVoInfos.clear();
    }
}
