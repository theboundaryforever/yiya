package com.yiya.mobile.room.avroom.adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.model.attention.AttentionModel;
import com.yiya.mobile.room.avroom.widget.MessageView;
import com.yiya.mobile.ui.widget.UrlImageSpan;
import com.yiya.mobile.view.htmlTextView.HtmlTextView;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.custom.bean.BurstGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.FaceAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LuckyGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PublicChatRoomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomHtmlTextAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRuleAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.TipMsgGoAttentionAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.face.FaceInfo;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.im.IMReportRoute;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.RichTextUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_SHARE_ROOM;
import static com.tongdaxing.xchat_core.manager.IMNetEaseManager.MESSAGE_COUNT_LOCAL_LIMIT;

/**
 * 公屏消息adapter
 */
public class MessageAdapter extends BaseQuickAdapter<ChatRoomMessage, BaseViewHolder> implements View.OnClickListener {
    private String account;

    public MessageAdapter() {
        super(R.layout.list_item_chatrrom_msg);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, ChatRoomMessage chatRoomMessage) {
        if (chatRoomMessage == null) return;
        //初始化布局  默认 显示基本消息容器 msgContainer：默认是带有等级和萌新标签的
        HtmlTextView tvContent = baseViewHolder.getView(R.id.tv_content);
        tvContent.setOnClickListener(this);
        tvContent.setTag(chatRoomMessage);
        tvContent.setText("");
//        int padding = DisplayUtils.dip2px(mContext, 5);
//        tvContent.setPadding(padding, padding, padding, padding);
        tvContent.setBackgroundResource(R.drawable.ic_msg_bg_default);

        if (IMReportRoute.ChatRoomTip.equalsIgnoreCase(chatRoomMessage.getRoute())) {//// 系统提示消息  - 不显示等级
            tvContent.setTextColor(mContext.getResources().getColor(R.color.color_41FE88));
            tvContent.setText(chatRoomMessage.getContent());
        } else if (IMReportRoute.sendTextReport.equalsIgnoreCase(chatRoomMessage.getRoute())) {// 普通消息
            setMsgText(chatRoomMessage, tvContent);
        } else if (IMReportRoute.chatRoomMemberIn.equalsIgnoreCase(chatRoomMessage.getRoute())) {// 有人进入房间
            setMsgNotification(chatRoomMessage, tvContent);
        } else if (IMReportRoute.sendMessageReport.equalsIgnoreCase(chatRoomMessage.getRoute())) {
            //自定义消息默认不显示等级
            IMCustomAttachment attachment = chatRoomMessage.getAttachment();
            if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_ROOM_TIP) {
                // 没有等级
                if (attachment.getSecond() == IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_GO_ATTENTION_ROOM) {
                    // 房间待够固定的时间就弹出提示框
//                    setTipMsgGoAttentionRoom(msgContainer, faceContainer, attachment);
                } else if (attachment.getSecond() == CUSTOM_MSG_SECOND_ROOM_TIP_SHARE_ROOM
                        || attachment.getSecond() == IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_ATTENTION_ROOM_OWNER) {// 分享房间消息  xxx分享了房间
                    setMsgRoomTip(tvContent, (RoomTipAttachment) attachment, chatRoomMessage.getImChatRoomMember());
                }
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {// 公屏赠送礼物
                setMsgHeaderGift(tvContent, chatRoomMessage);
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {// 全麦赠送
                setMsgMultiGift(tvContent, chatRoomMessage);
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE) {
                setMsgFace(tvContent, chatRoomMessage);
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_LOTTERY_BOX) {// 砸蛋
                setLotteryInfo((LotteryBoxAttachment) attachment, tvContent);
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MATCH) { // 速配消息
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_FIRST) { //Pk消息
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST) {// 房间规则
                if (attachment instanceof RoomRuleAttachment) {
                    tvContent.setTextColor(mContext.getResources().getColor(R.color.color_FFFFF600));
                    tvContent.setText(((RoomRuleAttachment) attachment).getRule());
                }
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_TYPE_BURST_GIFT) {// 爆出xx
                setBurstGift(tvContent, (BurstGiftAttachment) attachment);
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE) {// 屏蔽礼物特效等系统消息
                setSystemMsg(attachment.getSecond(), (RoomQueueMsgAttachment) attachment, tvContent);
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_ROOM_RED_PACKAGE) {// 房间红包消息
                setRedPackageMsgText(attachment, tvContent);
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_LuckyGift
                    && attachment.getSecond() == IMCustomAttachment.CUSTOM_MSG_SECOND_ALL_LuckyGift) {// 幸运礼物
                setLuckyGiftText(attachment, tvContent);
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_TYPE_COIN) {//豆币消息
                if (attachment.getSecond() == IMCustomAttachment.CUSTOM_MSG_SECOND_TYPE_COIN_ALL) {
                    //豆币全服消息
                    setCoinAllMsg((RoomHtmlTextAttachment) attachment, tvContent);
                }
            } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_HTML_FIRST) {//HTML 消息
                setHtmlMsg((RoomHtmlTextAttachment) attachment, tvContent);
//                if (attachment.getSecond() == IMCustomAttachment.CUSTOM_HTML_SECOND_PUB_ON ||
//                        attachment.getSecond() == IMCustomAttachment.CUSTOM_HTML_SECOND_PUB_OFF) {// 开关公屏消息
//                    setHtmlMsg((RoomHtmlTextAttachment) attachment, tvContent);
//                }
            }
        } else if (IMReportRoute.sendPkMsg.equalsIgnoreCase(chatRoomMessage.getRoute())) {
            setCPKMsg(chatRoomMessage.getContent(), tvContent);
        }
    }//新增显示在公屏中的自定义消息避免对msgContainer显示隐藏，这里没有做多布局处理，可能出现出现消息列表的重用bug

    /**
     * 礼物占位符
     */
    public static final String GIFT_PLACEHOLDER = "gift_placeholder";
    /**
     * 等级占位符
     */
    public static final String LEVEL_PLACEHOLDER = "level_placeholder";
    /**
     * 标签占位符
     */
    public static final String LABEL_PLACEHOLDER = "label_placeholder";

    /**
     * 专属勋章占位符
     */
    public static final String MEDAL_PLACEHOLDER = "medal_placeholder";

    /**
     * 萌新占位图
     */
    private void setNewLabel(SpannableStringBuilder builder, String userLabelUrl, HtmlTextView textView) {
        if (userLabelUrl != null) {
            builder.append(LEVEL_PLACEHOLDER);
            UrlImageSpan urlImageSpan = new UrlImageSpan(mContext, userLabelUrl, textView);
            urlImageSpan.setImgWidth(DisplayUtils.dip2px(mContext, 42));// 24 14
            urlImageSpan.setImgHeight(DisplayUtils.dip2px(mContext, 18));
            builder.setSpan(urlImageSpan, builder.toString().length() - LEVEL_PLACEHOLDER.length(),
                    builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    /**
     * 专属勋章
     *
     * @param builder
     * @param medalUrl
     */
    private void setMedalLabel(SpannableStringBuilder builder, String medalUrl, HtmlTextView textView) {
        builder.append(MEDAL_PLACEHOLDER);
        int start = builder.toString().length() - MEDAL_PLACEHOLDER.length();
        int end = builder.toString().length();
        UrlImageSpan imageSpan = new UrlImageSpan(mContext, medalUrl, textView);
        imageSpan.setImgWidth(DisplayUtils.dip2px(mContext, 23));
        imageSpan.setImgHeight(DisplayUtils.dip2px(mContext, 22));
        builder.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(" ");//添加空格
    }

    /**
     * 礼物占位图
     */
    private void setImageLabel(SpannableStringBuilder builder, String Url, HtmlTextView textView) {
        builder.append(LABEL_PLACEHOLDER);
        int start = builder.toString().length() - LABEL_PLACEHOLDER.length();
        int end = builder.toString().length();
        UrlImageSpan imageSpan = new UrlImageSpan(mContext, Url, textView);
        imageSpan.setImgWidth(DisplayUtils.dip2px(mContext, 20));
        imageSpan.setImgHeight(DisplayUtils.dip2px(mContext, 20));
        builder.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(" ");//添加空格
    }

    /**
     * 文字颜色
     */
    private void setStrColor(SpannableStringBuilder builder, int start, int end, int color) {
        ForegroundColorSpan redSpan = new ForegroundColorSpan(mContext.getResources().getColor(color));
        builder.setSpan(redSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    private void setStrColor(SpannableStringBuilder builder, int start, int end, String color) {
        ForegroundColorSpan redSpan = new ForegroundColorSpan(Color.parseColor(color != null ? color : "#1A1A1A"));
        builder.setSpan(redSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    /**
     * 用户等级
     */
    private void setLevel(SpannableStringBuilder builder, String userLevelUrl, String experLevelUrl, HtmlTextView textView) {
        if (userLevelUrl != null) {
            builder.append(LEVEL_PLACEHOLDER);
            UrlImageSpan urlImageSpan = new UrlImageSpan(mContext, userLevelUrl, textView);
            urlImageSpan.setImgWidth(DisplayUtils.dip2px(mContext, 42));// 24 14
            urlImageSpan.setImgHeight(DisplayUtils.dip2px(mContext, 18));
            builder.setSpan(urlImageSpan, builder.toString().length() - LEVEL_PLACEHOLDER.length(),
                    builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        if (experLevelUrl != null) {
            builder.append(" ");//添加空格
            builder.append(LEVEL_PLACEHOLDER);
            UrlImageSpan urlImageSpan = new UrlImageSpan(mContext, experLevelUrl, textView);
            urlImageSpan.setImgWidth(DisplayUtils.dip2px(mContext, 42));// 24 14
            urlImageSpan.setImgHeight(DisplayUtils.dip2px(mContext, 18));
            builder.setSpan(urlImageSpan, builder.toString().length() - LEVEL_PLACEHOLDER.length(),
                    builder.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }

        builder.append(" ");//添加空格
    }

    /**
     * 发送魅力值PK文本
     *
     * @param msg       文本属性
     * @param tvContent 控件
     */
    private void setCPKMsg(String msg, HtmlTextView tvContent) {
        SpannableStringBuilder builder = new SpannableStringBuilder(msg);
        setStrColor(builder, 0, builder.toString().length(), R.color.color_FFA8A8);
        tvContent.setText(builder);
    }

    /**
     * 普通聊天信息
     *
     * @param chatRoomMessage msg
     * @param tvContent       文本内容
     */
    private void setMsgText(ChatRoomMessage chatRoomMessage, HtmlTextView tvContent) {
        String senderNick = "";
        String content = "";
        IMChatRoomMember member = chatRoomMessage.getImChatRoomMember();
        if (member == null) {
            senderNick = "我：";
        } else {
            if (AvRoomDataManager.get().isOwner(member.getAccount())) {
                senderNick = "我：";
            } else {
                senderNick = member.getNick() + "：";
            }
        }
        SpannableStringBuilder builder = new SpannableStringBuilder(content);

        if (member != null) {

            if (member.getVideo_room_exper_level() < 5 && member.getExperLevel() < 5){
                setNewLabel(builder, member.getIs_new_user_pic(), tvContent);
            }

            setMedalLabel(builder, member.getMedal_url(), tvContent);
            setLevel(builder,
                    member.getVideo_room_exper_level_pic(),
                    member.getExper_level_pic(),
                    tvContent);
        }

        builder.append(senderNick);
        setStrColor(builder, builder.toString().length() - senderNick.length(), builder.toString().length(), R.color.color_FFFFF600);

        builder.append(chatRoomMessage.getContent());
        setStrColor(builder, builder.toString().length() - chatRoomMessage.getContent().length(), builder.toString().length(), R.color.white);

        tvContent.setText(builder);
    }

    private void setMsgRoomAttention(IMCustomAttachment attachment, HtmlTextView tvContent) {
        PublicChatRoomAttachment redPackageAttachment = (PublicChatRoomAttachment) attachment;
        SpannableStringBuilder builder = new SpannableStringBuilder(redPackageAttachment.getMsg());
        setStrColor(builder, 0, builder.toString().length(), R.color.color_FFA8A8);
        tvContent.setText(builder);
    }

    /**
     * 设置红包收发消息样式
     *
     * @param attachment attachment
     * @param tvContent  文本内容
     */
    private void setRedPackageMsgText(IMCustomAttachment attachment, HtmlTextView tvContent) {
        PublicChatRoomAttachment redPackageAttachment = (PublicChatRoomAttachment) attachment;
        SpannableStringBuilder builder = new SpannableStringBuilder();
//        setLevel(builder, redPackageAttachment.getExperLevel());
        builder.append(redPackageAttachment.getMsg());
        setStrColor(builder, builder.toString().length() - redPackageAttachment.getMsg().length(), builder.toString().length(), redPackageAttachment.getTxtColor());
        tvContent.setText(builder);
    }

    private void setLuckyGiftText(IMCustomAttachment attachment, HtmlTextView tvContent) {
        LuckyGiftAttachment luckyGiftAttachment = (LuckyGiftAttachment) attachment;
        String nick = luckyGiftAttachment.getNick();
        String proportion = (int) luckyGiftAttachment.getProportion() + "倍";

        String temp = "哇塞，恭喜 ";
        SpannableStringBuilder builder = new SpannableStringBuilder(temp);
        setStrColor(builder, 0, temp.length(), R.color.white);
        builder.append(nick);
        setStrColor(builder, builder.toString().length() - nick.length(), builder.toString().length(), R.color.color_FFFFF600);
        temp = " 通过赠送幸运礼物获得 ";
        builder.append(temp);
        setStrColor(builder, builder.toString().length() - temp.length(), builder.toString().length(), R.color.white);
        builder.append(proportion);
        setStrColor(builder, builder.toString().length() - proportion.length(), builder.toString().length(), R.color.color_FFFFF600);
        temp = " 返币";
        builder.append(temp);
        setStrColor(builder, builder.toString().length() - temp.length(), builder.toString().length(), R.color.white);

        tvContent.setBackgroundResource(R.drawable.ic_msg_bg_full_purple);

        tvContent.setText(builder);
    }

    //     公屏xxx赠送给xxx礼物
    private void setMsgHeaderGift(HtmlTextView tvContent, ChatRoomMessage chatRoomMessage) {
        GiftAttachment attachment = (GiftAttachment) chatRoomMessage.getAttachment();
        GiftReceiveInfo giftReceiveInfo = attachment.getGiftRecieveInfo();
        IMChatRoomMember member = chatRoomMessage.getImChatRoomMember();

        if (giftReceiveInfo == null)
            return;
        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftReceiveInfo.getGiftId());
        if (giftInfo != null) {
            String nick = attachment.getGiftRecieveInfo().getNick();
            String targetNick = attachment.getGiftRecieveInfo().getTargetNick();

            if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
                nick = nick.substring(0, 5) + "... ";
            }

            String content = "";
            SpannableStringBuilder builder = new SpannableStringBuilder(content);

            if (member != null) {
                if (member.getVideo_room_exper_level() < 5 && member.getExperLevel() < 5){
                    setNewLabel(builder, member.getIs_new_user_pic(), tvContent);
                }
                setLevel(builder,
                        member.getVideo_room_exper_level_pic(),
                        member.getExper_level_pic(),
                        tvContent);
            }

            String tmp = null;

            builder.append(nick);
            setStrColor(builder, builder.toString().length() - nick.length(), builder.toString().length(), R.color.color_FFFFF600);
            tmp = " 赠送给 ";
            builder.append(tmp);
            setStrColor(builder, builder.toString().length() - tmp.length(), builder.toString().length(), R.color.white);

            tmp = targetNick + ' ';
            builder.append(tmp);
            setStrColor(builder, builder.toString().length() - tmp.length(), builder.toString().length(), R.color.color_FFFFF600);

            tmp = giftInfo.getGiftName();
            builder.append(tmp);
            setStrColor(builder, builder.toString().length() - tmp.length(), builder.toString().length(), R.color.color_FFFFF600);

            setImageLabel(builder, giftInfo.getGiftUrl(), tvContent);

            tmp = " X " + attachment.getGiftRecieveInfo().getGiftNum();
            builder.append(tmp);
            setStrColor(builder, builder.toString().length() - tmp.length(), builder.toString().length(), R.color.white);

            tvContent.setText(builder);
        }
    }

    /**
     * 全麦赠送
     *
     * @param tvContent       控件ID
     * @param chatRoomMessage 消息实体
     */
    private void setMsgMultiGift(HtmlTextView tvContent, ChatRoomMessage chatRoomMessage) {
        GiftAttachment attachment = (GiftAttachment) chatRoomMessage.getAttachment();
        GiftReceiveInfo multiGiftReceiveInfo = attachment.getGiftRecieveInfo();
        IMChatRoomMember member = chatRoomMessage.getImChatRoomMember();
        if (multiGiftReceiveInfo == null) {
            return;
        }
        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(multiGiftReceiveInfo.getGiftId());
        if (giftInfo != null) {
            String tmp = null;
            String senderNick = attachment.getGiftRecieveInfo().getNick();
            if (!TextUtils.isEmpty(senderNick) && senderNick.length() > 6) {
                senderNick = senderNick.substring(0, 5) + "... ";
            }
            String content = "";
            SpannableStringBuilder builder = new SpannableStringBuilder(content);

            if (member != null) {
                if (member.getVideo_room_exper_level() < 5 && member.getExperLevel() < 5){
                    setNewLabel(builder, member.getIs_new_user_pic(), tvContent);
                }
                setLevel(builder,
                        member.getVideo_room_exper_level_pic(),
                        member.getExper_level_pic(),
                        tvContent);
            }

            builder.append(senderNick);

            setStrColor(builder, builder.toString().length() - senderNick.length(), builder.toString().length(), R.color.color_FFFFF600);
            tmp = " 全麦送出 ";
            builder.append(tmp);
            setStrColor(builder, builder.toString().length() - tmp.length(), builder.toString().length(), R.color.white);

            tmp = giftInfo.getGiftName() + ' ';
            builder.append(tmp);
            setStrColor(builder, builder.toString().length() - tmp.length(), builder.toString().length(), R.color.color_FFFFF600);

            setImageLabel(builder, giftInfo.getGiftUrl(), tvContent);

            tmp = " X " + attachment.getGiftRecieveInfo().getGiftNum();
            builder.append(tmp);
            setStrColor(builder, builder.toString().length() - tmp.length(), builder.toString().length(), R.color.white);

            tvContent.setText(builder);
        }
    }

    /**
     * 有人进入房间
     *
     * @param chatRoomMessage
     * @param tvContent
     */
    private void setMsgNotification(ChatRoomMessage chatRoomMessage, HtmlTextView tvContent) {

        IMChatRoomMember member = chatRoomMessage.getImChatRoomMember();
        if (member == null) return;
        String carName = member.getCar_name();
        String nick = member.getNick() + "";
        String content = "";
        SpannableStringBuilder builder = new SpannableStringBuilder(content);

        if (member.getVideo_room_exper_level() < 5 && member.getExperLevel() < 5){
            setNewLabel(builder, member.getIs_new_user_pic(), tvContent);
        }
        setLevel(builder,
                member.getVideo_room_exper_level_pic(),
                member.getExper_level_pic(),
                tvContent);


        if (!TextUtils.isEmpty(carName)) {
            String carString = "“" + carName + "”";

            builder.append(nick);
            setStrColor(builder, builder.toString().length() - nick.length(), builder.toString().length(), R.color.color_FFFFF600);
            builder.append("驾着");

            builder.append(carString);
            setStrColor(builder, builder.toString().length() - carString.length(), builder.toString().length(), R.color.color_FFFFF600);
            builder.append("来了");

            tvContent.setText(builder);
        } else {
            builder.append(nick);
            setStrColor(builder, 0, builder.toString().length(), R.color.white);
            builder.append(" 进入房间");
            setStrColor(builder, 0, builder.toString().length(), R.color.white);
            tvContent.setText(builder);
        }
    }

    /**
     * 房间内停留指定时间提示关注的消息
     *
     * @param msgContainer
     * @param faceContainer
     * @param attachment
     */
//    private void setTipMsgGoAttentionRoom(LinearLayout msgContainer, LinearLayout faceContainer, IMCustomAttachment attachment) {
//        if (attachment instanceof TipMsgGoAttentionAttachment) {
//            TipMsgGoAttentionAttachment attentionAttachment = (TipMsgGoAttentionAttachment) attachment;
//            String msg = "你已在房间停留" + ((TipMsgGoAttentionAttachment) attachment).getStayTime() + "分钟以上是否要关注房间";
//            msgContainer.setVisibility(GONE);
//            faceContainer.setVisibility(VISIBLE);
//            RelativeLayout container = new RelativeLayout(mContext);
//            LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(DisplayUtils.dip2px(mContext, 181), DisplayUtils.dip2px(mContext, 109));
//            container.setLayoutParams(ll);
//            container.setBackgroundResource(R.drawable.ic_tip_msg_go_attention_bg);
//
//            int padding = DisplayUtils.dip2px(mContext, 10);
//            HtmlTextView tvContent = new HtmlTextView(mContext);
//            RelativeLayout.LayoutParams rlContent = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//            tvContent.setPadding(padding, padding, padding, 0);
//            rlContent.addRule(RelativeLayout.CENTER_HORIZONTAL);
//            tvContent.setLayoutParams(rlContent);
//            tvContent.setTextSize(14);
//            tvContent.setTextColor(Color.WHITE);
//            tvContent.setMaxLines(2);
//            tvContent.setGravity(Gravity.CENTER_HORIZONTAL);
//            tvContent.setText(msg);
//
//            HtmlTextView tvAttention = new HtmlTextView(mContext);
//            RelativeLayout.LayoutParams rlAttention = new RelativeLayout.LayoutParams(DisplayUtils.dip2px(mContext, 71), DisplayUtils.dip2px(mContext, 26));
//            rlAttention.setMargins(0, 0, 0, padding);
//            rlAttention.addRule(RelativeLayout.CENTER_HORIZONTAL);
//            rlAttention.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//            tvAttention.setLayoutParams(rlAttention);
//            tvAttention.setBackgroundResource(R.drawable.selector_tip_msg_go_attention);
//            tvAttention.setSelected(attentionAttachment.isAttention());
//            tvAttention.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    changeAttention(attentionAttachment, tvAttention);
//                }
//            });
//            container.addView(tvContent);
//            container.addView(tvAttention);
//            faceContainer.addView(container);
//        }
//    }

    /**
     * 切换关注状态
     *
     * @param attentionAttachment
     * @param textView
     */
    private void changeAttention(TipMsgGoAttentionAttachment attentionAttachment, HtmlTextView textView) {
        textView.setEnabled(false);
        if (AvRoomDataManager.get().getRoomInfo() == null) {
            textView.setEnabled(true);
            return;
        }
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        long roomId = AvRoomDataManager.get().getRoomInfo().getRoomId();
        AttentionModel attentionModel = new AttentionModel();
        if (attentionAttachment.isAttention()) {
            attentionModel.deleteAttention(uid + "", roomId + "", new OkHttpManager.MyCallBack<Json>() {
                @Override
                public void onError(Exception e) {
                    textView.setEnabled(true);
                    SingleToastUtil.showToast(e.getMessage());
                }

                @Override
                public void onResponse(Json response) {
                    textView.setEnabled(true);
                    if (response != null) {
                        if (response.num("code") == 200) {
                            SingleToastUtil.showToast("取消关注成功");
                            attentionAttachment.setAttention(false);
                            if (AvRoomDataManager.get().getRoomInfo() != null) {
                                IMNetEaseManager.get().noticeRoomInfoUpdate(AvRoomDataManager.get().getRoomInfo());
                            }
                            textView.setSelected(false);
                        } else {
                            SingleToastUtil.showToast(response.str("message"));
                        }
                    } else {
                        SingleToastUtil.showToast("数据异常");
                    }
                }
            });
        } else {
            attentionModel.roomAttention(uid + "", roomId + "", new OkHttpManager.MyCallBack<Json>() {
                @Override
                public void onError(Exception e) {
                    textView.setEnabled(true);
                }

                @Override
                public void onResponse(Json response) {
                    textView.setEnabled(true);
                    if (response != null) {
                        if (response.num("code") == 200) {
                            SingleToastUtil.showToast("关注成功");
                            attentionAttachment.setAttention(true);
                            if (AvRoomDataManager.get().getRoomInfo() != null) {
                                IMNetEaseManager.get().noticeRoomInfoUpdate(AvRoomDataManager.get().getRoomInfo());
                            }
                            textView.setSelected(true);
                        } else {
                            SingleToastUtil.showToast(response.str("message"));
                        }
                    } else {
                        SingleToastUtil.showToast("数据异常");
                    }
                }
            });
        }
    }

    /**
     * 屏蔽礼物特效等系统消息
     *
     * @param second
     * @param roomQueueMsgAttachment
     * @param tvContent
     */
    private void setSystemMsg(int second, RoomQueueMsgAttachment roomQueueMsgAttachment, HtmlTextView tvContent) {
        String uid = roomQueueMsgAttachment.getUid();
        String handleNick = "";
        if (AvRoomDataManager.get().isRoomOwner(uid)) {
            handleNick = "房主";
        } else if (AvRoomDataManager.get().isRoomAdmin(uid)) {
            handleNick = "管理员";
        }
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list.add(RichTextUtil.getRichTextMap("系统消息: ", 0x8affffff));
        if (second == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN) {
            list.add(RichTextUtil.getRichTextMap(handleNick, 0xFFFFFFFF));
            list.add(RichTextUtil.getRichTextMap(" 已屏蔽该房间小礼物特效", 0x8affffff));
        } else if (second == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE) {
            list.add(RichTextUtil.getRichTextMap(handleNick, 0xFFFFFFFF));
            list.add(RichTextUtil.getRichTextMap(" 已开启该房间小礼物特效", 0x8affffff));
        } else if (second == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE) {
            list.add(RichTextUtil.getRichTextMap(handleNick, 0xFFFFFFFF));
            list.add(RichTextUtil.getRichTextMap(" 关闭了房间内聊天", 0x8affffff));
        } else if (second == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN) {
            list.add(RichTextUtil.getRichTextMap(handleNick, 0xFFFFFFFF));
            list.add(RichTextUtil.getRichTextMap(" 开启了房间内聊天", 0x8affffff));
        }
        tvContent.setText(RichTextUtil.getSpannableStringFromList(list));
    }

    /**
     * 设置爆出礼物消息样式
     *
     * @param tvContent
     * @param attachment
     */
    private void setBurstGift(HtmlTextView tvContent, BurstGiftAttachment attachment) {
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        list.add(RichTextUtil.getRichTextMap("神秘爆出~", 0xFFFFFFFF));
        list.add(RichTextUtil.getRichTextMap(attachment.getNick(), 0xFFFFD800));
        list.add(RichTextUtil.getRichTextMap(" 通过 ", 0xFFFFFFFF));
        list.add(RichTextUtil.getRichTextMap(attachment.getSendNick(), 0xFFFFD800));
        list.add(RichTextUtil.getRichTextMap(" 送礼意外获得 ", 0xFFFFFFFF));
        list.add(RichTextUtil.getRichTextMap("【" + attachment.getGiftName() + "】", 0xFFFFD800));
        list.add(RichTextUtil.getRichTextMap("X" + attachment.getGiftNum(), 0xFFFFFFFF));
        tvContent.setText(RichTextUtil.getSpannableStringFromList(list));
    }

    /***
     * 砸蛋
     * @param attachment
     * @param tvContent
     */
    private void setLotteryInfo(LotteryBoxAttachment attachment, HtmlTextView tvContent) {
        String nick = attachment.getNick();

        String temp = "哇塞，恭喜 ";
        SpannableStringBuilder builder = new SpannableStringBuilder(temp);
        setStrColor(builder, 0, temp.length(), R.color.white);

        builder.append(nick);
        setStrColor(builder, builder.toString().length() - nick.length(), builder.toString().length(), R.color.color_FFFFF600);

        temp = " 在种豆中，收获了";
        builder.append(temp);
        setStrColor(builder, builder.toString().length() - temp.length(), builder.toString().length(), R.color.white);

        temp = attachment.getGiftName() + " (" + attachment.getGoldPrice() + ")";
        builder.append(temp);
        setStrColor(builder, builder.toString().length() - temp.length(), builder.toString().length(), R.color.color_FFFFF600);

        temp = " X " + attachment.getCount();
        builder.append(temp);
        setStrColor(builder, builder.toString().length() - temp.length(), builder.toString().length(), R.color.white);

        tvContent.setText(builder);

        if (attachment.isFull()) {
            tvContent.setBackgroundResource(R.drawable.ic_msg_bg_full_red);
        } else {
            if (attachment.getGoldPrice() >= 1000) {
                tvContent.setBackgroundResource(R.drawable.ic_msg_bg_full_green);
            }
        }
    }

    /**
     * xxx分享了房间消息
     *
     * @param tvContent
     * @param roomTipAttachment
     */

    private void setMsgRoomTip(HtmlTextView tvContent, RoomTipAttachment roomTipAttachment, IMChatRoomMember member) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (roomTipAttachment == null || member == null) {
            return;
        }

        if (member.getVideo_room_exper_level() < 5 && member.getExperLevel() < 5){
            setNewLabel(builder, member.getIs_new_user_pic(), tvContent);
        }
        setLevel(builder,
                member.getVideo_room_exper_level_pic(),
                member.getExper_level_pic(),
                tvContent);

        String tmp;
        if (StringUtils.isEmpty(member.getNick())) {
            return;
        }

        tmp = member.getNick();
        builder.append(tmp);
        setStrColor(builder, builder.toString().length() - tmp.length(), builder.toString().length(), R.color.color_FFFFF600);
        if (roomTipAttachment.getSecond() == CUSTOM_MSG_SECOND_ROOM_TIP_SHARE_ROOM) {
            tmp = " 分享了房间";
        } else if (roomTipAttachment.getSecond() == IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_ATTENTION_ROOM_OWNER) {
            tmp = " 关注了房主";
        }
        builder.append(tmp);
        setStrColor(builder, builder.toString().length() - tmp.length(), builder.toString().length(), R.color.white);
        tvContent.setText(builder);
    }

    private void setMsgFace(HtmlTextView tvContent, ChatRoomMessage chatRoomMessage) {
        FaceAttachment faceAttachment = (FaceAttachment) chatRoomMessage.getAttachment();
        String senderNick = "";
        Log.i(TAG, "setMsgFace: " + faceAttachment.toString());

        IMChatRoomMember member = chatRoomMessage.getImChatRoomMember();
        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (member != null) {
            if (member.getVideo_room_exper_level() < 5 && member.getExperLevel() < 5){
                setNewLabel(builder, member.getIs_new_user_pic(), tvContent);
            }
            setLevel(builder,
                    member.getVideo_room_exper_level_pic(),
                    member.getExper_level_pic(),
                    tvContent);
        }

        List<FaceReceiveInfo> faceReceiveInfos = faceAttachment.getFaceReceiveInfos();
        for (int i = 0; i < faceReceiveInfos.size(); i++) {
            FaceReceiveInfo faceReceiveInfo = faceReceiveInfos.get(i);
            FaceInfo faceInfo = CoreManager.getCore(IFaceCore.class).findFaceInfoById(faceReceiveInfo.getFaceId());
            if (faceReceiveInfo.getResultIndexes().size() <= 0 ||
                    faceInfo == null) {
                continue;
            }
            for (Integer index : faceReceiveInfo.getResultIndexes()) {
                if (AvRoomDataManager.get().isOwner(faceReceiveInfo.getUid())) {
                    senderNick = "我：";
                } else {
                    senderNick = faceReceiveInfo.getNick() + "：";
                }
                builder.append(senderNick);
                setStrColor(builder, builder.toString().length() - senderNick.length(), builder.toString().length(), R.color.color_FFFFF600);
                setImageLabel(builder, faceInfo.getFacePath(index), tvContent);
            }
        }

        tvContent.setText(builder);
    }

    /**
     * html 消息
     *
     * @param attachment
     * @param tvContent
     */
    private void setHtmlMsg(RoomHtmlTextAttachment attachment, HtmlTextView tvContent) {
        if (attachment == null) {
            return;
        }
        tvContent.setHtmlText(attachment.getHtml());
    }

    /**
     * 豆币全服消息
     *
     * @param attachment
     * @param tvContent
     */
    private void setCoinAllMsg(RoomHtmlTextAttachment attachment, HtmlTextView tvContent) {
        if (attachment == null) {
            return;
        }
        tvContent.setHtmlText(attachment.getHtml());
    }

    @Override
    public void onClick(View v) {
        account = null;
        ChatRoomMessage chatRoomMessage = (ChatRoomMessage) v.getTag();
        if (chatRoomMessage == null) {
            return;
        }
        if (!IMReportRoute.ChatRoomTip.equalsIgnoreCase(chatRoomMessage.getRoute())) {
            if (IMReportRoute.sendMessageReport.equalsIgnoreCase(chatRoomMessage.getRoute())) {
                IMCustomAttachment attachment = chatRoomMessage.getAttachment();
                if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_ROOM_TIP) {
                    account = ((RoomTipAttachment) attachment).getUid() + "";
                } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_ROOM_RED_PACKAGE) {
                    account = ((PublicChatRoomAttachment) attachment).getUid() + "";
                } else if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT ||
                        attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT
                ) {
                    account = ((GiftAttachment) attachment).getGiftRecieveInfo().getUid() + "";
                }
            } else {
                if (chatRoomMessage.getImChatRoomMember() != null) {
                    account = chatRoomMessage.getImChatRoomMember().getAccount();
                }
            }
            // 房主在web端发送信息 是拿不到account的
            if (TextUtils.isEmpty(account) || account.equals("0") || onMsgContentClickListener == null) {
                return;
            } else {
                onMsgContentClickListener.onClick(Long.valueOf(account));
            }
        }
    }

    private MessageView.OnMsgContentClickListener onMsgContentClickListener;

    public void setOnMsgContentClickListener(MessageView.OnMsgContentClickListener onMsgContentClickListener) {
        this.onMsgContentClickListener = onMsgContentClickListener;
    }

    @Override
    public void addData(@NonNull Collection<? extends ChatRoomMessage> newData) {
        //原来没数据，就代表本次add导致所有数据产生了变更
        boolean isAllDataChanged = mData.size() == 0;
        mData.addAll(newData);
        try {
            int size = mData.size();
            //超出最大消息数限制
            if (size > MESSAGE_COUNT_LOCAL_LIMIT) {
                //移除最前面的1/3消息
                List<ChatRoomMessage> removeData = new ArrayList<>(mData.subList(0, MESSAGE_COUNT_LOCAL_LIMIT / 3));//移除时new，避免ConcurrentModificationException
                mData.removeAll(removeData);
                //移除了前面的数据，那么所有的数据也是产生了变更
                isAllDataChanged = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.w("messageView", " add data limit fail = " + e.getMessage());
        }
        if (isAllDataChanged) {
            notifyDataSetChanged();
        } else {
            notifyItemRangeInserted(mData.size() - newData.size() + getHeaderLayoutCount(), newData.size());
        }
    }
}
