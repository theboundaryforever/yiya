package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.juxiao.library_utils.DisplayUtils;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Zhangsongzhou
 * @date 2019/7/10
 */
public class BarrageView extends FrameLayout {

    private static final String TAG = "BarrageView";


    private static final int WHAT_SHOW_ITEM = 1;


    private static final long MAX_TIME = 3500;
    private static final int MAX_ITEM_NUM = 2; //最大弹道
    private BallisticInfo[] mState = new BallisticInfo[MAX_ITEM_NUM]; //弹道状态
    private static int[] TOP_MARGINS = new int[MAX_ITEM_NUM];

    private Queue<BarrageInfo> mQueueInfos;


    private Context mContext;
    private MHandler mHandler;

    public static final int STATE_RESUME = 0;
    public static final int STATE_STOP = 1;
    private int mLiveState = STATE_RESUME;

    public BarrageView(Context context) {
        super(context);
        init(context);
    }

    public BarrageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        mContext = context;

        TOP_MARGINS[0] = getResources().getDimensionPixelOffset(R.dimen.barrage_top_margin_176);
        TOP_MARGINS[1] = getResources().getDimensionPixelOffset(R.dimen.barrage_top_margin_261);

        for (int i = 0; i < MAX_ITEM_NUM; i++) {
            BallisticInfo info = new BallisticInfo();
            info.setState(false);
            info.setTime(System.currentTimeMillis());
            info.setTopMargin(TOP_MARGINS[i]);
            mState[i] = info;
        }
        mQueueInfos = new LinkedList<>();
        mHandler = new MHandler(this);


    }

    /**
     * @param value
     */
    public void setLiveState(int value) {
        mLiveState = value;
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);

    }

    public void addItem(BarrageInfo info) {
        synchronized (mState) {
            mQueueInfos.add(info);
            for (int i = 0; i < MAX_ITEM_NUM; i++) {
                if (!mState[i].isState()) {
                    Message message = new Message();
                    message.what = WHAT_SHOW_ITEM;
                    message.arg1 = i;
                    mHandler.sendMessage(message);
                    break;
                }
            }
        }

    }

    private void showItem(final int index, final BarrageInfo info) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        synchronized (mState) {
            layoutParams.topMargin = mState[index].getTopMargin();
        }
        final FrameLayout itemView = (FrameLayout) LayoutInflater.from(mContext).inflate(R.layout.view_item_barrage, null);
        itemView.setLayoutParams(layoutParams);

        TextView nameTv = itemView.findViewById(R.id.view_name_tv);
        String nameStr = info.getName();
        Rect nameRect = new Rect();
        nameTv.getPaint().getTextBounds(nameStr, 0, nameStr.length(), nameRect);
        int nameWidth = nameRect.width() + getResources().getDimensionPixelOffset(R.dimen.barrage_offset_nick);
        nameTv.setText(nameStr);

        ImageLoadUtils.loadAvatar(mContext, info.getAvatar(), itemView.findViewById(R.id.view_circle_avatar));

        TextView messageTv = itemView.findViewById(R.id.view_message);
        String tmpMessage = info.getMessage();
        Rect rect = new Rect();
        messageTv.getPaint().getTextBounds(tmpMessage, 0, tmpMessage.length(), rect);
        int viewWidth = rect.width() + getResources().getDimensionPixelOffset(R.dimen.barrage_offset);
        messageTv.setText(info.getMessage());

        //取最长的
        if (nameWidth > viewWidth) {
            viewWidth = nameWidth;
        }


        int width = DisplayUtils.getScreenWidth(mContext); //控件宽度(一般就是屏幕宽度)

        int offsetTime = Math.round(viewWidth / (width / 3500f));
        LogUtil.d(TAG, "offsetTime: " + offsetTime);

        //如果当前处于显示状态
        if (mLiveState == STATE_RESUME) {
            Animation animation = new TranslateAnimation(width, -viewWidth, 0, 0);
            animation.setDuration(MAX_TIME + offsetTime);
            animation.setInterpolator(new LinearInterpolator());
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    LogUtil.d(TAG, "onAnimationStart" + offsetTime);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    removeView(itemView);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            itemView.startAnimation(animation);
            addView(itemView);
        }
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                synchronized (mState) {
                    mState[index].setState(false);
                }
                LogUtil.d(TAG, "postDelayed: " + offsetTime);
                Message message = new Message();
                message.what = WHAT_SHOW_ITEM;
                message.arg1 = index;
                mHandler.sendMessage(message);
            }
        }, 200 + offsetTime);
    }


    private static class MHandler extends Handler {

        private WeakReference<BarrageView> mWeakReference;

        private MHandler(BarrageView view) {
            mWeakReference = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == WHAT_SHOW_ITEM) {
                BarrageView barrageView = mWeakReference.get();
                if (barrageView != null && barrageView.mQueueInfos != null && barrageView.mState != null) {
                    BarrageInfo info = barrageView.mQueueInfos.poll();
                    int index = msg.arg1;

                    if (info != null) {
                        if (!barrageView.mState[index].isState()) {
                            barrageView.mState[index].setState(true);
                            barrageView.showItem(index, info);
                        }
                    } else { //如果是为空，说明队列已经是没有了
//                        barrageView.resetState(false);
                    }
                }

            }
        }
    }

    /**
     * 弹幕
     */
    public static class BarrageInfo {
        private String name;
        private String message;
        private String avatar;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }

    /**
     * 弹道
     */
    private static class BallisticInfo {
        private long time;
        private boolean state;
        private int topMargin;

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public boolean isState() {
            return state;
        }

        public int getTopMargin() {
            return topMargin;
        }

        public void setTopMargin(int topMargin) {
            this.topMargin = topMargin;
        }

        public void setState(boolean state) {
            this.state = state;
        }
    }


}
