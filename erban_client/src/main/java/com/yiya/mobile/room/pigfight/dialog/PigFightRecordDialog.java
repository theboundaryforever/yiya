package com.yiya.mobile.room.pigfight.dialog;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.room.pigfight.adapter.PigFightRecordAdapter;
import com.yiya.mobile.ui.widget.itemdecotion.DividerItemDecoration;
import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.room.bean.PigRecordInfo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/26.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PigFightRecordDialog extends BaseTransparentDialogFragment {

    @BindView(R.id.rv_top)
    RecyclerView rvTop;
    @BindView(R.id.rv_bottom)
    RecyclerView rvBottom;

    private static final String KEY = "list";

    public static PigFightRecordDialog newInstance(List<PigRecordInfo> list) {
        PigFightRecordDialog dialog = new PigFightRecordDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY, (ArrayList<? extends Parcelable>) list);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_pig_fight_record;
    }

    protected int getWindowAnimationStyle() {
        return R.style.WindowBottomAnimationStyle;
    }

    protected int getWindowGravity() {
        return Gravity.BOTTOM;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() == null) {
            return;
        }
        List<PigRecordInfo> list = getArguments().getParcelableArrayList(KEY);
        if (ListUtils.isListEmpty(list)) {
            return;
        }

        List<PigRecordInfo> listTop = new LinkedList<>();
        List<PigRecordInfo> listBottom = new LinkedList<>();
        for (int i = 0; i < list.size(); i++) {
            if (i % 2 == 0) {
                //偶放下
                listBottom.add(list.get(i));
            } else {
                //奇放上
                listTop.add(list.get(i));
            }
        }

        setRecordList(rvTop, listTop);
        setRecordList(rvBottom, listBottom);
    }

    private void setRecordList(RecyclerView rv, List<PigRecordInfo> list) {
        rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rv.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.HORIZONTAL, DisplayUtils.dip2px(getContext(), 25), R.color.transparent));
        PigFightRecordAdapter adapter = new PigFightRecordAdapter();
        adapter.setNewData(list);
        rv.setAdapter(adapter);
    }

    @OnClick(R.id.iv_close)
    public void close() {
        dismiss();
    }
}
