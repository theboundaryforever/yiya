package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.bean.RoomPlantCucumberInfo;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/4.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantCucumberView extends ConstraintLayout {

    @BindView(R.id.tv_gold)
    TextView tvGold;

    public PlantCucumberView(Context context) {
        this(context, null);
    }

    public PlantCucumberView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlantCucumberView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.layout_plant_cucumber, this);
        ButterKnife.bind(this);
    }

    public void setOnCallback(Callback callback) {
        setOnClickListener(v -> {
            if (callback != null) {
                callback.onClick();
            }
        });
    }

    public void setData(RoomPlantCucumberInfo roomPlantCucumberInfo) {
        if (roomPlantCucumberInfo == null) {
            return;
        }
        tvGold.setText(String.valueOf(roomPlantCucumberInfo.getGoldPool()));
    }

    public interface Callback {
        void onClick();
    }
}
