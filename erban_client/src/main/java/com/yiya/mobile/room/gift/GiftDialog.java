package com.yiya.mobile.room.gift;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gcssloop.widget.PagerGridLayoutManager;
import com.gcssloop.widget.PagerGridSnapHelper;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.room.gift.adapter.GiftAdapter;
import com.yiya.mobile.room.gift.adapter.GiftAvatarAdapter;
import com.yiya.mobile.room.gift.adapter.GiftAvatarAdapterNew;
import com.yiya.mobile.room.gift.widget.PageIndicatorView;
import com.yiya.mobile.ui.me.wallet.activity.ChargeActivity;
import com.yiya.mobile.ui.widget.marqueeview.Utils;
import com.juxiao.library_ui.widget.DrawableTextView;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.zyyoona7.lib.EasyPopup;
import com.zyyoona7.lib.HorizontalGravity;
import com.zyyoona7.lib.VerticalGravity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GiftDialog extends BottomSheetDialog implements View.OnClickListener, GiftAdapter.OnItemClickListener,
        GiftAvatarAdapter.OnItemSelectedListener, PagerGridLayoutManager.PageListener,
        GiftAvatarAdapterNew.OnCancelAllMicSelectListener, RadioGroup.OnCheckedChangeListener {

    private static final String TAG = "giftdialog";
    public static final int NOTE_GIFT_ID = 1;//音符礼物的id
    public static final int ROWS = 2;
    public static final int COLUMNS = 4;
    private Context context;
    private RecyclerView avatarList;
    private GiftAvatarAdapterNew avatarListAdapter;
    private GiftAdapter adapter;
    private GiftInfo current;
    private List<GiftInfo> giftInfoList;
    private OnGiftDialogBtnClickListener giftDialogBtnClickListener;
    private TextView goldText;
    private EasyPopup giftNumberEasyPopup;
    private TextView giftNumberText;
    private int giftNumber = 1;
    private long uid;
    private List<MicMemberInfo> micMemberInfos;
    private MicMemberInfo defaultMicMemberInfo;
    private View giftNumLayout;
    private PageIndicatorView giftIndicator;
    private RelativeLayout allMicHead;
    private TextView cbAllMic;

    private boolean singlePeople;
    private int currentP = 0;//默认礼物
    private LinearLayout llGiftEmpty;
    private RelativeLayout rlGift;
    private List<GiftInfo> mysteryGiftInfo;
    private RelativeLayout gift_dialog_header;

    @BindView(R.id.gift_dialog_amount_layout)
    RelativeLayout gift_dialog_amount_layout;
    @BindView(R.id.llGiftTimeTick)
    LinearLayout llGiftTimeTick;
    @BindView(R.id.tvTimeTick)
    TextView tvTimeTick;

    private long comboId = 0;
    private long comboCount = 1;
    private long comboGiftId = 0;
    private long giftIdCur = 0;

    private DrawableTextView userInfoText;

    public void setSinglePeople(boolean singlePeople) {
        this.singlePeople = singlePeople;
    }

    public GiftDialog(FragmentActivity context, MicMemberInfo memberInfo) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;
        this.defaultMicMemberInfo = memberInfo;
        List<MicMemberInfo> micMemberInfos = new ArrayList<>();
        micMemberInfos.add(memberInfo);

        this.micMemberInfos = micMemberInfos;
    }

    public void setGiftDialogBtnClickListener(OnGiftDialogBtnClickListener giftDialogBtnClickListener) {
        this.giftDialogBtnClickListener = giftDialogBtnClickListener;
    }

    public GiftDialog(Context context, long uid) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;
        this.uid = uid;
    }

    public GiftDialog(Context context, SparseArray<RoomQueueInfo> mMicQueueMemberMap, long uid) {
        super(context, R.style.GiftBottomSheetDialog);
        this.context = context;

        List<MicMemberInfo> micMemberInfos = new ArrayList<>();
        if (!checkHasOwner(mMicQueueMemberMap)) {
            UserInfo roomOwner = AvRoomDataManager.get().getRoomOwnerUserInfo();
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            micMemberInfo.setRoomOwnner(true);
            micMemberInfo.setMicPosition(-1);
            if (roomOwner != null) {
                micMemberInfo.setNick(roomOwner.getNick());
                micMemberInfo.setAvatar(roomOwner.getAvatar());
                micMemberInfo.setUid(roomOwner.getUid());
            } else {
                IMChatRoomMember roomMember = AvRoomDataManager.get().getRoomOwnerDefaultMemberInfo();
                micMemberInfo.setNick(roomMember.getNick());
                micMemberInfo.setAvatar(roomMember.getAvatar());
                micMemberInfo.setUid(Long.valueOf(roomMember.getAccount()));
            }
            micMemberInfos.add(micMemberInfo);
        }
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            IMChatRoomMember mChatRoomMember = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i)).mChatRoomMember;
            if (mChatRoomMember == null) {
                continue;
            }
            // 合法判断
            String account = mChatRoomMember.getAccount();
            LogUtils.d("checkHasOwner", account + "   dd");
            if (TextUtils.isEmpty(account) ||
                    TextUtils.isEmpty(mChatRoomMember.getNick()) ||
                    TextUtils.isEmpty(mChatRoomMember.getAvatar())) {
                continue;
            }
            // 排除自己\
            if (AvRoomDataManager.get().isOwner(account)) {
                continue;
            }
            // 设置默认人员
            if (uid != 0 && String.valueOf(uid).equals(account)) {
                this.defaultMicMemberInfo = micMemberInfo;
            }
            // 设置房主
            if (AvRoomDataManager.get().isRoomOwner(account)) {
                micMemberInfo.setRoomOwnner(true);
            }
            micMemberInfo.setNick(mChatRoomMember.getNick());
            micMemberInfo.setAvatar(mChatRoomMember.getAvatar());
            micMemberInfo.setMicPosition(mMicQueueMemberMap.keyAt(i));
            micMemberInfo.setUid(JavaUtil.str2long(account));
            micMemberInfos.add(micMemberInfo);
        }
        this.micMemberInfos = micMemberInfos;
    }

    private boolean checkHasOwner(SparseArray<RoomQueueInfo> mMicQueueMemberMap) {
        if (AvRoomDataManager.get().getRoomInfo() == null) {
            return true;
        }
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i));
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                if ((AvRoomDataManager.get().getRoomInfo().getUid() + "").equals(account)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreManager.addClient(this);
        setCanceledOnTouchOutside(true);
        setContentView(R.layout.dialog_bottom_gift);
        ButterKnife.bind(this);

        init(findViewById(R.id.rl_dialog_bottom_gift));

        FrameLayout bottomSheet = findViewById(android.support.design.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(false);
            BottomSheetBehavior.from(bottomSheet).setPeekHeight((int) context.getResources()
                    .getDimension(R.dimen.dialog_gift_height) + (Utils.hasSoftKeys(context) ? Utils.getNavigationBarHeight(context) : 0));
        }
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display d = windowManager.getDefaultDisplay();
        DisplayMetrics realDisplayMetrics = new DisplayMetrics();
        d.getRealMetrics(realDisplayMetrics);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
    }

    private void init(View root) {
        rlGift = findViewById(R.id.rl_gift_container);
        llGiftEmpty = findViewById(R.id.ll_gift_empty);
        findViewById(R.id.ll_btn_recharge).setOnClickListener(this);
        findViewById(R.id.btn_send).setOnClickListener(this);
        findViewById(R.id.gift_number_layout).setOnClickListener(this);
        findViewById(R.id.gift_dialog_to_man_layout).setOnClickListener(this);

        RadioGroup rgIndex = root.findViewById(R.id.rg_gift_indicator);
        rgIndex.setOnCheckedChangeListener(this);
        rgIndex.check(R.id.rb_gift_tab);

        giftInfoList = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(2);
        mysteryGiftInfo = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(3);
        giftNumLayout = findViewById(R.id.gift_number_layout);
        RecyclerView gridView = findViewById(R.id.gridView);
        PagerGridSnapHelper pageSnapHelper = new PagerGridSnapHelper();
        pageSnapHelper.attachToRecyclerView(gridView);
        goldText = findViewById(R.id.text_gold);
        giftNumberText = findViewById(R.id.gift_number_text);
        TextView giftManText = findViewById(R.id.gift_man_text);
        avatarList = findViewById(R.id.avatar_list);
        userInfoText = findViewById(R.id.gift_dialog_info_text);
        userInfoText.setOnClickListener(this);
        adapter = new GiftAdapter(getContext());
        gridView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
        allMicHead = findViewById(R.id.rl_all_mic);
        cbAllMic = findViewById(R.id.cb_gift_all);
        cbAllMic.setOnClickListener(this);
        allMicHead.setOnClickListener(this);
        if (giftInfoList != null && giftInfoList.size() > 0) {
            adapter.setGiftInfoList(giftInfoList);
            adapter.notifyDataSetChanged();
            PagerGridLayoutManager pagerGridLayoutManager = new PagerGridLayoutManager(ROWS, COLUMNS, PagerGridLayoutManager.HORIZONTAL);
            pagerGridLayoutManager.setPageListener(this);
            gridView.setLayoutManager(pagerGridLayoutManager);
            giftIndicator.initIndicator((int) Math.ceil((float) giftInfoList.size() / (ROWS * COLUMNS)));
            current = giftInfoList.get(0);
        }
        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (walletInfo != null) {
            goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }

        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        CoreManager.getCore(IPayCore.class).getWalletInfo(uid);
        initEasyPop();
        if (singlePeople) {
            userInfoText.setVisibility(View.INVISIBLE);
        }
        if (this.uid > 0) {
            if (micMemberInfos == null) {
                micMemberInfos = new ArrayList<>();
            }
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(this.uid, false);
            if (userInfo == null) {
                userInfo = new UserInfo();
            }
            defaultMicMemberInfo = new MicMemberInfo();
            defaultMicMemberInfo.setAvatar(userInfo.getAvatar());
            defaultMicMemberInfo.setNick(userInfo.getNick());
            defaultMicMemberInfo.setUid(this.uid);
            micMemberInfos.add(defaultMicMemberInfo);
        }

        if (micMemberInfos != null && micMemberInfos.size() > 0) {
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            avatarList.setLayoutManager(mLayoutManager);
            avatarList.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    outRect.left = 10;
                    outRect.right = 10;
                }
            });
            avatarListAdapter = new GiftAvatarAdapterNew();
            avatarList.setAdapter(avatarListAdapter);
            avatarListAdapter.setOnCancelAllMicSelectListener(this);
            if (defaultMicMemberInfo != null) {
                int position = micMemberInfos.indexOf(defaultMicMemberInfo);
                if (position >= 0) {
                    avatarListAdapter.setSelectCount(1);
                    micMemberInfos.get(position).setSelect(true);
                    giftManText.setText(micMemberInfos.get(position).getNick());
                } else {
                    Log.e(TAG, "init: default mic member info not in mic member info list");
                }
            } else {
                avatarListAdapter.setSelectCount(1);
                micMemberInfos.get(0).setSelect(true);
                giftManText.setText(micMemberInfos.get(0).getNick());
            }
            avatarListAdapter.setNewData(micMemberInfos);
        } else {
            userInfoText.setVisibility(View.GONE);
        }


        gift_dialog_header = root.findViewById(R.id.gift_dialog_header);
        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().getRoomInfo();
        if (AvRoomDataManager.get().getRoomInfo() == null) {
            return;
        } else {
            if (mCurrentRoomInfo.getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE)
                gift_dialog_header.setVisibility(View.GONE);
        }
    }

    private void initEasyPop() {
        giftNumberEasyPopup = new EasyPopup(getContext())
                .setContentView(R.layout.dialog_gift_number)
                .setFocusAndOutsideEnable(true)
                .createPopup();

        giftNumberEasyPopup.getView(R.id.number_1).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_10).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_99).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_66).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_188).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_520).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_1314).setOnClickListener(this);
    }

    private void sendGift() {
        if (adapter == null) {
            return;
        }
        current = adapter.getIndexGift();
        if (current == null) {
            SingleToastUtil.showToast("请选择您需要赠送的礼物哦！");
            return;
        }
        if (current.getGiftType() == 3 && current.getUserGiftPurseNum() < giftNumber) {//神秘礼物
            SingleToastUtil.showToast("您的神秘礼物数量不够哦！");
            return;
        }
        if (giftDialogBtnClickListener != null) {//gift_dialog_amount_layout
            if (uid > 0) {
                giftDialogBtnClickListener.onSendGiftBtnClick(current, null, uid, giftNumber);
            } else if (avatarListAdapter != null && !ListUtils.isListEmpty(avatarListAdapter.getData())) {
                //其他人
                if (allMicHead != null && allMicHead.getVisibility() == View.VISIBLE && cbAllMic.isSelected()) {
                    //勾选全麦
                    giftDialogBtnClickListener.onSendGiftBtnClick(current, micMemberInfos, 0, giftNumber);
                } else {
                    //未勾选全麦
                    boolean noSend = true;
                    for (int i = 0; i < micMemberInfos.size(); i++) {
                        if (micMemberInfos.get(i).isSelect()) {
                            noSend = false;
                            giftIdCur = current.getGiftId(); // 用于判断与上一次发送的giftId是否为同一个
                            if (AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE) {
                                startCount();
                                giftDialogBtnClickListener.onSendGiftBtnClick(current, null, micMemberInfos.get(i).getUid(), giftNumber, comboId, comboCount);
                            } else {
                                giftDialogBtnClickListener.onSendGiftBtnClick(current, null, micMemberInfos.get(i).getUid(), giftNumber);
                            }
                        }
                    }

                    if (noSend) {
                        if (context instanceof BaseActivity) {
                            ((BaseActivity) context).toast("暂无成员在麦上");
                        } else if (context instanceof BaseMvpActivity) {
                            ((BaseMvpActivity) context).toast("暂无成员在麦上");
                        }
                        dismiss();
                    }
                }
            } else {
                if (context instanceof BaseActivity) {
                    ((BaseActivity) context).toast("暂无成员在麦上");
                } else if (context instanceof BaseMvpActivity) {
                    ((BaseMvpActivity) context).toast("暂无成员在麦上");
                }
                dismiss();
            }
        }
    }

    @Override
    @OnClick({R.id.llGiftTimeTick})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_btn_recharge:
//                WalletActivity.start(getContext());
                ChargeActivity.start(getContext());
                dismiss();
                break;
            case R.id.btn_send:
                sendGift();
                break;
            case R.id.llGiftTimeTick:
                sendGift();
                break;
            case R.id.number_1:
                updateNumber(1);
                break;
            case R.id.number_10:
                updateNumber(10);
                break;
            case R.id.number_99:
                updateNumber(99);
                break;
            case R.id.number_66:
                updateNumber(66);
                break;
            case R.id.number_188:
                updateNumber(188);
                break;
            case R.id.number_520:
                updateNumber(520);
                break;
            case R.id.number_1314:
                updateNumber(1314);
                break;
            case R.id.gift_number_layout:
                showGiftNumberEasyPopup();
                break;
            case R.id.gift_dialog_to_man_layout:
                showGiftAvatarListEasyPopup();
                break;
            case R.id.gift_dialog_info_text:
                UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomGiftUserInfo());
                displayUserInfo();
                dismiss();
                break;
            case R.id.rl_all_mic:
                allMicHead.setEnabled(false);
                boolean shouldSelect = !cbAllMic.isSelected();
                cbAllMic.setSelected(shouldSelect);
                cbAllMic.setText(!shouldSelect ? "全" : "");
                if (avatarListAdapter != null && !ListUtils.isListEmpty(avatarListAdapter.getData())) {
                    for (int i = 0; i < avatarListAdapter.getData().size(); i++) {
                        avatarListAdapter.getData().get(i).setSelect(shouldSelect);
                        avatarListAdapter.notifyDataSetChanged();
                    }
                    avatarListAdapter.setAllSelect(shouldSelect);
                }
                allMicHead.setEnabled(true);
                break;
            default:
        }
    }

    private void updateNumber(int number) {
        giftNumber = number;
        giftNumberText.setText(giftNumber + "");
        giftNumberEasyPopup.dismiss();
    }

    private void displayUserInfo() {
        long displayUid;
        if (uid > 0) {
            displayUid = uid;
        } else {
            if (defaultMicMemberInfo == null) {
                displayUid = micMemberInfos.get(0).getUid();
            } else {
                displayUid = defaultMicMemberInfo.getUid();
            }
        }

        if (giftDialogBtnClickListener != null) {
            giftDialogBtnClickListener.onUserInfoClick(displayUid);
        }
    }

    private void showGiftAvatarListEasyPopup() {
        avatarList.setVisibility(View.VISIBLE);
        if (allMicHead != null) {
            if (singlePeople) {
                allMicHead.setVisibility(View.GONE);
            } else {
                if (avatarListAdapter != null && !ListUtils.isListEmpty(avatarListAdapter.getData())) {
                    if (avatarListAdapter.getData().size() == 1) {
                        allMicHead.setVisibility(View.GONE);
                    }
                } else {
                    allMicHead.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void showGiftNumberEasyPopup() {
        giftNumberEasyPopup.showAtAnchorView(giftNumLayout, VerticalGravity.ABOVE, HorizontalGravity.CENTER, 50, 10);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
    }


    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onGetWalletInfo(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void refreshFreeGift() {
        if (adapter == null)
            return;
        if (currentP == 0) {
            giftInfoList = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(2);
            adapter.setGiftInfoList(giftInfoList);
        } else if (currentP == 1) {//更新礼物数量
            List<GiftInfo> gifts = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(3);
            if (ListUtils.isListEmpty(gifts)) {
                if (llGiftEmpty.getVisibility() == View.GONE) {
                    llGiftEmpty.setVisibility(View.VISIBLE);
                }
                if (rlGift.getVisibility() == View.VISIBLE) {
                    rlGift.setVisibility(View.GONE);
                }
                adapter.setIndex(0);
            } else {
                if (mysteryGiftInfo != null && gifts.size() < mysteryGiftInfo.size()) {
                    adapter.setIndex(0);
                }
            }
            mysteryGiftInfo = gifts;
            adapter.setGiftInfoList(mysteryGiftInfo);
        }
        adapter.notifyDataSetChanged();
    }


    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftPastDue() {
        ((BaseActivity) getContext()).toast("该礼物已过期");
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftMysteryNotEnough() {
        SingleToastUtil.showToast("您的神秘礼物数量不够哦！");
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendGiftFail(int code, String message) {
        SingleToastUtil.showToast(message);
    }

    @Override
    public void onItemSelected(int position) {

    }

    @Override
    public void onChange(boolean isAllMic, int selectCount) {
        if (isAllMic) {
            cbAllMic.setSelected(true);
            cbAllMic.setText("");
        } else {
            cbAllMic.setSelected(false);
            cbAllMic.setText("全");
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        if (adapter == null) {
            return;
        }
        adapter.setIndex(position);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPageSizeChanged(int pageSize) {
        Log.d(TAG, "onPageSizeChanged() called with: pageSize = [" + pageSize + "]");
    }

    @Override
    public void onPageSelect(int pageIndex) {
        giftIndicator.setSelectedPage(pageIndex);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_gift_tab:
                if (currentP == 0)
                    return;
                if (adapter == null)
                    return;
                currentP = 0;
                if (ListUtils.isListEmpty(giftInfoList)) {
                    if (llGiftEmpty.getVisibility() == View.GONE)
                        llGiftEmpty.setVisibility(View.VISIBLE);
                    if (rlGift.getVisibility() == View.VISIBLE) {
                        rlGift.setVisibility(View.GONE);
                    }
                } else {
                    if (rlGift.getVisibility() == View.GONE) {
                        rlGift.setVisibility(View.VISIBLE);
                    }
                    if (llGiftEmpty.getVisibility() == View.VISIBLE)
                        llGiftEmpty.setVisibility(View.GONE);
                }
                adapter.setIndex(0);
                adapter.setGiftInfoList(giftInfoList);
                adapter.notifyDataSetChanged();
                break;
            case R.id.rb_gift_pack_tab:
                if (currentP == 1)
                    return;
                if (adapter == null)
                    return;
                currentP = 1;
                if (ListUtils.isListEmpty(mysteryGiftInfo)) {
                    if (llGiftEmpty.getVisibility() == View.GONE)
                        llGiftEmpty.setVisibility(View.VISIBLE);
                    if (rlGift.getVisibility() == View.VISIBLE) {
                        rlGift.setVisibility(View.GONE);
                    }

                } else {
                    if (rlGift.getVisibility() == View.GONE) {
                        rlGift.setVisibility(View.VISIBLE);
                    }
                    if (llGiftEmpty.getVisibility() == View.VISIBLE)
                        llGiftEmpty.setVisibility(View.GONE);
                }
                adapter.setIndex(0);
                adapter.setGiftInfoList(mysteryGiftInfo);
                adapter.notifyDataSetChanged();
                break;
        }
    }

    public interface OnGiftDialogBtnClickListener {
        void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long uid, int number);

        void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long uid, int number, long comboId, long comboCount);

        void onUserInfoClick(long uid);
    }

    private void startCount() {
        llGiftTimeTick.setVisibility(View.VISIBLE);
        gift_dialog_amount_layout.setVisibility(View.INVISIBLE);
        timer.cancel();
        timer.start();

        if (comboId == 0 || giftIdCur != comboGiftId) {
            comboGiftId = giftIdCur;
            comboCount = 1;
            comboId = System.currentTimeMillis();
        } else {
            comboCount++;
        }
    }

    private void cancelCount() {
        llGiftTimeTick.setVisibility(View.INVISIBLE);
        gift_dialog_amount_layout.setVisibility(View.VISIBLE);
        timer.cancel();
        comboId = 0;
        comboCount = 1;
        giftIdCur = 0;
        comboGiftId = 0;
    }

    CountDownTimer timer = new CountDownTimer(5000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            tvTimeTick.setText(millisUntilFinished / 1000 + "s");
        }

        @Override
        public void onFinish() {
            cancelCount();
        }
    }.start();

    @Override
    public void show() {


        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
            cancelCount();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}