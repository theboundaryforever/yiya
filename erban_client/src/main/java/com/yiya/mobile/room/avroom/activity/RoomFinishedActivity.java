package com.yiya.mobile.room.avroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.room.avroom.adapter.VideoRoomClosedAdapter;
import com.yiya.mobile.room.model.RoomModel;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.LiveVideoRoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomClosedBean;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 房间结束activity
 */
public class RoomFinishedActivity extends BaseActivity {
    private static final String TAG = RoomFinishedActivity.class.getSimpleName();

    private RoomModel roomModel;

    //个人（自己 或者 别人）  房间
    public static final String BUNDLE_KEY_ROOM_UID = "roomUid";
    public static final String BUNDLE_KEY_ROOM_TYPE = "roomType"; //3 语音 6直播

    @BindView(R.id.nick)
    TextView nickTv;
//    @BindView(R.id.tv_uid)
//    TextView tv_uid;
    @BindView(R.id.avatar)
    ImageView avatarIv;

    @BindView(R.id.live_finish_layout)
    ScrollView live_finish_layout;

    @BindView(R.id.home_page_btn)
    RelativeLayout home_page_btn;
    @BindView(R.id.attention_iv)
    ImageView mAttentionsIv;
    @BindView(R.id.attentions_tv)
    TextView mAttentionsTv;
    @BindView(R.id.open_user_info_tv)
    TextView mOpenUserInfoTv;
    @BindView(R.id.close_tv)
    TextView mCloseTv;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.rlMore)
    RelativeLayout rlMore;

    private VideoRoomClosedAdapter mAdapter;
    private GridLayoutManager mLayoutManager;

    private long mUserUid = 0;

    private boolean isAttentions;

    private long mRoomUid = 0;

    public static void start(Context context, long uid, int type) {
        Intent intent = new Intent(context, RoomFinishedActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong(BUNDLE_KEY_ROOM_UID, uid);
        bundle.putInt(BUNDLE_KEY_ROOM_TYPE, type);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_finished);
        ButterKnife.bind(this);
        initView();
        setOnListener();
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        long roomUid = bundle.getLong(BUNDLE_KEY_ROOM_UID);
        int roomType = bundle.getInt(BUNDLE_KEY_ROOM_TYPE);
        live_finish_layout.smoothScrollTo(0, 20);
        mLayoutManager = new GridLayoutManager(this, 2);
        mAdapter = new VideoRoomClosedAdapter(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        roomModel = new RoomModel();

        requestInfo(roomUid, roomType);
    }

    private void setOnListener() {
        mAdapter.setOnItemClickListener((adapter, view, position) -> {
            LiveVideoRoomInfo.RoomListBean item = mAdapter.getItem(position);
            if (item != null) {
                RoomFrameActivity.start(this, item.getUid(), item.getType());
            }
        });

        home_page_btn.setOnClickListener(v -> {
            if (mRoomUid != 0) {
                if (!isAttentions) {
                    roomModel.roomAttention(CoreManager.getCore(IAuthCore.class).getCurrentUid(), mRoomUid, new HttpRequestCallBack<Object>() {
                        @Override
                        public void onSuccess(String message, Object response) {
                            isAttentions = true;
                            mAttentionsTv.setText("已关注");
                            mAttentionsIv.setImageResource(R.mipmap.icon_finish_attentions_already);
                        }

                        @Override
                        public void onFailure(int code, String msg) {
                            toast(msg);
                        }
                    });
                } else {
                    roomModel.deleteAttention(CoreManager.getCore(IAuthCore.class).getCurrentUid(), mRoomUid, new HttpRequestCallBack<Object>() {
                        @Override
                        public void onSuccess(String message, Object response) {
                            isAttentions = false;
                            mAttentionsTv.setText("+ 关注");
                            mAttentionsIv.setImageResource(R.drawable.bg_room_video_finish_btn);
                        }

                        @Override
                        public void onFailure(int code, String msg) {
                            toast(msg);
                        }
                    });

                }
            }
        });

        mOpenUserInfoTv.setOnClickListener(v -> {
            if (mUserUid != 0) {
                UIHelper.showUserInfoAct(this, mUserUid);
            }

        });

        mCloseTv.setOnClickListener(v -> {
            finish();
        });
    }

    private void refreshView(RoomClosedBean info) {
        ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), info.getAvater(), avatarIv);
        nickTv.setText(info.getTitle());
//        tv_uid.setText("ID:" + String.valueOf(info.getErbanNo()));

        mUserUid = info.getUid();
        isAttentions = info.isAttentions();
        mRoomUid = info.getRoomId();

        if (info.isAttentions()) {
            mAttentionsTv.setText("已关注");
            mAttentionsIv.setImageResource(R.mipmap.icon_finish_attentions_already);
        } else {
            mAttentionsTv.setText("+ 关注");
            mAttentionsIv.setImageResource(R.drawable.bg_room_video_finish_btn);
        }

        if (ListUtils.isNotEmpty(info.getRecommends())) {
            rlMore.setVisibility(View.VISIBLE);
            mAdapter.setNewData(info.getRecommends());
        } else {
            rlMore.setVisibility(View.INVISIBLE);
        }
    }


    public void requestInfo(long roomUid, int roomType) {
        if (roomModel == null) {
            SingleToastUtil.showToast("出错咯");
            finish();
            return;
        }

        roomModel.getRoomClosedInfo(roomUid, roomType, new HttpRequestCallBack<RoomClosedBean>() {
            @Override
            public void onSuccess(String message, RoomClosedBean response) {
                if (response != null) {
                    refreshView(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {

            }
        });
    }
}
