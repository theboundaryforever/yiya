package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.yiya.mobile.room.avroom.adapter.YiYaAudioMicroViewAdapter;
import com.yiya.mobile.room.avroom.holder.AudioMicroViewHolder;
import com.yiya.mobile.room.avroom.other.OnMicroItemClickListener;
import com.yiya.mobile.room.face.anim.AnimFaceFactory;
import com.yiya.mobile.ui.widget.magicindicator.buildins.UIUtil;
import com.juxiao.library_ui.widget.WaveView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.RoomMatchAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.room.face.FaceReceiveInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCoreClient;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.zego.zegoavkit2.soundlevel.ZegoSoundLevelInfo;

import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * 上麦布局界面
 *
 * @author zeda
 */
public class YiYaAudioMicroView extends BaseMicroView {
    protected RecyclerView recyclerView;
    private SparseArray<ImageView> faceImageViews;

    private Disposable subscribe;

    private final static int SPAN_COUNT = 4;

    private YiYaAudioMicroViewAdapter adapter;

    public YiYaAudioMicroView(Context context) {
        this(context, null);
    }

    public YiYaAudioMicroView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public YiYaAudioMicroView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    @Override
    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        adapter.setOnMicroItemClickListener(onMicroItemClickListener);
    }

    @Override
    public void refreshMicroView(int micPosition) {
        if (micPosition == MIC_POSITION_ALL) {
            getAdapter().notifyDataSetChanged();
        } else {
            getAdapter().notifyItemChanged(AvRoomDataManager.getMicroViewPositionByMicPosition(micPosition));
        }
    }

    private void init(Context context) {
        recyclerView = new RecyclerView(context);
        recyclerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        recyclerView.setOverScrollMode(OVER_SCROLL_NEVER);
        addView(recyclerView);

        faceImageViews = new SparseArray<>();
        setupRecyclerView();

        YiYaAudioMicroViewAdapter adapter = getAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void setupRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), SPAN_COUNT);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        ((DefaultItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                recyclerView.postDelayed(() -> calculateMicCenterPoint(), 500);
                recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private YiYaAudioMicroViewAdapter getAdapter() {
        if (adapter == null) {
            adapter = new YiYaAudioMicroViewAdapter(getContext(), SPAN_COUNT);
        }
        return adapter;
    }

    public YiYaAudioMicroViewAdapter getMicroViewAdapter() {
        return adapter;
    }

    private void calculateMicCenterPoint() {
        int giftWidth = UIUtil.dip2px(getContext(), 80);
        int giftHeight = UIUtil.dip2px(getContext(), 80);
        int faceWidth = UIUtil.dip2px(getContext(), 80);
        int faceHeight = UIUtil.dip2px(getContext(), 80);

        SparseArray<Point> centerPoints = new SparseArray<>();
        // 算出每一个麦位的位置
        int childCount = getViewContainer().getChildCount();
        View child;

        for (int i = 0; i < childCount; i++) {
            child = getViewContainer().getChildAt(i);
            if (i == 0) {
                int[] viewLocation = new int[2];
                child.getLocationInWindow(viewLocation);
                int x = viewLocation[0];
                int y = viewLocation[1];
                // 放置表情占位image view
                if (getFaceImageViews().get(i - 1) == null) {
                    LayoutParams params = new LayoutParams(faceWidth, faceHeight);
//                    child.getLocationInWindow(viewLocation);
                    int[] containerLocation = new int[2];
                    this.getLocationInWindow(containerLocation);
//                    params.leftMargin = ((viewLocation[0] - containerLocation[0] + child.getWidth() / 2) - faceWidth / 2);
//                    params.topMargin = ((viewLocation[1] - containerLocation[1] + child.getHeight() / 2) - faceHeight / 2);
                    ImageView face = new ImageView(getContext());
                    face.setLayoutParams(params);
                    getFaceImageViews().put(i - 1, face);
                    addView(face);
                }
                Point point = new Point(x, y);
                centerPoints.put(i - 1, point);
                continue;
            }
            int[] location = new int[2];
            int[] nameLocation = new int[2];
            // 找到头像
            View view = child.findViewById(R.id.micro_layout);
            View nameView = child.findViewById(R.id.nick);
            if (view != null) child = view;
            child.getLocationInWindow(location);
            if (nameView != null) {
                nameView.getLocationInWindow(nameLocation);
            }
            int x = (location[0] + child.getWidth() / 2) - giftWidth / 2;
            int y = ((location[1] >= nameLocation[1]) || nameView == null) ?
                    ((location[1] + child.getHeight() * 7 / 8) - giftHeight / 2) :
                    ((nameLocation[1] + nameView.getHeight() / 2) - giftHeight / 2);
            // 放置表情占位image view
            if (getFaceImageViews().get(i - 1) == null) {
                LayoutParams params = new LayoutParams(faceWidth, faceHeight);
                child.getLocationInWindow(location);
                int[] containerLocation = new int[2];
                this.getLocationInWindow(containerLocation);
                params.leftMargin = ((location[0] - containerLocation[0] + child.getWidth() / 2) - faceWidth / 2);
                params.topMargin = ((location[1] - containerLocation[1] + child.getHeight() / 2) - faceHeight / 2);
                ImageView face = new ImageView(getContext());
                face.setLayoutParams(params);
                getFaceImageViews().put(i - 1, face);
                addView(face);
            }
            Point point = new Point(x, y);
            centerPoints.put(i - 1, point);
        }
        AvRoomDataManager.get().setMicPoint(centerPoints);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        CoreManager.addClient(this);
        subscribe = IMNetEaseManager.get().getChatRoomEventObservable().subscribe(this::onReceiveRoomEvent);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
        if (subscribe != null) {
            subscribe.dispose();
            subscribe = null;
        }
    }

    protected RecyclerView getViewContainer() {
        return recyclerView;
    }

    protected SparseArray<ImageView> getFaceImageViews() {
        return faceImageViews;
    }


    private void onReceiveRoomEvent(RoomEvent roomEvent) {
        if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) return;
        switch (roomEvent.getEvent()) {
            case RoomEvent.SPEAK_STATE_CHANGE:
                onSpeakQueueUpdate(roomEvent.getMicPositionList());
                break;
            case RoomEvent.SPEAK_ZEGO_STATE_CHANGE:
                onZegoSpeakQueueUpdate(roomEvent.getSpeakQueueMembersPosition());
                break;
            case RoomEvent.CURRENT_SPEAK_STATE_CHANGE:
                onCurrentSpeakUpdate(roomEvent.getCurrentMicPosition(), roomEvent.getCurrentMicStreamLevel());
                break;
            default:
        }
    }

    /**
     * 声网的声浪监听
     */
    private void onSpeakQueueUpdate(List<Integer> positions) {
        int count = getViewContainer().getChildCount();
        int myMicPosition = AvRoomDataManager.get().getMicPosition(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        for (int i = 0; i < positions.size(); i++) {
            int micPosition = positions.get(i);
            int viewPosition = AvRoomDataManager.getMicroViewPositionByMicPosition(micPosition);
            if (viewPosition >= count) continue;
            WaveView speakState = getViewContainer().getChildAt(viewPosition).findViewById(R.id.wave_view);
            if (myMicPosition == micPosition && RtcEngineManager.get().isMute()) {
                //如果是我自己，并且已经静音本地麦的话，那就停止声浪
                //一般来说，声网下麦/静音本地麦后，声网不会有声音回调，会自动关闭声浪；但是有一个情况意外：断网的时候,声网下麦/静音本地麦后，声网还会有声音回调回来，所以这里加多一层判断
                speakState.stop();
            } else if (speakState != null) {
                speakState.start();
            }
        }
    }

    /**
     * 即构除自己外其他人的声浪监听
     */
    private void onZegoSpeakQueueUpdate(List<ZegoSoundLevelInfo> speakers) {
        int count = getViewContainer().getChildCount();
        for (int i = 0; i < speakers.size(); i++) {
            int pos = AvRoomDataManager.getMicroViewPositionByMicPosition(AvRoomDataManager.get().getMicPositionByStreamID(speakers.get(i).streamID));
            if (pos >= count) continue;
            WaveView speakState = getViewContainer().getChildAt(pos).findViewById(R.id.wave_view);
            if (speakState != null) {
                if (RtcEngineManager.get().isRemoteMute()) {
                    speakState.stop();
                } else {
                    if (speakers.get(i).soundLevel > 0) {
                        speakState.start();
                    } else {
                        speakState.stop();
                    }
                }
            }
        }
    }

    /**
     * 即构除自己的声浪监听
     *
     * @param currentMicPosition 当前麦位逻辑
     */
    private void onCurrentSpeakUpdate(int currentMicPosition, float currentMicStreamLevel) {
        if (currentMicPosition == Integer.MIN_VALUE) {
            return;
        }
        WaveView speakState = getViewContainer().getChildAt(AvRoomDataManager.getMicroViewPositionByMicPosition(currentMicPosition)).findViewById(R.id.wave_view);
        if (speakState != null) {
            if (RtcEngineManager.get().isMute()) {
                speakState.stop();
            } else {
                if (currentMicStreamLevel > 0) {
                    speakState.start();
                } else {
                    speakState.stop();
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IFaceCoreClient.class)
    public void onReceiveFace(List<FaceReceiveInfo> faceReceiveInfos) {
        Log.i("MicroView", "onReceiveFace: ");
        if (faceReceiveInfos == null || faceReceiveInfos.size() <= 0) return;
        for (FaceReceiveInfo faceReceiveInfo : faceReceiveInfos) {
            int position = AvRoomDataManager.get().getMicPosition(faceReceiveInfo.getUid());
            if (position < -1) continue;
            ImageView imageView = faceImageViews.get(position);
            if (imageView == null) continue;
            AnimationDrawable drawable = AnimFaceFactory.get(faceReceiveInfo, getContext(), imageView.getWidth(), imageView.getHeight());
            if (drawable == null) continue;
            imageView.setImageDrawable(drawable);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            drawable.setOneShot(true);
            drawable.start();
        }
    }

    @CoreEvent(coreClientClass = IFaceCoreClient.class)
    public void onReceiveMatchFace(RoomMatchAttachment faceAttachment) {
        if (faceAttachment == null) return;
        int position = AvRoomDataManager.get().getMicPosition(faceAttachment.getUid());
        if (position < -1) return;
        ImageView imageView = faceImageViews.get(position);
        if (imageView == null) return;
        AnimationDrawable drawable = AnimFaceFactory.get(faceAttachment.getNumArr(), getContext(), imageView.getWidth(), imageView.getHeight(), faceAttachment.isShowd(), faceAttachment.isShowd() ? 2000 : 1000);
        if (drawable == null) return;
        imageView.setImageDrawable(drawable);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        drawable.setOneShot(true);
        drawable.start();
    }

    @Override
    public void release() {
        for (int i = 0; i < getViewContainer().getChildCount(); i++) {
            View child = getViewContainer().getChildAt(i);
            RecyclerView.ViewHolder holder = getViewContainer().getChildViewHolder(child);
            if (holder instanceof AudioMicroViewHolder) {
                ((AudioMicroViewHolder) holder).clear();
            }
        }
        // 移除所有的表情动画
        for (int i = -1; i < getFaceImageViews().size() - 1; i++) {
            ImageView imageView = getFaceImageViews().get(i);
            if (imageView == null) continue;
            imageView.setImageDrawable(null);
            imageView.clearAnimation();
        }
    }
}

