package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/9
 * 描述        视频房item的用户信息View
 * <p>
 * 更新者      dell
 *
 * @author dell
 */

public class MicroUserInfoView extends RelativeLayout {

    public ImageView avatar, ivHeadWear;
    public TextView tvNick, tvId, tv_follow;
    private Context mContext;

    public MicroUserInfoView(Context context) {
        this(context, null);
        this.mContext = context;
    }

    public MicroUserInfoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        this.mContext = context;
    }

    public MicroUserInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    /**
     * 初始化View
     */
    private void initView() {
        inflate(getContext(), R.layout.layout_micro_user_info, this);
        avatar = findViewById(R.id.avatar);
        tvNick = findViewById(R.id.tv_nick);
        tvId = findViewById(R.id.tv_id);
        tv_follow = findViewById(R.id.tv_follow);
        ivHeadWear = findViewById(R.id.iv_headwear);
    }

    /**
     * 展示用户信息
     *
     * @param roomMember 用户信息
     * @param itemType   item类型
     */
    public void setMicroUserInfo(IMChatRoomMember roomMember, int itemType) {
        if (roomMember == null) {
            return;
        }

        //--------头饰-----------
        String headwearUrl = roomMember.getHeadwear_url();
        if (!TextUtils.isEmpty(headwearUrl)) {
            ImageLoadUtils.loadImage(mContext, headwearUrl, ivHeadWear);
            ivHeadWear.setVisibility(View.VISIBLE);
        } else {
            ivHeadWear.setVisibility(View.GONE);
        }
        //--------头饰-----------

        //设置公共数据
        ImageLoadUtils.loadAvatar(mContext, roomMember.getAvatar(), avatar, true);
        tvNick.setText(buildNick(roomMember.getNick()));
        tvId.setText("ID:"+roomMember.getAccount());
    }

    /**
     * 展示用户信息
     * @param roomInfo 房间信息
     */
    public void setMicroUserInfo(RoomInfo roomInfo) {
        if (roomInfo == null) {
            return;
        }

        RoomQueueInfo roomQueueMember= AvRoomDataManager.get().getRoomQueueMemberInfoByAccount(String.valueOf(roomInfo.getUid()));
        if(roomQueueMember == null){
            return;
        }
        IMChatRoomMember mChatRoomMember = roomQueueMember.getMChatRoomMember();
        if(mChatRoomMember == null){
            return;
        }

        //--------头饰-----------
        String headwearUrl = mChatRoomMember.getHeadwear_url();
        if (!TextUtils.isEmpty(headwearUrl)) {
            ImageLoadUtils.loadImage(getContext(), headwearUrl, ivHeadWear);
            ivHeadWear.setVisibility(View.VISIBLE);
        } else {
            ivHeadWear.setVisibility(View.GONE);
        }
        //--------头饰-----------
        if(RoomInfo.ROOMTYPE_VIDEO_LIVE == roomInfo.getType()){// 视频房取用户昵称
            tvNick.setText(roomInfo.getNick());
            ImageLoadUtils.loadAvatar(getContext(), mChatRoomMember.getAvatar(), avatar, true);// 语音房头像就是封面
        } else {
            ImageLoadUtils.loadAvatar(getContext(), roomInfo.getAvatar(), avatar, true);// 语音房头像就是封面
            if(!StringUtils.isEmpty(roomInfo.getTitle())){// 语音房优先房间标题
                tvNick.setText(roomInfo.getTitle());
            } else {
                tvNick.setText(roomInfo.getNick());
            }
        }

        tvId.setText("ID:"+roomInfo.getErbanNo());
    }

    public void setAttention(boolean attention){
        tv_follow.setSelected(attention);
        if(attention){
            tv_follow.setVisibility(GONE);
        }
    }

    private String buildLocation(String city) {
        if (StringUtils.isEmpty(city)) {
            return "未知";
        }
        if (city.length() > 3) {
            return city.substring(0, 3) + "...";
        }
        return city;
    }

    /**
     * 切割昵称
     */
    private String buildNick(String nick) {
        if (StringUtils.isEmpty(nick)) {
            return "未知";
        }
//        if (nick.length() > 3) {
//            return nick.substring(0, 3) + "...";
//        }
        return nick;
    }

}