package com.yiya.mobile.room.widget.dialog;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyRewardInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * ProjectName:
 * Description:超级宝箱dialog
 * Created by BlackWhirlwind on 2019/7/11.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomLuckyBoxRewardDialog extends BaseDialogFragment {

    private static final String KEY = "roomLuckyRewardInfo";
    private static final String EVENT = "EVENT";
    @BindView(R.id.iv_reward_first)
    ImageView ivRewardFirst;
    @BindView(R.id.iv_reward_second)
    ImageView ivRewardSecond;
    @BindView(R.id.iv_reward_third)
    ImageView ivRewardThird;
    @BindView(R.id.tv_reward_tips)
    TextView tvRewardTips;
    @BindView(R.id.tv_reward_first)
    TextView tvRewardFirst;
    @BindView(R.id.tv_reward_second)
    TextView tvRewardSecond;
    @BindView(R.id.tv_reward_third)
    TextView tvRewardThird;
    @BindView(R.id.group_reward_first)
    Group groupRewardFirst;
    @BindView(R.id.group_reward_second)
    Group groupRewardSecond;
    @BindView(R.id.group_reward_third)
    Group groupRewardThird;
    @BindView(R.id.iv_reward_first_bg_light)
    ImageView ivRewardFirstLight;
    @BindView(R.id.iv_reward_second_bg_light)
    ImageView ivRewardSecondLight;
    @BindView(R.id.iv_reward_third_bg_light)
    ImageView ivRewardThirdLight;
    private Unbinder unbinder;

    public static RoomLuckyBoxRewardDialog newInstance(RoomLuckyRewardInfo roomLuckyRewardInfo, int event) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY, roomLuckyRewardInfo);
        bundle.putInt(EVENT, event);
        RoomLuckyBoxRewardDialog dialog = new RoomLuckyBoxRewardDialog();
        dialog.setArguments(bundle);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(false);
        Window window = getDialog().getWindow();
        if (window != null) {
            View view = inflater.inflate(R.layout.dialog_lucky_box_reward, window.findViewById(android.R.id.content), false);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            unbinder = ButterKnife.bind(this, view);
            return view;
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            RoomLuckyRewardInfo roomLuckyRewardInfo = arguments.getParcelable(KEY);
            int event = arguments.getInt(EVENT);

            if (roomLuckyRewardInfo == null) {
                return;
            }
            ImageView[] imageViews = {ivRewardFirst, ivRewardSecond, ivRewardThird};
            ImageView[] ivLights = {ivRewardFirstLight, ivRewardSecondLight, ivRewardThirdLight};
            TextView[] textViews = {tvRewardFirst, tvRewardSecond, tvRewardThird};
            Group[] groups = {groupRewardFirst, groupRewardSecond, groupRewardThird};

            if (RoomEvent.CODE_LUCKY_BOX_REWARD_USER == event) {
                //用户提示
                tvRewardTips.setText("恭喜你成为探险者");
            } else {
                //主播提示
                String nick = roomLuckyRewardInfo.getNick();
                if (nick.length() > 6) {
                    nick = nick.substring(0, 6).concat("...");
                }
                String tips = "恭喜你成为探险主播\n";
                SpannableStringBuilder ssb = new SpannableStringBuilder(String.format(tips + "%s是本次探险者", nick));
                ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.color_FFC90D)),
                        tips.length(),
                        tips.length() + nick.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvRewardTips.setText(ssb);

            }
            //奖励
            List<RoomLuckyRewardInfo.Prize> prizeList = roomLuckyRewardInfo.getPrizeList();
            if (ListUtils.isNotEmpty(prizeList)) {
                for (int i = 0; i < prizeList.size(); i++) {
                    RoomLuckyRewardInfo.Prize prize = prizeList.get(i);
                    groups[i].setVisibility(View.VISIBLE);
                    ImageLoadUtils.loadImage(getContext(), prize.getPic(), imageViews[i]);

                    textViews[i].setText(prize.getDesc());

                    ObjectAnimator animLight = ObjectAnimator.ofFloat(ivLights[i], "rotation", 0, 359)
                            .setDuration(2000);
                    animLight.setInterpolator(new LinearInterpolator());
                    animLight.setRepeatCount(ValueAnimator.INFINITE);
                    animLight.start();
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @OnClick(R.id.tv_reward_confirm)
    public void onViewClicked() {
        dismiss();
    }
}
