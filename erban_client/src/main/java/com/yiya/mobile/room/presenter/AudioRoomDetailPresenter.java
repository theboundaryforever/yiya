package com.yiya.mobile.room.presenter;

import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.bean.RecommendWorldsInfo;
import com.tongdaxing.xchat_core.bean.RoomRedPackageInfo;
import com.tongdaxing.xchat_core.bean.SendRedPackageInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.LinkedList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 语音房间页面的presenter
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class AudioRoomDetailPresenter extends BaseRoomDetailPresenter<IAudioRoomDetailView> {

    private LinkedList<RoomRedPackageInfo> roomRedPackageInfos = new LinkedList<>();//可以抢的红包列表
    private List<RankingXCInfo.ListBean> charmList;
    private List<RankingXCInfo.ListBean> richList;

    public AudioRoomDetailPresenter() {
        super();
    }

//    /**
//     * 处理猪猪大作战结果
//     * @param pigFightBean
//     */
//    public void dealPFResult(PigFightBean pigFightBean){
//
//    }

    public void setPigFightCrt(int pigFightCrt){
        AvRoomDataManager.get().setPigFight(pigFightCrt);
    }

    /**
     * 获取贡献榜榜单
     */
    public void refleshContribute() {
        if (getCurrRoomInfo() != null) {
            getRoomCharm(getCurrRoomInfo().getRoomId());
            getRoomRich(getCurrRoomInfo().getRoomId());
        }
    }

    private void getRoomRich(long roomUid) {
        model.getRoomOnlineMembers(1, 1, roomUid, new CallBack<List<RankingXCInfo.ListBean>>() {
            @Override
            public void onSuccess(List<RankingXCInfo.ListBean> data) {
                getMvpView().refreshRich(data);
                setRichList(data);
            }
        });
    }

    private void getRoomCharm(long roomUid) {
        model.getRoomOnlineMembers(2, 1, roomUid, new CallBack<List<RankingXCInfo.ListBean>>() {
            @Override
            public void onSuccess(List<RankingXCInfo.ListBean> data) {
                getMvpView().refreshCharm(data);
                setCharmList(data);
            }
        });
    }

    public void insertRoomRedPackage(RoomRedPackageInfo roomRedPackageInfo) {
        roomRedPackageInfos.addFirst(roomRedPackageInfo);
        if (getMvpView() != null) {
            getMvpView().refreshRedPackageView(roomRedPackageInfos);
        }
    }

    /**
     * 领取红包
     */
    public void receiveRoomRedPackage() {
        RoomRedPackageInfo roomRedPackageInfo = roomRedPackageInfos.pollFirst();
        if (roomRedPackageInfo == null) {
            return;
        }

        if (roomRedPackageInfo.getRedpacketType() == RoomRedPackageInfo.REDPACKETTYPE_OMMIC) {
            if (isOnMicByMyself()) {
                receiveRoomRedPackage(AvRoomDataManager.get().getRoomInfo().getRoomId(), roomRedPackageInfo.getRedPackId());
            }
        } else {
            receiveRoomRedPackage(AvRoomDataManager.get().getRoomInfo().getRoomId(), roomRedPackageInfo.getRedPackId());
        }
        getMvpView().refreshRedPackageView(roomRedPackageInfos);
    }

    private void receiveRoomRedPackage(long roomId, long redPackId) {
        model.receiveRoomRedPackage(roomId, redPackId, new HttpRequestCallBack<SendRedPackageInfo>() {
            @Override
            public void onSuccess(String message, SendRedPackageInfo response) {
                if (getMvpView() != null) {
                    getMvpView().showReceivedRedPackageView(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 获取可以领取的红包列表
     */
    public void getSendRedPackageRecord() {
        if(getCurrRoomInfo() == null)return;

        model.getRoomSendRedPackageRecord(String.valueOf(getCurrRoomInfo().getRoomId()), new OkHttpManager.MyCallBack<ServiceResult<LinkedList<RoomRedPackageInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().toast(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<LinkedList<RoomRedPackageInfo>> response) {
                if (response != null && response.isSuccess()) {
                    roomRedPackageInfos = response.getData();
                    if (getMvpView() != null) {
                        getMvpView().refreshRedPackageView(roomRedPackageInfos);
                    }
                } else {
                    roomRedPackageInfos.clear();
                    if (getMvpView() != null) {
                        getMvpView().refreshRedPackageView(null);
                        getMvpView().toast(response == null ? "数据异常" : response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * 抱人上麦
     */
    public void holdUpMicro(long uid) {
        int freePosition = AvRoomDataManager.get().findFreePosition();
        if (freePosition == Integer.MIN_VALUE) {
            SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
            return;
        }
        IMNetEaseManager.get().inviteMicroPhoneBySdk(uid, freePosition);
    }

    public void agreeCharmPK(int pkId, long roomId, long uid) {
        model.agreeCharmPK(pkId, roomId, uid, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {

            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 获取房间魅力值pk倒计时
     */
    public void getCharmCountDown() {
        if (getCurrRoomInfo() == null) {
            return;
        }
        model.getCharmCountDown(getCurrRoomInfo().getRoomId(), new HttpRequestCallBack<Long>() {
            @Override
            public void onSuccess(String message, Long response) {
                if (getMvpView() != null) {
                    getMvpView().startCPKCountDown(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {

            }
        });
    }

    /**
     * 获取房间常用语接口
     */
    public void getRecommendWorlds() {
        if(getCurrRoomInfo() == null)return;

        model.getRecommendWorlds(new OkHttpManager.MyCallBack<ServiceResult<RecommendWorldsInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().showRecommendWorlds(null);
                }

            }

            @Override
            public void onResponse(ServiceResult<RecommendWorldsInfo> response) {
                if (getMvpView() != null) {
                    if (response != null && response.isSuccess()) {
                        getMvpView().showRecommendWorlds(response.getData());
                    }else {
                        getMvpView().showRecommendWorlds(null);
                    }
                }
            }
        });
    }

    /**
     * 启动关闭猪猪大作战UI
     */
    public void openOrClosePigFight(int pigSwitch) {
        model.openOrClosePigFight(pigSwitch, new HttpRequestCallBack<Object>() {

            @Override
            public void onSuccess(String message, Object response) {

            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    public void clear() {
        charmList = null;
        richList = null;
    }
}
