package com.yiya.mobile.room.avroom.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.fragment.BaseFragment;
import com.yiya.mobile.room.avroom.activity.RoomChatActivity;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.netease.nim.uikit.session.actions.ImageAction;
import com.netease.nim.uikit.session.constant.Extras;
import com.netease.nim.uikit.session.fragment.MessageFragment;
import com.netease.nim.uikit.uinfo.UserInfoHelper;
import com.netease.nim.uikit.uinfo.UserInfoObservable;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件描述：
 * 私聊的封装fragment
 *
 * @auther：zwk
 * @data：2019/1/2
 */
public class PrivateChatFragment extends BaseFragment {
    protected String sessionId;
    private MessageFragment messageFragment;
    private SessionCustomization customization;
    private UserInfoObservable.UserInfoObserver uinfoObserver;
    private TextView tvToolbarTitle;
    private ImageView ivBack;

    private LinearLayout mAttentionLl;
    private ImageView mAttentionIv;
    private SVGAImageView mWaveSVG;
    private ImageView mWaveIv;


    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (bundle != null) {
            sessionId = bundle.getString(Extras.EXTRA_ACCOUNT, sessionId);
        } else {
            if (getArguments() != null)
                sessionId = getArguments().getString(Extras.EXTRA_ACCOUNT, "");
        }
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_private_chat;
    }

    @Override
    public void onFindViews() {


        tvToolbarTitle = mView.findViewById(R.id.tv_list_data_title);
        ivBack = mView.findViewById(R.id.iv_private_chat_close);
        mAttentionLl = mView.findViewById(R.id.attention_layout_ll);
        mAttentionIv = mView.findViewById(R.id.attention_iv);

        mWaveSVG = mView.findViewById(R.id.svga_wave);
        mWaveIv = mView.findViewById(R.id.svga_wave_iv);


        messageFragment = new MessageFragment();
        Bundle arguments = getArguments();
        customization = new SessionCustomization();
        ArrayList<BaseAction> actions = new ArrayList<>();
        actions.add(new ImageAction());
        // 隐藏房间私聊送礼 zdw
//        actions.add(new GiftAction());
        customization.actions = actions;
        customization.withSticker = true;
        arguments.putSerializable(Extras.EXTRA_CUSTOMIZATION, customization);
        arguments.putSerializable(Extras.EXTRA_TYPE, SessionTypeEnum.P2P);
        messageFragment.setArguments(arguments);
        messageFragment.setContainerId(R.id.msg_fragment_container);
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_room_msg_container, messageFragment).commitAllowingStateLoss();
        requestBuddyInfo();

        if (!NimUIKit.isRobotUid(sessionId)) {
            CoreManager.getCore(IPraiseCore.class).checkUserFansAndBlack(CoreManager.getCore(IAuthCore.class).getCurrentUid(), Long.parseLong(sessionId));
        }


    }

//    @CoreEvent(coreClientClass = IPraiseClient.class)
//    public void onIsLiked(Boolean islike, long uid) {
//        mAttentionLl.setVisibility(islike ? View.GONE : View.VISIBLE);
//    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCheckUserFansAndBlack(long uid, String json) {
//        mAttentionLl.setVisibility(islike ? View.GONE : View.VISIBLE);

        try {
            Json tmpJson = new Json(json);
            Json dataJson = tmpJson.json("data");
            if (dataJson.boo("like")) {
                mAttentionLl.setVisibility(View.GONE);
            } else {
                mAttentionLl.setVisibility(View.VISIBLE);
            }
            if (dataJson.boo("online")) {

                SVGAParser svgaParser = new SVGAParser(mContext);
                svgaParser.decodeFromAssets("microredwave.svga", new SVGAParser.ParseCompletion() {
                    @Override
                    public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                        mWaveSVG.setVideoItem(svgaVideoEntity);
                        if (!mWaveSVG.isAnimating()) {
                            mWaveSVG.startAnimation();
                        }
                    }

                    @Override
                    public void onError() {
                        LogUtil.e("svgaWave onError");
                    }
                });
                mWaveSVG.setVisibility(View.VISIBLE);
                mWaveIv.setVisibility(View.GONE);

            } else {

                mWaveSVG.setVisibility(View.GONE);
                mWaveIv.setVisibility(View.VISIBLE);
            }


        } catch (Exception e) {
            e.printStackTrace();

            mAttentionLl.setVisibility(View.GONE);
        }

    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        mAttentionLl.setVisibility(View.GONE);
    }


    @Override
    public void onSetListener() {
        ivBack.setOnClickListener(this);
        mAttentionIv.setOnClickListener(this::onClick);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.attention_iv:
                CoreManager.getCore(IPraiseCore.class).praise(Long.parseLong(sessionId));
                break;
            default:
                if (mContext != null && mContext instanceof RoomChatActivity) {
                    ((RoomChatActivity) mContext).backToMainChatList();
                }
                break;
        }

    }

    @Override
    public void initiate() {
        CoreManager.addClient(this);
        GrowingIO.getInstance().setPageName(this, "房间-私聊页");
    }

    private void registerObservers(boolean register) {
        if (register) {
            registerUserInfoObserver();
        } else {
            unregisterUserInfoObserver();
        }

    }

    private void registerUserInfoObserver() {
        if (uinfoObserver == null) {
            uinfoObserver = new UserInfoObservable.UserInfoObserver() {
                @Override
                public void onUserInfoChanged(List<String> accounts) {
                    if (accounts.contains(sessionId)) {
                        requestBuddyInfo();
                    }
                }
            };
        }

        UserInfoHelper.registerObserver(uinfoObserver);
    }

    private void unregisterUserInfoObserver() {
        if (uinfoObserver != null) {
            UserInfoHelper.unregisterObserver(uinfoObserver);
        }
    }

    private void requestBuddyInfo() {
        // 显示自己的textview并且居中
        String userTitleName = UserInfoHelper.getUserTitleName(sessionId, SessionTypeEnum.P2P);
        if (tvToolbarTitle != null)
            tvToolbarTitle.setText(userTitleName);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (messageFragment != null) {
            messageFragment.onActivityResult(requestCode, resultCode, data);
        }
        if (customization != null) {
            customization.onActivityResult(getActivity(), requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        registerObservers(false);
        CoreManager.removeClient(this);


    }
}
