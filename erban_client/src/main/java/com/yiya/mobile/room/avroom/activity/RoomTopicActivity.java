package com.yiya.mobile.room.avroom.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.base.view.TitleBar;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/5/2.
 */

public class RoomTopicActivity extends BaseActivity {
    @BindView(R.id.title_bar)
    TitleBar titleBar;
    @BindView(R.id.edt_topic_title)
    EditText edtTopicTitle;

    @BindView(R.id.edt_topic_content)
    EditText edtTopicContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();

        setContentView(R.layout.room_topic_edit);

        ButterKnife.bind(this);

        String roomNotice = roomInfo.getRoomNotice();
        if (!TextUtils.isEmpty(roomNotice)) {
            edtTopicContent.setText(roomNotice);
        }
        GrowingIO.getInstance().trackEditText(edtTopicContent);
        edtTopicTitle.setText(roomInfo.getRoomDesc() + "");
        GrowingIO.getInstance().trackEditText(edtTopicTitle);
        initTitleBar("设置房间话题");
        TextView rightAction = new TextView(this);
        rightAction.setTextColor(Color.parseColor("#1a1a1a"));
        rightAction.setText("保存");
        rightAction.setOnClickListener(v -> {
            String topicTitle = edtTopicTitle.getText().toString();
            String topicContent = edtTopicContent.getText().toString();
            if (TextUtils.isEmpty(topicTitle)) {
                toast("标题不能为空");
                return;
            }
            String url = "";
            if (AvRoomDataManager.get().isRoomOwner()) {
                url = "/room/update";
            } else if (AvRoomDataManager.get().isRoomAdmin()) {
                url = "/room/updateByAdmin";
            } else {
                return;
            }
            save(url, topicTitle, topicContent);
        });
        titleBar.mRightLayout.addView(rightAction);
    }

    private void save(String url, String topicTitle, String topicContent) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo == null) {
            return;
        }
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("type", String.valueOf(RoomInfo.ROOMTYPE_HOME_PARTY));
        params.put("roomDesc", topicTitle);
        params.put("tagId", roomInfo.tagId + "");
        params.put("uid", uid + "");
        params.put("ticket", ticket);
        params.put("roomUid", roomInfo.getUid() + "");
        params.put("roomNotice", topicContent);
//        params.put("title", topicTitle);
//        params.put("backPic", null);
//        params.put("roomPwd", null);
//        params.put("playInfo", null);
//        params.put("giftEffectSwitch", null);
//        params.put("publicChatSwitch", null);
        OkHttpManager.getInstance().doPostRequest(UriProvider.JAVA_WEB_URL + url, params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(Json json) {
                if (json.num("code") == 200) {
                    toast("保存成功");
                    finish();
                } else {
                    toast(json.str("message", "网络异常"));
                }
            }
        });
    }


    public static void start(Context context) {
        Intent intent = new Intent(context, RoomTopicActivity.class);
        context.startActivity(intent);
    }
}
