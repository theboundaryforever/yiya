package com.yiya.mobile.room.gift;

/**
 * 文件描述：礼物列表界面
 *
 * @auther：zwk
 * @data：2019/2/28
 */

//public class GiftListFragment extends BaseLazyFragment implements GiftAdapterNew.OnItemClickListener, GiftDialogNew.OnSelectStateListener {
//    private GiftAdapterNew adapter;
//    RecyclerView gridView;
//    private int giftType = 2;
//    private int currentGiftPosition = 0;
//    private int selectGift = 0;
//
//    @Override
//    public int getRootLayoutId() {
//        return R.layout.fragment_gift_list;
//    }
//
//    @Override
//    public void onFindViews() {
//        gridView = mView.findViewById(R.id.gridView);
//        adapter = new GiftAdapterNew(getContext());
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 4);
//        gridView.setLayoutManager(gridLayoutManager);
//        gridView.setAdapter(adapter);
//        if (getArguments() != null) {
//            giftType = getArguments().getInt("giftType", 3);
//            currentGiftPosition = getArguments().getInt("currentGiftPosition", 0);
//        }
//        if (giftType == 2) {
//            adapter.setIndex(0);
//        }
//    }
//
//    @Override
//    public void onSetListener() {
//        adapter.setOnItemClickListener(this);
//        if (getParentFragment() instanceof GiftDialogNew) {
//            if (currentGiftPosition == 0) {
//                ((GiftDialogNew) getParentFragment()).setOnSelectStateListener(this);
//            } else if (currentGiftPosition == 1) {
//                ((GiftDialogNew) getParentFragment()).setOnMystSelectStateListener(this);
//            }
//        }
//    }
//
//    @Override
//    public void initiate() {
//    }
//
//    @Override
//    public void onItemClick(View view, int position) {
//        if (adapter == null)
//            return;
//        int currentIndex = adapter.getIndex();
//        adapter.setIndex(position);
//        adapter.notifyItemChanged(currentIndex);
//        adapter.notifyItemChanged(position);
//        if (adapter.getGiftInfoList() != null && adapter.getGiftInfoList().size() > position) {
//            if (getParentFragment() instanceof GiftDialogNew) {
//                ((GiftDialogNew) getParentFragment()).onChangeCurrentGiftInfo(currentGiftPosition, adapter.getGiftInfoList().get(position));
//            }
//        }
//    }
//
//
//    /**
//     * 取消选中状态
//     *
//     * @param currentGiftPosition
//     */
//    @Override
//    public void cancelSelect(int currentGiftPosition) {
//        if (this.currentGiftPosition != currentGiftPosition) {
//            if (adapter == null) {
//                return;
//            }
//            int currentIndex = adapter.getIndex();
//            if (currentIndex == -1) {
//                return;
//            }
//            adapter.setIndex(-1);
//            if (adapter.getGiftInfoList() != null && adapter.getGiftInfoList().size() > currentIndex) {
//                adapter.notifyItemChanged(currentIndex);
//            }
//        }
//    }
//
//    @Override
//    public void onUpdateGiftList(int currentGiftPosition) {
//        if (this.currentGiftPosition == currentGiftPosition) {
//            onLazyLoadData();
//        }
//    }
//
//    @Override
//    protected void onLazyLoadData() {
//        List<GiftInfo> giftInfoList = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(giftType);
//        adapter.setGiftInfoList(giftInfoList);
//        adapter.notifyDataSetChanged();
//    }
//}
