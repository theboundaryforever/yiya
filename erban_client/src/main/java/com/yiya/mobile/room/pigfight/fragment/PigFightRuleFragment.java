package com.yiya.mobile.room.pigfight.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.base.fragment.BaseMvpListFragment;
import com.yiya.mobile.presenter.pigfight.IPigFightRuleListView;
import com.yiya.mobile.presenter.pigfight.PigFightRuleListPresenter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

@CreatePresenter(PigFightRuleListPresenter.class)
public class PigFightRuleFragment extends BaseMvpListFragment<PigFightRuleFragment.PigFightRuleListAdapter, IPigFightRuleListView, PigFightRuleListPresenter> implements IPigFightRuleListView{
    @Override
    protected void initMyView() {
        hasDefualLoadding = false;
        srlRefresh.setEnableRefresh(false);
        srlRefresh.setEnableLoadMore(false);
    }

    @Override
    protected RecyclerView.LayoutManager initManager() {
        return new LinearLayoutManager(getContext());
    }

    @Override
    protected PigFightRuleListAdapter initAdapter() {
        return new PigFightRuleListAdapter();
    }

    @Override
    protected void initClickListener() {

    }

    @Override
    public void initData() {
        getMvpPresenter().getRuleList();
    }

    @Override
    public void showRuleList(ServiceResult<List<String>> serviceResult) {
        dealSuccess(serviceResult, "暂无数据");
    }


    public class PigFightRuleListAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

        public PigFightRuleListAdapter() {
            super(R.layout.item_plant_bean_rule);
        }

        @Override
        protected void convert(BaseViewHolder helper, String item) {
            helper.setText(R.id.tv_content, item)
                    .setText(R.id.tv_index, String.valueOf(helper.getAdapterPosition() + 1));
        }
    }
}
