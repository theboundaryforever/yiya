package com.yiya.mobile.room.avroom.widget.dialog;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.room.bean.RoomFunctionBean;
import com.tongdaxing.xchat_core.room.bean.RoomFunctionEnum;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SpUtils;

import java.util.ArrayList;
import java.util.List;

public class RoomFunctionModel {
    public final static int ROOM_AUDIO_MANAGE = 0;
    public final static int ROOM_AUDIO_NORMAL = ROOM_AUDIO_MANAGE + 1;
    public final static int ROOM_VIDEO_DEFAULT = ROOM_AUDIO_NORMAL + 1;// 游客
    public final static int ROOM_LIVE_OWNER = ROOM_VIDEO_DEFAULT + 1;// 主播

    public RoomFunctionEnum[] getRoomFunctionByType(int type) {

        List<RoomFunctionEnum> list = new ArrayList<>();
        switch (type) {
            case ROOM_AUDIO_MANAGE:// 房间设置  问题反馈 开启公屏 魅力值PK 发红包 种豆消息 开启大作战
                list.add(RoomFunctionEnum.ROOM_SETTING);
                list.add(RoomFunctionEnum.ROOM_FEEDBACK);
                list.add(RoomFunctionEnum.ROOM_PUBLIC_ENABLE);
//                list.add(RoomFunctionEnum.ROOM_CPK);
//                list.add(RoomFunctionEnum.ROOM_RED_PACKET);
//                list.add(RoomFunctionEnum.ROOM_PLANT_BEAN);
//                list.add(RoomFunctionEnum.ROOM_PIG_FIGHT);
                break;
            case ROOM_AUDIO_NORMAL://  问题反馈  红包 种豆消息
                list.add(RoomFunctionEnum.ROOM_FEEDBACK);
//                list.add(RoomFunctionEnum.ROOM_RED_PACKET);
//                list.add(RoomFunctionEnum.ROOM_PLANT_BEAN);
                break;
            case ROOM_VIDEO_DEFAULT:// 私聊 种豆消息
                list.add(RoomFunctionEnum.ROOM_PRIVATE_CHAT);
//                list.add(RoomFunctionEnum.ROOM_PLANT_BEAN);
                break;
            case ROOM_LIVE_OWNER://发言 私聊 反转  静音  镜像  分享 种豆消息
                list.add(RoomFunctionEnum.ROOM_SPEAK);
                list.add(RoomFunctionEnum.ROOM_PRIVATE_CHAT);
                list.add(RoomFunctionEnum.ROOM_REVERSAL);
                list.add(RoomFunctionEnum.ROOM_SOUND);
                list.add(RoomFunctionEnum.ROOM_SHARE);
//                list.add(RoomFunctionEnum.ROOM_PLANT_BEAN);
                break;
            default://举报房间  最小化  退出 -- 默认
                list.add(RoomFunctionEnum.ROOM_REPORT);
                list.add(RoomFunctionEnum.ROOM_MINIMIZE);
                list.add(RoomFunctionEnum.ROOM_SHARE);
                list.add(RoomFunctionEnum.ROOM_QUIT);
                break;
        }

        return list.toArray(new RoomFunctionEnum[list.size()]);
    }

    public RoomFunctionBean getFunctionBean(RoomFunctionEnum type) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        RoomFunctionBean functionBean = new RoomFunctionBean();
        functionBean.setFunctionType(type);
        switch (type) {
            case ROOM_REPORT:
                functionBean.setImgRes(R.mipmap.ic_room_report);
                functionBean.setTitle("举报");
                break;
            case ROOM_PLAY_MUSIC:
                functionBean.setImgRes(R.mipmap.ic_room_play_music);
                functionBean.setTitle("音乐播放");
                break;
            case ROOM_MINIMIZE:
                functionBean.setImgRes(R.drawable.ic_room_minimize);
                functionBean.setTitle("最小化");
                break;
            case ROOM_QUIT:
                functionBean.setImgRes(R.drawable.ic_quit_room);
                functionBean.setTitle("退出");
                break;
            case ROOM_SETTING:
                functionBean.setImgRes(R.mipmap.ic_room_setting);
                functionBean.setTitle("房间设置");
                break;
            case ROOM_PUBLIC_ENABLE:
                if (roomInfo != null && roomInfo.getPublicChatSwitch() == 1) {
                    functionBean.setImgRes(R.mipmap.icon_room_pub_on);
                    functionBean.setTitle("开启公屏");
                } else {
                    functionBean.setImgRes(R.mipmap.icon_room_pub_off);
                    functionBean.setTitle("关闭公屏");
                }

                break;
            case ROOM_SHARE:
                functionBean.setImgRes(R.mipmap.ic_room_share);
                functionBean.setTitle("分享房间");
                break;
            case ROOM_FEEDBACK:
                functionBean.setImgRes(R.mipmap.ic_room_feedback);
                functionBean.setTitle("问题反馈");
                break;
            case ROOM_RED_PACKET:
                functionBean.setImgRes(R.mipmap.ic_room_send_red_packet);
                functionBean.setTitle("发红包");
                break;

            case ROOM_SPEAK:
                functionBean.setImgRes(R.mipmap.icon_room_speak);
                functionBean.setTitle("发言");
                break;
            case ROOM_REVERSAL:
                functionBean.setImgRes(R.mipmap.icon_room_reversal);
                functionBean.setTitle("翻转");
                break;
            case ROOM_SOUND:
                if (RtcEngineManager.get().isMute()) {
                    functionBean.setImgRes(R.mipmap.icon_room_sound_off);  //icon_room_sound_on
                } else {
                    functionBean.setImgRes(R.mipmap.icon_room_sound_on);
                }

                functionBean.setTitle("静音");
                break;
            case ROOM_MIRRORING:
                functionBean.setImgRes(R.mipmap.icon_room_mirroring_on);//icon_room_mirroring_off
                functionBean.setTitle("镜像开");
                break;
            case ROOM_CPK:
                functionBean.setImgRes(R.mipmap.icon_room_cpk);
                functionBean.setTitle("魅力值PK");
                break;
            case ROOM_PLANT_BEAN:
                boolean isPlantBeanMsgEnabled = (boolean) SpUtils.get(BasicConfig.INSTANCE.getAppContext(), Constants.KEY_PLANT_BEAN_MSG, true);
                functionBean.setImgRes(isPlantBeanMsgEnabled ? R.mipmap.ic_plant_bean_msg_setting_enable : R.mipmap.ic_plant_bean_msg_setting_disable);
                functionBean.setTitle("种豆消息");
                break;
            case ROOM_PIG_FIGHT:
                if (roomInfo != null) {
                    if(roomInfo.getOpenPigFight() == 1){
                        functionBean.setImgRes(R.mipmap.icon_pig_fight_on);
                        functionBean.setTitle("关闭大作战");
                    } else {
                        functionBean.setImgRes(R.mipmap.icon_pig_fight_off);
                        functionBean.setTitle("开启大作战");
                    }
                } else {
                    functionBean.setImgRes(R.mipmap.icon_pig_fight_off);
                    functionBean.setTitle("开启大作战");
                }
                break;
            case ROOM_PRIVATE_CHAT:
                functionBean.setImgRes(R.mipmap.ic_private_chat);
                functionBean.setTitle("私聊");
                int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
                if (unreadCount > 0) {
                    functionBean.setTagEnable(true);
                } else {
                    functionBean.setTagEnable(false);
                }
                break;
            default:
                functionBean.setImgRes(R.drawable.ic_quit_room);
                functionBean.setTitle("退出");
                break;
        }
        return functionBean;
    }
}
