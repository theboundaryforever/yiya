package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.im.custom.bean.RoomHtmlTextAttachment;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.PigFightBean;
import com.tongdaxing.xchat_framework.im.IMReportRoute;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_HTML_FIRST;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_HTML_SECOND_PUB_OFF;

public class PigFightView extends RelativeLayout implements SVGACallback {

    private Context mContext;

    @BindView(R.id.cl_pig_fight_view)
    ConstraintLayout clPigFight;

    @BindView(R.id.svga_pig_fight)
    SVGAImageView svgaPigFight;

    @BindView(R.id.iv_pig)
    ImageView ivPig;

    private PigFightBean pigFightBean = null;

    public PigFightView(Context context) {
        super(context);
        init(context);
    }

    public PigFightView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PigFightView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_pig_fight_view, this, true);
        ButterKnife.bind(this, view);

        svgaPigFight.setCallback(this);
    }

    public void setupView(PigFightBean pigFightBean) {
        this.pigFightBean = pigFightBean;
        int drawableId = getResources().getIdentifier("ic_pig_" + pigFightBean.getWinPigNum(), "mipmap", mContext.getPackageName());
        if (drawableId <= 0) return;
        ivPig.setImageResource(drawableId);
        showSVGAPigFight();
    }

    public void showSVGAPigFight(){
        svgaPigFight.setVisibility(View.VISIBLE);
        SVGAParser parser = new SVGAParser(mContext);
        parser.decodeFromAssets("pigfight.svga", new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                svgaPigFight.setLoops(1);
                svgaPigFight.setImageDrawable(drawable);
                svgaPigFight.setClearsAfterStop(true);
                svgaPigFight.startAnimation();
            }

            @Override
            public void onError() {

            }
        });
    }

    public void startAnimation() {
        clPigFight.setVisibility(VISIBLE);
        ScaleAnimation scaleAnimation = new ScaleAnimation(
                0.6f, 1.0f, 0.6f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f
        );
        scaleAnimation.setDuration(1000);
//        scaleAnimation.setInterpolator(new AnticipateOvershootInterpolator());
        //动画执行完毕后是否停在结束时的状态
//        animation.setFillAfter(true);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                clPigFight.setVisibility(View.GONE);

                RoomHtmlTextAttachment textAttachment = new RoomHtmlTextAttachment(CUSTOM_HTML_FIRST, CUSTOM_HTML_SECOND_PUB_OFF);
                textAttachment.setHtml(pigFightBean.getHtml());
                ChatRoomMessage chatRoomMessage = new ChatRoomMessage();
                chatRoomMessage.setRoute(IMReportRoute.sendMessageReport);
                chatRoomMessage.setAttachment(textAttachment);
                IMNetEaseManager.get().addMessagesImmediately(chatRoomMessage);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        ivPig.startAnimation(scaleAnimation);
    }

    @Override
    public void onFinished() {
        svgaPigFight.setVisibility(View.INVISIBLE);
        if (pigFightBean == null) return;
        clPigFight.setVisibility(View.VISIBLE);
        startAnimation();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onRepeat() {

    }

    @Override
    public void onStep(int i, double v) {

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        svgaPigFight.setCallback(null);
        svgaPigFight.clearAnimation();
    }
}
