package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * 显示赠送礼物消息的view xx赠送给xx
 *
 * @author zeda
 */
public class GiftMsgView extends LinearLayout {

    private TextView nickTv, contentTv, nickTargetTv, giftTv, giftNumberTv;

    private OnViewClickListener onViewClickListener;

    public GiftMsgView(@NonNull Context context) {
        this(context, null);
    }

    public GiftMsgView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GiftMsgView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_gift_msg, this);
        nickTv = findViewById(R.id.tv_nick);
        contentTv = findViewById(R.id.tv_content);
        nickTargetTv = findViewById(R.id.tv_nick_target);
        giftTv = findViewById(R.id.tv_gift);
        giftNumberTv = findViewById(R.id.tv_gift_number);
        setOnClickListener(v -> {
            if (onViewClickListener != null) {
                onViewClickListener.onParentViewClick();
            }
        });
    }

    /**
     * 配置view显示的内容
     *
     * @param isMulti 是否是全麦
     * @param info    礼物信息
     */
    public void setupView(boolean isMulti, GiftReceiveInfo info) {
        GiftInfo giftInfo = CoreManager.getCore(IGiftCore.class).findGiftInfoById(info.getGiftId());
        if (giftInfo != null) {
            String nick = info.getNick();
            if (!TextUtils.isEmpty(nick) && nick.length() > 6) {
                nick = nick.substring(0, 6) + "...";
            }

            nickTv.setText(nick);
            if (!isMulti) {
                contentTv.setText("赠送给");

                String targetNick = info.getTargetNick();
                if (!TextUtils.isEmpty(targetNick) && targetNick.length() > 6) {
                    targetNick = targetNick.substring(0, 6) + "...";
                }
                nickTargetTv.setVisibility(View.VISIBLE);
                nickTargetTv.setText(targetNick);

                nickTargetTv.setOnClickListener(v -> {
                    if (onViewClickListener != null) {
                        onViewClickListener.onNickNameViewClick(info.getTargetUid());
                    }
                });
            } else {
                contentTv.setText("全麦送出");
                nickTargetTv.setVisibility(View.GONE);
            }
            giftTv.setText(giftInfo.getGiftName());
            giftNumberTv.setText("X" + info.getGiftNum());

            nickTv.setOnClickListener(v -> {
                if (onViewClickListener != null) {
                    onViewClickListener.onNickNameViewClick(info.getUid());
                }
            });
        }
    }

    public void setOnViewClickListener(OnViewClickListener listener) {
        this.onViewClickListener = listener;
    }

    public interface OnViewClickListener {
        void onNickNameViewClick(long clickUserUid);

        void onParentViewClick();
    }
}
