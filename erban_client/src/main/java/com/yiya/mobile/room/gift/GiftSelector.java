package com.yiya.mobile.room.gift;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opensource.svgaplayer.SVGAImageView;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.gift.IGiftCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.bean.ComboGift;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.room.gift.adapter.GiftAvatarAdapter;
import com.yiya.mobile.room.gift.adapter.GiftAvatarAdapterNew;
import com.yiya.mobile.room.gift.fragment.GiftFragment;
import com.yiya.mobile.ui.me.wallet.activity.MyWalletActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.zyyoona7.lib.EasyPopup;
import com.zyyoona7.lib.HorizontalGravity;
import com.zyyoona7.lib.VerticalGravity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GiftSelector extends RelativeLayout implements View.OnClickListener,
        GiftAvatarAdapter.OnItemSelectedListener, GiftAvatarAdapterNew.OnCancelAllMicSelectListener, RadioGroup.OnCheckedChangeListener, GiftFragment.OnItemClickListener {

    private static final String TAG = "giftdialog";
    public static final int NOTE_GIFT_ID = 1;//音符礼物的id
    public static final int ROWS = 2;
    public static final int COLUMNS = 4;
    public static final int NORMALPAGE = 1;// 普通礼物页
    //    public static final int MAGICPAGE = 1;// 魔法礼物页
//    public static final int LUCKYPAGE = 2;// 幸运礼物页
    public static final int PACKETPAGE = 0;// 背包礼物页

    private Context context;
    @BindView(R.id.avatar_list)
    RecyclerView avatarList;
    private GiftAvatarAdapterNew avatarListAdapter;
    private GiftInfo currentGiftInfo;
    private OnGiftSelectotBtnClickListener giftSelectorBtnClickListener;
    @BindView(R.id.text_gold)
    TextView goldText;
    private EasyPopup giftNumberEasyPopup;
    @BindView(R.id.gift_number_text)
    TextView giftNumberText;
    private int giftNumber = 1;
    private long uid;
    private List<MicMemberInfo> micMemberInfos;
    private MicMemberInfo defaultMicMemberInfo;
    @BindView(R.id.gift_number_layout)
    View giftNumLayout;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.rl_all_mic)
    RelativeLayout allMicHead;
    @BindView(R.id.cb_gift_all)
    TextView cbAllMic;

    private boolean singlePeople;
    private int currentPage = NORMALPAGE;//默认礼物页
    @BindView(R.id.rl_gift_container)
    RelativeLayout rlGiftContainer;
    @BindView(R.id.gift_dialog_header)
    RelativeLayout gift_dialog_header;

    @BindView(R.id.gift_man_text)
    TextView giftManText;

    @BindView(R.id.gift_dialog_amount_layout)
    RelativeLayout gift_dialog_amount_layout;
    @BindView(R.id.llGiftTimeTick)
    LinearLayout llGiftTimeTick;
    @BindView(R.id.tvTimeTick)
    TextView tvTimeTick;

    @BindView(R.id.rl_dialog_bottom_gift)
    RelativeLayout rl_dialog_bottom_gift;

    @BindView(R.id.ll_dialog_gift_layout)
    LinearLayout ll_dialog_gift_layout;
    @BindView(R.id.rg_gift_indicator)
    RadioGroup rgIndex;

    @BindView(R.id.rlGiftTip)
    RelativeLayout rlGiftTip;
    @BindView(R.id.tvGiftTip)
    TextView tvGiftTip;
    @BindView(R.id.svga_first_recharge)
    SVGAImageView svgaFirstRecharge;

    private long comboId = 0;
    private long comboCount = 1;
    private long comboFrequencyCount = 1;
    private long comboGiftId = 0;
    private long giftIdCur = 0;
    private ComboGift comboGift = new ComboGift(0, 1, 0);

    private List<Fragment> mGiftFragmentList = new ArrayList<>();

    @BindView(R.id.gift_dialog_info_text)
    ImageView userInfoText;
    private BaseIndicatorStateAdapter mGiftFragmentAdapter;

    public void setSinglePeople(boolean singlePeople) {
        this.singlePeople = singlePeople;
    }

    public GiftSelector(Context context) {
        super(context);
        init(context);
    }

    public GiftSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GiftSelector(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void setGiftSelectotBtnClickListener(OnGiftSelectotBtnClickListener giftDialogBtnClickListener) {
        this.giftSelectorBtnClickListener = giftDialogBtnClickListener;
    }

    public void setupView(long uid) {
        allMicSelect(false);
        this.uid = uid;
        refreshDate();
    }

    public void setupView(SparseArray<RoomQueueInfo> mMicQueueMemberMap, long uid) {
        allMicSelect(false);
        this.uid = 0;

        List<MicMemberInfo> micMemberInfos = new ArrayList<>();
        if (!checkHasOwner(mMicQueueMemberMap)) {// 房主
            UserInfo roomOwner = AvRoomDataManager.get().getRoomOwnerUserInfo();
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            micMemberInfo.setRoomOwnner(true);
            micMemberInfo.setMicPosition(-1);
            if (roomOwner != null) {
                micMemberInfo.setNick(roomOwner.getNick());
                micMemberInfo.setAvatar(roomOwner.getAvatar());
                micMemberInfo.setUid(roomOwner.getUid());
            } else {
                IMChatRoomMember roomMember = AvRoomDataManager.get().getRoomOwnerDefaultMemberInfo();
                micMemberInfo.setNick(roomMember.getNick());
                micMemberInfo.setAvatar(roomMember.getAvatar());
                micMemberInfo.setUid(Long.valueOf(roomMember.getAccount()));
            }
            micMemberInfos.add(micMemberInfo);
        }
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            MicMemberInfo micMemberInfo = new MicMemberInfo();
            IMChatRoomMember mChatRoomMember = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i)).mChatRoomMember;
            if (mChatRoomMember == null) {
                continue;
            }
            // 合法判断
            String account = mChatRoomMember.getAccount();
            LogUtils.d("checkHasOwner", account + "   dd");
            if (TextUtils.isEmpty(account) ||
                    TextUtils.isEmpty(mChatRoomMember.getNick()) ||
                    TextUtils.isEmpty(mChatRoomMember.getAvatar())) {
                continue;
            }
            // 排除自己\
            if (AvRoomDataManager.get().isOwner(account)) {
                continue;
            }
            // 设置默认人员
            if (uid != 0 && String.valueOf(uid).equals(account)) {
                this.defaultMicMemberInfo = micMemberInfo;
            }
            // 设置房主
            if (AvRoomDataManager.get().isRoomOwner(account)) {
                micMemberInfo.setRoomOwnner(true);
            }
            micMemberInfo.setNick(mChatRoomMember.getNick());
            micMemberInfo.setAvatar(mChatRoomMember.getAvatar());
            micMemberInfo.setMicPosition(mMicQueueMemberMap.keyAt(i));
            micMemberInfo.setUid(JavaUtil.str2long(account));
            micMemberInfos.add(micMemberInfo);
        }
        this.micMemberInfos = micMemberInfos;

        refreshDate();
    }

    private boolean checkHasOwner(SparseArray<RoomQueueInfo> mMicQueueMemberMap) {
        if (AvRoomDataManager.get().getRoomInfo() == null) {
            return true;
        }
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(mMicQueueMemberMap.keyAt(i));
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                if ((AvRoomDataManager.get().getRoomInfo().getUid() + "").equals(account)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void init(Context context) {
        CoreManager.addClient(this);
        this.context = context;
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_yiya_bottom_gift, this, true);
        ButterKnife.bind(this, view);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        avatarList.setLayoutManager(mLayoutManager);
        avatarList.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.left = 10;
                outRect.right = 10;
            }
        });

        RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().getRoomInfo();
        if (AvRoomDataManager.get().getRoomInfo() == null) {
            return;
        } else {
            if (mCurrentRoomInfo.getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE) {
                gift_dialog_header.setVisibility(View.GONE);
                userInfoText.setVisibility(View.GONE);
            } else {
                gift_dialog_header.setVisibility(View.VISIBLE);
                userInfoText.setVisibility(View.VISIBLE);
            }
        }

        if (singlePeople) {
            userInfoText.setVisibility(View.INVISIBLE);
        }

        rgIndex.setOnCheckedChangeListener(this);
        rgIndex.check(R.id.rb_gift_tab);

        initViewPager();
        initEasyPop();

        if (micMemberInfos == null) {
            micMemberInfos = new ArrayList<>();
        }
        //是否符合首充优惠
        IMChatRoomMember ownerMember = AvRoomDataManager.get().getMOwnerMember();
        if (ownerMember != null) {
            if (ownerMember.is_first_charge()) {
                svgaFirstRecharge.setVisibility(VISIBLE);
                svgaFirstRecharge.setOnClickListener(v ->
                        //跳转首充特惠H5
                        CommonWebViewActivity.start(getContext(), BaseUrl.FIRST_CHARGE));
            }
        }
    }

    private void initViewPager() {
        //背包
        mGiftFragmentList.add(getGiftFragment(PACKETPAGE));
        //普通
        mGiftFragmentList.add(getGiftFragment(NORMALPAGE));
        //魔法
//        mGiftFragmentList.add(getGiftFragment(MAGICPAGE));
        //幸运
//        mGiftFragmentList.add(getGiftFragment(LUCKYPAGE));

        mGiftFragmentAdapter = new BaseIndicatorStateAdapter(((FragmentActivity) getContext()).getSupportFragmentManager(), mGiftFragmentList);
        mViewPager.setAdapter(mGiftFragmentAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i == NORMALPAGE) {
                    currentPage = NORMALPAGE;
                    rgIndex.check(R.id.rb_gift_tab);
                }
//                else if (i == LUCKYPAGE) {
//                    currentPage = LUCKYPAGE;
//                    rgIndex.check(R.id.rb_gift_lucky_tab);
//                } else if (i == MAGICPAGE) {
//                    currentPage = MAGICPAGE;
//                    rgIndex.check(R.id.rb_gift_magic_tab);
//                }
                else if (i == PACKETPAGE) {
                    currentPage = PACKETPAGE;
                    rgIndex.check(R.id.rb_gift_pack_tab);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_gift_tab:
                currentPage = NORMALPAGE;
                break;
            case R.id.rb_gift_pack_tab:
                currentPage = PACKETPAGE;
                break;
//            case R.id.rb_gift_magic_tab:
//                currentPage = MAGICPAGE;
//                break;
//            case R.id.rb_gift_lucky_tab:
//                currentPage = LUCKYPAGE;
//                break;
            default:
                break;
        }
        mViewPager.setCurrentItem(currentPage, true);
        refreshGiftInfo();
    }

    @NotNull
    private GiftFragment getGiftFragment(int type) {
        GiftFragment normalFragment = new GiftFragment();
        Bundle normalBundle = new Bundle();
        normalBundle.putInt("viewId", R.layout.fragment_gift_normal);
        normalBundle.putInt("type", type);
        normalFragment.setArguments(normalBundle);
        normalFragment.setItemClickListener(this);
        return normalFragment;
    }

    private GiftFragment getCurrentFragment() {
        Fragment fragment = null;
        if (ListUtils.isListEmpty(mGiftFragmentList)) {
            return null;
        }
        switch (currentPage) {
            case NORMALPAGE:
                fragment = mGiftFragmentList.get(NORMALPAGE);
                break;
//            case LUCKYPAGE:
//                fragment = mGiftFragmentList.get(LUCKYPAGE);
//                break;
//            case MAGICPAGE:
//                fragment = mGiftFragmentList.get(MAGICPAGE);
//                break;
            case PACKETPAGE:
                fragment = mGiftFragmentList.get(PACKETPAGE);
                break;
        }

        return (GiftFragment) fragment;
    }


    private void refreshGiftInfo() {
        GiftFragment fragment = getCurrentFragment();
        if (fragment == null || fragment.getAdapter() == null) {
            return;
        }
        GiftInfo indexGift = fragment.getAdapter().getIndexGift();
        if (indexGift != null && indexGift.isLuckyGift()) {
            tvGiftTip.setText(indexGift.getLuckyGiftTips());
//            rlGiftTip.setVisibility(VISIBLE);
        } else {
//            rlGiftTip.setVisibility(GONE);
        }
    }

    private void refreshDate() {
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();

        refreshGiftInfos();
        refreshGold();
        refreshMicPosition();
    }

    // 更新礼物信息
    public void refreshGiftInfos() {
        if (mGiftFragmentList.size() > 0) {
            for (Fragment fragment : mGiftFragmentList) {
                if (fragment instanceof GiftFragment && fragment.isAdded()) {
                    ((GiftFragment) fragment).updateUI();
                }
            }
            GiftFragment giftFragment = (GiftFragment) mGiftFragmentList.get(currentPage);
            if (giftFragment.getNormalGiftInfos() != null && giftFragment.getNormalGiftInfos().size() > 0) {
                currentGiftInfo = giftFragment.getNormalGiftInfos().get(0);
            }
        }
    }

    // 更新金币
    public void refreshGold() {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        CoreManager.getCore(IPayCore.class).getWalletInfo(uid);
    }

    // 更新麦位信息（语聊房专有）
    private void refreshMicPosition() {
        if (this.uid > 0) {
            micMemberInfos.clear();
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(this.uid, false);
            if (userInfo == null) {
                userInfo = new UserInfo();
            }
            defaultMicMemberInfo = new MicMemberInfo();
            defaultMicMemberInfo.setAvatar(userInfo.getAvatar());
            defaultMicMemberInfo.setNick(userInfo.getNick());
            defaultMicMemberInfo.setUid(this.uid);
            micMemberInfos.add(defaultMicMemberInfo);
        } else {
            if (micMemberInfos != null && micMemberInfos.size() > 0) {
                avatarListAdapter = new GiftAvatarAdapterNew();
                avatarList.setAdapter(avatarListAdapter);
                avatarListAdapter.setOnCancelAllMicSelectListener(this);
                if (defaultMicMemberInfo != null) {
                    int position = micMemberInfos.indexOf(defaultMicMemberInfo);
                    if (position >= 0) {
                        avatarListAdapter.setSelectCount(1);
                        micMemberInfos.get(position).setSelect(true);
                        giftManText.setText(micMemberInfos.get(position).getNick());
                    } else {
                        Log.e(TAG, "init: default mic member info not in mic member info list");
                    }
                } else {
                    avatarListAdapter.setSelectCount(1);
                    micMemberInfos.get(0).setSelect(true);
                    giftManText.setText(micMemberInfos.get(0).getNick());
                }
                avatarListAdapter.setNewData(micMemberInfos);
            } else {
                userInfoText.setVisibility(View.GONE);
            }
        }
    }

    private void initEasyPop() {
        giftNumberEasyPopup = new EasyPopup(getContext())
                .setContentView(R.layout.dialog_gift_number)
                .setFocusAndOutsideEnable(true)
                .createPopup();

        giftNumberEasyPopup.getView(R.id.number_1).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_10).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_99).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_66).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_188).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_520).setOnClickListener(this);
        giftNumberEasyPopup.getView(R.id.number_1314).setOnClickListener(this);
    }

    private void sendVideoGift() {

    }

    private void sendGift() {
        GiftFragment fragment = getCurrentFragment();
        currentGiftInfo = fragment == null ? null : fragment.getAdapter().getIndexGift();
        if (currentGiftInfo == null) {
            SingleToastUtil.showToast("请选择您需要赠送的礼物哦！");
            return;
        }
        if (currentGiftInfo.getGiftType() == 3 && currentGiftInfo.getUserGiftPurseNum() < giftNumber) {//神秘礼物
            SingleToastUtil.showToast("您的神秘礼物数量不够哦！");
            return;
        }
        if (giftSelectorBtnClickListener != null) {//gift_dialog_amount_layout
            if (uid > 0) {// （语音房：非房主 非麦上，视频房走这里）
                if (AvRoomDataManager.get().getRoomInfo() != null && AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE) {
                    giftIdCur = currentGiftInfo.getGiftId(); // 用于判断与上一次发送的giftId是否为同一个
                    startCount();
                    giftSelectorBtnClickListener.onSendGiftBtnClick(currentGiftInfo, null, uid, giftNumber, comboId, comboCount, comboFrequencyCount);
                } else {
                    giftSelectorBtnClickListener.onSendGiftBtnClick(currentGiftInfo, null, uid, giftNumber);
                }
            } else if (avatarListAdapter != null && !ListUtils.isListEmpty(avatarListAdapter.getData())) {
                //其他人
                if (allMicHead != null && allMicHead.getVisibility() == View.VISIBLE && cbAllMic.isSelected()) {
                    // 勾选全麦
                    giftSelectorBtnClickListener.onSendGiftBtnClick(currentGiftInfo, micMemberInfos, 0, giftNumber);
                } else {
                    // 未勾选全麦
                    boolean noSend = true;
                    for (int i = 0; i < micMemberInfos.size(); i++) {
                        if (micMemberInfos.get(i).isSelect()) {
                            noSend = false;
                            giftSelectorBtnClickListener.onSendGiftBtnClick(currentGiftInfo, null, micMemberInfos.get(i).getUid(), giftNumber);
                        }
                    }

                    if (noSend) {
                        if (context instanceof BaseActivity) {
                            ((BaseActivity) context).toast("暂无成员在麦上");
                        } else if (context instanceof BaseMvpActivity) {
                            ((BaseMvpActivity) context).toast("暂无成员在麦上");
                        }
//                        dismiss();
                    }
                }
            } else {
                if (context instanceof BaseActivity) {
                    ((BaseActivity) context).toast("暂无成员在麦上");
                } else if (context instanceof BaseMvpActivity) {
                    ((BaseMvpActivity) context).toast("暂无成员在麦上");
                }
//                dismiss();
            }
        }
    }

    private void updateNumber(int number) {
        giftNumber = number;
        giftNumberText.setText(giftNumber + "");
        giftNumberEasyPopup.dismiss();
    }

    private void displayUserInfo() {
        long displayUid;
        if (uid > 0) {
            displayUid = uid;
        } else {
            if (defaultMicMemberInfo == null) {
                displayUid = micMemberInfos.get(0).getUid();
            } else {
                displayUid = defaultMicMemberInfo.getUid();
            }
        }

        if (giftSelectorBtnClickListener != null) {
            giftSelectorBtnClickListener.onUserInfoClick(displayUid);
        }
    }

    private void showGiftAvatarListEasyPopup() {
        avatarList.setVisibility(View.VISIBLE);
        if (allMicHead != null) {
            if (singlePeople) {
                allMicHead.setVisibility(View.GONE);
            } else {
                if (avatarListAdapter != null && !ListUtils.isListEmpty(avatarListAdapter.getData())) {
                    if (avatarListAdapter.getData().size() == 1) {
                        allMicHead.setVisibility(View.GONE);
                    }
                } else {
                    allMicHead.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void showGiftNumberEasyPopup() {
        giftNumberEasyPopup.showAtAnchorView(giftNumLayout, VerticalGravity.ABOVE, HorizontalGravity.CENTER, 50, 10);
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onGetWalletInfo(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldText.setText(getContext().getString(R.string.gold_num_text, walletInfo.getGoldNum()));
        }
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void refreshFreeGift() {
        if (ListUtils.isListEmpty(mGiftFragmentList)) {
            return;
        }
        List<GiftInfo> gifts = null;
        if (currentPage == NORMALPAGE) {
//            normalGiftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(2);
//            normalGiftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType2And4();
//            adapter.setGiftInfoList(normalGiftInfos);

//            ((GiftFragment) mGiftFragmentList.get(0)).updateUI();

        } else if (currentPage == PACKETPAGE) {//更新礼物数量
            gifts = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(3);
        }
//        else if (currentPage == MAGICPAGE) {//更新魔法礼物数量
//            gifts = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(IGiftCore.MAGIC_LIST);
//        } else if (currentPage == LUCKYPAGE) {//更新幸运礼物数量
//            gifts = CoreManager.getCore(IGiftCore.class).getGiftInfosByType(IGiftCore.LUCKY_LIST);
//        }
        updateGiftPage(gifts, currentPage);
    }

    private void updateGiftPage(List<GiftInfo> giftList, int listIndex) {
        if (listIndex != 0) {
            if (ListUtils.isListEmpty(giftList)) {
                ((GiftFragment) mGiftFragmentList.get(listIndex)).getAdapter().setIndex(0);
            } else {
                List<GiftInfo> tmpList = ((GiftFragment) mGiftFragmentList.get(listIndex)).getNormalGiftInfos();
                if (tmpList != null && giftList.size() < tmpList.size()) {
                    ((GiftFragment) mGiftFragmentList.get(listIndex)).getAdapter().setIndex(0);
                }
            }
        }
        ((GiftFragment) mGiftFragmentList.get(listIndex)).updateUI();
    }


    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftPastDue() {
        ((BaseActivity) getContext()).toast("该礼物已过期");
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onGiftMysteryNotEnough() {
        SingleToastUtil.showToast("您的神秘礼物数量不够哦！");
    }

    @CoreEvent(coreClientClass = IGiftCoreClient.class)
    public void onSendGiftFail(int code, String message) {
        SingleToastUtil.showToast(message);
    }

    @Override
    @OnClick({R.id.llGiftTimeTick, R.id.ll_btn_recharge, R.id.btn_send, R.id.gift_number_layout,
            R.id.gift_dialog_info_text, R.id.rl_all_mic, R.id.cb_gift_all, R.id.gift_dialog_to_man_layout
    })
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_btn_recharge:
                MyWalletActivity.start(getContext(),1);
                hide();
                break;
            case R.id.btn_send:
                sendGift();
                break;
            case R.id.rl_dialog_bottom_gift:
                hide();
                break;
            case R.id.llGiftTimeTick:
                sendGift();
                break;
            case R.id.number_1:
                updateNumber(1);
                break;
            case R.id.number_10:
                updateNumber(10);
                break;
            case R.id.number_99:
                updateNumber(99);
                break;
            case R.id.number_66:
                updateNumber(66);
                break;
            case R.id.number_188:
                updateNumber(188);
                break;
            case R.id.number_520:
                updateNumber(520);
                break;
            case R.id.number_1314:
                updateNumber(1314);
                break;
            case R.id.gift_number_layout:
                showGiftNumberEasyPopup();
                break;
            case R.id.gift_dialog_to_man_layout:
                showGiftAvatarListEasyPopup();
                break;
            case R.id.gift_dialog_info_text:
                UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomGiftUserInfo());
                displayUserInfo();
                break;
            case R.id.rl_all_mic:
                allMicSelect(!cbAllMic.isSelected());
                break;
            default:
        }
    }

    /**
     * 全麦选择
     *
     * @param shouldSelect true 全选 false 取消全选
     */
    private void allMicSelect(boolean shouldSelect) {
        allMicHead.setEnabled(false);
        cbAllMic.setSelected(shouldSelect);
//        cbAllMic.setText(!shouldSelect ? "全" : "");
        if (avatarListAdapter != null && !ListUtils.isListEmpty(avatarListAdapter.getData())) {
            for (int i = 0; i < avatarListAdapter.getData().size(); i++) {
                avatarListAdapter.getData().get(i).setSelect(shouldSelect);
                avatarListAdapter.notifyDataSetChanged();
            }
            avatarListAdapter.setAllSelect(shouldSelect);
        }
        allMicHead.setEnabled(true);
    }

    @Override
    public void onItemClick(int type, int position, GiftInfo info) {
        currentGiftInfo = info;
    }

    @Override
    public void onItemSelected(int position) {

    }

    @Override
    public void onChange(boolean isAllMic, int count) {
        if (isAllMic) {
            cbAllMic.setSelected(true);
//            cbAllMic.setText("");
        } else {
            cbAllMic.setSelected(false);
//            cbAllMic.setText("全");
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        CoreManager.removeClient(this);
    }


    public interface OnGiftSelectotBtnClickListener {
        void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long uid, int number);

        void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long uid, int number, long comboId, long comboCount, long comboFrequencyCount);

        void onUserInfoClick(long uid);
    }

    private void startCount() {
        if (ll_dialog_gift_layout.getVisibility() != INVISIBLE) {
            hide();
            llGiftTimeTick.setVisibility(View.VISIBLE);
        } else {
            if (llGiftTimeTick.getVisibility() != View.VISIBLE) {
                llGiftTimeTick.setVisibility(View.VISIBLE);
            }
        }

        timer.cancel();
        timer.start();

        if (comboId == 0 || giftIdCur != comboGiftId) {
            comboGiftId = giftIdCur;
            comboCount = giftNumber;
            comboFrequencyCount = 1;
            comboId = System.currentTimeMillis();
        } else {
            comboCount += giftNumber;
            comboFrequencyCount += 1;
        }
    }

    private void cancelCount() {
        llGiftTimeTick.setVisibility(View.INVISIBLE);
        gift_dialog_amount_layout.setVisibility(View.VISIBLE);
        timer.cancel();
        comboId = 0;
        comboCount = 1;
        giftIdCur = 0;
        comboGiftId = 0;
        comboFrequencyCount = 1;
    }

    CountDownTimer timer = new CountDownTimer(5000, 100) {
        @Override
        public void onTick(long millisUntilFinished) {
            tvTimeTick.setText(String.valueOf(millisUntilFinished / 100));
        }

        @Override
        public void onFinish() {
            cancelCount();
        }
    }.start();

    public void show() {
        ll_dialog_gift_layout.setVisibility(VISIBLE);
        rl_dialog_bottom_gift.setOnClickListener(this);
        cancelCount();
    }

    public void hide() {
        ll_dialog_gift_layout.setVisibility(INVISIBLE);
        rl_dialog_bottom_gift.setClickable(false);
    }

    public boolean isShowing() {
        return ll_dialog_gift_layout.getVisibility() == VISIBLE;
    }


}
