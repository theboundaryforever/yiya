package com.yiya.mobile.room.presenter;

import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.DiscountGiftSetting;
import com.tongdaxing.xchat_core.room.bean.LianMicroStatusInfo;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.config.SpEvent;
import com.tongdaxing.xchat_framework.util.util.SpUtils;

import java.util.List;

import static com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager.DEFAULT_CODE_ERROR;
import static com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager.DEFAULT_MSG_ERROR;

/**
 * 视频房间页面的presenter
 */
public class VideoRoomDetailPresenter extends BaseRoomDetailPresenter<IVideoRoomDetailView> {

    private LianMicroStatusInfo lianMicroStatusInfo;

    public VideoRoomDetailPresenter() {
        super();
    }

    /**
     * 邀请上麦
     *
     * @param roomUserInfos 邀请的用户
     */
    public void inviteUpMicro(List<RoomConsumeInfo> roomUserInfos, boolean isNeedPay) {
        model.inviteUpMicroOnVideoRoom(roomUserInfos, isNeedPay, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
                getMvpView().toast("连麦邀请发送成功");
            }

            @Override
            public void onFailure(int code, String msg) {
                getMvpView().toast(msg);
            }
        });
    }

    /**
     * 邀请进房
     *
     * @param roomUserInfos 邀请的用户
     */
    public void inviteEnterRoom(List<RoomConsumeInfo> roomUserInfos) {
        model.inviteEnterRoom(roomUserInfos, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
                getMvpView().toast("进房邀请发送成功");
            }

            @Override
            public void onFailure(int code, String msg) {
                getMvpView().toast(msg);
            }
        });
    }

    /**
     * 获取头部成员头像
     */
    public void getRoomMembers(int length) {
        model.getRoomMembers(0, length, new CallBack<List<IMChatRoomMember>>() {
            @Override
            public void onSuccess(List<IMChatRoomMember> paramT) {
                getMvpView().refreshOnlineView(paramT);
            }
        });
    }

    /**
     * 设置是否已申请连麦
     */
    public void setApplyLianMicro(boolean isApplyUpMicro) {
        AvRoomDataManager.get().setApplyLianMicro(isApplyUpMicro);
    }

    /**
     * 是否已经申请连麦
     */
    public boolean isApplyLianMicro() {
        return AvRoomDataManager.get().isApplyLianMicro();
    }

    /**
     * 设置是否已经在连麦
     */
    public void setInLianMicro(boolean isInLianMicro) {
        AvRoomDataManager.get().setInLianMicro(isInLianMicro);
    }

    /**
     * 是否已经在连麦
     */
    public boolean isInLianMicro() {
        return AvRoomDataManager.get().isInLianMicro();
    }

    /**
     * 设置连麦功能是否已经使用过（点击过）
     */
    public void setLianMicroUsed(boolean isLianMicroUsed) {
        SpUtils.put(BasicConfig.INSTANCE.getAppContext(), SpEvent.isLianMicroUsed, isLianMicroUsed);
    }

    /**
     * 连麦功能是否已经使用过（点击过）
     */
    public boolean isLianMicroUsed() {
        return (boolean) SpUtils.get(BasicConfig.INSTANCE.getAppContext(), SpEvent.isLianMicroUsed, false);
    }

    /**
     * 是否房主
     */
    public boolean isRoomOwner() {
        return AvRoomDataManager.get().isRoomOwner();
    }

    /**
     * 获取房间连麦设置 + 我自己的连麦状态
     *
     * @param isShowLianMicroApplyDialogAndTip 是否显示弹框和提示
     */
    public void getLianMicroStatus(boolean isShowLianMicroApplyDialogAndTip) {
        model.getLianMicroStatus(new HttpRequestCallBack<LianMicroStatusInfo>() {
            @Override
            public void onSuccess(String message, LianMicroStatusInfo statusInfo) {
                if (getMvpView() != null) {
                    if (statusInfo != null) {
                        lianMicroStatusInfo = statusInfo;
                        switch (statusInfo.getStatus()) {
                            case 0://主播关闭连麦申请
                            case 1://未连接（未申请）
                                setApplyLianMicro(false);
                            case 2://连接中（已申请）
                                if (statusInfo.getStatus() == LianMicroStatusInfo.STATUS_CONNECTING) {
                                    setApplyLianMicro(true);
                                }
                                setInLianMicro(false);
                                if (isShowLianMicroApplyDialogAndTip) {
                                    getMvpView().showLianMicroApplyDialog(statusInfo);
                                }
                                break;
                            case 3:
                                setApplyLianMicro(false);
                                setInLianMicro(true);
                                if (isShowLianMicroApplyDialogAndTip) {
                                    getMvpView().dismissLianMicroApplyDialog();
                                    getMvpView().toast("正在连线互动中");
                                }
                                break;
                        }
                        getMvpView().refreshLianMicroViewStatus();
                    } else {
                        onFailure(DEFAULT_CODE_ERROR, DEFAULT_MSG_ERROR);
                    }
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 申请连麦
     *
     * @param type 连麦类型 0语音，1视频
     */
    public void applyLianMicro(int type) {
        setMicroType(type);
        model.applyLianMicro(type, new HttpRequestCallBack<Object>() {

            @Override
            public void onFinish() {
                if (getMvpView() != null) {
                    getMvpView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(String message, Object response) {
                if (getMvpView() != null) {
                    setApplyLianMicro(true);
                    getMvpView().refreshLianMicroViewStatus();
                    if (lianMicroStatusInfo == null) {
                        lianMicroStatusInfo = new LianMicroStatusInfo();
                    }
                    lianMicroStatusInfo.setStatus(2);
                    lianMicroStatusInfo.setType(type);
                    getMvpView().showLianMicroApplyDialog(lianMicroStatusInfo);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                    if (lianMicroStatusInfo == null) {
                        lianMicroStatusInfo = new LianMicroStatusInfo();
                    }
                    lianMicroStatusInfo.setStatus(0);
                    getMvpView().showLianMicroApplyDialog(lianMicroStatusInfo);
                }
            }
        });
    }

    /**
     * 设置连麦类型
     *
     * @param type
     */
    private void setMicroType(int type) {
        AvRoomDataManager.get().setMicroType(type);
    }

    /**
     * 取消连麦申请
     */
    public void cancelLianMicroApply() {
        model.cancelLianMicroApply(new HttpRequestCallBack<Object>() {
            @Override
            public void onFinish() {
                if (getMvpView() != null) {
                    getMvpView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(String message, Object response) {
                setApplyLianMicro(false);
                if (getMvpView() != null) {
                    getMvpView().dismissLianMicroApplyDialog();
                    getMvpView().refreshLianMicroViewStatus();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 获取连麦列表
     */
    public void getMicApplyList() {
        model.getMicApplyList(6, new HttpRequestCallBack<RoomMicroApplyInfo>() {

            @Override
            public void onSuccess(String message, RoomMicroApplyInfo roomMicroApplyInfo) {
                //刷新底部micro按钮
                if (getMvpView() != null && roomMicroApplyInfo != null) {
                    getMvpView().refreshLianMicroViewStatus(roomMicroApplyInfo);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 获取特惠礼物设置信息
     */
    public void getDiscountGiftSetting() {
        model.getDiscountGiftSetting(new HttpRequestCallBack<DiscountGiftSetting>() {

            @Override
            public void onSuccess(String message, DiscountGiftSetting discountGiftSetting) {
                //刷新特惠礼物信息
                if (getMvpView() != null && discountGiftSetting != null) {
                    getMvpView().refreshDiscountData(discountGiftSetting);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 释放房间数据
     */
    public void releaseRoomData() {
        AvRoomDataManager.get().release();
    }
}
