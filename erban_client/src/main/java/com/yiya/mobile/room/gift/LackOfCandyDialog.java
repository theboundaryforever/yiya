package com.yiya.mobile.room.gift;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.R;

import butterknife.OnClick;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/3.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class LackOfCandyDialog extends BaseTransparentDialogFragment {

    public static LackOfCandyDialog newInstance() {
        return new LackOfCandyDialog();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_lack_of_candy;
    }

    @OnClick(R.id.tv_mission)
    public void onViewClicked() {
        //跳转任务中心
        CommonWebViewActivity.start(getContext(), BaseUrl.MISSION_CENTER);
        dismiss();
    }
}
