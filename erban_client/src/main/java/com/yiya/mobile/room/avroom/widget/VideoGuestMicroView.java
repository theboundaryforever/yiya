package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.yiya.mobile.ui.widget.DragView;
import com.yiya.mobile.ui.widget.floatView.IFloatView;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.blur.BlurTransformation;
import com.juxiao.library_utils.DisplayUtils;
import com.netease.nim.uikit.glide.GlideApp;
import com.opensource.svgaplayer.SVGAImageView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;

import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * ProjectName:
 * Description: 视频嘉宾视图
 * Created by BlackWhirlwind on 2019/6/20.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class VideoGuestMicroView extends DragView implements IFloatView {

    private ImageView avatarIv;
    private ImageView avatarBgIv;
    private TextView nameTv;
    private TextView nameTipsTv;
    private View downMicBtn;
    public FrameLayout guestFl;
    public SurfaceView guestView;
    private SVGAImageView svgaIv;
    private ImageView nameBgIv;
    private Observable<RoomQueueInfo> mObservable;
    private Disposable mDisposable;
    private ConstraintLayout bgCl;

    public VideoGuestMicroView(Context context) {
        this(context, null);
    }

    public VideoGuestMicroView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoGuestMicroView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.layout_video_micro_guest, this);
        bgCl = findViewById(R.id.cl_video_micro_guest);
        avatarIv = findViewById(R.id.iv_room_video_micro_guest_avatar);
        avatarBgIv = findViewById(R.id.iv_room_video_micro_guest_avatar_bg);
        nameTv = findViewById(R.id.tv_room_video_micro_guest_name);
        nameTipsTv = findViewById(R.id.tv_room_video_micro_guest_name_tips);
        downMicBtn = findViewById(R.id.btn_room_video_micro_guest_down_mic);
        guestFl = findViewById(R.id.fl_guest);
        svgaIv = findViewById(R.id.svga_iv);
        nameBgIv = findViewById(R.id.iv_name_bg);
        downMicBtn.setOnClickListener(v -> {
            if (onClickListener != null) {
                onClickListener.onClick(v);
            }
        });
    }

    public void setupView(RoomQueueInfo roomQueueInfo) {
        IMChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
        //过渡效果
        if (guestView == null) {
            nameTipsTv.setVisibility(VISIBLE);
            nameBgIv.setVisibility(GONE);
            avatarIv.setVisibility(VISIBLE);
            avatarBgIv.setVisibility(VISIBLE);
            svgaIv.setVisibility(VISIBLE);
            avatarIv.setImageResource(R.mipmap.icon_connecting);
            bgCl.setBackgroundResource(R.mipmap.bg_video_micro_guest);
            nameBgIv.setBackgroundResource(R.mipmap.bg_video_micro_guest_name);
            downMicBtn.setBackgroundResource(R.mipmap.bg_video_micro_guest_down_mic);
            //虚化头像背景
            GlideApp.with(getContext()).load(chatRoomMember.getAvatar()).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .dontAnimate().transform(new BlurTransformation(getContext(), 12)).into(avatarBgIv);
            if (AvRoomDataManager.get().isOwner(chatRoomMember.getAccount())) {
                //连麦者自己
                nameTipsTv.setText("您即将与主播进行连麦");
            } else {
                //普通观众/主播
                ImageLoadUtils.loadAvatar(getContext(), chatRoomMember.getAvatar(), avatarIv, true);
                nameTv.setText(chatRoomMember.getNick());
                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) nameTipsTv.getLayoutParams();
                params.setMarginEnd(DisplayUtils.dip2px(getContext(), 5));
                params.setMarginStart(DisplayUtils.dip2px(getContext(), 5));
                nameTipsTv.setText("将与主播进行连麦");
                nameTv.setVisibility(VISIBLE);
            }
            if (!AvRoomDataManager.get().isRoomOwner(roomQueueInfo.mChatRoomMember.getAccount())) {
                mObservable = Observable.just(roomQueueInfo);
            }
        }
        if (roomQueueInfo.isVideoMicroType()) {
            //视频连麦
            if (guestView == null) {
                guestView = RtcEngine.CreateRendererView(getContext());
            }
            String queueAccount = chatRoomMember.getAccount();
            if (AvRoomDataManager.get().isOwner(queueAccount)) {
                RtcEngineManager.get().setupLocalVideo(new VideoCanvas(guestView, VideoCanvas.RENDER_MODE_HIDDEN, 0));
            } else {
                RtcEngineManager.get().setupRemoteVideo(new VideoCanvas(guestView, VideoCanvas.RENDER_MODE_HIDDEN, Integer.valueOf(queueAccount)));
            }
        }
    }

    /**
     * 音视频第一帧
     */
    public void startFirstFrame() {
        if (mObservable == null) {
            return;
        }
        mDisposable = mObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(roomQueueInfo -> {
                    if (roomQueueInfo.isVideoMicroType()) {
                        //视频连麦
                        avatarIv.setVisibility(GONE);
                        avatarBgIv.setVisibility(GONE);
                        svgaIv.setVisibility(GONE);
                        addGuestView();
                        //方形边框
                        bgCl.setBackgroundResource(R.mipmap.bg_video_micro_guest_square);
                        nameBgIv.setBackgroundResource(R.mipmap.bg_video_micro_guest_name_square);
                        downMicBtn.setBackgroundResource(R.mipmap.bg_video_micro_guest_down_mic_square);
                    } else {
                        //语音连麦
                        ImageLoadUtils.loadAvatar(getContext(), roomQueueInfo.mChatRoomMember.getAvatar(), avatarIv, true);
                    }
                    //挂断按钮
                    String account = roomQueueInfo.mChatRoomMember.getAccount();
                    if (AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isOwner(account)) {
                        downMicBtn.setVisibility(VISIBLE);
                    } else {
                        downMicBtn.setVisibility(GONE);
                    }
                    nameBgIv.setVisibility(VISIBLE);
                    nameTipsTv.setVisibility(GONE);
                    nameTv.setText(roomQueueInfo.mChatRoomMember.getNick());
                    nameTv.setVisibility(VISIBLE);
                });
    }

    public void release() {
        guestFl.removeAllViews();
        guestView = null;
        mObservable = null;
        if (mDisposable != null && !mDisposable.isDisposed()) {
            mDisposable.dispose();
            mDisposable = null;
        }
    }

    private void addGuestView() {
        guestView.setZOrderMediaOverlay(true);
        guestView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.transparent));
        if (guestFl.getChildCount() != 0) {
            guestFl.removeAllViews();
        }
        guestFl.addView(guestView);
    }

    private OnClickListener onClickListener;

    public void setOnDownMicClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    /**
     * 嘉宾连麦悬浮窗初始位置
     *
     * @return
     */
    @Override
    public LayoutParams getParams() {
        LayoutParams params = new LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.BOTTOM | Gravity.END;
        params.setMargins(params.leftMargin, params.topMargin, DisplayUtils.dip2px(getContext(), 10f), getResources().getDimensionPixelOffset(R.dimen.video_micro_guest_view_margin_bottom));
        return params;
    }

}
