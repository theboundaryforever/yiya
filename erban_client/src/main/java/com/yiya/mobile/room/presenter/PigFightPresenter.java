package com.yiya.mobile.room.presenter;

import android.text.TextUtils;

import com.yiya.mobile.model.pigfight.PigFightModel;
import com.yiya.mobile.presenter.pigfight.IPigFightView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.room.bean.PigRecordInfo;

import java.util.List;

/**
 * @author zdw
 * @date 2019/8/12
 * @describe 猪猪大作战
 */

public class PigFightPresenter extends AbstractMvpPresenter<IPigFightView> {
    private PigFightModel pigFightModel;

    public PigFightPresenter() {
        this.pigFightModel = new PigFightModel();
    }

    public void startPlay() {
        pigFightModel.startPlay(new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().showPlayFail(msg);
                }
            }
        });
    }

    public void getPigFightRecordList() {

        pigFightModel.getPigFightRecordList(new HttpRequestCallBack<List<PigRecordInfo>>() {

            @Override
            public void onSuccess(String message, List<PigRecordInfo> list) {
                if (getMvpView() != null) {
                    getMvpView().showPigFightRecord(list);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (!TextUtils.isEmpty(msg)) {
                    if (getMvpView() != null) {
                        getMvpView().toast(msg);
                    }
                }
            }
        });
    }
}
