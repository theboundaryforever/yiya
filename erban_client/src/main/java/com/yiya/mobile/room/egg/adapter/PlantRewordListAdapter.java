package com.yiya.mobile.room.egg.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

/**
 * ProjectName:
 * Description:种豆中奖记录adapter
 * Created by BlackWhirlwind on 2019/7/2.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantRewordListAdapter extends BaseQuickAdapter<EggGiftInfo, BaseViewHolder> {

    public PlantRewordListAdapter() {
        super(R.layout.item_plant_bean_record);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, EggGiftInfo info) {
        if (info == null) {
            return;
        }
        baseViewHolder.setText(R.id.tv_gift_name, String.format("%s  x%s", info.getGiftName(), info.getGiftNum()))
                .setText(R.id.tv_gift_date, TimeUtils.getDateTimeString(info.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
        ImageLoadUtils.loadImage(mContext, info.getPicUrl(), baseViewHolder.getView(R.id.img_avatar));
    }
}
