package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tongdaxing.erban.R;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/9
 * 描述        视频房申请上麦的View
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class ApplyMicroView extends RelativeLayout {

    private TextView tvBg, tvApply, tvConsume;
    private OnApplyViewClickListener listener;

    public ApplyMicroView(Context context) {
        this(context, null);
    }

    public ApplyMicroView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ApplyMicroView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_apply_micro, this);
        tvBg = findViewById(R.id.tv_bg);
        tvApply = findViewById(R.id.tv_apply);
        tvConsume = findViewById(R.id.tv_consume);
        setOnClickListener(view -> {
            if (listener != null) {
                listener.onViewClick();
            }
        });
    }

    /**
     * 改变状态
     * @param enable false 已申请上麦  true 未申请上麦
     */
    public void setEnable(boolean enable) {
        setEnabled(enable);
        tvBg.setEnabled(enable);
        tvApply.setText(!enable ? R.string.have_applied : R.string.apply_for_wheat);
        if (!enable) {
            if (tvConsume.getVisibility() == VISIBLE) {
                tvConsume.setVisibility(GONE);
            }
        } else {
            if (tvConsume.getVisibility() == GONE) {
                tvConsume.setVisibility(VISIBLE);
            }
        }
    }

    public void setOnApplyViewClickListener(OnApplyViewClickListener listener) {
        this.listener = listener;
    }

    public interface OnApplyViewClickListener {
        void onViewClick();
    }

}
