package com.yiya.mobile.room.presenter;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyBoxInfo;

import java.util.List;

public interface IRoomDetailView extends IMvpBaseView {

    void refreshMicBtnStatus();

    void refreshGiftSelector();

    void showRechargeTipDialog();

    void startP2PSession(long friendUid);

    default void refreshRoomAttentionStatus(Boolean status) {
    }

    ;

    void showRoomGuideDialog();

    default void onGetActionDialog(List<ActionDialogInfo> actionDialogInfoList) {
    }

    /**
     * 获取活动信息失败
     */
    default void onGetActionDialogError(String error) {
    }

    void showLuckyBoxView(RoomLuckyBoxInfo roomLuckyBoxInfo);

    default void onBarrageCallback(int code, String message, int barrageNum) {
    }

    void showShareDialog();

    void showLackOfCandyDialog();
}