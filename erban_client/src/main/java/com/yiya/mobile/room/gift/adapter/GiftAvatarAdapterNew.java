package com.yiya.mobile.room.gift.adapter;

import android.support.constraint.ConstraintLayout;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.ui.common.widget.CircleImageView;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;

/**
 * Created by chenran on 2017/10/25.
 */

public class GiftAvatarAdapterNew extends BaseQuickAdapter<MicMemberInfo, BaseViewHolder> {
    private OnCancelAllMicSelectListener onCancelAllMicSelectListener;
    private int selectCount = 0;

    public void setAllSelect(boolean isAllMic) {
        this.selectCount = isAllMic? getItemCount():0;
    }

    public GiftAvatarAdapterNew() {
        super(R.layout.list_item_gift_avatar);
    }

    @Override
    protected void convert(BaseViewHolder helper, MicMemberInfo item) {
        ImageView avatar = (CircleImageView) helper.getView(R.id.avatar);
        ConstraintLayout avatarContainer = helper.getView(R.id.avatar_container);
        ImageLoadUtils.loadAvatar(mContext, item.getAvatar(), avatar);
        CheckBox cbSelect = helper.getView(R.id.cb_member_select);
        cbSelect.setText(item.getMicPosition() == -1 ? "房" : String.valueOf(item.getMicPosition() + 1));
        cbSelect.setChecked(item.isSelect());
        helper.getView(R.id.iv_bg).setSelected(item.isSelect());
        avatarContainer.setOnClickListener(v -> {
            if (item.isSelect()) {
                item.setSelect(false);
                selectCount--;
            } else {
                item.setSelect(true);
                selectCount++;
            }
            if (selectCount == getItemCount()) {//选中之后如果满人数则全选
                if (onCancelAllMicSelectListener != null)
                    onCancelAllMicSelectListener.onChange(true, selectCount);
            } else {
                if (onCancelAllMicSelectListener != null)//选中之前如果满人数则取消全选
                    onCancelAllMicSelectListener.onChange(false, selectCount);
            }
            notifyDataSetChanged();
        });
    }

    public interface OnCancelAllMicSelectListener {
        void onChange(boolean isAllMic, int count);
    }

    public void setOnCancelAllMicSelectListener(OnCancelAllMicSelectListener onCancelAllMicSelectListener) {
        this.onCancelAllMicSelectListener = onCancelAllMicSelectListener;
    }

    public void setSelectCount(int selectCount) {
        this.selectCount = selectCount;
    }
}
