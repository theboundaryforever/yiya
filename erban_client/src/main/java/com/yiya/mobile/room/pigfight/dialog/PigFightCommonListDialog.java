package com.yiya.mobile.room.pigfight.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.room.pigfight.fragment.PigFightRuleFragment;
import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.OnClick;

public class PigFightCommonListDialog extends BaseTransparentDialogFragment {
    public static final int TYPE_RULE_LIST = 0;
    public static final String KEY_TYPE = "KEY_TYPE";

    @BindView(R.id.fl_container)
    FrameLayout flContainer;
    @BindView(R.id.iv_title)
    ImageView ivTitle;

    public static PigFightCommonListDialog newInstance(int type) {
        PigFightCommonListDialog dialog = new PigFightCommonListDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_TYPE, type);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_pig_common_list;
    }

    @Override
    protected int getWindowAnimationStyle() {
        return R.style.WindowBottomAnimationStyle;
    }

    @Override
    protected int getWindowGravity() {
        return Gravity.BOTTOM;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            int type = getArguments().getInt(KEY_TYPE);
            Fragment fragment = null;
            fragment = new PigFightRuleFragment();
            int titleRes = -1;
            int height = 225;
            titleRes = R.mipmap.ic_pig_rule_title;
            if (fragment != null) {
                flContainer.getLayoutParams().height = DisplayUtils.dip2px(getContext(), height);
                ivTitle.setImageResource(titleRes);
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fl_container, fragment)
                        .commitAllowingStateLoss();
            }
        }
    }

    @OnClick(R.id.iv_close)
    public void onViewClicked() {
        dismiss();
    }
}
