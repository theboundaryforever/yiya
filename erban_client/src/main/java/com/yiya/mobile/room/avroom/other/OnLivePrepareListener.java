package com.yiya.mobile.room.avroom.other;

/**
 * 直播预览页面监听
 */
public interface OnLivePrepareListener {
    void onLivePrepareFinish(String coverUrl, String title, boolean isCanConnectMic);
}
