package com.yiya.mobile.room.avroom.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.room.bean.RoomFunctionBean;
import com.tongdaxing.xchat_core.room.bean.RoomFunctionEnum;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.view.DrawableTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * 房间功能弹框
 */
public class RoomFunctionDialog extends Dialog implements View.OnClickListener, BaseQuickAdapter.OnItemClickListener {
    private RecyclerView rvFunction;
    private OnFunctionClickListener onFunctionClickListener;
    private RoomFunctionEnum[] functions;
    private static int functionType = -1;
    private RoomFunctionAdapter mAdapter;
    private RoomFunctionModel roomFunctionModel;
    private DrawableTextView dtvQuit;

    public RoomFunctionDialog(@NonNull Context context) {
        super(context, R.style.GiftBottomSheetDialog);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_room_function);
        dtvQuit = findViewById(R.id.dtv_quit);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.BOTTOM);
        }

        setCanceledOnTouchOutside(true);
        rvFunction = findViewById(R.id.rv_bottom_function);
        int distance = DisplayUtils.dip2px(getContext(), 10);
        rvFunction.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.top = distance;
                outRect.bottom = distance;
            }
        });

        mAdapter = new RoomFunctionAdapter(getContext());
        rvFunction.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
        dtvQuit.setOnClickListener(this);
    }

    public RoomFunctionAdapter getAdapter() {
        return mAdapter;
    }

    public void setFunctionType(int functionType) {
        RoomFunctionDialog.functionType = functionType;
    }

    public void initData() {
        if (roomFunctionModel == null)
            roomFunctionModel = new RoomFunctionModel();
        functions = roomFunctionModel.getRoomFunctionByType(functionType);
        if (functions == null || functions.length <= 0)
            return;
        List<RoomFunctionBean> datas = new ArrayList<>();
        for (int i = 0; i < functions.length; i++) {
            datas.add(roomFunctionModel.getFunctionBean(functions[i]));
        }
        mAdapter.setNewData(datas);
    }

    /**
     * 去掉 后台控制显示的元素
     * @param list
     */
    public void initData(List<RoomFunctionEnum> list) {
        if (roomFunctionModel == null)
            roomFunctionModel = new RoomFunctionModel();
        functions = roomFunctionModel.getRoomFunctionByType(functionType);
        if (functions == null || functions.length <= 0)
            return;

        List<RoomFunctionBean> datas = new ArrayList<>();
        for (int i = 0; i < functions.length; i++) {
            if (list.indexOf(functions[i]) < 0) {
                datas.add(roomFunctionModel.getFunctionBean(functions[i]));
            }
        }
        mAdapter.setNewData(datas);

    }

    @Override
    public void onClick(View v) {
        dismiss();
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        RoomFunctionEnum functionType = null;
        if (onFunctionClickListener != null && adapter != null && !ListUtils.isListEmpty(adapter.getData())) {
            RoomFunctionBean roomFunctionBean = (RoomFunctionBean) adapter.getData().get(position);
            onFunctionClickListener.onItemClick(roomFunctionBean);
            functionType = roomFunctionBean.getFunctionType();
        }
        if (RoomFunctionEnum.ROOM_PLANT_BEAN != functionType) {
            //种豆消息不关闭dialog
            dismiss();
        }
    }


    public interface OnFunctionClickListener {
        void onItemClick(RoomFunctionBean bean);
    }

    public void setOnFunctionClickListener(OnFunctionClickListener onFunctionClickListener) {
        this.onFunctionClickListener = onFunctionClickListener;
    }


    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (onFunctionClickListener != null) {
            onFunctionClickListener = null;
        }
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
