package com.yiya.mobile.room.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.juxiao.library_ui.widget.DrawableTextView;
import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/6
 * 描述        房间连麦弹窗
 * <p>
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class BaseRemindDialog extends BaseDialogFragment implements View.OnClickListener {

    protected TextView tvTitle, tvGoldTip;
    protected DrawableTextView dtConfirm, dtCancel;

    public static final String KEY_TYPE = "KEY_TYPE";
    public static final String KEY_CONTENT = "KEY_CONTENT";
    public static final String KEY_SHOW_NOTE = "KEY_SHOW_NOTE";

    protected LianMaiType lianMaiType;
    protected String msgContent = "";
    protected String goldNote = "";

    public static BaseRemindDialog newInstance(String msgContent, String showGoldNoteMsg) {
        return newInstance(LianMaiType.OTHER, msgContent, showGoldNoteMsg);
    }

    protected static BaseRemindDialog newInstance(LianMaiType type, String msgContent, String showGoldNoteMsg) {
        BaseRemindDialog dialog = new BaseRemindDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_TYPE, type.ordinal());
        bundle.putString(KEY_CONTENT, msgContent);
        bundle.putString(KEY_SHOW_NOTE, showGoldNoteMsg);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            lianMaiType = LianMaiType.values()[arguments.getInt(KEY_TYPE, LianMaiType.OTHER.ordinal())];
            msgContent = arguments.getString(KEY_CONTENT, "");
            goldNote = arguments.getString(KEY_SHOW_NOTE, "");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View rootView = inflater.inflate(R.layout.dialog_room_lian_mai, window.findViewById(android.R.id.content), false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        window.setWindowAnimations(R.style.ErbanCommonWindowAnimationStyle);
        int width = ScreenUtil.getScreenWidth(getContext()) - ScreenUtil.dip2px(32);
        window.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        setCancelable(true);

        initView(rootView);

        return rootView;
    }

    private void initView(View rootView) {
        tvTitle = rootView.findViewById(R.id.tv_title);
        tvGoldTip = rootView.findViewById(R.id.tv_gold_tip);
        dtConfirm = rootView.findViewById(R.id.dt_confirm);
        dtCancel = rootView.findViewById(R.id.dt_cancel);

        dtConfirm.setOnClickListener(this);
        dtCancel.setOnClickListener(this);

        initViewData();
    }

    private void initViewData() {
        switch (lianMaiType) {
            //自定义
            case OTHER:
                customMsg();
                break;
            default:
                predefined(lianMaiType);
                break;
        }
    }

    protected void predefined(LianMaiType lianMaiType) {

    }

    private void customMsg() {
        if (StringUtils.isNotEmpty(msgContent)) {
            tvTitle.setText(msgContent);
        }
        tvGoldTip.setVisibility(StringUtils.isNotEmpty(goldNote) ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dt_confirm:
                if (listener != null) {
                    listener.onConfirm(lianMaiType);
                }
                dismiss();
                break;
            case R.id.dt_cancel:
                if (listener != null) {
                    listener.onCancel();
                }
                dismiss();
                break;
            default:
                break;
        }
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, null);
    }

    private OnLianMaiClickListener listener;

    public interface OnLianMaiClickListener {

        void onConfirm(LianMaiType value);

        default void onCancel() {

        }
    }

    public void setOnLianMaiClickListener(OnLianMaiClickListener listener) {
        this.listener = listener;
    }

    public enum LianMaiType {

        /**
         * 免费邀请上麦 0
         */
        INVITE_FREE,
        /**
         * 收费邀请上麦 1
         */
        INVITE_CHARGE,
        /**
         * 用户申请上麦 2
         */
        UPPER_MICRO,
        /**
         * 下麦 3
         */
        LOWER_MICRO,
        /**
         * 发送音符 4
         */
        SENT_GOLD_NOTE,
        /**
         * 上麦是否收费 5
         */
        UPPER_MICRO_CHARGE,
        /**
         * 邀请进房 6
         */
        INVITE_ENTER_ROOM,
        /**
         * 自定义 7
         */
        OTHER
    }
}

