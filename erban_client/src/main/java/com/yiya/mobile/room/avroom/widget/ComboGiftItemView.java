package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.gift.ComboGiftVoInfo;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2019/4/7.
 */

public class ComboGiftItemView extends FrameLayout {
    public final static String TAG = ComboGiftItemView.class.getSimpleName();

    private Context mContext;

    @BindView(R.id.tv_nick)
    TextView tv_nick;
    @BindView(R.id.tv_content)
    TextView tv_content;
    @BindView(R.id.iv_gift)
    ImageView iv_gift;
    @BindView(R.id.iv_avatar)
    ImageView iv_avatar;
    @BindView(R.id.tv_gift_num)
    TextView tvGiftNum;
    @BindView(R.id.tv_gift_combo)
    MagicTextView tv_gift_combo;

//    @BindView(R.id.animation_view)
//    LottieAnimationView animation_view;

    private ComboGiftVoInfo comboGiftVoInfo;
    private long comboCount = 0;

    public ComboGiftItemView(Context context) {
        super(context);
        init(context);
    }

    public ComboGiftItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ComboGiftItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;

        View view = LayoutInflater.from(mContext).inflate(R.layout.item_combo_gift_layout, this);
        ButterKnife.bind(view);

        initAnim();
    }

    /***
     * 首先是从左到右的
     */
    private void initAnim() {
        firstAnim();
    }

    public void updateCombo(ComboGiftVoInfo voInfo) {
        if (voInfo != null) {

            ImageLoadUtils.loadAvatar(mContext, voInfo.getAvatar(), iv_avatar, true);
            tv_nick.setText(voInfo.getNick());
            tv_content.setText(voInfo.getContent());
            ImageLoadUtils.loadImage(mContext, voInfo.getGiftUrl(), iv_gift);

            long tmpCount = voInfo.getComboCount();
            if (voInfo.getComboFrequencyCount() > 0){ // 说明是新版的礼物连击
                if (voInfo.getCurrentNum() == 1){
                    if (tmpCount > comboCount) {
                        tv_gift_combo.setText("x" + tmpCount);
                    }

                    comboCount = voInfo.getComboCount();
                } else {
                    if (voInfo.getComboFrequencyCount() > comboCount) {
                        tv_gift_combo.setText("x" + voInfo.getComboFrequencyCount());
                    }
                    tvGiftNum.setText(String.valueOf(voInfo.getCurrentNum()));
                    comboCount = voInfo.getComboFrequencyCount();
                }
            } else {// 旧版礼物连击
                if (voInfo.getComboCount() > comboCount) {
                    tv_gift_combo.setText("x" + tmpCount);
                }

                comboCount = tmpCount;
            }

            comboGiftVoInfo = voInfo;
            comboGiftVoInfo.setStartTime(System.currentTimeMillis());
            comboGiftVoInfo.setGiftAnim(ComboGiftVoInfo.GiftAnim.LOADING);

            comboAnim();
        }
    }

    public void firstAnim() {
        TranslateAnimation animation = (TranslateAnimation) AnimationUtils.loadAnimation(mContext, R.anim.anim_combo_gift_in);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tv_gift_combo.setVisibility(VISIBLE);
                comboAnim();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(animation);
    }

    /**
     * 连击动画
     */
    public void comboAnim() {
        ScaleAnimation animation = new ScaleAnimation(
                1.2f, 0.8f, 1.2f, 0.8f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f
        );
        animation.setDuration(200);
        animation.setInterpolator(new AnticipateOvershootInterpolator());
        //动画执行完毕后是否停在结束时的状态
        animation.setFillAfter(true);
        tv_gift_combo.startAnimation(animation);
    }

    public ComboGiftVoInfo getComboGiftVoInfo() {
        return comboGiftVoInfo;
    }

}

