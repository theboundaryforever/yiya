package com.yiya.mobile.room.match;


import android.widget.RadioButton;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.bean.RoomChoiceBean;

public class RoomChoiceAdapter extends BaseQuickAdapter<RoomChoiceBean, BaseViewHolder> {
    private int selectPosition = -1;
    public RoomChoiceAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, RoomChoiceBean item) {
        RadioButton rbChoice = helper.getView(R.id.rb_room_choice);
        if (item.isSelect()){
            rbChoice.setBackgroundResource(R.drawable.ic_match_choice_ball);
        }else {
            rbChoice.setBackgroundResource(item.getResId());
        }
    }

    public int getSelectPosition() {
        return selectPosition;
    }

    public void setSelectPosition(int selectPosition) {
        this.selectPosition = selectPosition;
    }

    public void changeState(int position){
        if (getData().get(position).isRandom()){//随机
            int select = RoomMatchUtil.getRandomNum(0,8);
            if (select == position)
                return;
            changePositionState(select,true);
        }else {//
            changePositionState(position,false);
        }
    }

    private void changePositionState(int position,boolean random){
        if (selectPosition == -1) {
            getData().get(position).setSelect(true);
            notifyDataSetChanged();
            selectPosition = position;
        } else {
            if (selectPosition != position) {
                getData().get(position).setSelect(true);
                getData().get(selectPosition).setSelect(false);
                notifyDataSetChanged();
                selectPosition = position;
            } else {//相同位置
                if (!random) {
                    getData().get(selectPosition).setSelect(false);
                    notifyDataSetChanged();
                    selectPosition = -1;
                }
                //随机到相同位置不做处理
            }
        }
    }

}
