package com.yiya.mobile.room.avroom.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.room.bean.CPKResultBean;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class CPKResultDialog extends Dialog {
    RelativeLayout rlResult;
    ImageView ivAvatar;
    TextView tvNick;
    TextView tvNoWinner;
    private Disposable disposable;

    public CPKResultDialog(Context context) {
        super(context, R.style.transDialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_cpk_result);

        rlResult = findViewById(R.id.rl_result);
        ivAvatar = findViewById(R.id.iv_avatar);
        tvNick = findViewById(R.id.tv_nick);
        tvNoWinner = findViewById(R.id.tv_no_winner);
    }

    public void setUpView(CPKResultBean bean) {
        if (bean == null) {
            return;
        }

        if (bean.getType() == CPKResultBean.CPKRESULT_WIN && ListUtils.isNotEmpty(bean.getAvatarList())) {// 获胜
            rlResult.setVisibility(View.VISIBLE);
            tvNoWinner.setVisibility(View.GONE);
            ImageLoadUtils.loadCircleImage(getContext(), bean.getAvatarList().get(0), ivAvatar, R.drawable.nim_avatar_default);
            tvNick.setText(bean.getWinNickName());
        } else if (bean.getType() == CPKResultBean.CPKRESULT_DRAW) {// 平局
            rlResult.setVisibility(View.GONE);
            tvNoWinner.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void show() {
        super.show();
        disposable = Observable.timer(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aLong -> dismiss());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

}
