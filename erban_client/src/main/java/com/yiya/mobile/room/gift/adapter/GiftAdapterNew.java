package com.yiya.mobile.room.gift.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaoyu
 * @date 2017/12/11
 */

public class GiftAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<GiftInfo> giftInfoList;

    public void setGiftInfoList(List<GiftInfo> giftInfoList) {
        this.giftInfoList = giftInfoList;
    }

    private Context context;
    private int index = -1;

    public void setIndex(int index) {
        this.index = index;
    }

    public GiftAdapterNew(Context context) {
        if (this.giftInfoList == null)
            this.giftInfoList = new ArrayList<>();
        this.context = context;
    }

    public GiftInfo getIndexGift() {
        if (giftInfoList != null && giftInfoList.size() > index) {
            return giftInfoList.get(index);
        }
        return null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.item_gift_new, parent, false);
        return new GiftViewHolder(root);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        GiftViewHolder holder = (GiftViewHolder) viewHolder;
        GiftInfo giftInfo = giftInfoList.get(position);
        holder.giftName.setText(giftInfo.getGiftName());
        holder.giftGold.setText(giftInfo.getGoldPrice() + "");
        if (giftInfo.getUserGiftPurseNum() > 0) {
            holder.freeGiftCount.setVisibility(View.VISIBLE);
            holder.freeGiftCount.setText("X" + giftInfo.getUserGiftPurseNum());
        } else {
            holder.freeGiftCount.setVisibility(View.GONE);
        }

        holder.bind(position);
        ImageLoadUtils.loadImage(BasicConfig.INSTANCE.getAppContext(), giftInfo.getGiftUrl(), holder.giftImage);
        if (position == index) {
//            holder.giftSelect.setVisibility(View.GONE);
            holder.giftImageSelected.setVisibility(View.VISIBLE);
        } else {
//            holder.giftSelect.setVisibility(View.GONE);
            holder.giftImageSelected.setVisibility(View.GONE);
        }
        if (giftInfo.isHasEffect()) {
//            if (position == index) {
//                holder.giftEffects.setVisibility(View.INVISIBLE);
//            } else {
            holder.giftEffects.setVisibility(View.VISIBLE);
//            }
        } else {
            holder.giftEffects.setVisibility(View.GONE);
        }
        if (giftInfo.isHasTimeLimit()) {
            holder.giftLimitTime.setVisibility(View.VISIBLE);
        } else {
            holder.giftLimitTime.setVisibility(View.GONE);
        }
        if (giftInfo.isHasLatest()) {
            holder.giftNew.setVisibility(View.VISIBLE);
        } else {
            holder.giftNew.setVisibility(View.GONE);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public int getItemCount() {
        return giftInfoList == null ? 0 : giftInfoList.size();
    }

    class GiftViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView giftImage;
        private ImageView giftImageSelected;
        private TextView giftName;
        private TextView giftGold;
        private ImageView giftSelect;
        private ImageView giftEffects;
        private ImageView giftNew;
        private ImageView giftLimitTime;
        private TextView freeGiftCount;

        GiftViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            giftImage = (ImageView) itemView.findViewById(R.id.gift_image);
            giftImageSelected = itemView.findViewById(R.id.gift_image_selected);
            giftGold = (TextView) itemView.findViewById(R.id.gift_gold);
            giftName = (TextView) itemView.findViewById(R.id.gift_name);
            giftEffects = (ImageView) itemView.findViewById(R.id.icon_gift_effect);
            giftNew = (ImageView) itemView.findViewById(R.id.icon_gift_new);
            giftLimitTime = (ImageView) itemView.findViewById(R.id.icon_gift_limit_time);
            giftSelect = (ImageView) itemView.findViewById(R.id.icon_room_gift_select);
            freeGiftCount = (TextView) itemView.findViewById(R.id.tv_free_gift_count);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, position);
            }
        }

        int position;

        public void bind(int position) {
            this.position = position;
        }
    }

    public List<GiftInfo> getGiftInfoList() {
        return giftInfoList;
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
