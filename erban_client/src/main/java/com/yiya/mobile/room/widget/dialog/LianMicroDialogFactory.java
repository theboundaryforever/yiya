package com.yiya.mobile.room.widget.dialog;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.tongdaxing.xchat_core.room.bean.LianMicroStatusInfo;

/**
 * 连麦申请弹框
 *
 * @author zeda
 */
public class LianMicroDialogFactory {

    //主播关闭了连麦功能、可以申请连麦、申请连麦等待中
    private static final int TYPE_CLOSE = 0, TYPE_APPLY = 1, TYPE_WAIT = 2;

    public static BaseDialogFragment create(@NonNull LianMicroStatusInfo statusInfo, @Nullable OnLianMicroDialogClickListener onLianMicroDialogClickListener) {
        BaseDialogFragment dialogFragment;
        switch (statusInfo.getStatus()) {
            case TYPE_APPLY:
                dialogFragment = LianMicroApplyDialog.newInstance(statusInfo, onLianMicroDialogClickListener);
                break;
            case TYPE_WAIT:
                dialogFragment = LianMicroWaitDialog.newInstance(statusInfo, onLianMicroDialogClickListener);
                break;
            default:
                dialogFragment = LianMicroCloseDialog.newInstance(statusInfo.getMessage());
                break;
        }
        return dialogFragment;
    }

    public interface OnLianMicroDialogClickListener {

        void onVideoBtnClick();

        void onAudioBtnClick();

        void onCancelBtnClick();
    }
}
