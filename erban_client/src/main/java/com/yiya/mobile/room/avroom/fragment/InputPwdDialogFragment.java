package com.yiya.mobile.room.avroom.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.R;

import org.jetbrains.annotations.NotNull;

/**
 * <p> 密码框输入  </p>
 *
 * @author Administrator
 * @date 2017/12/1
 */
public class InputPwdDialogFragment extends DialogFragment implements View.OnClickListener {
    private TextView mTvTitle;
    private EditText mInputText;
    private TextView mFailText;
    private TextView mConfirmText;
    private TextView mCancelText;

    private String mTitle;
    private String mOk;
    private String mCancel;

    public static InputPwdDialogFragment newInstance(String title, String okLabel, String cancelLabel) {
        InputPwdDialogFragment fragment = new InputPwdDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("okLabel", okLabel);
        bundle.putString("cancelLabel", cancelLabel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            mTitle = arguments.getString("title");
            mOk = arguments.getString("okLabel");
            mCancel = arguments.getString("cancelLabel");
        }

        setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.layout_pic_login_dialog, container);
        mTvTitle = mRootView.findViewById(R.id.pic_login_title);
        mInputText = mRootView.findViewById(R.id.pic_login_input);
        mFailText = mRootView.findViewById(R.id.pic_login_fail_msg);
        mConfirmText = mRootView.findViewById(R.id.btn_confirm);
        mCancelText = mRootView.findViewById(R.id.btn_cancel);
        return mRootView;
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mInputText.requestFocus();
        GrowingIO.getInstance().trackEditText(mInputText);
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }

        mTvTitle.setText(mTitle);
        mCancelText.setText(mCancel);
        mConfirmText.setText(mOk);

        mConfirmText.setOnClickListener(this);
        mCancelText.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:
                if (mOnDialogBtnClickListener != null) {
                    mOnDialogBtnClickListener.onBtnConfirm(mInputText.getText().toString());
                }

                break;
            case R.id.btn_cancel:
                if (mOnDialogBtnClickListener != null) {
                    mOnDialogBtnClickListener.onBtnCancel();
                }
                break;
            default:
        }
    }

    public void refreshFailText() {
        mFailText.setVisibility(View.GONE);
    }

    private OnDialogBtnClickListener mOnDialogBtnClickListener;

    public void setOnDialogBtnClickListener(OnDialogBtnClickListener onDialogBtnClickListener) {
        mOnDialogBtnClickListener = onDialogBtnClickListener;
    }

    public interface OnDialogBtnClickListener {
        void onBtnConfirm(String pwd);

        void onBtnCancel();
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();

    }

    @Override
    public void dismiss() {
        //临时解决方案，dialog show和dismiss之类的错误问题
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
