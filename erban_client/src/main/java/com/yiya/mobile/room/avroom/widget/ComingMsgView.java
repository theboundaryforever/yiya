package com.yiya.mobile.room.avroom.widget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yiya.mobile.view.LevelView;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.RoomMemberComeInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/18
 * 描述        房间内谁来了的封装view
 *
 * @author dell
 */
public class ComingMsgView extends RelativeLayout {

    private LevelView levelView;
    private TextView tvUserName, tvMsgCar, tvLevelNum, tvLevelLevel;
    private SvgaShowListener listener;
    private ObjectAnimator mTranslateAnimator;
    private Context mContext;

    public ComingMsgView(Context context) {
        this(context, null);
        this.mContext = context;
    }

    public ComingMsgView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
        this.mContext = context;
    }

    public ComingMsgView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initialView();
    }

    private void initialView() {
        inflate(getContext(), R.layout.layout_coming_msg, this);
        tvUserName = findViewById(R.id.tv_user_name);
        tvLevelNum = findViewById(R.id.tv_level_num);
        Typeface typeface= Typeface.createFromAsset(mContext.getAssets(),"ACCIDENTAL_PRESIDENCYT.TTF");
        tvLevelNum.setTypeface(typeface);
    }

    public void setupView(RoomMemberComeInfo comeInfo, int roomType) {
        if (comeInfo != null) {
            //展示用户的座驾动画
            if (StringUtils.isNotEmpty(comeInfo.getCarImgUrl()) && listener != null) {
                listener.onShow(comeInfo.getCarImgUrl());
            }
            //展示用户财富等级与背景
            int level = comeInfo.getExperLevel();
            int userLevel = comeInfo.getVideoRoomExperLevel();
            if (level >= 10 || userLevel >= 10) {
//                levelView.setExperLevel(level);
//                if (level < 20) {
//                    setBackgroundResource(R.drawable.bg_come_msg_tip_lv10_19);
//                    setBackgroundResource(R.drawable.bg_come_msg_tip_lv10_19);
//                } else if (level < 30) {
//                    setBackgroundResource(R.drawable.bg_come_msg_tip_lv20_29);
//                } else if (level < 40) {
//                    setBackgroundResource(R.drawable.bg_come_msg_tip_lv30_39);
//                } else if (level < 50) {
//                    setBackgroundResource(R.drawable.bg_come_msg_tip_lv40_49);
//                } else {
//                    setBackgroundResource(R.drawable.bg_come_msg_tip_lv50_59);
//                }
//                setBackgroundResource(R.drawable.bg_come_msg_tip);
                String nickName = comeInfo.getNickName();
                if(nickName.length()>14){
                    tvUserName.setText(nickName.substring(0, 14)+"...进入直播间");
                } else {
                    tvUserName.setText(nickName+"进入直播间");
                }

                if(roomType == RoomInfo.ROOMTYPE_VIDEO_LIVE && userLevel >= 10){
                    tvLevelNum.setText("Lv." + userLevel);// 视频房使用用户等级
                } else if( roomType == RoomInfo.ROOMTYPE_HOME_PARTY && level >= 10) {
                    tvLevelNum.setText("Lv." + level);// 语音房使用财富等级
                }

//                String carName = comeInfo.getCarName();
//                tvMsgCar.setText(StringUtils.isNotEmpty(carName) ? getContext().getString(R.string.come_msg_tip_car,
//                        carName) : getContext().getString(R.string.come_msg_tip_not_car));
            }
        }
    }

    public void setSvgaListener(SvgaShowListener listener) {
        this.listener = listener;
    }

    public interface SvgaShowListener {

        /**
         * 是否展示用户的svg座驾动画
         */
        void onShow(String svgUrl);
    }

    /**
     * 做弹幕动画
     */
    public void startAnimation(Animator.AnimatorListener listener) {
        float inInterval = 0.16f;
        float inSpeed = 3f;
        float inValue = inInterval * inSpeed;

        float middleInterval = 0.95f;
        float middleSpeed = 0.06f;
        float middleValue = inValue + (middleInterval - inInterval) * middleSpeed;

        float outSpeed = 2.65f;

        if (mTranslateAnimator == null) {
            mTranslateAnimator = ObjectAnimator.ofFloat(this, "translationX", ScreenUtil.getDisplayWidth(), -getContext().getResources()
                    .getDimensionPixelOffset(R.dimen.layout_come_msg_width));
            mTranslateAnimator.setInterpolator(v -> {
                if (v < inInterval) {
                    return inSpeed * v;
                } else if (v < middleInterval) {
                    return inValue + middleSpeed * (v - inInterval);
                } else {
                    return middleValue + outSpeed * (v - middleInterval);
                }
            });
            mTranslateAnimator.setDuration(4000);
        }
        mTranslateAnimator.removeAllListeners();
        mTranslateAnimator.addListener(listener);
        mTranslateAnimator.start();
    }

    public void release() {
        if (mTranslateAnimator != null) {
            mTranslateAnimator.removeAllListeners();
            mTranslateAnimator.cancel();
        }
    }
}
