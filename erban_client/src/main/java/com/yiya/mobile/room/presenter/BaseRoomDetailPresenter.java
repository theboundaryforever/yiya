package com.yiya.mobile.room.presenter;

import android.text.TextUtils;

import com.yiya.mobile.presenter.BaseMvpPresenter;
import com.yiya.mobile.room.model.RoomModel;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomMemberComeInfo;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.initial.ClientConfigure;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.msg.MsgModel;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyBoxInfo;
import com.tongdaxing.xchat_core.room.bean.WaitQueuePersonInfo;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.im.IMError;
import com.tongdaxing.xchat_framework.im.IMReportBean;
import com.tongdaxing.xchat_framework.im.IMReportResult;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtil;

import java.util.List;

import static com.tongdaxing.xchat_core.manager.AvRoomDataManager.MIC_POSITION_BY_OWNER;
import static com.tongdaxing.xchat_core.manager.AvRoomDataManager.MSG_SEND_MAX_LENGTH;

/**
 * 房间页面的presenter
 */
public class BaseRoomDetailPresenter<V extends IRoomDetailView> extends BaseMvpPresenter<V> {

    //除RoomModel外，AvRoomDataManager、RtcEngineManager也属于model
    protected RoomModel model;

    BaseRoomDetailPresenter() {
        model = new RoomModel();
    }

    ClientConfigure clientConfigure;

    /**
     * 退出房间
     *
     * @param callBack 退出结果回调
     */
    public void exitRoom(CallBack<String> callBack) {
        model.exitRoom(callBack);
    }


    /**
     * 获取当前房间的房间信息
     */
    public RoomInfo getCurrRoomInfo() {
        return model.getCurrentRoomInfo();
    }

    /**
     * 获取app设置
     *
     * @return
     */
    public ClientConfigure getClientConfigure() {
        if (clientConfigure == null) {
            clientConfigure = DemoCache.readClientConfigure();
        }
        return clientConfigure;
    }

    /**
     * 后台是否开启房间超级宝箱
     *
     * @return
     */
    public boolean isLuckyBoxEnabled() {
        return getClientConfigure() != null && getClientConfigure().getLucky_treasure_chest_flag() == 0;
    }

    /**
     * 后台是否开启房间种瓜
     *
     * @return
     */
    public boolean isPlantCucumberEnabled() {
        return getClientConfigure() != null && getClientConfigure().getSeed_melon_switch() == 1;
    }

    /**
     * 获取进入房间的用户信息
     */
    public RoomMemberComeInfo getAndRemoveFirstMemberComeInfo() {
        return AvRoomDataManager.get().getAndRemoveFirstMemberComeInfo();
    }

    /**
     * 获取进入房间用户的size
     */
    public int getMemberComeSize() {
        return AvRoomDataManager.get().getMemberComeSize();
    }

    /**
     * 检测是否关注过该房间
     */
    public void checkAttention(long roomId) {
        if (AvRoomDataManager.get().isRoomOwner()) {
            return;
        }
        model.checkAttention(roomId, new HttpRequestCallBack<Boolean>() {
            @Override
            public void onSuccess(String message, Boolean response) {
                if (getMvpView() != null) {
                    getMvpView().refreshRoomAttentionStatus(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(code + msg);
                }
            }
        });
    }

    /**
     * 点击关注按钮关注
     */
    public void roomAttention(long roomId, boolean needRefresh) {
        long roomUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        model.roomAttention(roomUid, roomId, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
                if (needRefresh) {
                    if (getMvpView() != null) {
                        getMvpView().refreshRoomAttentionStatus(true);
                    }
                }
                IMNetEaseManager.get().sendTipMsg(AvRoomDataManager.get().getRoomInfo().getUid(),
                        IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_ATTENTION_ROOM_OWNER);
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(code + msg);
                }
            }
        });
    }

    /**
     * 取消房间的关注
     */
    public void deleteAttention(long roomId) {
        long roomUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        model.deleteAttention(roomUid, roomId, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().refreshRoomAttentionStatus(false);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(code + msg);
                }
            }
        });
    }

    /**
     * 获取礼物消息列表
     */
    public List<ChatRoomMessage> getGiftMsgList() {
        return model.getGiftMsgList();
    }

    /**
     * 保存最小化状态
     *
     * @param isMinimize 当前是否是最小化
     */
    public void saveMinimizeStatus(boolean isMinimize) {
        AvRoomDataManager.get().setMinimize(isMinimize);
    }

    /**
     * 我自己当前是否在麦上
     */
    public boolean isOnMicByMyself() {
        return AvRoomDataManager.get().isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    /**
     * 此人当前是否在麦上
     */
    public boolean isOnMic(long uid) {
        return AvRoomDataManager.get().isOnMic(uid);
    }

    /**
     * 我自己当前是否是设置了禁听远程麦（静音）
     */
    public boolean isRemoteMuteByMyself() {
        return RtcEngineManager.get().isRemoteMute();
    }

    /**
     * 我自己当前是否禁麦状态
     */
    public boolean isMicMuteByMyself() {
        return RtcEngineManager.get().isMute();
    }

    /**
     * 我自己当前是否是观众身份
     */
    public boolean isAudienceRoleByMyself() {
        return RtcEngineManager.get().isAudienceRole();
    }

    /**
     * 根据礼物id获取礼物信息
     */
    public GiftInfo getLocalGiftInfoById(int giftId) {
        return CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftId);
    }

    /**
     * 检查并自动上麦（需要上麦的话）
     */
    public void checkAndAutoUpMic() {
        RoomMicInfo roomMicInfo = getRoomMicInfoByMyself();
        if (roomMicInfo != null) {//如果我当前已经在麦位上
            //不是禁麦的话，操作开麦
            setMicStatusByMyself(!roomMicInfo.isMicMute());
        } else if (isRoomOwnerByMyself()) {//当前我不在麦位上，但我是房主
            operateUpMicro(MIC_POSITION_BY_OWNER, true);
        }
    }

    /**
     * 操作上麦
     *
     * @param micPosition   上的麦位
     * @param isInviteUpMic 上麦成功后是否自动开麦
     */
    public void operateUpMicro(int micPosition, boolean isInviteUpMic) {
        model.operateUpMicro(micPosition, isInviteUpMic, new CallBack<String>() {
            @Override
            public void onSuccess(String data) {
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    switch (code) {
                        case 2103:
                            getMvpView().showRechargeTipDialog();
                            break;
                        default:
                            getMvpView().toast(error);
                            break;
                    }
                }
            }
        });
    }

    /**
     * 获取我当前在麦上的麦位信息
     *
     * @return 麦位信息 null代表我不在麦上
     */
    private RoomMicInfo getRoomMicInfoByMyself() {
        RoomQueueInfo queueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoMyself();
        return queueInfo != null ? queueInfo.mRoomMicInfo : null;
    }

    /**
     * 我自己是否是房主
     */
    public boolean isRoomOwnerByMyself() {
        return AvRoomDataManager.get().isRoomOwner();
    }

    /**
     * 此人是否是房主
     */
    public boolean isRoomOwner(long uid) {
        return AvRoomDataManager.get().isRoomOwner(String.valueOf(uid));
    }


    /**
     * 我自己是否是管理员
     */
    public boolean isRoomAdminByMyself() {
        return AvRoomDataManager.get().isRoomAdmin();
    }

    /**
     * 此人是否是管理员
     */
    public boolean isRoomAdmin(long uid) {
        return AvRoomDataManager.get().isRoomAdmin(String.valueOf(uid));
    }


    /**
     * 操作添加/撤销房间管理员身份
     */
    public void operateAddOrRemoveManager(boolean isAdd, long uid) {
        IMNetEaseManager.get().markManager(String.valueOf(uid), isAdd, null);
    }

    /**
     * 操作从房间黑名单中添加/删除
     *
     * @param uid
     * @param isAdd              true 加入房间黑名单 false 移除房间黑名单
     * @param synPersonBlackList
     */
    public void operateRoomBlackList(long uid, boolean isAdd, boolean synPersonBlackList) {
        long roomId = AvRoomDataManager.get().getRoomInfo().getRoomId();
        IMNetEaseManager.get().addRoomBlackList(String.valueOf(roomId), String.valueOf(uid), isAdd, synPersonBlackList, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().toast(e.getMessage());
            }

            @Override
            public void onResponse(Json json) {
                if (getMvpView() == null) {
                    return;
                }
                if (json.num("errno") == 0) {
                    if (isAdd) {
                        getMvpView().toast("加入黑名单成功，他将无法进入该房间");
                    } else {
                        getMvpView().toast("操作成功");
                    }
                } else {
                    getMvpView().toast(json.num("errno") + " : " + json.str("errmsg"));
                }
            }
        });
    }

    /**
     * 操作从个人黑名单中添加/删除
     */
    public void operateUserBlackList(boolean isAdd, long uid) {
        if (isAdd) {
            IMNetEaseManager.get().addUserBlackList(String.valueOf(uid), new HttpRequestCallBack<Json>() {
                @Override
                public void onSuccess(String message, Json response) {
                    if (getMvpView() != null) {
                        getMvpView().toast("加入个人黑名单成功，他将无法与你私聊");
                    }
                }

                @Override
                public void onFailure(int code, String msg) {
                    if (getMvpView() != null) {
                        getMvpView().toast(code + " : " + msg);
                    }
                }
            });
        } else {
            IMNetEaseManager.get().removeUserBlackList(uid, new HttpRequestCallBack<Json>() {
                @Override
                public void onSuccess(String message, Json response) {

                }

                @Override
                public void onFailure(int code, String msg) {

                }
            });
        }
    }

    /**
     * 检查黑名单
     *
     * @param tgUid
     * @param callBack
     */
    public void checkUserBlacklist(long tgUid, HttpRequestCallBack<Json> callBack) {
        IMNetEaseManager.get().checkUserBlacklist(tgUid, callBack);
    }


    /**
     * 获取房主默认信息
     */
    public IMChatRoomMember getRoomOwnerDefaultMemberInfo() {
        return AvRoomDataManager.get().getRoomOwnerDefaultMemberInfo();
    }

    /**
     * 获取 自己的实体
     */
    public IMChatRoomMember getMOwnerMember() {
        return AvRoomDataManager.get().getMOwnerMember();
    }

    public void setMOwnerMember(IMChatRoomMember mOwnerMember) {
        AvRoomDataManager.get().setMOwnerMember(mOwnerMember);
    }

    /**
     * 操作踢用户出房间
     *
     * @param uid 被踢的用户uid
     */
    public void operateKickMemberToRoom(long uid) {
        IMNetEaseManager.get().kickMember(String.valueOf(uid), new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().toast(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (getMvpView() != null) {
                    IMReportBean imReportBean = new IMReportBean(response);
                    if (imReportBean.getReportData().errno != 0) {
                        getMvpView().toast(imReportBean.getReportData().errmsg);
                    }
                }
            }
        });
    }

    /**
     * 操作房间用户禁言
     *
     * @param account
     * @param isMute
     */
    public void operateMarkChatRoomMute(String account, int isMute) {

        IMNetEaseManager.get().markChatRoomMute(account, isMute, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().toast(e.toString());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        if (response.num("errno") == 0) {
//                            getMvpView().toast("禁言成功");
                        }
                    } else {
                        getMvpView().toast("Error data.");
                    }
                }
            }
        });
    }

    /**
     * 查询房间用户禁言接口
     *
     * @param search_uid
     * @param callBack
     */
    public void checkChatRoomMute(long search_uid, OkHttpManager.MyCallBack callBack) {
        IMNetEaseManager.get().fetchChatRoomMuteList(search_uid, callBack);
    }


    /**
     * 设置我自己的麦位状态
     *
     * @param isOpen 是否开麦
     */
    private void setMicStatusByMyself(boolean isOpen) {
        RtcEngineManager.get().setRole(AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, isOpen ? io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER : io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE);
    }

    /**
     * 当房间人员变化的时候记录一下变化的时间戳
     */
    public void setTimestampOnMemberChanged(long timestamp) {
        AvRoomDataManager.get().setTimestamp(timestamp);
    }

    /**
     * 获取当房间人员变化的时候记录的时间戳
     */
    public long getTimestampOnMemberChanged() {
        return AvRoomDataManager.get().getTimestamp();
    }

    /**
     * 操作麦位开麦 或 关麦
     *
     * @param isOpen 是否开麦
     */
    public void operateMicroOpenOrClose(int micPosition, boolean isOpen) {
        model.operateMicroOpenOrClose(micPosition, isOpen);
    }

    /**
     * 切换 闭麦/开麦 状态
     */
    public void switchMicroMuteStatus() {
        if (AvRoomDataManager.get().getRoomInfo() != null) {
            RtcEngineManager.get().setMute(AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, !RtcEngineManager.get().isMute());
        }
    }

//    /**
//     * @param mute true 开麦 false 闭麦
//     */
//    public void switchMicroMute(boolean mute) {
//        if (AvRoomDataManager.get().getRoomInfo() != null) {
//            RtcEngineManager.get().setMute(AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, mute);
//        }
//    }

    //当前 麦位锁麦或解锁麦的操作 是否正在进行中
    private boolean isOperateMicroLockOrUnLockRequesting = false;

    /**
     * 操作麦位锁麦 或 解锁麦
     *
     * @param isLock 是否锁麦
     */
    public void operateMicroLockOrUnLock(int micPosition, boolean isLock) {
        if (isOperateMicroLockOrUnLockRequesting) {
            return;
        }
        isOperateMicroLockOrUnLockRequesting = true;
        model.operateMicroLockAndUnLock(micPosition, isLock, new HttpRequestCallBack<String>() {
            @Override
            public void onSuccess(String message, String response) {

            }

            @Override
            public void onFailure(int code, String msg) {

            }

            @Override
            public void onFinish() {
                isOperateMicroLockOrUnLockRequesting = false;
            }
        });
    }

    /**
     * 获取麦序队列信息（包含麦位信息 和 麦上人员信息）
     *
     * @return 返回null代表这个麦位是不存在的
     */
    public RoomQueueInfo getMicQueueInfoByMicPosition(int micPosition) {
        return AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(micPosition);
    }

    /**
     * 获取我自己在麦序队列上的信息（包含麦位信息 和 麦上人员信息）
     *
     * @return 返回null代表我不在麦上
     */
    public RoomQueueInfo getMicQueueInfoByMicPositionByMyself() {
        return AvRoomDataManager.get().getRoomQueueMemberInfoMyself();
    }

    /**
     * 发送文本消息
     */
    public void sendTextMsg(String content) {
        UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomSendMsg());//统计
        RoomInfo roomInfo = getCurrRoomInfo();
        if (roomInfo == null || TextUtils.isEmpty(content) || getMvpView() == null) {
            return;
        }
        if (roomInfo.getPublicChatSwitch() == 1) {//禁止了公屏
            getMvpView().toast("公屏消息已经被管理员禁止，请稍候再试");
        } else {
            if (content.length() > MSG_SEND_MAX_LENGTH) {//长度限制
                getMvpView().toast("当前文本长度" + content.length() + "已超过最大500限制");
            } else {
                String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
                if (!TextUtils.isEmpty(sensitiveWordData) && content.matches(sensitiveWordData)) {//敏感词检测
                    getMvpView().toast(R.string.sensitive_word_data);
                } else {
                    IMNetEaseManager.get().sendTextMsg(roomInfo.getRoomId(), content, new CallBack<String>() {
                        @Override
                        public void onSuccess(String data) {
                            if (getMvpView() == null) {
                                return;
                            }
                            IMReportBean imReportBean = new IMReportBean(data);
                            if (imReportBean.getReportData() != null && imReportBean.getReportData().errno != 0) {
                                int errorCode = imReportBean.getReportData().errno;
                                if (errorCode == IMError.USER_REAL_NAME_NEED_PHONE
                                        || errorCode == IMError.USER_REAL_NAME_AUDITING
                                        || errorCode == IMError.USER_REAL_NAME_NEED_VERIFIED) {
                                    getMvpView().showRoomGuideDialog();
                                } else if (errorCode == IMError.IM_USER_HAS_BEEN_BANNED_) {// 被禁言
                                    getMvpView().toast("你已被禁言，无法发送信息");
                                } else if (errorCode == IMError.IM_USER_NOT_MICR) {
                                    getMvpView().toast(imReportBean.getReportData().errmsg);
                                }
                            }
                        }

                        @Override
                        public void onFail(int code, String error) {
                            super.onFail(code, error);
                            if (getMvpView() != null) {
                                getMvpView().toast(error);
                            }

                        }
                    });
                }
            }
        }
    }

    /**
     * 发送弹幕
     *
     * @param content
     */
    public void sendBarrage(String content) {
        UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomSendMsg());//统计
        RoomInfo roomInfo = getCurrRoomInfo();
        if (roomInfo == null || TextUtils.isEmpty(content) || getMvpView() == null) {
            return;
        }
        if (roomInfo.getPublicChatSwitch() == 1) {//禁止了公屏
            getMvpView().toast("公屏消息已经被管理员禁止，请稍候再试");
        } else {
            if (content.length() > 20) {
                getMvpView().toast("当前弹幕文本长度" + content.length() + "已超过最大20限制");
            } else {
                String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
                if (!TextUtils.isEmpty(sensitiveWordData) && content.matches(sensitiveWordData)) {//敏感词检测
                    getMvpView().toast(R.string.sensitive_word_data);
                } else {
                    model.sendBarrage(roomInfo.getRoomId(), CoreManager.getCore(IAuthCore.class).getCurrentUid()
                            , CoreManager.getCore(IAuthCore.class).getTicket(), content, new OkHttpManager.MyCallBack<Json>() {
                                @Override
                                public void onError(Exception e) {
                                    LogUtil.d("sendBarrage onFailure: " + e.toString());
                                }

                                @Override
                                public void onResponse(Json response) {
                                    LogUtil.d("sendBarrage onSuccess: " + "response: " + response.num("code"));
                                    if (getMvpView() == null) {
                                        return;
                                    }
                                    int barrageNum = 0;
                                    if (response.num("code") == 200) {
                                        Json data = response.json("data");
                                        if (data != null)
                                            barrageNum = data.num("barrageNum");
                                        getMvpView().onBarrageCallback(200, "", barrageNum);
                                    } else {
                                        getMvpView().onBarrageCallback(response.num("errno"), response.str("errmsg"), barrageNum);
                                    }
                                }
                            });
                }
            }
        }
    }


    /**
     * 修改房间标题
     */
    public void updateRoomInfo(boolean isRoomOwner, String title) {
        model.updateRoomInfo(isRoomOwner, title, new HttpRequestCallBack<Object>() {

            @Override
            public void onSuccess(String message, Object response) {
                if (getMvpView() != null) {
                    getMvpView().toast("房间名称修改成功");
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast("房间名称修改失败 : " + "code : " + code + ", msg : " + msg);
                }
            }
        });
    }

    /**
     * 下麦（自己下麦）
     *
     * @param microPosition 麦位
     */
    public void downMicro(int microPosition) {
        IMNetEaseManager.get().downMicroPhoneBySdk(microPosition, null);
    }

    public void downMicro(int microPosition, CallBack<String> callBack) {
        IMNetEaseManager.get().downMicroPhoneBySdk(microPosition, callBack);
    }

    /**
     * 踢人下麦
     *
     * @param microPosition 麦位
     * @param targetUid     此人的uid
     */
    public void kickDownMicro(int microPosition, long targetUid) {
        if (AvRoomDataManager.get().getRoomInfo() != null) {
            IMNetEaseManager.get().kickMicroPhoneBySdk(microPosition, targetUid, AvRoomDataManager.get().getRoomInfo().getRoomId());
        }
    }

    /**
     * 送礼物
     *
     * @param targetUid  单人时不能为0
     * @param targetUids 单人时为空、多人时不能为空
     */
    public void sendGift(int giftId, long targetUid, List<Long> targetUids, int giftNum) {
        model.sendGift(giftId, targetUid, targetUids, giftNum, new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean needRefresh) {
                if (needRefresh != null && needRefresh && getMvpView() != null) {
                    getMvpView().refreshGiftSelector();
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    switch (code) {
                        case 8018:
                            getMvpView().showLackOfCandyDialog();
                            break;
                        case 2103:
                            getMvpView().showRechargeTipDialog();
                            break;
                        default:
                            getMvpView().toast(error);
                            break;
                    }
                }
            }
        });
    }

    /**
     * 视频房送礼物
     *
     * @param targetUid  单人时不能为0
     * @param targetUids 单人时为空、多人时不能为空
     */
    public void sendGift(int giftId, long targetUid, List<Long> targetUids, int giftNum, long comboId, long comboCount, long comboFrequencyCount) {
        model.sendGift(giftId, targetUid, targetUids, giftNum, comboId, comboCount, comboFrequencyCount, new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean needRefresh) {
                if (needRefresh != null && needRefresh && getMvpView() != null) {
                    getMvpView().refreshGiftSelector();
                }
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    switch (code) {
                        case 8018:
                            getMvpView().showLackOfCandyDialog();
                            break;
                        case 2103:
                            getMvpView().showRechargeTipDialog();
                            break;
                        default:
                            getMvpView().toast(error);
                            break;
                    }
                }
            }
        });
    }

    /**
     * 设置等待麦序的信息（等待人数）
     */
    public void setWaitQueuePersonInfo(WaitQueuePersonInfo waitQueuePersonInfo) {
        AvRoomDataManager.get().setWaitQueuePersonInfo(waitQueuePersonInfo);
    }

    /**
     * 获取microView的position
     */
    public int getMicroViewPositionByMicPosition(int micPosition) {
        return AvRoomDataManager.getMicroViewPositionByMicPosition(micPosition);
    }

    /**
     * 根据uid获取所在的麦位
     *
     * @return 麦位
     */
    public int getMicPositionByUid(long uid) {
        return AvRoomDataManager.get().getMicPosition(uid);
    }

    private MsgModel msgModel;

    /**
     * 提交举报
     *
     * @param type       '类型 1. 用户 2. 房间',
     * @param targetUid  对方的uid（如果是房间，传房主UID）
     * @param reportType '举报类型 1.政治 2.色情 3.广告 4.人身攻击 ',
     */
    public void commitReport(int type, long targetUid, int reportType) {
        model.commitReport(type, targetUid, reportType, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
                if (getMvpView() != null) {
                    getMvpView().toast("举报成功，我们会尽快为您处理");
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    /**
     * 获取默认选中的礼物赠送对象
     */
    public long getGiftDefaultSelectMemberUid() {
        return model.getGiftDefaultSelectMemberUid();
    }

    /**
     * @param type 2:新手礼包
     */
    public void getActionDialogState(int type) {
        model.getActionDialogState(type, new OkHttpManager.MyCallBack<ServiceResult<List<ActionDialogInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onGetActionDialogError(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<ActionDialogInfo>> data) {
                if (null != data && data.isSuccess()) {
                    if (getMvpView() != null && data.getData() != null) {
                        getMvpView().onGetActionDialog(data.getData());
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onGetActionDialogError(data.getErrorMessage());
                    }
                }
            }
        });
    }

    /**
     * 获取幸运宝箱倒计时
     */
    public void getLuckyBoxDetail() {
        if (getCurrRoomInfo() == null) {
            return;
        }
        model.getLuckyBoxDetail(getCurrRoomInfo().getRoomId(), new HttpRequestCallBack<RoomLuckyBoxInfo>() {
            @Override
            public void onSuccess(String message, RoomLuckyBoxInfo response) {
                if (getMvpView() != null) {
                    getMvpView().showLuckyBoxView(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {

            }
        });
    }


    /**
     * 刷新管理员列表
     */
    public void refreshRoomManagerMember() {
        model.getRoomManagers(new OkHttpManager.MyCallBack<IMReportResult<List<IMChatRoomMember>>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(IMReportResult<List<IMChatRoomMember>> response) {
                if (response != null && response.isSuccess() && response.getData() != null && !ListUtils.isListEmpty(response.getData())) {
                    AvRoomDataManager.get().mRoomManagerList = response.getData();
                }
            }
        });
    }

    /**
     * 获取房间游戏
     */
    public RoomInfo.GameBean getGame(int type) {
        if (AvRoomDataManager.get().getRoomInfo() == null || AvRoomDataManager.get().getRoomInfo().getRoomGameConfig() == null)
            return null;

        for (RoomInfo.GameBean game : AvRoomDataManager.get().getRoomInfo().getRoomGameConfig()) {
            if (game.getId() == type) {
                return game;
            }
        }
        return null;
    }
}