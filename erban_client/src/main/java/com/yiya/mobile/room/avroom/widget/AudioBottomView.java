package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yiya.mobile.room.avroom.other.BottomViewListenerWrapper;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtils;

public class AudioBottomView extends BaseBottomView implements View.OnClickListener {
    private BottomViewListenerWrapper wrapper;
    private ImageView openMic;
    private LinearLayout sendMsg;
    private TextView ivSendMsg;
    private ImageView sendGift;
    private ImageView sendFace;
    private ImageView remoteMute;
    private RelativeLayout rlMsg;
    private ImageView ivMsgMark;
    private boolean micInListOption;

    public AudioBottomView(Context context) {
        super(context);
        init();
    }

    public AudioBottomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AudioBottomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_room_audio_bottom_view, this);
        Json configData = CoreManager.getCore(VersionsCore.class).getConfigData();
        int micInListOption = configData.num("micInListOption");
        LogUtils.d("configData", configData + "");
        this.micInListOption = micInListOption == 1;
        openMic = (ImageView) findViewById(R.id.icon_room_open_mic);
        sendMsg = (LinearLayout) findViewById(R.id.icon_room_send_msg);
        ivSendMsg = findViewById(R.id.iv_room_send_msg);
        sendFace = (ImageView) findViewById(R.id.icon_room_face);
        sendGift = (ImageView) findViewById(R.id.icon_room_send_gift);
//        ivRedPackage = findViewById(R.id.iv_room_red_package);
        remoteMute = (ImageView) findViewById(R.id.icon_room_open_remote_mic);
        rlMsg = findViewById(R.id.rl_room_msg);
        ivMsgMark = findViewById(R.id.iv_room_msg_mark);
        openMic.setOnClickListener(this);
        sendMsg.setOnClickListener(this);
        sendFace.setOnClickListener(this);
        sendGift.setOnClickListener(this);
        remoteMute.setOnClickListener(this);
        rlMsg.setOnClickListener(this);

        setMicBtnEnable(false);
        setMicBtnOpen(false);
        RoomInfo currentRoomInfo = AvRoomDataManager.get().getRoomInfo();
        if (currentRoomInfo != null) {
            if (currentRoomInfo.getPublicChatSwitch() == 0) {
                setInputMsgBtnEnable(true);
            } else {
                setInputMsgBtnEnable(false);
            }
        }
    }

    public void setBottomViewCommonListener(BottomViewListenerWrapper wrapper) {
        this.wrapper = wrapper;
    }

    public void setMicBtnEnable(boolean enable) {
        if (enable) {
            openMic.setClickable(true);
            openMic.setOnClickListener(this);
        } else {
            openMic.setClickable(false);
            openMic.setOnClickListener(null);
        }
    }

    public void setMicBtnOpen(boolean isOpen) {
        LogUtil.i("mic_btn", "isOpen = " + isOpen);
        if (isOpen) {
            openMic.setImageResource(R.drawable.icon_room_open_mic);
        } else {
            openMic.setImageResource(R.drawable.icon_room_close_mic);
        }
    }

    public void setRemoteMuteOpen(boolean isOpen) {
        LogUtil.i("remote_mic_btn", "isOpen = " + isOpen);
        if (isOpen) {
            remoteMute.setImageResource(R.drawable.icon_remote_mute_close);
        } else {
            remoteMute.setImageResource(R.drawable.icon_remote_mute_open);
        }
    }

    public void showUpMicBottom() {
        openMic.setVisibility(VISIBLE);
        sendFace.setVisibility(VISIBLE);
    }

    public void showDownMicBottom() {
        openMic.setVisibility(GONE);
        sendFace.setVisibility(GONE);
    }

    /**
     * 新私聊消息提示
     *
     * @param isShow true 显示 false不显示
     */
    public void showMsgMark(boolean isShow) {
        if (ivMsgMark != null)
            ivMsgMark.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 公屏发言按钮状态
     *
     * @param enable
     */
    public void setInputMsgBtnEnable(boolean enable) {
        if(enable){
            ivSendMsg.setText("打招呼");
        } else {
            ivSendMsg.setText("公屏已关");
        }

        sendMsg.setEnabled(enable);
        ivSendMsg.setEnabled(enable);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.icon_room_open_mic:
                if (wrapper != null) {
                    wrapper.onOpenMicBtnClick();
                }
                break;
            case R.id.icon_room_send_msg:
                if (wrapper != null) {
                    wrapper.onSendMsgBtnClick();
                }
                break;
            case R.id.icon_room_send_gift:
                if (wrapper != null) {
                    wrapper.onSendGiftBtnClick();
                }
                break;
            case R.id.icon_room_face:
                if (wrapper != null) {
                    wrapper.onSendFaceBtnClick();
                }
                break;
            case R.id.icon_room_share:
                if (wrapper != null) {
                    wrapper.onShareBtnClick();
                }
                break;
            case R.id.icon_room_open_remote_mic:
                if (wrapper != null) {
                    wrapper.onRemoteMuteBtnClick();
                }
                break;
//            case R.id.icon_room_lottery_box:
//                if (wrapper != null) {
//                    wrapper.onLotteryBoxeBtnClick();
//                }
//                break;
//            case R.id.room_mic_layout_mic_in_list:
//                if (wrapper != null) {
//                    wrapper.onBuShowMicInList();
//                }
//                break;
            case R.id.rl_room_msg:
                if (wrapper != null) {
                    wrapper.onMsgBtnClick();
                }
                break;
//            case R.id.iv_room_red_package:
//                if (wrapper != null) {
//                    wrapper.onRomeRedPackage();
//                }
//                break;
            default:
        }
    }
}
