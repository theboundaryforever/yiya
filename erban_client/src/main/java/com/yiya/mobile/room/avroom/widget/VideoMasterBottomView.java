package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.yiya.mobile.room.avroom.other.BottomViewListenerWrapper;
import com.opensource.svgaplayer.SVGAImageView;
import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoMasterBottomView extends BaseBottomView {
    private BottomViewListenerWrapper wrapper;

    @BindView(R.id.rl_room_operate)
    RelativeLayout rlRoomOperate;
    @BindView(R.id.iv_room_beauty)
    ImageView ivRoomBeauty;
    @BindView(R.id.iv_room_msg_mark)
    ImageView ivMsgMark;

    @BindView(R.id.rl_room_lian_micro)
    View lianMicroRootView;
    @BindView(R.id.iv_room_lian_micro_new)
    ImageView lianMicroNewIconIv;
    @BindView(R.id.iv_room_lian_micro)
    ImageView lianMicroIv;
    @BindView(R.id.svga_room_lian_micro)
    SVGAImageView lianMicroAnimView;

    public VideoMasterBottomView(Context context) {
        super(context);
        init();
    }

    public VideoMasterBottomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VideoMasterBottomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View inflate = inflate(getContext(), R.layout.layout_room_video_master_bottom_view, this);
        ButterKnife.bind(inflate);
        setGravity(Gravity.CENTER_VERTICAL);
        setOrientation(HORIZONTAL);

        rlRoomOperate.setOnClickListener(v -> {
            if (wrapper != null) {
                wrapper.onMoreOperateBtnClick();
            }
        });
        lianMicroRootView.setOnClickListener(v -> {
            if (wrapper != null) {
                wrapper.onLianMicroBtnClick();
            }
        });
        ivRoomBeauty.setOnClickListener(v -> {
            if (wrapper != null) {
                wrapper.onBeautyBtnClick();
            }
        });
    }

    @Override
    public void setBottomViewCommonListener(BottomViewListenerWrapper wrapper) {
        this.wrapper = wrapper;
    }

    /**
     * 新私聊消息提示
     *
     * @param isShow true 显示 false不显示
     */
    public void showMsgMark(boolean isShow) {
        if (ivMsgMark != null)
            ivMsgMark.setVisibility(isShow ? VISIBLE : GONE);
    }

    /**
     * 配置连麦按钮
     *
     * @param isNewEnable         new图标是否启用
     * @param isLianMicroSelected 连麦按钮是否选中
     */
    public void setupLianMicroView(boolean isNewEnable, boolean isLianMicroSelected, OnClickListener onLianMicroClickListener) {
        lianMicroNewIconIv.setVisibility(isNewEnable ? VISIBLE : GONE);

        if (isLianMicroSelected) {
            lianMicroIv.setSelected(true);
            lianMicroAnimView.startAnimation();
        } else {
            lianMicroIv.setSelected(false);
            lianMicroAnimView.stopAnimation();
        }
        lianMicroRootView.setOnClickListener(onLianMicroClickListener);
    }
}
