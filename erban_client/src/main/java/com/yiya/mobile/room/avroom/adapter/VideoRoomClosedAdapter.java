package com.yiya.mobile.room.avroom.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.ui.widget.LiveItemView;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.LiveVideoRoomInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

public class VideoRoomClosedAdapter extends BaseQuickAdapter<LiveVideoRoomInfo.RoomListBean, BaseViewHolder> {

    private int mItemRoomWidth = 0;

    public VideoRoomClosedAdapter(Context context) {
        super(R.layout.item_home_attention_head_main);
        mItemRoomWidth = (DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 30) - DisplayUtils.dip2px(context, 10)) / 2;

    }

    @Override
    protected void convert(BaseViewHolder helper, LiveVideoRoomInfo.RoomListBean info) {
        ViewGroup.LayoutParams bgParams = helper.getView(R.id.item_bg_fl).getLayoutParams();
        bgParams.height = mItemRoomWidth;
        bgParams.width = mItemRoomWidth;
        helper.getView(R.id.item_bg_fl).setLayoutParams(bgParams);
        LiveItemView liveItemView = helper.getView(R.id.item_live_item_view);
        ViewGroup.LayoutParams layoutParams = liveItemView.getLayoutParams();
        layoutParams.width = mItemRoomWidth;
        layoutParams.height = mItemRoomWidth;
        liveItemView.setLayoutParams(layoutParams);


        ImageLoadUtils.loadImage(mContext, info.getAvatar(), liveItemView.getBgView());
        ImageLoadUtils.loadImage(mContext, info.getTagPict(), liveItemView.getTagIv());
        liveItemView.setCountNum(info.getOnlineNum());
        liveItemView.setNameStr((!TextUtils.isEmpty(info.getTitle()) ? info.getTitle() : info.getNick() ));
        liveItemView.setLocalStr(info.getCity());

        if (!TextUtils.isEmpty(info.getBadge())) {
            ImageLoadUtils.loadImage(mContext, info.getBadge(), liveItemView.getTopTagIv());
            liveItemView.getTopTagIv().setVisibility(View.VISIBLE);
        } else {
            liveItemView.getTopTagIv().setVisibility(View.INVISIBLE);
        }
    }
}
