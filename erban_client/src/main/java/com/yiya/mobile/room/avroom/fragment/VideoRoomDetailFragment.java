package com.yiya.mobile.room.avroom.fragment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.room.avroom.activity.RoomChatActivity;
import com.yiya.mobile.room.avroom.activity.RoomFinishedActivity;
import com.yiya.mobile.room.avroom.other.OnMicroItemClickListener;
import com.yiya.mobile.room.avroom.widget.VideoMicroView;
import com.yiya.mobile.room.avroom.widget.dialog.SpecialDiscountDialog;
import com.yiya.mobile.room.presenter.IVideoRoomDetailView;
import com.yiya.mobile.room.presenter.VideoRoomDetailPresenter;
import com.yiya.mobile.room.widget.dialog.HeadLineStarDialog;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.ui.widget.floatView.FloatViewManager;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.opensource.svgaplayer.SVGAImageView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.VideoRoomMessage;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.DiscountGiftSetting;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * 视频房的房间fragment
 *
 * @author zeda
 */


@CreatePresenter(VideoRoomDetailPresenter.class)
public abstract class VideoRoomDetailFragment extends BaseRoomDetailFragment<IVideoRoomDetailView, VideoRoomDetailPresenter> implements IVideoRoomDetailView {

    abstract void changeState();

    abstract void onMicroBtnClick(View v);

    protected abstract @NonNull
    ImageView getAvatarView();

    protected abstract @NonNull
    TextView getOnlineNumView();

    protected abstract @NonNull
    ImageView getRoomCloseView();

    protected abstract @NonNull
    LinearLayout getDistanceView();

    protected abstract @NonNull
    TextView getDistanceTextView();

    protected abstract @NonNull
    ImageView getSpecialDiscountView();

    @Nullable
    protected View getReceiveSumView() {
        return null;
    }

    @Nullable
    protected TextView getFollowView() {
        return null;
    }

    protected abstract SVGAImageView getAddGoldSvga();

    protected abstract void showExitTipDialog();


    @Override
    public void initiate() {
        super.initiate();

        // 关闭小礼物特效
        getGiftView().setSmallGift(false);

        changeState();
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {
        super.onSetListener();

        getAvatarView().setOnClickListener(v -> {
            showUserInfoDialog(getMvpPresenter().getCurrRoomInfo().getUid());
        });

        getRoomCloseView().setOnClickListener(v -> showExitTipDialog());

        getDistanceView().setOnClickListener(v -> {
            CommonWebViewActivity.start(mContext, BaseUrl.HEAD_LINE_STAR);
        });

        getSpecialDiscountView().setOnClickListener(v -> {
            showSpecialDiscountDialog();
        });
    }

    @Override
    public void refreshDiscountData(DiscountGiftSetting discountGiftSetting) {
        if (discountGiftSetting == null) return;
        AvRoomDataManager.get().setDiscountGiftSetting(discountGiftSetting);
        if (discountGiftSetting.getUserBuyCount() >= discountGiftSetting.getMaxBuyCount()) {
            getSpecialDiscountView().setVisibility(View.GONE);// 关闭特惠礼物入口
        } else {
            getSpecialDiscountView().setVisibility(View.VISIBLE);
            if (discountGiftSetting.getUserRead() != 1
                    && discountGiftSetting.getUserBuyCount() < discountGiftSetting.getMaxBuyCount()
                    && !AvRoomDataManager.get().isHasDelay()) {
                // 若弹出过一次或已购买次数大于最大次数，不再弹
                // 进房10秒弹出特惠礼物
                Disposable mDisposableDS = Observable.just("Amit")
                        .delay(discountGiftSetting.getWaitSecord(), TimeUnit.SECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(a -> {
                            if (AvRoomDataManager.get().isDialogShow())
                                return;
                            showSpecialDiscountDialog();
                        });
                addDisposable(mDisposableDS);
                AvRoomDataManager.get().setHasDelay(true);
            }
        }
    }

    /**
     * 刷新在线人数
     */
    @Override
    protected void refreshOnlineCount() {
        int onlineNum = getMvpPresenter().getCurrRoomInfo().getOnlineNum();
        String result;
        if (onlineNum < 10000) {
            result = String.valueOf(onlineNum);
        } else {
            result = StringUtils.transformToW(onlineNum) + "万";
        }
        getOnlineNumView().setText(result);

        getMvpPresenter().getRoomMembers(3);
    }

    @Override
    public void showGiftDialog(long targetUid) {
        getGiftSelector().setupView(targetUid);
        getGiftSelector().setGiftSelectotBtnClickListener(onGiftSelectorBtnClickListener);
        getGiftSelector().show();
    }

    /**
     * 打开特惠礼物dialog
     */
    protected void showSpecialDiscountDialog() {
//        if (AvRoomDataManager.get().isDialogShow()) {
//            // 延迟打开特惠礼物dialog
//            Disposable mDisposableHLS = Observable.just("Amit")
//                    .delay(2, TimeUnit.SECONDS)
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(v -> {
//                        showSpecialDiscountDialog();
//                    });
//            addDisposable(mDisposableHLS);
//            return;
//        }
        AvRoomDataManager.get().setDialogShow(true);

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null) return;
        SpecialDiscountDialog dialog = new SpecialDiscountDialog();
        dialog.setListener(specialDiscountDialogListener);
        dialog.show(fragmentManager, VideoRoomDetailFragment.class.getSimpleName());
    }

    private SpecialDiscountDialog.SpecialDiscountDialogListener specialDiscountDialogListener = new SpecialDiscountDialog.SpecialDiscountDialogListener() {
        @Override
        public void hideEnter() {
            if (getSpecialDiscountView() != null) {
                getSpecialDiscountView().setVisibility(View.GONE);
            }
        }
    };

    /**
     * 头条之星dialog
     */
    private void showHeadLineStarDialog() {
//        if (AvRoomDataManager.get().isDialogShow()) {
//            // 延迟头条之星dialog
//            Disposable mDisposableDS = Observable.just("Amit")
//                    .delay(2, TimeUnit.SECONDS)
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(a -> {
//                        showHeadLineStarDialog();
//                    });
//            addDisposable(mDisposableDS);
//            return;
//        }

        if (AvRoomDataManager.get().getAnnounce() == null) return;
        HeadLineStarDialog dialog = new HeadLineStarDialog(mContext);
        dialog.show();
        dialog.setData(AvRoomDataManager.get().getAnnounce());

//        AvRoomDataManager.get().setDialogShow(true);
    }


    /**
     * 麦位点击监听
     */
    @Override
    public OnMicroItemClickListener getOnMicroItemClickListener() {
        return new OnMicroItemClickListener() {

            @Override
            public void onAvatarBtnClick(int position, @Nullable IMChatRoomMember chatRoomMember) {
                if (chatRoomMember != null) {
                    showUserInfoDialog(Integer.valueOf(chatRoomMember.getAccount()));
                }
            }

            @Override
            public void onDownMicBtnClick(int micPosition) {
                getMvpPresenter().downMicro(micPosition);
            }

            @Override
            public void onGiftBtnClick(@NonNull IMChatRoomMember chatRoomMember) {
                showGiftDialog(Long.valueOf(chatRoomMember.getAccount()));
            }
        };
    }

    @Override
    public void showFinishRoomView() {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo != null) {
            RoomFinishedActivity.start(getContext(), roomInfo.getUid(), roomInfo.getType());
            getMvpPresenter().exitRoom(null);
            finish();
        } else {
            Log.e(TAG, "showFinishRoomView: roomInfo is null");
        }
    }

    /**
     * 在线列表点击事件监听
     */
    protected OnlineUserFragment.OnlineItemClick onlineItemClick = new OnlineUserFragment.OnlineItemClick() {

        @Override
        public void onItemClick(OnlineChatMember chatRoomMember) {
            if (null != chatRoomMember && null != chatRoomMember.chatRoomMember) {
                IMChatRoomMember member = chatRoomMember.chatRoomMember;
                showUserInfoDialog(Integer.valueOf(member.getAccount()));
            }
        }
    };

    /**
     * 连麦按钮点击监听
     */
    protected View.OnClickListener onLianMicroBtnClickListener = v -> {
        if (!getMvpPresenter().isLianMicroUsed()) {
            getMvpPresenter().setLianMicroUsed(true);
        }
        onMicroBtnClick(v);
    };

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceiveRecentContactChanged(List<RecentContact> imMessages) {
        changeState();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        changeState();
    }

    @Override
    public void receiveRoomEvent(RoomEvent roomEvent) {
        if (roomEvent != null && getMvpPresenter() != null) {
            int event = roomEvent.getEvent();
            switch (event) {
                case RoomEvent.ENTER_ROOM:
                    enterRoom(roomEvent);
                    break;
                case RoomEvent.ROOM_MEMBER_IN:
                case RoomEvent.ROOM_MEMBER_EXIT:
                    IMChatRoomMember member = roomEvent.getChatRoomMessage().getImChatRoomMember();
                    if (member.getTimestamp() > AvRoomDataManager.get().getTimestamp()) {
                        if (getMvpPresenter().isRoomOwner(Long.valueOf(member.getAccount()))) {// 房主
                            showFinishRoomView();
                        } else {
                            int currOnLineNum = member.getOnline_num();
                            RoomInfo currRoomInfo = getMvpPresenter().getCurrRoomInfo();
                            if (currRoomInfo != null) {
                                currRoomInfo.setOnlineNum(currOnLineNum);
                                //                            getMvpPresenter().getCurrRoomInfo().setOnlineNum(currOnLineNum);
                                //当房间人员变化的时候记录一下变化的时间戳
                                getMvpPresenter().setTimestampOnMemberChanged(member.getTimestamp());
                                //刷新在线人数显示
                                refreshOnlineCount();
                            }
                        }
                    }
                    break;
                case RoomEvent.CODE_INVITE_MIC_ON_VIDEO_ROOM:
//                    showReceiveInviteUpMicroDialog((VideoRoomMessage) roomEvent.getChatRoomMessage());
                    break;
                case RoomEvent.CODE_APPLY_WAIT_QUEUE_NOTICE:
                    VideoRoomMessage videoRoomMessage = (VideoRoomMessage) roomEvent.getChatRoomMessage();
                    getMvpPresenter().setWaitQueuePersonInfo(videoRoomMessage.getWaitQueuePersonInfo());
                    break;
                case RoomEvent.DOWN_MIC:
                case RoomEvent.KICK_DOWN_MIC:
                    getMvpPresenter().setInLianMicro(false);
                    refreshLianMicroViewStatus();
                    break;
                case RoomEvent.CODE_ROOM_RECEIVE_SUM:
                    if (getReceiveSumView() != null) {
                        ((TextView) getReceiveSumView()).setText(roomEvent.getReceiveSum());
                    }
                    break;
                case RoomEvent.CODE_FIRST_FRAME://音视频第一帧
                    if (getMicroView() instanceof VideoMicroView) {
                        ((VideoMicroView) getMicroView()).startFirstFrame();
                    }
                    break;
                case RoomEvent.ROOM_LUCKY_GIFT:
                    // 收到幸运礼物
                    showAddGoldSvga();
                    break;
                case RoomEvent.HEAD_LINE_STAR_DISTANCE:
                    // 头条之星距离上一名
                    if (roomEvent.getHeadLineStarDistance() == null) return;
                    refreshDistanceView(roomEvent.getHeadLineStarDistance().getDistance());
                    break;
                case RoomEvent.HEAD_LINE_STAR_ANNOUNCE:
                    // 头条之星dialog
                    AvRoomDataManager.get().setAnnounce(roomEvent.getHeadLineStarAnnounce());
                    showHeadLineStarDialog();
                    break;
                default:
                    break;
            }
        }
        super.receiveRoomEvent(roomEvent);
    }

    /**
     * 用户成功进入房间处理
     */
    private void enterRoom(RoomEvent roomEvent) {
        RoomInfo roomInfo = roomEvent.getRoomInfo();
        if (roomInfo != null) {
            refreshDistanceView(roomEvent.getRoomInfo().getHeadlineStarDistance());
        }

//        // 进房20秒弹出头条之星
//        Disposable mDisposableHLS = Observable.just("Amit")
//                .delay(20, TimeUnit.SECONDS)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe();
//        addDisposable(mDisposableHLS);
    }

    /**
     * 头条之星距离上一名
     */
    private void refreshDistanceView(long distance) {
        String tmp = null;
        if (distance == -1) {// 为第一名
            tmp = "当前头条第一";
        } else {
            tmp = "距离上一名 " + distance + "分";
        }

        getDistanceTextView().setText(tmp);
    }

    /**
     * 幸运礼物加币特效
     */
    private void showAddGoldSvga() {
        if (getAddGoldSvga() == null || getAddGoldSvga().isAnimating()) {
            return;
        }
        getAddGoldSvga().startAnimation();
    }

    @Override
    protected void onBottomMsgClick() {
        RoomChatActivity.startDialogActivity(getContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        FloatViewManager.get().attach(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        Disposable mDisposableDS = Observable.just("Amit")
                .delay(2, TimeUnit.SECONDS)
                .repeat(4)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(a -> {
                    getMvpPresenter().getDiscountGiftSetting();
                });
        addDisposable(mDisposableDS);
    }

    @Override
    public void onStop() {
        super.onStop();
        FloatViewManager.get().detach(getActivity());
    }

}
