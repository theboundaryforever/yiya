package com.yiya.mobile.room.avroom.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.uinfo.constant.GenderEnum;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.yiya.mobile.ui.widget.HeadWearImageView;
import com.yiya.mobile.utils.ImageLoadUtils;

import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import io.reactivex.disposables.Disposable;

/**
 * <p> 房间在线人数列表 （上麦，房主，游客，管理员）  </p>
 *
 * @author Administrator
 * @date 2017/12/4
 */


public class OnlineUserAdapter extends BaseQuickAdapter<OnlineChatMember, BaseViewHolder> {

    private Disposable mDisposable;
    private boolean isVideoRoom;
    private boolean isCanHoldMic;

    public OnlineUserAdapter(boolean isVideoRoom) {
        super(R.layout.item_audio_room_online_user);
        this.isVideoRoom = isVideoRoom;
        isCanHoldMic = AvRoomDataManager.get().isRoomOwner() || AvRoomDataManager.get().isRoomAdmin();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        registerRoomEvent();
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, OnlineChatMember onlineChatMember) {
        if (onlineChatMember != null && onlineChatMember.chatRoomMember != null) {
            IMChatRoomMember member = onlineChatMember.chatRoomMember;
            final ImageView sexImage = baseViewHolder.getView(R.id.iv_sex);

            NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(member.getAccount());
            if (nimUserInfo == null) {
                NimUserInfoCache.getInstance().getUserInfoFromRemote(member.getAccount(), new RequestCallbackWrapper<NimUserInfo>() {

                    @Override
                    public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                        if (nimUserInfo != null) {
                            if (nimUserInfo.getGenderEnum() == GenderEnum.MALE) {
//                                sexImage.setVisibility(View.VISIBLE);
                                sexImage.setImageResource(R.drawable.ic_online_male);
                            } else if (nimUserInfo.getGenderEnum() == GenderEnum.FEMALE) {
                                sexImage.setVisibility(View.VISIBLE);
                                sexImage.setImageResource(R.drawable.ic_online_female);
                            } else {
                                sexImage.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            } else {
                if (nimUserInfo.getGenderEnum() == GenderEnum.MALE) {
                    sexImage.setVisibility(View.VISIBLE);
                    sexImage.setImageResource(R.drawable.ic_online_male);
                } else if (nimUserInfo.getGenderEnum() == GenderEnum.FEMALE) {
                    sexImage.setVisibility(View.VISIBLE);
                    sexImage.setImageResource(R.drawable.ic_online_female);
                } else {
                    sexImage.setVisibility(View.GONE);
                }
            }

            HeadWearImageView hwIv = baseViewHolder.getView(R.id.hw_iv);
            hwIv.setAvatar(member.getAvatar());
            hwIv.setHeadWear(member.getHeadwear_url());

            TextView nick = baseViewHolder.getView(R.id.nick);
            nick.setText(member.getNick());

//            LevelView mLevelView = baseViewHolder.getView(R.id.iv_lv);
            ImageView ivUserLevel = baseViewHolder.getView(R.id.iv_user_level);
            ivUserLevel.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(mContext, member.getExper_level_pic(), ivUserLevel);
            //是否在麦上
            baseViewHolder.getView(R.id.tv_on_mic).setVisibility(member.is_on_mic() ? View.VISIBLE : View.GONE);
        }
    }

    private void registerRoomEvent() {
        mDisposable = IMNetEaseManager.get()
                .getChatRoomEventObservable()
                .subscribe(roomEvent -> {
                    if (roomEvent == null) {
                        return;
                    }
                    int event = roomEvent.getEvent();
                    if (roomEvent.getEvent() == RoomEvent.ADD_BLACK_LIST ||
                            roomEvent.getEvent() == RoomEvent.DOWN_MIC ||
                            event == RoomEvent.ROOM_MEMBER_EXIT ||
                            roomEvent.getEvent() == RoomEvent.KICK_OUT_ROOM) {
                        if (roomEvent.getEvent() == RoomEvent.ADD_BLACK_LIST ||
                                roomEvent.getEvent() == RoomEvent.KICK_OUT_ROOM) {
                            if (mListener != null
                                    && !AvRoomDataManager.get().isOwner(CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
                                mListener.addMemberBlack();
                                return;
                            }
                        }
                        if (ListUtils.isListEmpty(mData)) {
                            return;
                        }
                        if (roomEvent.getEvent() == RoomEvent.DOWN_MIC) {
                            updateDownUpMic(roomEvent.getAccount(), false);
                            return;
                        }
                        ListIterator<OnlineChatMember> iterator = mData.listIterator();
                        for (; iterator.hasNext(); ) {
                            OnlineChatMember onlineChatMember = iterator.next();
                            if (onlineChatMember.chatRoomMember != null && Objects.equals(onlineChatMember.chatRoomMember.getAccount(), roomEvent.getAccount())) {
                                iterator.remove();
                            }
                        }
                        notifyDataSetChanged();
                    } else if (roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_ADD
                            || roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_REMOVE) {
                        updateManager(roomEvent);
                    } else if (roomEvent.getEvent() == RoomEvent.UP_MIC) {
                        updateDownUpMic(roomEvent.getAccount(), true);
                    } else if (event == RoomEvent.ROOM_MEMBER_IN) {
                        updateMemberIn(roomEvent);
                    }
                });
    }

    private void updateMemberIn(RoomEvent roomEvent) {
        if (mListener != null) {

            mListener.onMemberIn(roomEvent.getAccount(), mData, roomEvent.getChatRoomMessage());
        }
    }

    private void updateManager(RoomEvent roomEvent) {
        if (mListener != null)
            mListener.onUpdateMemberManager(roomEvent.getAccount(),
                    roomEvent.getEvent() == RoomEvent.ROOM_MANAGER_REMOVE, mData);
    }


    private void updateDownUpMic(String account, boolean isUpMic) {
        if (mListener != null) {
            mListener.onMemberDownUpMic(account, isUpMic, mData);
        }
    }

    public void release() {
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
    }

    private OnRoomOnlineNumberChangeListener mListener;

    public void setListener(OnRoomOnlineNumberChangeListener listener) {
        mListener = listener;
    }

    public interface OnRoomOnlineNumberChangeListener {
        /**
         * 成员进来回调
         *
         * @param account
         * @param chatRoomMessage
         */
        void onMemberIn(String account, List<OnlineChatMember> dataList, ChatRoomMessage chatRoomMessage);

        /**
         * 成员上下麦更新
         *
         * @param account
         * @param isUpMic
         * @param dataList
         */
        void onMemberDownUpMic(String account, boolean isUpMic,
                               List<OnlineChatMember> dataList);

        /**
         * 设置管理员回调
         *
         * @param account
         * @param dataList
         */
        void onUpdateMemberManager(String account, boolean isRemoveManager,
                                   List<OnlineChatMember> dataList);

        void addMemberBlack();
    }
}
