package com.yiya.mobile.room.avroom.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.faceunity.FURenderer;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.room.avroom.other.OnLivePrepareListener;
import com.yiya.mobile.room.avroom.widget.LivePrepareShareView;
import com.yiya.mobile.room.avroom.widget.dialog.LiveRoomFaceuDialog;
import com.yiya.mobile.room.model.RoomModel;
import com.yiya.mobile.ui.common.permission.PermissionActivity;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.jph.takephoto.app.TakePhotoFragment;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.listener.SingleClickListener;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RoomLiveStatus;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.fu.FuTextureCamera;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.model.RoomSettingModel;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_core.share.IShareCoreClient;
import com.tongdaxing.xchat_core.utils.ThreadUtil;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.file.JXFileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.sharesdk.framework.Platform;
import io.agora.rtc.mediaio.AgoraSurfaceView;
import io.agora.rtc.mediaio.MediaIO;

/**
 * 视频开播预览页
 */
public class LivePrepareFragment extends TakePhotoFragment {
    private static final String TAG = "LivePrepareFragment";
    private static final String CAMERA_PREFIX = "picture_";
    private final RoomSettingModel settingModel = new RoomSettingModel();
    private final RoomModel roomModel = new RoomModel();

    private String coverUrl = null;// 封面url

    @BindView(R.id.tv_lian_micro)
    TextView tvLianMicro;

    @BindView(R.id.tv_camera_flip)
    TextView tvCameraClip;
    @BindView(R.id.iv_close)
    ImageView ivClose;

    @BindView(R.id.tv_beauty)
    TextView tv_beauty;
    @BindView(R.id.tv_agreement)
    TextView tvAgreement;
    @BindView(R.id.tv_start_live)
    TextView tvStartLive;

    @BindView(R.id.iv_cover)
    ImageView ivCover;

    @BindView(R.id.et_live_title)
    EditText etLiveTitle;
    @BindView(R.id.tv_limit)
    TextView tvLimit;

    @BindView(R.id.live_prepare_share_view)
    LivePrepareShareView livePrepareShareView;

    @BindView(R.id.surface_view)
    protected AgoraSurfaceView roomOwnerView;

    private boolean mIsCanConnectMic = true;

    private boolean isDoInitResume = false;//onCreate 和 onResume 都会走一次初始化，开始的时候，用一个就行了

    private FuTextureCamera mFuTextureCamera;
    private FURenderer mFURenderer;

    private OnLivePrepareListener onLivePrepareListener;

    private boolean isFirstFrame = true;

    private Handler mainHandler = new Handler(Looper.getMainLooper());

    RoomFrameActivity.MyTouchListener myTouchListener;

    public static LivePrepareFragment newInstance() {
        return new LivePrepareFragment();
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_live_prepare;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {
        tvCameraClip.setOnClickListener(v -> {
            mFuTextureCamera.switchCameraFacing();
            AvRoomDataManager.get().setCameraFacing(mFuTextureCamera.getCameraFacing());
        });// 镜头翻转

        ivClose.setOnClickListener(v -> finish());

        ivCover.setOnClickListener(v -> {
            ButtonItem buttonItem = new ButtonItem("拍照上传", this::checkPermissionAndStartCamera);
            ButtonItem buttonItem1 = new ButtonItem("本地相册", () -> {
                String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
//                    旧
//                    File cameraOutFile = JXFileUtils.getTempFile(UserInfoModifyActivity.this, mCameraCapturingName);
                File cameraOutFile = JXFileUtils.getTempFile(mCameraCapturingName);
                if (!cameraOutFile.getParentFile().exists()) {
                    cameraOutFile.getParentFile().mkdirs();
                }
                Uri uri = Uri.fromFile(cameraOutFile);
                CompressConfig compressConfig = new CompressConfig.Builder().create();
                getTakePhoto().onEnableCompress(compressConfig, true);
                CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
                getTakePhoto().onPickFromGalleryWithCrop(uri, options);

            });
            List<ButtonItem> buttonItems = new ArrayList<>();
            buttonItems.add(buttonItem);
            buttonItems.add(buttonItem1);
            getDialogManager().showCommonPopupDialog(buttonItems, "取消", false);

        });

        tvAgreement.setOnClickListener(v -> {// 主播协议
            Intent intent = new Intent(getContext(), CommonWebViewActivity.class);
            String url = UriProvider.getBroadcastingAreementPage();
            intent.putExtra("coverUrl", url);
            startActivity(intent);
        });

        etLiveTitle.addTextChangedListener(new TextWatcher() {
                                               @Override
                                               public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                               }

                                               @Override
                                               public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                   tvLimit.setText(String.format("%s/15", s.length()));
                                               }

                                               @Override
                                               public void afterTextChanged(Editable s) {
                                               }
                                           }
        );

        tvStartLive.setOnClickListener(new SingleClickListener(1000) {
            @Override
            public void singleClick(View v) {
                Platform selectSharePlatform = livePrepareShareView.getSelectSharePlatform();
                if (selectSharePlatform != null) {
                    //先分享
                    CoreManager.getCore(IShareCore.class).shareRoom(selectSharePlatform, CoreManager.getCore(IAuthCore.class).getCurrentUid(), "");
                } else {
                    startLive();
                }
            }
        });

        tv_beauty.setOnClickListener(v -> {
            LiveRoomFaceuDialog dialog = new LiveRoomFaceuDialog(getContext());
            if (mFURenderer == null) {
                LogUtil.w("fuRender is null");
                toast("视屏渲染出错");
            } else {
                dialog.setFaceUnityControlListener(mFURenderer);
                dialog.show();
            }
        });

        //连麦开关
        tvLianMicro.setOnClickListener(v -> tvLianMicro.setSelected(mIsCanConnectMic = !tvLianMicro.isSelected()));

        myTouchListener = new RoomFrameActivity.MyTouchListener(){
            @Override
            public void onTouchEvent(MotionEvent event) {
                FragmentActivity activity = getActivity();
                if(event.getAction() == MotionEvent.ACTION_DOWN && activity != null){
                    View view = activity.getCurrentFocus(); //获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
                    if (isShouldHideInput(view, event)) {
                        hideSoftInput(view.getWindowToken());
                    }
                }
            }
        };

        FragmentActivity activity = getActivity();
        if(activity != null){ // 将myTouchListener注册到分发列表  
            ((RoomFrameActivity)activity).registerMyTouchListener(myTouchListener);
        }
    }

    private boolean isShouldHideInput(View v, MotionEvent event){
        if (v != null && (v instanceof EditText)) {
            int[] l = { 0, 0 };
            v.getLocationInWindow(l);
            int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
                    + v.getWidth();
            if (event.getX() > left && event.getX() < right && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。  
                return false;
            } else {
                return true;
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    private void hideSoftInput(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    public void initiate() {
//        GrowingIO.getInstance().setPageName(this, "视频-预览页");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "预览页");
        if (AvRoomDataManager.get().getRoomInfo() != null) {
            if (AvRoomDataManager.get().isRoomOwner()) {
                toast("你已开播");
                finish();
                return;
            } else if (AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
                toast("你已在语音房，请先退出语音房");
                finish();
                return;
            }
        }

        RtcEngineManager.get().configEngine(RoomInfo.ROOMTYPE_VIDEO_LIVE, true);

        mFURenderer = new FURenderer.Builder(BasicConfig.INSTANCE.getAppContext()).maxFaces(1).inputTextureType(FURenderer.FU_ADM_FLAG_EXTERNAL_OES_TEXTURE).build();

        mFuTextureCamera = new FuTextureCamera(BasicConfig.INSTANCE.getAppContext(), 1280, 720);
        mFuTextureCamera.setOnCaptureListener(new FuTextureCamera.OnCaptureListener() {
            @Override
            public int onTextureBufferAvailable(int textureId, byte[] buffer, int width, int height) {
                if (isFirstFrame) {
                    if (roomOwnerView != null) {
                        //这里做延时，尽量保证捕获的画面出来了再显示，避免因为透明显示了底部页面
                        mainHandler.postDelayed(() -> roomOwnerView.setBackgroundColor(getResources().getColor(R.color.transparent)), 200);
                    }
                    isFirstFrame = false;
                }
                return mFURenderer.onDrawFrame(buffer, textureId, width, height);
            }

            @Override
            public void onCapturerStarted() {
                LogUtil.i(TAG, "onCapturerStarted() called");
                mFURenderer.onSurfaceCreated();
            }

            @Override
            public void onCapturerStopped() {
                LogUtil.i(TAG, "onCapturerStopped() called");
                mFURenderer.onSurfaceDestroyed();
            }

            @Override
            public void onCameraSwitched(int facing, int orientation) {
                LogUtil.i(TAG, "onCameraSwitched() called with: facing = [" + facing + "], orientation = [" + orientation + "]");
                mFURenderer.onCameraChange(facing, orientation);
            }
        });
        if (mFuTextureCamera == null) {
            LogUtil.i("mFuTextureCamera is null");
            SingleToastUtil.showToast("抱歉，初始化摄像头失败");
            return;
        }
        // 初始化默认摄像头的方向 因为new出来的mFuTextureCamera 默认是前置
        AvRoomDataManager.get().setCameraFacing(mFuTextureCamera.getCameraFacing());

        LiveRoomFaceuDialog.initFuData(DemoCache.readFUConfigure(), mFURenderer);

        roomOwnerView.init(mFuTextureCamera.getEglContext());
        roomOwnerView.setBufferType(MediaIO.BufferType.TEXTURE);
        roomOwnerView.setPixelFormat(MediaIO.PixelFormat.TEXTURE_2D);

        if (RtcEngineManager.get().setLocalView(roomOwnerView)) {
            LogUtil.i("setLocalView is failed");
            SingleToastUtil.showToast("抱歉，初始化摄像头失败");
            return;
        }
        RtcEngineManager.get().setVideoSource(mFuTextureCamera);
        RtcEngineManager.get().startPreview(true);

        //设置连麦开关
        tvLianMicro.setSelected(mIsCanConnectMic);

        checkLiveStatus();
    }

    // 检查直播状态
    private void checkLiveStatus() {
        roomModel.liveStatus(new HttpRequestCallBack<RoomLiveStatus>() {
            @Override
            public void onSuccess(String message, RoomLiveStatus roomLiveStatus) {
                if (roomLiveStatus != null) {
                    ImageLoadUtils.loadImage(getContext(), roomLiveStatus.getAvatar(), ivCover, R.drawable.nim_avatar_default);
                    if (roomLiveStatus.getWebOnLive() != 0 || roomLiveStatus.getPhoneOnLive() != 0) {
                        if (roomLiveStatus.getWebOnLive() == 1) {// 当前在web端开播
                            showForceOnLiveDialog(roomLiveStatus);
                        } else {// 当前在手机端开播
                            // 暂无处理
                        }
                    }
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                toast(code + " : " + msg);
            }
        });
    }

    // 显示强制开播view
    private void showForceOnLiveDialog(RoomLiveStatus roomLiveStatus) {
        getDialogManager().showOkCancelDialog(roomLiveStatus.getToast(), true, new DialogManager.OkCancelDialogListener() {
            @Override
            public void onCancel() {
                finish();
            }

            @Override
            public void onOk() {
                roomModel.forceOnLive(new HttpRequestCallBack<Boolean>() {
                    @Override
                    public void onSuccess(String message, Boolean response) {
                        LogUtil.i("forceOnLive : " + response);
                    }

                    @Override
                    public void onFailure(int code, String msg) {
                        toast(msg);
                    }
                });
            }
        });
    }

    private void startLive() {
        LogUtil.i(TAG, "onLivePrepareFinish");

        ThreadUtil.runOnUiThread(() -> {
            String title = etLiveTitle.getText().toString();
            roomOwnerView.setBackgroundColor(getResources().getColor(R.color.video_micro_view_bg));

            if (onLivePrepareListener != null) {
                onLivePrepareListener.onLivePrepareFinish(coverUrl, title, mIsCanConnectMic);
            }
        });
    }

    /**
     * 设置直播预览监听
     */
    public void setOnLivePrepareListener(OnLivePrepareListener onLivePrepareListener) {
        this.onLivePrepareListener = onLivePrepareListener;
    }

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, Manifest.permission.CAMERA);
    }

    PermissionActivity.CheckPermListener checkPermissionListener = this::takePhoto;

    private void takePhoto() {
        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
//        File cameraOutFile = JXFileUtils.getTempFile(UserInfoModifyActivity.this, mCameraCapturingName);
        File cameraOutFile = JXFileUtils.getTempFile(mCameraCapturingName);
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        CompressConfig compressConfig = new CompressConfig.Builder().create();
        getTakePhoto().onEnableCompress(compressConfig, false);
        CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
        getTakePhoto().onPickFromCaptureWithCrop(uri, options);
    }


    /***************************图片上传相关*****************************/
    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {// 七牛服务器上传成功
        this.coverUrl = url;
        ImageLoadUtils.loadImage(getContext(), url, ivCover, R.drawable.nim_avatar_default);
        getDialogManager().dismissDialog();
        // 由于进房会将该头像信息进行更新 不用update
//        settingModel.updateRoomInfo(RoomInfo.ROOMTYPE_VIDEO_LIVE, url, new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
//            @Override
//            public void onError(Exception e) {
//                getDialogManager().dismissDialog();
//                toast("上传图片失败");
//                LogUtil.i(TAG, String.format("updateRoomInfo fail : %s", e != null ? e.getMessage() : ""));
//            }
//
//            @Override
//            public void onResponse(ServiceResult<RoomInfo> data) {
//                getDialogManager().dismissDialog();
//                if (null != data && data.isSuccess()) {
//                    toast("上传图片成功");
//                    RoomInfo roomInfo = data.getData();
//                    ImageLoadUtils.loadImage(getContext(), roomInfo.getAvatar(), ivCover, R.drawable.nim_avatar_default);
//                } else {
//                    toast("上传失败");
//                }
//            }
//        });
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {// 七牛
        getDialogManager().dismissDialog();
        toast("上传失败");
    }

    /***************************拍照相关*****************************/
    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(getContext(), "请稍后");
        CoreManager.getCore(IFileCore.class).upload(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
    }

    /***************************分享相关*****************************/
    //------------------------------IShareCoreClient----------------------------------
    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoom() {
        LogUtil.i(TAG, "onShareRoom");
        startLive();
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoomFail() {
        LogUtil.i(TAG, "onShareRoomFail");
        startLive();
    }

    @CoreEvent(coreClientClass = IShareCoreClient.class)
    public void onShareRoomCancel() {
        LogUtil.i(TAG, "onShareRoomCancel");
        startLive();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mFuTextureCamera != null) {
            LogUtil.i(TAG, "onResume");
            if (isDoInitResume) {
                mFuTextureCamera.onResume();
            } else {
                isDoInitResume = true;
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mFuTextureCamera != null) {
            LogUtil.i(TAG, "mFuTextureCamera onPause");
            mFuTextureCamera.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 将myTouchListener注册到分发列表
        FragmentActivity activity = this.getActivity();
        if(activity != null){
            ((RoomFrameActivity)activity).unRegisterMyTouchListener(myTouchListener);
        }

        mainHandler.removeCallbacksAndMessages(null);
        if (mFuTextureCamera != null) {
            RtcEngineManager.get().startPreview(false);
            mFuTextureCamera.release();
            mFuTextureCamera = null;
        }
    }
}
