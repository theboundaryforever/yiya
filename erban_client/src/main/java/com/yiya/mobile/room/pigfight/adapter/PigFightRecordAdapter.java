package com.yiya.mobile.room.pigfight.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.bean.PigRecordInfo;

import java.lang.reflect.Field;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/26.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PigFightRecordAdapter extends BaseQuickAdapter<PigRecordInfo, BaseViewHolder> {

    public PigFightRecordAdapter() {
        super(R.layout.item_pig_fight_record);
    }

    @Override
    protected void convert(BaseViewHolder helper, PigRecordInfo item) {
        if (item == null) {
            return;
        }
        int resId = getResId("bg_pig_" + item.getWiningPidNum(), R.drawable.class);
        if (resId != -1) {
            helper.setImageResource(R.id.iv_pig, resId);
        }
    }

    private int getResId(String variableName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(variableName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
