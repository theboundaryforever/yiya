package com.yiya.mobile.room.gift.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaoyu
 * @date 2017/12/11
 */

public class GiftAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<GiftInfo> giftInfoList;

    public void setGiftInfoList(List<GiftInfo> giftInfoList) {
        this.giftInfoList = giftInfoList;
    }

    private Context context;
    private int index;

    public void setIndex(int index) {
        this.index = index;
    }

    public GiftAdapter(Context context) {
        if (this.giftInfoList == null)
            this.giftInfoList = new ArrayList<>();
        this.context = context;
    }

    public GiftInfo getIndexGift() {
        if (giftInfoList != null && giftInfoList.size() > index) {
            return giftInfoList.get(index);
        }
        return null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context).inflate(R.layout.list_item_gift, null);
        return new GiftViewHolder(root);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        GiftViewHolder holder = (GiftViewHolder) viewHolder;
        GiftInfo giftInfo = giftInfoList.get(position);
        holder.tvGiftName.setText(giftInfo.getGiftName());
        holder.tvGiftGold.setText(giftInfo.getGoldPrice() + "");
        if (giftInfo.getUserGiftPurseNum() > 0) {
            holder.tvFreeGiftCount.setVisibility(View.VISIBLE);
            holder.tvFreeGiftCount.setText("X" + giftInfo.getUserGiftPurseNum());
        } else {
            holder.tvFreeGiftCount.setVisibility(View.GONE);
        }

        holder.bind(position);
        ImageLoadUtils.loadImage(BasicConfig.INSTANCE.getAppContext(), giftInfo.getGiftUrl(), holder.ivGiftImage);
        holder.giftImageSelected.setVisibility(position == index ? View.VISIBLE : View.GONE);
        holder.tvGiftName.setSelected(position == index);
        holder.tvGiftGold.setSelected(position == index);
        holder.llGiftTags.removeAllViews();
        for (String url : giftInfo.getSubPicUrls()) {
            ImageView iv = new ImageView(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(DisplayUtils.dip2px(context, 15), DisplayUtils.dip2px(context, 15));
            layoutParams.setMarginEnd(DisplayUtils.dip2px(context, 5));
            iv.setLayoutParams(layoutParams);
            ImageLoadUtils.loadImage(BasicConfig.INSTANCE.getAppContext(), url, iv);
            holder.llGiftTags.addView(iv);
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return giftInfoList == null ? 0 : giftInfoList.size();
    }

    class GiftViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivGiftImage;
        private ImageView giftImageSelected;
        private TextView tvGiftName;
        private TextView tvGiftGold;
        private ImageView ivRoomGiftSelect;
        private ImageView ivGiftEffect;
        private ImageView ivGiftNew;
        private ImageView ivGiftLimitTime;
        private ImageView ivGiftLucky;
        private TextView tvFreeGiftCount;
        private LinearLayout llGiftTags;

        GiftViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ivGiftImage = (ImageView) itemView.findViewById(R.id.ivGiftImage);
            giftImageSelected = itemView.findViewById(R.id.gift_image_selected);
            tvGiftGold = (TextView) itemView.findViewById(R.id.tvGiftGold);
            tvGiftName = (TextView) itemView.findViewById(R.id.tvGiftName);

//            ivGiftEffect = (ImageView) itemView.findViewById(R.id.ivGiftEffect);
//            ivGiftNew = (ImageView) itemView.findViewById(R.id.ivGiftNew);
//            ivGiftLimitTime = (ImageView) itemView.findViewById(R.id.ivGiftLimitTime);
//            ivGiftLucky = (ImageView) itemView.findViewById(R.id.ivGiftLucky);

            ivRoomGiftSelect = (ImageView) itemView.findViewById(R.id.ivRoomGiftSelect);
            tvFreeGiftCount = (TextView) itemView.findViewById(R.id.tvFreeGiftCount);
            llGiftTags = (LinearLayout) itemView.findViewById(R.id.ll_gift_tags);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, position);
            }
        }

        int position;

        public void bind(int position) {
            this.position = position;
        }
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
