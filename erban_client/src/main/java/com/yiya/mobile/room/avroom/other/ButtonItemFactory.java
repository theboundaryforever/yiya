package com.yiya.mobile.room.avroom.other;

import android.content.Context;

import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.im.IMReportBean;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.room.gift.GiftDialog;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import RoomBlackListActivity;
//import RoomManagerListActivity;
//import com.yiya.mobile.room.widget.dialog.AuctionDialog;
//import UserInfoActivity;
//import WalletActivity;
//import UIHelper;
//import com.tongdaxing.xchat_core.gift.GiftInfo;
//import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
//import com.tongdaxing.xchat_core.room.model.AuctionModel;
//import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/12
 * 描述        用户基本信息修改的底部弹窗工厂类
 *
 * @author dell
 */
public class ButtonItemFactory {

    /**
     * 公屏点击的所有items
     *
     * @return -
     */
//    public static List<ButtonItem> createAllRoomPublicScreenButtonItems(Context context, String account) {
//        if (AvRoomDataManager.get().getRoomInfo() == null
//                || TextUtils.isEmpty(account)) return null;
//        List<ButtonItem> buttonItems = new ArrayList<>();
//        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
//        String roomId = String.valueOf(roomInfo.getRoomId());
//
//        IMChatRoomMember chatRoomMember = AvRoomDataManager.get().getChatRoomMember(account);
//        LogUtils.d("ButtonItemFactory", "account:" + account + "   " + "chatRoomMember:" + chatRoomMember);
//        boolean isMyself = AvRoomDataManager.get().isOwner(account);
//        boolean isTargetGuess = AvRoomDataManager.get().isGuess(account);
//        boolean isTargetRoomAdmin = AvRoomDataManager.get().isRoomAdmin(account);
//        boolean isTargetRoomOwner = AvRoomDataManager.get().isRoomOwner(account);
//        boolean isTargetOnMic = AvRoomDataManager.get().isOnMic(account);
//        // 房主点击
//        if (AvRoomDataManager.get().isRoomOwner()) {
//            if (isMyself) {
//                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
//            } else if (isTargetRoomAdmin || isTargetGuess) {
//                //送礼物
//                buttonItems.add(createSendGiftItem(context, account));
//                // 查看资料
//                buttonItems.add(createCheckUserInfoDialogItem(context, account));
//                // 抱Ta上麦
//                if (!isTargetOnMic)
//                    buttonItems.add(createInviteOnMicItem(account));
//                // 踢出房间
//                buttonItems.add(createKickOutRoomItem(context, chatRoomMember, roomId, account));
//                // 增加/移除管理员
//                buttonItems.add(createMarkManagerListItem(roomId, account, isTargetGuess));
//                // 加入黑名单
//                buttonItems.add(createAddRoomBlackListItem(context, chatRoomMember, roomId));
//            }
//        } else if (AvRoomDataManager.get().isRoomAdmin()) {
//            if (isMyself) {
//                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
//            } else if (isTargetRoomAdmin || isTargetRoomOwner) {
//                if (isTargetOnMic) {
//                    new UserInfoDialog(context, JavaUtil.str2long(account)).show();
//                    return null;
//                }
//                //送礼物
//                buttonItems.add(createSendGiftItem(context, account));
//                // 查看资料
//                buttonItems.add(createCheckUserInfoDialogItem(context, account));
//                // 抱Ta上麦
//                buttonItems.add(createInviteOnMicItem(account));
//            } else if (isTargetGuess) {
//                //送礼物
//                buttonItems.add(createSendGiftItem(context, account));
//                // 查看资料
//                buttonItems.add(createCheckUserInfoDialogItem(context, account));
//                // 抱Ta上麦
//                if (!isTargetOnMic)
//                    buttonItems.add(createInviteOnMicItem(account));
//                // 踢出房间
//                buttonItems.add(createKickOutRoomItem(context, chatRoomMember, roomId, account));
//                // 加入黑名单
//                buttonItems.add(createAddRoomBlackListItem(context, chatRoomMember, roomId));
//            }
//        } else if (AvRoomDataManager.get().isGuess()) {
//            if (isMyself) {
//                new UserInfoDialog(context, JavaUtil.str2long(account)).show();
//            } else {
//                showGiftDialog(context, account);
//            }
//            return null;
//        }
//        return buttonItems;
//    }

    /**
     * 出现礼物弹框
     *
     * @param context
     * @param account
     */
    private static void showGiftDialog(Context context, String account) {
        GiftDialog giftDialog = new GiftDialog(context, JavaUtil.str2long(account));
        giftDialog.setGiftDialogBtnClickListener(new GiftDialog.OnGiftDialogBtnClickListener() {
            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long uid, int number) {
                RoomInfo currentRoomInfo = AvRoomDataManager.get().getRoomInfo();
                if (giftInfo != null && currentRoomInfo != null && (!ListUtils.isListEmpty(micMemberInfos) || uid == 0)) {
                    if (!ListUtils.isListEmpty(micMemberInfos)) {
                        List<Long> targetUids = new ArrayList<>();
                        for (int i = 0; i < micMemberInfos.size(); i++) {
                            targetUids.add(micMemberInfos.get(i).getUid());
                        }
                        CoreManager.getCore(IGiftCore.class).sendRoomMultiGift(giftInfo.getGiftId(), targetUids, currentRoomInfo.getUid(), currentRoomInfo.getType(), number, giftInfo.getGoldPrice());
                    } else {
                        CoreManager.getCore(IGiftCore.class).sendRoomGift(giftInfo.getGiftId(), uid, currentRoomInfo.getUid(), currentRoomInfo.getType(), number);
                    }
                }
            }

            @Override
            public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long uid, int number, long comboId, long comboCount) {

            }

            @Override
            public void onUserInfoClick(long uid) {

            }
        });
        giftDialog.show();
    }


    public interface OnItemClick {
        void itemClick();
    }

    public static ButtonItem createMsgBlackListItem(String s, OnItemClick onItemClick) {
        return new ButtonItem(s, onItemClick::itemClick);
    }


    /**
     * 踢Ta下麦
     */
    public static ButtonItem createKickDownMicItem(final String account) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.embrace_down_mic), () -> {
            if (AvRoomDataManager.get().isOnMic(JavaUtil.str2long(account))) {
                int micPosition = AvRoomDataManager.get().getMicPosition(JavaUtil.str2long(account));
                RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
                if (roomInfo != null) {
                    IMNetEaseManager.get().kickMicroPhoneBySdk(micPosition, JavaUtil.str2long(account), roomInfo.getRoomId());
                }
            }
        });
    }

    /**
     * 拉他上麦
     */
    public static ButtonItem createInviteOnMicItem(final String account) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.embrace_up_mic), () -> {
            int freePosition = AvRoomDataManager.get().findFreePosition();
            if (freePosition == Integer.MIN_VALUE) {
                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_empty_mic));
                return;
            }
            IMNetEaseManager.get().inviteMicroPhoneBySdk(JavaUtil.str2long(account),
                    freePosition);
        });
    }

    /**
     * 踢出房间:  先强制下麦，再踢出房间
     */
    public static ButtonItem createKickOutRoomItem(final Context context, final IMChatRoomMember chatRoomMember, final String roomId, final String account) {
        return new ButtonItem("踢出房间", () -> {
            if (chatRoomMember != null) {
                ((BaseMvpActivity) context).getDialogManager().showOkCancelDialog("是否将" + chatRoomMember.getNick() + "踢出房间？", true, new DialogManager.AbsOkDialogListener() {
                    @Override
                    public void onOk() {

                        IMNetEaseManager.get().kickMember(chatRoomMember.getAccount(), new OkHttpManager.MyCallBack<Json>() {
                            @Override
                            public void onError(Exception e) {
                                SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), "网络异常");
                            }

                            @Override
                            public void onResponse(Json response) {
                                IMReportBean imReportBean = new IMReportBean(response);
                                if (imReportBean.getReportData().errno != 0) {
                                    SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), imReportBean.getReportData().errmsg + "");
                                } else {

                                }
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     * 下麦
     */
    public static ButtonItem createDownMicItem() {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.down_mic_text), () -> {
            long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            IMNetEaseManager.get().downMicroPhoneBySdk(
                    AvRoomDataManager.get().getMicPosition(currentUid), null);
        });
    }

    //设置管理员
    public static ButtonItem createMarkManagerListItem(final String roomId, final String account, final boolean mark) {
        String title = BasicConfig.INSTANCE.getAppContext().getString(mark ? R.string.set_manager : R.string.remove_manager);
        return new ButtonItem(title, () -> IMNetEaseManager.get().markManager(account, mark, null));
    }

    //加入房间黑名单
    public static ButtonItem createAddRoomBlackListItem(final Context context, final IMChatRoomMember chatRoomMember, boolean isRoomOwner) {
        final String roomText;
        if(isRoomOwner){
            roomText = "加入黑名单";
        } else {
            roomText = "加入房间黑名单";
        }
        long roomId = AvRoomDataManager.get().getRoomInfo().getRoomId();
        String uid = chatRoomMember.getAccount();
        boolean isAdd = true;
        boolean synPersonBlackList = true;
        return new ButtonItem(roomText, () -> {
            if (chatRoomMember != null) {
                ((BaseMvpActivity) context).getDialogManager().showOkCancelDialog(
                        "是否将该用户" + roomText +"？加入后他将无法进入该房间", true,
                        new DialogManager.AbsOkDialogListener() {
                            @Override
                            public void onOk() {
                                IMNetEaseManager.get().addRoomBlackList(String.valueOf(roomId), uid, true, true, new OkHttpManager.MyCallBack<Json>() {
                                    @Override
                                    public void onError(Exception e) {
                                        SingleToastUtil.showToast(roomText+"成功，他将无法进入该房间");
                                    }

                                    @Override
                                    public void onResponse(Json json) {
                                        if (json.num("errno") == 0) {
                                                SingleToastUtil.showToast("加入黑名单成功，他将无法进入该房间");
                                        } else {
                                            SingleToastUtil.showToast(json.num("errno") + " : " + json.str("errmsg"));
                                        }
                                    }
                                });
                            }
                        });
            } else {
                LogUtils.d("incomingChatObserver", "无用户信息");
            }
        });
    }


    //加入私聊黑名单
    public static ButtonItem createAddUserBlackListItem(final Context context, final IMChatRoomMember chatRoomMember, final String roomId) {
        return new ButtonItem("加入个人黑名单", () -> {
            if (chatRoomMember != null) {
                ((BaseMvpActivity) context).getDialogManager().showOkCancelDialog(
                        "是否将该用户加入个人黑名单？加入后他将无法与你私聊", true,
                        new DialogManager.AbsOkDialogListener() {
                            @Override
                            public void onOk() {
                                //现在拉黑，后端会把用户从队列移除
                                IMNetEaseManager.get().addUserBlackList(chatRoomMember.getAccount(), new HttpRequestCallBack<Json>() {
                                    @Override
                                    public void onSuccess(String message, Json response) {
                                        SingleToastUtil.showToast("加入个人黑名单成功，他将无法与你私聊");
                                    }

                                    @Override
                                    public void onFailure(int code, String msg) {
                                        SingleToastUtil.showToast(code + msg);
                                    }
                                });
                            }
                        });
            } else {
                LogUtils.d("incomingChatObserver", "无用户信息");
            }
        });
    }

    //发送礼物
    public static ButtonItem createSendGiftItem(final Context context, final IMChatRoomMember chatRoomMember, final GiftDialog.OnGiftDialogBtnClickListener giftDialogBtnClickListener) {
        return new ButtonItem("送礼物", () -> {
            GiftDialog dialog = new GiftDialog(context, AvRoomDataManager.get().mMicQueueMemberMap, Long.valueOf(chatRoomMember.getAccount()));
            if (giftDialogBtnClickListener != null) {
                dialog.setGiftDialogBtnClickListener(giftDialogBtnClickListener);
            }
            dialog.show();

        });
    }

    public static ButtonItem createSendGiftItem(final Context context, String uid) {
        return new ButtonItem("送礼物", () -> showGiftDialog(context, uid));
    }

    /**
     * 禁麦
     */
    public static ButtonItem createLockMicItem(final int position, ButtonItem.OnClickListener onClickListener) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.forbid_mic), onClickListener);
    }

    /**
     * 取消禁麦
     */
    public static ButtonItem createFreeMicItem(final int position, ButtonItem.OnClickListener onClickListener) {
        return new ButtonItem(BasicConfig.INSTANCE.getAppContext().getString(R.string.no_forbid_mic), onClickListener);
    }

    /**
     * 举报功能按钮
     *
     * @param context
     * @param title   房间内为举报房间  个人为举报
     * @param type    个人 和 房间举报
     * @return
     */
    public static ButtonItem createReportItem(Context context, String title, int type, long uid) {
        return new ButtonItem(title, () -> {
            if (context == null) {
                return;
            }
            List<ButtonItem> buttons = new ArrayList<>();
            ButtonItem button1 = new ButtonItem("政治敏感", () -> reportCommit(context, type, 1, uid));
            ButtonItem button2 = new ButtonItem("色情低俗", () -> reportCommit(context, type, 2, uid));
            ButtonItem button3 = new ButtonItem("广告骚扰", () -> reportCommit(context, type, 3, uid));
            ButtonItem button4 = new ButtonItem("人身攻击", () -> reportCommit(context, type, 4, uid));
            buttons.add(button1);
            buttons.add(button2);
            buttons.add(button3);
            buttons.add(button4);
            DialogManager dialogManager = null;
            if (context instanceof BaseMvpActivity) {
                dialogManager = ((BaseMvpActivity) context).getDialogManager();
            } else if (context instanceof BaseActivity) {
                dialogManager = ((BaseActivity) context).getDialogManager();
            }
            if (dialogManager != null) {
                dialogManager.showCommonPopupDialog(buttons, "取消");
            }
        });
    }

    /**
     * uid '被举报的用户',
     * report_uid  '举报的用户',
     * report_type '举报类型 1.政治 2.色情 3.广告 4.人身攻击 ',
     * type  		'类型 1. 用户 2. 房间',
     *
     * @param reportType
     */
    public static void reportCommit(Context context, int type, int reportType, long uid) {
        if (context != null) {
            if (context instanceof BaseMvpActivity) {
                ((BaseMvpActivity) context).toast("举报成功，我们会尽快为您处理");
            } else if (context instanceof BaseActivity) {
                ((BaseActivity) context).toast("举报成功，我们会尽快为您处理");
            }
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("reportType", reportType + "");
        params.put("type", type + "");
        params.put("phoneNo", CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo() == null ? "" : CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getPhone());
        params.put("reportUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("uid", uid + "");

        OkHttpManager.getInstance().doPostRequest(UriProvider.reportUserUrl(), params, (OkHttpManager.MyCallBack) null);
    }
}