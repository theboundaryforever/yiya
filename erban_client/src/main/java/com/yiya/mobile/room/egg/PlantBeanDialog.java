package com.yiya.mobile.room.egg;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.Group;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseMvpDialogFragment;
import com.yiya.mobile.presenter.egg.IPlantBeanView;
import com.yiya.mobile.presenter.egg.PlantBeanPresenter;
import com.yiya.mobile.room.egg.dialog.PlantCommonListDialog;
import com.yiya.mobile.room.egg.dialog.PlantResultDialog;
import com.yiya.mobile.ui.me.wallet.activity.MyWalletActivity;
import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGADynamicEntity;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.bean.DrawCfg;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.SpUtils;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.tongdaxing.xchat_core.room.bean.DrawCfg.FREE_DRAW_ENABLE;

/**
 * ProjectName:
 * Description:种豆首页dialog
 * Created by BlackWhirlwind on 2019/7/19.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
@CreatePresenter(PlantBeanPresenter.class)
public class PlantBeanDialog extends BaseMvpDialogFragment<IPlantBeanView, PlantBeanPresenter> implements IPlantBeanView {

    @BindView(R.id.iv_home_bg)
    ImageView ivHomeBg;
    @BindView(R.id.iv_recharge_bg)
    ImageView ivRechargeBg;
    @BindView(R.id.tv_gold)
    TextView tvGold;
    @BindView(R.id.iv_nutrition_free)
    ImageView ivNutritionFree;
    @BindView(R.id.iv_nutrition_once)
    ImageView ivNutritionOnce;
    @BindView(R.id.iv_nutrition_10_times)
    ImageView ivNutrition10Times;
    @BindView(R.id.tv_free_times)
    TextView tvFreeTimes;
    @BindView(R.id.iv_seed)
    ImageView ivSeed;
    @BindView(R.id.iv_rule)
    ImageView ivRule;
    @BindView(R.id.iv_rank)
    ImageView ivRank;
    @BindView(R.id.iv_reward)
    ImageView ivReward;
    @BindView(R.id.iv_nutrition_50_times)
    ImageView ivNutrition50Times;
    @BindView(R.id.iv_nutrition_100_times)
    ImageView ivNutrition100Times;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.svga_seed)
    SVGAImageView svgaSeed;
    @BindView(R.id.svga_reward)
    SVGAImageView svgaReward;
    @BindView(R.id.iv_record)
    ImageView ivRecord;
    @BindView(R.id.tv_anim_setting)
    TextView tvAnimSetting;
    @BindView(R.id.group_free_times)
    Group groupFreeTimes;
    @BindView(R.id.group_50_times)
    Group group50Times;
    @BindView(R.id.group_100_times)
    Group group100Times;

    public static final int LOTTERYTYPE_1 = 1; // 砸1次
    public static final int LOTTERYTYPE_10 = 2; // 砸10次
    public static final int LOTTERYTYPE_50 = 4; // 砸50次
    public static final int LOTTERYTYPE_100 = 5; // 砸100次
    public static final int LOTTERYTYPE_FREE = 3; // 免费

    private double EVERY_TIME_COUNT = 20;// 单位
    private double goldNum;
    private int freeTimes = 0;// 免费砸蛋熟
    private int lotteryType = 0; //当前连砸类型 砸蛋次数类别 1为1次、2为10次、4为50次
    private boolean lotteryLoading = false;
    private PlantResultDialog mPlantBeanRewardRecordDialog;
    private Unbinder unbinder;
    private boolean mIsAnimEnabled;

    public static PlantBeanDialog newInstance() {
        return new PlantBeanDialog();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View rootView = inflater.inflate(R.layout.dialog_plant_bean, window.findViewById(android.R.id.content), false);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setWindowAnimations(R.style.WindowBottomAnimationStyle);
        window.setGravity(Gravity.BOTTOM);

        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        CoreManager.getCore(IPayCore.class).getWalletInfo(uid);
    }

    @Override
    public int getRootLayoutId() {
        return 0;
    }

    @Override
    public void onFindViews() {
        //开奖动画
        svgaReward.setCallback(new SVGACallback() {
            @Override
            public void onFinished() {
            }

            @Override
            public void onPause() {

            }

            @Override
            public void onRepeat() {

            }

            @Override
            public void onStep(int i, double v) {
                if (v == 1) {
                    if (svgaReward == null) {
                        return;
                    }
                    resetPlantState();
                }
            }
        });
    }

    /**
     * 重置盆栽状态
     */
    private void resetPlantState() {
        if (svgaSeed == null) {
            return;
        }
        svgaSeed.stopAnimation(true);
        ivSeed.setImageResource(R.drawable.bg_plant_bean_seed);
        finishLottery();
    }

    @Override
    public void onSetListener() {
    }

    @Override
    public void initiate() {
        initGold();
        initIsAnimOpen();
        getMvpPresenter().getPoundCfg();
    }

    private void initIsAnimOpen() {
        mIsAnimEnabled = (boolean) SpUtils.get(getContext(), Constants.KEY_PLANT_BEAN_ANIM, true);
        tvAnimSetting.setSelected(!mIsAnimEnabled);
    }

    private void initGold() {
        CoreManager.getCore(IPayCore.class).getWalletInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        WalletInfo walletInfo = CoreManager.getCore(IPayCore.class).getCurrentWalletInfo();
        if (walletInfo != null) {
            goldNum = walletInfo.getGoldNum();
            tvGold.setText(getContext().getString(R.string.gold_num_text, goldNum));
        }
    }

    @OnClick({R.id.iv_recharge_bg, R.id.iv_nutrition_free, R.id.iv_nutrition_once, R.id.iv_nutrition_10_times, R.id.tv_free_times, R.id.iv_seed, R.id.iv_rule, R.id.iv_record, R.id.iv_rank, R.id.iv_reward, R.id.iv_nutrition_50_times, R.id.iv_nutrition_100_times, R.id.iv_close, R.id.tv_anim_setting,
            R.id.space_50_times, R.id.space_100_times})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_recharge_bg:
                //充值
                MyWalletActivity.start(getContext(),1);
                finishLottery();
                break;
            case R.id.iv_nutrition_free:
                // 免费浇水
                if (freeTimes <= 0) {
                    SingleToastUtil.showToast("免费次数不足，请先完成任务");
                    return;
                }
                startLottery(LOTTERYTYPE_FREE);
                break;
            case R.id.iv_nutrition_once:
                // 砸一次
                startLottery(LOTTERYTYPE_1);
                break;
            case R.id.iv_nutrition_10_times:
                // 砸十次
                UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRoomEggTen());
                startLottery(LOTTERYTYPE_10);
                break;
            case R.id.space_50_times:
                // 砸50次
                startLottery(LOTTERYTYPE_50);
                break;
            case R.id.space_100_times:
                // 砸100次
                startLottery(LOTTERYTYPE_100);
                break;
            case R.id.iv_rule:
                //规则
                showListDialog(PlantCommonListDialog.TYPE_RULE_LIST);
                break;
            case R.id.iv_record:
                //中奖记录
                showListDialog(PlantCommonListDialog.TYPE_RECORD_LIST);
                break;
            case R.id.iv_rank:
                //排行榜
                showListDialog(PlantCommonListDialog.TYPE_RANK_LIST);
                break;
            case R.id.iv_reward:
                //奖品
                showListDialog(PlantCommonListDialog.TYPE_REWARD_LIST);
                break;
            case R.id.iv_close:
                dismiss();
                break;
            case R.id.tv_anim_setting:
                //动画开关
                tvAnimSetting.setSelected(!tvAnimSetting.isSelected());
                mIsAnimEnabled = !tvAnimSetting.isSelected();
                break;
        }
    }

    private void showListDialog(int typeRecordList) {
        PlantCommonListDialog.newInstance(typeRecordList)
                .show(getChildFragmentManager(), null);
    }

    // 刷新种豆配置
    public void refreshCfg(DrawCfg drawCfg) {
        if (drawCfg == null) return;
        groupFreeTimes.setVisibility(drawCfg.getFreeDrawEnable() == FREE_DRAW_ENABLE ? View.VISIBLE : View.GONE);
        freeTimes = drawCfg.getFreeDrawTimes();
        tvFreeTimes.setText(getString(R.string.txt_pound_egg_free_time, drawCfg.getFreeDrawTimes() > 0 ? drawCfg.getFreeDrawTimes() : 0));

        List<DrawCfg.DrawConfList> drawConfList = drawCfg.getDrawConfList();
        if (ListUtils.isNotEmpty(drawConfList)) {
            for (DrawCfg.DrawConfList drawConf : drawConfList) {
                if (drawConf == null) {
                    return;
                }
                switch (drawConf.getDrawType()) {
                    case LOTTERYTYPE_1:
                        ivNutritionOnce.setVisibility(View.VISIBLE);
                        break;
                    case LOTTERYTYPE_10:
                        ivNutrition10Times.setVisibility(View.VISIBLE);
                        break;
                    case LOTTERYTYPE_50:
                        group50Times.setVisibility(View.VISIBLE);
                        break;
                    case LOTTERYTYPE_100:
                        group100Times.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }
    }

    /**
     * 开始砸蛋
     *
     * @param type 次数类别 1为1次、2为10次、4为50次
     */
    private void startLottery(int type) {
        if (lotteryLoading) {
//            SingleToastUtil.showShortToast("请稍候");
            return;
        }
        lotteryType = type;
        lotteryLoading = true;
        if (type == LOTTERYTYPE_FREE) {
            getMvpPresenter().poundEggFree(mIsAnimEnabled);
        } else {
            getMvpPresenter().getPoundEggFreeReward(type, mIsAnimEnabled);
        }
    }

    /**
     * 再砸
     */
    private void continueLottery() {
        if (lotteryLoading) {
            SingleToastUtil.showShortToast("请稍候");
            return;
        }
        lotteryLoading = true;
        getMvpPresenter().getPoundEggFreeReward(lotteryType, false);
    }

    /**
     * 种豆结束
     */
    @Override
    public void showPoundEggReward(List<EggGiftInfo> eggGiftInfos, boolean playAnim) {
        //dialog销毁之后不执行方法
        if (getContext() == null) {
            return;
        }
        if (ListUtils.isListEmpty(eggGiftInfos)) {
            finishLottery();
            return;
        }
        if (playAnim) {
            //种豆动画
            startGrowingUpAnim(eggGiftInfos);
        } else {
            //直接展示结果
            if (lotteryType == LOTTERYTYPE_FREE || lotteryType == LOTTERYTYPE_1) {
                //免费、1次
                startGiftAnim(eggGiftInfos.get(0), false);
            } else {
                showResultDialog(eggGiftInfos);
            }
        }
    }

    /**
     * 刷新金币
     */
    @Override
    public void refreshGold() {
        double subtractNum = 0;
        switch (lotteryType) {
            case 1:
                subtractNum = EVERY_TIME_COUNT;
                break;
            case 2:
                subtractNum = EVERY_TIME_COUNT * 10;
                break;
            case 4:
                subtractNum = EVERY_TIME_COUNT * 50;
                break;
            case 5:
                subtractNum = EVERY_TIME_COUNT * 100;
                break;
        }
        goldNum -= subtractNum;
        CoreManager.getCore(IPayCore.class).minusGold(subtractNum);
        if (tvGold != null) {
            tvGold.setText(getContext().getString(R.string.gold_num_text, goldNum));
        }
    }

    /**
     * 种豆长大动画
     */
    private void startGrowingUpAnim(List<EggGiftInfo> eggGiftInfos) {
        lotteryLoading = true;
        svgaSeed.setCallback(new SVGACallback() {
            @Override
            public void onFinished() {
            }

            @Override
            public void onPause() {

            }

            @Override
            public void onRepeat() {

            }

            @Override
            public void onStep(int i, double v) {
                if (i == 0) {
                    ivSeed.setImageResource(R.drawable.bg_plant_bean_seed_empty);
                } else if (v == 1) {
                    if (svgaSeed == null) {
                        return;
                    }
                    if (lotteryType == LOTTERYTYPE_FREE || lotteryType == LOTTERYTYPE_1) {
                        startGiftAnim(eggGiftInfos.get(0), true);
                    } else {
                        resetPlantState();
                        //展示中奖dialog
                        showResultDialog(eggGiftInfos);
                    }
                }
            }
        });
        svgaSeed.startAnimation();
    }

    /**
     * 礼物开奖动画
     *
     * @param eggGiftInfo
     * @param playAnim
     */
    private void startGiftAnim(EggGiftInfo eggGiftInfo, boolean playAnim) {
        if (eggGiftInfo.getGiftId() == 0) {
            finishLottery();
            return;
        }
        SVGAParser parser = new SVGAParser(mContext);
        parser.decodeFromAssets("plantbeanreward.svga", new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                if (svgaReward == null) {
                    return;
                }
                SVGADynamicEntity dynamicEntity = new SVGADynamicEntity();
                dynamicEntity.setDynamicImage(eggGiftInfo.getPicUrl(), "img_250");
                SVGADrawable svgaDrawable = new SVGADrawable(svgaVideoEntity, dynamicEntity);
                svgaReward.setImageDrawable(svgaDrawable);
                if (playAnim) {
                    svgaReward.startAnimation();
                } else {
                    svgaReward.stepToFrame(33, true);
                }
            }

            @Override
            public void onError() {

            }
        });
    }


    /**
     * 展示中奖dialog
     *
     * @param eggGiftInfos
     */
    private void showResultDialog(List<EggGiftInfo> eggGiftInfos) {
        finishLottery();
        if (mPlantBeanRewardRecordDialog == null) {
            mPlantBeanRewardRecordDialog = PlantResultDialog.newInstance(eggGiftInfos, lotteryType)
                    .setOnNutritionClickListener(new PlantResultDialog.OnNutritionClickListener() {
                        @Override
                        public void onNutritionClick() {
                            continueLottery();
                        }

                        @Override
                        public void onDismiss() {
                            mPlantBeanRewardRecordDialog = null;
                        }
                    });
            mPlantBeanRewardRecordDialog.show(getChildFragmentManager(), null);
        } else {
            mPlantBeanRewardRecordDialog.setData(eggGiftInfos);
        }
    }

    @Override
    public void showPoundEggErrorAndToast(String error) {
        finishLottery();
        if (getContext() != null) {
            SingleToastUtil.showShortToast(error);
        }
    }

    /**
     * 结束砸蛋
     */
    private void finishLottery() {
        lotteryLoading = false;
        Log.d(TAG, "onLotterySuccess: finishLottery");
    }

    @Override
    public void showPoundEggFreeTimes(int times) {
        freeTimes = times;
        tvFreeTimes.setText(getString(R.string.txt_pound_egg_free_time, times > 0 ? times : 0));
    }

    @Override
    public void showPoundEggFreeBtn(boolean isShow) {
        groupFreeTimes.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    /**
     * 更新金额信息
     */
    private void updateWalletInfo(WalletInfo walletInfo) {
        if (walletInfo != null) {
            goldNum = walletInfo.getGoldNum();
            if (tvGold != null) {
                tvGold.setText(getContext().getString(R.string.gold_num_text, goldNum));
            }
        }
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onGetWalletInfo(WalletInfo walletInfo) {
        updateWalletInfo(walletInfo);
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        updateWalletInfo(walletInfo);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SpUtils.put(getContext(), Constants.KEY_PLANT_BEAN_ANIM, mIsAnimEnabled);
        release();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    private void release() {
        if (svgaSeed != null) {
            svgaSeed.stopAnimation();
            svgaSeed.setCallback(null);
        }
        if (svgaReward != null) {
            svgaReward.stopAnimation();
            svgaReward.setCallback(null);
        }
    }

    @Override
    public void finish() {

    }

    @Override
    public void dismissDialog() {

    }
}
