package com.yiya.mobile.room.avroom.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.bean.LiveRoomFinishInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.yiya.mobile.base.fragment.BaseFragment;
import com.yiya.mobile.room.model.RoomModel;
import com.yiya.mobile.utils.ImageLoadUtils;

/**
 * @author Zhangsongzhou
 * @date 2019/6/4
 */
public class LiveRoomOwnerFinishFragment extends BaseFragment {

    public static LiveRoomOwnerFinishFragment newInstance(long roomId, long uid) {
        Bundle bundle = new Bundle();
        bundle.putLong("uid", uid);
        bundle.putLong("roomId", roomId);
        bundle.putString("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        LiveRoomOwnerFinishFragment fragment = new LiveRoomOwnerFinishFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private RoomModel mRoomModel;

    private TextView mLiveTimeTv, mEarningsTv, mBeholderTv, mAddFansTv;
    private Button mExitBtn;
    private ImageView mAvatarIv;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_live_room_finish;
    }

    private void getData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            long roomId = bundle.getLong("roomId");
            long uid = bundle.getLong("uid");
            mRoomModel.getLiveRoomFinishInfo(roomId, uid, CoreManager.getCore(IAuthCore.class).getTicket(), new HttpRequestCallBack<LiveRoomFinishInfo>() {
                @Override
                public void onSuccess(String message, LiveRoomFinishInfo response) {
                    updateUI(response);
                }

                @Override
                public void onFailure(int code, String msg) {
                    toast(msg);
                }
            });
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo != null) {
                ImageLoadUtils.loadImage(getContext(), userInfo.getAvatar(), mAvatarIv);
            }
        }
    }

    private void updateUI(LiveRoomFinishInfo info) {
        if (info == null) {
            return;
        }

        mLiveTimeTv.setText(!TextUtils.isEmpty(info.getLiveTime()) ? info.getLiveTime() : "00:00:00");
        mBeholderTv.setText(info.getWatchNum() + "");
        mEarningsTv.setText(info.getRedBeanNum() + "咿呀");
        mAddFansTv.setText(info.getFansNum() + "");
    }

    @Override
    public void onFindViews() {
        mAvatarIv = mView.findViewById(R.id.avatar_iv);
        mLiveTimeTv = mView.findViewById(R.id.live_time_tv);
        mEarningsTv = mView.findViewById(R.id.earnings_tv);
        mBeholderTv = mView.findViewById(R.id.beholder_tv);
        mAddFansTv = mView.findViewById(R.id.add_fans_tv);
        mExitBtn = mView.findViewById(R.id.exit_btn);
    }

    @Override
    public void onSetListener() {
        mExitBtn.setOnClickListener(v -> finish());
    }

    @Override
    public void initiate() {
        mRoomModel = new RoomModel();
        getData();
    }
}
