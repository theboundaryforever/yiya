package com.yiya.mobile.room.presenter;

import android.support.annotation.NonNull;

import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.room.bean.DiscountGiftSetting;
import com.tongdaxing.xchat_core.room.bean.LianMicroStatusInfo;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo;

import java.util.List;

public interface IVideoRoomDetailView extends IRoomDetailView {

    /**
     * 显示结束直播界面
     */
    void showFinishRoomView();

    /**
     * 更新连麦按钮状态
     */
    default void refreshLianMicroViewStatus() {
    }

    default void refreshLianMicroViewStatus(RoomMicroApplyInfo roomMicroApplyInfo) {
    }

    /**
     * 更新在线用户头像
     */
    void refreshOnlineView(List<IMChatRoomMember> memberList);

    /**
     * 显示连麦申请弹框
     *
     * @param statusInfo 连麦状态信息
     */
    default void showLianMicroApplyDialog(@NonNull LianMicroStatusInfo statusInfo) {
    }

    /**
     * 关闭连麦申请弹框
     */
    default void dismissLianMicroApplyDialog() {

    }

    /**
     * 获取特惠礼物dialog
     */
    default void refreshDiscountData(DiscountGiftSetting discountGiftSetting) {

    }
}

