package com.yiya.mobile.room.avroom.other;

/**
 * Created by chenran on 2017/11/21.
 */

public abstract class BottomViewListenerWrapper {
    public BottomViewListenerWrapper() {

    }

    public void onOpenMicBtnClick() {

    }

    public void onSendMsgBtnClick() {

    }

    public void onSendFaceBtnClick() {

    }

    public void onSendGiftBtnClick() {

    }

    public void onShareBtnClick() {

    }

    public void onRemoteMuteBtnClick() {

    }

    public void onMsgBtnClick() {

    }

    public void onMoreOperateBtnClick() {

    }

    public void onLianMicroBtnClick() {

    }

    public void onBeautyBtnClick() {

    }
}
