package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.yiya.mobile.room.avroom.other.OnMicroItemClickListener;

import static com.tongdaxing.xchat_core.manager.AvRoomDataManager.MIC_POSITION_BY_OWNER;

public abstract class BaseMicroView extends RelativeLayout {
    /**
     * 刷新所有麦位
     */
    public static final int MIC_POSITION_ALL = MIC_POSITION_BY_OWNER - 1;

    public BaseMicroView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public abstract void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener);

    /**
     * 刷新麦位
     *
     * @param micPosition 麦位position 刷新所有麦位传MIC_POSITION_ALL
     */
    public abstract void refreshMicroView(int micPosition);

    public abstract void release();
}
