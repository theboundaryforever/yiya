package com.yiya.mobile.room.widget.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.yiya.mobile.room.avroom.adapter.RoomConsumeListAdapter;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.tongdaxing.xchat_core.UriProvider.JAVA_WEB_URL;

/**
 * Created by MadisonRong on 13/01/2018.
 */

public class BigListDataDialog extends BaseDialogFragment implements View.OnClickListener, BaseQuickAdapter.OnItemClickListener,
        RadioGroup.OnCheckedChangeListener {

    public static final String TYPE_CONTRIBUTION = "ROOM_CONTRIBUTION";
    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_TYPE = "KEY_TYPE";

    @BindView(R.id.user_rank)
    RadioGroup userRank;
    @BindView(R.id.bu_pay_tab)
    TextView buPayTab;
    @BindView(R.id.bu_imcome_tab)
    TextView buImcomeTab;
    @BindView(R.id.iv_close_dialog)
    ImageView ivCloseDialog;
    @BindView(R.id.rv_pay_income_list)
    RecyclerView rvPayIncomeList;
    Unbinder unbinder;


    private int type = 1;
    private int dataType = 1;
    private long roomId;
    private RoomConsumeListAdapter roomConsumeListAdapter;
    private View noDataView;

    public static BigListDataDialog newContributionListInstance(Context context) {
        return newInstance(context.getString(R.string.contribution_list_text), TYPE_CONTRIBUTION);
    }

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, null);
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();

    }

    public static BigListDataDialog newInstance(String title, String type) {
        BigListDataDialog listDataDialog = new BigListDataDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE, title);
        bundle.putString(KEY_TYPE, type);
        listDataDialog.setArguments(bundle);
        return listDataDialog;
    }

    public BigListDataDialog() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_big_list_data, window.findViewById(android.R.id.content), false);
        unbinder = ButterKnife.bind(this, view);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        setCancelable(true);

        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo != null) {
            roomId = roomInfo.getUid();
        }
        view.findViewById(R.id.iv_close_dialog).setOnClickListener(this);
        noDataView = view.findViewById(R.id.tv_no_data);
        unbinder = ButterKnife.bind(this, view);
        initView();
        initRv();
        return view;
    }

    private void initRv() {
        rvPayIncomeList.setLayoutManager(new LinearLayoutManager(getContext()));
        roomConsumeListAdapter = new RoomConsumeListAdapter(getContext());
        roomConsumeListAdapter.setOnItemClickListener(this);
        rvPayIncomeList.setAdapter(roomConsumeListAdapter);
        getData();

    }

    private void initView() {
        buPayTab.setSelected(true);

        buPayTab.setOnClickListener(v -> typeChange(1));
        buImcomeTab.setOnClickListener(v -> typeChange(2));

        userRank.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        switch (checkedId) {
            case R.id.rb_tab_day:
                dataTypeChange(1, false);
                break;
            case R.id.rb_tab_week:
                dataTypeChange(2, false);
                break;
            case R.id.rb_tab_all:
                dataTypeChange(3, false);
                break;
            default:
                break;
        }
    }

    private void dataTypeChange(int i, boolean force) {
        if (!force && i == dataType) {
            return;
        }
        dataType = i;
        if (selectOptionAction != null) {
            selectOptionAction.optionClick();
        }
        getData();
    }


    private void getData() {
        Map<String,String> param = CommonParamUtil.getDefaultParam();
        param.put("uid",String.valueOf(roomId));
        param.put("dataType",String.valueOf(dataType));
        param.put("type",String.valueOf(type));
        String url = JAVA_WEB_URL + "/roomctrb/queryByType";
        OkHttpManager.getInstance().getRequest(url, param, new OkHttpManager.MyCallBack<ServiceResult<List<RoomConsumeInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (selectOptionAction != null) {
                    selectOptionAction.onDataResponse();
                }
            }

            @Override
            public void onResponse(ServiceResult<List<RoomConsumeInfo>> response) {
                if (selectOptionAction != null) {
                    selectOptionAction.onDataResponse();
                }
                if (response.isSuccess()) {
                    if (response.getData() != null) {
                        noDataView.setVisibility(response.getData().size() > 0 ? View.GONE : View.VISIBLE);
                        roomConsumeListAdapter.setNewData(response.getData());
                    }
                }
            }
        });
    }

    private void typeChange(int i) {
        if (i == type) {
            return;
        }
        if (roomConsumeListAdapter != null) {
            if (i == 1) {
                roomConsumeListAdapter.rankType = 0;
            } else {
                roomConsumeListAdapter.rankType = 1;
            }
        }

        dataType = 1;
        type = i;

        buPayTab.setSelected(i == 1);
        buImcomeTab.setSelected(i == 2);

        userRank.check(R.id.rb_tab_day);
        dataTypeChange(dataType, true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_close_dialog:
                dismiss();
                break;
        }
    }

    @Override
    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
        List<RoomConsumeInfo> list = roomConsumeListAdapter.getData();
        if (ListUtils.isListEmpty(list)) {
            return;
        }
        RoomConsumeInfo roomConsumeInfo = list.get(i);
        YiYaUserInfoDialog.newInstance(roomConsumeInfo.getUid())
                .show(getChildFragmentManager(), null);
    }

    private SelectOptionAction selectOptionAction;

    public void setSelectOptionAction(SelectOptionAction selectOptionAction) {
        this.selectOptionAction = selectOptionAction;
    }

    public interface SelectOptionAction {

        void optionClick();
        void onDataResponse();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}


//public class BigListDataDialog extends BaseDialogFragment
//        implements View.OnClickListener, BaseQuickAdapter.OnItemClickListener,
//        RadioGroup.OnCheckedChangeListener {
//
//    public static final String TYPE_ONLINE_USER = "ONLINE_USER";
//    public static final String TYPE_CONTRIBUTION = "ROOM_CONTRIBUTION";
//    public static final String KEY_TITLE = "KEY_TITLE";
//    public static final String KEY_TYPE = "KEY_TYPE";
//
//    @BindView(R.id.user_rank)
//    RadioGroup userRank;
//    @BindView(R.id.bu_pay_tab)
//    TextView buPayTab;
//    @BindView(R.id.bu_imcome_tab)
//    TextView buImcomeTab;
//    @BindView(R.id.iv_close_dialog)
//    ImageView ivCloseDialog;
//    @BindView(R.id.rv_pay_income_list)
//    RecyclerView rvPayIncomeList;
//    Unbinder unbinder;
//
//
//    private int type = 1;
//    private int dataType = 1;
//    private long roomId;
//    private RoomConsumeListAdapter roomConsumeListAdapter;
//    private View noDataView;
//
//    public static BigListDataDialog newOnlineUserListInstance(Context context) {
//        return newInstance(context.getString(R.string.online_user_text), TYPE_ONLINE_USER);
//    }
//
//    public static BigListDataDialog newContributionListInstance(Context context) {
//        return newInstance(context.getString(R.string.contribution_list_text), TYPE_CONTRIBUTION);
//    }
//
//    public void show(FragmentManager fragmentManager) {
//        show(fragmentManager, null);
//    }
//
//    @Override
//    public int show(FragmentTransaction transaction, String tag) {
//        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.add(this, tag).addToBackStack(null);
//        return transaction.commitAllowingStateLoss();
//
//    }
//
//    public static BigListDataDialog newInstance(String title, String type) {
//        BigListDataDialog listDataDialog = new BigListDataDialog();
//        Bundle bundle = new Bundle();
//        bundle.putString(KEY_TITLE, title);
//        bundle.putString(KEY_TYPE, type);
//        listDataDialog.setArguments(bundle);
//        return listDataDialog;
//    }
//
//    public BigListDataDialog() {
//
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
//            @Nullable Bundle savedInstanceState) {
//        Window window = getDialog().getWindow();
//        // setup window and width
//        View view = inflater.inflate(R.layout.dialog_big_list_data, window.findViewById(android.R.id.content), false);
//        unbinder = ButterKnife.bind(this, view);
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        window.setGravity(Gravity.BOTTOM);
//        setCancelable(true);
//
//        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
//        if (roomInfo != null) {
//            roomId = roomInfo.getUid();
//        }
//        view.findViewById(R.id.iv_close_dialog).setOnClickListener(this);
//        noDataView = view.findViewById(R.id.tv_no_data);
//        unbinder = ButterKnife.bind(this, view);
//        initView();
//        initRv();
//        return view;
//    }
//
//    private void initRv() {
//        rvPayIncomeList.setLayoutManager(new LinearLayoutManager(getContext()));
//        roomConsumeListAdapter = new RoomConsumeListAdapter(getContext());
//        roomConsumeListAdapter.setOnItemClickListener(this);
//        rvPayIncomeList.setAdapter(roomConsumeListAdapter);
//        getData();
//
//    }
//
//    private void initView() {
//        buPayTab.setSelected(true);
//
//        buPayTab.setOnClickListener(v -> typeChange(1));
//        buImcomeTab.setOnClickListener(v -> typeChange(2));
//
//        userRank.setOnCheckedChangeListener(this);
//    }
//
//    @Override
//    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
//        switch (checkedId) {
//            case R.id.rb_tab_day:
//                dataTypeChange(1, false);
//                break;
//            case R.id.rb_tab_week:
//                dataTypeChange(2, false);
//                break;
//            case R.id.rb_tab_all:
//                dataTypeChange(3, false);
//                break;
//            default:
//                break;
//        }
//    }
//
//    private void dataTypeChange(int i, boolean force) {
//        if (!force && i == dataType) {
//            return;
//        }
//        dataType = i;
//        if (selectOptionAction != null) {
//            selectOptionAction.optionClick();
//        }
//        getData();
//    }
//
//
//    private void getData() {
//        Map<String,String> param = CommonParamUtil.getDefaultParam();
//        param.put("uid",String.valueOf(roomId));
//        param.put("dataType",String.valueOf(dataType));
//        param.put("type",String.valueOf(type));
//        String url = JAVA_WEB_URL + "/roomctrb/queryByType";
//        OkHttpManager.getInstance().getRequest(url, param, new OkHttpManager.MyCallBack<ServiceResult<List<RoomConsumeInfo>>>() {
//            @Override
//            public void onError(Exception e) {
//                if (selectOptionAction != null) {
//                    selectOptionAction.onDataResponse();
//                }
//            }
//
//            @Override
//            public void onResponse(ServiceResult<List<RoomConsumeInfo>> response) {
//                if (selectOptionAction != null) {
//                    selectOptionAction.onDataResponse();
//                }
//                if (response.isSuccess()) {
//                    if (response.getData() != null) {
//                        noDataView.setVisibility(response.getData().size() > 0 ? View.GONE : View.VISIBLE);
//                        roomConsumeListAdapter.setNewData(response.getData());
//                    }
//                }
//            }
//        });
//    }
//
//    private void typeChange(int i) {
//        if (i == type) {
//            return;
//        }
//        if (roomConsumeListAdapter != null) {
//            if (i == 1) {
//                roomConsumeListAdapter.rankType = 0;
//            } else {
//                roomConsumeListAdapter.rankType = 1;
//            }
//        }
//
//        dataType = 1;
//        type = i;
//
//        buPayTab.setSelected(i == 1);
//        buImcomeTab.setSelected(i == 2);
//
//        userRank.check(R.id.rb_tab_day);
//        dataTypeChange(dataType, true);
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.iv_close_dialog:
//                dismiss();
//                break;
//        }
//    }
//
//    @Override
//    public void onItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
//        List<RoomConsumeInfo> list = roomConsumeListAdapter.getData();
//        if (ListUtils.isListEmpty(list)) {
//            return;
//        }
//        RoomConsumeInfo roomConsumeInfo = list.get(i);
//        UserInfoDialog userInfoDialog = new UserInfoDialog(getContext(), roomConsumeInfo.getCtrbUid());
//        userInfoDialog.show();
//    }
//
//    private SelectOptionAction selectOptionAction;
//
//    public void setSelectOptionAction(SelectOptionAction selectOptionAction) {
//        this.selectOptionAction = selectOptionAction;
//    }
//
//    public interface SelectOptionAction {
//
//        void optionClick();
//        void onDataResponse();
//
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        unbinder.unbind();
//    }
//}
