package com.yiya.mobile.room.model;

import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;
import com.tongdaxing.xchat_core.gift.GiftSendInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.ReUsedSocketManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.redpacket.bean.ActionDialogInfo;
import com.tongdaxing.xchat_core.room.bean.LianMicroStatusInfo;
import com.tongdaxing.xchat_core.room.bean.RoomClosedBean;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.im.IMProCallBack;
import com.tongdaxing.xchat_framework.im.IMReportBean;
import com.tongdaxing.xchat_framework.im.IMReportResult;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.config.SpEvent;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SpUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager.DEFAULT_CODE_ERROR;
import static com.tongdaxing.xchat_framework.im.IMReportBean.CODE_NO_ERROR;

/**
 * 房间model
 *
 * @author zeda
 */
public class RoomModel extends BaseMvpModel {

    public static final int ERROR_CODE_AUDIO_CHANNEL_INIT = -1102;
    public static final int ERROR_CODE_DEFAULT = 0;
    public static final int ERROR_CODE_VIDEO_LIVE_FINISH = 1501;
    private static final String EXCEPTION_MSG_ERROR = "数据异常，请尝试重新进房";

    public RoomInfo getCurrentRoomInfo() {
        return AvRoomDataManager.get().getRoomInfo();
    }

    /**
     * 直播状态判断
     *
     * @param callBack
     */
    public void liveStatus(HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("roomType", String.valueOf(RoomInfo.ROOMTYPE_VIDEO_LIVE));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        getRequest(UriProvider.liveStatus(), params, callBack);
    }

    /**
     * 强制开播
     *
     * @param callBack
     */
    public void forceOnLive(HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("clientType", String.valueOf(2));// 1 web端 2手机端
        params.put("roomType", String.valueOf(RoomInfo.ROOMTYPE_VIDEO_LIVE));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        postRequest(UriProvider.forceOnLive(), params, callBack);
    }

    /**
     * 关注房间
     *
     * @param uid
     * @param roomId
     * @param callBack
     */
    public void roomAttention(long uid, long roomId, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("roomId", String.valueOf(roomId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        postRequest(UriProvider.roomAttention(), params, callBack);
    }

    /**
     * 检查是否关注房间
     *
     * @param roomId
     * @param callBack
     */
    public void checkAttention(long roomId, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("roomId", String.valueOf(roomId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        postRequest(UriProvider.checkAttention(), params, callBack);
    }

    /**
     * 取消房间关注
     *
     * @param uid
     * @param roomId
     * @param callBack
     */
    public void deleteAttention(long uid, long roomId, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("roomId", String.valueOf(roomId));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        postRequest(UriProvider.deleteAttention(), params, callBack);
    }

    /**
     * 获取关注房间列表
     *
     * @param uid
     * @param pageNum
     * @param pageSize
     * @param callBack
     */
    public void getRoomAttentionByUid(long uid, int pageNum, int pageSize, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        getRequest(UriProvider.getRoomAttentionById(), params, callBack);
    }

    /**
     * 获取关注房间列表
     *
     * @param uid
     * @param pageNum
     * @param pageSize
     * @param callBack
     */
    public void getRoomAttentionRecommendList(long uid, int pageNum, int pageSize, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        getRequest(UriProvider.getAttentionRecommendList(), params, callBack);
    }

    /**
     * 获取房间信息
     *
     * @param roomUid
     * @param roomType
     * @param callBack
     */
    public void requestRoomInfo(long roomUid, int roomType, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("type", String.valueOf(roomType));
        params.put("roomUid", String.valueOf(roomUid));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        getRequest(UriProvider.getRoomInfo(), params, callBack);
    }

    /**
     * 获取房间结束推荐
     *
     * @param callBack
     */
    public void requestRoomClosed(HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        getRequest(UriProvider.getRoomInfo(), params, callBack);
    }

    /**
     * 检查是否已经被踢了
     */
    public boolean checkIsKick(long roomUid, int roomType) {
        int kickTime = CoreManager.getCore(VersionsCore.class).checkKick();
        if (kickTime >= 1) {
            if (kickTime > 600) {
                kickTime = 600;
            }
            String kickInfo = (String) SpUtils.get(BasicConfig.INSTANCE.getAppContext(), SpEvent.onKickRoomInfo, "");
            Json json = Json.parse(kickInfo);
            String roomUidCache = json.str(SpEvent.roomUid);
            String roomTypeCache = json.str(SpEvent.roomType);
            String time = json.str(SpEvent.time);
            if (roomUidCache.equals(String.valueOf(roomUid)) && roomTypeCache.equals(String.valueOf(roomType))) {
                int i = kickTime * 1000;
                if (BasicConfig.isDebug && i > 10000) {
                    i = 10000;
                }
                return System.currentTimeMillis() - JavaUtil.str2long(time) < i;
            }
        }
        return false;
    }

    /**
     * 获取声网动态key
     */
    public void getRoomAgoraKey(long roomId, HttpRequestCallBack<String> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("roomId", String.valueOf(roomId));
        OkHttpManager.getInstance().doPostRequest(UriProvider.getRoomAgoraKey(), params, callBack);
    }


    /**
     * 获取房间管理员列表
     */
    public void getRoomManagers(OkHttpManager.MyCallBack<IMReportResult<List<IMChatRoomMember>>> myCallBack) {
        OkHttpManager.getInstance().doPostRequest(UriProvider.fetchRoomManagers(),
                IMNetEaseManager.get().getImDefaultParamsMap(), myCallBack);
    }

    /**
     * 获取已经收到的礼物消息
     */
    public List<ChatRoomMessage> getGiftMsgList() {
        List<ChatRoomMessage> chatRoomMessages = IMNetEaseManager.get().messages;
        if (ListUtils.isListEmpty(chatRoomMessages)) {
            return null;
        }
        List<ChatRoomMessage> messages = new ArrayList<>();
        for (int i = chatRoomMessages.size() - 1; i >= 0; i--) {
            if (chatRoomMessages.get(i).getAttachment() != null) {
                if ((chatRoomMessages.get(i).getAttachment()).getFirst()
                        == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT || (chatRoomMessages.get(i).getAttachment())
                        .getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
                    messages.add(chatRoomMessages.get(i));
                }
            }
        }
        return messages;
    }

    /**
     * 当前自己是否能上这个麦位
     */
    private boolean isMeCanUpMic(int micPosition, boolean isInviteUpMic) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(micPosition);
        if (roomQueueInfo != null && roomQueueInfo.mRoomMicInfo != null) {
            IMChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
            String currentUserId = String.valueOf(getCurrentUserId());
            //防止把房主挤掉了
            if (micPosition == AvRoomDataManager.MIC_POSITION_BY_OWNER && !AvRoomDataManager.get().isRoomOwner()) {
                return false;
            }
            //坑上没人 且 （没锁 或者 我是房主 或者 我是管理员 或者 我是被邀请上麦的）
            return chatRoomMember == null
                    && ((!roomQueueInfo.mRoomMicInfo.isMicLock() || AvRoomDataManager.get().isRoomOwner(currentUserId)
                    || AvRoomDataManager.get().isRoomAdmin(currentUserId)) || isInviteUpMic);
        }
        return false;
    }

    /**
     * 操作上麦
     *
     * @param micPosition   需要上的麦位
     * @param isInviteUpMic 是否是受邀请上麦
     * @param callBack      上麦结果回调
     */
    public void operateUpMicro(int micPosition, boolean isInviteUpMic, CallBack<String> callBack) {
        //检查自己是否能上这个麦位
        if (isMeCanUpMic(micPosition, isInviteUpMic) && getCurrentRoomInfo() != null) {
            //如果是被邀请上麦的话，上麦成功后不开麦
            AvRoomDataManager.get().mIsNeedOpenMic = !isInviteUpMic;
            ReUsedSocketManager.get().updateQueue(String.valueOf(getCurrentRoomInfo().getRoomId()), micPosition,
                    getCurrentUserId(), new IMProCallBack() {
                        @Override
                        public void onSuccessPro(IMReportBean imReportBean) {
                            if (imReportBean == null || imReportBean.getReportData() == null) {
                                if (callBack != null) {
                                    callBack.onFail(-1, "数据异常！");
                                }
                                return;
                            }
                            if (imReportBean.getReportData().errno == CODE_NO_ERROR) {

                                if (callBack != null) {
                                    callBack.onSuccess("上麦成功");
                                }
                            } else {
                                if (callBack != null) {
                                    callBack.onFail(imReportBean.getReportData().errno,
                                            "上麦失败:" + imReportBean.getReportData().errmsg);
                                }
                            }
                        }

                        @Override
                        public void onError(int errorCode, String errorMsg) {
                            if (callBack != null) {
                                callBack.onFail(errorCode, "上麦失败:" + errorMsg);
                            }
                        }
                    });
        }
    }

    /**
     * 操作麦位开麦 或 关麦
     *
     * @param isOpen 是否开麦
     */
    public void operateMicroOpenOrClose(int micPosition, boolean isOpen) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null && (AvRoomDataManager.get().isRoomAdmin() || AvRoomDataManager.get().isRoomOwner())) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            param.put("position", String.valueOf(micPosition));
            param.put("state", String.valueOf(isOpen ? 0 : 1));
            param.put("roomUid", String.valueOf(roomInfo.getUid()));
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            param.put("type", String.valueOf(roomInfo.getType()));
            OkHttpManager.getInstance().doPostRequest(UriProvider.operateMicroPhone(), param,
                    (HttpRequestCallBack) null);
        }
    }

    /**
     * 操作麦位锁麦 或 解锁麦
     *
     * @param isLock 是否锁麦
     */
    public void operateMicroLockAndUnLock(int micPosition, boolean isLock, HttpRequestCallBack<String> callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null && (AvRoomDataManager.get().isRoomAdmin() || AvRoomDataManager.get().isRoomOwner())) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            param.put("position", String.valueOf(micPosition));
            param.put("state", String.valueOf(isLock ? 1 : 0));
            param.put("roomUid", String.valueOf(roomInfo.getUid()));
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            param.put("type", String.valueOf(roomInfo.getType()));
            OkHttpManager.getInstance().doPostRequest(UriProvider.getlockMicroPhone(), param, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, "");
        }
    }

    /**
     * 申请连麦
     *
     * @param type 连麦类型 0语音，1视频
     */
    public void applyLianMicro(int type, HttpRequestCallBack<Object> callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            param.put("roomId", String.valueOf(roomInfo.getRoomId()));
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            param.put("type", String.valueOf(type));
            OkHttpManager.getInstance().doPostRequest(UriProvider.applyLianMicro(), param, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, EXCEPTION_MSG_ERROR);
        }
    }

    /**
     * 邀请上麦
     *
     * @param roomUserInfos 邀请的用户
     * @param isNeedPay     是否需要收费
     */
    public void inviteUpMicroOnVideoRoom(List<RoomConsumeInfo> roomUserInfos, boolean isNeedPay,
                                         HttpRequestCallBack<Object> callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null && !ListUtils.isListEmpty(roomUserInfos)) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            StringBuilder inviteUserStrBuilder = new StringBuilder();
            for (int i = 0; i < roomUserInfos.size(); i++) {
                if (i == roomUserInfos.size() - 1) {
                    inviteUserStrBuilder.append(roomUserInfos.get(i).getUid());
                } else {
                    inviteUserStrBuilder.append(roomUserInfos.get(i).getUid()).append(",");
                }
            }
            param.put("micUid", inviteUserStrBuilder.toString());
            param.put("roomid", String.valueOf(roomInfo.getRoomId()));
            param.put("needPay", isNeedPay ? "1" : "0");
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            OkHttpManager.getInstance().doPostRequest(UriProvider.inviteUpMicroOnVideoRoom(), param, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, EXCEPTION_MSG_ERROR);
        }
    }

    /**
     * 邀请进房
     *
     * @param roomUserInfos 邀请的用户
     */
    public void inviteEnterRoom(List<RoomConsumeInfo> roomUserInfos, HttpRequestCallBack<Object> callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null && !ListUtils.isListEmpty(roomUserInfos)) {
            Map<String, String> param = CommonParamUtil.getDefaultParam();
            StringBuilder inviteUserStrBuilder = new StringBuilder();
            for (int i = 0; i < roomUserInfos.size(); i++) {
                if (i == roomUserInfos.size() - 1) {
                    inviteUserStrBuilder.append(roomUserInfos.get(i).getUid());
                } else {
                    inviteUserStrBuilder.append(roomUserInfos.get(i).getUid()).append(",");
                }
            }
            param.put("reqUid", inviteUserStrBuilder.toString());
            param.put("roomid", String.valueOf(roomInfo.getRoomId()));
            param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            OkHttpManager.getInstance().doPostRequest(UriProvider.inviteEnterRoom(), param, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, EXCEPTION_MSG_ERROR);
        }
    }

    /**
     * 更新房间信息
     *
     * @param roomTitle 房间标题
     */
    public void updateRoomInfo(boolean roomOwner, String roomTitle, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("title", roomTitle);
        String shortUrl;
        if (roomOwner) {
            shortUrl = UriProvider.updateRoomInfo();
        } else {
            params.put("roomUid", String.valueOf(getCurrentRoomInfo().getUid()));
            shortUrl = UriProvider.updateByAdimin();
        }
        params.put("type", String.valueOf(getCurrentRoomInfo().getType()));
        OkHttpManager.getInstance().doPostRequest(shortUrl, params, callBack);
    }

    /**
     * 获取房间用户在线列表信息
     */
    public void getRoomMembers(int index, int length, CallBack<List<IMChatRoomMember>> callBack) {
        Map<String, String> imDefaultParamsMap = IMNetEaseManager.get().getImDefaultParamsMap();
        if (imDefaultParamsMap == null) {
            return;
        }
        imDefaultParamsMap.put("start", String.valueOf(index));
        imDefaultParamsMap.put("limit", String.valueOf(length));
        OkHttpManager.getInstance().doPostRequest(UriProvider.fetchRoomMembers(), imDefaultParamsMap, new OkHttpManager.MyCallBack<IMReportResult<List<IMChatRoomMember>>>() {
            @Override
            public void onError(Exception e) {
                onResponse(null);
            }

            @Override
            public void onResponse(IMReportResult<List<IMChatRoomMember>> response) {
                if (callBack != null) {
                    List<IMChatRoomMember> members = response == null || response.getData() == null ? new ArrayList<>() : response.getData();
                    callBack.onSuccess(members);
                }
            }
        });
    }

    /**
     * 获取房间用户在线列表信息
     */
    public void getRoomOnlineMembers(int type, int dateType, long roomUid, CallBack<List<RankingXCInfo.ListBean>> callBack) {
        Map<String, String> imDefaultParamsMap = IMNetEaseManager.get().getImDefaultParamsMap();
        if (imDefaultParamsMap == null) {
            return;
        }
        imDefaultParamsMap.put("dataType", dateType + "");
        imDefaultParamsMap.put("type", type + "");
        imDefaultParamsMap.put("uid", String.valueOf(roomUid));
        OkHttpManager.getInstance().getRequest(UriProvider.getRoomRankingList(), imDefaultParamsMap, new OkHttpManager.MyCallBack<ServiceResult<List<RankingXCInfo.ListBean>>>() {
            @Override
            public void onError(Exception e) {
                onResponse(null);
            }

            @Override
            public void onResponse(ServiceResult<List<RankingXCInfo.ListBean>> response) {
                if (callBack != null) {
                    List<RankingXCInfo.ListBean> infos = response == null || response.getData() == null ? new ArrayList<>() : response.getData();
                    callBack.onSuccess(infos);
                }
            }
        });
    }

    /**
     * 送礼物
     *
     * @param targetUid  单人时不能为0
     * @param targetUids 单人时为空、多人时不能为空
     * @param callBack   onSuccess为true代表需要刷新礼物列表
     */
    public void sendGift(int giftId, long targetUid, List<Long> targetUids, int giftNum, CallBack<Boolean> callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();

        if (roomInfo == null || (targetUid == 0 && ListUtils.isListEmpty(targetUids))) {
            if (callBack != null) {
                callBack.onFail(DEFAULT_CODE_ERROR, "送礼失败");
            }
        } else {
            Map<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("giftId", giftId + "");//303 test
            params.put("uid", getCurrentUserId() + "");
            params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            params.put("roomUid", getCurrentRoomInfo().getUid() + "");
            params.put("giftNum", giftNum + "");
            params.put("roomType", String.valueOf(getCurrentRoomInfo().getType()));

            String url;
            if (!ListUtils.isListEmpty(targetUids)) {
                url = UriProvider.sendWholeGiftV3();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < targetUids.size(); i++) {
                    long tUid = targetUids.get(i);
                    sb.append(tUid);
                    sb.append(",");
                }
                sb.deleteCharAt(sb.length() - 1);
                params.put("targetUids", sb.toString());
            } else {
                url = UriProvider.sendGiftV3();
                params.put("targetUid", targetUid + "");
                if (targetUid == getCurrentRoomInfo().getUid()) {
                    params.put("type", "1");
                } else {
                    params.put("type", "3");
                }
            }
            OkHttpManager.getInstance().doPostRequest(url, params, new HttpRequestCallBack<GiftSendInfo>() {
                @Override
                public void onSuccess(String message, GiftSendInfo giftReceiveInfo) {
                    GiftInfo giftInfoGrowing = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftId);
                    if (giftInfoGrowing != null && roomInfo != null) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("liwujine", giftInfoGrowing.getGoldPrice());
                            jsonObject.put("liwutype", giftInfoGrowing.getGiftType());
                            jsonObject.put("fangjiantype", roomInfo.getType());
                            jsonObject.put("liwuid", giftInfoGrowing.getGiftId());

                            if (!AvRoomDataManager.get().isGrowingHasSend()) {
                                AvRoomDataManager.get().setGrowingHasSend(true);
                                long interval = (System.currentTimeMillis() - AvRoomDataManager.get().getEnterTimeStamp()) / 1000;
                                LogUtil.i("sendGift : " + interval);
                                jsonObject.put("songlitime", interval);
                            }

                            GrowingIO.getInstance().track("songli", jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    CoreManager.getCore(IPayCore.class).minusGold(giftReceiveInfo.getGoldCost());
//                    giftReceiveInfo.setRoomType(AvRoomDataManager.get().getRoomInfo().getType());
//                    giftReceiveInfo.setGiftInfo(giftInfoGrowing);
//                    sendGiftMessage(!ListUtils.isListEmpty(targetUids), giftReceiveInfo);// 发送礼物信息
                    if (callBack != null) {
                        List<GiftInfo> giftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosAll();
                        boolean needRefresh = false;
                        if (giftInfos != null) {
                            for (int i = 0; i < giftInfos.size(); i++) {
                                GiftInfo giftInfo = giftInfos.get(i);
                                if (giftInfo != null) {
                                    if (giftInfo.getGiftId() == giftReceiveInfo.getGiftId()) { // 这里进行礼物数赋值
                                        giftInfo.setUserGiftPurseNum(giftReceiveInfo.getGiftPurseAmount());
                                        giftInfo.setDisplayable(giftReceiveInfo.isDisplayable());
                                        needRefresh = true;
                                    }
                                }
                            }
                        }
                        callBack.onSuccess(needRefresh);
                    }
                }

                @Override
                public void onFailure(int code, String msg) {
                    if (callBack != null) {
                        callBack.onFail(code, msg);
                    }
                }
            });
        }
    }

    public void sendGift(int giftId, long targetUid, List<Long> targetUids, int giftNum, long comboId, long comboCount, long comboFrequencyCount, CallBack<Boolean> callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();

        if (roomInfo == null || (targetUid == 0 && ListUtils.isListEmpty(targetUids))) {
            if (callBack != null) {
                callBack.onFail(DEFAULT_CODE_ERROR, "送礼失败");
            }
        } else {
            Map<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("giftId", giftId + "");
            params.put("uid", getCurrentUserId() + "");
            params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            params.put("roomUid", getCurrentRoomInfo().getUid() + "");
            params.put("giftNum", giftNum + "");
            params.put("roomType", String.valueOf(getCurrentRoomInfo().getType()));

            String url;
            if (!ListUtils.isListEmpty(targetUids)) {
                url = UriProvider.sendWholeGiftV3();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < targetUids.size(); i++) {
                    long tUid = targetUids.get(i);
                    sb.append(tUid);
                    sb.append(",");
                }
                sb.deleteCharAt(sb.length() - 1);
                params.put("targetUids", sb.toString());
            } else {
                url = UriProvider.sendGiftV3();
                params.put("targetUid", targetUid + "");
                if (targetUid == getCurrentRoomInfo().getUid()) {
                    params.put("type", "1");
                } else {
                    params.put("type", "3");
                }
            }
            OkHttpManager.getInstance().doPostRequest(url, params, new HttpRequestCallBack<GiftSendInfo>() {
                @Override
                public void onSuccess(String message, GiftSendInfo giftReceiveInfo) {
                    GiftInfo giftInfoGrowing = CoreManager.getCore(IGiftCore.class).findGiftInfoById(giftId);
                    if (giftInfoGrowing != null && roomInfo != null) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("liwujine", giftInfoGrowing.getGoldPrice());
                            jsonObject.put("liwutype", giftInfoGrowing.getGiftType());
                            jsonObject.put("fangjiantype", roomInfo.getType());
                            jsonObject.put("liwuid", giftInfoGrowing.getGiftId());

                            if (!AvRoomDataManager.get().isGrowingHasSend()) {
                                AvRoomDataManager.get().setGrowingHasSend(true);
                                long interval = (System.currentTimeMillis() - AvRoomDataManager.get().getEnterTimeStamp()) / 1000;
                                LogUtil.i("sendGift : " + interval);
                                jsonObject.put("songlitime", interval);
                            }

                            GrowingIO.getInstance().track("songli", jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    CoreManager.getCore(IPayCore.class).minusGold(giftReceiveInfo.getGoldCost());
//                    giftReceiveInfo.setRoomType(AvRoomDataManager.get().getRoomInfo().getType());
//                    sendGiftMessage(!ListUtils.isListEmpty(targetUids), giftReceiveInfo, comboId, comboCount, comboFrequencyCount);
                    if (callBack != null) {
                        List<GiftInfo> giftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosAll();
                        boolean needRefresh = false;
                        if (giftInfos != null) {
                            for (int i = 0; i < giftInfos.size(); i++) {
                                GiftInfo giftInfo = giftInfos.get(i);
                                if (giftInfo != null) {
                                    if (giftInfo.getGiftId() == giftReceiveInfo.getGiftId()) {
                                        giftInfo.setUserGiftPurseNum(giftReceiveInfo.getGiftPurseAmount());
                                        giftInfo.setDisplayable(giftReceiveInfo.isDisplayable());
                                        needRefresh = true;
                                    }
                                }
                            }
                        }
                        callBack.onSuccess(needRefresh);
                    }
                }

                @Override
                public void onFailure(int code, String msg) {
                    if (callBack != null) {
                        callBack.onFail(code, msg);
                    }
                }
            });
        }
    }

    /**
     * 发送礼物通知
     *
     * @param isMultiGift     是否是全麦礼物
     * @param giftReceiveInfo 礼物信息
     */
    private void sendGiftMessage(boolean isMultiGift, GiftReceiveInfo giftReceiveInfo) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo == null || giftReceiveInfo == null) {
            return;
        }
        GiftAttachment giftAttachment = null;
        if (isMultiGift) {
            giftAttachment = new GiftAttachment(IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT, IMCustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT);
        } else {
            giftAttachment = new GiftAttachment(IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, IMCustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
        }

        // 礼物连击
        giftAttachment.setUid(getCurrentUserId() + "");
        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
        ChatRoomMessage message = new ChatRoomMessage();
        message.setRoom_id(roomInfo.getRoomId() + "");
        message.setAttachment(giftAttachment);
        message.setImChatRoomMember(AvRoomDataManager.get().getMOwnerMember());
        ReUsedSocketManager.get().sendCustomMessage(message.getRoom_id() + "", message, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {

            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    private void sendGiftMessage(boolean isMultiGift, GiftReceiveInfo giftReceiveInfo, long comboId, long comboCount, long comboFrequencyCount) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo == null || giftReceiveInfo == null) {
            return;
        }
        GiftAttachment giftAttachment = null;
        if (isMultiGift) {
            giftAttachment = new GiftAttachment(IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT, IMCustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT);
        } else {
            giftAttachment = new GiftAttachment(IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, IMCustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
        }

        // 礼物连击
        giftReceiveInfo.setComboId(comboId);
        giftReceiveInfo.setComboCount(comboCount);
        giftReceiveInfo.setComboFrequencyCount(comboFrequencyCount);
        giftAttachment.setUid(getCurrentUserId() + "");
        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
        ChatRoomMessage message = new ChatRoomMessage();
        message.setRoom_id(roomInfo.getRoomId() + "");
        message.setAttachment(giftAttachment);
        message.setImChatRoomMember(AvRoomDataManager.get().getMOwnerMember());
        ReUsedSocketManager.get().sendCustomMessage(message.getRoom_id() + "", message, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {

            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

//    private void sendGiftMessage(GiftReceiveInfo giftReceiveInfo) {
//        RoomInfo roomInfo = getCurrentRoomInfo();
//        if (roomInfo == null || giftReceiveInfo == null) return;
//        GiftAttachment giftAttachment = new GiftAttachment(IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, IMCustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
//        giftAttachment.setUid(getCurrentUserId() + "");
//        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
//        ChatRoomMessage message = new ChatRoomMessage();
//        message.setRoom_id(roomInfo.getRoomId() + "");
//        message.setAttachment(giftAttachment);
//        message.setImChatRoomMember(AvRoomDataManager.get().mOwnerMember);
//        CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
//    }

    /**
     * 提交举报
     *
     * @param type       '类型 1. 用户 2. 房间',
     * @param targetUid  对方的uid（如果是房间，传房主UID）
     * @param reportType '举报类型 1.政治 2.色情 3.广告 4.人身攻击 ',
     */
    public void commitReport(int type, long targetUid, int reportType, HttpRequestCallBack<Object> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("reportType", reportType + "");
        params.put("type", type + "");
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        params.put("phoneNo", userInfo != null ? userInfo.getPhone() : "");
        params.put("reportUid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("uid", String.valueOf(targetUid));
        OkHttpManager.getInstance().doPostRequest(UriProvider.reportUserUrl(), params, callBack);
    }

    /**
     * 获取默认选中的礼物赠送对象
     */
    public long getGiftDefaultSelectMemberUid() {// 默认选中房主
        return getCurrentRoomInfo().getUid();

        //视频房默认选中麦上异性
//        if (getCurrentRoomInfo() != null) {
//            IMChatRoomMember roomMember = AvRoomDataManager.get().getFirstOppositeSexOnMic();
//            if (getCurrentRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE && roomMember != null) {
//                return Long.valueOf(roomMember.getAccount());
//            } else {
//                //如果麦上没异性，且我不是房主，那么默认选中房主
//                return getCurrentRoomInfo().getUid();
//            }
//        } else {
//            return 0;
//        }
    }

    public void exitRoom(CallBack<String> callBack) {
        RoomInfo currentRoom = getCurrentRoomInfo();
        AvRoomDataManager.get().release();
        if (currentRoom == null) {
            if (callBack != null) {
                callBack.onFail(0, "");
            }
            return;
        }
        ReUsedSocketManager.get().exitRoom(currentRoom.getRoomId(), new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                if (imReportBean.getReportData().errno == 0) {
                    if (callBack != null) {
                        callBack.onSuccess("");
                    }
                    IMNetEaseManager.get().getChatRoomEventObservable()
                            .onNext(new RoomEvent().setEvent(RoomEvent.ROOM_EXIT));
                } else {
                    if (callBack != null) {
                        callBack.onFail(0, "");
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (callBack != null) {
                    callBack.onFail(0, "");
                }
            }
        });
    }

    /**
     * 获取房间结束相关信息
     *
     * @param roomUid
     * @param roomType
     * @param callBack
     */
    public void getRoomClosedInfo(long roomUid, int roomType, HttpRequestCallBack<RoomClosedBean> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("roomUid", String.valueOf(roomUid));
        params.put("roomType", String.valueOf(roomType));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getClosedInfo(), params, callBack);
    }

    /**
     * 当前用户领取某个房间的对应id的红包接口
     *
     * @param roomid
     * @param redPackId
     * @param callBack
     */
    public void receiveRoomRedPackage(long roomid, long redPackId, HttpRequestCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomid", roomid + "");
        params.put("redPackId", redPackId + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.receiveRedPackage(), params, callBack);
    }

    /**
     * 获取对应房间未领取完的红包发送记录
     *
     * @param roomId
     * @param myCallBack
     */
    public void getRoomSendRedPackageRecord(String roomId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomid", roomId);
        OkHttpManager.getInstance().getRequest(UriProvider.getSendRedPackageRecord(), params, myCallBack);
    }

    /**
     * 获取房间活动接口
     *
     * @param type     2:新手礼包
     * @param callBack
     */
    public void getActionDialogState(int type, OkHttpManager.MyCallBack<ServiceResult<List<ActionDialogInfo>>> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("type", type + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getRedBagDialogType(), params, callBack);
    }

    /**
     * 获取我的连麦状态接口
     */
    public void getLianMicroStatus(HttpRequestCallBack<LianMicroStatusInfo> callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null) {
            Map<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            params.put("roomId", String.valueOf(roomInfo.getRoomId()));
            OkHttpManager.getInstance().getRequest(UriProvider.getLianMicroStatus(), params, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, EXCEPTION_MSG_ERROR);
        }
    }

    /**
     * 取消连麦申请
     */
    public void cancelLianMicroApply(HttpRequestCallBack<Object> callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null) {
            Map<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
            params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
            params.put("roomId", String.valueOf(roomInfo.getRoomId()));
            OkHttpManager.getInstance().doPostRequest(UriProvider.cancelLianMicroApply(), params, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, EXCEPTION_MSG_ERROR);
        }
    }

    /**
     * 获取申请上麦列表
     *
     * @param roomType
     * @param callBack
     */
    public void getMicApplyList(int roomType, HttpRequestCallBack callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null) {
            Map<String, String> params = getDefaultParams();
            params.put("roomType", String.valueOf(roomType));
            getRequest(UriProvider.getMicApplyList(), params, callBack);
        } else if (callBack != null) {
            callBack.onFailure(DEFAULT_CODE_ERROR, EXCEPTION_MSG_ERROR);
        }
    }

    /**
     * 同意连麦
     *
     * @param tgUid
     * @param callBack
     */
    public void agreeMicroApply(long tgUid, HttpRequestCallBack callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null) {
            Map<String, String> params = getDefaultParams();
            params.put("roomId", String.valueOf(roomInfo.getRoomId()));
            params.put("tgUid", String.valueOf(tgUid));
            postRequest(UriProvider.agreeMicroApply(), params, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, EXCEPTION_MSG_ERROR);
        }
    }

    /**
     * 拒绝连麦
     *
     * @param tgUid    拒绝目标uid,多个批操作‘,’隔开例如 ‘100,101,102’
     * @param callBack
     */
    public void rejectMicroApply(String tgUid, HttpRequestCallBack callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null) {
            Map<String, String> params = getDefaultParams();
            params.put("roomId", String.valueOf(roomInfo.getRoomId()));
            params.put("tgUid", tgUid);
            postRequest(UriProvider.rejectMicroApply(), params, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, EXCEPTION_MSG_ERROR);
        }
    }

    /**
     * 设置连麦申请开关
     *
     * @param isCanConnectMic 状态 0、关闭 1、打开
     * @param roomType
     * @param callBack
     */
    public void setRoomConnectMic(int isCanConnectMic, int roomType, HttpRequestCallBack callBack) {
        RoomInfo roomInfo = getCurrentRoomInfo();
        if (roomInfo != null) {
            Map<String, String> params = getDefaultParams();
            params.put("isCanConnectMic", String.valueOf(isCanConnectMic));
            params.put("roomType", String.valueOf(roomType));
            postRequest(UriProvider.setRoomConnectMic(), params, callBack);
        } else if (callBack != null) {
            callBack.onFinish();
            callBack.onFailure(DEFAULT_CODE_ERROR, EXCEPTION_MSG_ERROR);
        }
    }

    /**
     * 直播房结束
     *
     * @param roomId
     * @param uid
     * @param ticket
     * @param callBack
     */
    public void getLiveRoomFinishInfo(long roomId, long uid, String ticket, HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("roomId", roomId + "");
        params.put("ticket", ticket);
        params.put("uid", uid + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.getLiveRoomFinish(), params, callBack);
    }

    /**
     * 幸运宝箱倒计时
     *
     * @return
     */
    public void getLuckyBoxDetail(long roomId, HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("roomId", roomId + "");
        getRequest(UriProvider.getLuckyBoxDetail(), params, callBack);
    }

    /**
     * 发送弹幕
     *
     * @param roomId
     * @param uid
     * @param ticket
     * @param msg
     * @param callBack
     */
    public void sendBarrage(long roomId, long uid, String ticket, String msg, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("room_id", roomId + "");
        params.put("ticket", ticket);
        params.put("uid", uid + "");
        params.put("barrage_msg", msg);
        OkHttpManager.getInstance().doPostRequest(UriProvider.getBarrageSend(), params, callBack);

    }

    /**
     * 获取免费弹幕
     *
     * @param callBack
     */
    public void getFreeBarrages(HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        getRequest(UriProvider.getFreeBarrages(), params, callBack);
    }

    /**
     * 魅力值PK确定
     *
     * @param expireSeconds 比赛时间 单位为秒
     * @param opUid
     * @param roomId
     * @param uids          用户列表，逗号
     * @param callBack
     */
    public void confirmCharmPK(int expireSeconds, long opUid, long roomId, String uids, HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("expireSeconds", expireSeconds + "");
        params.put("opUid", opUid + "");
        params.put("roomId", roomId + "");
        params.put("uids", uids);
        OkHttpManager.getInstance().doPostRequest(UriProvider.charmPKConfirm(), params, callBack);
    }

    /**
     * 魅力值PK邀请确定
     *
     * @param pkId
     * @param roomId
     * @param uid
     * @param callBack
     */
    public void agreeCharmPK(int pkId, long roomId, long uid, HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("pkId", pkId + "");
        params.put("roomId", roomId + "");
        params.put("uid", uid + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.agreeCharmPK(), params, callBack);
    }

    /**
     * 获取房间魅力值pk倒计时
     *
     * @param roomId
     * @param callBack
     */
    public void getCharmCountDown(long roomId, HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("roomId", roomId + "");
        getRequest(UriProvider.getCharmCountDown(), params, callBack);
    }

    /**
     * 获取房间常用语接口
     *
     * @param myCallBack
     */
    public void getRecommendWorlds(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        if (myCallBack != null) {
            OkHttpManager.getInstance().getRequest(UriProvider.getUserRecommendWorlds(), params, myCallBack);
        }
    }

    /**
     * 开启、关闭猪猪大作战
     *
     * @param type     1、开启 0、 关闭
     * @param callBack 回调
     */
    public void openOrClosePigFight(int type, HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("type", type + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("roomId", AvRoomDataManager.get().getRoomInfo() != null ? (AvRoomDataManager.get().getRoomInfo().getRoomId() + "") : "");
        postRequest(UriProvider.openOrClosePigFight(), param, callBack);
    }

    // 获取特惠礼物设置
    public void getDiscountGiftSetting(HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        getRequest(UriProvider.getGiftPreferentialSetting(), param, callBack);
    }

    /**
     * 获取用户房间信息
     */
    public void getUserRoomInfo(long userId, HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(userId));
        getRequest(UriProvider.getUserRoomInfo(), param, callBack);
    }
}
