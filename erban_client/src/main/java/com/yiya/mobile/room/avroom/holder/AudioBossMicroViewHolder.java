package com.yiya.mobile.room.avroom.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

public class AudioBossMicroViewHolder extends AudioMicroViewHolder {

    /**
     * 主席位特有
     */
    private ImageView tvState;

    private String avatarPicture = "";
    private String userName = "";

    public AudioBossMicroViewHolder(Context context, View itemView) {
        super(context, itemView);
        tvState = itemView.findViewById(R.id.tv_state);
    }

    @Override
    public void bind(RoomQueueInfo info, int position) {
        super.bind(info, position);
        if (info == null || AvRoomDataManager.get().getRoomInfo() == null) {
            return;
        }
        try {
            RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(AvRoomDataManager.MIC_POSITION_BY_OWNER);
            if (roomQueueInfo == null || roomQueueInfo.mChatRoomMember != null) {
                tvState.setVisibility(View.GONE);
            } else {
                tvState.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            tvState.setVisibility(View.VISIBLE);
        }

        ivGender.setVisibility(View.GONE);
        tvNick.setVisibility(View.VISIBLE);
        ivLockImage.setVisibility(View.GONE);
        ivMuteImage.setVisibility(View.GONE);
        ivUpImage.setVisibility(View.GONE);
        ivAvatar.setVisibility(View.VISIBLE);
        avatarBg.setVisibility(View.VISIBLE);
        ivAvatar.setOnClickListener(v -> onMicroItemClickListener.onAvatarBtnClick(position, null));
        if (StringUtil.isEmpty(avatarPicture)) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(AvRoomDataManager.get().getRoomInfo().getUid());
            if (userInfo != null) {
                if (userInfo.getAvatar() != null) {
                    if (!userInfo.getAvatar().equals(avatarPicture)) {
                        avatarPicture = userInfo.getAvatar();
                        userName = userInfo.getNick();
                        tvNick.setText(userInfo.getNick());
                        ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), avatarPicture, ivAvatar);
                    }
                }
            } else {
                CoreManager.getCore(IUserCore.class).requestUserInfo(AvRoomDataManager.get().getRoomInfo().getUid(), new HttpRequestCallBack<UserInfo>() {
                    @Override
                    public void onSuccess(String message, UserInfo response) {
                        if (response != null && ivAvatar != null) {
                            avatarPicture = response.getAvatar();
                            tvNick.setText(response.getNick());
                            userName = response.getNick();
                            ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), avatarPicture, ivAvatar);
                        }
                    }

                    @Override
                    public void onFailure(int code, String msg) {

                    }
                });
            }
        } else {
            ImageLoadUtils.loadAvatar(BasicConfig.INSTANCE.getAppContext(), avatarPicture, ivAvatar);
            tvNick.setText(userName);
        }
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
    }
}