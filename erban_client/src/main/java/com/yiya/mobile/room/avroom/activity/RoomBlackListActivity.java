package com.yiya.mobile.room.avroom.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.im.room.IIMRoomCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.presenter.RoomBlackPresenter;
import com.tongdaxing.xchat_core.room.view.IRoomBlackView;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.room.avroom.adapter.RoomBlackAdapter;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.widget.emptyView.DefaultEmptyEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

/**
 * 房间黑名单
 *
 * @author chenran
 * @date 2017/10/11
 */
@CreatePresenter(RoomBlackPresenter.class)
public class RoomBlackListActivity extends BaseMvpActivity<IRoomBlackView, RoomBlackPresenter>
        implements IRoomBlackView, RoomBlackAdapter.RoomBlackDelete, OnLoadMoreListener {
    private TextView count;
    private RecyclerView recyclerView;
    private RoomBlackAdapter normalListAdapter;
    private int start = 0;
    private int loadSize = 30;
    private SmartRefreshLayout srlBlack;
//    private LinearLayout mEmptyLinearLayout;


    public static void start(Context context) {
        Intent intent = new Intent(context, RoomBlackListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_black_list);
        initTitleBar("黑名单");
        initView();
        showLoading();
        loadData();
    }

    private void loadData() {
        //一次最多只能加载200条
        getMvpPresenter().queryNormalListFromIm(loadSize, start);
    }

    private void initView() {
        count = (TextView) findViewById(R.id.count);
        srlBlack = (SmartRefreshLayout) findViewById(R.id.srl_black);
        srlBlack.setEnableRefresh(false);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        normalListAdapter = new RoomBlackAdapter(R.layout.list_item_room_black);
        normalListAdapter.setRoomBlackDelete(this);
        normalListAdapter.setEnableLoadMore(false);
        normalListAdapter.disableLoadMoreIfNotFullPage(recyclerView);
        srlBlack.setOnLoadMoreListener(this);
        // normalListAdapter.setListOperationClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        normalListAdapter.setEmptyView(getEmptyView(recyclerView, DefaultEmptyEnum.EMPTY_BLACK_LIST));
        recyclerView.setAdapter(normalListAdapter);
//        mEmptyLinearLayout = (LinearLayout) findViewById(R.id.content_empty_ll);
    }

    @Override
    public void onDeleteClick(final IMChatRoomMember chatRoomMember) {
        getDialogManager().showOkCancelDialog(
                "是否将" + chatRoomMember.getNick() + "移除黑名单列表？",
                true,
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
                        if (roomInfo != null) {
                            if (AvRoomDataManager.get().isRoomOwner()) {// 房主
                                getMvpPresenter().operateRoomBlackList(String.valueOf(roomInfo.getRoomId()), chatRoomMember.getAccount(), false, true);
                            } else if (AvRoomDataManager.get().isRoomAdmin()) { // 管理员
                                getMvpPresenter().operateRoomBlackList(String.valueOf(roomInfo.getRoomId()), chatRoomMember.getAccount(), false, false);
                            }

                        }
                    }
                });
    }

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                loadData();
            }
        };
    }

    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void onMemberBeRemoveManager(String account) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (uid == JavaUtil.str2long(account)) {
            finish();
            toast(R.string.remove_room_manager);
        }
    }

    @Override
    public void queryNormalListSuccess(List<IMChatRoomMember> blackList) {
        hideStatus();
//        if (chatRoomMemberList != null) {
//            normalListAdapter.loadMoreComplete();
//            if (chatRoomMemberList.size() < loadSize) {
//                normalListAdapter.loadMoreEnd();
//            }
//
//        }
//        List<IMChatRoomMember> blackList = getBlackList(chatRoomMemberList);
        if (blackList != null && blackList.size() > 0) {
            if (normalListAdapter.getData() != null && normalListAdapter.getData().size() > 0) {
                srlBlack.finishLoadmore();
                normalListAdapter.addData(blackList);
                count.setText("黑名单" + normalListAdapter.getData().size() + "人");
            } else {
                normalListAdapter.setNewData(blackList);
                count.setText("黑名单" + blackList.size() + "人");
            }

        } else {
            if (normalListAdapter.getData() == null || normalListAdapter.getData().size() < 1) {
//                showNoData("暂没有设置黑名单");
                count.setText("黑名单0人");
//                showEmpty(true);
            } else {
                srlBlack.finishLoadmoreWithNoMoreData();

            }
        }
    }


    private List<IMChatRoomMember> getBlackList(List<IMChatRoomMember> chatRoomMemberList) {
        List<IMChatRoomMember> blackList = null;
        if (!ListUtils.isListEmpty(chatRoomMemberList)) {
            blackList = new ArrayList<>();
            for (IMChatRoomMember chatRoomMember : chatRoomMemberList) {
                if (chatRoomMember.isIs_black_list()) {
                    blackList.add(chatRoomMember);
                }
            }
            //根据最后的时间戳分页加载
            // TODO: 2018/11/14 im 黑名单移除更新时间
//            lastUserUpdateTime = chatRoomMemberList.get(chatRoomMemberList.size() - 1).getUpdateTime();
        }
        return blackList;
    }

    @Override
    public void queryNormalListFail() {
        showNetworkErr();
    }

    @Override
    public void makeBlackListSuccess(String account, boolean mark) {
        if (account == null) return;
        List<IMChatRoomMember> normalList = normalListAdapter.getData();
        if (!ListUtils.isListEmpty(normalList)) {
            hideStatus();
            ListIterator<IMChatRoomMember> iterator = normalList.listIterator();
            for (; iterator.hasNext(); ) {
                if (Objects.equals(iterator.next().getAccount(), account)) {
                    iterator.remove();
                }
            }
            normalListAdapter.notifyDataSetChanged();
            count.setText("黑名单" + normalList.size() + "人");
            if (normalList.size() == 0) {
//                showNoData("暂没有设置黑名单");
//                showEmpty(true);
            }
        } else {
//            showNoData("暂没有设置黑名单");
//            showEmpty(true);
            count.setText("黑名单0人");
        }
        toast("操作成功");

    }

//    private void showEmpty(boolean value) {
//        mEmptyLinearLayout.setVisibility(value ? View.VISIBLE : View.GONE);
//    }


    @Override
    public void makeBlackListFail(String error) {
        toast(error);
    }


    @Override
    public void onLoadMore(RefreshLayout refreshLayout) {
        start = normalListAdapter.getData().size() + 1;
        loadData();
    }
}
