package com.yiya.mobile.room.egg.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantRewardListAdapter extends BaseQuickAdapter<EggGiftInfo, BaseViewHolder> {

    public PlantRewardListAdapter() {
        super(R.layout.item_plant_reward_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, EggGiftInfo item) {
        if (item == null) {
            return;
        }
        ImageLoadUtils.loadImage(mContext, item.getPicUrl(), helper.getView(R.id.iv_reward));
        helper.setText(R.id.tv_reward_name, item.getGiftName())
                .setText(R.id.tv_reward_price, item.getGoldPrice() + "金币");
    }
}
