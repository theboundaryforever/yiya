package com.yiya.mobile.room.avroom.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/1/25
 */
public class BaseRecycleViewHolder extends RecyclerView.ViewHolder {

    public BaseRecycleViewHolder(View itemView) {
        super(itemView);
    }
}
