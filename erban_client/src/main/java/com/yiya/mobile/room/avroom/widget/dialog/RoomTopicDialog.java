package com.yiya.mobile.room.avroom.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

/**
 * Created by Administrator on 2018/5/4.
 */

public class RoomTopicDialog extends BaseDialogFragment {
    private TextView title;
    private TextView content;
    private ImageView iv_ok;

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, null);
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Window window = getDialog().getWindow();
        View view = inflater.inflate(R.layout.dialog_room_topic, window.findViewById(android.R.id.content), false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setCancelable(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title = view.findViewById(R.id.tv_topic_title);
        content = view.findViewById(R.id.tv_topic_content);
        iv_ok = view.findViewById(R.id.iv_ok);
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();

        if (roomInfo != null) {
            if (!TextUtils.isEmpty(roomInfo.getRoomNotice())) {
                content.setText(roomInfo.getRoomNotice() + "");
            } else {
                content.setText("暂无房间内容");
            }
            if (!TextUtils.isEmpty(roomInfo.getRoomDesc())) {
                title.setText(roomInfo.getRoomDesc() + "");
            } else {
                title.setText("暂无房间话题");
            }


        }

        iv_ok.setOnClickListener(v -> dismiss());
//        view.findViewById(R.id.iv_topic_close_dialog).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });
    }
}
