package com.yiya.mobile.room.egg.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;

public class PlantRuleListAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public PlantRuleListAdapter() {
        super(R.layout.item_plant_bean_rule);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tv_content, item)
                .setText(R.id.tv_index, String.valueOf(helper.getAdapterPosition() + 1));
    }
}
