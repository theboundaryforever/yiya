package com.yiya.mobile.room.avroom.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.faceunity.FURenderer;
import com.yiya.mobile.room.avroom.other.OnMicroItemClickListener;
import com.yiya.mobile.room.avroom.widget.dialog.LiveRoomFaceuDialog;
import com.yiya.mobile.ui.widget.floatView.FloatViewManager;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.fu.FuTextureCamera;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import io.agora.rtc.RtcEngine;
import io.agora.rtc.mediaio.AgoraSurfaceView;
import io.agora.rtc.mediaio.MediaIO;
import io.agora.rtc.video.VideoCanvas;

import static com.tongdaxing.xchat_core.manager.AvRoomDataManager.MIC_POSITION_BY_VIDEO_GUEST;

/**
 * 上麦布局界面
 *
 * @author zeda
 */
public class VideoMicroView extends BaseMicroView {
    private static final String TAG = "VideoMicroView";

    private Context context;
    private AgoraSurfaceView roomOwnerAgoraView;
    private SurfaceView roomOwnerView;
    private OnMicroItemClickListener onMicroItemClickListener;
    private VideoGuestMicroView guestMicroView;

    private FuTextureCamera mFuTextureCamera;
    private FURenderer mFURenderer;
    private Handler mainHandler = new Handler(Looper.getMainLooper());

    public VideoMicroView(Context context) {
        this(context, null);
        init(context);
    }

    public VideoMicroView(Context context, AttributeSet attr) {
        this(context, attr, 0);
        init(context);
    }

    public VideoMicroView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
    }

    private boolean isFirstFrame = true;

    private void createRoomOwnerView(Context context, boolean roomOwner) {
        if (roomOwner) {
            mFURenderer = new FURenderer.Builder(getContext()).maxFaces(1).inputTextureType(FURenderer.FU_ADM_FLAG_EXTERNAL_OES_TEXTURE).build();

            mFuTextureCamera = new FuTextureCamera(getContext(), 1280, 720, AvRoomDataManager.get().getCameraFacing());
            mFuTextureCamera.setOnCaptureListener(new FuTextureCamera.OnCaptureListener() {
                @Override
                public int onTextureBufferAvailable(int textureId, byte[] buffer, int width, int height) {
                    if (isFirstFrame) {
                        if (roomOwnerAgoraView != null) {
                            //这里做延时，尽量保证捕获的画面出来了再显示，避免因为透明显示了底部页面
                            mainHandler.postDelayed(() -> roomOwnerAgoraView.setBackgroundColor(getResources().getColor(R.color.transparent)), 150);
                        }
                        isFirstFrame = false;
                    }
                    return mFURenderer.onDrawFrame(buffer, textureId, width, height);
                }

                @Override
                public void onCapturerStarted() {
                    LogUtil.i(TAG, "onCapturerStarted() called");
                    mFURenderer.onSurfaceCreated();
                }

                @Override
                public void onCapturerStopped() {
                    LogUtil.i(TAG, "onCapturerStopped() called");
                    mFURenderer.onSurfaceDestroyed();
                }

                @Override
                public void onCameraSwitched(int facing, int orientation) {
                    LogUtil.i(TAG, "onCameraSwitched() called with: facing = [" + facing + "], orientation = [" + orientation + "]");
                    mFURenderer.onCameraChange(facing, orientation);
                }
            });
//
            if (mFuTextureCamera == null) {
                LogUtil.i("mFuTextureCamera is null");
                SingleToastUtil.showToast("抱歉，初始化摄像头失败");
                return;
            }
            LiveRoomFaceuDialog.initFuData(DemoCache.readFUConfigure(), mFURenderer);

            roomOwnerAgoraView = new AgoraSurfaceView(context);
            roomOwnerAgoraView.init(mFuTextureCamera.getEglContext());
            roomOwnerAgoraView.setBufferType(MediaIO.BufferType.TEXTURE);
            roomOwnerAgoraView.setPixelFormat(MediaIO.PixelFormat.TEXTURE_2D);
            roomOwnerAgoraView.setBackgroundColor(getResources().getColor(R.color.video_micro_view_bg));

            addView(roomOwnerAgoraView, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            if (RtcEngineManager.get().setLocalView(roomOwnerAgoraView)) {
                LogUtil.i("setLocalView is failed");
                SingleToastUtil.showToast("抱歉，初始化摄像头失败");
                return;
            }

            RtcEngineManager.get().setVideoSource(mFuTextureCamera);
            RtcEngineManager.get().startPreview(true);
        } else {
//            int height = getResources().getDimensionPixelOffset(R.dimen.video_micro_owner_view_height);
            int height = (int) (DisplayUtils.getScreenWidth(context) * 3.0/ 4);// 4:3 时的屏幕高度
            int marginTop = getResources().getDimensionPixelOffset(R.dimen.video_micro_surface_margin_top);
            if (AvRoomDataManager.get().getRoomInfo().getRoomUserOnLiveClient() == 1) { //手机端
                height = ViewGroup.LayoutParams.MATCH_PARENT;
                marginTop = 0;
            }

            roomOwnerView = RtcEngine.CreateRendererView(context.getApplicationContext());
            roomOwnerView.setId(R.id.video_surface_id_room_owner);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
            layoutParams.topMargin = marginTop;
            addView(roomOwnerView, layoutParams);
        }
    }

    @Override
    public void setOnMicroItemClickListener(OnMicroItemClickListener onMicroItemClickListener) {
        this.onMicroItemClickListener = onMicroItemClickListener;
    }

    @Override
    public void refreshMicroView(int micPosition) {
        if (micPosition == MIC_POSITION_ALL) {
            //刷新所有麦位
            refreshRoomOwnerView();
            refreshGuestView();
        } else {
            //目前只有房主和一个嘉宾（连麦者）
            if (micPosition == AvRoomDataManager.MIC_POSITION_BY_OWNER) {
                refreshRoomOwnerView();
            } else if (micPosition == MIC_POSITION_BY_VIDEO_GUEST) {
                refreshGuestView();
            }
        }
    }

    /**
     * 刷新房主麦位
     */
    private void refreshRoomOwnerView() {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(AvRoomDataManager.MIC_POSITION_BY_OWNER);
        if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
            String queueAccount = roomQueueInfo.mChatRoomMember.getAccount();
            if (AvRoomDataManager.get().isOwner(queueAccount)) {
                if (roomOwnerAgoraView == null) {
                    createRoomOwnerView(context, true);
                }
            } else {
                if (roomOwnerView == null) {
                    createRoomOwnerView(context, false);
                }
                RtcEngineManager.get().setupRemoteVideo(new VideoCanvas(roomOwnerView, VideoCanvas.RENDER_MODE_HIDDEN, Integer.valueOf(queueAccount)));
                roomOwnerView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.transparent));
            }
        }
    }

    /**
     * 刷新嘉宾麦位（目前只有一个嘉宾，连麦者）
     */
    private void refreshGuestView() {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoByMicPosition(MIC_POSITION_BY_VIDEO_GUEST);
        if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
            if (guestMicroView == null) {
                guestMicroView = FloatViewManager.get().add(getContext()).getView();
                if (guestMicroView == null) {
                    return;
                }
                guestMicroView.setOnDownMicClickListener(v -> {
                    if (onMicroItemClickListener != null) {
                        onMicroItemClickListener.onDownMicBtnClick(MIC_POSITION_BY_VIDEO_GUEST);
                    }
                });
            }
            guestMicroView.setupView(roomQueueInfo);
        } else {
            if (guestMicroView != null) {
                FloatViewManager.get().remove();
                releaseGuestView();
            }
        }
    }

    public void setRoomOwnerAgoraViewBackground(@ColorInt int color) {
        roomOwnerAgoraView.setBackgroundColor(color);
    }

    /**
     * 开始音视频第一帧
     */
    public void startFirstFrame() {
        if (guestMicroView != null) {
            guestMicroView.startFirstFrame();
        }
    }

    private void releaseOwnerView() {
        if (roomOwnerView != null) {
            ViewGroup parent = (ViewGroup) roomOwnerView.getParent();
            if (parent != null) {
                parent.removeView(roomOwnerView);
            }
            roomOwnerView = null;
        }
        if (roomOwnerAgoraView != null) {
            ViewGroup parent = (ViewGroup) roomOwnerAgoraView.getParent();
            if (parent != null) {
                parent.removeView(roomOwnerAgoraView);
            }
            roomOwnerAgoraView = null;
        }
    }

    private void releaseGuestView() {
        if (guestMicroView != null) {
            ViewGroup guestParent = (ViewGroup) guestMicroView.getParent();
            if (guestParent != null) {
                guestParent.removeView(guestMicroView);
            }
            FloatViewManager.get().remove();
            guestMicroView.release();
            guestMicroView = null;
        }
    }

    public FURenderer getFURenderer() {
        return mFURenderer;
    }

    public void onResume() {
        if (mFuTextureCamera != null) {
            mFuTextureCamera.onResume();
        }
    }

    public void onPause() {
        if (mFuTextureCamera != null) {
            mFuTextureCamera.onPause();
        }
    }

    public void switchCameraFacing() {
        if (mFuTextureCamera != null) {
            mFuTextureCamera.switchCameraFacing();
        } else {
            LogUtil.e("mFuTextureCamera is null");
        }
    }

    @Override
    public void release() {
        mainHandler.removeCallbacksAndMessages(null);
        releaseOwnerView();
        releaseGuestView();
        if (mFuTextureCamera != null) {
            RtcEngineManager.get().startPreview(false);
            mFuTextureCamera.release();
            mFuTextureCamera = null;
        }
    }
}
