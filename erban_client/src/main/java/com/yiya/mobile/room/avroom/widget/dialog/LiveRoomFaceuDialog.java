package com.yiya.mobile.room.avroom.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.faceunity.FURenderer;
import com.faceunity.OnFaceUnityControlListener;
import com.faceunity.entity.Filter;
import com.yiya.mobile.room.avroom.widget.FaceuCheckBox;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.room.bean.FUConfigure;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangsongzhou
 * @date 2019/6/5
 */
public class LiveRoomFaceuDialog extends Dialog {

    private static final int[] FILTER_SOURCE_ID = {R.mipmap.icon_filter_artwork, R.mipmap.icon_filter_nature, R.mipmap.icon_filter_elegant
            , R.mipmap.icon_filter_dawn, R.mipmap.icon_filter_shimmer, R.mipmap.icon_filter_vitality
            , R.mipmap.icon_fiter_oxygen, R.mipmap.icon_filter_pink, R.mipmap.icon_filter_dew
            , R.mipmap.icon_filter_warm_braw, R.mipmap.icon_filter_mid_summer};
    private static final String[] FILTER_SOURCE_NAME = {"原图", "自然", "淡雅"
            , "粉嫩", "清新", "微光"
            , "盛夏", "晨曦", "暖风"
            , "元气", "氧气"};
    /**
     * 0.0f- 1.0f
     */
    private static final String[] FILTER_SOURCE_KEY = {"origin", "ziran", "danya"
            , "fennen", "qingxin", "bailiang1"
            , "nuansediao1", "xiaoqingxin1", "slowlived"
            , "bailiang5", "lengsediao2"};

    @BindViews({R.id.beauty_tv, R.id.filter_tv})
    List<TextView> mIndicatorTitleList;
    @BindViews({R.id.beauty_iv, R.id.filter_iv})
    List<ImageView> mIndicatorList;


    @BindView(R.id.beauty_bg_ll)
    LinearLayout mBeautyLl;
    @BindViews({R.id.beauty_skin_fcb, R.id.beauty_buffing_fcb, R.id.beauty_face_lift_fcb, R.id.beauty_big_eyes_fcb})
    List<FaceuCheckBox> mFaceuCheckBoxList;

    @BindView(R.id.filter_recycle_view)
    RecyclerView mFilterRecyclerView;

    private FilterRecyclerAdapter mFilterRecyclerAdapter;

    @BindView(R.id.beauty_bar_value_tv)
    TextView mBeautyBarValueTv;
    @BindView(R.id.beauty_seek_bar)
    SeekBar mBeautySeekBar;

    private Context mContext;

    private OnFaceUnityControlListener mFaceUnityControlListener;

    private int mTmpBeautyViewId = -1;

    private float mBeautyColorLevel = 0.0f; //美白 0.0f - 1.0f
    private float mBeautyBlurLevel = 0.0f;  //磨皮 0.0f - 1.0f (SDK 为0-6，已经做了处理)
    private float mBeautyCheekThin = 0.0f;  //瘦脸 0.0f - 1.0f
    private float mBeautyEnlargeEye = 0.0f; //大眼 0.0f - 1.0f
    private float mFaceBeautyFaceShape = 4.0f;//脸型 (demo中默认设置有)

    private FUConfigure mFuConfigure;

    /**
     * 初始化美颜
     *
     * @param configure
     * @param renderer
     */
    public static void initFuData(FUConfigure configure, FURenderer renderer) {
        if (configure == null) {
            configure = new FUConfigure();
            configure.setMBeautyColorLevel(0.5f);
            configure.setMBeautyBlurLevel(0.5f);
            configure.setMBeautyCheekThin(0.5f);
            configure.setMBeautyEnlargeEye(0.5f);
            DemoCache.saveFUConfigure(configure);

        }

        if (renderer != null && configure != null) {
            renderer.onColorLevelSelected(configure.getMBeautyColorLevel());
            renderer.onBlurLevelSelected(configure.getMBeautyBlurLevel());
            renderer.onCheekThinSelected(configure.getMBeautyCheekThin());
            renderer.onEnlargeEyeSelected(configure.getMBeautyEnlargeEye());
            renderer.onFaceShapeSelected(4.0f);

            //（设置了美颜 & 没有设置过滤镜）
            if (!TextUtils.isEmpty(configure.getFilterName())) {
                Filter filter = new Filter(configure.getFilterName(), -1, "", 0);
                renderer.onFilterLevelSelected(configure.getFilterLevel());
                renderer.onFilterSelected(filter);
            }
        }
    }


    public LiveRoomFaceuDialog(Context context) {
        super(context, R.style.GiftBottomSheetDialog);
        mContext = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_room_faceu);
        ButterKnife.bind(this);
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.BOTTOM);
        }
        setCanceledOnTouchOutside(true);

        mFuConfigure = DemoCache.readFUConfigure();


        initView();
        initData();

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (mFuConfigure != null) {
                    DemoCache.saveFUConfigure(mFuConfigure);
                }
            }
        });

        if (mFuConfigure == null) {
            mFuConfigure = new FUConfigure();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    private void initView() {
        mBeautySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBeautyBarValueTv.setText(progress + "%");
                if (!fromUser) {
                    return;
                }
                if (mTmpBeautyViewId == -1) {
                    return;
                }

                float valueF = 1.0f * progress / 100;
                changeFaceBeautyLevel(mTmpBeautyViewId, valueF);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        List<Filter> data = new ArrayList<>();
        for (int i = 0; i < FILTER_SOURCE_NAME.length; i++) {
            Filter info = new Filter(FILTER_SOURCE_KEY[i], FILTER_SOURCE_ID[i], FILTER_SOURCE_NAME[i], Filter.FILTER_TYPE_FILTER);
            info.setSelect(false);
            info.setLevel(0.5f);
            data.add(info);
        }
        mFilterRecyclerAdapter = new FilterRecyclerAdapter(R.layout.item_beauty_filter);
        mFilterRecyclerAdapter.setNewData(data);
        mFilterRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mFilterRecyclerView.setAdapter(mFilterRecyclerAdapter);
        mFilterRecyclerView.setItemAnimator(null);
        mFilterRecyclerAdapter.setOnItemClickListener(new FilterRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Filter info, int position) {
                if (mFaceUnityControlListener != null) {
                    mFaceUnityControlListener.onFilterLevelSelected(info.getLevel());
                    mFaceUnityControlListener.onFilterSelected(info);

                    if (mFuConfigure != null) {
                        mFuConfigure.setFilterName(info.filterName());
                        mFuConfigure.setFilterLevel(info.getLevel());
                    }
                }
            }
        });


        changeIndicator(0);
    }

    @OnClick({R.id.beauty_ll, R.id.filter_ll})
    void onClickIndicator(View view) {
        int pos = -1;
        switch (view.getId()) {
            case R.id.beauty_ll:
                pos = 0;
                break;
            case R.id.filter_ll:
                pos = 1;
                break;
        }
        if (pos >= 0) {
            changeIndicator(pos);
        }
    }

    private void changeIndicator(int position) {
        mBeautyLl.setVisibility(View.GONE);
        mFilterRecyclerView.setVisibility(View.GONE);

        for (int i = 0; i < mIndicatorTitleList.size(); i++) {
            if (i == position) {
                mIndicatorTitleList.get(i).setTextColor(mContext.getResources().getColor(R.color.white));
                mIndicatorList.get(i).setVisibility(View.VISIBLE);
            } else {
                mIndicatorList.get(i).setVisibility(View.INVISIBLE);
                mIndicatorTitleList.get(i).setTextColor(Color.parseColor("#A19F9F"));
            }
        }
        switch (position) {
            case 0:
                mBeautyLl.setVisibility(View.VISIBLE);
                break;
            case 1:
                mFilterRecyclerView.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void initData() {


        int position = 0;
        if (mFuConfigure != null) {
            mBeautyColorLevel = mFuConfigure.getMBeautyColorLevel();
            mBeautyBlurLevel = mFuConfigure.getMBeautyBlurLevel();
            mBeautyCheekThin = mFuConfigure.getMBeautyCheekThin();
            mBeautyEnlargeEye = mFuConfigure.getMBeautyEnlargeEye();

            int pos = getPosByFilterName(mFuConfigure.getFilterName());
            if (pos >= 0) {
                position = pos;
            }
        }


//        //设置参数
//        changeFaceBeautyLevel(R.id.beauty_skin_fcb, mBeautyColorLevel);
//        changeFaceBeautyLevel(R.id.beauty_buffing_fcb, mBeautyBlurLevel);
//        changeFaceBeautyLevel(R.id.beauty_face_lift_fcb, mBeautyCheekThin);
//        changeFaceBeautyLevel(R.id.beauty_big_eyes_fcb, mBeautyEnlargeEye);
//        changeFaceBeautyLevel(4, mFaceBeautyFaceShape); //默认

        //默认选中第一个
        onClickCheckBox(mFaceuCheckBoxList.get(0)); //美颜


        mFilterRecyclerAdapter.setSelect(position, false);    //滤镜
    }

    private int getPosByFilterName(String filterName) {
        if (!TextUtils.isEmpty(filterName)) {
            for (int i = 0; i < FILTER_SOURCE_KEY.length; i++) {
                if (filterName.equals(FILTER_SOURCE_KEY[i])) {
                    return i;
                }
            }
        }
        return -1;

    }


    @Override
    public void show() {
        super.show();
    }

    /**
     * @param view 美白 磨皮 瘦脸 大眼
     */
    @OnClick({R.id.beauty_skin_fcb, R.id.beauty_buffing_fcb, R.id.beauty_face_lift_fcb, R.id.beauty_big_eyes_fcb})
    void onClickCheckBox(FaceuCheckBox view) {
        mTmpBeautyViewId = view.getId();
        for (FaceuCheckBox faceuCheckBox : mFaceuCheckBoxList) {
            if (mTmpBeautyViewId == faceuCheckBox.getId()) {
                faceuCheckBox.setCheck(true);
            } else {
                faceuCheckBox.setCheck(false);
            }
        }
        float tmpF = 0.0f;


        if (mTmpBeautyViewId == R.id.beauty_skin_fcb) {
            tmpF = mBeautyColorLevel;
        } else if (mTmpBeautyViewId == R.id.beauty_buffing_fcb) {
            tmpF = mBeautyBlurLevel;
        } else if (mTmpBeautyViewId == R.id.beauty_face_lift_fcb) {
            tmpF = mBeautyCheekThin;
        } else if (mTmpBeautyViewId == R.id.beauty_big_eyes_fcb) {
            tmpF = mBeautyEnlargeEye;
        }

        int progress = (int) (tmpF * 100);
        mBeautyBarValueTv.setText(progress + "%");
        mBeautySeekBar.setProgress(progress);

    }

    public void setFaceUnityControlListener(@Nullable OnFaceUnityControlListener listener) {
        mFaceUnityControlListener = listener;
    }

    /**
     * 美颜部分
     * * @param viewId
     *
     * @param level
     */
    private void changeFaceBeautyLevel(int viewId, float level) {
        float value = level;
        if (value >= 1000) {
            value = 0;
        }
        switch (viewId) {
            case R.id.beauty_skin_fcb: //美白
                mBeautyColorLevel = value;

                if (mFaceUnityControlListener != null) {
                    mFaceUnityControlListener.onColorLevelSelected(value);
                }

                if (mFuConfigure != null) {
                    mFuConfigure.setMBeautyColorLevel(mBeautyColorLevel);
                }

                break;
            case R.id.beauty_buffing_fcb:   //磨皮
                mBeautyBlurLevel = value;
                if (mFaceUnityControlListener != null) {
                    mFaceUnityControlListener.onBlurLevelSelected(value);
                }
                if (mFuConfigure != null) {
                    mFuConfigure.setMBeautyBlurLevel(mBeautyBlurLevel);
                }
                break;
            case R.id.beauty_face_lift_fcb: //瘦脸
                mBeautyCheekThin = value;
                if (mFaceUnityControlListener != null) {
                    mFaceUnityControlListener.onCheekThinSelected(value);
                }
                if (mFuConfigure != null) {
                    mFuConfigure.setMBeautyCheekThin(mBeautyCheekThin);
                }
                break;
            case R.id.beauty_big_eyes_fcb: //大眼
                mBeautyEnlargeEye = value;
                if (mFaceUnityControlListener != null) {
                    mFaceUnityControlListener.onEnlargeEyeSelected(value);
                }
                if (mFuConfigure != null) {
                    mFuConfigure.setMBeautyEnlargeEye(mBeautyEnlargeEye);
                }
                break;
            case 4:
                mFaceBeautyFaceShape = value;
                if (mFaceUnityControlListener != null) {
                    mFaceUnityControlListener.onFaceShapeSelected(mFaceBeautyFaceShape);
                }
                break;
        }
    }


    public static class FilterRecyclerAdapter extends BaseQuickAdapter<Filter, BaseViewHolder> {

        private int mCurrentPos = 0;
        private OnItemClickListener mListener;

        private interface OnItemClickListener {
            void onItemClick(View view, Filter info, int position);
        }

        public void setOnItemClickListener(OnItemClickListener listener) {
            mListener = listener;
        }


        public FilterRecyclerAdapter(int layoutResId) {
            super(layoutResId);
        }

        @Override
        protected void convert(BaseViewHolder helper, Filter item) {
            ((TextView) helper.getView(R.id.item_name_tv)).setText(item.description());

            ImageView avatarIv = helper.getView(R.id.item_avatar_riv);
            avatarIv.setImageResource(item.resId());
            avatarIv.setOnClickListener(v -> {
                if (mCurrentPos >= 0) {
                    getData().get(mCurrentPos).setSelect(false);
                    notifyItemChanged(mCurrentPos);
                }
                mCurrentPos = getData().indexOf(item);
                item.setSelect(true);
                notifyItemChanged(mCurrentPos);

                if (mListener != null) {
                    mListener.onItemClick(v, item, getData().indexOf(item));
                }
            });

            ImageView selectIv = helper.getView(R.id.item_avatar_select_iv);
            selectIv.setVisibility(item.isSelect() ? View.VISIBLE : View.INVISIBLE);
        }

        public void setSelect(int position, boolean isClick) {
            if (getData() != null) {
                for (int i = 0; i < getData().size(); i++) {
                    getData().get(i).setSelect(i == position);
                }
                notifyDataSetChanged();
                mCurrentPos = position;
                if (isClick) {
                    if (mListener != null) {
                        Filter filter = getData().get(position);
                        mListener.onItemClick(null, filter, position);
                    }
                }

            }
        }
    }
}
