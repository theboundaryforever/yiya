package com.yiya.mobile.room.avroom.holder;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.HashMap;

import static com.yiya.mobile.room.avroom.adapter.AudioMicroViewAdapter.TYPE_INVALID;

public class YiYaAudioMicroViewHolder extends BaseAudioMicroViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private final View groupAvatar;
    TextView tvNick;
    ImageView ivUpImage;
    ImageView ivLockImage;
    ImageView ivMuteImage;
    Context context;
    TextView charmTv;

    public YiYaAudioMicroViewHolder(Context context, View itemView) {
        super(context, itemView);
        this.context = context;
        ivUpImage = itemView.findViewById(R.id.up_image);
        ivLockImage = itemView.findViewById(R.id.lock_image);
        ivMuteImage = itemView.findViewById(R.id.mute_image);
        tvNick = itemView.findViewById(R.id.nick);
        ivHeadWear = itemView.findViewById(R.id.avatar);
        groupAvatar = itemView.findViewById(R.id.group_avatar);

        charmTv = itemView.findViewById(R.id.charm_tv);

        ivUpImage.setOnClickListener(this);
        ivLockImage.setOnClickListener(this);
        ivHeadWear.setOnLongClickListener(this);
    }

    public void clear() {
        super.clear();
        ivUpImage.setVisibility(View.VISIBLE);
        ivLockImage.setVisibility(View.GONE);
        ivMuteImage.setVisibility(View.GONE);
        groupAvatar.setVisibility(View.GONE);
        tvNick.setText("");


    }

    public void bind(RoomQueueInfo info, int position) { // 传过来的是麦位
        super.bind(info, position);
        if (info == null) {
            return;
        }
        RoomMicInfo roomMicInfo = info.mRoomMicInfo;
        IMChatRoomMember chatRoomMember = info.mChatRoomMember;
        if (roomMicInfo == null) {
            ivUpImage.setVisibility(View.VISIBLE);
            ivLockImage.setVisibility(View.GONE);
            ivMuteImage.setVisibility(View.GONE);
            groupAvatar.setVisibility(View.GONE);
            tvNick.setText("");
            return;
        }

        //显示，先展示人，无视麦的锁
        if (chatRoomMember != null) {
            ivLockImage.setVisibility(View.GONE);
            ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
            if (!TextUtils.isEmpty(chatRoomMember.getAccount()) && JavaUtil.str2long(chatRoomMember.getAccount()) > 0) {
                ivUpImage.setVisibility(View.INVISIBLE);
                groupAvatar.setVisibility(View.VISIBLE);
                tvNick.setVisibility(View.VISIBLE);
                ivHeadWear.setOnClickListener(v -> onMicroItemClickListener.onAvatarBtnClick(position, chatRoomMember));

                if ((AvRoomDataManager.get().getRoomInfo() != null && AvRoomDataManager.get().getRoomInfo().getCharmEnable() == 2) || AvRoomDataManager.get().isHasCharm()) {
                    charmTv.setVisibility(View.VISIBLE);
                } else {
                    charmTv.setVisibility(View.INVISIBLE);
                }

                tvNick.setText(chatRoomMember.getNick());

            } else {
                ivUpImage.setVisibility(View.VISIBLE);
                groupAvatar.setVisibility(View.GONE);
                tvNick.setText("");
                charmTv.setVisibility(View.INVISIBLE);
            }
        } else {
            charmTv.setVisibility(View.INVISIBLE);
            //锁麦
            if (roomMicInfo.isMicLock()) {
                ivUpImage.setVisibility(View.INVISIBLE);
                ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
                ivLockImage.setVisibility(View.VISIBLE);
                groupAvatar.setVisibility(View.GONE);
            } else {
                ivMuteImage.setVisibility(roomMicInfo.isMicMute() ? View.VISIBLE : View.GONE);
                ivUpImage.setVisibility(View.VISIBLE);
                groupAvatar.setVisibility(View.GONE);
                ivLockImage.setVisibility(View.GONE);
            }

            tvNick.setText((position + 1) + "号麦位");
        }
    }

    @Override
    protected void updateCharmValue(HashMap<String, Integer> map) {
        if (map == null) {
            charmTv.setText("0");
            return;
        }
        Integer charm = map.get(KEY_CHARM);
        charmTv.setText(StringUtils.charmCalculate(charm == null ? 0 : charm));
        Integer gender = map.get(KEY_GENDER);
        int genderResId = (gender != null && gender == 1) ? R.drawable.ic_room_charm_male : R.drawable.ic_room_charm_female;
        charmTv.setCompoundDrawablesWithIntrinsicBounds(genderResId, 0, 0, 0);
    }

    @Override
    public void onClick(View v) {
        if (info == null || position == TYPE_INVALID || onMicroItemClickListener == null)
            return;
        if (v.getId() == R.id.up_image || v.getId() == R.id.lock_image) {
            if (position == -1) return;
            onMicroItemClickListener.onUpMicBtnClick(position);
        } else if (v.getId() == R.id.lock_image) {
            if (position == -1) return;
            onMicroItemClickListener.onLockBtnClick(position);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (onMicroItemClickListener != null) {
            onMicroItemClickListener.onAvatarSendMsgClick(position);
        }
        return true;
    }
}
