package com.yiya.mobile.room.egg.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.yiya.mobile.base.fragment.BaseMvpListFragment;
import com.yiya.mobile.presenter.egg.IPlantBeanRuleListView;
import com.yiya.mobile.presenter.egg.PlantBeanRuleListPresenter;
import com.yiya.mobile.room.egg.adapter.PlantRuleListAdapter;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/22.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
@CreatePresenter(PlantBeanRuleListPresenter.class)
public class PlantRuleListFragment extends BaseMvpListFragment<PlantRuleListAdapter, IPlantBeanRuleListView, PlantBeanRuleListPresenter> implements IPlantBeanRuleListView {

    @Override
    protected void initMyView() {
        hasDefualLoadding = false;
        srlRefresh.setEnableRefresh(false);
        srlRefresh.setEnableLoadMore(false);
    }

    @Override
    protected RecyclerView.LayoutManager initManager() {
        return new LinearLayoutManager(getContext());
    }

    @Override
    protected PlantRuleListAdapter initAdapter() {
        return new PlantRuleListAdapter();
    }


    @Override
    public void onSetListener() {
    }

    @Override
    protected void initClickListener() {
    }

    @Override
    public void initData() {
        getMvpPresenter().getRuleList();
    }

    @Override
    public void showRuleList(ServiceResult<List<String>> serviceResult) {
        dealSuccess(serviceResult, "暂无数据");
    }
}
