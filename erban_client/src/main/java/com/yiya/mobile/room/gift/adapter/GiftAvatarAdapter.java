package com.yiya.mobile.room.gift.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yiya.mobile.ui.common.widget.CircleImageView;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;

import java.util.List;

/**
 * Created by chenran on 2017/10/25.
 */

public class GiftAvatarAdapter extends RecyclerView.Adapter<GiftAvatarAdapter.ViewHolder> implements View.OnClickListener {
    private List<MicMemberInfo> micMemberInfos;
    private Context context;
    private int selectCount = 1;
    private boolean isSingle;
    private OnItemSelectedListener onItemSelectedListener;
    public boolean justRoomOwner = false;

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public GiftAvatarAdapter(Context context) {
        this.context = context;
        this.isSingle = false;
    }

    public void setMicMemberInfos(List<MicMemberInfo> micMemberInfos) {
        this.micMemberInfos = micMemberInfos;
    }

    public List<MicMemberInfo> getMicMemberInfos() {
        return micMemberInfos;
    }

    public void setSelectCount(int selectCount) {
        this.selectCount = selectCount;
    }

    public int getSelectCount() {
        return selectCount;
    }

    public boolean isSingle() {
        return isSingle;
    }

    public void setSingle(boolean single) {
        this.isSingle = single;
    }

    @Override
    public GiftAvatarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_gift_avatar, parent, false);
        return new GiftAvatarAdapter.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(GiftAvatarAdapter.ViewHolder holder, int position) {
        holder.avatarContainer.setTag(position);
        if (this.isSingle) {
            setupItem(holder, position);
        } else {
            if (position == 0) {

                holder.itemView.setVisibility(justRoomOwner ? View.GONE : View.VISIBLE);
                holder.nickName.setText(R.string.dialog_gift_avatar_list_default_value);
                if (selectCount == 0) {
                    holder.nickName.setTextColor(ContextCompat.getColor(this.context, R.color.red_fd2772));
                } else {
                    holder.nickName.setTextColor(ContextCompat.getColor(this.context, R.color.white));
                }
            } else {
                setupItem(holder, position);
            }
        }
    }

    private void setupItem(ViewHolder holder, int position) {
        int micMemberInfoPosition = isSingle ? position : position - 1;
        final MicMemberInfo micMemberInfo = micMemberInfos.get(micMemberInfoPosition);
        ImageLoadUtils.loadAvatar(holder.avatar.getContext(), micMemberInfo.getAvatar(), holder.avatar);
        holder.nickName.setText(micMemberInfo.getNick());
        if (selectCount == 0) {
            holder.nickName.setSelected(true);
        } else {
            if (selectCount == position) {
                holder.nickName.setSelected(true);
            } else {
                holder.nickName.setSelected(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (micMemberInfos == null) {
            return 0;
        } else {
            return isSingle ? micMemberInfos.size() : micMemberInfos.size() + 1;
        }
    }

    @Override
    public void onClick(View v) {
        Integer position = (Integer) v.getTag();
        selectCount = position;
        notifyDataSetChanged();
        if (onItemSelectedListener != null) {
            onItemSelectedListener.onItemSelected(position);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View itemView;
        private CircleImageView avatar;
        private TextView nickName;
        private RelativeLayout avatarContainer;

        public ViewHolder(View itemView) {
            super(itemView);

            avatar = (CircleImageView) itemView.findViewById(R.id.avatar);
            nickName = (TextView) itemView.findViewById(R.id.user_name);
            avatarContainer = (RelativeLayout) itemView.findViewById(R.id.avatar_container);
            avatarContainer.setOnClickListener(GiftAvatarAdapter.this);

            this.itemView = itemView;
        }
    }

    public interface OnItemSelectedListener {
        void onItemSelected(int position);
    }
}
