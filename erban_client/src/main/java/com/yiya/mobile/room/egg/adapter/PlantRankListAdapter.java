package com.yiya.mobile.room.egg.adapter;

import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/22.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantRankListAdapter extends BaseQuickAdapter<UserInfo, BaseViewHolder> {

    private int roomType = RoomInfo.ROOMTYPE_VIDEO_LIVE;


    public PlantRankListAdapter() {
        super(R.layout.item_plant_bean_rank_list);
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    @Override
    protected void convert(BaseViewHolder helper, UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }

        int resDrawable = R.mipmap.ic_plant_bean_rank_first;
        int rank = helper.getAdapterPosition() + 1;
        switch (rank) {
            case 1:
                resDrawable = R.mipmap.ic_plant_bean_rank_first;
                break;
            case 2:
                resDrawable = R.mipmap.ic_plant_bean_rank_second;
                break;
            case 3:
                resDrawable = R.mipmap.ic_plant_bean_rank_third;
                break;
        }
        helper.setText(R.id.tv_gold, String.valueOf(userInfo.getTol()))
                .setText(R.id.tv_gift_name, userInfo.getNick())
                .setVisible(R.id.iv_rank, helper.getAdapterPosition() < 3)
                .setImageResource(R.id.iv_rank, resDrawable)
                .setGone(R.id.tv_rank, !(helper.getAdapterPosition() < 3))
                .setText(R.id.tv_rank, String.valueOf(rank));

        ImageLoadUtils.loadAvatar(mContext, userInfo.getAvatar(), helper.getView(R.id.civ_avatar));

        //等级
        ImageView ivUserLevel = helper.getView(R.id.iv_user_level);
        ImageView ivExperLevel = helper.getView(R.id.iv_exper_level);
        ImageView ivCharmLevel = helper.getView(R.id.iv_charm_level);
        //用户等级
        if(ivUserLevel != null){
            if (StringUtils.isNotEmpty(userInfo.getVideoRoomExperLevelPic())) {
                ivUserLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, userInfo.getVideoRoomExperLevelPic(), ivUserLevel);
            } else {
                ivUserLevel.setVisibility(View.GONE);
            }
        }
        // 财富等级
        if(ivExperLevel != null){
            if (StringUtils.isNotEmpty(userInfo.getExperLevelPic())) {
                ivExperLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, userInfo.getExperLevelPic(), ivExperLevel);
            } else {
                ivExperLevel.setVisibility(View.GONE);
            }
        }
//        // 魅力等级
//        if(ivCharmLevel != null){
//            if (StringUtils.isNotEmpty(userInfo.getCharmLevelPic())) {
//                ivCharmLevel.setVisibility(View.VISIBLE);
//                ImageLoadUtils.loadImage(mContext, userInfo.getCharmLevelPic(), ivCharmLevel);
//            } else {
//                ivCharmLevel.setVisibility(View.GONE);
//            }
//        }
    }
}
