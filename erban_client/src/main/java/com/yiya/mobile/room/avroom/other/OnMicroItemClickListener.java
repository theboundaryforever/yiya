package com.yiya.mobile.room.avroom.other;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tongdaxing.xchat_core.bean.IMChatRoomMember;

public abstract class OnMicroItemClickListener {
    public void onAvatarBtnClick(int position, @Nullable IMChatRoomMember chatRoomMember) {

    }

    public void onUpMicBtnClick(int micPosition) {

    }

    public void onDownMicBtnClick(int micPosition) {

    }

    public void onNoteGiftClick(@NonNull IMChatRoomMember chatRoomMember) {

    }

    public void onLockBtnClick(int position) {

    }

    public void onAvatarSendMsgClick(int position) {

    }

    public void onGiftBtnClick(@NonNull IMChatRoomMember chatRoomMember) {

    }
}
