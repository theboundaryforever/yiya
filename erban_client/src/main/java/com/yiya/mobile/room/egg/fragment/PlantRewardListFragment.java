package com.yiya.mobile.room.egg.fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.yiya.mobile.base.fragment.BaseMvpListFragment;
import com.yiya.mobile.presenter.egg.IPlantRewardListView;
import com.yiya.mobile.presenter.egg.PlantRewardListPresenter;
import com.yiya.mobile.room.egg.adapter.PlantRewardListAdapter;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
@CreatePresenter(PlantRewardListPresenter.class)
public class PlantRewardListFragment extends BaseMvpListFragment<PlantRewardListAdapter, IPlantRewardListView, PlantRewardListPresenter> implements IPlantRewardListView {

    @Override
    protected void initMyView() {
        hasDefualLoadding = false;
        srlRefresh.setEnableRefresh(false);
        srlRefresh.setEnableLoadMore(false);
    }

    @Override
    protected RecyclerView.LayoutManager initManager() {
        return new GridLayoutManager(getContext(), 3);
    }

    @Override
    protected PlantRewardListAdapter initAdapter() {
        return new PlantRewardListAdapter();
    }

    @Override
    public void onSetListener() {
    }

    @Override
    protected void initClickListener() {
    }

    @Override
    public void initData() {
        getMvpPresenter().getPoundEggGifts();
    }

    @Override
    public void showPoundEggGifts(ServiceResult<List<EggGiftInfo>> serviceResult) {
        dealSuccess(serviceResult, "暂无数据");
    }
}
