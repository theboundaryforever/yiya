package com.yiya.mobile.room.avroom.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.yiya.mobile.ui.widget.ScaleTransitionPagerTitleView;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/15.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomChatIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;

    private int normalColorId = R.color.color_383950;
    private int selectColorId = R.color.color_FF495C;

    public RoomChatIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mContext = context;
        mTitleList = titleList;
    }

    private int size = 18;
    private Typeface mTextStyle = Typeface.DEFAULT;

    public void setSize(int size) {
        this.size = size;
    }

    public void setTextStyle(Typeface textStyle) {
        mTextStyle = textStyle;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {
        ScaleTransitionPagerTitleView scaleTransitionPagerTitleView = new ScaleTransitionPagerTitleView(context);
        scaleTransitionPagerTitleView.setNormalColor(ContextCompat.getColor(mContext, normalColorId));
        scaleTransitionPagerTitleView.setSelectedColor(ContextCompat.getColor(mContext, selectColorId));
        scaleTransitionPagerTitleView.setMinScale(1);
        scaleTransitionPagerTitleView.setTextSize(size);
        scaleTransitionPagerTitleView.setTypeface(mTextStyle);
        scaleTransitionPagerTitleView.setText(mTitleList.get(i).getName());
        scaleTransitionPagerTitleView.setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }

        });
        return scaleTransitionPagerTitleView;
    }


    @Override
    public IPagerIndicator getIndicator(Context context) {
        LinePagerIndicator indicator = new LinePagerIndicator(context);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        indicator.setColors(Color.TRANSPARENT);
        indicator.setLayoutParams(lp);
        return indicator;
    }

    private OnItemSelectListener mOnItemSelectListener;

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}