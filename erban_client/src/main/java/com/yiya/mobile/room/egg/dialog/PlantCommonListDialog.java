package com.yiya.mobile.room.egg.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.room.egg.fragment.MyPlantRewordFragment;
import com.yiya.mobile.room.egg.fragment.PlantRankListFragment;
import com.yiya.mobile.room.egg.fragment.PlantRewardListFragment;
import com.yiya.mobile.room.egg.fragment.PlantRuleListFragment;
import com.juxiao.library_utils.DisplayUtils;
import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ProjectName:
 * Description:种豆公共ListDialog
 * Created by BlackWhirlwind on 2019/7/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantCommonListDialog extends BaseTransparentDialogFragment {
    public static final int TYPE_REWARD_LIST = 0;
    public static final int TYPE_RANK_LIST = 1;
    public static final int TYPE_RECORD_LIST = 2;
    public static final int TYPE_RULE_LIST = 3;
    public static final String KEY_TYPE = "KEY_TYPE";
    @BindView(R.id.fl_container)
    FrameLayout flContainer;
    @BindView(R.id.iv_title)
    ImageView ivTitle;

    public static PlantCommonListDialog newInstance(int type) {
        PlantCommonListDialog dialog = new PlantCommonListDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_TYPE, type);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_plant_common_list;
    }

    @Override
    protected int getWindowAnimationStyle() {
        return R.style.WindowBottomAnimationStyle;
    }

    @Override
    protected int getWindowGravity() {
        return Gravity.BOTTOM;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            int type = getArguments().getInt(KEY_TYPE);
            Fragment fragment = null;
            int titleRes = -1;
            int height = 420;
            switch (type) {
                case TYPE_REWARD_LIST:
                    titleRes = R.drawable.bg_egg_reward_list_title;
                    fragment = new PlantRewardListFragment();
                    height = 420;
                    break;
                case TYPE_RANK_LIST:
                    titleRes = R.drawable.bg_plant_rank_list_title;
                    fragment = new PlantRankListFragment();
                    height = 420;
                    break;
                case TYPE_RECORD_LIST:
                    titleRes = R.drawable.bg_plant_bean_record_title;
                    fragment = new MyPlantRewordFragment();
                    height = 420;
                    break;
                case TYPE_RULE_LIST:
                    titleRes = R.drawable.bg_plant_bean_rule_title;
                    fragment = new PlantRuleListFragment();
                    height = 225;
                    break;
            }
            if (fragment != null) {
                flContainer.getLayoutParams().height = DisplayUtils.dip2px(getContext(), height);
                ivTitle.setImageResource(titleRes);
                getChildFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fl_container, fragment)
                        .commitAllowingStateLoss();
            }
        }
    }

    @OnClick(R.id.iv_close)
    public void onViewClicked() {
        dismiss();
    }
}
