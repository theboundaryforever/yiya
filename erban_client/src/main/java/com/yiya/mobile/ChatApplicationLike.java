package com.yiya.mobile;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.http.HttpResponseCache;
import android.os.Bundle;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.bumptech.glide.request.target.ViewTarget;
import com.growingio.android.sdk.collection.Configuration;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.reciever.ConnectiveChangedReceiver;
import com.yiya.mobile.ui.launch.activity.MiddleActivity;
import com.yiya.mobile.ui.launch.activity.NimMiddleActivity;
import com.yiya.mobile.utils.ContactHelper;
import com.yiya.mobile.utils.CrashHandler;
import com.yiya.mobile.utils.SessionHelper;
import com.juxiao.library_utils.LibUtils;
import com.juxiao.library_utils.SystemUtil;
import com.juxiao.library_utils.log.LogLevel;
import com.llew.huawei.verifier.LoadedApkHuaWei;
import com.mcxiaoke.packer.helper.PackerNg;
import com.microquation.linkedme.android.LinkedME;
import com.mob.MobSDK;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.SDKOptions;
import com.netease.nimlib.sdk.StatusBarNotificationConfig;
import com.netease.nimlib.sdk.msg.MessageNotifierCustomization;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.pingplusplus.android.Pingpp;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.tinker.anno.DefaultLifeCycle;
import com.tencent.tinker.entry.DefaultApplicationLike;
import com.tencent.tinker.loader.shareutil.ShareConstants;
import com.tinkerpatch.sdk.TinkerPatch;
import com.tongdaxing.erban.BuildConfig;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.ErBanAllHostnameVerifier;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RxNet;
import com.tongdaxing.erban.libcommon.net.rxnet.model.HttpParams;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.CoreRegisterCenter;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.Env;
import com.tongdaxing.xchat_core.LoginSyncDataStatusObserver;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.activity.IActivityCore;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCore;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.login.IIMLoginCore;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.im.notification.INotificationCore;
import com.tongdaxing.xchat_core.im.room.IIMRoomCore;
import com.tongdaxing.xchat_core.im.sysmsg.ISysMsgCore;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.realm.IRealmCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCore;
import com.tongdaxing.xchat_core.room.auction.IAuctionCore;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.constant.AppKey;
import com.tongdaxing.xchat_framework.util.util.ChannelUtil;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.VersionUtil;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;
import com.zhy.autolayout.utils.ScreenUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Stack;

import cn.jpush.android.api.JPushInterface;
import io.reactivex.plugins.RxJavaPlugins;

import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI;
import static com.tongdaxing.xchat_framework.util.util.VersionUtil.HUAWEI;
import static com.tongdaxing.xchat_framework.util.util.VersionUtil.VERSION_5_1;
import static com.tongdaxing.xchat_framework.util.util.VersionUtil.VERSION_5_1_1;

//import com.didichuxing.doraemonkit.DoraemonKit;

@DefaultLifeCycle(application = "com.yiya.mobile.ChatApplication",
        flags = ShareConstants.TINKER_ENABLE_ALL,
        loadVerifyFlag = false)
public class ChatApplicationLike extends DefaultApplicationLike {
    public ChatApplicationLike(Application application, int tinkerFlags, boolean tinkerLoadVerifyFlag, long applicationStartElapsedTime, long applicationStartMillisTime, Intent tinkerResultIntent) {
        super(application, tinkerFlags, tinkerLoadVerifyFlag, applicationStartElapsedTime, applicationStartMillisTime, tinkerResultIntent);
    }

    public static final String TAG = "ChatApplication";
    public Application application;
    private static ChatApplicationLike applicationLike;

    private RefWatcher mRefWatcher;

    //代码段防止SmartRefreshLayout内存泄露
    static {
        SmartRefreshLayout.setDefaultRefreshHeaderCreater((context, layout) -> {
            layout.setEnableHeaderTranslationContent(false);
            MaterialHeader materialHeader = new MaterialHeader(context);
            materialHeader.setColorSchemeColors(R.color.mm_theme);
            materialHeader.setShowBezierWave(false);
            return materialHeader;
        });
        SmartRefreshLayout.setDefaultRefreshFooterCreater(
                (context, layout) -> new ClassicsFooter(context).setDrawableSize(20));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application.registerActivityLifecycleCallbacks(onActivityLifecycleCallbacks);

        LibUtils.init(application);
        if (!com.tongdaxing.xchat_framework.BuildConfig.IS_DEBUG) {
            com.juxiao.library_utils.log.LogUtil.setLevelForConsole(LogLevel.LEVEL_NONE);
        } else {
            com.juxiao.library_utils.log.LogUtil.setLevelForConsole(LogLevel.LEVEL_ALL);
        }
        com.juxiao.library_utils.log.LogUtil.setLevelForFile(LogLevel.LEVEL_INFO);

        LogUtils.d("runTest", TAG + " onCreate start " + System.currentTimeMillis());
        //云信config在非主进程也调用
        NIMClient.config(application, null, options());

        String channel = getChannel();

        //初始化tinker，官方声明需要确保至少对主进程跟patch进程初始化，所以这里所有进程都做初始化
        initTinker(channel);

        MobSDK.init(application);

        //初始化faceu 全局加载相应的底层数据包
//        FURenderer.initFURenderer(application);

//        LogUtils.e("application-process: ",);

        //growingio
        GrowingIO.startWithConfiguration(application, new Configuration()
                .trackAllFragments()
                .setChannel(channel)
                .setHashTagEnable(true)
                .setDebugMode(BasicConfig.isDebug)
                .setTestMode(BasicConfig.isDebug));

        String processName = SystemUtil.getProcessName(application);

        if (SystemUtil.inMainProcess(application)) {
            OkHttpManager.getInstance().init(application);
            //云信initSdk只需要在主进程调用
            NIMClient.initSDK();
            RxJavaPlugins.setErrorHandler(throwable -> {
                throwable.printStackTrace();

                com.juxiao.library_utils.log.LogUtil.e(TAG, "rxjava error ", throwable);
                com.juxiao.library_utils.log.LogUtil.flushToDisk();
            });
            //fixed: Glide Exception:"You must not call setTag() on a view Glide is targeting"
            ViewTarget.setTagId(R.id.tag_glide);

            init(channel);

            //友盟统计
            UMConfigure.init(application, AppKey.getUmengAppkey(), channel, UMConfigure.DEVICE_TYPE_PHONE, null);
            MobclickAgent.setDebugMode(true);
            if (!BasicConfig.isDebug) {
                CrashHandler.getInstance().init(application);
            }
            if (BasicConfig.isDebug) {
                //设置debug模式下打印LinkedME日志
                LinkedME.getInstance(application).setDebug();
            } else {
                LinkedME.getInstance(application);//执行一遍（初始化）
            }
            LinkedME.getInstance().setImmediate(false);
            LinkedME.getInstance().setHandleActivity(MiddleActivity.class.getName());

            //bugly初始化
            CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(application);
            strategy.setAppChannel(channel);
            strategy.setUploadProcess(processName.equals("null_name") || processName.equals(application.getPackageName()));
            strategy.setCrashHandleCallback(new CrashReport.CrashHandleCallback() {
                public Map<String, String> onCrashHandleStart(int crashType, String errorType,
                                                              String errorMessage, String errorStack) {
                    if (CrashReport.CrashHandleCallback.CRASHTYPE_NATIVE == crashType) {
                        com.juxiao.library_utils.log.LogUtil.i(TAG, "native crash occur");
                    } else {
                        com.juxiao.library_utils.log.LogUtil.i(TAG, "crash occur");
                    }
                    com.juxiao.library_utils.log.LogUtil.e(TAG, String.format("%s \n %s", errorMessage, errorStack));
                    com.juxiao.library_utils.log.LogUtil.flushToDisk();
                    return null;
                }

                @Override
                public byte[] onCrashHandleStart2GetExtraDatas(int crashType, String errorType,
                                                               String errorMessage, String errorStack) {
                    return null;
                }

            });
            if (!BasicConfig.isDebug) {
                CrashReport.initCrashReport(application, "445011e867", false, strategy);
            } else {
                CrashReport.initCrashReport(application, "7900a8aa58", true, strategy);
            }

            //极光推送
            JPushInterface.setDebugMode(true);
            JPushInterface.init(application);
//            Log.d(TAG, "onCreate: ----->" + JPushInterface.getRegistrationID(application));


            //华为手机5.1 和 5.1.1系统注册广播过多的异常
            initHuaweiVerifier();
        } else if ("com.yiya.mobile:pushcore".equals(processName)) {
            //极光推送(3.3.1版本后不需要)

            JPushInterface.setDebugMode(true);
            JPushInterface.init(application);

//            Log.d(TAG, "onCreate: ----->" + JPushInterface.getRegistrationID(application));


        }
        LogUtils.d("runTest", TAG + " onCreate end " + System.currentTimeMillis());
    }


    @Override
    public void onBaseContextAttached(Context base) {
        super.onBaseContextAttached(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);
        applicationLike = this;
        application = getApplication();
    }

    /**
     * 修复华为华为5.1与5.1.1两个版本提示注册广播过多的bug
     */
    private void initHuaweiVerifier() {
        if (VersionUtil.getManufacturer().equals(HUAWEI)) {
            String systemVersion = VersionUtil.getSystemVersion();
            if (VERSION_5_1.equals(systemVersion) || VERSION_5_1_1.equals(systemVersion)) {
                LoadedApkHuaWei.hookHuaWeiVerifier(application);
            }
        }
    }

    /**
     * 云信sdk配置
     */
    public SDKOptions options() {
        SDKOptions options = new SDKOptions();
        options.asyncInitSDK = true;
        if (BasicConfig.isDebug)
            options.checkManifestConfig = true;
        // 如果将新消息通知提醒托管给 SDK 完成，需要添加以下配置。否则无需设置。
        StatusBarNotificationConfig config = new StatusBarNotificationConfig();
        // 点击通知栏跳转到该Activity
        config.notificationEntrance = NimMiddleActivity.class;
//        config.notificationSmallIconId = R.drawable.icon_msg_normal;
        // 呼吸灯配置
        config.ledARGB = Color.GREEN;
        config.ledOnMs = 1000;
        config.ledOffMs = 1500;
        // 通知铃声的uri字符串
        config.notificationSound = "android.resource://com.netease.nim.demo/raw/msg";
        options.statusBarNotificationConfig = config;
        // 定制通知栏提醒文案（可选，如果不定制将采用SDK默认文案）
        options.messageNotifierCustomization = messageNotifierCustomization;

        options.appKey = Constants.nimAppKey;

        // 配置保存图片，文件，log 等数据的目录
        // 如果 options 中没有设置这个值，SDK 会使用下面代码示例中的位置作为 SDK 的数据目录。
        // 该目录目前包含 log, file, image, audio, video, thumb 这6个目录。
        // 如果第三方 APP 需要缓存清理功能， 清理这个目录下面个子目录的内容即可。
        String sdkPath = null;
        try {
            sdkPath = Environment.getExternalStorageDirectory() + "/" + application.getPackageName() + "/nim";
        } catch (ArrayIndexOutOfBoundsException e) {
            LogUtil.w(TAG, "NIMClient init options exception : " + e.getMessage());
        }
        options.sdkStorageRootPath = sdkPath;

        // 配置是否需要预下载附件缩略图，默认为 true
        options.preloadAttach = true;

        // 配置附件缩略图的尺寸大小。表示向服务器请求缩略图文件的大小
        // 该值一般应根据屏幕尺寸来确定， 默认值为 Screen.width / 2
        int[] size = ScreenUtils.getScreenSize(application, true);
        options.thumbnailSize = size[0] / 2;
//        // save cache，留做切换账号备用
        DemoCache.setNotificationConfig(config);
        return options;
    }

    /**
     * 云信消息通知配置
     */
    private MessageNotifierCustomization messageNotifierCustomization = new MessageNotifierCustomization() {
        @Override
        public String makeNotifyContent(String nick, IMMessage message) {
            if (message.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment customAttachment = (CustomAttachment) message.getAttachment();
                if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                    return message.getFromNick();
                }
            }
            // 采用SDK默认文案
            return "收到一条消息";
        }

        @Override
        public String makeTicker(String nick, IMMessage message) {
            if (message.getMsgType() == MsgTypeEnum.custom) {
                CustomAttachment customAttachment = (CustomAttachment) message.getAttachment();
                if (customAttachment != null && customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                    return message.getFromNick();
                }
            }
            // 采用SDK默认文案
            return "收到一条消息";
        }

        @Override
        public String makeRevokeMsgTip(String revokeAccount, IMMessage item) {
            return null;
        }
    };

    private String getChannel() {
        String channel = PackerNg.getChannel(application);
        String urlChannel = "";
        try {
            urlChannel = ChannelUtil.getChannel(application);
        } catch (Exception e) {
            LogUtil.w(TAG, "ChannelUtil exception : " + e.getMessage());
        }

        if (!TextUtils.isEmpty(urlChannel)) {
            channel = urlChannel;
        }
        return channel;
    }

    public static void initRxNet(Context context, String url) {
        HttpParams httpParams = new HttpParams();
        RxNet.init(context)
                .debug(BasicConfig.isDebug)
                .setBaseUrl(url)
                .setHttpParams(httpParams)
                .certificates()
                .hostnameVerifier(new ErBanAllHostnameVerifier())
                .build();
    }

    private void init(String channel) {
        initNimUIKit();

        BasicConfig.INSTANCE.setAppContext(application.getApplicationContext());

        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return BasicConfig.isDebug;
            }
        });

        //切换生产坏境和测试环境 true/测试环境 false/生产环境
        BasicConfig.INSTANCE.setDebuggable(BasicConfig.isDebug);
        BasicConfig.INSTANCE.setChannel(channel);

        Pingpp.DEBUG = BasicConfig.isDebug;
        Env.instance().init();

        BasicConfig.INSTANCE.setRootDir(Constants.ERBAN_DIR_NAME);
        BasicConfig.INSTANCE.setLogDir(Constants.LOG_DIR);

        try {
            File cacheDir = new File(application.getApplicationContext().getCacheDir(), "http");
            HttpResponseCache.install(cacheDir, 1024 * 1024 * 128);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // url
        UriProvider.initDevUri(UriProvider.JAVA_WEB_URL);

        initRxNet(BasicConfig.INSTANCE.getAppContext(), UriProvider.JAVA_WEB_URL);

        //内存泄漏监控
        setupLeakCanary();

        if (BasicConfig.INSTANCE.isDebuggable()) {
//            DoraemonKit.install(application);

        }

        ConnectiveChangedReceiver.getInstance().init(application);
        //corexxxw
        CoreManager.init(BasicConfig.INSTANCE.getLogDir().getAbsolutePath());
        CoreRegisterCenter.registerCore();

        CoreManager.getCore(IRealmCore.class);
        CoreManager.getCore(IUserCore.class);
        CoreManager.getCore(IAuctionCore.class);
        CoreManager.getCore(IRedPacketCore.class);
        CoreManager.getCore(IActivityCore.class);
        CoreManager.getCore(IIMLoginCore.class);
        CoreManager.getCore(IIMFriendCore.class);
        CoreManager.getCore(IGiftCore.class);
        CoreManager.getCore(IFaceCore.class);
        CoreManager.getCore(IPayCore.class);
        CoreManager.getCore(IIMMessageCore.class);
        CoreManager.getCore(IIMRoomCore.class);
        CoreManager.getCore(IAVRoomCore.class);

        LogUtil.i(TAG, channel);
//        initBaiduStatistic(channel);

        // 监听登录同步数据完成通知
        LoginSyncDataStatusObserver.getInstance().registerLoginSyncDataStatus(true);
        //系统通知的到达和已读
        CoreManager.getCore(ISysMsgCore.class).registSystemMessageObserver(true);
        //注册im用户状态的系统通知
        CoreManager.getCore(IIMLoginCore.class).registAuthServiceObserver(true);
        //其他端登录观察者
//        CoreManager.getCore(IIMLoginCore.class).registerOtherClientsObserver(true);
        //全局自定义通知
        CoreManager.getCore(INotificationCore.class).observeCustomNotification(true);
    }

    private void initNimUIKit() {
        NimUIKit.init(application);
        // 可选定制项
        // 注册定位信息提供者类（可选）,如果需要发送地理位置消息，必须提供。
        // demo中使用高德地图实现了该提供者，开发者可以根据自身需求，选用高德，百度，google等任意第三方地图和定位SDK。
//        NimUIKit.setLocationProvider(new NimDemoLocationProvider());

        // 会话窗口的定制: 示例代码可详见demo源码中的SessionHelper类。
        // 1.注册自定义消息附件解析器（可选）
        // 2.注册各种扩展消息类型的显示ViewHolder（可选）
        // 3.设置会话中点击事件响应处理（一般需要）
        SessionHelper.init();

        // 通讯录列表定制：示例代码可详见demo源码中的ContactHelper类。
        // 1.定制通讯录列表中点击事响应处理（一般需要，UIKit 提供默认实现为点击进入聊天界面)
        ContactHelper.init();
    }

    private void setupLeakCanary() {
        if (!BasicConfig.INSTANCE.isDebuggable()) {
            return;
        }
        if (LeakCanary.isInAnalyzerProcess(application)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        mRefWatcher = LeakCanary.install(application);
    }

    private void initTinker(String channel) {
        if (BuildConfig.TINKER_ENABLE) {
            //开始检查是否有补丁，这里配置的是每隔访问3小时服务器是否有更新。
            TinkerPatch.init(this)
                    .reflectPatchLibrary()
                    .setPatchRollbackOnScreenOff(true)
                    .setPatchRestartOnSrceenOff(true)
                    .setAppChannel(channel)
                    //每3小时访问服务器是否有补丁更新
                    .setFetchPatchIntervalByHours(3);

            //通过handler实现轮训的效果
            TinkerPatch.with().fetchPatchUpdateAndPollWithInterval();
        }
    }

    public static ChatApplicationLike getApplicationLike() {
        return applicationLike;
    }

    public static RefWatcher getRefWatcher() {
        return ChatApplicationLike.getApplicationLike().mRefWatcher;
    }

    private Stack<Activity> activities;

    private Application.ActivityLifecycleCallbacks onActivityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if (activities == null) {
                activities = new Stack<>();
            }
            activities.push(activity);
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            if (activities != null) {
                activities.remove(activity);
            }
        }
    };

    public Stack<Activity> getActivityStack() {
        return activities;
    }
}
