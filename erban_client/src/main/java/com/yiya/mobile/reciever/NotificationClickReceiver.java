package com.yiya.mobile.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.yiya.mobile.ui.EmptyActivity;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

/**
 * Created by chenran on 2017/11/16.
 */

public class NotificationClickReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo != null) {
            EmptyActivity.startAvRoom(context, roomInfo.getUid(), roomInfo.getType());
        }
    }
}
