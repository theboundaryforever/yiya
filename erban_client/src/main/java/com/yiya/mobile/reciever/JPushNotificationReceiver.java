package com.yiya.mobile.reciever;

import android.content.Context;
import android.text.TextUtils;

import com.yiya.mobile.ui.EmptyActivity;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import cn.jpush.android.api.NotificationMessage;
import cn.jpush.android.service.JPushMessageReceiver;

/**
 * 文件描述：极光推送消息接收位置
 *
 * @auther：zwk
 * @data：2019/2/13
 */
public class JPushNotificationReceiver extends JPushMessageReceiver {

    /**
     * 2019-05-23 15:27:37.206 31390-31902/com.yiya.mobile D/LogUtil0: JPushNotificationReceiver action: NotificationMessage{notificationId=534578054, msgId='58546805464775164', appkey='9dcd7bb74aada4857c976ac0', notificationContent='ios看到消息', notificationAlertType=-1, notificationTitle='Android看到的消息', notificationSmallIcon='null', notificationLargeIcon='null', notificationExtras='{"apnsProduction":"true","roomType":"6","roomUid":"2915"}', notificationStyle=0, notificationBuilderId=0, notificationBigText='null', notificationBigPicPath='null', notificationInbox='null', notificationPriority=0, notificationCategory='null', developerArg0='null', platform=0, notificationType=0}
     *
     * @param context
     * @param notificationMessage
     */
    @Override
    public void onNotifyMessageOpened(Context context, NotificationMessage notificationMessage) {
        super.onNotifyMessageOpened(context, notificationMessage);

        if (notificationMessage != null && !TextUtils.isEmpty(notificationMessage.notificationExtras)) {
            LogUtil.i("JPushNotificationReceiver action: " + notificationMessage.toString());
            try {
                Json json = new Json(notificationMessage.notificationExtras);
                if (json.has("roomType") && json.has("roomUid")) {
                    int roomType = json.num("roomType");
                    int roomUid = json.num("roomUid");
                    EmptyActivity.startAvRoom(context, roomUid, roomType);
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        EmptyActivity.startHome(context);
    }

    @Override
    public void onRegister(Context context, String s) {
        super.onRegister(context, s);
        LogUtil.i("JPushNotificationReceiver onRegister: " + s);
    }
}
