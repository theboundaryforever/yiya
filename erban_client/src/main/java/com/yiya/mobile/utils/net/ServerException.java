package com.yiya.mobile.utils.net;

/**
 * Created by fwhm on 2017/7/27.
 */

public class ServerException extends Exception {
    public ServerException(String message) {
        super(message);
    }
}
