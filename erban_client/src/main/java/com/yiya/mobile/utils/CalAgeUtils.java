package com.yiya.mobile.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author zdw
 * @date 2019/7/25
 * @describe 年龄计算
 */

public class CalAgeUtils {



    public static Date parse (String strDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return sdf.parse(strDate);
    }
}