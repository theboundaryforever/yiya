package com.yiya.mobile.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;

import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.yiya.mobile.constant.AppConstant;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.room.audio.activity.AudioRecordActivity;
import com.yiya.mobile.ui.launch.activity.UserGuideActivity;
import com.yiya.mobile.ui.login.activity.ForgetPswActivity;
import com.yiya.mobile.ui.login.activity.RegisterActivity;
import com.yiya.mobile.ui.me.setting.activity.ScanActivity;
import com.yiya.mobile.ui.me.setting.activity.SettingActivity;
import com.yiya.mobile.ui.me.user.activity.ModifyInfoActivity;
import com.yiya.mobile.ui.me.user.activity.ShowPhotoActivity;
import com.yiya.mobile.ui.me.user.activity.UserInfoModifyActivity;
import com.yiya.mobile.ui.me.user.activity.UserInfoOthersActivity;
import com.yiya.mobile.ui.me.user.activity.UserModifyPhotosActivity;
import com.yiya.mobile.ui.me.user.activity.YiYaUserInfoActivity;
import com.yiya.mobile.ui.me.wallet.activity.WalletActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;


/**
 * *************************************************************************
 *
 * @Version 1.0
 * @ClassName: UIHelper
 * @Description: 应用程序UI工具包：封装UI相关的一些操作
 * @Author zhouxiangfeng
 * @date 2013-8-6 下午1:39:11
 * **************************************************************************
 */
public class UIHelper {

    public static void showUserGuideAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, UserGuideActivity.class));
    }

    public static void showPhotoPreview(Context context, int position, Json json) {
        if (json == null) {
            return;
        }
        Intent intent = new Intent(context, ShowPhotoActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("photoJsonData", json.toString());
        context.startActivity(intent);
    }

    public static void showRegisterAct(Context mContext, String validateStr) {
        Intent intent = new Intent(mContext, RegisterActivity.class);
        intent.putExtra("validateStr", validateStr);
        mContext.startActivity(intent);
    }

    public static void showSettingAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, SettingActivity.class));
    }

    public static void showScanAct(Context context) {
        context.startActivity(new Intent(context, ScanActivity.class));
    }


    public static void showForgetPswAct(Context mContext) {
        mContext.startActivity(new Intent(mContext, ForgetPswActivity.class));
    }

    //修改用户资料
    public static void showUserInfoModifyAct(Context mContext, long userId) {
        Intent intent = new Intent(mContext, UserInfoModifyActivity.class);
        intent.putExtra("userId", userId);
        mContext.startActivity(intent);
    }

    //我的钱包
    public static void showWalletAct(Context mContext) {
        Intent intent = new Intent(mContext, WalletActivity.class);
        mContext.startActivity(intent);
    }

    //侧边栏===>帮助
    public static void showUsinghelp(Context mContext) {
//        CommonWebViewActivity.start(mContext, "https://mp.weixin.qq.com/s/SRkcOk4QIkqz_zK52KTZmw");

        CommonWebViewActivity.start(mContext, UriProvider.JAVA_WEB_URL + "/front/agreement/index.html");
    }

    /**
     * 邀请奖励H5
     *
     * @param context
     */
    public static void showInvitationH5(Context context) {
        CommonWebViewActivity.start(context, BaseUrl.INVITATION);
    }

    /**
     * 个人主页贡献榜H5
     *
     * @param context
     */
    public static void showUserContributionH5(Context context) {
        CommonWebViewActivity.start(context, BaseUrl.USER_INFO_CONTRIBUTION);
    }

    public static void showUserInfoAct(Context mContext, long userId) {
        Class cls = CoreManager.getCore(IAuthCore.class).getCurrentUid() == userId
                ? YiYaUserInfoActivity.class : UserInfoOthersActivity.class;
        Intent intent = new Intent(mContext, cls);
        intent.putExtra(UserInfo.KEY_USER_ID, userId);
        mContext.startActivity(intent);
    }

    public static void showAudioRecordAct(Context mContext) {
        Intent intent = new Intent(mContext, AudioRecordActivity.class);
        mContext.startActivity(intent);
    }

    public static void showModifyInfoAct(Activity mActivity, int requestCode, String title) {
        Intent intent = new Intent(mActivity, ModifyInfoActivity.class);
        intent.putExtra("title", title);
        mActivity.startActivityForResult(intent, requestCode);
    }

    public static void showModifyPhotosAct(Activity mActivity, long userId) {
        Intent intent = new Intent(mActivity, UserModifyPhotosActivity.class);
        intent.putExtra("userId", userId);
        mActivity.startActivity(intent);
    }

    public static void showModifyPhotosAct(Context mContext, long userId) {
        Intent intent = new Intent(mContext, UserModifyPhotosActivity.class);
        intent.putExtra("userId", userId);
        mContext.startActivity(intent);
    }

    public static void showModifyPhotosAndPhotos(Activity mActivity, long userId, boolean isSelf) {
        Intent intent = new Intent(mActivity, UserModifyPhotosActivity.class);
        intent.putExtra("isSelf", isSelf);
        intent.putExtra("userId", userId);
        mActivity.startActivity(intent);
    }

    /**
     * 贡献榜
     *
     * @param context
     */
    public static void showContributionList(Context context) {
        CommonWebViewActivity.start(context, BaseUrl.CONTRIBUTION);
    }

    /**
     * 圆盘抽奖
     *
     * @param context
     */
    public static void showLuckyDrawH5(Context context) {
        CommonWebViewActivity.start(context, BaseUrl.LUCK_DRAW);
    }

    /**
     * 商城H5
     *
     * @param context
     */
    public static void showDressMallH5(Context context) {
        CommonWebViewActivity.start(context, BaseUrl.DRESS_MALL);
    }

    /**
     * 排行榜
     *
     * @param context
     */
    public static void showAllRankListH5(Context context) {
        CommonWebViewActivity.start(context, AppConstant.WEB_TITLE_TYPE_RNAKING_THEME, BaseUrl.ROOM_RANK);
    }



    /**
     * 启动应用的设置
     */
    public static void startAppSettings(Context context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        context.startActivity(intent);
    }

    public static void openContactUs(Context context) {
        CommonWebViewActivity.start(context, UriProvider.IM_SERVER_URL + "/mm/links/links.html");
    }
}
