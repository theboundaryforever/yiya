package com.yiya.mobile.utils;

import com.tongdaxing.erban.R;

public class ImageResUtils {


    public static int getLocalImg(boolean isSelect, int tabInfoId) {
        switch (tabInfoId) {
            case -2:
                return isSelect?R.drawable.ic_home_tab_follow_select:R.drawable.ic_home_tab_follow;
            case -1:
                return isSelect?R.drawable.ic_home_tab_hot_select:R.drawable.ic_home_tab_hot;
            case 2:
                return isSelect?R.drawable.ic_home_tab_nan_shen_select:R.drawable.ic_home_tab_nan_shen;
            case 3:
                return isSelect?R.drawable.ic_home_tab_nv_shen_select:R.drawable.ic_home_tab_nv_shen;
            case 5:
                return isSelect?R.drawable.ic_home_tab_play_select:R.drawable.ic_home_tab_play;
            case 6:
                return isSelect?R.drawable.ic_home_tab_radio_select:R.drawable.ic_home_tab_radio;
            case 7:
                return isSelect?R.drawable.ic_home_tab_game_select:R.drawable.ic_home_tab_game;
            case 8:
                return isSelect?R.drawable.ic_home_tab_friend_select:R.drawable.ic_home_tab_friend;
            case 9:
                return isSelect?R.drawable.ic_home_tab_new_select:R.drawable.ic_home_tab_new;
            default:
                return isSelect?R.drawable.ic_home_tab_default_select:R.drawable.ic_home_tab_default;
        }
    }
}
