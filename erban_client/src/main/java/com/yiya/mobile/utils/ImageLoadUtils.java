package com.yiya.mobile.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.yiya.mobile.utils.blur.BlurTransformation;
import com.juxiao.library_utils.SystemUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.erban.R;

import java.io.File;
import java.security.MessageDigest;

/**
 * 图片加载处理
 * Created by chenran on 2017/11/9.
 */

public class ImageLoadUtils {
    private static final String PIC_PROCESSING = "?imageslim";
    private static final String ACCESS_URL = "img.erbanyy.com";

    public static void loadAvatar(Context context, String avatar, ImageView imageView, boolean isCircle) {
        if (StringUtil.isEmpty(avatar)) {
            return;
        }

        StringBuilder sb = new StringBuilder(avatar);
        if (avatar.contains(ACCESS_URL)) {
            if (!avatar.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/100/h/100");
        }
        if (isCircle) {
            loadCircleImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
        } else {
            loadImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
        }
    }

    public static void loadMediumAvatar(Context context, String avatar, ImageView imageView, boolean isCircle) {
        if (StringUtil.isEmpty(avatar)) {
            return;
        }

        StringBuilder sb = new StringBuilder(avatar);
        if (avatar.contains(ACCESS_URL)) {
            if (!avatar.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/200/h/200");
        }
        if (isCircle) {
            loadCircleImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
        } else {
            loadImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
        }
    }

    public static void loadBigAvatar(Context context, String avatar, ImageView imageView) {
        if (StringUtil.isEmpty(avatar)) {
            return;
        }
        StringBuffer sb = new StringBuffer(avatar);
        if (avatar.contains(ACCESS_URL)) {
            if (!avatar.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/400/h/200");
        }
        loadImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
    }

    public static void loadAvatar(Context context, String avatar, ImageView imageView) {
        loadAvatar(context, avatar, imageView, false);
    }

    public static void loadSmallRoundBackground(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuilder sb = new StringBuilder(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/220/h/220");
        }
        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(
                                context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size)))
                .placeholder(R.drawable.nim_avatar_default)
                .error(R.drawable.nim_avatar_default)
                .into(imageView);
    }

    public static void loadSmallRoundBackground(Context context, String url, ImageView imageView, int radius) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuilder sb = new StringBuilder(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/220/h/220");
        }
        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(radius))
                .placeholder(R.mipmap.ic_home_list_placeholder)
                .error(R.mipmap.ic_home_list_placeholder)
                .into(imageView);
    }

    public static void loadRoomBgBackground(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuilder sb = new StringBuilder(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/720/h/1280");
        }
        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    public static void loadBannerRoundBackground(Context context, String url, ImageView imageView, int roundSize) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/660/h/220");
        }

        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(roundSize)))
                .placeholder(R.drawable.nim_avatar_default)
                .into(imageView);
    }

    public static void loadBannerRoundBackground(Context context, String url, ImageView imageView, int roundSize,
                                                 @DrawableRes int placeHolder) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/660/h/220");
        }

        GlideApp.with(context)
                .load(sb.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(roundSize)))
                .placeholder(placeHolder)
                .into(imageView);
    }

    public static void loadPhotoThumbnail(Context context, String url, ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/150/h/150");
        }

        loadImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
    }

    public static void loadLiveStreamAvatar(Context context, String url, ImageView imageView, int defaultAva) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/400/h/520");
        }

        loadImage(context, sb.toString(), imageView, defaultAva);
    }


    public static void loadImageWithBlurTransformation(Context context, String url, final ImageView imageView) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/75/h/75");
        }
        GlideApp.with(context)
                .load(sb.toString())
                .dontTransform()
                .dontAnimate()
                .override(75, 75)
                .centerInside()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .error(R.drawable.nim_avatar_default)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object o,
                                                Target<Drawable> target, boolean b) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable drawable, Object o,
                                                   Target<Drawable> target, DataSource dataSource, boolean b) {
                        imageView.setImageDrawable(drawable);
                        if (!b) {
                            imageView.setAlpha(0.F);
                            imageView.animate().alpha(1F).setDuration(500).start();
                        }
                        return true;
                    }
                })
                // “23”：设置模糊度(在0.0到25.0之间)，默认”25";"4":图片缩放比例,默认“1”。
                .transforms(new BlurTransformation(context, 10, 1))
                .into(imageView);
    }

    public static void loadImageWithBlurTransformationAndCorner(Context context, String url, ImageView imageView, int radius) {
        if (StringUtil.isEmpty(url)) {
            return;
        }
        StringBuffer sb = new StringBuffer(url);
        if (url.contains(ACCESS_URL)) {
            if (!url.contains("?")) {
                sb.append(PIC_PROCESSING);
            }
            sb.append("|imageView2/1/w/75/h/75");
        }
        GlideApp.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transforms(new CenterCrop(),
                        new BlurTransformation(context, 25, 3),
                        new RoundedCorners(context.getResources().getDimensionPixelOffset(radius))
                ).into(imageView);

    }


    public static void loadCircleImage(Context context, String url, ImageView imageView, int defaultRes) {
        GlideApp.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .centerCrop()
                .transform(new CircleCrop())
                .error(defaultRes)
                .placeholder(defaultRes)
                .into(imageView);
    }

    public static void loadImage(Context context, String url, ImageView imageView, int defaultRes) {
        GlideApp.with(context).load(url)
                .dontAnimate()
                .centerCrop()
                .placeholder(defaultRes)
                .error(defaultRes)
                .into(imageView);
    }

    public static void loadImage(Context context, File file, ImageView imageView, int defaultRes) {
        GlideApp.with(context).load(file).dontAnimate().placeholder(defaultRes).into(imageView);
    }

    public static void loadImage(Context context, @DrawableRes int id, ImageView imageView) {
        GlideApp.with(context).asGif().load(id).into(imageView);
    }

    /**
     * 加载app资源图片方法
     */
    public static void loadImageRes(Context context, @DrawableRes int id, ImageView imageView,
                                    @DrawableRes int holderRes) {
        GlideApp.with(context).load(id).placeholder(holderRes).diskCacheStrategy(DiskCacheStrategy.NONE).centerCrop().into(imageView);
    }

    /**
     * 加载app资源图片方法
     */
    public static void loadImageRes(Context context, @DrawableRes int id, ImageView imageView,
                                    @DrawableRes int holderRes,RequestOptions options) {
        GlideApp.with(context).load(id).apply(options).placeholder(holderRes).diskCacheStrategy(DiskCacheStrategy.NONE).centerCrop().into(imageView);
    }

    /**
     * 加载app资源图片方法
     */
    public static void loadImageRes(Context context, @DrawableRes int id, ImageView imageView) {
        GlideApp.with(context).load(id).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);
    }

    public static void loadImage(Context context, File file, ImageView imageView) {
        GlideApp.with(context).load(file).dontAnimate().into(imageView);
    }

    public static void loadImage(Context context, String url, ImageView imageView) {
        GlideApp.with(context).load(url).dontAnimate().into(imageView);
    }


    public static void loadImage(Context context, Bitmap bitmap, ImageView imageView) {
        GlideApp.with(context).load(bitmap).dontAnimate().into(imageView);
    }


    public static void loadCarImage(Context context, String url, ImageView imageView) {
        GlideApp.with(context).asBitmap().load(url).dontAnimate().into(imageView);
    }

    /**
     * 可以设置错误图和占位图方法
     */
    public static void loadImage(Context context, String url, ImageView imageView, int holder, int error) {
        GlideApp.with(context).load(url).placeholder(holder).error(error).into(imageView);
    }

    public static void loadImage_8888(Context context, String url, ImageView imageView, int holder, int error) {
        GlideApp.with(context).load(url).apply(RequestOptions.formatOf(DecodeFormat.PREFER_ARGB_8888))
                .placeholder(holder).error(error).into(imageView);
    }

    /**
     * 可以设置错误图和占位图方法
     */
    public static void loadImages(Context context, String url, ImageView imageView, int holder, int error) {
        GlideApp.with(context).load(url).placeholder(holder).error(error).centerCrop().into(imageView);
    }

    public static void loadImages(Context context, String url, ImageView imageView) {
        GlideApp.with(context).load(url).fitCenter().into(imageView);
    }

    /**
     * 加载本地gif图
     */
    public static void loadGifImage(Context context, @DrawableRes int id, ImageView imageView) {
        GlideApp.with(context).asGif().load(id).centerCrop().into(imageView);
    }

    public static void clearMemory(Context context) {
        if (SystemUtil.inMainThread()) {
            Glide.get(context).clearMemory();
        } else {
            new Handler(Looper.getMainLooper()).post(() -> Glide.get(context).clearMemory());
        }
    }

    public static class CornerTransform implements Transformation<Bitmap> {

        private BitmapPool mBitmapPool;

        private float radius;

        private boolean exceptLeftTop, exceptRightTop, exceptLeftBottom, exceptRightBotoom;

        /**
         * 除了那几个角不需要圆角的
         *
         * @param leftTop
         * @param rightTop
         * @param leftBottom
         * @param rightBottom
         */
        public void setExceptCorner(boolean leftTop, boolean rightTop, boolean leftBottom, boolean rightBottom) {
            this.exceptLeftTop = leftTop;
            this.exceptRightTop = rightTop;
            this.exceptLeftBottom = leftBottom;
            this.exceptRightBotoom = rightBottom;
        }

        public CornerTransform(Context context, float radius) {
            this.mBitmapPool = Glide.get(context).getBitmapPool();
            this.radius = radius;
        }

        @Override
        public Resource<Bitmap> transform(Context context, Resource<Bitmap> resource, int outWidth, int outHeight) {
            Bitmap source = resource.get();
            int finalWidth, finalHeight;
            float ratio; //输出目标的宽高或高宽比例
            if (outWidth > outHeight) { //输出宽度>输出高度,求高宽比
                ratio = (float) outHeight / (float) outWidth;
                finalWidth = source.getWidth();
                finalHeight = (int) ((float) source.getWidth() * ratio); //固定原图宽度,求最终高度
                if (finalHeight > source.getHeight()) { //求出的最终高度>原图高度,求宽高比
                    ratio = (float) outWidth / (float) outHeight;
                    finalHeight = source.getHeight();
                    finalWidth = (int) ((float) source.getHeight() * ratio);//固定原图高度,求最终宽度
                }
            } else if (outWidth < outHeight) { //输出宽度 < 输出高度,求宽高比
                ratio = (float) outWidth / (float) outHeight;
                finalHeight = source.getHeight();
                finalWidth = (int) ((float) source.getHeight() * ratio);//固定原图高度,求最终宽度
                if (finalWidth > source.getWidth()) { //求出的最终宽度 > 原图宽度,求高宽比
                    ratio = (float) outHeight / (float) outWidth;
                    finalWidth = source.getWidth();
                    finalHeight = (int) ((float) source.getWidth() * ratio);
                }
            } else { //输出宽度=输出高度
                finalHeight = source.getHeight();
                finalWidth = finalHeight;
            }

            //修正圆角
            this.radius *= (float) finalHeight / (float) outHeight;
            Bitmap outBitmap = this.mBitmapPool.get(finalWidth, finalHeight, Bitmap.Config.ARGB_8888);
            if (outBitmap == null) {
                outBitmap = Bitmap.createBitmap(finalWidth, finalHeight, Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(outBitmap);
            Paint paint = new Paint();
            //关联画笔绘制的原图bitmap
            BitmapShader shader = new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            //计算中心位置,进行偏移
            int width = (source.getWidth() - finalWidth) / 2;
            int height = (source.getHeight() - finalHeight) / 2;
            if (width != 0 || height != 0) {
                Matrix matrix = new Matrix();
                matrix.setTranslate((float) (-width), (float) (-height));
                shader.setLocalMatrix(matrix);
            }

            paint.setShader(shader);
            paint.setAntiAlias(true);
            RectF rectF = new RectF(0.0F, 0.0F, (float) canvas.getWidth(), (float) canvas.getHeight());
            canvas.drawRoundRect(rectF, this.radius, this.radius, paint); //先绘制圆角矩形

            if (exceptLeftTop) { //左上角不为圆角
                canvas.drawRect(0, 0, radius, radius, paint);
            }
            if (exceptRightTop) {//右上角不为圆角
                canvas.drawRect(canvas.getWidth() - radius, 0, radius, radius, paint);
            }

            if (exceptLeftBottom) {//左下角不为圆角
                canvas.drawRect(0, canvas.getHeight() - radius, radius, canvas.getHeight(), paint);
            }

            if (exceptRightBotoom) {//右下角不为圆角
                canvas.drawRect(canvas.getWidth() - radius, canvas.getHeight() - radius, canvas.getWidth(), canvas.getHeight(), paint);
            }

            return BitmapResource.obtain(outBitmap, this.mBitmapPool);
        }


        @Override
        public void updateDiskCacheKey(MessageDigest messageDigest) {

        }
    }
}


//public class ImageLoadUtils {
//    private static final String PIC_PROCESSING = "?imageslim";
//    private static final String ACCESS_URL = "img.erbanyy.com";
//
//    public static void loadAvatar(Context context, String avatar, ImageView imageView, boolean isCircle) {
//        if (StringUtil.isEmpty(avatar)) {
//            return;
//        }
//
//        StringBuilder sb = new StringBuilder(avatar);
//        if (avatar.contains(ACCESS_URL)) {
//            if (!avatar.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/100/h/100");
//        }
//        if (isCircle) {
//            loadCircleImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
//        } else {
//            loadImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
//        }
//    }
//
//    public static void loadBigAvatar(Context context, String avatar, ImageView imageView) {
//        if (StringUtil.isEmpty(avatar)) {
//            return;
//        }
//        StringBuffer sb = new StringBuffer(avatar);
//        if (avatar.contains(ACCESS_URL)) {
//            if (!avatar.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/400/h/200");
//        }
//        loadImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
//    }
//
//    public static void loadAvatar(Context context, String avatar, ImageView imageView) {
//        loadAvatar(context, avatar, imageView, false);
//    }
//
//    public static void loadSmallRoundBackground(Context context, String url, ImageView imageView) {
//        if (StringUtil.isEmpty(url)) {
//            return;
//        }
//        StringBuilder sb = new StringBuilder(url);
//        if (url.contains(ACCESS_URL)) {
//            if (!url.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/220/h/220");
//        }
//        GlideApp.with(context)
//                .load(sb.toString())
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .transforms(new CenterCrop(),
//                        new RoundedCorners(
//                                context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size)))
//                .placeholder(R.drawable.nim_avatar_default)
//                .error(R.drawable.nim_avatar_default)
//                .into(imageView);
//    }
//
//    public static void loadSmallRoundBackground(Context context, String url, ImageView imageView, int radius) {
//        if (StringUtil.isEmpty(url)) {
//            return;
//        }
//        StringBuilder sb = new StringBuilder(url);
//        if (url.contains(ACCESS_URL)) {
//            if (!url.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/220/h/220");
//        }
//        GlideApp.with(context)
//                .load(sb.toString())
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .transforms(new CenterCrop(),
//                        new RoundedCorners(radius))
//                .placeholder(R.drawable.ic_home_list_placeholder)
//                .error(R.drawable.ic_home_list_placeholder)
//                .into(imageView);
//    }
//
//    public static void loadRoomBgBackground(Context context, String url, ImageView imageView) {
//        if (StringUtil.isEmpty(url)) {
//            return;
//        }
//        StringBuilder sb = new StringBuilder(url);
//        if (url.contains(ACCESS_URL)) {
//            if (!url.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/720/h/1280");
//        }
//        GlideApp.with(context)
//                .load(sb.toString())
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(imageView);
//    }
//
//    public static void loadBannerRoundBackground(Context context, String url, ImageView imageView, int roundSize) {
//        if (StringUtil.isEmpty(url)) {
//            return;
//        }
//        StringBuffer sb = new StringBuffer(url);
//        if (url.contains(ACCESS_URL)) {
//            if (!url.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/660/h/220");
//        }
//
//        GlideApp.with(context)
//                .load(sb.toString())
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .transforms(new CenterCrop(),
//                        new RoundedCorners(context.getResources().getDimensionPixelOffset(roundSize)))
//                .placeholder(R.drawable.nim_avatar_default)
//                .into(imageView);
//    }
//
//    public static void loadBannerRoundBackground(Context context, String url, ImageView imageView, int roundSize,
//            @DrawableRes int placeHolder) {
//        if (StringUtil.isEmpty(url)) {
//            return;
//        }
//        StringBuffer sb = new StringBuffer(url);
//        if (url.contains(ACCESS_URL)) {
//            if (!url.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/660/h/220");
//        }
//
//        GlideApp.with(context)
//                .load(sb.toString())
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .transforms(new CenterCrop(),
//                        new RoundedCorners(context.getResources().getDimensionPixelOffset(roundSize)))
//                .placeholder(placeHolder)
//                .into(imageView);
//    }
//
//    public static void loadRoomBannerBackground(Context context, String url, ImageView imageView, int roundSize,
//                                                 @DrawableRes int placeHolder) {
//        if (StringUtil.isEmpty(url)) {
//            return;
//        }
//        StringBuffer sb = new StringBuffer(url);
//        if (url.contains(ACCESS_URL)) {
//            if (!url.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/660/h/220");
//        }
//
//        GlideApp.with(context)
//                .load(sb.toString())
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .transforms(new CenterInside())
//                .placeholder(placeHolder)
//                .into(imageView);
//    }
//
//    public static void loadPhotoThumbnail(Context context, String url, ImageView imageView) {
//        if (StringUtil.isEmpty(url)) {
//            return;
//        }
//        StringBuffer sb = new StringBuffer(url);
//        if (url.contains(ACCESS_URL)) {
//            if (!url.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/150/h/150");
//        }
//
//        loadImage(context, sb.toString(), imageView, R.drawable.nim_avatar_default);
//    }
//
//    public static void loadImageWithBlurTransformation(Context context, String url, final ImageView imageView) {
//        if (StringUtil.isEmpty(url)) {
//            return;
//        }
//        StringBuffer sb = new StringBuffer(url);
//        if (url.contains(ACCESS_URL)) {
//            if (!url.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/75/h/75");
//        }
//        GlideApp.with(context)
//                .load(sb.toString())
//                .dontTransform()
//                .dontAnimate()
//                .override(75, 75)
//                .centerInside()
//                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
//                .listener(new RequestListener<Drawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object o,
//                            Target<Drawable> target, boolean b) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Drawable drawable, Object o,
//                            Target<Drawable> target, DataSource dataSource, boolean b) {
//                        imageView.setImageDrawable(drawable);
//                        if (!b) {
//                            imageView.setAlpha(0.F);
//                            imageView.animate().alpha(1F).setDuration(500).start();
//                        }
//                        return true;
//                    }
//                })
//                // “23”：设置模糊度(在0.0到25.0之间)，默认”25";"4":图片缩放比例,默认“1”。
//                .transforms(new BlurTransformation(context, 10, 1))
//                .into(imageView);
//    }
//
//    public static void loadImageWithBlurTransformationAndCorner(Context context, String url, ImageView imageView) {
//        if (StringUtil.isEmpty(url)) {
//            return;
//        }
//        StringBuffer sb = new StringBuffer(url);
//        if (url.contains(ACCESS_URL)) {
//            if (!url.contains("?")) {
//                sb.append(PIC_PROCESSING);
//            }
//            sb.append("|imageView2/1/w/75/h/75");
//        }
//        GlideApp.with(context)
//                .load(url)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .transforms(new CenterCrop(),
//                        new BlurTransformation(context, 25, 3),
//                        new RoundedCorners(
//                                context.getResources().getDimensionPixelOffset(R.dimen.common_cover_round_size))
//                )
//                .into(imageView);
//
//    }
//
//
//    public static void loadCircleImage(Context context, String url, ImageView imageView, int defaultRes) {
//        GlideApp.with(context).load(url)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .dontAnimate()
//                .centerCrop()
//                .transform(new CircleCrop())
//                .error(defaultRes)
//                .placeholder(defaultRes)
//                .into(imageView);
//    }
//
//    public static void loadImage(Context context, String url, ImageView imageView, int defaultRes) {
//        GlideApp.with(context).load(url)
//                .dontAnimate()
//                .centerCrop()
//                .placeholder(defaultRes)
//                .error(defaultRes)
//                .into(imageView);
//    }
//
//    public static void loadImage(Context context, File file, ImageView imageView, int defaultRes) {
//        GlideApp.with(context).load(file).dontAnimate().placeholder(defaultRes).into(imageView);
//    }
//
//    public static void loadImage(Context context, @DrawableRes int id, ImageView imageView) {
//        GlideApp.with(context).asGif().load(id).into(imageView);
//    }
//
//    /**
//     * 加载app资源图片方法
//     */
//    public static void loadImageRes(Context context, @DrawableRes int id, ImageView imageView,
//            @DrawableRes int holderRes) {
//        GlideApp.with(context).load(id).placeholder(holderRes).diskCacheStrategy(DiskCacheStrategy.NONE).centerCrop().into(imageView);
//    }
//
//    /**
//     * 加载app资源图片方法
//     */
//    public static void loadImageRes(Context context, @DrawableRes int id, ImageView imageView) {
//        GlideApp.with(context).load(id).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);
//    }
//
//    public static void loadImage(Context context, File file, ImageView imageView) {
//        GlideApp.with(context).load(file).dontAnimate().into(imageView);
//    }
//
//    public static void loadImage(Context context, String url, ImageView imageView) {
//        GlideApp.with(context).load(url).dontAnimate().into(imageView);
//    }
//
//    public static void loadCarImage(Context context, String url, ImageView imageView) {
//        GlideApp.with(context).asBitmap().load(url).dontAnimate().into(imageView);
//    }
//
//    /**
//     * 可以设置错误图和占位图方法
//     */
//    public static void loadImage(Context context, String url, ImageView imageView, int holder, int error) {
//        GlideApp.with(context).load(url).placeholder(holder).error(error).into(imageView);
//    }
//
//    /**
//     * 可以设置错误图和占位图方法
//     */
//    public static void loadImages(Context context, String url, ImageView imageView, int holder, int error) {
//        GlideApp.with(context).load(url).placeholder(holder).error(error).centerCrop().into(imageView);
//    }
//
//    /**
//     * 加载本地gif图
//     */
//    public static void loadGifImage(Context context, @DrawableRes int id, ImageView imageView) {
//        GlideApp.with(context).asGif().load(id).centerCrop().into(imageView);
//    }
//
//    public static void clearMemory(Context context) {
//        if (SystemUtils.isMainThread()) {
//            Glide.get(context).clearMemory();
//        }
//    }
//}
