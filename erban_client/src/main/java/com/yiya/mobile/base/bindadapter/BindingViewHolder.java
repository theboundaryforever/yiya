package com.yiya.mobile.base.bindadapter;

import android.databinding.ViewDataBinding;
import android.view.View;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;

/**
 * Created by fwhm on 2017/3/28.
 */

public class BindingViewHolder<T extends ViewDataBinding> extends BaseViewHolder {

    private T mBinding;

    public BindingViewHolder(View view) {
        super(view);
    }

    public T getBinding() {
        return mBinding;
    }

    public void setBinding(T binding) {
        mBinding=binding;
    }

}
