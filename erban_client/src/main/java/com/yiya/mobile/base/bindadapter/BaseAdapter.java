package com.yiya.mobile.base.bindadapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;


/**
 * Created by fwhm on 2017/7/5.
 */

public abstract class BaseAdapter<T, V extends ViewDataBinding> extends BaseQuickAdapter<T, BindingViewHolder<V>> {

    public BaseAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BindingViewHolder<V> helper, T item) {
        convert(helper.getBinding(), item);
        helper.getBinding().executePendingBindings();
    }

    @Override
    protected BindingViewHolder<V> createBaseViewHolder(ViewGroup parent, int layoutResId) {
        V binding = DataBindingUtil.inflate(mLayoutInflater, layoutResId, parent, false);
        BindingViewHolder<V> holder = new BindingViewHolder<>(binding.getRoot());
        holder.setBinding(binding);
        return holder;
    }


    protected abstract void convert(V binding, T item);
}
