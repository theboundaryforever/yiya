package com.yiya.mobile.base.fragment;

import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;

/**
 * 解决AppCompatDialogFragment show方法的异常问题
 *
 * @author dell
 */
public class BaseDialogFragment extends AppCompatDialogFragment {

    //fragment是否在显示中
    protected boolean isShowing = false;

    @Override
    public void show(FragmentManager fragmentManager, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    /**
     * 在activity显示使用上面的方法会导致
     * dialog出现几次，activity需要多返回几次才能finish
     * 将fragment添加到stack中dismiss是因为mBackStackId=-1没有退栈
     *
     * @param fragmentManager
     * @param tag
     */
    public void showDialog(FragmentManager fragmentManager, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag);
        transaction.commitAllowingStateLoss();
        isShowing = true;
        //或者
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.addToBackStack(null);
//        show(transaction,tag);
    }

    public boolean isShowing() {
        return isShowing;
    }

    @Override
    public void dismiss() {
        try {
            isShowing = false;
            super.dismissAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isShowing = true;

    }

    @Override
    public void onPause() {
        super.onPause();
        isShowing = false;
    }

    @Override
    public void onDestroy() {
        isShowing = false;
        super.onDestroy();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        isShowing = false;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        isShowing = !hidden;
    }
}