package com.yiya.mobile.base.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/6.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public abstract class BaseTransparentDialogFragment extends BaseDialogFragment {

    Unbinder unbinder;

    protected abstract int getLayoutResId();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutResId(), getDialog().getWindow().findViewById(android.R.id.content), false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setWindowAnimations(getWindowAnimationStyle());
        window.setGravity(getWindowGravity());
    }

    protected Window getWindow() {
        return getDialog() != null ? getDialog().getWindow() : null;
    }

    //R.style.WindowBottomAnimationStyle
    protected int getWindowAnimationStyle() {
        return 0;
    }

    protected int getWindowGravity() {
        return Gravity.CENTER;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

}
