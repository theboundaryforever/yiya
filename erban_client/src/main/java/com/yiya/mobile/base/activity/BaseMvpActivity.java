package com.yiya.mobile.base.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.juxiao.library_ui.widget.AppToolBar;
import com.netease.nim.uikit.StatusBarUtil;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.AbstractMvpActivity;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.listener.IDisposableAddListener;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.ImeUtil;
import com.tongdaxing.xchat_framework.util.util.NetworkUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.UIUtils;
import com.tongdaxing.xchat_framework.util.util.log.MLog;
import com.umeng.analytics.MobclickAgent;
import com.yiya.mobile.ChatApplicationLike;
import com.yiya.mobile.base.callback.IDataStatus;
import com.yiya.mobile.base.fragment.BaseFragment;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.base.view.TitleBar;
import com.yiya.mobile.reciever.ConnectiveChangedReceiver;
import com.yiya.mobile.ui.common.fragment.LoadingFragment;
import com.yiya.mobile.ui.common.fragment.NetworkErrorFragment;
import com.yiya.mobile.ui.common.fragment.NoDataFragment;
import com.yiya.mobile.ui.common.fragment.ReloadFragment;
import com.yiya.mobile.ui.common.permission.EasyPermissions;
import com.yiya.mobile.ui.common.permission.PermissionActivity;
import com.yiya.mobile.ui.common.widget.StatusLayout;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.common.widget.dialog.LoginPopupDialog;
import com.yiya.mobile.ui.widget.emptyView.DefaultEmptyEnum;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * @author alvin hwang
 */
public abstract class BaseMvpActivity<V extends IMvpBaseView, P extends AbstractMvpPresenter<V>> extends AbstractMvpActivity<V, P>
        implements IDataStatus, ConnectiveChangedReceiver.ConnectiveChangedListener, EasyPermissions.PermissionCallbacks, IDisposableAddListener {

    private static final Object TAG = "BaseActivity";
    private Handler mHandler = new Handler();
    //底部登录弹窗
    private LoginPopupDialog mLoginDialog;
    private DialogManager mDialogManager;
    protected TitleBar mTitleBar;
    protected AppToolBar mToolBar;
    protected CompositeDisposable mCompositeDisposable;
    private FrameLayout frameLayout;
    private boolean isAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCompositeDisposable = new CompositeDisposable();
        if (setBgColor() > 0) {
            getWindow().setBackgroundDrawableResource(setBgColor());
        }
        if (needSteepStateBar()) {
            setStatusBar();
        }
        //全局竖屏
        ViewGroup decorView = (ViewGroup) getWindow().getDecorView();
        frameLayout = decorView.findViewById(android.R.id.content);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        CoreManager.addClient(this);
        mCompositeDisposable.add(IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(roomEvent -> {
                    if (roomEvent == null) return;
                    onReceiveChatRoomEvent(roomEvent);
                }));
        setSwipeBackEnable(false);
    }

    protected void onReceiveChatRoomEvent(RoomEvent roomEvent) {

    }

    public void addView(int i) {
        if (i == 0 && !isAdd) {
            isAdd = true;
            View view = LayoutInflater.from(this).inflate(R.layout.layout_system_bar, frameLayout, false);
            frameLayout.addView(view);
        }
    }

    public void initToolBar() {
        mToolBar = (AppToolBar) findViewById(R.id.toolbar);
        if (mToolBar != null) {
//            mToolBar.setCenterTitle(title);
//            mToolBar.setNavigationIcon(R.drawable.arrow_left);
            mToolBar.setOnBackBtnListener(v -> onLeftClickListener());
        }
    }

    public void initTitleBar() {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && needSteepStateBar()) {
                mTitleBar.setImmersive(true);
            }
            mTitleBar.setBackgroundColor(getResources().getColor(R.color.primary));
            mTitleBar.setTitleColor(getResources().getColor(R.color.text_primary));
        }
    }

    public void initTitleBar(String title) {
        mTitleBar = (TitleBar) findViewById(R.id.title_bar);
        if (mTitleBar != null) {
            mTitleBar.setTitle(title);
            mTitleBar.setImmersive(false);
            mTitleBar.setTitleColor(getResources().getColor(R.color.back_font));
            mTitleBar.setLeftImageResource(R.drawable.arrow_left);
            mTitleBar.setLeftClickListener(v -> onLeftClickListener());
        }
    }

    protected void onLeftClickListener() {
        finish();
    }

    protected boolean needSteepStateBar() {
        return true;
    }

    protected int setBgColor() {
        return 0;
    }

    /**
     * 设置沉浸式状态栏
     */
    protected void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //透明导航栏
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        // create our manager instance after the content view is set
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        // enable status bar tint
        tintManager.setStatusBarTintEnabled(true);
        // enable navigation bar tint
        tintManager.setNavigationBarTintEnabled(true);
        tintManager.setTintColor(Color.parseColor("#00000000"));
    }

    /**
     * 获取状态栏的高度
     *
     * @return
     */
    protected int getStatusBarHeight() {
        try {
            Class<?> c = Class.forName("com.android.internal.R$dimen");
            Object obj = c.newInstance();
            Field field = c.getField("status_bar_height");
            int x = Integer.parseInt(field.get(obj).toString());
            return getResources().getDimensionPixelSize(x);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initToolBar();
        initTitleBar();
    }

    /**
     * 是否需要渲染成主题色的status bar
     *
     * @return
     */
    protected boolean shouldConfigStatusBar() {
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (blackStatusBar()) {
            addView(StatusBarUtil.StatusBarLightMode(this));
        }
    }

    public boolean blackStatusBar() {
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.dispose();
            mCompositeDisposable = null;
        }
        if (mDialogManager != null) {
            mDialogManager = null;
        }
        super.onDestroy();
        LogUtil.i(this.getClass().getName(), "onDestroy");
        CoreManager.removeClient(this);
        /* ImageLoadUtils.clearMemory(this);*/

        if (BasicConfig.INSTANCE.isDebuggable()) {
            ChatApplicationLike.getRefWatcher().watch(this);
        }
    }

    public DialogManager getDialogManager() {
        if (mDialogManager == null) {
            mDialogManager = new DialogManager(this);
            mDialogManager.setCanceledOnClickOutside(false);
        }
        return mDialogManager;
    }

    public DialogManager getDialogManager(Context context) {
        if (mDialogManager == null) {
            mDialogManager = new DialogManager(context);
            mDialogManager.setCanceledOnClickOutside(false);
        }
        return mDialogManager;
    }

    public void releaseManager() {
        mDialogManager = null;
    }

    public void dismissDialog() {
        getDialogManager().dismissDialog();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Fragment fragment = getTopFragment();
        if (fragment != null && fragment instanceof BaseFragment) {
            if (((BaseFragment) fragment).onKeyDown(keyCode, event)) {
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getTopFragment();
        if (fragment != null && (fragment instanceof BaseFragment || fragment instanceof BaseMvpFragment)) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public Fragment getTopFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible()) {
                    return fragment;
                }
            }
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        hideIME();
        try {
            super.onBackPressed();
        } catch (Exception ex) {
            MLog.error(this, ex);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Fragment fragment = getTopFragment();
                if (fragment != null && fragment instanceof BaseFragment) {
                    if (fragment.onOptionsItemSelected(item)) {
                        return true;
                    }
                }
                onBackPressed();
                return true;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (featureId == Window.FEATURE_ACTION_BAR && menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {
                }
            }
        }
        return super.onMenuOpened(featureId, menu);
    }

    /**
     * 获取backstack top
     *
     * @return
     */
    private Fragment getLastFragment() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (null != fragments && fragments.size() > 0) {
            for (int i = fragments.size() - 1; i >= 0; i--) {
                Fragment f = fragments.get(i);
                if (f != null && f instanceof BaseFragment) {
                    return f;
                }
            }
        }

        return null;
    }

    /**
     * @param fragmentName Fragment.class.getName()
     */
    public void popFragment(String fragmentName) {
        getSupportFragmentManager().popBackStack(fragmentName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public String getTopFragmentName() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            return fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        }
        return null;
    }

//    public Handler getHandler() {
//        return mHandler;
//    }

    protected void updateBottomBar(boolean isShow) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        GlideApp.with(this).resumeRequests();

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        GlideApp.with(this).pauseRequests();
    }

    /**
     * 网络连接变化
     */
    /**
     * 网络连接变化
     */
    /**
     * wifi 转 2G/3G/4G
     */
    public void wifiChange2MobileData() {

    }

    /**
     * 有网络变为无网络
     */
    public void change2NoConnection() {
        if (isTopActive()) {

        }
    }

    /**
     * 连上wifi
     */
    public void connectiveWifi() {
        if (isTopActive()) {

        }
    }

    /**
     * 连上移动数据网络
     */
    public void connectiveMobileData() {
        if (isTopActive()) {

        }
    }

    /**
     * 移动数据网络 改为连上wifi
     */
    public void mobileDataChange2Wifi() {

    }

    protected boolean checkActivityValid() {
        return UIUtils.checkActivityValid(this);
    }

    /**
     * 弹出登录框
     *
     * @return
     */
    public void showLoginDialog() {
//        if (mLoginDialog == null) {
//            mLoginDialog = new LoginPopupDialog(this, new OnClickLoginPopupListener(this));
//        }
        ImeUtil.hideIME(this);
//        mLoginDialog.show();
    }

    /**
     * --------------------------------------------------
     * -------------------------数据状态状态相关-------------
     * --------------------------------------------------
     */

    private static final String STATUS_TAG = "STATUS_TAG";

    @Override
    public View.OnClickListener getLoadListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReloadDate();
            }
        };
    }

    /**
     * 网络错误重新加载数据
     */
    public void onReloadDate() {

    }

    @Override
    public View.OnClickListener getLoadMoreListener() {
        return null;
    }

    @Override
    public View.OnClickListener getNoMobileLiveDataListener() {
        return null;
    }

    @Override
    public void showLoading() {
        showLoading(0, 0);
    }

    @Override
    public void showLoading(View view) {
        showLoading(view, 0, 0);
    }

    @Override
    public void showReload() {
        showReload(0, 0);
    }

    @Override
    public void showNoData() {
        showNoData(0, "");
    }

    @Override
    public void showNoLogin() {

    }

    @Override
    public void showLoading(int drawable, int tips) {
        if (!checkActivityValid()) {
            return;
        }

        View status = findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            MLog.error(this, "xuwakao, had not set layout id ");
            return;
        }
        Fragment fragment = LoadingFragment.newInstance(drawable, tips);
        getSupportFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void showLoading(View view, int drawable, int tips) {

    }

    @Override
    public void showReload(int drawable, int tips) {
        if (!checkActivityValid()) {
            return;
        }

        View status = findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            MLog.error(this, "xuwakao, had not set layout id ");
            return;
        }
        ReloadFragment fragment = ReloadFragment.newInstance(drawable, tips);
        fragment.setListener(getLoadListener());
        getSupportFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void showReload(View view, int drawable, int tips) {

    }

    @Override
    public void showNoData(CharSequence charSequence) {
        showNoData(0, charSequence);
    }

    @Override
    public void showNoData(int drawable, CharSequence charSequence) {
        if (!checkActivityValid()) {
            return;
        }

        View status = findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            MLog.error(this, "xuwakao, had not set layout id ");
            return;
        }
        NoDataFragment fragment = NoDataFragment.newInstance(drawable, charSequence);
        fragment.setListener(getLoadListener());
        getSupportFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void showNoData(View view, int drawable, CharSequence charSequence) {

    }

    @Override
    public void showNetworkErr() {
        if (!checkActivityValid()) {
            return;
        }

        View status = findViewById(R.id.status_layout);
        if (status == null || status.getId() <= 0) {
            MLog.error(this, "xuwakao, had not set layout id ");
            return;
        }
        NetworkErrorFragment fragment = new NetworkErrorFragment();
        fragment.setListener(getLoadListener());
        getSupportFragmentManager().beginTransaction().replace(status.getId(), fragment, STATUS_TAG).commitAllowingStateLoss();
    }

    @Override
    public void hideStatus() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(STATUS_TAG);
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    @Override
    public void showPageError(int tips) {
        if (!checkActivityValid()) {
            return;
        }

        View more = findViewById(R.id.loading_more);
        if (more == null) {
            MLog.error(this, "xuwakao, showReload more is NULL");
            return;
        }
        StatusLayout statusLayout = (StatusLayout) more.getParent();
        statusLayout.showErrorPage(tips, getLoadMoreListener());
    }

    @Override
    public void showPageError(View view, int tips) {

    }

    @Override
    public void showPageLoading() {
        if (!checkActivityValid()) {
            return;
        }

        View more = findViewById(R.id.loading_more);
        if (more == null) {
            MLog.error(this, "xuwakao, showReload more is NULL");
            return;
        }
        StatusLayout statusLayout = (StatusLayout) more.getParent();
        statusLayout.showLoadMore();
    }

    /**
     * 当前网络是否可用
     *
     * @return
     */
    public boolean isNetworkAvailable() {
        return NetworkUtils.isNetworkStrictlyAvailable(this);
    }

    public boolean checkNetToast() {
        boolean flag = isNetworkAvailable();
        if (!flag) {
            SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), R.string.str_network_not_capable);
        }
        return flag;
    }

    /**
     * --------------------------------------------------
     * -------------------------UI基本功能------------------
     * --------------------------------------------------
     */
    public void hideIME() {
        View v = getCurrentFocus();
        if (null != v)
            hideIME(v);
    }

    public void hideIME(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    //todo 重构2019/3/18
//    public void showIME(final View vv) {
//        View v = vv;
//        if (null == v) {
//            v = getCurrentFocus();
//            if (null == v)
//                return;
//        }
//        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
//                .showSoftInput(v, InputMethodManager.SHOW_FORCED);
//    }

    /**
     * 当前无Fragment 或 无Fragment添加 视为顶部激活状态
     *
     * @return
     */
    public boolean isTopActive() {
        if (isTopActivity()) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            List<Fragment> fragments = fragmentManager.getFragments();
            if (fragments == null || fragments.size() == 0) {
                return true;
            } else {
                int size = fragments.size();
                for (int i = 0; i < size; i++) {
                    if (fragments.get(i) != null && fragments.get(i).isAdded())
                        return false;
                }
                return true;
            }
        }
        return false;
    }

    public boolean isTopActivity() {
        return UIUtils.isTopActivity(this);
    }

    /**
     * 通用消息提示
     *
     * @param resId
     */
    public void toast(int resId) {
        toast(resId, Toast.LENGTH_SHORT);
    }

    public void toast(String toast) {
        toast(toast, Toast.LENGTH_SHORT);
    }

    /**
     * 通用消息提示
     *
     * @param resId
     * @param length
     */
    public void toast(int resId, int length) {
        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), resId, length);
    }

    public void toast(String toast, int length) {
        SingleToastUtil.showToast(BasicConfig.INSTANCE.getAppContext(), toast, length);
    }


    private void configStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.primary));

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // create our manager instance after the content view is set
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            // enable status bar tint
            tintManager.setStatusBarTintEnabled(true);
            // enable navigation bar tint
            tintManager.setNavigationBarTintEnabled(true);

            tintManager.setTintResource(R.color.primary);
        }
    }


    /**
     * 权限回调接口
     */
    private PermissionActivity.CheckPermListener mListener;

    protected static final int RC_PERM = 123;

    protected static int reSting = R.string.ask_again;//默认提示语句

    public interface CheckPermListener {
        //权限通过后的回调方法
        void superPermission();
    }

    public void checkPermission(PermissionActivity.CheckPermListener listener, int resString, String... mPerms) {
        mListener = listener;
        if (EasyPermissions.hasPermissions(this, mPerms)) {
            if (mListener != null)
                mListener.superPermission();
        } else {
            EasyPermissions.requestPermissions(this, getString(resString), RC_PERM, mPerms);
        }
    }

    /**
     * 用户权限处理,
     * 如果全部获取, 则直接过.
     * 如果权限缺失, 则提示Dialog.
     *
     * @param requestCode  请求码
     * @param permissions  权限
     * @param grantResults 结果
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == EasyPermissions.SETTINGS_REQ_CODE) {
//            //设置返回
//        }
//    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        //同意了某些权限可能不是全部
    }

    @Override
    public void onPermissionsAllGranted() {
        if (mListener != null)
            mListener.superPermission();//同意了全部权限的回调
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                getString(R.string.perm_tip),
                R.string.setting, R.string.cancel, null, perms);
    }

    /**
     * 当前Activity 是否有效
     */
    protected boolean isValid() {
        return !isFinishing() && !isDestroyed();
    }

    @Override
    public void addDisposable(Disposable disposable) {
        if (mCompositeDisposable != null && disposable != null)
            mCompositeDisposable.add(disposable);
    }


    public void onActivityFinish(View view) {
        finish();
    }

    public View getEmptyView(ViewGroup viewGroup, DefaultEmptyEnum emptyEnum) {
        View inflate = LayoutInflater.from(this).inflate(R.layout.layout_empty_view, viewGroup, false);
        TextView view = inflate.findViewById(R.id.no_data_text);
        ImageView img = inflate.findViewById(R.id.no_data_icon);
        view.setText(emptyEnum.getTitle());
        img.setImageResource(emptyEnum.getResId());
        return inflate;
    }

}