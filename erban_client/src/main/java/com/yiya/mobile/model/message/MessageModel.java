package com.yiya.mobile.model.message;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.msg.BlackListInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.Map;

/**
 * @author Zhangsongzhou
 * @date 2019/5/20
 */
public class MessageModel extends BaseMvpModel {

    /**
     * 检查互黑关系
     *
     * @param tgUid
     * @param callBack
     */
    public void checkUserBlacklist(String tgUid, HttpRequestCallBack<Json> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("tgUid", tgUid);
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getCheckBlacklist(), params, callBack);
    }

    /**
     * 加入私聊黑名单
     *
     * @param tgUid
     * @param callBack
     */
    public void addUserBlacklist(String tgUid, HttpRequestCallBack<Json> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("tgUid", tgUid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.addUserBlackList(), params, callBack);
    }

    /**
     * @param tgUid
     * @param value    "synRoomBlackList", value = "同步到房间黑名单,传“1”同步",
     * @param callBack
     */
    public void addUserBlacklist(String tgUid, int value, HttpRequestCallBack<Json> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("tgUid", tgUid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("synRoomBlackList", value + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.addUserBlackList(), params, callBack);
    }


    /**
     * 移除私聊黑名单
     *
     * @param tgUid
     * @param callBack
     */
    public void removeUserBlacklist(String tgUid, HttpRequestCallBack<Json> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("tgUid", tgUid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.removeUserBlacklist(), params, callBack);
    }

    /**
     * 获取私聊黑名单列表
     *
     * @param pageNum
     * @param pageSize
     * @param callBack
     */
    public void getUserBlacklist(int pageNum, int pageSize, HttpRequestCallBack<BlackListInfo> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("pageNum", pageNum + "");
        params.put("pageSize", pageSize + "");

        OkHttpManager.getInstance().getRequest(UriProvider.getUserBlacklist(), params, callBack);
    }

    /**
     * 检查用户行为
     *
     * @param uid
     * @param ticket
     * @param callBack
     */
    public void checkMessageImagePermission(String uid, String ticket, HttpRequestCallBack<Json> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", ticket);
        params.put("uid", uid);
        OkHttpManager.getInstance().doPostRequest(UriProvider.checkUserOperation(), params, callBack);

    }

    public void checkUserOperation(String tgUid, String content, String operation,  OkHttpManager.MyCallBack<Json> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("operation", operation);
        params.put("content", content);
        params.put("tgUid", tgUid + "");

        OkHttpManager.getInstance().doPostRequest(UriProvider.checkUserOperation(), params, callBack);
    }



}
