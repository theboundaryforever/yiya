package com.yiya.mobile.model.home;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

public class HomeModel extends BaseMvpModel {


    /**
     * 获取首页分类列表数据
     */
    public void getHomeTabMenuList(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getMainDataByMenu(), params, myCallBack);
    }

    public void getRecommendTagList(int roomType, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("roomType", roomType + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getMainDataByMenu(), params, myCallBack);
    }


    /**
     * 获取首页精选房间列表
     */
    public void getHomeHotFeatrueList(int mPage, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageNum", mPage + "");
        params.put("pageSize", Constants.BILL_PAGE_SIZE + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getHomePopularRoom(), params, myCallBack);
    }

    /**
     * 语聊房获取排行榜前三
     *
     * @param myCallBack
     */
    public void getYuChatRankList(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getRank(), params, myCallBack);

    }


    /**
     * 获取首页热门房间列表
     */
    public void getHomeHotRoomList(int mPage, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("pageNum", mPage + "");
        params.put("pageSize", Constants.PAGE_HOME_HOT_SIZE + "");
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getMainHotData(), params, myCallBack);
    }


    /**
     * 根据分类id获取不同的房间列表
     */
    public void getHomeRoomListByTabId(int tagId, int pageNum, int pageSize, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("tagId", String.valueOf(tagId));
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        OkHttpManager.getInstance().getRequest(UriProvider.getMainDataByTab(), params, myCallBack);
    }

    /**
     * 根据分类id获取不同的房间列表
     */
    public void getHomeRoomListByTabId(int tagId, int pageNum, int pageSize, HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("tagId", String.valueOf(tagId));
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        getRequest(UriProvider.getMainDataByTab(), params, callBack);
    }

    /**
     * 根据首页广告
     */
    public void getHomeRoomBanner(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getHomeHeadBanner(), params, myCallBack);
    }


    /**
     * 获取app内部配置 这里用于萌币中心入口开关判断
     */
    public void getAppConfig(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        OkHttpManager.getInstance().getRequest(UriProvider.getConfigUrl(), params, myCallBack);
    }

    /**
     * 获取首页视频房间列表
     */
    public void getHomeVideoList(int roomType, int pageNum, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();

        params.put("pageNum", String.valueOf(pageNum));
        params.put("roomType", String.valueOf(roomType));
        params.put("pageSize", String.valueOf(Constants.PAGE_HOME_HOT_SIZE));

        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));

        getRequest(UriProvider.getHomeList(), params, callBack);
    }

    /**
     * 通过tag获取首页数据
     */
    public void getTagVideoRoom(final int tagId, final int pageNum, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("tagId", String.valueOf(tagId));
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(Constants.PAGE_SIZE));
        OkHttpManager.getInstance().getRequest(UriProvider.getTagVideoRoom(), params, callBack);
    }

    /***获取首页数据*/
    public void getHomeTalkRoomList(String roomType,int pageNum, int pageSize, OkHttpManager.MyCallBack myCallBack){
        Map<String, String> params = getDefaultParams();
        params.put("roomType",roomType);
        params.put("appid","123");
        params.put("pageNum",String.valueOf(pageNum));
        params.put("pageSize",String.valueOf(pageSize));
        OkHttpManager.getInstance().getRequest(UriProvider.getHomeTabRoom(),params,myCallBack);
    }
}
