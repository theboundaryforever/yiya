package com.yiya.mobile.model.pigfight;

import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

public class PigFightModel extends BaseMvpModel {

    /**
     * 开始猪猪大作战
     *
     * @param callBack 回调
     */
    public void startPlay(HttpRequestCallBack callBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        IAuthCore core = CoreManager.getCore(IAuthCore.class);
        requestParam.put("uid", core.getCurrentUid() + "");
        requestParam.put("ticket", core.getTicket());
        requestParam.put("roomId", AvRoomDataManager.get().getRoomInfo() != null ? (AvRoomDataManager.get().getRoomInfo().getRoomId() + "") : "");
        postRequest(UriProvider.startPigFight(), requestParam, callBack);
    }

    /**
     * 获取大作战规则
     *
     * @param callBack 回调
     */
    public void getRuleList(HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        getRequest(UriProvider.getPigFightRuleList(), param, callBack);
    }

    /**
     * 开启、关闭猪猪大作战
     *
     * @param type     1、开启 0、 关闭
     * @param callBack 回调
     */
    public void openOrClosePigFight(int type, HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("type", type + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("roomId", AvRoomDataManager.get().getRoomInfo() != null ? (AvRoomDataManager.get().getRoomInfo().getRoomId() + "") : "");
        getRequest(UriProvider.getPigFightRuleList(), param, callBack);
    }

    /**
     * 获取猪猪大作战最近8天记录
     *
     * @param callBack
     */
    public void getPigFightRecordList(HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("roomId", AvRoomDataManager.get().getRoomInfo() != null ? (AvRoomDataManager.get().getRoomInfo().getRoomId() + "") : "");
        getRequest(UriProvider.getPigFightRecordList(), params, callBack);
    }
}