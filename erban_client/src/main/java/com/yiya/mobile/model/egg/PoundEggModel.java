package com.yiya.mobile.model.egg;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;

/**
 * 文件描述：砸蛋数据请求模型
 *
 * @auther：zwk
 * @data：2019/3/7
 */

public class PoundEggModel extends BaseMvpModel {
    /**
     * 砸蛋 ： 一次 和 十次
     * 砸蛋次数类别 1为1次、2为10次、3为自动砸（循环1次1次的砸）、4为50连砸
     *
     * @param type
     * @param jsonMyCallBack
     */
    public void poundEgg(int type, OkHttpManager.MyCallBack<ServiceResult<List<EggGiftInfo>>> jsonMyCallBack) {
        int numbersType = type == 3 ? 1 : type;//自动砸蛋：1次1次的砸
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        IAuthCore core = CoreManager.getCore(IAuthCore.class);
        requestParam.put("uid", core.getCurrentUid() + "");
        requestParam.put("type", String.valueOf(numbersType));
        requestParam.put("ticket", core.getTicket());
        requestParam.put("roomId", AvRoomDataManager.get().getRoomInfo() != null ? (AvRoomDataManager.get().getRoomInfo().getRoomId() + "") : "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.poundEgg(), requestParam, jsonMyCallBack);
    }

    /**
     * 免费砸蛋
     *
     * @param myCallBack 回调
     */
    public void poundEggFree(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        IAuthCore core = CoreManager.getCore(IAuthCore.class);
        requestParam.put("uid", core.getCurrentUid() + "");
        requestParam.put("ticket", core.getTicket());
        requestParam.put("roomId", AvRoomDataManager.get().getRoomInfo() != null ? (AvRoomDataManager.get().getRoomInfo().getRoomId() + "") : "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.poundEggFree(), requestParam, myCallBack);
    }

    /**
     * 免费砸蛋次数
     *
     * @param myCallBack 回调
     */
    public void getPoundEggFreeTimes(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.getPoundEggFreeTimes(), requestParam, myCallBack);
    }

    /**
     * 获取砸蛋礼物列表
     *
     * @param myCallBack 回调
     */
    public void getPoundEggGifts(HttpRequestCallBack myCallBack) {
        Map<String, String> requestParam = CommonParamUtil.getDefaultParam();
        requestParam.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        requestParam.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getPoundEggRewords(), requestParam, myCallBack);
    }

    /**
     * 获取种豆排行榜列表
     *
     * @param roomId 房间Id
     * @param type 类型
     * @param callBack 回调
     */
    public void getRankList(long roomId, int type, HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("roomId", roomId + "");
        param.put("type", type + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        getRequest(UriProvider.getPoundEggRank(), param, callBack);
    }

    /**
     * 获取种豆规则说明
     *
     * @param callBack 回调
     */
    public void getRuleList(HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        getRequest(UriProvider.getPlantBeanRuleList(), param, callBack);
    }

    /**
     * 获取砸蛋配置
     *
     * @param callBack 回调
     */
    public void getPoundCfg(HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        getRequest(UriProvider.getPoundCfg(), param, callBack);
    }
}
