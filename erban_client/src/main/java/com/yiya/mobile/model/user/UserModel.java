package com.yiya.mobile.model.user;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * 用户相关的数据模型
 * @author zwk
 */

public class UserModel extends BaseMvpModel{


    /**
     * 判断用户是否绑定手机号
     * @param myCallBack
     */
    public void isBindPhone(OkHttpManager.MyCallBack myCallBack){
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        OkHttpManager.getInstance().getRequest(UriProvider.isPhones(), params,myCallBack);
    }
    /**
     * 绑定微信支付账号
     * @param uid
     * @param myCallBack
     */
    public void bindWithdrawWeixin(long uid,String ticket,String accessToken,String openId,String unionId, OkHttpManager.MyCallBack myCallBack){
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", ticket);
        params.put("accessToken", accessToken);
        params.put("openId", openId);
        params.put("unionId", unionId);
        params.put("type","1");
        OkHttpManager.getInstance().doPostRequest(UriProvider.bindWeixinWithdraw(), params, myCallBack);
    }


    /**
     * 获取邀请分成相关信息
     * @param myCallBack
     */
    public void getInviteInfo(OkHttpManager.MyCallBack myCallBack){
        Map<String,String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        OkHttpManager.getInstance().getRequest(UriProvider.getRedPacket(), param, myCallBack);
    }


    /**
     * 更新用户邀请码
     * @param inviteCode
     * @param myCallBack
     */
    public void userUpdateInviteCode(String inviteCode, OkHttpManager.MyCallBack myCallBack){
        Map<String,String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        param.put("shareCode", inviteCode);
        OkHttpManager.getInstance().doPostRequest(UriProvider.updateUserInfo(), param, myCallBack);
    }

    /**
     * 获取邀请码
     * @param uid
     * @param myCallBack
     */
    public void getInviteCode(long uid,String ticket,OkHttpManager.MyCallBack myCallBack){
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", ticket);
        OkHttpManager.getInstance().getRequest(UriProvider.getSms(), params, myCallBack);
    }
    /**
     * 获取邀请码
     * @param uid
     * @param myCallBack
     */
    public void getCheckCode(long uid,String ticket,String code,OkHttpManager.MyCallBack myCallBack){
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(uid));
        params.put("ticket", ticket);
        params.put("code",code);
        OkHttpManager.getInstance().doPostRequest(UriProvider.checkCode(), params, myCallBack);
    }
}

//public class UserModel extends BaseMvpModel{
//
//
//    /**
//     * 判断用户是否绑定手机号
//     * @param myCallBack
//     */
//    public void isBindPhone(OkHttpManager.MyCallBack myCallBack){
//        Map<String, String> params = CommonParamUtil.getDefaultParam();
//        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
//        params.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
//        OkHttpManager.getInstance().getRequest(UriProvider.isPhones(), params,myCallBack);
//    }
//
//
//    /**
//     * 获取邀请分成相关信息
//     * @param myCallBack
//     */
//    public void getInviteInfo(OkHttpManager.MyCallBack myCallBack){
//        Map<String,String> param = CommonParamUtil.getDefaultParam();
//        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
//        param.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
//        OkHttpManager.getInstance().getRequest(UriProvider.getRedPacket(), param, myCallBack);
//    }
//
//
//    /**
//     * 更新用户邀请码
//     * @param inviteCode
//     * @param myCallBack
//     */
//    public void userUpdateInviteCode(String inviteCode, OkHttpManager.MyCallBack myCallBack){
//        Map<String,String> param = CommonParamUtil.getDefaultParam();
//        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
//        param.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
//        param.put("shareCode", inviteCode);
//        OkHttpManager.getInstance().doPostRequest(UriProvider.updateUserInfo(), param, myCallBack);
//    }
//
//
//    /**
//     * 获取邀请码
//     * @param uid
//     * @param myCallBack
//     */
//    public void getInviteCode(long uid,String ticket,OkHttpManager.MyCallBack myCallBack){
//        Map<String, String> params = CommonParamUtil.getDefaultParam();
//        params.put("uid", String.valueOf(uid));
//        params.put("ticket", ticket);
//        OkHttpManager.getInstance().getRequest(UriProvider.getSms(), params, myCallBack);
//    }
//
//    /**
//     * 获取邀请码
//     * @param uid
//     * @param myCallBack
//     */
//    public void getCheckCode(long uid,String ticket,String code,OkHttpManager.MyCallBack myCallBack){
//        Map<String, String> params = CommonParamUtil.getDefaultParam();
//        params.put("uid", String.valueOf(uid));
//        params.put("ticket", ticket);
//        params.put("code",code);
//        OkHttpManager.getInstance().doPostRequest(UriProvider.checkCode(), params, myCallBack);
//    }
//
//    /**
//     * 绑定微信支付账号
//     * @param uid
//     * @param myCallBack
//     */
//    public void bindWithdrawWeixin(long uid,String ticket,String accessToken,String openId,String unionId, OkHttpManager.MyCallBack myCallBack){
//        Map<String, String> params = CommonParamUtil.getDefaultParam();
//        params.put("uid", String.valueOf(uid));
//        params.put("ticket", ticket);
//        params.put("accessToken", accessToken);
//        params.put("openId", openId);
//        params.put("unionId", unionId);
//        params.put("type","1");
//        OkHttpManager.getInstance().doPostRequest(UriProvider.bindWeixinWithdraw(), params, myCallBack);
//    }
//}
