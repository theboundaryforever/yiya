package com.yiya.mobile.model.attention;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * 关注相关功能model
 */
public class AttentionModel extends BaseMvpModel {

    /**
     * 关注房间
     *
     * @param uid
     * @param roomId
     * @param myCallBack
     */
    public void roomAttention(String uid, String roomId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid);
        params.put("roomId", roomId);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.roomAttention(), params, myCallBack);
    }

    /**
     * 检查是否关注房间
     *
     * @param uid
     * @param roomId
     * @param myCallBack
     */
    public void checkAttention(String uid, String roomId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid);
        params.put("roomId", roomId);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.checkAttention(), params, myCallBack);
    }

    /**
     * 取消房间关注
     *
     * @param uid
     * @param roomId
     * @param myCallBack
     */
    public void deleteAttention(String uid, String roomId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid);
        params.put("roomId", roomId);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.deleteAttention(), params, myCallBack);
    }

    /**
     * 获取关注房间列表
     *
     * @param uid
     * @param pageNum
     * @param pageSize
     * @param myCallBack
     */
    public void getRoomAttentionByUid(String uid, int pageNum, int pageSize, HttpRequestCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid);
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getRoomAttentionById(), params, myCallBack);
    }

    /**
     * 获取关注房间列表
     *
     * @param uid
     * @param pageNum
     * @param pageSize
     * @param myCallBack
     */
    public void getRoomAttentionRecommendList(String uid, int pageNum, int pageSize, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid);
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getAttentionRecommendList(), params, myCallBack);
    }

    public void getVideoRoomList(String uid, int pageNum, int pageSize, OkHttpManager.MyCallBack myCallBack) {

        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", uid);
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(pageSize));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getHomeVideoRoom(), params, myCallBack);
    }

    /***获取首页数据*/
    public void getHomeTalkRoomList(String tagid ,int pageNum, int pageSize, OkHttpManager.MyCallBack myCallBack){
        Map<String, String> params = getDefaultParams();
        params.put("tagId",tagid);
        params.put("appid","123");
        params.put("pageNum",String.valueOf(pageNum));
        params.put("pageSize",String.valueOf(pageSize));
        OkHttpManager.getInstance().getRequest(UriProvider.getHomeTalkRoom(),params,myCallBack);
    }
}
