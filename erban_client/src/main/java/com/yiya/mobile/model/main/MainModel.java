package com.yiya.mobile.model.main;

import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;

import java.util.Map;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/29.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MainModel extends BaseMvpModel {

    /**
     * 获取首页七天签到任务列表
     *
     * @param callBack
     */
    public void getSignInMissions(HttpRequestCallBack callBack) {
        getRequest(UriProvider.getSignInMissions(), getDefaultParams(), callBack);
    }

    /**
     * 签到领取奖励
     *
     * @param callBack
     */
    public void postSignInMission(HttpRequestCallBack callBack) {
        postRequest(UriProvider.postSignInMission(), getDefaultParams(), callBack);
    }

    /**
     * 获取首页种瓜弹窗
     *
     * @param callBack
     */
    public void getMelonPopup(HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        getRequest(UriProvider.getMelonPopup(), params, callBack);
    }

}
