package com.yiya.mobile.model.specialpay;

import android.content.Context;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_HC;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_HJ;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_PPP;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_BANK;

/**
 * @author zdw
 * @date 2019/9/2
 *
 * @describe
 */

public class SpecialDiscountModel extends BaseMvpModel {

    /**
     * 发起充值
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_ALIPAY} 和
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_WX}
     */
    public void requestCharge(Context context, String chargeProdId, String payChannel, int payType, long roomId, int giftId, OkHttpManager.MyCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("roomId", String.valueOf(roomId));
        param.put("giftId", String.valueOf(giftId));
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        String url = null;
        switch (payType){
            case CHARGE_TYPE_PPP:
                url = UriProvider.requestCharge();
                break;
            case CHARGE_TYPE_HC:
                url = UriProvider.requestChargeHC();
                break;
            case CHARGE_TYPE_HJ:
                url = UriProvider.requestChargeHJ();
                break;
            case CHARGE_TYPE_BANK:
                param.put("successUrl", "http://beta.lyy18.cn/front/formField/success.html");
                url = UriProvider.requestChargeSD();
                break;
        }

        OkHttpManager.getInstance().doPostRequest(url, param, callBack);
    }

    /**
     * 获取要赠送的特惠礼物
     *
     * @param callBack 回调
     */
    public void getPreferentialGiftList(long roomId, HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("roomId", String.valueOf(roomId));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        getRequest(UriProvider.getPreferentialGiftList(), param, callBack);
    }

    /**
     * 减少特惠礼物
     */
    public void reducePreferentialGift(long roomId, int giftId, HttpRequestCallBack callBack){
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        param.put("roomId", String.valueOf(roomId));
        param.put("giftId", String.valueOf(giftId));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        postRequest(UriProvider.reducePreferentialGift(), param, callBack);
    }
}
