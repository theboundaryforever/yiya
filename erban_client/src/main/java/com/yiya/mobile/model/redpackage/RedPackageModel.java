package com.yiya.mobile.model.redpackage;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/1/7
 */
public class RedPackageModel extends BaseMvpModel {


    /**
     * 当前用户发送红包接口
     * @param roomid
     * @param amount
     * @param num
     * @param myCallBack
     */
    public void sendRedPakcageFromMyself(String roomid ,String amount,String num, int redpackType,int noticeAll, OkHttpManager.MyCallBack myCallBack){
         Map<String,String> params = CommonParamUtil.getDefaultParam();
         params.put("uid",CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
         params.put("ticket",CoreManager.getCore(IAuthCore.class).getTicket());
         params.put("amount",amount);
         params.put("num",num);
         params.put("roomid",roomid);
         params.put("redpackType", String.valueOf(redpackType));
         params.put("noticeAll", String.valueOf(noticeAll));

        OkHttpManager.getInstance().doPostRequest(UriProvider.sendRedPackage(),params,myCallBack);
    }


    /**
     * 当前用户领取某个房间的对应id的红包接口
     * @param roomid
     * @param redPackId
     * @param myCallBack
     */
    public void receiveRoomRedPakcageToMyself(long roomid ,long redPackId ,OkHttpManager.MyCallBack myCallBack){
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("uid",CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        params.put("ticket",CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomid",roomid+"");
        params.put("redPackId",redPackId+"");
        OkHttpManager.getInstance().doPostRequest(UriProvider.receiveRedPackage(),params,myCallBack);
    }

    /**
     * 获取对应房间未领取完的红包发送记录
     * @param roomid
     * @param myCallBack
     */
    public void getRoomSendRedPackageRecord(String roomid ,OkHttpManager.MyCallBack myCallBack){
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("uid",CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        params.put("ticket",CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomid",roomid);
        OkHttpManager.getInstance().getRequest(UriProvider.getSendRedPackageRecord(),params,myCallBack);
    }

    /**
     * 获取对应房间的对应红包的历史领取记录
     * @param roomid
     * @param redPackId
     * @param myCallBack
     */
    public void receiveRoomRedPakcageHistoryRecord(long roomid ,long redPackId ,OkHttpManager.MyCallBack myCallBack){
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("uid",CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        params.put("ticket",CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomid",roomid+"");
        params.put("redPackId",redPackId+"");
        OkHttpManager.getInstance().getRequest(UriProvider.receiveRedPackageHistoryRecord(),params,myCallBack);
    }
}
