package com.yiya.mobile.model.log;


import com.juxiao.library_utils.log.LogHelper;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.manager.AgoraRtcEngine;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.utils.ThreadUtil;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

/**
 * @author Zhangsongzhou
 * @date 2019/5/30
 */
public class LogModel extends BaseMvpModel {

    private UploadFileRunnable mRunnable;
    private CallBack<Boolean> callBack;

    public LogModel() {
        CoreManager.addClient(this);
    }

    public void uploadLog(@Nullable CallBack<Boolean> callBack, int day) {
        mRunnable = new UploadFileRunnable(day);
        ThreadUtil.runInThread(mRunnable);
        this.callBack = callBack;
    }


    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("url", url);
        params.put("sn", "");//此值没实际意义，sn在header中、为了兼容后端传值校验传“”
        postRequest(UriProvider.uploadLogFile(), params, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
                onUploadFinish(true);
            }

            @Override
            public void onFailure(int code, String msg) {
                onUploadFinish(false);
            }
        });
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        onUploadFinish(false);
    }

    private void onUploadFinish(boolean isSuccess) {
        finishUpload();
        if (callBack != null) {
            callBack.onSuccess(isSuccess);
        }
    }

    public void finishUpload() {
        if (mRunnable != null) {
            ThreadUtil.cancelThread(mRunnable);
        }
        CoreManager.removeClient(this);
    }

    private class UploadFileRunnable implements Runnable {

        private int mDay = -1;

        public UploadFileRunnable(int day) {
            mDay = day;
        }

        @Override
        public void run() {
            List<String> tmpList = new ArrayList<>();
            switch (mDay) {
                case 1:
                    tmpList.addAll(LogHelper.getCurrentDayLogFilePath());
                    break;
                case 2:
                    tmpList.addAll(LogHelper.getLastDayLogFilePath());
                    break;
                case 3:
                    tmpList.addAll(LogHelper.getCurrentDayLogFilePath());
                    tmpList.addAll(LogHelper.getLastDayLogFilePath());
                    break;
                default:
                    tmpList.addAll(LogHelper.getDefaultLogFilePath());
                    break;
            }

            tmpList.add(AgoraRtcEngine.get().getLogFilePath());

            File tmpFile = LogHelper.getInstance().getLogFile(false, tmpList);

            if (tmpFile != null && tmpFile.exists()) {
                CoreManager.getCore(IFileCore.class).upload(tmpFile.getAbsoluteFile());
            } else {
                onUploadFinish(false);
            }
        }
    }
}
