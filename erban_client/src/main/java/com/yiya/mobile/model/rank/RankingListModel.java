package com.yiya.mobile.model.rank;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.UserLevelInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * 排行榜 model
 *
 * @author dell
 */
public class RankingListModel extends BaseMvpModel {

    /**
     * 排行榜
     *
     * @param type     排行榜类型 0巨星榜，1贵族榜，2房间榜
     * @param dateType 榜单周期类型 0日榜，1周榜，2总榜
     */
    public void getRankingList(String queryUid, int type, int dateType, HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("dateType", dateType + "");
        params.put("type", type + "");
        params.put("queryUid", queryUid);
        getRequest(UriProvider.getRankingXCList(), params, callBack);
    }

    public void getRankingList(String queryUid, int dateType, HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("dateType", dateType + "");
        params.put("queryUid", queryUid);
        getRequest(UriProvider.getRankingXCList(), params, callBack);
    }

    /**
     * 获取用户等级或魅力
     */
    public void getUserLevel(String url, OkHttpManager.MyCallBack<ServiceResult<UserLevelInfo>> callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        getRequest(url, param, callBack);
    }

    /**
     * 获取房间日排行榜榜首头像
     *
     * @param myCallBack
     */
    public void getRoomDayRankFirst(String roomUid, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", String.valueOf(CoreManager.getCore(IAuthCore.class).getTicket()));
        params.put("roomUid", roomUid);
        OkHttpManager.getInstance().getRequest(UriProvider.getRoomRankFirstAvatar(), params, myCallBack);
    }

    /**
     * @param dateType 类型：1，日；2，周；3，总榜 dateType     类型：1，日；2，周；3，总榜
     * @param callBack
     */
    public void getXCRankingList(String queryUid, int type, int dateType, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("dateType", dateType + "");
        params.put("type", type + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("queryUid", queryUid);
        OkHttpManager.getInstance().getRequest(UriProvider.getRankingXCList(), params, callBack);
    }

    public void getXCRankingList(String uid, String queryUid, int type, int dateType, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("dateType", dateType + "");
//        params.put("type", type + "");

        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo != null){
            params.put("roomType", roomInfo.getType()+"");
        }

        params.put("uid", uid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("queryUid", queryUid);
        OkHttpManager.getInstance().getRequest(UriProvider.getRankingXCList(), params, callBack);
    }


}
