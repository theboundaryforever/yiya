package com.yiya.mobile.ui.newfind.fragment;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.user.bean.NewUserBean;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.ui.newfind.adapter.SproutingNewRecyclerAdapter;
import com.yiya.mobile.ui.newfind.presenter.SproutingNewPresenter;
import com.yiya.mobile.ui.newfind.view.SproutingNewView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.bean.HomeLiveInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.yiya.mobile.ui.newfind.fragment.SproutingNewFragment.SproutingNewinfo.ITEM_TYPE_2;

@CreatePresenter(SproutingNewPresenter.class)
public class SproutingNewFragment extends BaseMvpFragment<SproutingNewView, SproutingNewPresenter> implements SproutingNewView , OnRefreshListener, OnLoadMoreListener {

    @BindView(R.id.sproutingnew_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.smart_refresh_layout)
    SmartRefreshLayout mSmartRefreshLayout;

    private SproutingNewRecyclerAdapter sproutingNewRecyclerAdapter;
    private GridLayoutManager mLayoutManager;
    private int mPageSize = 20;
    private int mPageNum = 1;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_sproutingnew;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        mLayoutManager = new GridLayoutManager(mContext, 1);
        sproutingNewRecyclerAdapter = new SproutingNewRecyclerAdapter(null,mContext);
        sproutingNewRecyclerAdapter.setSpanSizeLookup(new com.chad.library.adapter.base.BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                int value = 1;
                return value;
            }
        });
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(sproutingNewRecyclerAdapter);

        mSmartRefreshLayout.setOnRefreshListener(this::onRefresh);
        mSmartRefreshLayout.setOnLoadMoreListener(this::onLoadMore);

    }

    @Override
    public void onResume() {
        super.onResume();
        getData(mPageNum = 1);
        if(sproutingNewRecyclerAdapter!=null){
            if(sproutingNewRecyclerAdapter.getData()==null || sproutingNewRecyclerAdapter.getData().size()==0){
                getData(mPageNum = 1);
            }
        }
    }

    private void getData(int pageNum){
        mPageNum = pageNum;
        getMvpPresenter().getmengxinlist(pageNum,mPageSize);
    }

    @Override
    public void onLoadMore(RefreshLayout refreshLayout) { //上啦
        getData(mPageNum+1);
    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {  //下拉
        mPageNum = 1;
        getData(mPageNum);
    }

    @Override
    public void onSucceed(int pageNum, List<SproutingNewinfo> data) {
        if(pageNum == 1){
            sproutingNewRecyclerAdapter.setData(data);
        }else{
            sproutingNewRecyclerAdapter.notifyItemRangeChanged(sproutingNewRecyclerAdapter.getData().size(),data.size());
        }
        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
    }

    @Override
    public void onFailed(String message) {
        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
    }

    public static class SproutingNewinfo implements MultiItemEntity {

        public static final int ITEM_TYPE_1 = 1;  //第一列轮播广告列表
        public static final int ITEM_TYPE_2 = 2;  //第二列萌新人员列表
        private int itemType;
        private int roomIndex = 0;

        private List<BannerInfo> mList1;
        private NewUserBean newUserBean;

        public void setItemType(int itemType) {
            this.itemType = itemType;
        }

        public int getRoomIndex() {
            return roomIndex;
        }

        public void setRoomIndex(int roomIndex) {
            this.roomIndex = roomIndex;
        }

        public List<BannerInfo> getmList1() {
            return mList1;
        }

        public void setmList1(List<BannerInfo> mList1) {
            this.mList1 = mList1;
        }

        public NewUserBean getNewUserBean() {
            return newUserBean;
        }

        public void setNewUserBean(NewUserBean newUserBean) {
            this.newUserBean = newUserBean;
        }

        @Override
        public int getItemType() {
            return itemType;
        }
    }

}
