package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.ui.widget.LiveItemView;
import com.yiya.mobile.ui.widget.LiveTabView;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.List;

import static com.tongdaxing.xchat_core.home.HomeRoom.ITEM_TYPE_VIDEO;

public class LiveMoreRecyclerAdapter extends BaseMultiItemQuickAdapter<HomeRoom, BaseViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(int itemType, View view, int position, LiveTabView.TabInfo tabInfo);
    }

    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    private Context mContext;
    private int mItemRoomWidth = 0;

    public LiveMoreRecyclerAdapter(Context context, List<HomeRoom> data) {
        this(data);
        mContext = context;
        mItemRoomWidth = (DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 30) - DisplayUtils.dip2px(mContext, 10)) / 2;
    }

    public LiveMoreRecyclerAdapter(List<HomeRoom> data) {
        super(data);
        addItemType(ITEM_TYPE_VIDEO, R.layout.item_home_live_room);
    }

    public void setData(List<HomeRoom> data) {
        setNewData(data);
    }


    @Override
    protected void convert(BaseViewHolder helper, HomeRoom item) {
        handleItemRoomChat(helper, item);
    }

    /**
     * 底部房间item样式
     *
     * @param helper
     * @param info
     */
    private void handleItemRoomChat(BaseViewHolder helper, HomeRoom info) {
        ViewGroup.LayoutParams bgParams = helper.getView(R.id.item_bg_fl).getLayoutParams();
        bgParams.height = mItemRoomWidth;
        bgParams.width = mItemRoomWidth;
        helper.getView(R.id.item_bg_fl).setLayoutParams(bgParams);
        LiveItemView liveItemView = helper.getView(R.id.item_live_item_view);
        ViewGroup.LayoutParams layoutParams = liveItemView.getLayoutParams();
        layoutParams.width = mItemRoomWidth;
        layoutParams.height = mItemRoomWidth;
        liveItemView.setLayoutParams(layoutParams);

        ImageLoadUtils.loadImage(mContext, info.getAvatar(), liveItemView.getBgView());
        ImageLoadUtils.loadImage(mContext, info.getTagPict(), liveItemView.getTagIv());
        liveItemView.setCountNum(info.getOnlineNum());
        liveItemView.setNameStr((!TextUtils.isEmpty(info.getTitle()) ? info.getTitle() : info.getNick() ));
        liveItemView.setLocalStr(info.getCity());

        helper.getView(R.id.item_bg_fl).setOnClickListener(v -> {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(info.getItemType(), null, getData().indexOf(info), null);
            }
        });
    }
}
