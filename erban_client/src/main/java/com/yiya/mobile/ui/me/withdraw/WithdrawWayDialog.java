package com.yiya.mobile.ui.me.withdraw;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/2/15
 */
public class WithdrawWayDialog extends BaseDialogFragment implements View.OnClickListener {

    private WithdrawInfo withdrawInfo;
    private OnWithdrawWayChangeListener onWithdrawWayChangeListener;

    private ImageView mExitIv, mWeixinIv, mAliIv;
    private TextView mWayWeixinTv, mWayAliTv, mWeixinInputTv, mAliInpuTv;


    public static WithdrawWayDialog newInstance(WithdrawInfo withdrawInfo) {
        WithdrawWayDialog withdrawWayDialog = new WithdrawWayDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("withdrawInfo", withdrawInfo);
        withdrawWayDialog.setArguments(bundle);
        return withdrawWayDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            withdrawInfo = (WithdrawInfo) bundle.getSerializable("withdrawInfo");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams params = window.getAttributes();
            params.gravity = Gravity.BOTTOM;
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = 1000;
            window.setAttributes(params);
        }
        setCancelable(true);
//        return inflater.inflate(R.layout.dialog_withdraw_way, container, false);
        return inflater.inflate(R.layout.dialog_withdraw_select, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       /* mExitIv = (ImageView) view.findViewById(R.id.dialog_exit_iv);//退出*/

        mWayWeixinTv = (TextView) view.findViewById(R.id.weixin_pay_way_tv); //微信选择
        mWayAliTv = (TextView) view.findViewById(R.id.ali_pay_way_tv); //支付宝选择

        mWeixinIv = (ImageView) view.findViewById(R.id.weixin_pay_way_iv); //微信logo
        mAliIv = (ImageView) view.findViewById(R.id.ali_pay_way_iv); //支付宝logo

        mWeixinInputTv = (TextView) view.findViewById(R.id.weixin_modify_info_tv);
        mAliInpuTv = (TextView) view.findViewById(R.id.ali_modify_info_tv);


        /*mExitIv.setOnClickListener(this);*/
        mWeixinIv.setOnClickListener(this);
        mAliIv.setOnClickListener(this);

        mWeixinInputTv.setOnClickListener(this);
        mAliInpuTv.setOnClickListener(this);

        if (withdrawInfo != null) {
            //如果有支付宝账号，显示修改信息
            if (StringUtils.isNotEmpty(withdrawInfo.alipayAccount) && !"null".equals(withdrawInfo.alipayAccount)) {
                mAliInpuTv.setText("修改信息");
            }
            //如果有微信账号，显示已绑定（貌似不能修改）
            if (withdrawInfo.hasWx) {
                mWeixinInputTv.setText("重新验证");
            }

            updateSelectUI(withdrawInfo.withDrawType);


        }

    }

    @Override
    public void onResume() {
        super.onResume();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        getDialog().getWindow().setLayout((int)(dm.widthPixels*0.96),ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    /**
     * @param value 1: weixin ; 2: ali
     */
    private void updateSelectUI(int value) {

        if (value == 2) {//优先以后端状态为主
            if (StringUtils.isNotEmpty(withdrawInfo.alipayAccount) && !"null".equals(withdrawInfo.alipayAccount)) {//有支付宝账号
                setAliSelect();
            } else {
                if (withdrawInfo.hasWx) {//有微信账号改变状态，否则默认
                    setWeixinSelect();
                }
            }
        } else {
            if (withdrawInfo.hasWx) {//有微信账号改变状态，否则默认
                setWeixinSelect();

            } else {//有支付宝账号
                if (StringUtils.isNotEmpty(withdrawInfo.alipayAccount) && !"null".equals(withdrawInfo.alipayAccount)) {//有支付宝账号
                    setAliSelect();
                }
            }

        }
    }

    private void setWeixinSelect() {
        mWeixinIv.setImageResource(R.mipmap.ic_wechat);
        mWayWeixinTv.setVisibility(View.VISIBLE);
    }

    private void setAliSelect() {
        mAliIv.setImageResource(R.mipmap.ic_alipay);
        mWayAliTv.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.weixin_pay_way_iv:
                if (withdrawInfo != null && withdrawInfo.hasWx) {
                    if (withdrawInfo.withDrawType != 1) {
                        setWeixinSelect();
                        withdrawInfo.withDrawType = 1;
                        if (onWithdrawWayChangeListener != null) {
                            onWithdrawWayChangeListener.onChangeListener(withdrawInfo);
                        }
                    }
                } else {
                    ChangeWeixinPayActivity.start(getContext());
                }
                dismiss();
                break;
            case R.id.ali_pay_way_iv:
                if (withdrawInfo != null && StringUtils.isNotEmpty(withdrawInfo.alipayAccount) && !"null".equals(withdrawInfo.alipayAccount)) {
                    if (withdrawInfo.withDrawType != 2) {
                        setAliSelect();
                        withdrawInfo.withDrawType = 2;
                        if (onWithdrawWayChangeListener != null) {
                            onWithdrawWayChangeListener.onChangeListener(withdrawInfo);
                        }
                    } else {
//                        BinderAlipayActivity.start(getContext(), withdrawInfo);
                        ChangeAlipayActivity.start(getContext(), withdrawInfo);
                    }
                }
                dismiss();
                break;
            case R.id.weixin_modify_info_tv:
                if (withdrawInfo != null) {
//                    BinderWeixinPayActivity.start(getContext(), withdrawInfo);
                    ChangeWeixinPayActivity.start(getContext());
                    dismiss();
                }
                break;
            case R.id.ali_modify_info_tv:
                if (withdrawInfo != null && getContext() != null) {
                    //跳转绑定手机号码，绑定成功以后显示bindersucceed
//                    BinderAlipayActivity.start(getContext(), withdrawInfo);
                    ChangeAlipayActivity.start(getContext(), withdrawInfo);
                }
                dismiss();
                break;
            /*case R.id.dialog_exit_iv:
                dismiss();
                break;*/
        }
    }

    public void setOnWithdrawWayChangeListener(OnWithdrawWayChangeListener onWithdrawWayChangeListener) {
        this.onWithdrawWayChangeListener = onWithdrawWayChangeListener;
    }

    public interface OnWithdrawWayChangeListener {
        void onChangeListener(WithdrawInfo withdrawInfo);
    }
}
