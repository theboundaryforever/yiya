package com.yiya.mobile.ui.web;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.MainActivity;
import com.yiya.mobile.ui.me.wallet.activity.MyWalletActivity;
import com.yiya.mobile.ui.widget.dialog.ShareDialog;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;
import com.yiya.ndklib.JniUtils;
import com.juxiao.library_ui.widget.AppToolBar;
import com.juxiao.library_utils.JxUuidFactory;
import com.juxiao.safetychecker.AvdChecker;
import com.netease.nim.uikit.common.util.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.JsResponseInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.VersionUtil;
import com.tongdaxing.xchat_framework.util.util.json.JsonParser;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * <p> html js 与webview 交互接口</p>
 * Created by ${user} on 2017/11/6.
 */
public class JSInterface {
    private static final String TAG = JSInterface.class.getSimpleName();
    private WebView mWebView;
    private BaseWebViewActivity mActivity;
    private int mPosition;

    public JSInterface(WebView webView, BaseWebViewActivity activity) {
        mWebView = webView;
        mActivity = activity;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    /**
     * 跳转个人主页
     *
     * @param uid 用户id
     */
    @JavascriptInterface
    public void openPersonPage(String uid) {
        LogUtil.i(TAG, "openPersonPage：" + uid);
        if (!TextUtils.isEmpty(uid)) {
            try {
                long uidLong = Long.parseLong(uid);
                UIHelper.showUserInfoAct(mActivity, uidLong);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 跳转个人资料编辑界面
     */
    @JavascriptInterface
    public void skipToEditPersonalInfo() {
        UIHelper.showUserInfoModifyAct(mActivity, CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    /**
     * 跳转充值页面
     */
    @JavascriptInterface
    public void openChargePage() {
        if (mActivity != null) {
//            WalletActivity.start(mActivity);
//            ChargeActivity.start(mActivity);
            MyWalletActivity.start(mActivity,1);
        }
    }

    @JavascriptInterface
    public void showShareButton(boolean isShow) {
        if (mActivity instanceof CommonWebViewActivity && ((CommonWebViewActivity) mActivity).getToolbar() != null) {
            mActivity.runOnUiThread(() -> {
                if (mActivity != null && ((CommonWebViewActivity) mActivity).getToolbar() != null) {
                    ((CommonWebViewActivity) mActivity).getToolbar().setRightImageBtnVisibility(isShow ? View.VISIBLE : View.GONE);
                }
            });

        }
    }

    @JavascriptInterface
    public void openSharePage() {
        if (mActivity instanceof CommonWebViewActivity) {
            ShareDialog shareDialog = new ShareDialog(mActivity);
            shareDialog.setOnShareDialogItemClick((CommonWebViewActivity) mActivity);
            shareDialog.show();
        }
    }

    /**
     * 调转钱包页
     */
    @JavascriptInterface
    public void openPurse() {
        LogUtil.i(TAG, "openPurse：");
    }

    /**
     * 调转房间
     *
     * @param uid 房主uid
     */
    @JavascriptInterface
    public void openRoom(String uid) {
        LogUtil.i(TAG, "openRoom：" + uid);
        if (!TextUtils.isEmpty(uid)) {
            try {
                long uidLong = Long.parseLong(uid);
                // todo 开启房间要传入房间类型
                RoomFrameActivity.start(mActivity, uidLong, RoomInfo.ROOMTYPE_HOME_PARTY);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 获取用户ticket
     */
    @JavascriptInterface
    public String getTicket() {
        return CoreManager.getCore(IAuthCore.class).getTicket();
    }

    /**
     * 获取设备ID
     *
     * @return 设备id
     */
    @JavascriptInterface
    public String getDeviceId() {
        String jxUuid = null;
        try {
            jxUuid = JxUuidFactory.getFactory().getJxUuid(BasicConfig.INSTANCE.getAppContext());
        } catch (IOException e) {
            jxUuid = null;
            com.tongdaxing.xchat_framework.util.util.LogUtil.i("getJxUuid fail");
            e.printStackTrace();
        }

        return jxUuid;
    }

    /**
     * 获取设备ID
     *
     * @return 设备id
     */
    @JavascriptInterface
    public String getDeviceIsSimulator() {
        return AvdChecker.getInstance().check(BasicConfig.INSTANCE.getAppContext()) ? "1":"0";
    }

    /**
     * 获取uid
     *
     * @return uid
     */
    @JavascriptInterface
    public String getUid() {
        return String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @JavascriptInterface
    public String getPosition() {
        return String.valueOf(mPosition);
    }

    @JavascriptInterface
    public String getAppVersion() {
        return String.valueOf(VersionUtil.getLocalName(BasicConfig.INSTANCE.getAppContext()));
    }

    /*--------------------图片选择 start--------------------------*/

    /**
     * 请求调起拍照+相册选择器
     */
    @JavascriptInterface
    public void requestImageChooser() {
        if (mActivity != null) {
            mActivity.showImageChooser();
        }
    }

    /**
     * 通知H5图片选择结果
     */
    public void onImageChooserResult(String imageUrl) {
        mWebView.evaluateJavascript("onImageChooserResult('" + imageUrl + "')", value -> {

        });
    }



    /*--------------------图片选择 end--------------------------*/

    /*-----------------网络请求 start----------------------*/

    /**
     * 发起网络请求
     */
    @JavascriptInterface
    public void httpRequest(int requestMethod, String urlController, String headerMapString, String paramMapString) {
        Map<String, String> header;
        Map<String, String> params;
        try {
            header = mapObejctsToMapStrings(JsonParser.toMap(headerMapString));
            params = mapObejctsToMapStrings(JsonParser.toMap(paramMapString));
        } catch (Exception e) {
            e.printStackTrace();
            onHttpResponse(urlController, true, "", e.getMessage());
            return;
        }
        if (params == null) {
            params = new HashMap<>();
        }
        params = CommonParamUtil.getDefaultParam(params);
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        String url = UriProvider.JAVA_WEB_URL.concat(urlController);
        if (requestMethod == 1) {
            OkHttpManager.getInstance().doPostRequest(url, header, params, new OkHttpManager.MyCallBack<String>() {

                @Override
                public void onError(Exception e) {
                    onHttpResponse(urlController, true, "", e.getMessage());
                }

                @Override
                public void onResponse(String response) {
                    onHttpResponse(urlController, false, response, "");
                }
            });
        } else {
            OkHttpManager.getInstance().getRequest(url, header, params, new OkHttpManager.MyCallBack<String>() {

                @Override
                public void onError(Exception e) {
                    onHttpResponse(urlController, true, "", e.getMessage());
                }

                @Override
                public void onResponse(String response) {
                    onHttpResponse(urlController, false, response, "");
                }
            });
        }
    }

    private Map<String, String> mapObejctsToMapStrings(Map<String, Object> mapObjects) {
        Map<String, String> mapStrings = new HashMap<>();
        if (mapObjects != null) {
            for (Map.Entry<String, Object> mapObject : mapObjects.entrySet()) {
                mapStrings.put(mapObject.getKey(), mapObject.getValue().toString());
            }
        }
        return mapStrings;
    }

    /**
     * 通知H5请求结果
     */
    public void onHttpResponse(String urlController, boolean isRequestError, String bodyString, String errorMsg) {
        String responseStr;
        JsResponseInfo responseInfo = new JsResponseInfo();
        responseInfo.setUrlController(urlController);
        responseInfo.setRequestError(isRequestError);
        responseInfo.setBodyString(bodyString);
        responseInfo.setErrorMsg(errorMsg);
        responseStr = JsonParser.toJson(responseInfo);
        mWebView.evaluateJavascript("onHttpResponse(" + responseStr + ")", value -> {

        });
    }

    /**
     * 请求参数加密
     *
     * @param request
     * @return
     */
    @JavascriptInterface
    public String buildRequest(String request) {
        try {
            String s = JniUtils.encryptAes(mActivity, request);
            com.juxiao.library_utils.log.LogUtil.e("buildRequest:" + s);
            return s;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 请求响应内容解密
     *
     * @param response
     * @return
     */
    @JavascriptInterface
    public String extractResponse(String response) {
        try {
            com.juxiao.library_utils.log.LogUtil.e("response:" + response);
            String s = JniUtils.decryptAes(mActivity, response);
            com.juxiao.library_utils.log.LogUtil.e("extractResponse:" + s);
            return s;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    /*-----------------网络请求 end----------------------*/




    /*-------------------用户信息 start---------------------------*/

    @JavascriptInterface
    public String getUserPhoneNumber() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        //手机号为用户id 也是属于未绑定手机号
        if (userInfo != null && !TextUtils.isEmpty(userInfo.getPhone()) && !userInfo.getPhone().equals(
                String.valueOf(userInfo.getErbanNo()))) {
            return userInfo.getPhone();
        }
        return "";
    }

    /*-------------------------------------------- UI相关 start -----------------------------------*/
    @JavascriptInterface
    public void setupNavigationBarRightItem(String imageUrl) {
        if (mActivity instanceof CommonWebViewActivity) {
            if (!mActivity.isFinishing() && !mActivity.isDestroyed()) {
                mActivity.runOnUiThread(() -> {
                    if (mActivity == null || mActivity.isFinishing() || mActivity.isDestroyed()) {
                        return;
                    }
                    if (((CommonWebViewActivity) mActivity).getToolbar() != null && StringUtils.isNotEmpty(imageUrl)) {
                        Json json = Json.parse(imageUrl);
                        Json data = json.json("data");
                        if (data != null && data.has("imageUrl")) {
                            String url = data.str("imageUrl");
                            if (StringUtils.isNotEmpty(url)) {
                                ImageView imageView = ((CommonWebViewActivity) mActivity).getToolbar().getIvRight();
                                RelativeLayout.LayoutParams rl = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                                rl.width = DisplayUtils.dip2px(mActivity, 35);
                                rl.height = rl.width;
                                imageView.setLayoutParams(rl);
                                imageView.setVisibility(View.VISIBLE);
                                imageView.setOnClickListener(v -> {
                                    if (mWebView != null) {
                                        mWebView.loadUrl("javascript:onNavigationBarRightItemDidClicked()");
                                    }
                                });
                                ImageLoadUtils.loadImage(mActivity, url, imageView);
                            }
                        }
                    }
                });
            }
        }
    }

    /**
     * 设置导航栏背景颜色
     *
     * @param jsonString
     */
    @JavascriptInterface
    public void setupNavigationBarBackgroundColor(String jsonString) {
        try {
            String color = new JSONObject(jsonString).getString("color");
            //color[String]: 背景颜色(#FFFFFF)
            if (mActivity instanceof CommonWebViewActivity && ((CommonWebViewActivity) mActivity).getToolbar() != null) {
                mActivity.runOnUiThread(() -> {
                    ((CommonWebViewActivity) mActivity).getToolbar().setBackgroundColor(Color.parseColor(color));
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 设置导航栏Item样式
     *
     * @param jsonString
     */
    @JavascriptInterface
    public void setupNavigationBarItemStyle(String jsonString) {
        try {
            int style = new JSONObject(jsonString).getInt("style");
            //style[String]: Item风格, 1为白色, 2为黑色
            if (mActivity instanceof CommonWebViewActivity && ((CommonWebViewActivity) mActivity).getToolbar() != null) {
                mActivity.runOnUiThread(() -> {
                    AppToolBar toolbar = ((CommonWebViewActivity) mActivity).getToolbar();
                    int resDrawable = R.drawable.arrow_left_white;
                    int resColor = R.color.white;
                    if (style == 2) {
                        resDrawable = R.drawable.arrow_left;
                        resColor = R.color.black;
                    }
                    toolbar.setLeftImgRes(resDrawable);
                    toolbar.setTitleColor(ContextCompat.getColor(mActivity, resColor));
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 跳转首页
     */
    @JavascriptInterface
    public void skipToHome() {
        MainActivity.startPage(mActivity, 0);
    }

    /**
     * 跳转设置页
     */
    @JavascriptInterface
    public void skipToSetting() {
        UIHelper.showSettingAct(mActivity);
    }

    /**
     * 通知H5原生WebView将要显示
     */
    public void onWebViewWillAppear() {
        mWebView.evaluateJavascript("onWebViewWillAppear()", value -> {

        });
    }

    /**
     * 关闭WebView
     */
    @JavascriptInterface
    public void closeWin() {
        if (mActivity != null) {
            mActivity.finish();
        }
    }

    /*-------------------------------------------- UI相关 end -----------------------------------*/
}

/**
 * <p> html js 与webview 交互接口</p>
 * Created by ${user} on 2017/11/6.
 */
//public class JSInterface {
//    private static final String TAG = JSInterface.class.getSimpleName();
//    private WebView mWebView;
//    private CommonWebViewActivity mActivity;
//    private int mPosition;
//
//    public JSInterface(WebView webView, CommonWebViewActivity activity) {
//        mWebView = webView;
//        mActivity = activity;
//    }
//
//    public void setPosition(int position) {
//        mPosition = position;
//    }
//
//    @JavascriptInterface
//    public void showShareButton(boolean isShow) {
//        if (mActivity != null && mActivity.getToolbar() != null) {
//            mActivity.runOnUiThread(() -> {
//                if (mActivity == null || mActivity.isFinishing() || mActivity.isDestroyed())
//                    return;
//                if (mActivity.getToolbar() != null) {
//                    mActivity.getToolbar().setRightImageBtnVisibility(isShow ? View.VISIBLE : View.GONE);
//                }
//            });
//
//        }
//    }
//
//
//    @JavascriptInterface
//    public void openSharePage() {
//        if (mActivity != null) {
//            mActivity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    ShareDialog shareDialog = new ShareDialog(mActivity);
//                    shareDialog.setOnShareDialogItemClick(mActivity);
//                    shareDialog.show();
//                }
//            });
//        }
//    }
//
//    /**
//     * 获取用户ticket
//     *
//     * @return
//     */
//    @JavascriptInterface
//    public String getTicket() {
//        return CoreManager.getCore(IAuthCore.class).getTicket();
//    }
//
//    /**
//     * 获取设备ID
//     *
//     * @return 设备id
//     */
//    @JavascriptInterface
//    public String getDeviceId() {
//        return DeviceUuidFactory.getDeviceId(BasicConfig.INSTANCE.getAppContext());
//    }
//
//    /**
//     * 获取uid
//     *
//     * @return uid
//     */
//    @JavascriptInterface
//    public String getUid() {
//        return String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
//    }
//
//    @JavascriptInterface
//    public String getPosition() {
//        return String.valueOf(mPosition);
//    }
//
//
//
//    /*--------------------图片选择 start--------------------------*/
//
//    /**
//     * 请求调起拍照+相册选择器
//     */
//    @JavascriptInterface
//    public void requestImageChooser() {
//        if (mActivity != null) {
//            mActivity.showImageChooser();
//        }
//    }
//
//    /**
//     * 通知H5图片选择结果
//     */
//    public void onImageChooserResult(String imageUrl) {
//        mWebView.evaluateJavascript("onImageChooserResult('" + imageUrl + "')", value -> {
//
//        });
//    }
//
//    /*--------------------图片选择 end--------------------------*/
//
//    /*-----------------网络请求 start----------------------*/
//
//    /**
//     * 发起网络请求
//     */
//    @JavascriptInterface
//    public void httpRequest(int requestMethod, String urlController, String headerMapString, String paramMapString) {
//        Map<String, String> header;
//        Map<String, String> params;
//        try {
//            header = mapObejctsToMapStrings(JsonParser.toMap(headerMapString));
//            params = mapObejctsToMapStrings(JsonParser.toMap(paramMapString));
//        } catch (Exception e) {
//            e.printStackTrace();
//            onHttpResponse(urlController, true, "", e.getMessage());
//            return;
//        }
//        if (params == null) {
//            params = new HashMap<>();
//        }
//        params = CommonParamUtil.getDefaultParam(params);
//        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
//        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
//        String url = UriProvider.JAVA_WEB_URL.concat(urlController);
//        if (requestMethod == 1) {
//            OkHttpManager.getInstance().doPostRequest(url, header, params, new OkHttpManager.MyCallBack<Json>() {
//                @Override
//                public void onError(Exception e) {
//                    onHttpResponse(urlController, true, "", e.getMessage());
//                }
//
//                @Override
//                public void onResponse(Json response) {
//                    if (response != null ){
//                        onHttpResponse(urlController, false, response.toString(), "");
//                    }else {
//                        onHttpResponse(urlController, true, "", "数据异常");
//                    }
//                }
//            });
//        } else {
//            OkHttpManager.getInstance().getRequest(url, header, params, new OkHttpManager.MyCallBack<Json>() {
//                @Override
//                public void onError(Exception e) {
//                    onHttpResponse(urlController, true, "", e.getMessage());
//                }
//
//                @Override
//                public void onResponse(Json response) {
//                    if (response != null ){
//                        onHttpResponse(urlController, false,response.toString(), "");
//                    }else {
//                        onHttpResponse(urlController, true, "", "数据异常");
//                    }
//                }
//            });
//        }
//    }
//
//    private Map<String, String> mapObejctsToMapStrings(Map<String, Object> mapObjects) {
//        Map<String, String> mapStrings = new HashMap<>();
//        if (mapObjects != null) {
//            for (Map.Entry<String, Object> mapObject : mapObjects.entrySet()) {
//                mapStrings.put(mapObject.getkey(), mapObject.getValue().toString());
//            }
//        }
//        return mapStrings;
//    }
//
//    /**
//     * 通知H5请求结果
//     */
//    public void onHttpResponse(String urlController, boolean isRequestError, String bodyString, String errorMsg) {
//        String responseStr;
//        JsResponseInfo responseInfo = new JsResponseInfo();
//        responseInfo.setUrlController(urlController);
//        responseInfo.setRequestError(isRequestError);
//        responseInfo.setBodyString(bodyString);
//        responseInfo.setErrorMsg(errorMsg);
//        responseStr = JsonParser.toJson(responseInfo);
//        mWebView.evaluateJavascript("onHttpResponse(" + responseStr + ")", value -> {
//
//        });
//    }
//
//    /*-----------------网络请求 end----------------------*/
//
//
//
//
//    /*-------------------用户信息 start---------------------------*/
//
//    @JavascriptInterface
//    public String getUserPhoneNumber() {
//        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//        if (userInfo != null && !TextUtils.isEmpty(userInfo.getPhone()) && !userInfo.getPhone().equals(String.valueOf(userInfo.getErbanNo()))) {//手机号为用户id 也是属于未绑定手机号
//            return userInfo.getPhone();
//        }
//        return "";
//    }
//
//
//
//
//    /*-------------------------------------------- UI相关 start -----------------------------------*/
//    @JavascriptInterface
//    public void setupNavigationBarRightItem(String imageUrl) {
//        if (mActivity != null && !mActivity.isFinishing() && !mActivity.isDestroyed()) {
//            mActivity.runOnUiThread(() -> {
//                if (mActivity == null || mActivity.isFinishing() || mActivity.isDestroyed())
//                    return;
//                if (mActivity.getToolbar() != null && StringUtils.isNotEmpty(imageUrl)) {
//                    Json json = Json.parse(imageUrl);
//                    Json data = json.json("data");
//                    if (data != null && data.has("imageUrl")) {
//                        String url = data.str("imageUrl");
//                        if (StringUtils.isNotEmpty(url)) {
//                            ImageView imageView = mActivity.getToolbar().getIvRight();
//                            RelativeLayout.LayoutParams rl = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
//                            rl.width = DisplayUtils.dip2px(mActivity, 35);
//                            rl.height = rl.width;
//                            imageView.setLayoutParams(rl);
//                            imageView.setVisibility(View.VISIBLE);
//                            imageView.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    if (mWebView != null) {
//                                        mWebView.loadUrl("javascript:onNavigationBarRightItemDidClicked()");
//                                    }
//                                }
//                            });
//                            ImageLoadUtils.loadImage(mActivity, url, imageView);
//                        }
//                    }
//                }
//            });
//        }
//    }
//
//    @JavascriptInterface
//    public void closeWin(){
//        if (mActivity != null){
//            mActivity.finish();
//        }
//    }
//
//    /*-------------------------------------------- UI相关 end -----------------------------------*/
//
//
//    /*--------------------------------------------- 页面跳转 -------------------------------------*/
//
//    /**
//     * 跳转Scheme(只支持HTTP/HTTPS协议)
//     * @param scheme
//     */
//    @JavascriptInterface
//    public void skipToScheme(String scheme){
//        if (mActivity != null && StringUtils.isNotEmpty(scheme)) {
//            Json json = Json.parse(scheme);
//            Json data = json.json("data");
//            if (data != null && data.has("url")) {
//                String url = data.str("url");
//                if (url.startsWith("http") || url.startsWith("https")){
//                    CommonWebViewActivity.start(mActivity,url);
//                }
//            }
//        }
//    }
//
//    /**
//     * 调转房间
//     *
//     * @param uid 房主uid
//     */
//    @JavascriptInterface
//    public void openRoom(String uid) {
//        if (!TextUtils.isEmpty(uid)) {
//            try {
//                long uidLong = Long.parseLong(uid);
//                RoomFrameActivity.start(mActivity, uidLong);
//            } catch (NumberFormatException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * 调转个人主页
//     *
//     * @param uid 用户id
//     */
//    @JavascriptInterface
//    public void openPersonPage(String uid) {
//        if (!TextUtils.isEmpty(uid)) {
//            try {
//                long uidLong = Long.parseLong(uid);
//                UIHelper.showUserInfoAct(mActivity, uidLong);
//            } catch (NumberFormatException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * 跳转充值页面
//     */
//    @JavascriptInterface
//    public void openChargePage() {
//        if (mActivity != null) {
//            WalletActivity.start(mActivity);
//        }
//    }
//
//    /**
//     * 调转钱包页
//     */
//    @JavascriptInterface
//    public void openPurse() {
//        if (mActivity != null) {
//            ChargeActivity.start(mActivity);
//        }
//    }
//
//    @JavascriptInterface
//    public void skipToInviteFriends(String json){//跳转邀请页面
//        if (mActivity != null) {
//            InviteFriendActivity.start(mActivity);
//        }
//    }
//
//    @JavascriptInterface
//    public void skipToCarMall(String json){//跳转商城界面(座驾头饰)
//        if (mActivity != null) {
//            ShopActivity.start(mActivity, true, 0);
//        }
//    }
//
//
//    @JavascriptInterface
//    public void skipToBindPhone(String json){//跳转绑定手机界面
//        if (mActivity != null) {
//            BinderPhoneActivity.start(mActivity);
//        }
//    }
//
//    @JavascriptInterface
//    public void skipToRank(String json){//跳转排行榜界面
//        if (mActivity != null) {
//            RankingListActivity.start(mActivity);
//        }
//    }
//
//     /*-------------------------------------------- 页面跳转 -------------------------------------*/
//
//}
