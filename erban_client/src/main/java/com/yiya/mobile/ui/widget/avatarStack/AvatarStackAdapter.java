package com.yiya.mobile.ui.widget.avatarStack;

import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.juxiao.library_utils.DisplayUtils;
import com.makeramen.roundedimageview.RoundedImageView;
import com.tongdaxing.erban.R;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/12.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class AvatarStackAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    private RoundedImageView avatarRiv;

    public AvatarStackAdapter() {
        super(R.layout.item_avatar_stack);
    }

    @Override
    protected void convert(BaseViewHolder helper, String url) {
        avatarRiv = helper.getView(R.id.avatar);
        if (url != null) {
            ImageLoadUtils.loadAvatar(mContext, url, avatarRiv, true);
        }
        ((RecyclerView.LayoutParams) helper.getView(R.id.cl_avatar).getLayoutParams()).setMarginStart(helper.getAdapterPosition() == 0 ? 0 : DisplayUtils.dip2px(mContext, -10));
    }

    protected RoundedImageView getAvatarRiv() {
        return avatarRiv;
    }
}
