package com.yiya.mobile.ui.newfind.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.juxiao.library_utils.log.LogUtil;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.SquareLoopInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.yiya.mobile.ui.home.adpater.LiveStreamRecyclerAdapter;
import com.yiya.mobile.ui.newfind.coutomview.HorizontalListView;
import com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment;
import com.yiya.mobile.ui.widget.LiveTabView;
import com.yiya.mobile.ui.widget.avatarStack.AvatarStackView;
import com.yiya.mobile.ui.widget.avatarStack.SquareAvatarStackAdapter;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.view.htmlTextView.HtmlTextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_1;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_2;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_3;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_4;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_5;

public class NewFindFriendsRecyclerAdapter extends BaseMultiItemQuickAdapter<NewFindFriendsFragment.NewFindFriendsinfo, BaseViewHolder> {

    private Context mContext;

    public interface OnItemClickListener {
        void onItemClick(int itemType, View view, int position, LiveTabView.TabInfo tabInfo);
    }

    private LiveStreamRecyclerAdapter.OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(LiveStreamRecyclerAdapter.OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public NewFindFriendsRecyclerAdapter(List<NewFindFriendsFragment.NewFindFriendsinfo> data) {
        super(data);
        addItemType(ITEM_TYPE_1, R.layout.item_newfindfriends_txt);  //文字标题
        addItemType(ITEM_TYPE_2, R.layout.item_newfindfriends_headportrait);  //头像列表
        addItemType(ITEM_TYPE_3, R.layout.item_viewflipper);  //公聊大厅 item_newfindfriends_publicchat item_viewflipper
        addItemType(ITEM_TYPE_4, R.layout.item_newfindfriends_txtt);  //交友广场标题
        addItemType(ITEM_TYPE_5, R.layout.item_newfindfriends_friends);  //交友广场列表
    }

    private int mItemRoomWidth = 0;

    public NewFindFriendsRecyclerAdapter(List<NewFindFriendsFragment.NewFindFriendsinfo> data, Context mContext) {
        this(data);
        this.mContext = mContext;
        mItemRoomWidth = (DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 30) - DisplayUtils.dip2px(mContext, 10)) / 2;
    }

    public void setData(List<NewFindFriendsFragment.NewFindFriendsinfo> data) {
        setNewData(data);
    }

    @Override
    protected void convert(BaseViewHolder helper, NewFindFriendsFragment.NewFindFriendsinfo item) {
        switch(item.getItemType()){
            case ITEM_TYPE_1:
                break;
            case ITEM_TYPE_2:
                handleItemHeadportrait(helper,item);
                break;
            case ITEM_TYPE_3:
                handleItemPublicChat(helper,item);//公聊大厅
                break;
            case ITEM_TYPE_4:
                break;
            case ITEM_TYPE_5:
                handleItemFriends(helper,item);
                break;
        }
    }


    /***用户在房间列表展示（自己已关注用户）*/
    private void handleItemHeadportrait(BaseViewHolder baseViewHolder, NewFindFriendsFragment.NewFindFriendsinfo newFindFriendsinfo){
        HorizontalListView gridView = baseViewHolder.getView(R.id.headportrait_listview);
        gridView.setAdapter(new HeadportraitGirdViewAdapter(mContext,newFindFriendsinfo.getOnlinesFriendsBeanList()));
    }

    /***公聊大厅*/
    private void handleItemPublicChat(BaseViewHolder baseViewHolder, NewFindFriendsFragment.NewFindFriendsinfo newFindFriendsinfo){
        ViewFlipper flipper = baseViewHolder.getView(R.id.item_rank_flipper);

        if(newFindFriendsinfo.getSquareLoopInfoList()!=null){
            flipper.removeAllViews();
            for (int i = 0; i < newFindFriendsinfo.getSquareLoopInfoList().size(); i++) {
                if (i % 2 == 0) {
                    ArrayList<SquareLoopInfo> list = new ArrayList<>();
                    list.add(newFindFriendsinfo.getSquareLoopInfoList().get(i));
                    if (i + 1 < newFindFriendsinfo.getSquareLoopInfoList().size()) {
                        list.add(newFindFriendsinfo.getSquareLoopInfoList().get(i + 1));
                        flipper.addView(generateFlipperView(list));
                    }
                }
            }
            flipper.startFlipping();
        }else{
            int tmp = 0;
            View wealthView = View.inflate(mContext, R.layout.item_newfindfriends_publicchat, null);
            flipper.addView(wealthView);
            //tmp++;
            View view = View.inflate(mContext, R.layout.item_newfindfriends_publicchat, null);
            ((ImageView)view.findViewById(R.id.iv_3)).setImageResource(R.drawable.ic_sex_girl1);
            flipper.addView(view);
            //tmp++;
            if (tmp > 1) {
                flipper.startFlipping();
            }
        }

    }

    /***交由广场列表数据展示*/
    private void handleItemFriends(BaseViewHolder baseViewHolder, NewFindFriendsFragment.NewFindFriendsinfo newFindFriendsinfo){
        Log.d("交友广场---交友列表赋值",""+newFindFriendsinfo.getHomeYuChatListInfo().getNick());
        TextView tv_title = baseViewHolder.getView(R.id.tv_newfindfriends_title);
        TextView tv_homeid = baseViewHolder.getView(R.id.tv_newfindfriends_homeid);
        TextView tb_number = baseViewHolder.getView(R.id.tb_newfindfriends_number);
        tv_title.setText(newFindFriendsinfo.getHomeYuChatListInfo().getNick());
        tv_homeid.setText("ID:"+newFindFriendsinfo.getHomeYuChatListInfo().getRoomId());
        tb_number.setText(String.valueOf(newFindFriendsinfo.getHomeYuChatListInfo().getOnlineNum()));
        ImageLoadUtils.loadImage(mContext, newFindFriendsinfo.getHomeYuChatListInfo().getAvatar(), baseViewHolder.getView(R.id.cv_newfindfriends_headportrait));
        if(newFindFriendsinfo.getHomeYuChatListInfo().getTagPict().length()>0){
            baseViewHolder.getView(R.id.iv_icon).setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(mContext, newFindFriendsinfo.getHomeYuChatListInfo().getTagPict(), baseViewHolder.getView(R.id.iv_icon));
        }
        //声浪
        SVGAImageView svgaWave = baseViewHolder.getView(R.id.svga_wave_newfindfriends);
        if (svgaWave.getDrawable() != null) {
            if (!svgaWave.isAnimating()) {
                svgaWave.startAnimation();
            }
        } else {
            SVGAParser svgaParser = new SVGAParser(mContext);
            svgaParser.decodeFromAssets("microredwave.svga", new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                    svgaWave.setVideoItem(svgaVideoEntity);
                    if (!svgaWave.isAnimating()) {
                        svgaWave.startAnimation();
                    }
                }

                @Override
                public void onError() {
                    LogUtil.e("svgaWave onError");
                }
            });
        }
        baseViewHolder.getView(R.id.item_bg_f1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mItemClickListener != null){
                    mItemClickListener.onItemClick(newFindFriendsinfo.getItemType(),null,getData().indexOf(newFindFriendsinfo),null);
                }
            }
        });
    }


    private View generateFlipperView(List<SquareLoopInfo> squareLoopInfos) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.laytout_home_friends_square_flipper, null);
        AvatarStackView asvUser = view.findViewById(R.id.asv_user);
        asvUser.setAdapter(new SquareAvatarStackAdapter());
        Observable.fromIterable(squareLoopInfos)
                .map(squareLoopInfo -> squareLoopInfo.getMember().getAvatar())
                .toList()
                .subscribe((avatars, throwable) -> asvUser.setAvatarList(avatars));
        HtmlTextView tvTop = view.findViewById(R.id.tv_content_top);
        tvTop.setHtmlText(squareLoopInfos.get(0).getCustom().getData().getLoopHtml());
        HtmlTextView tvBottom = view.findViewById(R.id.tv_content_bottom);
        tvBottom.setHtmlText(squareLoopInfos.get(1).getCustom().getData().getLoopHtml());
        return view;
    }

}
