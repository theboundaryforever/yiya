package com.yiya.mobile.ui.widget;

import android.content.Context;
import android.support.annotation.Px;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */
public class EnableScrollViewPager extends ViewPager {
    private static final String TAG = "JANoScroll";
    private boolean noScrollStatus = true;
    private boolean mScrollTo = true;

    public EnableScrollViewPager(Context context) {
        super(context);
    }

    public EnableScrollViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        return noScrollStatus ? noScrollStatus : super.onInterceptHoverEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public void scrollTo(@Px int x, @Px int y) {
        super.scrollTo(mScrollTo ? x : 0, y);
    }

    @Override
    public void setCurrentItem(int item, boolean smoothScroll) {
        super.setCurrentItem(item, smoothScroll);

    }

    @Override
    public void setCurrentItem(int item) {
        super.setCurrentItem(item);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
    }


    /**
     * @param status
     * @deprecated
     */
    public void setScrollTo(boolean status) {
        this.mScrollTo = status;
    }

}