package com.yiya.mobile.ui.home.fragment;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.xchat_core.bean.HomeHotRoom;
import com.tongdaxing.xchat_core.bean.HomeTalkInfo;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.constant.AppConstant;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.presenter.home.HomeLiveStreamPresenter;
import com.yiya.mobile.presenter.home.HomeLiveStreamView;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.home.adpater.LiveStreamRecyclerAdapter;
import com.yiya.mobile.ui.me.shopping.activity.ShopActivity;
import com.yiya.mobile.ui.more.MoreActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.bean.HomeLiveInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.home.HomeLiveStreamPresenter;
import com.yiya.mobile.presenter.home.HomeLiveStreamView;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.home.adpater.LiveStreamRecyclerAdapter;
import com.yiya.mobile.ui.more.MoreActivity;
import com.yiya.mobile.utils.UIHelper;

import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_BANNER;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_RANK;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_CHAT;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_HOT;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_HOT_TAG;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_TAG;


/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */

@CreatePresenter(HomeLiveStreamPresenter.class)
public class HomeLiveStreamFragment extends BaseMvpFragment<HomeLiveStreamView, HomeLiveStreamPresenter> implements HomeLiveStreamView, OnRefreshListener, OnLoadMoreListener {

    @BindView(R.id.smart_refresh_layout)
    SmartRefreshLayout mSmartRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private LiveStreamRecyclerAdapter mAdapter;
    private GridLayoutManager mLayoutManager;
    private static String tabId;

    private int mPageSize = 20;
    private int mPageNum = 1;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_home_live_stream;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdapter != null) {
            if (mAdapter.getData() == null || mAdapter.getData().size() == 0) {
                getData(mPageNum = 1);
            }
        }
    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
//        mPageNum = 1;
//        getData(mPageNum);
    }

    private void getData(int pageNum) {
        mPageNum = pageNum;
        tabId = getArguments().getString("tabId");
        Log.d("首页--tabId",":"+tabId);
        getMvpPresenter().getHomeVideoRoomList(mPageNum, mPageSize, mPageNum == 1);

    }

    @Override
    public void initiate() {
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "直播List页");

        mLayoutManager = new GridLayoutManager(mContext, 2);
        mAdapter = new LiveStreamRecyclerAdapter(mContext, null);
        mAdapter.setSpanSizeLookup(new com.chad.library.adapter.base.BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                int value = 1;
                try {
                    int type = mAdapter.getData().get(position).getItemType();
                    if (type == ITEM_TYPE_BANNER || type == ITEM_TYPE_RANK || type == ITEM_TYPE_ROOM_HOT_TAG
                            || type == ITEM_TYPE_ROOM_TAG) {
                        value = 2;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return value;
            }
        });
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new MyItemDecoration());

        mSmartRefreshLayout.setOnRefreshListener(this::onRefresh);
        mSmartRefreshLayout.setOnLoadMoreListener(this::onLoadMore);

        mAdapter.setOnItemClickListener((LiveStreamRecyclerAdapter.OnItemClickListener) (itemType, view, position, tabInfo) -> {
            switch (itemType) {
                case ITEM_TYPE_ROOM_HOT:
                    LiveStreamInfo streamInfo = mAdapter.getItem(position);
                    if (streamInfo != null) {
                        RoomFrameActivity.start(getActivity(), streamInfo.getHomeHotRoomList().getUid(), streamInfo.getHomeHotRoomList().getType());
                    }
                    break;

                case ITEM_TYPE_ROOM_TAG:
                    if (tabInfo != null) {
                        MoreActivity.start(mContext, tabInfo.getId());
                    }
                    break;
                case ITEM_TYPE_RANK:
                    if(position==1){
                        //全服榜单
                        UIHelper.showAllRankListH5(mContext);
                    }else if(position==2){
                        //圆盘抽奖
                        UIHelper.showLuckyDrawH5(mContext);
                    }else if(position==3){
                        //商城
                        UIHelper.showDressMallH5(mContext);
                    }
                    break;
                case ITEM_TYPE_ROOM_CHAT:
                    LiveStreamInfo streamInfo1 = mAdapter.getItem(position);
                    if (streamInfo1 != null) {
                        RoomFrameActivity.start(getActivity(), streamInfo1.getHomeYuChatListInfo().getUid(), streamInfo1.getHomeYuChatListInfo().getType());
                    }
                    break;
            }

        });
    }

    @Override
    public void onLiveVideoRoomSucceed(int pageNum, List<LiveStreamInfo> data) {
        if (pageNum == 1) {
            mAdapter.setData(data);
        } else {
            mAdapter.addData(data);
        }


        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();

    }

    @Override
    public void onLiveVideoRoomFailed(String message) {
        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
    }

    @Override
    public String getTabId() {
        return tabId;
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        mPageNum = 1;
        getData(mPageNum);

    }

    @Override
    public void onLoadMore(RefreshLayout refreshlayout) {
        getData(mPageNum + 1);
    }

    private int mOffsetPadding = 0;
    private int mOffsetContext = 0;

    private class MyItemDecoration extends RecyclerView.ItemDecoration {

        private MyItemDecoration() {
            mOffsetPadding = DisplayUtils.dip2px(mContext, 15);
            mOffsetContext = DisplayUtils.dip2px(mContext, 10);
        }

        @Override
        public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.onDraw(c, parent, state);
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            int pos = parent.getChildAdapterPosition(view);
            LiveStreamInfo info = mAdapter.getData().get(pos);
            boolean tmpBool = false;
            if (mAdapter.getData() != null && info != null) {
                if (info.getItemType() == ITEM_TYPE_ROOM_HOT || info.getItemType() == ITEM_TYPE_ROOM_CHAT) {
                    tmpBool = true;
                    if (info.getRoomIndex() % 2 == 0) {
                        outRect.left = mOffsetPadding;
                        outRect.right = mOffsetContext;
                    } else {
                        outRect.left = mOffsetContext / 3;
                        outRect.right = mOffsetPadding;
                    }
                    outRect.bottom = mOffsetContext / 5 * 4;

                }
            }
            outRect.top = 0;
            if (!tmpBool) {
                outRect.right = 0;
                outRect.left = 0;
                outRect.bottom = 0;
            }
        }
    }


    public static class LiveStreamInfo implements MultiItemEntity {
        public static final int ITEM_TYPE_BANNER = 1;

        public static final int ITEM_TYPE_RANK = 2;

        public static final int ITEM_TYPE_ROOM_HOT = 3;

        public static final int ITEM_TYPE_ROOM_TAG = 4;

        public static final int ITEM_TYPE_ROOM_CHAT = 5;

        public static final int ITEM_TYPE_ROOM_HOT_TAG = 6;

        private int itemType;


        private int roomIndex = 0;


        private List<HomeLiveInfo.MasterBannerBean> masterBanner; //广告
        private RankBean rankBean;  //榜单
        private HomeLiveInfo.RoomListBean roomListBean; //房间
        private List<HomeLiveInfo.VideoRoomTagListBean> videoRoomTagList; //房间TAG

        private List<BannerInfo> bannerInfoList; //广告
        private HomeHotRoom homeHotRoomList;  //热门房间

        private HomeYuChatListInfo homeYuChatListInfo;//推荐房间

        public HomeYuChatListInfo getHomeYuChatListInfo() {
            return homeYuChatListInfo;
        }

        public void setHomeYuChatListInfo(HomeYuChatListInfo homeYuChatListInfo) {
            this.homeYuChatListInfo = homeYuChatListInfo;
        }

        public List<BannerInfo> getBannerInfoList() {
            return bannerInfoList;
        }

        public void setBannerInfoList(List<BannerInfo> bannerInfoList) {
            this.bannerInfoList = bannerInfoList;
        }

        public HomeHotRoom getHomeHotRoomList() {
            return homeHotRoomList;
        }

        public void setHomeHotRoomList(HomeHotRoom homeHotRoomList) {
            this.homeHotRoomList = homeHotRoomList;
        }

        public int getRoomIndex() {
            return roomIndex;
        }

        public void setRoomIndex(int roomIndex) {
            this.roomIndex = roomIndex;
        }

        public void setItemType(int value) {
            itemType = value;
        }

        public List<HomeLiveInfo.MasterBannerBean> getMasterBanner() {
            return masterBanner;
        }

        public void setMasterBanner(List<HomeLiveInfo.MasterBannerBean> masterBanner) {
            this.masterBanner = masterBanner;
        }

        public RankBean getRankBean() {
            return rankBean;
        }

        public void setRankBean(RankBean rankBean) {
            this.rankBean = rankBean;
        }

        public HomeLiveInfo.RoomListBean getRoomListBean() {
            return roomListBean;
        }

        public void setRoomListBean(HomeLiveInfo.RoomListBean roomListBean) {
            this.roomListBean = roomListBean;
        }

        public List<HomeLiveInfo.VideoRoomTagListBean> getVideoRoomTagList() {
            return videoRoomTagList;
        }

        public void setVideoRoomTagList(List<HomeLiveInfo.VideoRoomTagListBean> videoRoomTagList) {
            this.videoRoomTagList = videoRoomTagList;
        }

        @Override
        public int getItemType() {
            return itemType;
        }

        public static class RankBean {
            private List<HomeLiveInfo.WealthRankVoListBean> wealthRankVoList;
            private List<HomeLiveInfo.CharmRankVoListBean> charmRankVoList;

            public List<HomeLiveInfo.WealthRankVoListBean> getWealthRankVoList() {
                return wealthRankVoList;
            }

            public void setWealthRankVoList(List<HomeLiveInfo.WealthRankVoListBean> wealthRankVoList) {
                this.wealthRankVoList = wealthRankVoList;
            }

            public List<HomeLiveInfo.CharmRankVoListBean> getCharmRankVoList() {
                return charmRankVoList;
            }

            public void setCharmRankVoList(List<HomeLiveInfo.CharmRankVoListBean> charmRankVoList) {
                this.charmRankVoList = charmRankVoList;
            }
        }

    }

}
