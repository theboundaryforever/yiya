package com.yiya.mobile.ui.login.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.ui.widget.TextWatcherListener;
import com.yiya.mobile.utils.UIHelper;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.ClientType;
import com.netease.nis.captcha.Captcha;
import com.netease.nis.captcha.CaptchaListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.config.SpEvent;
import com.tongdaxing.xchat_framework.util.constant.AppKey;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.SpUtils;

/**
 * Created by zhouxiangfeng on 17/2/26.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final String KICK_OUT = "KICK_OUT";

    /**
     * 基本权限管理
     */
    private final String[] BASIC_PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO,
    };

    private AppToolBar mAppToolBar;
    private ImageView wxLogin;
    private ImageView qqLogin;
    private EditText etPhone;
    private EditText etPassword;
    private Button btnLogin;
    private boolean show = false;
    private Captcha mCaptcha;
    private int nextStepId = -1;
    private String validateStr = "";
    private TextWatcherListener phoneTextWatcherListener;
    private TextWatcherListener passwordTextWatcherListener;

    public static void start(Context context) {
        start(context, false);
    }

    public static void start(Context context, String validateStr) {

        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(KICK_OUT, false);
        intent.putExtra("validateStr", validateStr);
        context.startActivity(intent);
    }

    public static void start(Context context, boolean kickOut) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(KICK_OUT, kickOut);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "手机号登录");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "手机登录页");

        setContentView(R.layout.activity_ring_up);
        validateStr = getIntent().getStringExtra("validateStr");
        initImgCheck();
        onParseIntent();
        onFindViews();
        onSetListener();
//        permission();
        setSwipeBackEnable(false);

    }

    private void onParseIntent() {
        if (getIntent().getBooleanExtra(KICK_OUT, false)) {
            int type = NIMClient.getService(AuthService.class).getKickedClientType();
            String client;
            switch (type) {
                case ClientType.Web:
                    client = "网页端";
                    break;
                case ClientType.Windows:
                    client = "电脑端";
                    break;
                case ClientType.REST:
                    client = "服务端";
                    break;
                default:
                    client = "移动端";
                    break;
            }
            getDialogManager().showOkAndLabelDialog("你的帐号在" + client + "登录，被踢出下线，请确定帐号信息安全", "下线通知", true, true, false, false, new DialogManager.OkDialogListener() {
                @Override
                public void onOk() {
                }
            });
        }
    }

    private void permission() {
        checkPermission(() -> {
        }, R.string.ask_again, BASIC_PERMISSIONS);
    }


    private void onSetListener() {
        wxLogin.setOnClickListener(this);
        qqLogin.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        phoneTextWatcherListener = new PhoneTextWatcherListener();
        passwordTextWatcherListener = new PasswordTextWatcherListener();
        etPhone.addTextChangedListener(phoneTextWatcherListener);
        etPassword.addTextChangedListener(passwordTextWatcherListener);
        findView(R.id.tv_agreement).setOnClickListener(this);

        mAppToolBar.setOnBackBtnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void onFindViews() {
        mAppToolBar = (AppToolBar) findViewById(R.id.toolbar);
        etPhone = findView(R.id.et_phone);
        GrowingIO.getInstance().trackEditText(etPhone);
        etPassword = findView(R.id.et_password);
        GrowingIO.getInstance().trackEditText(etPassword);
        btnLogin = findView(R.id.btn_login);
        wxLogin = findView(R.id.img_wx_loging);
        qqLogin = findView(R.id.img_qq_loging);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            moveTaskToBack(true);
//            return true;
//        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        //TODO:test
        if (false) {
            Intent intent = new Intent(this, AddUserInfoActivity.class);
            startActivity(intent);
            return;
        }


        switch (v.getId()) {
            case R.id.img_wx_loging:
            case R.id.img_qq_loging:
            case R.id.btn_login:
            case R.id.btn_register:
                if (captchaIsShowing) {
                    return;
                }
                nextStepId = v.getId();
                if (cleckLoginTimeNeedCaptcha()) {
                    captchaIsShowing = true;
                    mCaptcha.start();
                    //简单测试是否填写captcha 是否初始化，包括captchaId与设置监听对象
                    if (mCaptcha.checkParams()) {
                        if (this == null || this.isDestroyed() || this.isFinishing()) {
                            return;
                        }
                        mCaptcha.Validate();
                    } else {
                        nextStep();
                    }
                } else {
                    nextStep();
                }
                break;
            case R.id.tv_agreement:
                openWebView(BaseUrl.USER_AGREEMENT);
                break;
            case R.id.btn_forget:
                UIHelper.showForgetPswAct(this);
                break;
            default:

        }
    }

    private void openWebView(String url) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        Intent intent = new Intent(this, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    private void showDialog(String tip, boolean cancelable, DialogInterface.OnDismissListener listener) {
        getDialogManager().showProgressDialog(LoginActivity.this, tip, cancelable, cancelable, listener);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLoginFail(String error) {
        LogUtil.i("onLogin", error);
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
//        CoreManager.getCore(IAppInfoCore.class).checkBanned(true);
//        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
//        //这个接口是覆盖逻辑，而不是增量逻辑。即新的调用会覆盖之前的设置 -- 所以不用考虑登陆需要删除之前的alias
//        if (uid > 0) {
//            JPushHelper.getInstance().onAliasAction(this, uid + "", JPushHelper.ACTION_SET_ALIAS);
//        }
        // GrowingIO 账号密码埋点
        try {
            etPhone.clearFocus();
            etPassword.clearFocus();
            btnLogin.setFocusableInTouchMode(true);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.i("view is null");
        }
        getDialogManager().dismissDialog();
        finish();
        /**
         * MainActivity 里面登录成功会触发logout的回调，logout回调会跳回登录界面
         */
//        getHandler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                finish();
//            }
//        }, 1000);

    }

    @Override
    protected boolean needSteepStateBar() {
        return true;
    }

    private boolean checkEnableLogin() {
        return (etPhone != null && etPhone.getText() != null && !StringUtil.isEmpty(etPhone.getText().toString()))
                && (etPassword != null && etPassword.getText() != null && etPassword.getText().length() > 5);
    }


    /**
     * 10分钟内操作是否是要验证的
     *
     * @return
     */
    private boolean cleckLoginTimeNeedCaptcha() {
        long l = (Long) SpUtils.get(this, SpEvent.cleckLoginTime, 0L);
        long currentTimeMillis = System.currentTimeMillis();
        int i = 600000;
        if (BasicConfig.isDebug) {
            i = 60000;
        }
        if (currentTimeMillis - l > i) {
            SpUtils.put(this, SpEvent.cleckLoginTime, currentTimeMillis);
            return false;
        }
        return true;
    }

    //避免重复点击多吃执行
    private boolean captchaIsShowing = false;

    /**
     * 网易图形验证码初始化
     */
    private void initImgCheck() {
        //初始化验证码SDK相关参数，设置CaptchaId、Listener最后调用start初始化。
        if (mCaptcha == null) {
            mCaptcha = new Captcha(this);
        }
        mCaptcha.setCaptchaId(AppKey.getCaptchaId());
        mCaptcha.setCaListener(myCaptchaListener);
        //可选：开启debug
        mCaptcha.setDebug(false);
        //可选：设置超时时间
        mCaptcha.setTimeout(10000);
    }

    /**
     * 网易图形验证码操作回调
     */
    CaptchaListener myCaptchaListener = new CaptchaListener() {

        @Override
        public void onValidate(String result, String validate, String message) {
            captchaIsShowing = false;
            //验证结果，valiadte，可以根据返回的三个值进行用户自定义二次验证
            //验证成功
            if (validate.length() > 0) {
                validateStr = validate;
                nextStep();
            } else {
                SingleToastUtil.showToast(LoginActivity.this, "验证失败：" + message);
            }
        }

        @Override
        public void closeWindow() {
            //请求关闭页面
            captchaIsShowing = false;
        }

        @Override
        public void onError(String errormsg) {
            //出错
            captchaIsShowing = false;
        }

        @Override
        public void onCancel() {
            captchaIsShowing = false;
        }

        @Override
        public void onReady(boolean ret) {
            //该为调试接口，ret为true表示加载Sdk完成
        }

    };

    /**
     * 图形验证码验证成功后操作
     */
    private void nextStep() {
        switch (nextStepId) {
            case R.id.img_wx_loging:
                getDialogManager().showProgressDialog(this, "请稍后");
                CoreManager.getCore(IAuthCore.class).wxLogin(validateStr);
                break;
            case R.id.img_qq_loging:
                getDialogManager().showProgressDialog(this, "请稍后");
                CoreManager.getCore(IAuthCore.class).qqLogin(validateStr);
                break;
            case R.id.btn_login:
                if (etPhone.getText().toString().length() == 0) {
                    toast("手机号码不能为空");
                    return;
                } else if (etPassword.getText().toString().length() < 6 || etPassword.getText().toString().length() > 12) {
                    return;
                }

                CoreManager.getCore(IAuthCore.class).login(etPhone.getText().toString(), etPassword.getText().toString(), validateStr);
                getDialogManager().showProgressDialog(this, "正在登录...");
                break;
            case R.id.btn_register:
                UIHelper.showRegisterAct(LoginActivity.this, validateStr);
                break;
            default:

        }
    }

    public class PhoneTextWatcherListener extends TextWatcherListener {

        @Override
        public void afterTextChanged(Editable editable) {
            btnLogin.setEnabled(checkEnableLogin());
        }
    }

    public class PasswordTextWatcherListener extends TextWatcherListener {
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.length() < 6 || charSequence.length() > 12) {
                if (!show) {
                    toast("密码长度6-12个字符");
                    show = true;
                }
            } else {
                show = false;
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            btnLogin.setEnabled(checkEnableLogin());
        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        if (etPhone != null) {
            etPhone.removeTextChangedListener(phoneTextWatcherListener);
        }
        if (etPassword != null) {
            etPassword.removeTextChangedListener(passwordTextWatcherListener);
        }
        if (mCaptcha != null) {
            mCaptcha.setCaListener(null);
        }

    }
}
