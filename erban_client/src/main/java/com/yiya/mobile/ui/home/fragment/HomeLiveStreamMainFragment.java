package com.yiya.mobile.ui.home.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.widget.LinearLayout;

import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.home.HomeLiveStreamMainPresenter;
import com.yiya.mobile.presenter.home.HomeLiveStreamMainView;
import com.yiya.mobile.ui.home.adpater.LiveStreamIndicatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Zhangsongzhou
 * @date 2019/5/7
 */

@CreatePresenter(HomeLiveStreamMainPresenter.class)
public class HomeLiveStreamMainFragment extends BaseMvpFragment<HomeLiveStreamMainView, HomeLiveStreamMainPresenter> implements HomeLiveStreamMainView, SwipeRefreshLayout.OnRefreshListener, OnRefreshListener {


    @BindView(R.id.home_yu_chat_indicator)
    MagicIndicator mMagicIndicator;
    @BindView(R.id.vp_home_content)
    ViewPager mViewPager;

    @BindView(R.id.srl_home_refresh)
    SmartRefreshLayout mSwipeRefreshLayout;


    private BaseIndicatorStateAdapter mIndicatorStateAdapter;
    private LiveStreamIndicatorAdapter mIndicatorAdapter;
    private CommonNavigator mCommonNavigator;

    private List<Fragment> mFragmentList = new ArrayList<>();
    private List<TabInfo> mTabInfoList = new ArrayList<>();

    private int mPosition = 0;
    private String mPositionStr = "";


    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_home_live_stream_main;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {
//        mSwipeRefreshLayout.setFooterHeight(0.1f);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setEnableLoadMore(false);
        mSwipeRefreshLayout.setEnableAutoLoadMore(false);
        mSwipeRefreshLayout.setEnableFooterTranslationContent(false);

    }

    @Override
    public void initiate() {
//        GrowingIO.getInstance().setPageName(this, "首页-直播");
    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        getData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mFragmentList.size() == 0) {
            getData();
        }
    }

    @Override
    public void onRefresh() {
        getData();
    }

    private void getData() {
        getMvpPresenter().getLiveStreamTagList();
    }

    @Override
    public void callbackRecommendTagList(List<TabInfo> data) {
        updateFragmentList(data);

    }

    @Override
    public void callbackRecommendTagListFail(String message) {
        updateFragmentList(null);
    }


    private void updateFragmentList(List<TabInfo> data) {

        for (Fragment fragment : mFragmentList) {
            fragment = null;
        }
        System.gc();

        mFragmentList.clear();
        mTabInfoList.clear();


        if (data != null) {
            for (TabInfo tabInfo : data) {
                HomeLiveStreamFragment itemFragment = new HomeLiveStreamFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("tabId", tabInfo.getId());
                itemFragment.setArguments(bundle);

                mFragmentList.add(itemFragment);
                mTabInfoList.add(tabInfo);
            }

            if (mIndicatorAdapter != null) {
                mIndicatorAdapter = null;
            }
            mIndicatorAdapter = new LiveStreamIndicatorAdapter(mContext, data);
            mIndicatorAdapter.setOnIndicatorSelectListener(new LiveStreamIndicatorAdapter.OnIndicatorSelectListener() {
                @Override
                public void onIndicatorSelect(TabInfo info, int position) {
                    mPosition = position;
                    mPositionStr = info.getName();
                    mViewPager.setCurrentItem(mPosition, true);
                }
            });
            if (mCommonNavigator != null) {
                mCommonNavigator = null;
            }
            mCommonNavigator = new CommonNavigator(mContext);
            mCommonNavigator.setAdapter(mIndicatorAdapter);
            mCommonNavigator.setAdjustMode(false);
            mMagicIndicator.setNavigator(mCommonNavigator);

            LinearLayout titleContainer = mCommonNavigator.getTitleContainer();
            titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);

            if (mIndicatorStateAdapter != null) {
                mIndicatorStateAdapter = null;
            }
            mIndicatorStateAdapter = new BaseIndicatorStateAdapter(getChildFragmentManager(), mFragmentList);
            mViewPager.setAdapter(mIndicatorStateAdapter);
            mViewPager.setOffscreenPageLimit(mFragmentList.size());

            ViewPagerHelper.bind(mMagicIndicator, mViewPager);

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    mPosition = i;
                    mPositionStr = mTabInfoList.get(i).getName();
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });

            if (!TextUtils.isEmpty(mPositionStr) && mPosition > 0) {
                for (TabInfo tabInfo : mTabInfoList) {
                    if (!TextUtils.isEmpty(tabInfo.getName()) && tabInfo.getName().equals(mPositionStr)) {
                        mViewPager.setCurrentItem(mTabInfoList.indexOf(tabInfo));
                        break;
                    }

                }
            }

        }


//        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.finishRefresh();
        mSwipeRefreshLayout.finishLoadmore();

    }


    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        getData();
    }

}
