package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment;
import com.yiya.mobile.ui.widget.LiveItemView;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.RoomAttentionInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/22
 */
public class HomeAttentionMainAdapter extends BaseMultiItemQuickAdapter<HomeAttentionMainFragment.HomeAttentionInfo, BaseViewHolder> {

    private Context mContext;


    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, HomeAttentionMainFragment.HomeAttentionInfo info, int pos, int index);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }


    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public HomeAttentionMainAdapter(List<HomeAttentionMainFragment.HomeAttentionInfo> data) {
        super(data);
        addItemType(HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ATTENTION_EMPTY, R.layout.item_home_chat_attention_empty);
        addItemType(HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ATTENTION, R.layout.item_home_live_room);
        addItemType(HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ROOM_HOT_TITLE, R.layout.item_home_live_room_hot);
        addItemType(HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_REC_LIST, R.layout.item_home_chat_rec_list);
        addItemType(HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ROOM, R.layout.item_home_live_room);

    }

    private int mItemRoomWidth = 0;

    public HomeAttentionMainAdapter(Context context, List<HomeAttentionMainFragment.HomeAttentionInfo> data) {
        this(data);
        mContext = context;
        mItemRoomWidth = (DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 30) - DisplayUtils.dip2px(mContext, 10)) / 2;
    }


    public void setData(List<HomeAttentionMainFragment.HomeAttentionInfo> data) {
        setNewData(data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeAttentionMainFragment.HomeAttentionInfo info) {
        switch (info.getItemType()) {
            case HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ATTENTION:  //关注列表
                handleAttention(helper, info);
                break;
            case HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ATTENTION_EMPTY:
                break;
            case HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ROOM_HOT_TITLE:  //为你推荐标题（可以不用处理）
                break;
            case HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_REC_LIST:    //为你推荐横向列表
                handleRecList(helper, info);
                break;
            case HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ROOM:    //底部房间列表
                handleRoomList(helper, info);
                break;
        }
    }

    /**
     * 处理关注列表
     *
     * @param helper
     * @param info
     */
    private void handleAttention(BaseViewHolder helper, HomeAttentionMainFragment.HomeAttentionInfo info) {

        ViewGroup.LayoutParams bgParams = helper.getView(R.id.item_bg_fl).getLayoutParams();
        bgParams.height = mItemRoomWidth;
        bgParams.width = mItemRoomWidth;
        helper.getView(R.id.item_bg_fl).setLayoutParams(bgParams);

        LiveItemView liveItemView = helper.getView(R.id.item_live_item_view);
        ViewGroup.LayoutParams layoutParams = liveItemView.getLayoutParams();
        layoutParams.width = mItemRoomWidth;
        layoutParams.height = mItemRoomWidth;
        liveItemView.setLayoutParams(layoutParams);

        liveItemView.setLocalStr(info.getAttention().getCity());
        liveItemView.setCountNum(info.getAttention().getOnlineNum());
        liveItemView.setNameStr(info.getAttention().getTitle());

        ImageLoadUtils.loadImage(mContext, info.getAttention().getAvatar(), liveItemView.getBgView());
        ImageLoadUtils.loadImage(mContext, info.getAttention().getTagPict(), liveItemView.getTagIv());

        helper.getView(R.id.item_bg_fl).setOnClickListener(v -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, info, getData().indexOf(info), -1);
            }
        });
    }

    /**
     * 处理为你推荐横向列表
     *
     * @param helper
     * @param info
     */
    private void handleRecList(BaseViewHolder helper, HomeAttentionMainFragment.HomeAttentionInfo info) {
        RecyclerView recyclerView = helper.getView(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        RecListRecyclerAdapter adapter = new RecListRecyclerAdapter(R.layout.item_home_chat_rec_item);
        adapter.setOnItemRecClickListener(new OnItemRecClickListener() {
            @Override
            public void onItemRecClick(View view, RoomAttentionInfo.RoomAttentionRecBean bean, int index) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, info, getData().indexOf(info), info.getRecBeanList().indexOf(bean));
                }
            }
        });
        recyclerView.setAdapter(adapter);
        adapter.setNewData(info.getRecBeanList());


    }

    public interface OnItemRecClickListener {
        void onItemRecClick(View view, RoomAttentionInfo.RoomAttentionRecBean bean, int index);
    }


    private class RecListRecyclerAdapter extends BaseQuickAdapter<RoomAttentionInfo.RoomAttentionRecBean, BaseViewHolder> {


        public RecListRecyclerAdapter(int layoutResId) {
            super(layoutResId);
        }

        private OnItemRecClickListener mListener;

        public void setOnItemRecClickListener(OnItemRecClickListener listener) {
            mListener = listener;
        }


        @Override
        protected void convert(BaseViewHolder helper, RoomAttentionInfo.RoomAttentionRecBean item) {
            ((TextView)(helper.getView(R.id.item_name_tv))).setText(item.getTitle());
            ImageLoadUtils.loadImage(mContext, item.getAvatar(), helper.getView(R.id.item_avatar_civ));
            helper.getView(R.id.item_avatar_civ).setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onItemRecClick(v, item, getData().indexOf(item));
                }
            });
            helper.getView(R.id.item_attention_btn).setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onItemRecClick(v, item, getData().indexOf(item));
                    v.setBackgroundResource(R.mipmap.icon_home_chat_rec_attention_pressed);
                }
            });
        }
    }

    /**
     * 处理底部房间item
     *
     * @param helper
     * @param info
     */
    private void handleRoomList(BaseViewHolder helper, HomeAttentionMainFragment.HomeAttentionInfo info) {
        ViewGroup.LayoutParams bgParams = helper.getView(R.id.item_bg_fl).getLayoutParams();
        bgParams.height = mItemRoomWidth;
        bgParams.width = mItemRoomWidth;
        helper.getView(R.id.item_bg_fl).setLayoutParams(bgParams);
        LiveItemView liveItemView = helper.getView(R.id.item_live_item_view);
        ViewGroup.LayoutParams layoutParams = liveItemView.getLayoutParams();
        layoutParams.width = mItemRoomWidth;
        layoutParams.height = mItemRoomWidth;
        liveItemView.setLayoutParams(layoutParams);


        ImageLoadUtils.loadImage(mContext, info.getRoomBean().getAvatar(), liveItemView.getBgView());
        ImageLoadUtils.loadImage(mContext, info.getRoomBean().getTagPict(), liveItemView.getTagIv());
        liveItemView.setLocalStr(info.getRoomBean().getCity());
        liveItemView.setCountNum(info.getRoomBean().getOnlineNum());
        liveItemView.setNameStr(info.getRoomBean().getNick());

        helper.getView(R.id.item_bg_fl).setOnClickListener(v -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, info, getData().indexOf(info), -1);
            }
        });


    }


}
