package com.yiya.mobile.ui.newfind.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment;
import com.yiya.mobile.ui.newfind.fragment.SproutingNewFragment;

import java.util.List;

public interface SproutingNewView extends IMvpBaseView {

    /***刷新或加载数据*/
    void onSucceed(int pageNum, List<SproutingNewFragment.SproutingNewinfo> data);

    /**异常停止刷新或加载*/
    void onFailed(String message);

}
