package com.yiya.mobile.ui.me.wallet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.ui.me.bills.activity.BillActivity;
import com.yiya.mobile.ui.me.bills.activity.WithdrawBillsActivity;
import com.yiya.mobile.ui.me.wallet.fragment.EarningsFragment;
import com.yiya.mobile.ui.me.wallet.fragment.RechargeFragment;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangsongzhou
 * @date 2019/5/5
 */
public class MyWalletActivity extends BaseActivity {

    public static void start(Context context,int count) {
        Intent intent = new Intent(context, MyWalletActivity.class);
        intent.putExtra("count",count);
        context.startActivity(intent);
    }

    @BindView(R.id.toolbar)
    AppToolBar mToolBar;
    @BindViews({R.id.indicator_recharge_tv, R.id.indicator_earnings_tv})
    List<TextView> mIndicatorTvs;

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    private List<Fragment> mFragmentList;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "我的钱包");
        setContentView(R.layout.activity_my_wallet_layout);
        ButterKnife.bind(this);

        count = getIntent().getIntExtra("count",0);
        mToolBar.setOnBackBtnListener(view -> finish());
        mToolBar.setOnRightImgBtnClickListener(new AppToolBar.OnRightImgBtnClickListener() {
            @Override
            public void onRightImgBtnClickListener() {
                if(count==1){
                    startActivity(new Intent(MyWalletActivity.this, BillActivity.class));
                }else if(count==2){
                    startActivity(new Intent(MyWalletActivity.this, WithdrawBillsActivity.class));
                }
            }
        });

        mFragmentList = new ArrayList<>();
        if(count==1){  //金币
            mFragmentList.add(new RechargeFragment());
            mToolBar.setTitle("我的金币");
        }else if(count==2){  //砖石
            mFragmentList.add(new EarningsFragment());
            mToolBar.setTitle("我的钻石");
        }
        mViewPager.setAdapter(new BaseIndicatorStateAdapter(getSupportFragmentManager(), mFragmentList));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                changeIndicator(mIndicatorTvs.get(i));
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


    }

    @OnClick(R.id.indicator_recharge_tv)
    void onClickRecharge(View view) {
        changeIndicator(view);
    }

    @OnClick(R.id.indicator_earnings_tv)
    void onClickEarnings(View view) {
        changeIndicator(view);
    }

    private void changeIndicator(View view) {
        for (TextView textView : mIndicatorTvs) {
            if (textView.getId() == view.getId()) {
                textView.setTextColor(Color.parseColor("#FFFFFF"));
                textView.setBackgroundResource(R.drawable.bg_wallet_my_selector);
            } else {
                textView.setTextColor(Color.parseColor("#FFDEE2"));
                textView.setBackground(null);
            }
        }

        int index = mIndicatorTvs.indexOf(view);
        if (index >= 0) {
            mViewPager.setCurrentItem(index, true);
        }

    }
}
