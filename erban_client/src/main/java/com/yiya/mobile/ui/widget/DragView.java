package com.yiya.mobile.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.juxiao.library_utils.DisplayUtils;

/**
 * ProjectName:
 * Description: 拖拽view
 * Created by BlackWhirlwind on 2019/6/20.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class DragView extends FrameLayout {

    private float mOriginalRawX;
    private float mOriginalRawY;
    private float mOriginalX;
    private float mOriginalY;
    protected int mScreenWidth;
    private int mScreenHeight;
    private int mStatusBarHeight;


    public DragView(Context context) {
        this(context, null);
    }

    public DragView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DragView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mStatusBarHeight = DisplayUtils.getStatusBarHeight(getContext());
        updateSize();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event == null) {
            return false;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                changeOriginalTouchParams(event);
                updateSize();
                break;
            case MotionEvent.ACTION_MOVE:
                updateViewPosition(event);
                break;
        }
        return true;
    }

    private void updateViewPosition(MotionEvent event) {
        //限制横向拖动区域
        float desX = mOriginalX + event.getRawX() - mOriginalRawX;
        if (desX < 0) {
            desX = 0;
        }
        if (desX > mScreenWidth) {
            desX = mScreenWidth;
        }
        setX(desX);
        //限制纵向拖动区域
        float desY = mOriginalY + event.getRawY() - mOriginalRawY;
        if (desY < mStatusBarHeight) {
            desY = mStatusBarHeight;
        }
        if (desY > mScreenHeight - getHeight()) {
            desY = mScreenHeight - getHeight();
        }
        setY(desY);
    }

    private void changeOriginalTouchParams(MotionEvent event) {
        mOriginalX = getX();
        mOriginalY = getY();
        mOriginalRawX = event.getRawX();
        mOriginalRawY = event.getRawY();
    }

    protected void updateSize() {
        mScreenWidth = DisplayUtils.getScreenWidth(getContext()) - this.getWidth();
        mScreenHeight = DisplayUtils.getScreenHeight(getContext());
    }

}
