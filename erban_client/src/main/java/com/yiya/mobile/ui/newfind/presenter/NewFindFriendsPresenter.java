package com.yiya.mobile.ui.newfind.presenter;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.tongdaxing.erban.BuildConfig;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.bean.HomeFriendsInfo;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;
import com.tongdaxing.xchat_core.bean.OnlinesFriendsBean;
import com.tongdaxing.xchat_core.bean.SquareLoopInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.home.RoomAttentionInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.yiya.mobile.model.find.FindSquareModel;
import com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment;
import com.yiya.mobile.ui.newfind.mode.NewFindFriendsMode;
import com.yiya.mobile.ui.newfind.view.NewFindFriendsView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_1;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_2;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_3;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_4;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_5;

public class NewFindFriendsPresenter extends AbstractMvpPresenter<NewFindFriendsView> {

    private NewFindFriendsMode newFindFriendsMode;
    private List<NewFindFriendsFragment.NewFindFriendsinfo> list = new ArrayList<>();
    private int pageNum,pageSize;


    public NewFindFriendsPresenter() {
        newFindFriendsMode = new NewFindFriendsMode();
    }

    /**关注好友房间列表*/
    public void getFriendsHomeList(int pageNum, int pageSize){
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        if(list.size()>0){
            list.removeAll(list);
        }
        Log.d("交友广场--在线好友","接口请求");
        newFindFriendsMode.getRoomAttentionList(new OkHttpManager.MyCallBack<ServiceResult<List<OnlinesFriendsBean>>>() { //ServiceResult<RoomAttentionInfo>
            @Override
            public void onError(Exception e) {
                LogUtils.e(e.getMessage());
                if(getMvpView()!=null){
                    getMvpView().onFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<OnlinesFriendsBean>> response) {
                Log.d("交友广场--在线好友",""+response);
                if(response!=null){
                    if(response.getData()!=null){
                        List<NewFindFriendsFragment.NewFindFriendsinfo> FriendsHomeList = new ArrayList<>();
                        List<OnlinesFriendsBean> Olist = response.getData();
                        Log.d("交友广场--在线好友","response.getData()="+Olist.size());
                        //判断是否有在线好友
                        if(Olist.size()>0){
                            //添加正在玩文本
                            NewFindFriendsFragment.NewFindFriendsinfo newFindFriendsinfo1 = new NewFindFriendsFragment.NewFindFriendsinfo();
                            newFindFriendsinfo1.setItemType(ITEM_TYPE_1);
                            FriendsHomeList.add(newFindFriendsinfo1);
                            //添加头像列表
                            NewFindFriendsFragment.NewFindFriendsinfo newFindFriendsinfo2 = new NewFindFriendsFragment.NewFindFriendsinfo();
                            newFindFriendsinfo2.setOnlinesFriendsBeanList(Olist);
                            newFindFriendsinfo2.setItemType(ITEM_TYPE_2);
                            FriendsHomeList.add(newFindFriendsinfo2);
                        }
                        list.addAll(FriendsHomeList);
                    }
                }
                getPublicChatList();
                //publicTitle();
            }
        });
    }

    /**获取公聊大厅数据*/
    private void getPublicChatList(){  //SquareLoopInfo
        newFindFriendsMode.getpublicChat(BuildConfig.DEBUG ? FindSquareModel.DEBUG_ROOM_ID : FindSquareModel.RELEASE_ROOM_ID, new OkHttpManager.MyCallBack<String>() {
            @Override
            public void onError(Exception e) {
                LogUtils.e("交友广场---公聊大厅error:"+e.getMessage());
                getMvpView().onFailed(e.getMessage());
            }

            @Override
            public void onResponse(String response) {// SquareLoopInfo
                Log.d("交友广场---公聊大厅",""+response.toString());
                List<NewFindFriendsFragment.NewFindFriendsinfo> PublicChatList = new ArrayList<>();
                try{
                    JSONObject js = new Json(response);
                    if(js.getInt("code")==200){
                        JSONObject je = js.getJSONObject("data");
                        JSONArray ja = je.getJSONArray("his_list");
                        List<SquareLoopInfo> Slist = new ArrayList<>();
                        for (int i = 0; i < ja.length(); i++) {
                            JSONObject jc = ja.getJSONObject(i);
                            SquareLoopInfo squareLoopInfo = new Gson().fromJson(jc.toString(),SquareLoopInfo.class);
                            Log.d("交友广场---公聊大厅","数据解析成功:"+squareLoopInfo.getRoom_id());
                            Slist.add(squareLoopInfo);
                        }
                        NewFindFriendsFragment.NewFindFriendsinfo newFindFriendsinfo3 = new NewFindFriendsFragment.NewFindFriendsinfo();
                        newFindFriendsinfo3.setItemType(ITEM_TYPE_3);
                        newFindFriendsinfo3.setSquareLoopInfoList(Slist);
                        PublicChatList.add(newFindFriendsinfo3);
                    }else{
                       LogUtils.e(js.getInt("code")+"");
                        NewFindFriendsFragment.NewFindFriendsinfo newFindFriendsinfo3 = new NewFindFriendsFragment.NewFindFriendsinfo();
                        newFindFriendsinfo3.setItemType(ITEM_TYPE_3);
                        PublicChatList.add(newFindFriendsinfo3);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                list.addAll(PublicChatList);
                getFindFriendList(pageNum,pageSize);
            }
        });
    }

    /***获取交友广场列表*/
    public void getFindFriendList(int pageNum,int pageSize){
        newFindFriendsMode.getFindFriendList(pageNum, pageSize, new OkHttpManager.MyCallBack<ServiceResult<List<HomeYuChatListInfo>>>() {  //ServiceResult<List<HomeRoom>>
            @Override
            public void onError(Exception e) {
                LogUtils.e("交友广场---交友广场列表error:"+e.getMessage());
                getMvpView().onFailed(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<HomeYuChatListInfo>> response) {
                Log.d("交友广场--交友广场列表",""+response);
                if(response!=null){
                    if(response.getData()!=null){
                        Log.d("交友广场--交友广场列表","response.getData():"+response.getData().size());
                        List<NewFindFriendsFragment.NewFindFriendsinfo> FindFriendList = new ArrayList<>();
                        List<HomeYuChatListInfo> Hlist = response.getData();
                        //判断列表数据是否为空
                        if(Hlist.size()>0){
                            //显示交友广场标题
                            NewFindFriendsFragment.NewFindFriendsinfo newFindFriendsinfo4 = new NewFindFriendsFragment.NewFindFriendsinfo();
                            newFindFriendsinfo4.setItemType(ITEM_TYPE_4);
                            FindFriendList.add(newFindFriendsinfo4);
                            for (HomeYuChatListInfo homeYuChatListInfo : Hlist) {
                                Log.d("交友广场--交友广场列表","homeRoom:"+homeYuChatListInfo.toString());
                                //添加交友广场列表数据
                                NewFindFriendsFragment.NewFindFriendsinfo newFindFriendsinfo5 = new NewFindFriendsFragment.NewFindFriendsinfo();
                                newFindFriendsinfo5.setHomeYuChatListInfo(homeYuChatListInfo);
                                newFindFriendsinfo5.setItemType(ITEM_TYPE_5);
                                FindFriendList.add(newFindFriendsinfo5);
                            }
                        }
                        list.addAll(FindFriendList);
                    }else{
                        getMvpView().onFailed("Error data");
                    }
                }
                getMvpView().onSucceed(pageNum,list);
            }
        });
    }

    /***上拉加载时，加载交友广场数据*/
    public void onLoad(int pageNum,int pageSize){
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        getFindFriendList(pageNum,pageSize);
    }

}
