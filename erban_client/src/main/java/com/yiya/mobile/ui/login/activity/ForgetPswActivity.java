package com.yiya.mobile.ui.login.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.utils.Logger;
import com.tongdaxing.xchat_core.utils.VerifyPhoneUtils;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by zhouxiangfeng on 17/3/5.
 */
public class ForgetPswActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    public static void start(Context context) {
        Intent intent = new Intent(context, ForgetPswActivity.class);
        context.startActivity(intent);
    }


    @BindView(R.id.et_phone)
    EditText mPhoneEdt;
    @BindView(R.id.et_code)
    EditText mCodeEdt;
    @BindView(R.id.btn_get_code)
    TextView mCodeGetBtn;
    @BindView(R.id.et_password)
    EditText mPwdEdt;
    @BindView(R.id.btn_modify)
    Button mSureBtn;


    private String errorStr;
    private static final String TAG = "RegisterActivity";
    private CodeDownTimer mCodeDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "忘记密码");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "重置密码页");
        setContentView(R.layout.activity_forget_psw_xc);
        ButterKnife.bind(this);
        onFindViews();
        onSetListener();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        checkEnable();
    }

    private void checkEnable() {
        if ((mPhoneEdt.getText() != null && mPhoneEdt.getText().length() == 11) &&
                (mCodeEdt.getText() != null && mCodeEdt.getText().length() == 5) &&
                (mPwdEdt.getText() != null && mPwdEdt.getText().length() > 5) /*&&
                (forgetPswBinding.confirmPw.getText() != null && forgetPswBinding.confirmPw.getText().length() > 5)*/) {
            mSureBtn.setEnabled(true);
        } else {
            mSureBtn.setEnabled(false);
        }
    }


    public void onFindViews() {
        mPhoneEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s) && s.length() >= 11) {
                    mCodeGetBtn.setBackgroundResource(R.drawable.shape_ff495c);
                } else {
                    mCodeGetBtn.setBackgroundResource(R.drawable.shape_t50_ff495c);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkEnable();
            }
        });
        mCodeEdt.addTextChangedListener(this);
        mPwdEdt.addTextChangedListener(this);
//        forgetPswBinding.confirmPw.addTextChangedListener(this);
    }

    public void onSetListener() {
        mCodeGetBtn.setOnClickListener(this::onClick);
        mSureBtn.setOnClickListener(this::onClick);

    }

    @OnClick(R.id.exit_fl)
    void onClickExit(View view) {
        finish();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onSmsFail(String error) {
        toast(error);
        Logger.error(TAG, "获取短信失败!");
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onModifyPsw() {

        // GrowingIO 账号密码埋点
        try {
            mCodeEdt.clearFocus();
            mPhoneEdt.clearFocus();
            mPwdEdt.clearFocus();
            mSureBtn.setFocusableInTouchMode(true);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.i("view is null");
        }

        toast("重置密码成功！");
        finish();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onModifyPswFail(String error) {
        toast(error);
        System.out.println("error===" + error);
    }

    @Override
    public void onClick(View v) {
        String phone = mPhoneEdt.getText().toString();
        switch (v.getId()) {
            case R.id.btn_modify:
                String psw = mPwdEdt.getText().toString();
                String confrim = psw;
                String smsCode = mCodeEdt.getText().toString();
                if (!StringUtils.isEmpty(smsCode)) {
                    if (isOK(phone, psw, confrim)) {
                        CoreManager.getCore(IAuthCore.class).requestResetPsw(phone, smsCode, psw);
                    } else {
                        toast(errorStr);
                    }
                } else {
                    toast("验证码不能为空！");
                }
                break;
            case R.id.btn_get_code:
//                if (phone.length() == VerifyPhoneUtils.PHONE_LENGTH && VerifyPhoneUtils.isMatch(phone)) {
                if (phone.length() == VerifyPhoneUtils.PHONE_LENGTH) {
                    mCodeDownTimer = new CodeDownTimer(mCodeGetBtn, 60000, 1000, new CodeDownTimer.OnTickListener() {
                        @Override
                        public void onTick(TextView textView, long time) {
                            textView.setClickable(false);
                            textView.setBackgroundColor(Color.TRANSPARENT);
                            textView.setTextColor(getResources().getColor(R.color.color_A8A8A8));
                            textView.setText(time / 1000 + "s");
                        }

                        @Override
                        public void onFinish(TextView textView) {
                            textView.setClickable(true);
                            textView.setText("重新获取");
                            textView.setTextColor(getResources().getColor(R.color.white));
                            textView.setBackgroundResource(R.drawable.shape_ff495c);
                        }
                    });
                    mCodeDownTimer.setColorId(getResources().getColor(R.color.color_A8A8A8));
                    mCodeDownTimer.start();
                    CoreManager.getCore(IAuthCore.class).requestSMSCode(phone, 3);
                } else {
                    toast("手机号码不正确");
                }
                break;
            default:
                break;
        }
    }

    private boolean isOK(String phone, String psw, String confirm) {
        if (StringUtils.isEmpty(psw) && psw.length() < 6) {
            errorStr = "请核对密码！";
            return false;
        }
        if (StringUtils.isEmpty(phone)) {
            errorStr = "请填写手机号码！";
            return false;
        }
        //秀场
        if (!psw.equals(confirm)) {
            errorStr = "新密码与确认密码不一致！";
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCodeDownTimer != null) {
            mCodeDownTimer.cancel();
        }
        if (mPhoneEdt != null) {
            mPhoneEdt.removeTextChangedListener(this);
        }
        if (mCodeEdt != null) {
            mCodeEdt.removeTextChangedListener(this);
        }
        if (mPwdEdt != null) {
            mPwdEdt.removeTextChangedListener(this);
        }
    }
}
