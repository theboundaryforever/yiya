package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;

import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import com.juxiao.library_ui.widget.DrawableTextView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/23
 */
public class YuChatIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mData;


    private OnIndicatorSelectListener mListener;

    public void setOnIndicatorSelectListener(OnIndicatorSelectListener listener) {
        mListener = listener;
    }

    public interface OnIndicatorSelectListener {
        void onIndicatorSelect(TabInfo info, int position);
    }


    public YuChatIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mContext = context;
        mData = titleList;
    }


    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public IPagerTitleView getTitleView(Context context, int index) {
        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.include_home_yu_chat_indicator);

        DrawableTextView titleName = pagerTitleView.findViewById(R.id.dtv_name);
        titleName.setText(mData.get(index).getName());

        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onSelected(int index, int totalCount) {
                titleName.setTextColor(mContext.getResources().getColor(R.color.color_FF495C));
                titleName.changeSoildColor(ContextCompat.getColor(mContext, R.color.color_ffebed));
            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onDeselected(int index, int totalCount) {
                titleName.setTextColor(mContext.getResources().getColor(R.color.topbar_line_color));
                titleName.changeSoildColor(Color.TRANSPARENT);
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {

            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {

            }
        });

        pagerTitleView.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onIndicatorSelect(mData.get(index), index);
            }
        });
        return pagerTitleView;
    }

    @Override
    public IPagerIndicator getIndicator(Context context) {
        return null;
    }
}
