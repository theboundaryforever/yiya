package com.yiya.mobile.ui.login.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.utils.Logger;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.yiya.mobile.ui.web.CommonWebViewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangsongzhou
 * @date 2019/7/31
 */
public class VerificationLoginActivity extends BaseActivity {

    private static final String TAG = "VerificationLogin";

    public static void start(Context context, String validateStr) {
        Intent intent = new Intent(context, VerificationLoginActivity.class);
        intent.putExtra("validateStr", validateStr);
        context.startActivity(intent);
    }

    @BindView(R.id.code_edt)
    EditText mCodeEdt;
    @BindView(R.id.phone_edt)
    EditText mPhoneEdt;
    @BindView(R.id.code_get_tv)
    TextView mCodeGetTv;

    @BindView(R.id.next_btn)
    Button mNextBtn;

    private String mValidateStr = "";

    private CodeDownTimer mCodeDownTimer;

    private boolean isResume = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_login);
        ButterKnife.bind(this);
        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        isResume = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isResume = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCodeDownTimer != null) {
            mCodeDownTimer.cancel();
            mCodeDownTimer = null;
        }
    }

    private void checkEnable() {

        if (!TextUtils.isEmpty(mPhoneEdt.getText().toString())) {
            if (mPhoneEdt.getText().toString().length() > 10) {
                mCodeGetTv.setEnabled(true);
                if (!TextUtils.isEmpty(mCodeEdt.getText().toString())) {
                    if (mCodeEdt.getText().toString().length() > 4) {
                        mNextBtn.setEnabled(true);
                    } else {
                        mNextBtn.setEnabled(false);
                    }
                } else {
                    mNextBtn.setEnabled(false);
                }
            } else {
                mNextBtn.setEnabled(false);
                mCodeGetTv.setEnabled(false);
            }
        } else {
            mNextBtn.setEnabled(false);
            mCodeGetTv.setEnabled(false);
        }

    }


    @OnClick(R.id.exit_fl)
    void onClickExit(View view) {
        finish();
    }

    @OnClick(R.id.next_btn)
    void onClickNext(View view) {
        String phone = mPhoneEdt.getText().toString();
        String smsCode = mCodeEdt.getText().toString();
        CoreManager.getCore(IAuthCore.class).phoneLogin(phone, smsCode);
        getDialogManager().showProgressDialog(this, "正在登录...");

    }

    @OnClick(R.id.code_get_tv)
    void onClickCodeGet(View view) {
        String phone = mPhoneEdt.getText().toString();

        if (!TextUtils.isEmpty(phone) && phone.length() == 11) {
            mCodeDownTimer = new CodeDownTimer(mCodeGetTv, 60 * 1000, 1000, new CodeDownTimer.OnTickListener() {
                @Override
                public void onTick(TextView textView, long time) {
                    textView.setEnabled(false);
                    /*textView.setTextColor(getResources().getColor(R.color.color_A8A8A8));*/
                    textView.setText(time / 1000 + "s");
                }

                @Override
                public void onFinish(TextView textView) {
                    textView.setEnabled(true);
                    textView.setText("重新获取");
                    /*textView.setTextColor(getResources().getColor(R.color.white));*/
                    //textView.setBackgroundResource(R.drawable.shape_ff495c);
                }
            });
            mCodeDownTimer.setColorId(getResources().getColor(R.color.color_A8A8A8));
            mCodeDownTimer.start();
            CoreManager.getCore(IAuthCore.class).requestSMSCode(phone, 4);
        } else {
            toast("请输入正确的手机号码");
        }
    }


    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        getDialogManager().dismissDialog();
        if (accountInfo != null && !TextUtils.isEmpty(accountInfo.getNewRegisterTip())) {
            toast(accountInfo.getNewRegisterTip());
        }
        finish();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLoginFail(String error) {
        LogUtil.i("onLogin", error);
        getDialogManager().dismissDialog();
        if (isResume) {
            toast(error);
        }
    }


    @OnClick(R.id.pwd_tv)
    void onClickPWDLogin(View view) {
        PasswordLoginActivity.start(this, mValidateStr);
    }

/*    @OnClick(R.id.not_get_code_tv)
    void onClickNOtGetCode(View view) {
        CommonWebViewActivity.start(this, BaseUrl.NOT_GET_SMS_CODE);
    }*/


    private void init() {

        mValidateStr = getIntent().getStringExtra("validateStr");

        mPhoneEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkEnable();
               /* if (!TextUtils.isEmpty(s) && s.length() >= 11) {
                    mCodeGetTv.setBackgroundResource(R.drawable.shape_ff495c);
                } else {
                    mCodeGetTv.setBackgroundResource(R.drawable.shape_t50_ff495c);

                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mCodeEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkEnable();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onSmsFail(String error) {
        toast(error);
        Logger.error(TAG, "获取短信失败!");
    }

    @OnClick(R.id.tv_help)
    public void toHelpH5() {
        CommonWebViewActivity.start(this, BaseUrl.HELP);
    }
}
