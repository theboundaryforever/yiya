package com.yiya.mobile.ui.home.adpater;

import android.content.Context;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/20
 * 描述        ${}
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class FindImageIndicatorAdapter extends CommonImageIndicatorAdapter {

    public FindImageIndicatorAdapter(Context context, List<TabInfo> titleList) {
        super(context, titleList);
    }

    @Override
    protected int getDrawable(int tabId) {
        return R.drawable.icon_find_family_indicator_tag;
    }
}
