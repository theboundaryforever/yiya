package com.yiya.mobile.ui.me.withdraw;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.presenter.withdraw.IWithdrawView;
import com.yiya.mobile.presenter.withdraw.WithdrawPresenter;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.withdraw.bean.RefreshInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * 文件描述：绑定微信提现页面
 *
 * @auther：zwk
 * @data：2019/2/15
 */
@CreatePresenter(WithdrawPresenter.class)
public class BinderWeixinPayActivity extends BaseMvpActivity<IWithdrawView, WithdrawPresenter> implements IWithdrawView, View.OnClickListener {
    private AppToolBar mToolBar;
    private EditText etPhone;
    private EditText etCode;
    private TextView tvCode;
    private Button btnCommit;
    private Platform wechat;
    private CodeDownTimer timer;


    public static void start(Context context, WithdrawInfo withdrawInfo) {
        Intent intent = new Intent(context, BinderWeixinPayActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putSerializable("withdrawInfo", withdrawInfo);
        intent.putExtras(mBundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binder_weixin_pay);
        initView();
        initClickListener();
    }

    private void initView() {
        mToolBar = (AppToolBar) findViewById(R.id.toolbar);
        etPhone = (EditText) findViewById(R.id.et_phone_number);
        GrowingIO.getInstance().trackEditText(etPhone);
        etCode = (EditText) findViewById(R.id.et_smscode);
        GrowingIO.getInstance().trackEditText(etCode);
        tvCode = (TextView) findViewById(R.id.btn_get_code);
        btnCommit = (Button) findViewById(R.id.btn_binder);

        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();

        if (userInfo != null && !TextUtils.isEmpty(userInfo.getPhone())) {
            etPhone.setText(userInfo.getPhone());
            try {
                etPhone.setSelection(etPhone.getText().length());
            } catch (Exception e) {

            }
        }
        btnCommit.setEnabled(false);
    }


    private void initClickListener() {
        mToolBar.setOnBackBtnListener(view -> finish());
        tvCode.setOnClickListener(this);
        btnCommit.setOnClickListener(this);

        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!TextUtils.isEmpty(tvCode.getText().toString()) && !TextUtils.isEmpty(s.toString())) {
                    btnCommit.setEnabled(true);
                } else {
                    btnCommit.setEnabled(false);
                }

            }
        });
        etCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(etPhone.getText().toString()) && !TextUtils.isEmpty(s.toString())) {
                    btnCommit.setEnabled(true);
                } else {
                    btnCommit.setEnabled(false);
                }

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_get_code:
                String phone = etPhone.getText().toString();
                if (StringUtil.isEmpty(phone)) {
                    toast("手机号码不能为空哦！");
                    return;
                }
                if (phone.length() < 11) {
                    toast("请填写完整的手机号码");
                    return;
                }
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                if (userInfo != null && StringUtils.isNotEmpty(userInfo.getPhone())) {

                    if (phone.equals(userInfo.getPhone()) || phone.contains(userInfo.getPhone().substring(5))) {//188992716
                        if (timer == null) {
                            timer = new CodeDownTimer(tvCode, 60000, 1000);
                            timer.setColorId(Color.WHITE);
                        }

                        timer.start();
                        getMvpPresenter().getInviteCode(CoreManager.getCore(IAuthCore.class).getCurrentUid(), CoreManager.getCore(IAuthCore.class).getTicket());
                    } else {
                        toast("填写的手机号与已绑定的手机号不一致！");
                    }
                }
                break;
            case R.id.btn_binder:
                String phone2 = etPhone.getText().toString();
                String code = etCode.getText().toString();
                if (StringUtil.isEmpty(phone2)) {
                    toast("手机号码不能为空哦！");
                    return;
                }
                if (StringUtil.isEmpty(code)) {
                    toast("邀请码不能为空哦！");
                    return;
                }
                getMvpPresenter().checkCode(CoreManager.getCore(IAuthCore.class).getCurrentUid(), CoreManager.getCore(IAuthCore.class).getTicket(), code);
                break;
        }
    }

    @Override
    public void onRemindToastError(String error) {
        toast(error);
    }

    @Override
    public void onRemindToastSuc() {
        toast("验证码获取成功！");
    }

    @Override
    public void onCheckCodeFailToast(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void onCheckCodeSucWeixinLogin() {
        getDialogManager().showProgressDialog(this, "加载中...");
        wechat = ShareSDK.getPlatform(Wechat.NAME);
        if (!wechat.isClientValid()) {
            toast("未安装微信");
            getDialogManager().dismissDialog();
            return;
        }
        if (wechat.isAuthValid()) {
            wechat.removeAccount(true);
        }
        wechat.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(final Platform platform, int i, HashMap<String, Object> hashMap) {
                if (i == Platform.ACTION_USER_INFOR) {
                    String openid = platform.getDb().getUserId();
                    String unionid = platform.getDb().get("unionid");
                    String access_token = platform.getDb().getToken();
//                    String nickName  = platform.get
                    getMvpPresenter().bindWithdrawWeixin(CoreManager.getCore(IAuthCore.class).getCurrentUid(), CoreManager.getCore(IAuthCore.class).getTicket(), access_token, openid, unionid);
                } else {
                    getDialogManager().dismissDialog();
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                getDialogManager().dismissDialog();
                toast("微信授权错误");
            }

            @Override
            public void onCancel(Platform platform, int i) {
                getDialogManager().dismissDialog();
                toast("取消微信授权");
            }
        });
        wechat.SSOSetting(false);
        wechat.showUser(null);
    }

    @Override
    public void onRemindBindWeixinSucFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void onRemindBindWeixinSucToast() {
        getDialogManager().dismissDialog();
        toast("绑定成功");
        EventBus.getDefault().post(new RefreshInfo());
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (wechat != null) {
            wechat.setPlatformActionListener(null);
        }
    }
}
