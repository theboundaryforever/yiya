package com.yiya.mobile.ui.me.wallet.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.ui.me.bills.activity.BillActivity;
import com.yiya.mobile.ui.me.wallet.adapter.ChargeAdapter;
import com.yiya.mobile.ui.me.wallet.presenter.ChargePresenter;
import com.yiya.mobile.ui.me.wallet.view.IChargeView;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.juxiao.library_ui.widget.AppToolBar;
import com.juxiao.library_utils.log.LogUtil;
import com.pingplusplus.android.Pingpp;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.count.IChargeClient;
import com.tongdaxing.xchat_core.initial.ClientConfigure;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.ChargeBean;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;

import static com.yiya.mobile.ui.me.wallet.activity.WalletActivity.isRefresh;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_HC;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_HJ;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_PPP;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_BANK;

/**
 * @author Zhangsongzhou
 * @date 2019/5/5
 */

@CreatePresenter(ChargePresenter.class)
public class RechargeFragment extends BaseMvpFragment<IChargeView, ChargePresenter> implements IChargeView, View.OnClickListener {

    private static final String TAG = RechargeFragment.class.getSimpleName();

    @BindView(R.id.toolbar)
    AppToolBar mToolBar;
    @BindView(R.id.tv_gold)
    TextView mTv_gold;
    @BindView(R.id.tv_number)
    TextView nTv_number;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_charge)
    Button btnCharge;
    private ChargeAdapter mChargeAdapter;
    private ChargeBean mSelectChargeBean;

    private WalletInfo mLastWalletInfo;
    private WalletInfo mNewWalletInfo;

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {
        setListener();
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.activity_charge;
    }

    public void initiate() {
//        GrowingIO.getInstance().setPageName(this, "我的钱包-充值");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "钱包充值页");
        mToolBar.setOnBackBtnListener(view -> finish());
        mToolBar.setOnRightImgBtnClickListener(new AppToolBar.OnRightImgBtnClickListener() {
            @Override
            public void onRightImgBtnClickListener() {
                startActivity(new Intent(mContext, BillActivity.class));
            }
        });
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        mChargeAdapter = new ChargeAdapter();
        mRecyclerView.setAdapter(mChargeAdapter);
        mChargeAdapter.setOnItemClickListener((baseQuickAdapter, view, position) -> {
            List<ChargeBean> list = mChargeAdapter.getData();
            if (ListUtils.isListEmpty(list)) {
                return;
            }
            mSelectChargeBean = list.get(position);
            int size = list.size();
            for (int i = 0; i < size; i++) {
                list.get(i).isSelected = position == i;
            }
            mChargeAdapter.notifyDataSetChanged();
            btnCharge.setEnabled(true);
        });
        getMvpPresenter().getChargeList();
    }

    private void setListener() {
        btnCharge.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_charge:
                if (mSelectChargeBean != null) {
                    UmengEventUtil.getInstance().onRechargeAmount(BasicConfig.INSTANCE.getAppContext(), mSelectChargeBean.chargeProdId, mSelectChargeBean.money);
                }
                getMvpPresenter().showChargeOptionsDialog();
                break;
            default:
                break;
        }
    }

    @Override
    public void setupUserInfo(UserInfo userInfo) {

    }

    @Override
    public void buildChargeList(List<ChargeBean> chargeBeanList) {
        if (chargeBeanList != null && chargeBeanList.size() > 0) {
            for (int i = 0; i < chargeBeanList.size(); i++) {
                ChargeBean chargeBean = chargeBeanList.get(i);
                chargeBean.isSelected = chargeBean.getMoney() == 48;
                if (48 == chargeBean.getMoney()) {
                    mSelectChargeBean = chargeBean;
                }
            }
            mChargeAdapter.setNewData(chargeBeanList);
        }
    }

    @Override
    public void getChargeListFail(String error) {
        toast(error);
        LogUtil.i("getChargeListFail", error);
    }

    @Override
    public void displayChargeOptionsDialog() {
        if (mSelectChargeBean == null) {
            return;
        }

        ClientConfigure clientConfigure = DemoCache.readClientConfigure();
        mGrowingCount = 0;
        ButtonItem buttonItem = null;
        ButtonItem buttonItem1 = null;
        if((clientConfigure != null) && (clientConfigure.getAlipayChannel() == 2)){//支付宝 1,ping++;2,汇潮
            buttonItem = new ButtonItem(getString(R.string.charge_alipay),
                    () -> {
                        if (!checkAliPayInstalled(mContext)){
                            toast("未安装支付宝");
                            return;
                        }
                        if (mSelectChargeBean == null) return;
                        mGrowingCount = 3;
                        getMvpPresenter().requestCharge(mContext, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_HC_ALIPAY, CHARGE_TYPE_HC);
                    });
        } else {
            buttonItem = new ButtonItem(getString(R.string.charge_alipay),
                    () -> {
                        if (mSelectChargeBean == null) return;
                        getMvpPresenter().requestCharge(mContext, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_ALIPAY, CHARGE_TYPE_PPP);
                    });
        }


        if ((clientConfigure != null) && (clientConfigure.getPayChannel() == 2)) {//微信 1,ping++;2,汇聚
            buttonItem1 = new ButtonItem(getString(R.string.charge_webchat),
                    () -> {
                        Platform wechat = ShareSDK.getPlatform(Wechat.NAME);
                        if (!wechat.isClientValid()) {
                            toast("未安装微信");
                            return;
                        }
                        if (mSelectChargeBean == null) return;
                        mGrowingCount = 3;
                        getMvpPresenter().requestCharge(mContext, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_HJ_WX, CHARGE_TYPE_HJ);
                    });
        } else {
            buttonItem1 = new ButtonItem(getString(R.string.charge_webchat),
                    () -> {
                        Platform wechat = ShareSDK.getPlatform(Wechat.NAME);
                        if (!wechat.isClientValid()) {
                            toast("未安装微信");
                            return;
                        }
                        if (mSelectChargeBean == null) return;
                        getMvpPresenter().requestCharge(mContext, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_WX, CHARGE_TYPE_PPP);
                    });
        }

        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(buttonItem);
        buttonItems.add(buttonItem1);

        if ((clientConfigure != null) && (clientConfigure.getJoinpay_mobilePay() == 1)){// 0关 1开
            ButtonItem buttonItem2 = new ButtonItem("银行卡支付",
                    () -> {
                        if (mSelectChargeBean == null) return;
                        getMvpPresenter().requestCharge(mContext, String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_WX, CHARGE_TYPE_BANK);
                    });
            buttonItems.add(buttonItem2);
        }

        getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel), false);
    }

    @Override
    public void getChargeOrOrderInfo(String data, boolean isHJ, boolean isHC) {
        if (data != null) {
            if (isHJ) {
                getMvpPresenter().joinPay(getActivity(), data);
            } else if (isHC){
                //汇潮支付
                try {
                    String payUrl = new Json(data).str("payUrl");
                    LogUtil.e("payUrl == " + payUrl);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("alipays://platformapi/startApp?appId=10000011&url=" + URLEncoder.encode(payUrl, "UTF-8")));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    toast("发起充值失败，请联系客服人员");
                }
            } else {
                Pingpp.createPayment(this, data);
            }
        }
    }

    @Override
    public void getChargeOrOrderInfo(String data, int payType) {
        if (data != null) {
            String url = null;
            switch (payType){
                case CHARGE_TYPE_PPP:
                    Pingpp.createPayment(this, data);
                    break;
                case CHARGE_TYPE_HC: //汇潮支付
                    try {
                        String payUrl = new Json(data).str("payUrl");
                        LogUtil.e("payUrl == " + payUrl);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("alipays://platformapi/startApp?appId=10000011&url=" + URLEncoder.encode(payUrl, "UTF-8")));
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        toast("发起充值失败，请联系客服人员");
                    }
                    break;
                case CHARGE_TYPE_HJ: //汇聚支付
                    getMvpPresenter().joinPay(getActivity(), data);
                    break;
                case CHARGE_TYPE_BANK:
                    try {
                        String urlData = new Json(data).str("payUrl");
                        LogUtil.e("urlData == " + urlData);
                        CommonWebViewActivity.start(getActivity(), urlData, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                        toast("发起充值失败，请联系客服人员");
                    }
                    break;
            }
        }
    }

    @Override
    public void getChargeOrOrderInfoFail(String error) {
        toast("发起充值失败" + error);
        LogUtil.i("getChargeOrOrderInfoFail", error);
    }

    @Override
    public void setupUserWalletBalance(WalletInfo walletInfo) {
        mTv_gold.setText(String.format(Locale.getDefault(), "%.1f", walletInfo.goldNum));
        nTv_number.setText(String.format(Locale.getDefault(), "%.1f", walletInfo.goldNum));
        //Growing 更新值
        mNewWalletInfo = walletInfo;
        if (mLastWalletInfo == null) {
            mLastWalletInfo = walletInfo;
        } else {
            if (mGrowingCount <= 0) {
                mLastWalletInfo = walletInfo;
            }
        }
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null) {
            mTv_gold.setText(getString(R.string.charge_gold, walletInfo.getGoldNum()));
            nTv_number.setText(getString(R.string.charge_gold, walletInfo.getGoldNum()));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: ");
        //支付页面返回处理
        if (requestCode == Pingpp.REQUEST_CODE_PAYMENT) {
            if (data != null && data.getExtras() != null) {
                String result = data.getExtras().getString("pay_result");
                if (result != null) {

                    /* 处理返回值
                     * "success" - 支付成功
                     * "fail"    - 支付失败
                     * "cancel"  - 取消支付
                     * "invalid" - 支付插件未安装（一般是微信客户端未安装的情况）
                     * "unknown" - app进程异常被杀死(一般是低内存状态下,app进程被杀死)
                     */
                    // 错误信息
                    String errorMsg = data.getExtras().getString("error_msg");
                    String extraMsg = data.getExtras().getString("extra_msg");
                    MLog.error(TAG, "errorMsg:" + errorMsg + "extraMsg:" + extraMsg);
                    CoreManager.notifyClients(IChargeClient.class, IChargeClient.chargeAction, result);
                    LogUtil.i("onResultFromPay", result);

                    if ("success".equals(result)) {
                        isRefresh = true;
                        //间隔1200毫秒请求数据
                        handler.sendMessageDelayed(handler.obtainMessage(), 1200);

                        if (mSelectChargeBean != null) {
                            handleGrowing(mSelectChargeBean.money);
                        }

                        toast("支付成功！");
                    } else if ("cancel".equals(result)) {
                        toast("支付被取消！");
                    } else {
                        toast("支付失败！");
                    }
                }
            }
        }
    }

    private WalletInfoHandler handler = new WalletInfoHandler(this);

    private static final int WHAT_GROWING = 2;

    private int mGrowingCount = 0;

    private static class WalletInfoHandler extends Handler {

        private WeakReference<RechargeFragment> mReference;

        WalletInfoHandler(RechargeFragment activity) {
            mReference = new WeakReference<>(activity);
        }


        @Override
        public void handleMessage(Message msg) {
            if (mReference == null || mReference.get() == null) {
                return;
            }

            if (msg.what == WHAT_GROWING) {//循环三次，进行更新值判断
                try {
                    double tmp = mReference.get().mNewWalletInfo.goldNum - mReference.get().mLastWalletInfo.goldNum;
                    if (tmp > 0) {
                        mReference.get().mLastWalletInfo = mReference.get().mNewWalletInfo;
                        mReference.get().mGrowingCount = 0;
                        mReference.get().handleGrowing((int) tmp);
                        mReference.get().toast("支付成功！");
                    } else {
                        if (--mReference.get().mGrowingCount > 0) {

                            mReference.get().getMvpPresenter().refreshWalletInfo(true);
                            sendEmptyMessageDelayed(WHAT_GROWING, 2 * 1000);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                mReference.get().getMvpPresenter().refreshWalletInfo(true);
            }


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getMvpPresenter().refreshWalletInfo(true);

        if (mGrowingCount > 0) {
            ClientConfigure clientConfigure = DemoCache.readClientConfigure();
            if (clientConfigure != null && clientConfigure.getPayChannel() == 2) {
                handler.sendEmptyMessage(WHAT_GROWING);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    /**
     * ------------------------ Growing
     */

    private void handleGrowing(int value) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("jine", value);
            GrowingIO.getInstance().track("chongzhichenggong", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean checkAliPayInstalled(Context context) {

        Uri uri = Uri.parse("alipays://platformapi/startApp");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        return componentName != null;
    }
}
