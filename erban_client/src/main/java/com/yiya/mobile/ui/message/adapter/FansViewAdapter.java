package com.yiya.mobile.ui.message.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.user.bean.FansInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

/**
 * Created by chenran on 2017/10/2.
 */

public class FansViewAdapter extends BaseQuickAdapter<FansInfo, BaseViewHolder> {

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {

        void onItemClick(FansInfo fansInfo);

        void onAttentionBtnClick(FansInfo fansInfo);
    }

    public void setRylListener(OnItemClickListener onClickListener) {
        onItemClickListener = onClickListener;
    }

    public FansViewAdapter(List<FansInfo> fansInfoList) {
        super(R.layout.fans_list_item, fansInfoList);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final FansInfo fansInfo) {
        if (fansInfo == null) return;
        baseViewHolder.setText(R.id.tv_userName, fansInfo.getNick())
                .setVisible(R.id.view_line, baseViewHolder.getLayoutPosition() != getItemCount() - 1)
                .setOnClickListener(R.id.rly, v -> {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(fansInfo);
                    }
                })
                .setOnClickListener(R.id.attention_img, v -> {
                    if (onItemClickListener != null) {
                        onItemClickListener.onAttentionBtnClick(fansInfo);
                    }
                })
        ;
        TextView attention = baseViewHolder.getView(R.id.attention_img);
        boolean state = CoreManager.getCore(IIMFriendCore.class).isMyFriend(fansInfo.getUid() + "");
        attention.setSelected(state);
        fansInfo.setMyFriend(state);
        if(state){
            attention.setTextColor(mContext.getResources().getColor(R.color.color_555555));
            attention.setText("已关注");
        }else{
            attention.setTextColor(mContext.getResources().getColor(R.color.color_ff6a99));
            attention.setText("+关注");
        }
        ImageView avatar = baseViewHolder.getView(R.id.imageView);
        ImageLoadUtils.loadCircleImage(mContext, fansInfo.getAvatar(), avatar, R.drawable.nim_avatar_default);
    }
}
