package com.yiya.mobile.ui.me.setting.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.PreferencesUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.VersionsCoreClient;
import com.tongdaxing.xchat_core.user.bean.CheckUpdataBean;
import com.tongdaxing.xchat_core.utils.AppCacheUtils;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.me.MePresenter;
import com.yiya.mobile.ui.me.task.view.IMeView;
import com.yiya.mobile.ui.me.wallet.activity.BinderPhoneActivity;
import com.yiya.mobile.ui.me.wallet.activity.SetPasswordActivity;
import com.yiya.mobile.utils.UIHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by zhouxiangfeng on 2017/4/16.
 */
@CreatePresenter(MePresenter.class)
public class SettingActivity extends BaseMvpActivity<IMeView, MePresenter> implements View.OnClickListener, IMeView {

    @BindView(R.id.toolbar)
    AppToolBar mAppToolBar;
    @BindView(R.id.version_tv)
    TextView mVersionTv;

    private boolean isBindPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "设置");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "设置页");
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        mAppToolBar.setOnBackBtnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        CoreManager.getCore(VersionsCore.class).checkVersion();

        mVersionTv.setText("V" + BasicConfig.getLocalVersionName(getApplicationContext()));
    }

    @OnClick({R.id.binding_ll, R.id.scan_ll, R.id.clear_cache_ll, R.id.about_me_ll, R.id.user_agreement_ll, R.id.logout_tv, R.id.rly_password})
    void onClickView(View view) {
        switch (view.getId()) {
            case R.id.binding_ll:
                queryBindPhone(true);
                break;
            case R.id.clear_cache_ll:
                //清除缓存
                addDisposable(
                        Observable.just(getApplicationContext())
                                .subscribeOn(Schedulers.io())
                                .subscribe(AppCacheUtils::clearAppCache));
                toast("清除成功");
                break;
            case R.id.scan_ll:
                 //扫一扫
                UIHelper.showScanAct(this);
                break;
            case R.id.about_me_ll:
                startActivity(new Intent(getApplicationContext(), AboutActivity.class));
                break;
            case R.id.user_agreement_ll:
                UIHelper.showUsinghelp(this);
                break;
            case R.id.logout_tv:
                getDialogManager().showProgressDialog(SettingActivity.this, "正在退出...");
                String accessToken = CoreManager.getCore(IAuthCore.class).getCurrentAccount().getAccess_token();
                if (!TextUtils.isEmpty(accessToken)) {
                    CoreManager.getCore(IAuthCore.class).accLogout(accessToken, new HttpRequestCallBack<Json>() {

                        @Override
                        public void onFinish() {
                            super.onFinish();
                            CoreManager.getCore(IAuthCore.class).logout();
                            PreferencesUtils.setFristQQ(true);
                            getDialogManager().dismissDialog();
                            finish();

                        }

                        @Override
                        public void onSuccess(String message, Json response) {
                        }

                        @Override
                        public void onFailure(int code, String msg) {
                        }
                    });
                }

                break;
            case R.id.rly_password:
//                startActivity(new Intent(getApplicationContext(), ResetPWDActivity.class));
//                queryBindPhone(false);
                checkBindPhone();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rly_content://反馈
                startActivity(new Intent(getApplicationContext(), FeedbackActivity.class));
                break;
            case R.id.rl_msg_not_disturb:
                MessageNotDisturbActivity.start(this);
                break;
            case R.id.rly_binder://绑定手机
                queryBindPhone(true);
                break;
            case R.id.rly_password://修改密码
//                queryBindPhone(false);
                break;
            case R.id.rly_contact_us:
                UIHelper.openContactUs(this);
                break;
            case R.id.rly_help:
                UIHelper.showUsinghelp(this);
                break;
            case R.id.rly_update://关于我们
                startActivity(new Intent(getApplicationContext(), AboutActivity.class));
                break;
            case R.id.rly_check://检查更新
                CoreManager.getCore(VersionsCore.class).checkVersion();
                break;
            case R.id.rly_lab:
                startActivity(new Intent(getApplicationContext(), LabActivity.class));
                break;
            case R.id.btn_login_out://退出登录
////                JPushHelper.getInstance().onAliasAction(this,CoreManager.getCore(IAuthCore.class).getCurrentUid()+"",JPushHelper.ACTION_DELETE_ALIAS);
//                CoreManager.getCore(IAuthCore.class).logout();
//
//                PreferencesUtils.setFristQQ(true);
//                finish();
                break;
            case R.id.rly_blacklist://退出登录
//                startActivity(new Intent(getApplicationContext(), BlackListActivity.class));
                break;
            case R.id.rly_protocol:
                UIHelper.showUsinghelp(this);
                break;

        }
    }

    /**
     * 查询是否已经绑定手机
     *
     * @param isBindPhone true 绑定手机 false
     */
    private void queryBindPhone(boolean isBindPhone) {
        this.isBindPhone = isBindPhone;
        getDialogManager().showProgressDialog(this, "正在查询请稍后...");
        getMvpPresenter().isPhone();
    }

    private void checkBindPhone() {

        getDialogManager().showProgressDialog(this, "正在查询请稍后...");
        getMvpPresenter().checkPwdV2();
    }

    @Override
    public void callbackCheckPwdFail(String message) {
        getDialogManager().dismissDialog();
        toast(message);
    }

    /**
     * @param state 1:未绑定过手机号 2：绑定过手机，没有设置密码  3：修改密码
     */
    @Override
    public void callbackCheckPwd(int state) {
        getDialogManager().dismissDialog();
        switch (state) {
            case 1:
                startActivity(new Intent(this, ResetBindPhoneActivity.class));
                break;
            case 2:
                ResetPWDActivity.start(this);
                break;
            case 3:
                ResetPWDActivity.start(this);
                break;
        }
    }

    @Override
    public void onIsBindPhone() {
        if (isBindPhone) {
            getDialogManager().dismissDialog();
            Intent intent = new Intent(this, BinderPhoneActivity.class);
            intent.putExtra(BinderPhoneActivity.hasBand, BinderPhoneActivity.modifyBand);
            startActivity(intent);
        } else {
            getMvpPresenter().checkPwd();
        }
    }

    @Override
    public void onIsBindPhoneFail(String msg) {
        getDialogManager().dismissDialog();
        Intent intent = new Intent(this, BinderPhoneActivity.class);
        if (!isBindPhone) {
            intent.putExtra(BinderPhoneActivity.isSetPassword, true);
        }
        startActivity(intent);
    }

    @Override
    public void onIsSetPwd(boolean isSetPassWord) {
        getDialogManager().dismissDialog();
        SetPasswordActivity.start(this, isSetPassWord);

    }

    @Override
    public void onIsSetPwdFail(String msg) {
        getDialogManager().dismissDialog();
        toast(msg);
    }

//    @CoreEvent(coreClientClass = IAuthClient.class)
//    public void onIsPhone() {
//        getDialogManager().dismissDialog();
//
//        Intent intent = new Intent(this, BinderPhoneActivity.class);
//        intent.putExtra(BinderPhoneActivity.hasBand, BinderPhoneActivity.modifyBand);
//        startActivity(intent);
//    }
//
//    @CoreEvent(coreClientClass = IAuthClient.class)
//    public void onIsphoneFail(String error) {
//        getDialogManager().dismissDialog();
//        Intent intent = new Intent(this, BinderPhoneActivity.class);
//
//        startActivity(intent);
//    }

    @CoreEvent(coreClientClass = VersionsCoreClient.class)
    public void onVersionUpdataDialog(CheckUpdataBean checkUpdataBean) {

        if (checkUpdataBean == null)
            return;

        mVersionTv.setText(checkUpdataBean.getVersion());
        if (checkUpdataBean.getStatus() == 1) {
            toast("已经是最新版本");
            return;
        }


        AlertDialog alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)

                .setTitle("检测到有最新的版本是否更新")
                .setMessage(TextUtils.isEmpty(checkUpdataBean.getUpdateVersionDesc()) ? checkUpdataBean.getVersionDesc() : checkUpdataBean.getUpdateVersionDesc())
                .setPositiveButton("更新", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String download_url = checkUpdataBean.getDownloadUrl();
                        if (TextUtils.isEmpty(download_url)) {
                            download_url = "https://www.baidu.com/";
                        }
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(download_url));
                        startActivity(intent);

                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                }).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

}
