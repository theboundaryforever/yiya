package com.yiya.mobile.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

/**
 * <p> main tab 控件 </p>
 * Created by Administrator on 2017/11/14.
 */
public class MainTabNew extends FrameLayout {
    private static final int DEFAULT_COLOR = Color.parseColor("#333333");
    private int mTabIcon, mTabIconSelect;
    private int mTabtextColor, mTabTextSelectColor;
    private float mTabTextSize = 10;
    private String mTabText;
    private TextView tvContent;
    private ImageView ivContent;

    public MainTabNew(Context context) {
        this(context, null);
    }

    public MainTabNew(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainTabNew(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MainTab);
        mTabIcon = typedArray.getResourceId(R.styleable.MainTab_tab_icon, R.drawable.ic_main_tab_home);
        mTabTextSize = typedArray.getDimension(R.styleable.MainTab_tab_text_size, 10);
        mTabIconSelect = typedArray.getResourceId(R.styleable.MainTab_tab_icon_select, R.drawable.ic_main_tab_home_pressed);
        mTabtextColor = typedArray.getColor(R.styleable.MainTab_tab_text_color, DEFAULT_COLOR);
        mTabTextSelectColor = typedArray.getColor(R.styleable.MainTab_tab_text_color_select, DEFAULT_COLOR);
        mTabText = typedArray.getString(R.styleable.MainTab_tab_text);
        typedArray.recycle();

        tvContent = new TextView(context);
        FrameLayout.LayoutParams fl = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        tvContent.setPadding(3, 3, 3, 3);
        tvContent.setGravity(Gravity.CENTER);
        tvContent.setText(mTabText);
        tvContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTabTextSize);
        tvContent.setLayoutParams(fl);
        tvContent.setTextColor(mTabtextColor);
        Drawable mainTabHome = ContextCompat.getDrawable(getContext(), mTabIcon);
        //必须加上这句，否则不显示
        mainTabHome.setBounds(0, 0, mainTabHome.getMinimumWidth(), mainTabHome.getIntrinsicHeight());
        tvContent.setCompoundDrawables(null, mainTabHome, null, null);
//        tvContent.setCompoundDrawablePadding(ScreenUtil.dip2px(1));
        addView(tvContent);

        ivContent = new ImageView(context);
        FrameLayout.LayoutParams flImg = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, DisplayUtils.dip2px(context, 40));
        ivContent.setLayoutParams(flImg);
        ivContent.setImageResource(mTabIconSelect);
        addView(ivContent);


        select(false);
    }

    public void setIcon(int iconResId) {

//        ivContent.setImageResource(iconResId);
    }

    public void select(boolean select) {
//        setIcon(select ? mTabIconSelect : mTabIcon);
        tvContent.setVisibility(select ? GONE : VISIBLE);
        ivContent.setVisibility(select ? VISIBLE : GONE);

    }

}
