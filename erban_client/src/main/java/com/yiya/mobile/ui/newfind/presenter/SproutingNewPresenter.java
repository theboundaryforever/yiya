package com.yiya.mobile.ui.newfind.presenter;

import android.util.Log;

import com.tongdaxing.xchat_core.bean.SproutingNewInfo;
import com.tongdaxing.xchat_core.user.bean.NewUserBean;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.yiya.mobile.ui.newfind.fragment.SproutingNewFragment;
import com.yiya.mobile.ui.newfind.mode.SproutingNewModel;
import com.yiya.mobile.ui.newfind.view.SproutingNewView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.yiya.mobile.ui.newfind.fragment.SproutingNewFragment.SproutingNewinfo.ITEM_TYPE_1;
import static com.yiya.mobile.ui.newfind.fragment.SproutingNewFragment.SproutingNewinfo.ITEM_TYPE_2;

public class SproutingNewPresenter extends AbstractMvpPresenter<SproutingNewView> {

    private SproutingNewModel sproutingNewModel;

    public SproutingNewPresenter() {
        if(sproutingNewModel == null){
            sproutingNewModel = new SproutingNewModel();
        }
    }

    /**获取萌新列表数据*/
    public void getmengxinlist(int mPageNum,int mpageSize){
        sproutingNewModel.getmengxin(mPageNum, mpageSize, new OkHttpManager.MyCallBack<ServiceResult<SproutingNewInfo>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.e(e.getMessage());
                if(getMvpView()!=null){
                    getMvpView().onFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<SproutingNewInfo> response) {
                Log.d("萌新列表","response="+response);
                List<SproutingNewFragment.SproutingNewinfo> Slist = new ArrayList<>();
                if(response!=null){
                    if(response.getData()!=null){
                        SproutingNewInfo sproutingNewInfo = response.getData();
                        //判断是否有广告数据
                        if(sproutingNewInfo.getBanners().size()>0){
                            SproutingNewFragment.SproutingNewinfo sproutingNewinfo1 = new SproutingNewFragment.SproutingNewinfo();
                            sproutingNewinfo1.setItemType(ITEM_TYPE_1);
                            sproutingNewinfo1.setmList1(sproutingNewInfo.getBanners());
                            Slist.add(sproutingNewinfo1);
                        }
                        //判断是否有萌新数据
                        if(sproutingNewInfo.getNewUsers().size()>0){
                            for (NewUserBean newUserBean : sproutingNewInfo.getNewUsers()) {
                                SproutingNewFragment.SproutingNewinfo sproutingNewinfo2 = new SproutingNewFragment.SproutingNewinfo();
                                sproutingNewinfo2.setItemType(ITEM_TYPE_2);
                                sproutingNewinfo2.setNewUserBean(newUserBean);
                                Slist.add(sproutingNewinfo2);
                            }
                        }
                    }
                }
                //停止刷新或加载
                getMvpView().onSucceed(mPageNum,Slist);
            }
        });
    }

}
