package com.yiya.mobile.ui.widget.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/29.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class OKCancelV2Dialog extends BaseTransparentDialogFragment {

    @BindView(R.id.iv_img)
    ImageView ivImg;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;

    private String mTitle;
    private int mImgRes;
    private OnConfirmClickListener mListener;
    private String mContent;
    private String mLeftText;
    private String mRighttext;

    public static OKCancelV2Dialog newInstance() {
        return new OKCancelV2Dialog();
    }

    public OKCancelV2Dialog setTitle(String title) {
        mTitle = title;
        return this;
    }

    public OKCancelV2Dialog setImgRes(int imgRes) {
        mImgRes = imgRes;
        return this;
    }

    public OKCancelV2Dialog setContent(String content) {
        mContent = content;
        return this;
    }

    public OKCancelV2Dialog setLeftText(String text) {
        mLeftText = text;
        return this;
    }

    public OKCancelV2Dialog setRightText(String text) {
        mRighttext = text;
        return this;
    }

    public OKCancelV2Dialog setOnConfirmClickListener(OnConfirmClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_ok_cancel_v2;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!TextUtils.isEmpty(mTitle)) {
            tvTitle.setText(mTitle);
        }
        if (mImgRes != 0) {
            ivImg.setImageResource(mImgRes);
        }
        if (!TextUtils.isEmpty(mLeftText)) {
            tvCancel.setText(mLeftText);
        }
        if (!TextUtils.isEmpty(mRighttext)) {
            tvConfirm.setText(mRighttext);
        }
    }

    @OnClick({R.id.tv_cancel, R.id.tv_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_confirm:
                if (mListener != null) {
                    mListener.onConfirm();
                }
                dismiss();
                break;
        }
    }

    public interface OnConfirmClickListener {
        void onConfirm();
    }
}
