package com.yiya.mobile.ui.me.user.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author Zhangsongzhou
 * @date 2019/8/2
 */
public class SelectSexDialog extends BaseDialogFragment {


    private Unbinder unbinder;
    private int mGender = 0;  //1 :man  2：wom


    @BindView(R.id.sex_man_iv)
    ImageView mSexManIv;
    @BindView(R.id.sex_man_tv)
    TextView mSexManTv;
    @BindView(R.id.sex_wom_iv)
    ImageView mSexWomIv;
    @BindView(R.id.sex_wom_tv)
    TextView mSexWomTv;

    public static SelectSexDialog newInstance(int sexValue) {
        SelectSexDialog dialog = new SelectSexDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("sex", sexValue);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        // setup window and width
        View view = inflater.inflate(R.layout.dialog_select_sex, window.findViewById(android.R.id.content), false);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setGravity(Gravity.CENTER);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            mGender = bundle.getInt("sex");
            if (mGender > 0) {
                updateSelect(mGender);
            }
        }
    }

    @OnClick(R.id.sex_man_ll)
    void onClickMan(View view) {
        mGender = 1;
        updateSelect(mGender);
    }

    @OnClick(R.id.sex_wom_ll)
    void onClickWom(View view) {
        mGender = 2;
        updateSelect(mGender);
    }

    @OnClick(R.id.sure_btn)
    void onClickSure(View view) {
        if (mListener != null) {
            mListener.onContentClick(view.getId(), mGender);
        }

        dismiss();

    }

    private void updateSelect(int sexValue) {
        if (sexValue == 1) {
            mSexManIv.setImageResource(R.mipmap.icon_sex_man_pressed);
            mSexManTv.setTextColor(getResources().getColor(R.color.color_383950));


            mSexWomIv.setImageResource(R.mipmap.icon_sex_wom_normal);
            mSexWomTv.setTextColor(getResources().getColor(R.color.color_A8A8A8));
        } else {
            mSexManIv.setImageResource(R.mipmap.icon_sex_man_normal);
            mSexManTv.setTextColor(getResources().getColor(R.color.color_A8A8A8));


            mSexWomIv.setImageResource(R.mipmap.icon_sex_wom_pressed);
            mSexWomTv.setTextColor(getResources().getColor(R.color.color_383950));
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    private OnContentClickListener mListener;

    public void setOnContentClickListener(OnContentClickListener listener) {
        mListener = listener;
    }

    public interface OnContentClickListener {
        void onContentClick(int viewId, int sexValue);
    }
}
