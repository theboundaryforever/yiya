package com.yiya.mobile.ui.home.adpater;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.jetbrains.annotations.NotNull;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/3.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class HomeYuChatListAdapter extends BaseQuickAdapter<HomeYuChatListInfo, BaseViewHolder> {

    public HomeYuChatListAdapter() {
        super(R.layout.item_home_friends_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeYuChatListInfo item) {
        setHomeFriendsItem(helper, item);
    }

    private void setHomeFriendsItem(BaseViewHolder helper, HomeYuChatListInfo item) {
        String onlineCount = StringUtils.transformToW(item.getOnlineNum());

        String title = !TextUtils.isEmpty(item.getTitle()) ? item.getTitle() : item.getNick();
        helper.setText(R.id.tv_gift_name, title)
                .setText(R.id.tv_id, "ID:" + item.getErbanNo())
                .setText(R.id.tv_num, onlineCount + "人");
//                    .setGone(R.id.tv_state, item.isOnline());//本地写死“在线”
        //头像
        ImageLoadUtils.loadAvatar(mContext, item.getAvatar(), helper.getView(R.id.riv_avatar), true);
        //首标签
        ImageView ivTag = helper.getView(R.id.iv_tag);
        if (TextUtils.isEmpty(item.getTagPict())) {
            ivTag.setVisibility(View.GONE);
        } else {
            loadRatioImg(ivTag, item.getTagPict());
        }
        //次标签
        ImageView ivBadge = helper.getView(R.id.iv_badge);
        if (TextUtils.isEmpty(item.getBadge())) {
            ivBadge.setVisibility(View.GONE);
        } else {
            loadRatioImg(ivBadge, item.getBadge());
        }
        //声浪
        SVGAImageView svgaWave = helper.getView(R.id.svga_wave);
        if (svgaWave.getDrawable() != null) {
            if (!svgaWave.isAnimating()) {
                svgaWave.startAnimation();
            }
        } else {
            SVGAParser svgaParser = new SVGAParser(mContext);
            svgaParser.decodeFromAssets("microredwave.svga", new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                    svgaWave.setVideoItem(svgaVideoEntity);
                    if (!svgaWave.isAnimating()) {
                        svgaWave.startAnimation();
                    }
                }

                @Override
                public void onError() {
                    LogUtil.e("svgaWave onError");
                }
            });
        }
    }

    private void loadRatioImg(ImageView iv, String url) {
        GlideApp.with(mContext)
                .load(url)
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Drawable> target, boolean b) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable drawable, Object o, Target<Drawable> target, DataSource dataSource, boolean b) {
                        resetIvRatio(drawable, iv);
                        return true;
                    }
                })
                .into(iv);
    }

    private void resetIvRatio(Drawable drawable, ImageView iv) {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) iv.getLayoutParams();
        params.dimensionRatio = String.valueOf((float) drawable.getIntrinsicWidth() / drawable.getIntrinsicHeight());
        iv.setLayoutParams(params);
        iv.setImageDrawable(drawable);
        iv.setVisibility(View.VISIBLE);
    }

}
