package com.yiya.mobile.ui.widget.dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.yiya.mobile.room.avroom.activity.ShareFansActivity;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * @author xiaoyu
 * @date 2017/12/13
 */
public class ShareDialog extends BottomSheetDialog implements View.OnClickListener {
    public static final int SHARE_INVITE_FRIEND = 1;
    private Context context;
    private TextView tvName;
    private static final String TAG = "ShareDialog";
    private OnShareDialogItemClick onShareDialogItemClick;
    private boolean hasInvite = false;//是否有房间内邀请好友 默认无
    private int shareType = -1;
    private boolean shareRoom = false;// 是否分享房间

    public ShareDialog(Context context) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
    }

    public ShareDialog(Context context, boolean shareRoom) {
        super(context, R.style.ErbanBottomSheetDialog);
        this.context = context;
        this.shareRoom = shareRoom;
    }

    public void setOnShareDialogItemClick(OnShareDialogItemClick onShareDialogItemClick) {
        this.onShareDialogItemClick = onShareDialogItemClick;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_share);
        setCanceledOnTouchOutside(true);
        TextView mTitle = findViewById(R.id.title);
//        LinearLayout llTitle = findViewById(R.id.ll_share_title);
//        LinearLayout llInviteTitle = findViewById(R.id.ll_invite_share);
        int dialogHeight = 230;
//        if (shareType == SHARE_INVITE_FRIEND){
//            llInviteTitle.setVisibility(View.VISIBLE);
//            dialogHeight = 270;
//            mTitle.setVisibility(View.GONE);
//            llInviteTitle.setVisibility(View.GONE);
//        }
        tvName = (TextView) findViewById(R.id.tv_title);
        TextView tvWeixin = (TextView) findViewById(R.id.tv_weixin);
        TextView tvWeixinpy = (TextView) findViewById(R.id.tv_weixinpy);
        TextView tvQq = (TextView) findViewById(R.id.tv_qq);
        TextView tvQqZone = (TextView) findViewById(R.id.tv_qq_zone);

        // todo 暂时隐藏  4-18 邀请咿呀好友（邀请功能）
        LinearLayout llInvite = findViewById(R.id.ll_share2);
        if (!isHasInvite()) {
            llInvite.setVisibility(View.GONE);
        } else {
            llInvite.setVisibility(View.VISIBLE);
        }
//        LinearLayout llInvite = findViewById(R.id.ll_share2);
        //新增邀请好友
        TextView tvInvite = findViewById(R.id.tv_invite_friend);
        TextView tvCancel = (TextView) findViewById(R.id.tv_cancel);

        tvWeixin.setOnClickListener(this);
        tvWeixinpy.setOnClickListener(this);
        tvQq.setOnClickListener(this);
        tvQqZone.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvInvite.setOnClickListener(this);
        String textTitle = "分享至";
        mTitle.setText(textTitle);
        int gravity = Gravity.START;
        mTitle.setGravity(gravity);
        // todo 暂时隐藏  4/8
//        if (hasInvite){
//            llInvite.setVisibility(View.VISIBLE);
//        }
        FrameLayout bottomSheet = (FrameLayout) findViewById(android.support.design.R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(false);
            BottomSheetBehavior.from(bottomSheet).setPeekHeight(
                    hasInvite ? DisplayUtils.dip2px(context, 310) : DisplayUtils.dip2px(context, dialogHeight));
        }
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
    }

    public void setName(String name) {
        if (!StringUtils.isEmpty(name)) {
            tvName.setText(name);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_weixin:
                if (shareRoom) {
                    onShareRoom(ShareSDK.getPlatform(Wechat.NAME));
                } else {
                    handleClick(Wechat.NAME);
                }
                dismiss();
                break;
            case R.id.tv_weixinpy:
                if (shareRoom) {
                    onShareRoom(ShareSDK.getPlatform(WechatMoments.NAME));
                } else {
                    handleClick(WechatMoments.NAME);
                }
                dismiss();
                break;
            case R.id.tv_qq:
                if (shareRoom) {
                    onShareRoom(ShareSDK.getPlatform(QQ.NAME));
                } else {
                    handleClick(QQ.NAME);
                }
                dismiss();
                break;
            case R.id.tv_qq_zone:
                if (shareRoom) {
                    onShareRoom(ShareSDK.getPlatform(QZone.NAME));
                } else {
                    handleClick(QZone.NAME);
                }
                dismiss();
                break;
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_invite_friend:
                if (context == null)
                    return;
                context.startActivity(new Intent(context, ShareFansActivity.class));
                dismiss();
                break;
            default:
                break;
        }
    }

    private void handleClick(String name) {
        if (onShareDialogItemClick != null) {
            onShareDialogItemClick.onSharePlatformClick(ShareSDK.getPlatform(name));
        }
    }

    public void onShareRoom(Platform platform) {
        RoomInfo currentRoomInfo = AvRoomDataManager.get().getRoomInfo();
        IMChatRoomMember member = IMNetEaseManager.get().getCurrentChatMember();
        if (currentRoomInfo != null) {
            CoreManager.getCore(IShareCore.class).shareRoom(platform, member, currentRoomInfo.getUid(), currentRoomInfo.getTitle());
        }
    }

    public boolean isHasInvite() {
        return hasInvite;
    }

    public void setHasInvite(boolean hasInvite) {
        this.hasInvite = hasInvite;
    }

    public int getShareType() {
        return shareType;
    }

    public void setShareType(int shareType) {
        this.shareType = shareType;
    }

    public interface OnShareDialogItemClick {
        void onSharePlatformClick(Platform platform);
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
