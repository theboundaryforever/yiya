package com.yiya.mobile.ui.find.adapter;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.ReceiveRedPackageInfo;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/1/8
 */
public class PublicRedPackageAdapter extends BaseQuickAdapter<ReceiveRedPackageInfo, BaseViewHolder> {

    public PublicRedPackageAdapter() {
        super(R.layout.item_red_package_record);
    }

    @Override
    protected void convert(BaseViewHolder helper, ReceiveRedPackageInfo item) {
        helper.setText(R.id.tv_receive_rp_nick, item.getNick());
        helper.setText(R.id.tv_receive_rp_gold, item.getGetCoin()+item.getShowUnit());
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), helper.getView(R.id.iv_receive_rp_avatar), R.drawable.nim_avatar_default);
        if (getData().size() > 0 && (getData().size() - 1) == helper.getAdapterPosition()) {
            helper.getView(R.id.v_receive_rp_line).setVisibility(View.GONE);
        } else {
            helper.getView(R.id.v_receive_rp_line).setVisibility(View.VISIBLE);
        }
    }
}
