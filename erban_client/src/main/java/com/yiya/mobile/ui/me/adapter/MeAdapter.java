package com.yiya.mobile.ui.me.adapter;

import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.user.bean.MeInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yiya.mobile.ui.widget.FlexUserInfoView;
import com.yiya.mobile.ui.widget.HeadWearImageView;
import com.yiya.mobile.ui.widget.avatarStack.AvatarStackView;
import com.yiya.mobile.ui.widget.avatarStack.CrownAvatarStackAdapter;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/6/28.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MeAdapter extends BaseMultiItemQuickAdapter<MeInfo, BaseViewHolder> {

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public MeAdapter(List<MeInfo> data) {
        super(data);
        addItemType(MeInfo.TYPE_TOP, R.layout.item_me_main_top);
        addItemType(MeInfo.TYPE_GIFT, R.layout.item_me_main_gift);
        addItemType(MeInfo.TYPE_NAME_VERIFY, R.layout.item_me_main_common);
        addItemType(MeInfo.TYPE_BROADCASTER_VERIFY, R.layout.item_me_main_common);
        addItemType(MeInfo.TYPE_ABOUT,R.layout.item_line);
        addItemType(MeInfo.TYPE_FEEDBACK, R.layout.item_me_main_common);
        addItemType(MeInfo.TYPE_INVITE, R.layout.item_me_main_common);
        addItemType(MeInfo.TYPE_HOST_RECORD, R.layout.item_me_main_common);
        /*addItemType(MeInfo.TYPE_ABOUT, R.layout.item_me_main_common);*/
    }

    public void setData(List<MeInfo> data) {
        setNewData(data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MeInfo meInfo) {
        if (meInfo == null) {
            return;
        }
        //icon
        if (meInfo.getResDrawable() != -1) {
            helper.setImageResource(R.id.iv_icon, meInfo.getResDrawable());
        }
        //title
        if (!TextUtils.isEmpty(meInfo.getTitle())) {
            helper.setText(R.id.tv_title, meInfo.getTitle());
        }
        switch (meInfo.getItemType()) {
            case MeInfo.TYPE_TOP:
                setTypeTop(helper, meInfo);
                break;
            case MeInfo.TYPE_GIFT:
                //礼物贡献榜
               /* setTypeGift(helper, meInfo);*/
                break;
            case MeInfo.TYPE_HOST_RECORD:
                //实名验证
                setTypeNameVerify(helper, meInfo);
                break;
            case MeInfo.TYPE_BROADCASTER_VERIFY:
                //主播验证
                /*setTypeBroadcasterVerify(helper, meInfo);*/
                break;
//            case MeInfo.TYPE_FEEDBACK:
//            case MeInfo.TYPE_ABOUT:
//                break;
        }
    }

    private void setTypeTop(BaseViewHolder helper, MeInfo meInfo) {
        UserInfo userInfo = meInfo.getUserInfo();
        if (userInfo == null) {
            return;
        }
        //关注数
        String followNum = String.valueOf(userInfo.getFollowNum());
     /*   SpannableStringBuilder ssbFollow = new SpannableStringBuilder(String.format("关注  %s", followNum));
        ssbFollow.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.color_383950)), ssbFollow.length() - followNum.length(), ssbFollow.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssbFollow.setSpan(new AbsoluteSizeSpan(22, true), ssbFollow.length() - followNum.length(), ssbFollow.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/
        //粉丝数
        String fansNum = String.valueOf(userInfo.getFansNum());
/*        SpannableStringBuilder ssbFans = new SpannableStringBuilder(String.format("粉丝  %s", fansNum));
        ssbFans.setSpan(new ForegroundColorSpan(ContextCompat.getColor(mContext, R.color.color_383950)), ssbFans.length() - fansNum.length(), ssbFans.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssbFans.setSpan(new AbsoluteSizeSpan(22, true), ssbFans.length() - fansNum.length(), ssbFans.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/
        //等级
//        ImageView ivUserLevel = helper.getView(R.id.iv_user_level);
//        ImageView ivExperLevel = helper.getView(R.id.iv_exper_level);
//        ImageView ivCharmLevel = helper.getView(R.id.iv_charm_level);

//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        layoutParams.setMargins(10, 5, 10, 5);

        //勋章信息
        FlexUserInfoView flexUserInfoView = helper.getView(R.id.flex_user_view);
        flexUserInfoView.setData(userInfo);

        /*//用户等级
        if (ivUserLevel != null) {
            if (StringUtils.isNotEmpty(userInfo.getVideoRoomExperLevelPic())) {
                ivUserLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, userInfo.getVideoRoomExperLevelPic(), ivUserLevel);
            } else {
                ivUserLevel.setVisibility(View.GONE);
            }
        }
        // 财富等级
        if (ivExperLevel != null) {
            if (StringUtils.isNotEmpty(userInfo.getExperLevelPic())) {
                ivExperLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, userInfo.getExperLevelPic(), ivExperLevel);
            } else {
                ivExperLevel.setVisibility(View.GONE);
            }
        }

        // 魅力等级
        if (ivCharmLevel != null) {
            if (StringUtils.isNotEmpty(userInfo.getCharmLevelPic())) {
                ivCharmLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, userInfo.getCharmLevelPic(), ivCharmLevel);
            } else {
                ivCharmLevel.setVisibility(View.GONE);
            }
        }*/

        helper.setText(R.id.tv_gift_name, userInfo.getNick())
                .setText(R.id.tv_id, mContext.getString(R.string.me_user_id, userInfo.getErbanNo()))
                .setText(R.id.tv_attentions, followNum)
                .setText(R.id.tv_fans, fansNum)
                .addOnClickListener(R.id.tv_user_info)//主页
                .addOnClickListener(R.id.tv_attentions)//关注
                .addOnClickListener(R.id.tv_fans)//粉丝
                /*.addOnClickListener(R.id.tv_wallet)//钱包
                .addOnClickListener(R.id.tv_my_level)//等级*/
                .addOnClickListener(R.id.iv_room)//房间
                .addOnClickListener(R.id.tv_copy)//ID复制
                .addOnClickListener(R.id.tv_mission);//任务
              /*  .addOnClickListener(R.id.tv_shop);//商城*/
        HeadWearImageView ivAvatar = helper.getView(R.id.iv_avatar);
        ivAvatar.setAvatar(userInfo.getAvatar());
        ivAvatar.setHeadWear(userInfo.getHeadwearUrl());
    }

    private void setTypeGift(BaseViewHolder helper, MeInfo meInfo) {
        RankingXCInfo rankingXCInfo = meInfo.getRankingXCInfo();
        AvatarStackView aSv = helper.getView(R.id.avatar_stack_view);
        aSv.setAdapter(new CrownAvatarStackAdapter());
        if (rankingXCInfo == null || ListUtils.isListEmpty(rankingXCInfo.getList())) {
            aSv.setAvatarList(null);
            return;
        }
        Observable.fromIterable(rankingXCInfo.getList())
                .take(3)
                .subscribeOn(Schedulers.io())
                .map(RankingXCInfo.ListBean::getAvatar)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((avatarList, throwable) -> aSv.setAvatarList(avatarList));
    }

    // 实名认证
    private void setTypeNameVerify(BaseViewHolder helper, MeInfo meInfo) {
        String auditStr = "未验证";
        UserInfo userInfo = meInfo.getUserInfo();
        if (userInfo != null) {
            switch (userInfo.getAuditStatus()) {
                case -1:
                    auditStr = "未验证";
                    break;
                case 0:
                    auditStr = "审核中";
                    break;
                case 1:
                    auditStr = "已验证";
                    break;
                case 2:
                    auditStr = "未验证";
                    break;
            }
        }
        helper.setGone(R.id.tv_tips, true)
                .setText(R.id.tv_tips, auditStr)
                .setTextColor(R.id.tv_tips, userInfo != null && userInfo.getAuditStatus() == 1
                        ? ContextCompat.getColor(mContext, R.color.color_ff6a99)
                        : ContextCompat.getColor(mContext, R.color.color_ff6a99));
    }

    // 主播认证
    private void setTypeBroadcasterVerify(BaseViewHolder helper, MeInfo meInfo) {
        String anchorStr = "未验证";
        UserInfo userInfo = meInfo.getUserInfo();
        if (userInfo != null) {
            switch ((userInfo.getAnchorStatus())) {
                case 0:
                    anchorStr = "未验证";
                    break;
                case 1:
                    anchorStr = "审核中";
                    break;
                case 2:
                    anchorStr = "已验证";
                    break;
                case 3:
                    anchorStr = "审核中";
                    break;
                case 4:
                    anchorStr = "已验证";
                    break;
            }
        }
        helper.setGone(R.id.tv_tips, true)
                .setText(R.id.tv_tips, anchorStr)
                .setTextColor(R.id.tv_tips, userInfo != null && (userInfo.getAnchorStatus() == 4 || userInfo.getAnchorStatus() == 2)
                        ? ContextCompat.getColor(mContext, R.color.color_c0c1c8)
                        : ContextCompat.getColor(mContext, R.color.color_383950));
    }

//    // 主播记录
//    private void setBroadcasterRecord(BaseViewHolder helper, MeInfo meInfo) {
//        UserInfo userInfo = meInfo.getUserInfo();
//        if (userInfo != null) {
//            if((userInfo.getAnchorStatus() == 2) || (userInfo.getAnchorStatus() == 4)){// 已验证
////                helper.getView(R.id.cl_container).setVisibility(View.VISIBLE);
//                helper.getView(R.id.cl_container).setVisibility(View.GONE);
//            } else {
//                helper.getView(R.id.cl_container).setVisibility(View.GONE);
//            }
//        }
//    }
}
