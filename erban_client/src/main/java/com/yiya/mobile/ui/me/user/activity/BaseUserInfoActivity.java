package com.yiya.mobile.ui.me.user.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nim.uikit.common.util.sys.ClipboardUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.presenter.user.IUserInfoView;
import com.yiya.mobile.presenter.user.UserInfoPresenter;
import com.yiya.mobile.ui.home.adpater.HomeIndicatorAdapter;
import com.yiya.mobile.ui.me.user.fragment.GiftWallFragment;
import com.yiya.mobile.ui.me.user.fragment.UserInfoFragment;
import com.yiya.mobile.ui.me.user.fragment.UserPhotoFragment;
import com.yiya.mobile.ui.widget.EnableScrollViewPager;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.yiya.mobile.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@CreatePresenter(UserInfoPresenter.class)
public abstract class BaseUserInfoActivity extends BaseMvpActivity<IUserInfoView, UserInfoPresenter> implements IUserInfoView, HomeIndicatorAdapter.OnItemSelectListener {

    @BindView(R.id.view_pager)
    EnableScrollViewPager viewPager;
    @BindView(R.id.indicator)
    MagicIndicator indicator;
    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.tv_id)
    TextView tvId;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_follow_num)
    TextView tvFollowNum;
    @BindView(R.id.tv_fans_num)
    TextView tvFansNum;

    protected long mUserId;
    protected UserInfo mUserInfo;

    protected abstract int getLayoutResId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);

        initData();
    }

    protected void initData() {
        mUserId = getIntent().getLongExtra(UserInfo.KEY_USER_ID, 0);
        mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(mUserId, true);
        updateUI(mUserInfo);

        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(0, "资料"));
        tabInfoList.add(new TabInfo(1, "相册"));
        tabInfoList.add(new TabInfo(2, "礼物墙"));
        HomeIndicatorAdapter indicatorAdapter = new HomeIndicatorAdapter(this, tabInfoList);
        indicatorAdapter.setOnItemSelectListener(this);

        ArrayList<Fragment> list = new ArrayList<>();
        list.add(UserInfoFragment.newInstance(mUserInfo));
        list.add(UserPhotoFragment.newInstance(mUserInfo));
        list.add(GiftWallFragment.newInstance(mUserId));
        BaseIndicatorStateAdapter adapter = new BaseIndicatorStateAdapter(getSupportFragmentManager(), list);
        viewPager.setAdapter(adapter);

        CommonNavigator commonNavigator = new CommonNavigator(this);
        //默认等分
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(indicatorAdapter);
        indicator.setNavigator(commonNavigator);

        ViewPagerHelper.bind(indicator, viewPager);
    }

    private void updateUI(UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }
        ImageLoadUtils.loadAvatar(this, userInfo.getAvatar(), ivAvatar, true);
        tvName.setText(userInfo.getNick());
        tvId.setText("ID:" + userInfo.getErbanNo());
        tvFollowNum.setText(String.valueOf(userInfo.getFollowNum()));
        tvFansNum.setText(String.valueOf(userInfo.getFansNum()));
    }


    @Override
    public void onItemSelect(int position) {
        viewPager.setCurrentItem(position);
        int bgId = -1;
        switch (position) {
            case 0:
                bgId = R.drawable.bg_user_info_indicator_left;
                break;
            case 1:
                bgId = R.drawable.bg_user_info_indicator_mid;
                break;
            case 2:
                bgId = R.drawable.bg_user_info_indicator_right;
                break;
        }
        if (bgId != -1) {
            indicator.setBackgroundResource(bgId);
        }
    }

    @OnClick({R.id.iv_copy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_copy:
                CharSequence text = tvId.getText();
                if (!TextUtils.isEmpty(text)) {
                    ClipboardUtil.clipboardCopyText(this, text.subSequence(3, text.length()));
                    toast("复制成功");
                }
                break;
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == mUserId) {
            mUserInfo = info;
            updateUI(mUserInfo);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        if (info.getUid() == mUserId) {
            mUserInfo = info;
            updateUI(mUserInfo);
        }
    }
}
