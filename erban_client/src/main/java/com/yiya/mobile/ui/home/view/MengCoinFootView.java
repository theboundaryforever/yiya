package com.yiya.mobile.ui.home.view;


import android.content.Context;
import android.widget.LinearLayout;

import com.tongdaxing.erban.R;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/2/28
 */
public class MengCoinFootView extends LinearLayout {


    public MengCoinFootView(Context context) {
        super(context);
        inflate(context, R.layout.ly_meng_foot_radius,this);
    }
}
