package com.yiya.mobile.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.google.android.flexbox.FlexboxLayoutManager;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.user.bean.Medal;
import com.tongdaxing.xchat_core.user.bean.MultiInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yiya.mobile.ui.me.user.adapter.MedalAdapter;

import java.util.ArrayList;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/10/12.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class FlexUserInfoView extends RecyclerView {

    public FlexUserInfoView(@NonNull Context context) {
        this(context, null);
    }

    public FlexUserInfoView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FlexUserInfoView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initData();
    }

    private void initData() {
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        setLayoutManager(layoutManager);
    }

    public void setData(UserInfo userInfo) {
        MedalAdapter medalAdapter = new MedalAdapter();
        ArrayList<MultiInfo> list = new ArrayList<>();
        if (!TextUtils.isEmpty(userInfo.getExperLevelPic())) {
            list.add(new MultiInfo(MultiInfo.TYPE_PIC_URL)
                    .setPicUrl(userInfo.getExperLevelPic()));
        }
        if (!TextUtils.isEmpty(userInfo.getCharmLevelPic())) {
            list.add(new MultiInfo(MultiInfo.TYPE_PIC_URL)
                    .setPicUrl(userInfo.getCharmLevelPic()));
        }
        Medal medal = userInfo.getMedal();
        if (medal != null) {
            if (!TextUtils.isEmpty(medal.getPicUrl())) {
                list.add(new MultiInfo(MultiInfo.TYPE_PIC_URL)
                        .setPicUrl(medal.getPicUrl()));
            }
        }
        if (userInfo.getGender() != 0) {
            list.add(new MultiInfo(MultiInfo.TYPE_RES_ID)
                    .setResId(userInfo.getGender() == 1 ? R.drawable.ic_user_male : R.drawable.ic_user_female));
        }
        setAdapter(medalAdapter);
        medalAdapter.setNewData(list);
    }
}
