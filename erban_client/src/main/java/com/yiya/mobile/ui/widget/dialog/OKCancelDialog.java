package com.yiya.mobile.ui.widget.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/29.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class OKCancelDialog extends BaseTransparentDialogFragment {
    @BindView(R.id.iv_img)
    ImageView ivImg;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    private String mTitle;
    private int mImgRes;
    private OnConfirmClickListener mListener;

    public static OKCancelDialog newInstance() {
        return new OKCancelDialog();
    }

    public OKCancelDialog setTitle(String title) {
        mTitle = title;
        return this;
    }

    public OKCancelDialog setImgRes(int imgRes) {
        mImgRes = imgRes;
        return this;
    }

    public OKCancelDialog setOnConfirmClickListener(OnConfirmClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_ok_cancel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!TextUtils.isEmpty(mTitle)) {
            tvTitle.setText(mTitle);
        }
        if (mImgRes != 0) {
            ivImg.setImageResource(mImgRes);
        }
    }

    @OnClick({R.id.tv_cancel, R.id.tv_confirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_confirm:
                if (mListener != null) {
                    mListener.onConfirm();
                }
                dismiss();
                break;
        }
    }

    public interface OnConfirmClickListener {
        void onConfirm();
    }
}
