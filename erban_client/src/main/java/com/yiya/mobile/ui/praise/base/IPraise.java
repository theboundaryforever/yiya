package com.yiya.mobile.ui.praise.base;

public interface IPraise {

    /**
     * 转换成可以绘制的对象
     *
     * @return
     */
    IDrawable toDrawable();

}
