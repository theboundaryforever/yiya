package com.yiya.mobile.ui.me.shopping.adapter;

import android.view.View;

import com.yiya.mobile.base.bindadapter.BaseAdapter;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.databinding.ListItemShareFansBinding;


/**
 * Created by chenran on 2017/10/3.
 */

public class FriendListGiftAdapter extends BaseAdapter<NimUserInfo, ListItemShareFansBinding> {

    public FriendListGiftAdapter() {
        super(R.layout.list_item_share_fans);
    }

    public IGiveAction iGiveAction;

    @Override
    protected void convert(ListItemShareFansBinding binding, NimUserInfo item) {
        ImageLoadUtils.loadImage(mContext, item.getAvatar(), binding.imageView);
        String name = item.getName();
        String account = item.getAccount();
        binding.tvItemName.setText(name);
        binding.buInvite.setText("赠送");

        binding.itemIdTv.setVisibility(View.GONE);
        binding.buInvite.setOnClickListener(view -> {
            if (iGiveAction != null)
                iGiveAction.onGiveEvent(account, name);
        });
    }

    public interface IGiveAction {
        void onGiveEvent(String uid, String userName);
    }
}
