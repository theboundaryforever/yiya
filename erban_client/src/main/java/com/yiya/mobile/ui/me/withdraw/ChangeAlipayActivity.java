package com.yiya.mobile.ui.me.withdraw;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.presenter.withdraw.IWithdrawView;
import com.yiya.mobile.presenter.withdraw.WithdrawPresenter;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.juxiao.library_ui.widget.AppToolBar;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCoreClient;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * @author Zhangsongzhou
 * @date 2019/5/5
 */
@CreatePresenter(WithdrawPresenter.class)
public class ChangeAlipayActivity extends BaseMvpActivity<IWithdrawView, WithdrawPresenter> implements IWithdrawView {


    public static void start(Context context, WithdrawInfo withdrawInfo) {
        Intent intent = new Intent(context, ChangeAlipayActivity.class);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        ((Activity) context).startActivityForResult(intent, 1001);
    }


    private AppToolBar mToolBar;
    private EditText etAlipayAccount;
    private EditText etAlipayName;
    private EditText etSmsCode;
    private Button btnGetCode;
    private Button btnBinder;
    private Button btnBinderRquest;
    private CodeDownTimer timer;

    private TextWatcher textWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_binder_alipay);

        initView();
        initData();
        onSetListener();
    }

    private void onSetListener() {
        //获取绑定支付宝验证码
        btnGetCode.setOnClickListener(v -> {
            String phoneStr = etAlipayAccount.getText().toString();
            if (TextUtils.isEmpty(phoneStr)) {
                toast("手机号码不能为空哦！");
                return;
            }
            if (phoneStr.length() < 11) {
                toast("请填写完整的手机号码");
                return;
            }
            if (timer == null)
                timer = new CodeDownTimer(btnGetCode, 60000, 1000);
            timer.start();
            CoreManager.getCore(IWithdrawCore.class).getSmsCode(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        });

        //验证code 码
        btnBinderRquest.setOnClickListener(v -> {
            String codeStr = etSmsCode.getText().toString();

            if (StringUtil.isEmpty(etAlipayAccount.getText().toString())) {
                toast("手机号码不能为空哦！");
                return;
            }
            if (TextUtils.isEmpty(codeStr)) {
                toast("邀请码不能为空哦！");
                return;
            }
            getDialogManager().showProgressDialog(this, "加载中...");
            getMvpPresenter().checkCode(CoreManager.getCore(IAuthCore.class).getCurrentUid(), CoreManager.getCore(IAuthCore.class).getTicket(), codeStr);

        });
        //输入框监听改变
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etAlipayAccount.getText() != null && etAlipayAccount.getText().length() > 0
                        && etAlipayName.getText() != null && etAlipayName.getText().length() > 0
                        && etSmsCode.getText() != null && etSmsCode.getText().length() > 0) {
                    btnBinder.setVisibility(View.GONE);
                    btnBinderRquest.setVisibility(View.VISIBLE);
                } else {
                    btnBinder.setVisibility(View.VISIBLE);
                    btnBinderRquest.setVisibility(View.GONE);
                }
            }
        };

        etAlipayAccount.addTextChangedListener(textWatcher);
        etAlipayName.addTextChangedListener(textWatcher);
        etSmsCode.addTextChangedListener(textWatcher);
        mToolBar.setOnBackBtnListener(view -> finish());
    }

    private void initData() {
        WithdrawInfo info = (WithdrawInfo) getIntent().getSerializableExtra("withdrawInfo");
        RedPacketInfo redPacketInfos = (RedPacketInfo) getIntent().getSerializableExtra("redPacketInfo");
        try {
            if (info != null && !info.isNotBoundPhone) {
                etAlipayAccount.setText(info.alipayAccount);
                etAlipayName.setText(info.alipayAccountName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        etAlipayAccount = (EditText) findViewById(R.id.et_phone);
        GrowingIO.getInstance().trackEditText(etAlipayAccount);
        etAlipayName = (EditText) findViewById(R.id.et_name);
        GrowingIO.getInstance().trackEditText(etAlipayName);
        etSmsCode = (EditText) findViewById(R.id.et_smscode);
        GrowingIO.getInstance().trackEditText(etSmsCode);
        etSmsCode.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});
        btnGetCode = (Button) findViewById(R.id.btn_get_code);
        btnBinder = (Button) findViewById(R.id.btn_binder);
        btnBinderRquest = (Button) findViewById(R.id.btn_binder_request);
        mToolBar = (AppToolBar) findViewById(R.id.toolbar);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (etAlipayAccount != null) {
            etAlipayAccount.addTextChangedListener(textWatcher);
        }
        if (etAlipayName != null) {
            etAlipayName.addTextChangedListener(textWatcher);
        }
        if (etSmsCode != null) {
            etSmsCode.addTextChangedListener(textWatcher);
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetSmsCodeFail(String error) {
        toast(error);
    }

    @Override
    public void onRemindToastError(String error) {
        getDialogManager().dismissDialog();
        toast(error);

    }

    @Override
    public void onRemindToastSuc() {

    }

    @Override
    public void onCheckCodeFailToast(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void onCheckCodeSucWeixinLogin() {
        getDialogManager().dismissDialog();

        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString(WithdrawActivity.BUNDLE_KEY_ALIPAY_PHONE, etAlipayAccount.getText().toString());
        bundle.putString(WithdrawActivity.BUNDLE_KEY_ALIPAY_NAME, etAlipayName.getText().toString());
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onRemindBindWeixinSucFail(String error) {

    }

    @Override
    public void onRemindBindWeixinSucToast() {

    }
}
