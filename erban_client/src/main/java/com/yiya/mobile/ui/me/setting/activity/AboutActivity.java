package com.yiya.mobile.ui.me.setting.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.util.Locale;

public class AboutActivity extends BaseActivity {

    TextView versinos;
    TextView slogan;
    AppToolBar mToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "关于");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "关于页");
        setContentView(R.layout.activity_about);
        initView();
        initData();
    }

    private void initData() {
        versinos.setText(String.format(Locale.getDefault(), getString(R.string.about_version_name),
                BasicConfig.getLocalVersionName(getApplication())));
    }

    private void initView() {
        versinos = (TextView) findViewById(R.id.versions);
        slogan = (TextView) findViewById(R.id.about_slogan);
        mToolBar = (AppToolBar) findViewById(R.id.toolbar);
        mToolBar.setOnBackBtnListener(view -> finish());
    }
}
