package com.yiya.mobile.ui.me.setting.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_scan.Result;
import com.juxiao.library_scan.ResultHandler;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.me.MePresenter;
import com.yiya.mobile.ui.me.task.view.IMeView;
import com.yiya.mobile.view.ScanView;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;

/**
 * @author Zhangsongzhou
 * @date 2019/3/27
 */
@CreatePresenter(MePresenter.class)
public class ScanActivity extends BaseMvpActivity<IMeView, MePresenter> implements IMeView, View.OnClickListener {

    private static final String TAG = "ScanActivity";

    private AppToolBar mAppToolBar;
    private ScanView mScannerView;
    private LinearLayout mScanResultLl;
    private Button mScanLoginBtn, mScanCancelBtn;

    private String mScanResultCode = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "扫一扫");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "扫一扫页");
        setContentView(R.layout.activity_scan);

        mAppToolBar = (AppToolBar) findViewById(R.id.toolbar);
        mScannerView = (ScanView) findViewById(R.id.custom_scanner_view);

        mScannerView.setBorderColor(getResources().getColor(R.color.color_theme));
        mScannerView.setLaserColor(getResources().getColor(R.color.color_theme));

        mScanResultLl = (LinearLayout) findViewById(R.id.scan_result_ll);
        mScanLoginBtn = (Button) findViewById(R.id.scan_login_btn);
        mScanCancelBtn = (Button) findViewById(R.id.scan_cancel_btn);

        mScanCancelBtn.setOnClickListener(this);
        mScanLoginBtn.setOnClickListener(this);


        mAppToolBar.setOnBackBtnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        startScan();
    }

    private void startScan() {
        mScannerView.setResultHandler(new ScanResultHandler());
    }

    private class ScanResultHandler implements ResultHandler {

        @Override
        public void handleResult(Result result) {
            try {
                if (result.getContent().length() == 32) {
                    mScanResultCode = result.getContent();
                    mScanResultLl.setVisibility(View.VISIBLE);
                } else {

                    getDialogManager().showOkDialog("请扫描正确的二维码", new DialogManager.OkDialogListener() {
                        @Override
                        public void onOk() {
                            mScannerView.resumeCameraPreview(new ScanResultHandler());
                        }
                    });
                    View okView = getDialogManager().getDialog().getWindow().findViewById(R.id.btn_ok);
                    if (okView != null) {
                        TextView okTextView = (TextView) okView;
                        okTextView.setText("重新扫描");
                        okTextView.setTextColor(getResources().getColor(R.color.color_333333));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mScannerView != null) {
            mScannerView.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mScannerView != null) {
            mScannerView.onPause();
        }
    }

    @Override
    public void checkCodeState(int state, String message) {
        if (state == 200) {
            getMvpPresenter().accessCode(message);
        } else {
            toast(message);
        }
    }

    @Override
    public void accessCodeState(int state, String message) {
        getDialogManager().dismissDialog();
        if (state == 200) {
            if (!TextUtils.isEmpty(message) && "success".equals(message)) {
                toast("登录成功");
                finish();
            }
        } else {
            toast(message);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scan_login_btn:
                getDialogManager().showProgressDialog(this, "正在登录...");
                getMvpPresenter().accessCode(mScanResultCode);
                break;
            case R.id.scan_cancel_btn:
                finish();
                break;
        }
    }
}
