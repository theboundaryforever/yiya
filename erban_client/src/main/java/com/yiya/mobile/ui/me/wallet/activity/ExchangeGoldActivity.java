package com.yiya.mobile.ui.me.wallet.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.ui.me.withdraw.ExchangeDialog;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.me.wallet.presenter.ExchangeGoldPresenter;
import com.yiya.mobile.ui.me.wallet.view.IExchangeGoldView;
import com.yiya.mobile.ui.widget.dialog.ExchangeAwardsDialog;
import com.yiya.mobile.ui.widget.dialog.ExchangeVerificationDialog;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.pay.bean.ExchangeAwardInfo;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MadisonRong on 09/01/2018.
 */
@CreatePresenter(ExchangeGoldPresenter.class)
public class ExchangeGoldActivity extends BaseMvpActivity<IExchangeGoldView, ExchangeGoldPresenter>
        implements IExchangeGoldView, View.OnClickListener, TextWatcher {

    @BindView(R.id.toolbar)
    AppToolBar mToolBar;
    @BindView(R.id.et_exchange_gold_diamond_amount)
    EditText inputEditText;
    @BindView(R.id.tv_exchange_gold_diamond_balance)
    TextView diamondBalanceTextView;
    @BindView(R.id.tv_exchange_gold_result)
    TextView resultTextView;
    @BindView(R.id.tv_goldcoin)
    TextView tv_goldcoin;
//    @BindView(R.id.tv_exchange_gold_gold_balance)
//    TextView goldBalanceTextView;
    @BindView(R.id.btn_exchange_gold_confirm)
    Button confirmButton;
    @BindString(R.string.exchange_gold_title)
    String titleContent;
    @BindString(R.string.exchange_gold_loading)
    String loading;
    private ExchangeVerificationDialog exchangeVerificationDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange_gold);
        ButterKnife.bind(this);
        GrowingIO.getInstance().trackEditText(inputEditText);
        initTitleBar(titleContent);
        initListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMvpPresenter().refreshWalletInfo(false);
    }

    private void initListeners() {
        confirmButton.setOnClickListener(this);
        inputEditText.addTextChangedListener(this);
        mToolBar.setOnBackBtnListener(view -> finish());
    }

    @Override
    public void onClick(View view) {
        String str = inputEditText.getText().toString();
        getMvpPresenter().confirmToExchangeGold(str);
    }

    @Override
    public void setupUserWalletBalance(WalletInfo walletInfo) {
        WalletActivity.isRefresh = true;
        diamondBalanceTextView.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.diamondNum));
        tv_goldcoin.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.goldNum));
//        goldBalanceTextView.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.goldNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
        diamondBalanceTextView.setText("0");
        tv_goldcoin.setText("0");
//        goldBalanceTextView.setText("0");
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (inputEditText.getText().toString().equals("0")) {
            inputEditText.setText("");
        }
        String str = inputEditText.getText().toString();
        getMvpPresenter().calculateResult(str);
    }

    @Override
    public void toastForError(int errorResId) {
        toast(errorResId);
    }

    @Override
    public void displayResult(String result) {
        resultTextView.setText(result);
    }

    @Override
    public void requestExchangeGold(long value) {
        getDialogManager().showProgressDialog(this, loading);
        getMvpPresenter().exchangeGold(String.valueOf(value));
    }

    @Override
    public void requestExchangeGold(long value, String sms) {
        getDialogManager().showProgressDialog(this, loading);
        getMvpPresenter().exchangeGold(String.valueOf(value), sms);
    }

    @Override
    public void exchangeGold(WalletInfo walletInfo) {
        WalletActivity.isRefresh = true;
        getDialogManager().dismissDialog();
        /*toast(R.string.exchange_gold_success);*/

        ExchangeDialog exchangeDialog = ExchangeDialog.newInstance("兑换成功");
        /*exchangeDialog.setOnExchangeChangeListener(new ExchangeDialog.OnExchangeChangeListener() {
            @Override
            public void onChangeListener() {
                exchangeDialog.dismiss();
            }
        });*/
        exchangeDialog.showDialog(getSupportFragmentManager(), "exchangedialog1");
        inputEditText.setText("");
        resultTextView.setText("0");
        if (exchangeVerificationDialog != null) {
            exchangeVerificationDialog.dismiss();
        }
    }

    @Override
    public void exchangeGoldFail(int code, String error) {
        getDialogManager().dismissDialog();
        //需要验证账号的
        if (code == 401) {
            if (exchangeVerificationDialog != null) {
                exchangeVerificationDialog.dismiss();
            }

            String phone = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getPhone();
            if (TextUtils.isEmpty(phone) || phone.length() != 11) {
                toast("请先绑定手机");
                return;
            }

            exchangeVerificationDialog = ExchangeVerificationDialog.newInstance();
            exchangeVerificationDialog.iOnSubmit = sms -> {
                String str = inputEditText.getText().toString();
                getMvpPresenter().confirmToExchangeGold(str, sms);
            };
            exchangeVerificationDialog.show(getSupportFragmentManager(), null);

            return;
        }

        toast(error);
    }

    @Override
    public void showAward(ExchangeAwardInfo data) {
        String drawMsg = data.getDrawMsg();
        String drawUrl = data.getDrawUrl();
        if (TextUtils.isEmpty(drawMsg) || TextUtils.isEmpty(drawUrl)) {
            return;
        }
        ExchangeAwardsDialog exchangeAwardsDialog = ExchangeAwardsDialog.newInstance();
        exchangeAwardsDialog.setData(data);
        exchangeAwardsDialog.show(getSupportFragmentManager(), null);
    }

    @Override
    protected void onDestroy() {
        if (inputEditText != null) {
            inputEditText.removeTextChangedListener(this);
        }
        super.onDestroy();
    }
}
