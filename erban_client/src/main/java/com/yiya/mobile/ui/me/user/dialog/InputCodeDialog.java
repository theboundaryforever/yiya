package com.yiya.mobile.ui.me.user.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.R;

/**
 * @author Zhangsongzhou
 * @date 2019/4/16
 */
public class InputCodeDialog extends BaseDialogFragment {

    public interface OnDialogContentClickListener {
        void onDialogContentClick(String code);
    }

    public void show(FragmentManager fragmentManager) {
        show(fragmentManager, null);
    }


    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();

    }


    private OnDialogContentClickListener mListener;

    public void setOnDialogContentClickListener(OnDialogContentClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Window window = getDialog().getWindow();
        View view = inflater.inflate(R.layout.dialog_input_code, window.findViewById(android.R.id.content), false);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setCancelable(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.dialog_exit_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        final EditText editText = view.findViewById(R.id.dialog_code_edt);
        GrowingIO.getInstance().trackEditText(editText);
        view.findViewById(R.id.dialog_save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onDialogContentClick(editText.getText().toString());
                }
            }
        });
    }
}
