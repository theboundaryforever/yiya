package com.yiya.mobile.ui.home.fragment;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.home.HomeYuChatListPresenter;
import com.yiya.mobile.presenter.home.HomeYuChatListView;
import com.yiya.mobile.presenter.home.HomeYuChatSwipeRefreshListener;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.home.adpater.HomeYuChatListAdapter;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;

/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */
@CreatePresenter(HomeYuChatListPresenter.class)
public class HomeYuChatListFragment extends BaseMvpFragment<HomeYuChatListView, HomeYuChatListPresenter> implements HomeYuChatListView, HomeYuChatSwipeRefreshListener, BaseQuickAdapter.OnItemClickListener {

    public static final int YU_CHAT_HOT = -10;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.content_empty_ll)
    LinearLayout mContentEmptyLl;


    private HomeYuChatListAdapter mAdapter;


    private int mTabId = -1;
    private int mPageNum = 1;
    private int mPageSize = 20;


    @Override

    public int getRootLayoutId() {
        return R.layout.fragment_home_yu_chat_list;
    }


    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {

        mTabId = getArguments().getInt("tabId", -1);

        mAdapter = new HomeYuChatListAdapter();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);


        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                if (mAdapter != null) {
                    if (mAdapter.getData() != null && mAdapter.getData().size() >= mPageSize) {
                        mPageNum++;
                        getData();
                    } else {
                        mAdapter.loadMoreEnd(true);
                    }
                }

            }
        }, mRecyclerView);
        mAdapter.disableLoadMoreIfNotFullPage();
        mRecyclerView.addOnScrollListener(new LoadMoreListener());

//        mContentEmptyLl.setVisibility(View.VISIBLE);


    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        mPageNum = 1;
        getData();
    }

    @Override
    public void onResume() {
        super.onResume();
//        try {
//            if (mAdapter.getData() == null || mAdapter.getData().size() == 0) {
//                getData();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onDestroyView() {
        mAdapter = null;
        mRecyclerView.setAdapter(null);
        super.onDestroyView();

    }

    private void getData() {
        switch (mTabId) {
            case YU_CHAT_HOT:
                getMvpPresenter().getRoomHotList(mPageNum);
                break;
            default:
                getMvpPresenter().getRoomListByTabId(mTabId, mPageNum, mPageSize);
                break;
        }


    }


    @Override
    public void onYuChatSwipeRefreshCallback(int value) {


    }

    @Override
    public void onRoomListSucceed(List<HomeYuChatListInfo> data) {
        if (mAdapter != null) {
            try {
                if (data == null) {
                    if (mAdapter.getData() == null || mAdapter.getData().size() == 0) {
                        mContentEmptyLl.setVisibility(View.VISIBLE);
                    } else {
                        mContentEmptyLl.setVisibility(View.GONE);
                        mAdapter.loadMoreFail();
                    }
                } else {
                    if (data.size() == 0) {
                        mAdapter.loadMoreEnd(true);
                    } else {
                        if (mPageNum == 1) {
                            mAdapter.setNewData(data);
                        } else {
                            mAdapter.addData(data);
                        }
                        mAdapter.loadMoreComplete();
                    }

                }
                if (mAdapter.getData() == null || mAdapter.getData().size() == 0) {
                    mContentEmptyLl.setVisibility(View.VISIBLE);
                } else {
                    mContentEmptyLl.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onRoomListFailed(String message) {
        mContentEmptyLl.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        HomeYuChatListInfo info = mAdapter.getItem(position);
        if (info != null) {
            RoomFrameActivity.start(getActivity(), info.getUid(), info.getType());
        }
    }


    /**
     *
     */
    private class HomeYuChatAdapter extends BaseQuickAdapter<HomeYuChatListInfo, BaseViewHolder> {
        private Context mContext;

        /**
         * Same as QuickAdapter#QuickAdapter(Context,int) but with
         * some initialization data.
         *
         * @param data A new list is created out of this one to avoid mutable list
         */
        public HomeYuChatAdapter(List<HomeYuChatListInfo> data) {
            super(R.layout.item_home_yu_chat_list, data);
        }


        public HomeYuChatAdapter(Context context, List<HomeYuChatListInfo> data) {
            this(data);
            mContext = context;
        }


        @Override
        protected void convert(BaseViewHolder helper, HomeYuChatListInfo item) {
            ImageLoadUtils.loadAvatar(mContext, item.getAvatar(), helper.getView(R.id.item_avatar_iv));
            ((TextView) helper.getView(R.id.item_title_tv)).setText(item.getTitle());
            ImageLoadUtils.loadImage(mContext, item.getTagPict(), helper.getView(R.id.item_tag_iv));

            ((TextView) helper.getView(R.id.item_id_tv)).setText("ID: " + item.getErbanNo());


            String onlineCount = item.getOnlineNum() + "";
            if (item.getOnlineNum() >= 10000) {
                onlineCount = new DecimalFormat("#.0").format(item.getOnlineNum() / 1000f) + "万";
            }

            ((TextView) helper.getView(R.id.tv_item_online_num)).setText(onlineCount);
        }

    }

    private class LoadMoreListener extends RecyclerView.OnScrollListener {

        private int mStartDy = 0, mEndDy = 0;

        private int mTmpState = 0;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (mTmpState == RecyclerView.SCROLL_STATE_DRAGGING) {
                mStartDy = dy;
            }
            if (mTmpState == RecyclerView.SCROLL_STATE_IDLE) {
                mEndDy = dy;
            }

        }

        /**
         * 1 2 0
         *
         * @param recyclerView
         * @param newState
         */
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            mTmpState = newState;
            switch (newState) {
                case RecyclerView.SCROLL_STATE_IDLE:
                    int valueDy = mEndDy - mStartDy;
                    if (valueDy < 0) {
                        LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
                        int lastVisibleItem = manager.findLastCompletelyVisibleItemPosition();
                        int totalItemCount = manager.getItemCount();
                        if (lastVisibleItem == (totalItemCount - 1)) {

                        }
                    }
                    Log.d(TAG, "onScrollStateChanged: ---->" + valueDy);
                    break;
                case RecyclerView.SCROLL_STATE_DRAGGING:
                    break;
                case RecyclerView.SCROLL_STATE_SETTLING:
                    break;
            }
        }
    }


}
