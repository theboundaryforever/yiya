package com.yiya.mobile.ui.web;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.TResult;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.common.ICommonClient;
import com.tongdaxing.xchat_core.count.IChargeClient;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.file.JXFileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.yiya.mobile.constant.BaseUrl.USER_AGENT;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/15.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public abstract class BaseWebViewActivity extends TakePhotoActivity {
    private static final String CAMERA_PREFIX = "picture_";
    protected WebView webView;
    protected String url;
    protected Handler mHandler = new Handler();
    protected WebChromeClient wvcc;
    protected WebViewInfo webViewInfo = null;
    protected JSInterface jsInterface;

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mHandler.post(() -> webView.evaluateJavascript("onStart()", value -> {

            }));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        if (mHandler != null) {
            releaseHandler(mHandler);
            mHandler = null;
        }
        super.onDestroy();
    }

    protected void releaseHandler(Handler handler) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        getMyIntent(getIntent());
        initView();
        setListener();
        ShowWebView(url);
    }

    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(this, "请稍后");
        CoreManager.getCore(IFileCore.class).upload(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
        jsInterface.onImageChooserResult("");
    }

    protected abstract int getLayoutId();

    protected void getMyIntent(Intent intent) {
        url = intent.getStringExtra("url");
    }

    protected void initView() {
        webView = getWebView();
    }

    protected void setListener() {
    }

    public void ShowWebView(String url) {
        webView.loadUrl(url);
        initData();
    }

    protected abstract WebView getWebView();

    @SuppressLint("SetJavaScriptEnabled")
    protected void initData() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        jsInterface = new JSInterface(webView, this);
        setJsInterface(jsInterface);
        webView.addJavascriptInterface(jsInterface, "androidJsObj");
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {

                try {
                    webView.evaluateJavascript("shareInfo()", value -> {
                        LogUtil.i("onReceiveValue11", webViewInfo + "");
                        if (!StringUtil.isEmpty(value) || !value.equals("null")) {
                            String jsonData = value.replace("\\", "");
                            if (jsonData.indexOf("\"") == 0) {
                                //去掉第一个 "
                                jsonData = jsonData.substring(1, jsonData.length());
                            }
                            if (jsonData.lastIndexOf("\"") == (jsonData.length() - 1)) {
                                jsonData = jsonData.substring(0, jsonData.length() - 1);
                            }
                            try {
                                Gson gson = new Gson();
                                webViewInfo = gson.fromJson(jsonData, WebViewInfo.class);
                                LogUtil.i("onReceiveValue",
                                        webViewInfo + "");
                            } catch (JsonSyntaxException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                BaseWebViewActivity.this.onReceivedError(view, request, error);

            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//                super.onReceivedSslError(view, handler, error); //一定要去掉
                // handler.cancel();// Android默认的处理方式
                handler.proceed();// 接受所有网站的证书
                // handleMessage(Message msg);// 进行其他处理
            }
        });
        //获取webviewtitle作为titlebar的title
        wvcc = new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress == 100) {
                    BaseWebViewActivity.this.onPageFinished(view, newProgress);
                }
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                BaseWebViewActivity.this.onReceivedTitle(view, title);

            }
        };
        // 设置setWebChromeClient对象
        webView.setWebChromeClient(wvcc);
        // 设置Webview的user-agent
        webView.getSettings().setUserAgentString(webView.getSettings().getUserAgentString() + USER_AGENT);
    }

    protected void setJsInterface(JSInterface jsInterface) {
    }

    protected void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
    }

    protected void onPageFinished(WebView view, int progress) {
    }

    protected void onReceivedTitle(WebView view, String title) {
    }

    @CoreEvent(coreClientClass = ICommonClient.class)
    public void onRecieveNeedRefreshWebView() {
        if (!StringUtil.isEmpty(url)) {
            ShowWebView(url);
        }
    }

    @CoreEvent(coreClientClass = IChargeClient.class)
    public void chargeAction(String action) {
        try {
            mHandler.post(() -> webView.evaluateJavascript("getStatus()", value -> {

            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showImageChooser() {
        //图片压缩配置 暂时写死 长或宽不超过当前屏幕的长或宽、图片大小不超过2M
        CompressConfig compressConfig = new CompressConfig.Builder().setMaxPixel(DisplayUtils.getScreenHeight(this)).setMaxSize(2048 * 1024).create();
        getTakePhoto().onEnableCompress(compressConfig, true);
        ButtonItem buttonItem = new ButtonItem("拍照上传", () ->
                checkPermission(this::takePhoto, R.string.ask_camera, android.Manifest.permission.CAMERA));
        ButtonItem buttonItem1 = new ButtonItem("本地相册", () ->
                getTakePhoto().onPickFromGallery());
        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(buttonItem);
        buttonItems.add(buttonItem1);
        getDialogManager().showCommonPopupDialog(buttonItems, "取消", false);
    }

    private void takePhoto() {
        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
        File cameraOutFile = JXFileUtils.getTempFile(mCameraCapturingName);
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        getTakePhoto().onPickFromCapture(uri);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        getDialogManager().dismissDialog();
        jsInterface.onImageChooserResult(url);
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        getDialogManager().dismissDialog();
        jsInterface.onImageChooserResult("");
        toast("上传失败");
    }

}
