package com.yiya.mobile.ui.me.wallet.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.redpacket.bean.WithdrawRedListInfo;

/**
 * <p> 红包提现展示列表 </p>
 * Created by Administrator on 2017/11/20.
 */
public class WithdrawRedListAdapter extends BaseQuickAdapter<WithdrawRedListInfo, BaseViewHolder> {

    public WithdrawRedListAdapter() {
        super(R.layout.item_red_withdraw);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, WithdrawRedListInfo withdrwaListInfo) {
        if (withdrwaListInfo == null) {
            return;
        }

        baseViewHolder.setText(R.id.list_name, "¥" + withdrwaListInfo.getPacketNum());

        ImageView bg = baseViewHolder.itemView.findViewById(R.id.select_withdraw);
        bg.setSelected(withdrwaListInfo.isSelected);
        TextView gold = baseViewHolder.getView(R.id.list_name);
        gold.setSelected(withdrwaListInfo.isSelected);
        TextView amount = baseViewHolder.getView(R.id.tv_wd_money);
        amount.setSelected(withdrwaListInfo.isSelected);

        //TextView tvWd = baseViewHolder.getView(R.id.tv_wd_money);
        //tvWd.setEnabled(withdrwaListInfo.isWd());

        //LinearLayout layout = baseViewHolder.getView(R.id.select_withdraw);
        //TextView money = baseViewHolder.getView(R.id.list_name);
        //baseViewHolder.getView(R.id.text1).setVisibility(View.GONE);
        //baseViewHolder.getView(R.id.text2).setVisibility(View.GONE);
        //baseViewHolder.getView(R.id.tv_jewel).setVisibility(View.GONE);
        //TextView tvWd = baseViewHolder.getView(R.id.tv_wd_money);
        //tvWd.setEnabled(withdrwaListInfo.isWd());
        //baseViewHolder.addOnClickListener(R.id.tv_wd_money);
    }
}
