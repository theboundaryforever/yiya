package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.ui.widget.magicindicator.buildins.ArgbEvaluatorHolder;
import com.yiya.mobile.ui.widget.magicindicator.buildins.UIUtil;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * <p> 公共多个滑动tab样式 </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class CommonMagicIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;
    private int mBottomMargin;

    private int normalColorId = R.color.color_999999;
    private int selectColorId = R.color.color_1A1A1A;

    public CommonMagicIndicatorAdapter(Context context, List<TabInfo> titleList, int bottomMargin) {
        mContext = context;
        mTitleList = titleList;
        mBottomMargin = bottomMargin;
    }

    private int size = 16;
    private Typeface mTextStyle = Typeface.DEFAULT;

    public void setSize(int size) {
        this.size = size;
    }

    public void setTextStyle(Typeface textStyle) {
        mTextStyle = textStyle;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    public void setNormalColorId(@ColorRes int normalColorId) {
        this.normalColorId = normalColorId;
    }

    public void setSelectColorId(@ColorRes int selectColorId) {
        this.selectColorId = selectColorId;
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {
        /*ScaleTransitionPagerTitleView scaleTransitionPagerTitleView = new ScaleTransitionPagerTitleView(context);
        scaleTransitionPagerTitleView.setNormalColor(ContextCompat.getColor(mContext, normalColorId));
        scaleTransitionPagerTitleView.setSelectedColor(ContextCompat.getColor(mContext, selectColorId));
        scaleTransitionPagerTitleView.setMinScale(1);
        scaleTransitionPagerTitleView.setTextSize(size);
        scaleTransitionPagerTitleView.setTypeface(mTextStyle);
        scaleTransitionPagerTitleView.setText(mTitleList.get(i).getName());
        scaleTransitionPagerTitleView.setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }

        });
        return scaleTransitionPagerTitleView;*/

        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.ly_indicator_bill);

        final TextView titleTv = pagerTitleView.findViewById(R.id.tv_name_ind);

        final ImageView lineView = pagerTitleView.findViewById(R.id.view_ind);

        /*final View pointView = pagerTitleView.findViewById(R.id.view_point);*/

        titleTv.setText(mTitleList.get(i).getName());


        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {

            @Override
            public void onSelected(int index, int totalCount) {
                /*titleTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));*/
                lineView.setVisibility(View.VISIBLE);
                /*titleTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);*/
            }

            @Override
            public void onDeselected(int index, int totalCount) {
                /*titleTv.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));*/
                lineView.setVisibility(View.INVISIBLE);
                /*titleTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);*/
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
                int colorId = ArgbEvaluatorHolder.eval(leavePercent, ContextCompat.getColor(mContext, R.color.color_67ffffff)
                        , ContextCompat.getColor(mContext, R.color.white));
                titleTv.setTextColor(colorId);

//                titleTv.setScaleX(1.0f + (0.8f - 1.0f) * leavePercent);
//                titleTv.setScaleY(1.0f + (0.8f - 1.0f) * leavePercent);
            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
                int colorId = ArgbEvaluatorHolder.eval(enterPercent, ContextCompat.getColor(mContext, R.color.color_67ffffff)
                        , ContextCompat.getColor(mContext, R.color.white));
                titleTv.setTextColor(colorId);

//                titleTv.setScaleX(0.8f + (1.0f - 0.8f) * enterPercent);
//                titleTv.setScaleY(0.8f + (1.0f - 0.8f) * enterPercent);
            }
        });
        pagerTitleView.findViewById(R.id.indicator_bg_ll).setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }
        });
        /*mCommonPagerTitleViewList.add(pagerTitleView);*/
        return pagerTitleView;
    }


    @Override
    public IPagerIndicator getIndicator(Context context) {
        LinePagerIndicator indicator = new LinePagerIndicator(context);
        indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
        indicator.setLineHeight(UIUtil.dip2px(mContext, 2.5));
        indicator.setRoundRadius(UIUtil.dip2px(mContext, 1.25));
        indicator.setLineWidth(UIUtil.dip2px(mContext, 27));
        indicator.setColors(ContextCompat.getColor(mContext, selectColorId));
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.bottomMargin = mBottomMargin;
        indicator.setLayoutParams(lp);
        return null;
    }

    private OnItemSelectListener mOnItemSelectListener;

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}