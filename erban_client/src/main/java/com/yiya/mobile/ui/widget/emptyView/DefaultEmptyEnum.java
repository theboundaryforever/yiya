package com.yiya.mobile.ui.widget.emptyView;

import com.tongdaxing.erban.R;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/10/12.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public enum DefaultEmptyEnum {
    EMPTY_HOME(R.drawable.bg_default_empty_kiss, "暂无房间噢~逛逛其他先吧！"),//首页
    EMPTY_BLACK_LIST(R.drawable.bg_default_empty_laugh, "好人缘的您，黑名单是空哟~"),//黑名单
    EMPTY_ROOM_MANAGER(R.drawable.bg_default_empty_kiss, "房间暂无管理员，赶紧去设置一个吧~"),//房间管理员
    EMPTY_USER_PHOTO(R.drawable.bg_default_empty_normal, "相册空空如也噢~"),//个人主页相册
    EMPTY_FRIENDS(R.drawable.bg_default_empty_cry, "很遗憾，您还没有好友噢~"),//好友
    EMPTY_FOLLOW(R.drawable.bg_default_empty_cry, "还没有关注任何人噢~"),//关注
    EMPTY_MESSAGE(R.drawable.bg_default_empty_cry, "暂时还没有私信噢~"),//消息
    EMPTY_FANS(R.drawable.bg_default_empty_cry, "很遗憾，您还没有粉丝噢~"),//粉丝
    EMPTY_BILL(R.drawable.bg_default_empty_cry, "您还没有产生账单噢~"),//金币账单
    EMPTY_WALLET(R.drawable.bg_default_empty_cry, "您还没有充值哦~"),//钱包
    EMPTY_GIFT_RECEIVE_RECORD(R.drawable.bg_default_empty_cry, "暂时还没有收到过礼物噢~"),//收礼记录
    EMPTY_GIFT_SEND_RECORD(R.drawable.bg_default_empty_cry, "暂时还没有送过礼物噢~"),//送礼记录
    EMPTY_GIFT_WALL(R.drawable.bg_default_empty_normal, "暂时还没有收到过礼物噢~");//礼物墙

    private int resId;
    private String title;

    DefaultEmptyEnum(int resId, String title) {
        this.resId = resId;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public int getResId() {
        return resId;
    }
}
