package com.yiya.mobile.ui.me.bills.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.yiya.mobile.ui.me.bills.adapter.OrmosiaFragmentPagerAdapter;
import com.yiya.mobile.ui.me.bills.fragment.OrmosiaBillsExpendFragment;
import com.yiya.mobile.ui.me.bills.fragment.OrmosiaBillsIncomeFragment;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/4/19
 */
public class OrmosiaBillsActivity extends BaseActivity implements View.OnClickListener, CommonMagicIndicatorAdapter.OnItemSelectListener {

    private MagicIndicator mMagicIndicator;
    private ViewPager mViewPager;
    private AppToolBar mToolBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_bills);
        initView();
        initData();
        setListener();
    }

    private void initView() {


        mMagicIndicator = (MagicIndicator) findViewById(R.id.magic_indicator2);
        mToolBar = (AppToolBar) findViewById(R.id.toolbar);
        mViewPager = (ViewPager) findViewById(R.id.vPager);
    }

    private void initData() {
        mToolBar.setTitle("咿呀账单");

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new OrmosiaBillsIncomeFragment());
        fragmentList.add(new OrmosiaBillsExpendFragment());
        OrmosiaFragmentPagerAdapter adapter = new OrmosiaFragmentPagerAdapter(getSupportFragmentManager());
        adapter.setData(fragmentList);
        mViewPager.setAdapter(adapter);
        initMagicIndicator2();
    }

    private void setListener() {
        mToolBar.setOnBackBtnListener(view -> finish());
    }

    private void initMagicIndicator2() {
        final String[] titles = getResources().getStringArray(R.array.bill_withdraw_titles);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        List<TabInfo> tabInfoList = new ArrayList<>(2);
        for (int i = 0; i < titles.length; i++) {
            tabInfoList.add(new TabInfo(i, titles[i]));
        }
        CommonMagicIndicatorAdapter adapter = new CommonMagicIndicatorAdapter(this, tabInfoList, 0);
        adapter.setSelectColorId(R.color.color_FA5D70);
        adapter.setSize(18);
        adapter.setOnItemSelectListener(this);
        commonNavigator.setAdapter(adapter);
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    @Override
    public void onItemSelect(int position) {
        mViewPager.setCurrentItem(position);
    }
}
