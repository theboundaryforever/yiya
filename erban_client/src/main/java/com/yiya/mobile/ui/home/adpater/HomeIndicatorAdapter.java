package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/5/16
 */
public class HomeIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;

    private List<CommonPagerTitleView> mCommonPagerTitleViewList;

    public HomeIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mContext = context;
        mTitleList = titleList;
        mCommonPagerTitleViewList = new ArrayList<>();
        mCommonPagerTitleViewList.clear();
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {
        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.ly_indicator_home);

        final TextView titleTv = pagerTitleView.findViewById(R.id.tv_name_ind);

        final View lineView = pagerTitleView.findViewById(R.id.view_ind);

        final View pointView = pagerTitleView.findViewById(R.id.view_point);

        titleTv.setText(mTitleList.get(i).getName());


        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {

            @Override
            public void onSelected(int index, int totalCount) {
                titleTv.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                lineView.setVisibility(View.VISIBLE);
                titleTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
            }

            @Override
            public void onDeselected(int index, int totalCount) {
                titleTv.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                lineView.setVisibility(View.INVISIBLE);
                titleTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
//                int colorId = ArgbEvaluatorHolder.eval(leavePercent, ContextCompat.getColor(mContext, R.color.color_383950)
//                        , ContextCompat.getColor(mContext, R.color.white));
//                titleTv.setTextColor(colorId);

//                titleTv.setScaleX(1.0f + (0.8f - 1.0f) * leavePercent);
//                titleTv.setScaleY(1.0f + (0.8f - 1.0f) * leavePercent);
            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
//                int colorId = ArgbEvaluatorHolder.eval(enterPercent, ContextCompat.getColor(mContext, R.color.color_383950)
//                        , ContextCompat.getColor(mContext, R.color.white));
//                titleTv.setTextColor(colorId);

//                titleTv.setScaleX(0.8f + (1.0f - 0.8f) * enterPercent);
//                titleTv.setScaleY(0.8f + (1.0f - 0.8f) * enterPercent);
            }
        });
        pagerTitleView.findViewById(R.id.indicator_bg_ll).setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }
        });
        mCommonPagerTitleViewList.add(pagerTitleView);
        return pagerTitleView;
    }

    public View getChildView(int index, int viewId) {
        View tmpView = null;
        if (mCommonPagerTitleViewList.size() > index) {
            tmpView = mCommonPagerTitleViewList.get(index).findViewById(viewId);
        }
        return tmpView;
    }


    @Override
    public IPagerIndicator getIndicator(Context context) {
        return null;
    }

    private HomeIndicatorAdapter.OnItemSelectListener mOnItemSelectListener;

    public void setOnItemSelectListener(HomeIndicatorAdapter.OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}