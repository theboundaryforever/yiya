package com.yiya.mobile.ui.me.user.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.model.user.UserModel;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

public class InviteCodeFillActivity extends BaseActivity {
    private AppToolBar mToolBar;
    private EditText etContent;
    private String inviteCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_code_fill);
        mToolBar = (AppToolBar) findViewById(R.id.toolbar);
        etContent = (EditText) findViewById(R.id.et_content);
        GrowingIO.getInstance().trackEditText(etContent);
        mToolBar.setOnBackBtnListener(view -> finish());
        mToolBar.setOnRightBtnClickListener(view -> {
            getDialogManager().showOkCancelDialog("邀请码填写后不可更改，是否确认填写", "确定并填写", "再考虑下", true, new DialogManager.OkCancelDialogListener() {
                @Override
                public void onCancel() {
                }

                @Override
                public void onOk() {
                    save();
                }
            });
        });
    }

    public void save() {
        if (etContent.getText() == null || StringUtils.isEmpty(etContent.getText().toString())) {
            SingleToastUtil.showToast("邀请码不能为空哦！");
            return;
        }
        getDialogManager().showProgressDialog(this, "正在保存...");
        inviteCode = etContent.getText().toString();
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            new UserModel().userUpdateInviteCode(inviteCode, new OkHttpManager.MyCallBack<ServiceResult<UserInfo>>() {

                @Override
                public void onError(Exception e) {
                    getDialogManager().dismissDialog();
                    toast(e.getMessage());
                }

                @Override
                public void onResponse(ServiceResult<UserInfo> response) {
                    if (response != null && response.isSuccess()) {
                        Intent intent = new Intent();
                        intent.putExtra("inviteCode", inviteCode);
                        setResult(RESULT_OK, intent);
                        finish();
                        finish();
                    } else {
                        getDialogManager().dismissDialog();
                        toast(response == null ? "数据异常" : response.getMessage());
                    }
                }
            });
        }
    }
}
