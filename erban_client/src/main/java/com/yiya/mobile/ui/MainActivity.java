package com.yiya.mobile.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.microquation.linkedme.android.LinkedME;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.OnCheckAttentionListener;
import com.netease.nim.uikit.cache.NimUserInfoCache;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.contact.ContactEventListener;
import com.netease.nim.uikit.session.CheckSendImageListener;
import com.netease.nim.uikit.session.SessionCustomization;
import com.netease.nim.uikit.session.SessionEventListener;
import com.netease.nim.uikit.session.actions.BaseAction;
import com.netease.nim.uikit.session.actions.ImageAction;
import com.netease.nim.uikit.session.module.IShareFansCoreClient;
import com.netease.nim.uikit.session.panl.InputPanelListener;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderInvitationFans;
import com.netease.nimlib.sdk.NimIntent;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.StatusCode;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.activity.IActivityCoreClient;
import com.tongdaxing.xchat_core.activity.bean.LotteryInfo;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.HomeMelon;
import com.tongdaxing.xchat_core.home.mission.CheckInMissionInfo;
import com.tongdaxing.xchat_core.home.mission.Missions;
import com.tongdaxing.xchat_core.home.view.IMainView;
import com.tongdaxing.xchat_core.im.custom.bean.NewUserWelfareAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoticeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.OpenRoomNotiAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.ShareFansAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.nim.LotteryAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.nim.SysMsgNewFansAttachment;
import com.tongdaxing.xchat_core.im.login.IIMLoginClient;
import com.tongdaxing.xchat_core.im.message.IIMMessageCore;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_core.linked.ILinkedCoreClient;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCoreClient;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfoV2;
import com.tongdaxing.xchat_core.room.IRoomCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_core.user.VersionsCoreClient;
import com.tongdaxing.xchat_core.user.bean.CheckUpdataBean;
import com.tongdaxing.xchat_core.user.bean.NewRecommendBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.coremanager.IAppInfoCore;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.im.IMError;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.SpUtils;
import com.tongdaxing.xchat_framework.util.util.pref.CommonPref;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.constant.Extras;
import com.yiya.mobile.im.actions.GiftAction;
import com.yiya.mobile.im.holder.MsgViewHolderContent;
import com.yiya.mobile.im.holder.MsgViewHolderLottery;
import com.yiya.mobile.im.holder.MsgViewHolderNewFans;
import com.yiya.mobile.im.holder.MsgViewHolderNewUserWelfare;
import com.yiya.mobile.im.holder.MsgViewHolderOnline;
import com.yiya.mobile.im.holder.MsgViewHolderRedPacket;
import com.yiya.mobile.presenter.home.MainPresenter;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.service.DaemonService;
import com.yiya.mobile.ui.common.widget.CircleImageView;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.home.PlantCucumberDialog;
import com.yiya.mobile.ui.home.SignInMissionDialog;
import com.yiya.mobile.ui.home.SignInRewardDialog;
import com.yiya.mobile.ui.home.fragment.HomeFragment;
import com.yiya.mobile.ui.launch.activity.MiddleActivity;
import com.yiya.mobile.ui.login.activity.CompleteInfoActivity;
import com.yiya.mobile.ui.login.activity.MainLoginActivity;
import com.yiya.mobile.ui.me.MeFragment;
import com.yiya.mobile.ui.message.fragment.MessageFragment;
import com.yiya.mobile.ui.newfind.fragment.NewFindFragment;
import com.yiya.mobile.ui.verified.VerifiedDialog;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.ui.widget.MainTabLayout;
import com.yiya.mobile.ui.widget.dialog.NewUserDialog;
import com.yiya.mobile.ui.widget.dialog.RedPacketDialog;
import com.yiya.mobile.ui.widget.dialog.RoomCreateDialog;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.JPushHelper;
import com.yiya.mobile.utils.SessionHelper;
import com.yiya.mobile.utils.UIHelper;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Administrator
 */
@CreatePresenter(MainPresenter.class)
public class MainActivity extends BaseMvpActivity<IMainView, MainPresenter>
        implements MainTabLayout.OnTabClickListener, IMainView, View.OnTouchListener {

    private static final String EXTRA_APP_QUIT = "APP_QUIT";
    private static final String TAG = "MainActivity";
    private RelativeLayout avatarLayout;
    private CircleImageView avatarImage;
    private SVGAImageView svgaFloatSate;
    private MainTabLayout mMainTabLayout;
    private int mCurrentMainPosition = 0;
    private ViewGroup mViewGroup;
    private int mWidthPixels;
    private int mHeightPixels;
    private long mDownTimeMillis;
    private int mMainTabHeight;

    @BindView(R.id.tv_room_title)
    TextView tvRoomTitle;
    @BindView(R.id.tv_room_id)
    TextView tvRoomId;
    @BindView(R.id.iv_room_close)
    ImageView ivRoomClose;

    public static void start(Context context) {
        start(context, null);
    }


    public static void start(Context context, Intent extras) {
        Intent intent = new Intent();
        intent.setClass(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if (extras != null) {
            intent.putExtras(extras);
        }
        context.startActivity(intent);
    }

    public static void startPage(Context context, int page) {
        Intent home = new Intent(context, MainActivity.class);
        home.putExtra(Extras.EXTRA_CHANGE_INDEX, page);
        context.startActivity(home);
    }

    @Override
    protected boolean needSteepStateBar() {
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.KEY_MAIN_POSITION, mCurrentMainPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentMainPosition = savedInstanceState.getInt(Constants.KEY_MAIN_POSITION);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentMainPosition = savedInstanceState.getInt(Constants.KEY_MAIN_POSITION);
        }
        ;
        String string = new DecimalFormat("#.0").format(12345 / 10000f) + "万";
        Log.d(TAG, "convert: ----->" + "\t" + string);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSwipeBackEnable(false);
//        Log.d(TAG, "start finish MainActivity: ----->");
        //自动登录
        CoreManager.getCore(IAuthCore.class).autoLogin();
        initTitleBar(getString(R.string.app_name));
        initView();
        initListener();
        permission();
        onParseIntent();
        updateDatas();
        initP2PSessionCustomization();
        IMNetEaseManager.get().subscribeChatRoomEventObservable(roomEvent -> {
            if (roomEvent == null || roomEvent.getEvent() == RoomEvent.NONE) {
                return;
            }
            int event = roomEvent.getEvent();
            if (event == RoomEvent.ENTER_ROOM) {
                onEnter(AvRoomDataManager.get().getRoomInfo());
            } else if (event == RoomEvent.KICK_OUT_ROOM) {
                onRoomExit();
            } else if (event == RoomEvent.ROOM_EXIT) {
                onRoomExit();
            }
        }, this);
        IMNetEaseManager.get().subscribeLogEventObservable(logEvent -> {
            if (logEvent == null) {
                return;
            }
            int day = logEvent.getDay();
            getMvpPresenter().uploadLogFile(day);

        }, this);

        checkUpdata();

    }

    private void initListener() {
        ivRoomClose.setOnClickListener(v -> {
            getMvpPresenter().exitRoom();
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void checkUpdata() {
        VersionsCore core = CoreManager.getCore(VersionsCore.class);
        core.getConfig();
        core.checkVersion();
        core.requestSensitiveWord();
        CoreManager.getCore(IAppInfoCore.class).checkBanned();
    }

    @CoreEvent(coreClientClass = VersionsCoreClient.class)
    public void onVersionUpdataDialog(CheckUpdataBean checkUpdataBean) {
        if (checkUpdataBean == null) {
            return;
        }
        String updateVersion = checkUpdataBean.getUpdateVersion();
        if (TextUtils.isEmpty(updateVersion)) {
            return;
        }
        boolean force = false;
        //1线上版本,2审核中版本,3强制更新版本,4建议更新版本,5已删除版本
        switch (checkUpdataBean.getStatus()) {
            case 1:
                return;
            case 3:
                force = true;
                break;
            case 4:
                force = false;
                break;
            default:

        }

        boolean finalForce = force;

        long l = (Long) SpUtils.get(MainActivity.this, "updataDialogDissTime", 0L);
        if (System.currentTimeMillis() - l < (86400 * 1000) && !finalForce) {
            return;
        }
        AlertDialog alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                .setTitle("检测到有最新的版本是否更新")
                .setMessage(TextUtils.isEmpty(checkUpdataBean.getUpdateVersionDesc()) ? checkUpdataBean.getVersionDesc() : checkUpdataBean.getUpdateVersionDesc())
                .setPositiveButton("更新", (dialog, which) -> {
                    String download_url = checkUpdataBean.getDownloadUrl();
                    if (TextUtils.isEmpty(download_url)) {
                        download_url = "https://www.baidu.com/";
                    }
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(download_url));
                    startActivity(intent);
                    finish();
                })
                .setNegativeButton("取消", (dialog, which) -> {
                    if (finalForce) {
                        finish();
                    } else {
                        SpUtils.put(MainActivity.this, "updataDialogDissTime", System.currentTimeMillis());
                    }
                }).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(!force);
        alertDialog.show();
    }


    private void closeOpenRoomAnimation() {
        if (avatarLayout != null) {
            avatarImage.clearAnimation();
            avatarLayout.setVisibility(View.GONE);
            svgaFloatSate.setImageDrawable(null);
        }
    }

    private void initP2PSessionCustomization() {
        SessionCustomization sessionCustomization = new SessionCustomization();
        ArrayList<BaseAction> actions = new ArrayList<>();
        ImageAction imageAction = new ImageAction();
        actions.add(imageAction);
        actions.add(new GiftAction());
        sessionCustomization.actions = actions;
        sessionCustomization.withSticker = true;
        NimUIKit.setCommonP2PSessionCustomization(sessionCustomization);
//
        NimUIKit.registerMsgItemViewHolder(OpenRoomNotiAttachment.class, MsgViewHolderOnline.class);
        NimUIKit.registerMsgItemViewHolder(NoticeAttachment.class, MsgViewHolderContent.class);
        NimUIKit.registerMsgItemViewHolder(RedPacketAttachment.class, MsgViewHolderRedPacket.class);
        NimUIKit.registerMsgItemViewHolder(LotteryAttachment.class, MsgViewHolderLottery.class);
        NimUIKit.registerMsgItemViewHolder(ShareFansAttachment.class, MsgViewHolderInvitationFans.class);
        NimUIKit.registerMsgItemViewHolder(SysMsgNewFansAttachment.class, MsgViewHolderNewFans.class);//系统消息 - 关注了你
        NimUIKit.registerMsgItemViewHolder(NewUserWelfareAttachment.class, MsgViewHolderNewUserWelfare.class);//新用户福利

        NimUIKit.setSessionListener(listener);
        NimUIKit.setContactEventListener(listener1);
        NimUIKit.setInputPanelListener(new InputPanelListener() {
            @Override
            public void onInputPanel(View view, String account) {
                getMvpPresenter().getCheckBlacklist(view, account);
            }

            @Override
            public void onTxtOperation(View view, String account, String content) {
                getMvpPresenter().checkUserOperation(account, content, "send_text", new OkHttpManager.MyCallBack<Json>() {
                    @Override
                    public void onError(Exception e) {
                        LogUtil.d(TAG);
                    }

                    @Override
                    public void onResponse(Json response) {
                        LogUtil.d(TAG);
                        switch (response.num("code")) {
                            case 200:
                                if (NimUIKit.getCheckUserBlacklistListener() != null) {
                                    NimUIKit.getCheckUserBlacklistListener().onTxtOperationResult(view, content);
                                }
                                break;
                            default:
                                toast(response.str("message"));
                                break;
                        }


                    }
                });
            }
        });
        NimUIKit.setCheckSendImageListener(new CheckSendImageListener() {
            @Override
            public void onCheckSendImage(int position) {
                getMvpPresenter().checkMessageImagePermission(position);
            }
        });

        //是否是关注
        NimUIKit.setOnCheckAttentionListener(new OnCheckAttentionListener() {
            @Override
            public void onCheckAttention(long uid) {
                CoreManager.getCore(IPraiseCore.class).checkUserFansAndBlack(CoreManager.getCore(IAuthCore.class).getCurrentUid(), uid);
            }

            @Override
            public void onDoAttention(long uid) {
                CoreManager.getCore(IPraiseCore.class).praise(uid);
            }
        });
    }

//    @CoreEvent(coreClientClass = IPraiseClient.class)
//    public void onIsLiked(Boolean islike, long uid) {
//        if (NimUIKit.getOnCheckResultAttentionListener() != null) {
//            NimUIKit.getOnCheckResultAttentionListener().onCheckResultAttention(uid, islike);
//        }
//    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCheckUserFansAndBlack(long uid, String json) {

        if (NimUIKit.getOnCheckResultAttentionListener() == null) {
            return;
        }
        try {
            Json tmpJson = new Json(json);

            Json dataJson = tmpJson.json("data");
            NimUIKit.getOnCheckResultAttentionListener().onCheckResultAttention(uid, dataJson.boo("like"), dataJson.boo("online"));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        if (NimUIKit.getOnCheckResultAttentionListener() != null) {
            NimUIKit.getOnCheckResultAttentionListener().onDoAttentionResult(likedUid);
        }
    }

    /**
     * @param view
     * @param json
     */
    @Override
    public void onCheckBlacklist(View view, Json json) {
        if (json != null) {
            if (json.has("inTgUserBlacklist")) {
                if (json.boo("inTgUserBlacklist")) {
                    toast("已在对方黑名单");
                } else {
                    if (NimUIKit.getCheckUserBlacklistListener() != null) {
                        NimUIKit.getCheckUserBlacklistListener().onCheckUserBlacklist(view);
                    }
                }
            }
        }
    }

    /**
     * 显示7天签到dialog
     *
     * @param checkInMissions
     */
    @Override
    public void showSignInMissionDialog(List<Missions.CheckInMissions> checkInMissions) {
        SignInMissionDialog.newInstance(checkInMissions)
                .setOnConfirmClickListener(new SignInMissionDialog.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        //领取签到奖励
                        getDialogManager().showProgressDialog(MainActivity.this, "");
                        getMvpPresenter().postSignInMission();
                    }

                    @Override
                    public void onDismiss() {
                        //获取种瓜
                        getMvpPresenter().getMelonPopup();
                    }
                })
                .show(getSupportFragmentManager(), null);
    }

    @Override
    public void showMissionRewardDialog(CheckInMissionInfo checkInMissionInfo) {
        SignInRewardDialog.newInstance(checkInMissionInfo)
                .setOnConfirmClickListener(new SignInRewardDialog.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        //跳转任务中心
                        CommonWebViewActivity.start(MainActivity.this, BaseUrl.MISSION_CENTER);
                        //获取种瓜
                        getMvpPresenter().getMelonPopup();
                    }

                    @Override
                    public void onDismiss() {
                        //获取种瓜
                        getMvpPresenter().getMelonPopup();
                    }
                })
                .show(getSupportFragmentManager(), null);
    }

    @Override
    public void showMelonDialog(HomeMelon homeMelon) {
        PlantCucumberDialog.newInstance(homeMelon)
                .show(getSupportFragmentManager(), null);
    }

    private ContactEventListener listener1 = new ContactEventListener() {
        @Override
        public void onItemClick(Context context, String account) {
            NimUIKit.startP2PSession(context, account);
        }

        @Override
        public void onItemLongClick(Context context, String account) {

        }

        @Override
        public void onAvatarClick(Context context, String account) {
            UIHelper.showUserInfoAct(MainActivity.this, JavaUtil.str2long(account));
        }
    };

    private SessionEventListener listener = new SessionEventListener() {
        @Override
        public void onAvatarClicked(Context context, IMMessage message) {
            //点击私聊用户头像，可以跳转到该用户个人主页
            String account = message.getFromAccount();
            if ("90000000".equals(account) || NimUIKit.isRobotUid(account) || NimUIKit.isMySelf(account)) {
                return;
            }
            UIHelper.showUserInfoAct(MainActivity.this, JavaUtil.str2long(account));
        }

        @Override
        public void onAvatarLongClicked(Context context, IMMessage message) {

        }

        @Override
        public void onCustomClicked(Context context, long uid) {
            UIHelper.showUserInfoAct(MainActivity.this, uid);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        LinkedME.getInstance().setImmediate(true);
//        checkUpdata();
    }

    private void initView() {
        mHeightPixels = getResources().getDisplayMetrics().heightPixels;
        mWidthPixels = getResources().getDisplayMetrics().widthPixels;
        mMainTabLayout = (MainTabLayout) findViewById(R.id.main_tab_layout);
        avatarLayout = (RelativeLayout) findViewById(R.id.avatar_image_layout);
        avatarImage = (CircleImageView) findViewById(R.id.avatar_image);
        svgaFloatSate = (SVGAImageView) findViewById(R.id.svga_main_floating_state);
        mViewGroup = (ViewGroup) findViewById(R.id.root);
        avatarLayout.setVisibility(View.GONE);
        mMainTabLayout.setOnTabClickListener(this);

        findViewById(R.id.start_room_iv).setOnClickListener(v -> {
            long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            RoomCreateDialog dialog = new RoomCreateDialog();
            dialog.setOnRoomClickListener(new RoomCreateDialog.OnRoomClickListener() {
                @Override
                public void turn2VideoLive() {
                    RoomFrameActivity.start(MainActivity.this, currentUid, RoomInfo.ROOMTYPE_VIDEO_LIVE);
                }

                @Override
                public void turn2AudioChat() {
                    RoomFrameActivity.start(MainActivity.this, currentUid, RoomInfo.ROOMTYPE_HOME_PARTY);
                }
            });
            dialog.show(getSupportFragmentManager(), "RoomCreateDialog");
        });

        initAvatarLayout();
    }

    /**
     * 房间入口按钮拖动逻辑
     */
    private void initAvatarLayout() {
        avatarLayout.setOnTouchListener(this);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) avatarLayout.getLayoutParams();
        int width1 = layoutParams.width;
        int height1 = layoutParams.height;
        int distance = DisplayUtils.dip2px(this, 20);
        ViewGroup.LayoutParams mMainTabLayoutLayoutParams = mMainTabLayout.getLayoutParams();
        mMainTabHeight = mMainTabLayoutLayoutParams.height;
        int mh = mHeightPixels - height1 - mMainTabHeight - 2 * distance;
        int mw = mWidthPixels - width1 - distance;

        layoutParams.leftMargin = mw;
        layoutParams.topMargin = mh;
        avatarLayout.setLayoutParams(layoutParams);
        mViewGroup.invalidate();
    }

    private void updateDatas() {
        mMainTabLayout.select(mCurrentMainPosition);
    }

    private void resetPosition() {
        mCurrentMainPosition = 0;
        updateDatas();
    }

    private void startLoginAct() {
        resetPosition();
        MainLoginActivity.start(MainActivity.this);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        onParseIntent();
    }

    private long startTime;
    private boolean shown;

    private void onParseIntent() {
        Intent intent = getIntent();
        if (intent.hasExtra(NimIntent.EXTRA_NOTIFY_CONTENT)) {
            IMMessage message = (IMMessage) getIntent().getSerializableExtra(NimIntent.EXTRA_NOTIFY_CONTENT);
            switch (message.getSessionType()) {
                case P2P:
                    SessionHelper.startP2PSession(this, message.getSessionId());
                    break;
                case Team:
                    SessionHelper.startTeamSession(this, message.getSessionId());
                    break;
                default:
                    break;
            }
        } else if (intent.hasExtra(EXTRA_APP_QUIT)) {
            onLogout();
        } else if (intent.hasExtra(Extras.EXTRA_JUMP_P2P)) {
            Intent data = intent.getParcelableExtra(Extras.EXTRA_DATA);
            String account = data.getStringExtra(Extras.EXTRA_ACCOUNT);
            if (!TextUtils.isEmpty(account)) {
                SessionHelper.startP2PSession(this, account);
            }
        } else if (intent.hasExtra("url") && intent.hasExtra("type")) {
            startTime = System.currentTimeMillis();
        } else if (intent.hasExtra(Extras.EXTRA_CHANGE_INDEX)) {
            int index = intent.getIntExtra(Extras.EXTRA_CHANGE_INDEX, 0);//切换主界面的显示页面
            if (index != mCurrentMainPosition) {
                mMainTabLayout.select(index);
            }
        }
    }

    private void permission() {
        checkPermission(() -> {
                }, R.string.ask_again,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    @Override
    protected void onDestroy() {
        getMvpPresenter().exitRoom();
        listener = null;
        listener1 = null;
        NimUIKit.setSessionListener(null);
        NimUIKit.setContactEventListener(null);
        super.onDestroy();
    }

    /**
     * 双击返回键退出
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private void checkoutLinkedMe() {
        Intent intent = new Intent(this, MiddleActivity.class);
        startActivity(intent);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        if (accountInfo != null) {
            //获取7天签到任务
//            getMvpPresenter().getSignInMissions();
        }
        checkoutLinkedMe();

        try {
            resetPosition();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onRequestTicketFail(String error) {
        toast(error);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {

        JPushHelper.getInstance().deleteAliasAction(this, JPushHelper.ACTION_DELETE_ALIAS);

        //这里会登录错误的时候，竟然会触发这个。。。
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        boolean isStart = true;
        try {
            ComponentName componentName = activityManager.getRunningTasks(1).get(0).topActivity;
            Log.d(TAG, "onLogout: ----->" + componentName.getClassName());
//            isStart = !componentName.getClassName().contains("LoginActivity");
            if (componentName.getClassName().contains("MiddleActivity") || componentName.getClassName().contains("LoginActivity")) {
                isStart = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (isStart) {
            MainLoginActivity.start(MainActivity.this);
        }
        getMvpPresenter().exitRoom();
//        LoginActivity.start(MainActivity.this);

    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onNeedLogin() {
//        LoginActivity.start(MainActivity.this);
        MainLoginActivity.start(MainActivity.this);
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onImLoginSuccess(LoginInfo loginInfo) {
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        mMainTabLayout.setMsgNum(unreadCount);
        // 如果需要跳转,在这里实现
        if (!shown && (System.currentTimeMillis() - startTime) <= 2000 &&
                getIntent().hasExtra("url") && getIntent().hasExtra("type")) {
            shown = true;
            int type = getIntent().getIntExtra("type", 0);
            String url = getIntent().getStringExtra("url");
            if (type == 3) {
                Intent intent = new Intent(this, CommonWebViewActivity.class);
                intent.putExtra("url", url);
                startActivity(intent);
            } else if (type == 2) {
                // todo 开启房间要传入房间类型
                RoomFrameActivity.start(this, JavaUtil.str2long(url), RoomInfo.ROOMTYPE_HOME_PARTY);
            }
        }
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onImLoginFaith(String error) {
        toast(error);
        MainLoginActivity.start(MainActivity.this);
//        LoginActivity.start(MainActivity.this);
    }

    @CoreEvent(coreClientClass = IIMLoginClient.class)
    public void onKickedOut(StatusCode code) {
        toast("您已被踢下线，若非正常行为，请及时修改密码");
        CoreManager.getCore(IAuthCore.class).logout();
    }

    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
    public void onReceiveRecentContactChanged(List<RecentContact> imMessages) {
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        mMainTabLayout.setMsgNum(unreadCount);
    }

    /**
     * 是否需要完善性别
     */
    @CoreEvent(coreClientClass = IUserClient.class)
    public void onNeedCompleteInfo() {
        getDialogManager().dismissDialog();
        CompleteInfoActivity.start(this);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdateFail(String msg) {
        onLogout();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        CoreManager.getCore(IAuthCore.class).setThirdUserInfo(null);
        int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
        mMainTabLayout.setMsgNum(unreadCount);
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onReceiveNewPacket(final RedPacketInfoV2 redPacketInfo) {
        CommonPref.instance(this).putBoolean("redPacketPoint", true);
        if (redPacketInfo.isNeedAlert()) {
            //新手推荐弹框检测
            if (redPacketInfo.getType() == 1) {
                checkRecommendShow(redPacketInfo);
            } else {
                RedPacketDialog.start(MainActivity.this, redPacketInfo);
            }
        }
    }

    /**
     * 判断是否需要提示新手推荐
     */
    private void checkRecommendShow(RedPacketInfoV2 redPacketInfo) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getNewUserRecommend(), params,
                new OkHttpManager.MyCallBack<ServiceResult<NewRecommendBean>>() {
                    @Override
                    public void onError(Exception e) {
                        RedPacketDialog.start(MainActivity.this, redPacketInfo);
                    }

                    @Override
                    public void onResponse(ServiceResult<NewRecommendBean> response) {
                        if (response != null && response.isSuccess() && response.getData() != null) {
                            NewUserDialog user = NewUserDialog.newInstance(response.getData().getTitle(),
                                    response.getData().getAvatar(), response.getData().getUid());
                            user.show(getSupportFragmentManager(), "new_user");
                        } else {
                            RedPacketDialog.start(MainActivity.this, redPacketInfo);
                        }
                    }
                });
    }

    @CoreEvent(coreClientClass = IActivityCoreClient.class)
    public void onReceiveLotteryActivity(LotteryInfo lotteryInfo) {
//        LotteryDialog.start(this, lotteryInfo);
    }

//    @CoreEvent(coreClientClass = IUserClient.class)
//    public void onCurrentUserInfoCompleteFaith(String msg) {
//        getDialogManager().dismissDialog();
//        toast("用户信息加载失败，请检查网络后重新登录");
//        CoreManager.getCore(IAuthCore.class).logout();
//    }

    private void updateRoomState() {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        //注意视频房没有最小化
        if (roomInfo != null && roomInfo.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(roomInfo.getUid());
            if (roomInfo.getErbanNo() != 0) {
                tvRoomId.setText(String.valueOf(roomInfo.getErbanNo()));
            }
            if (userInfo != null) {
                avatarLayout.clearAnimation();
                avatarLayout.setVisibility(View.VISIBLE);
                Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
                LinearInterpolator lin = new LinearInterpolator();
                operatingAnim.setInterpolator(lin);
                avatarImage.startAnimation(operatingAnim);

                SVGAParser parser = new SVGAParser(this);
                parser.decodeFromAssets("audio.svga", new SVGAParser.ParseCompletion() {
                    @Override
                    public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                        SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                        svgaFloatSate.setImageDrawable(drawable);
                        svgaFloatSate.setClearsAfterStop(true);
                        svgaFloatSate.startAnimation();
                    }

                    @Override
                    public void onError() {

                    }
                });

                ImageLoadUtils.loadAvatar(MainActivity.this, userInfo.getAvatar(), avatarImage);
                if (roomInfo.getTitle() != null) {
                    tvRoomTitle.setText(roomInfo.getTitle());
                } else if (userInfo.getNick() != null) {
                    tvRoomTitle.setText(userInfo.getNick());
                }
            } else {
                NimUserInfo nimUserInfo = NimUserInfoCache.getInstance().getUserInfo(roomInfo.getUid() + "");
                Context mContext = this;
                if (nimUserInfo == null) {
                    NimUserInfoCache.getInstance().getUserInfoFromRemote(String.valueOf(roomInfo.getUid()), new RequestCallbackWrapper<NimUserInfo>() {

                        @Override
                        public void onResult(int i, NimUserInfo nimUserInfo, Throwable throwable) {
                            if (nimUserInfo != null) {
                                avatarLayout.clearAnimation();
                                avatarLayout.setVisibility(View.VISIBLE);
                                Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
                                LinearInterpolator lin = new LinearInterpolator();
                                operatingAnim.setInterpolator(lin);
                                avatarImage.startAnimation(operatingAnim);
                                SVGAParser parser = new SVGAParser(mContext);
                                parser.decodeFromAssets("audio.svga", new SVGAParser.ParseCompletion() {
                                    @Override
                                    public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                                        SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                                        svgaFloatSate.setImageDrawable(drawable);
                                        svgaFloatSate.setClearsAfterStop(true);
                                        svgaFloatSate.startAnimation();
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                                ImageLoadUtils.loadAvatar(MainActivity.this, nimUserInfo.getAvatar(), avatarImage);
                                if (roomInfo.getTitle() != null) {
                                    tvRoomTitle.setText(roomInfo.getTitle());
                                } else if (nimUserInfo.getName() != null) {
                                    tvRoomTitle.setText(nimUserInfo.getName());
                                }
                            } else {
                                closeOpenRoomAnimation();
                            }
                        }
                    });
                } else {
                    avatarLayout.clearAnimation();
                    avatarLayout.setVisibility(View.VISIBLE);
                    Animation operatingAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_anim);
                    LinearInterpolator lin = new LinearInterpolator();
                    operatingAnim.setInterpolator(lin);
                    avatarImage.startAnimation(operatingAnim);

                    SVGAParser parser = new SVGAParser(this);
                    parser.decodeFromAssets("audio.svga", new SVGAParser.ParseCompletion() {
                        @Override
                        public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                            SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                            svgaFloatSate.setImageDrawable(drawable);
                            svgaFloatSate.setClearsAfterStop(true);
                            svgaFloatSate.startAnimation();
                        }

                        @Override
                        public void onError() {

                        }
                    });

                    ImageLoadUtils.loadAvatar(MainActivity.this, nimUserInfo.getAvatar(), avatarImage);
                }
            }
        } else {
            closeOpenRoomAnimation();
        }
    }

    public void onEnter(RoomInfo roomInfo) {
        updateRoomState();
        DaemonService.start(this, roomInfo);
    }

    @CoreEvent(coreClientClass = ILinkedCoreClient.class)
    public void onLinkedInfoUpdateNotLogin() {
        onLogout();
    }

    @CoreEvent(coreClientClass = ILinkedCoreClient.class)
    public void onLinkedInfoUpdate(LinkedInfo linkedInfo) {
        if (!StringUtil.isEmpty(linkedInfo.getRoomUid())) {
            // todo 开启房间要传入房间类型
            RoomFrameActivity.start(this, JavaUtil.str2long(linkedInfo.getRoomUid()), RoomInfo.ROOMTYPE_HOME_PARTY);
        }
    }

    @CoreEvent(coreClientClass = IShareFansCoreClient.class)
    public void onShareFansJoin(long uid, int roomType) {
        if (uid <= 0 || (roomType != RoomInfo.ROOMTYPE_HOME_PARTY && roomType != RoomInfo.ROOMTYPE_VIDEO_LIVE)) {
            SingleToastUtil.showToast("出错咯");
            return;
        } else {
            RoomFrameActivity.start(this, uid, roomType);
            Log.d(TAG, "onShareFansJoin: roomType :" + roomType);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onTabClick(int position) {
//        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        switchFragment(position);
    }

    private void switchFragment(int position) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment mainFragment = fragmentManager.findFragmentByTag(HomeFragment.TAG);

        /*Fragment attentionFragment = fragmentManager.findFragmentByTag(HomeAttentionMainFragment.TAG);*/
       /* Fragment attentionFragment = fragmentManager.findFragmentByTag(HomeFriendsFragment.TAG);*/
//        Fragment msgFragment = fragmentManager.findFragmentByTag(MsgFragment.TAG);
        Fragment messageFragment = fragmentManager.findFragmentByTag(MessageFragment.TAG);
        Fragment meFragment = fragmentManager.findFragmentByTag(MeFragment.TAG);
        /**咿呀新发现页面*/
        Fragment newFindFragment = fragmentManager.findFragmentByTag(NewFindFragment.TAG);

        //先隐藏所有的
        if (mainFragment != null && mainFragment.isVisible()) {
            fragmentTransaction.hide(mainFragment);
        }
        /*if (attentionFragment != null && attentionFragment.isVisible()) {
            fragmentTransaction.hide(attentionFragment);
        }*/
        if(newFindFragment != null && newFindFragment.isVisible()){
            fragmentTransaction.hide(newFindFragment);
        }
        if (messageFragment != null && messageFragment.isVisible()) {
            fragmentTransaction.hide(messageFragment);
        }
        if (meFragment != null && meFragment.isVisible()) {
            fragmentTransaction.hide(meFragment);
        }
        if (position == 0) {
            if (mainFragment == null) {
                mainFragment = new HomeFragment();
                fragmentTransaction.add(R.id.main_fragment, mainFragment, HomeFragment.TAG);
            }
            fragmentTransaction.show(mainFragment);
        }/* else if (position == 1) {
            if (attentionFragment == null) {
                attentionFragment = new HomeAttentionMainFragment();
                fragmentTransaction.add(R.id.main_fragment, attentionFragment, HomeAttentionMainFragment.TAG);
            }
            fragmentTransaction.show(attentionFragment);
        }*/
        else if(position == 1){
            if(newFindFragment == null){
                newFindFragment = new NewFindFragment();
                fragmentTransaction.add(R.id.main_fragment,newFindFragment,NewFindFragment.TAG);
            }
            fragmentTransaction.show(newFindFragment);
        }else if (position == 2) {
            if (messageFragment == null) {
                messageFragment = new MessageFragment();
                fragmentTransaction.add(R.id.main_fragment, messageFragment, MessageFragment.TAG);
            }
            fragmentTransaction.show(messageFragment);
        } else if (position == 3) {
            if (meFragment == null) {
                meFragment = new MeFragment();
                fragmentTransaction.add(R.id.main_fragment, meFragment, MeFragment.TAG);
            }
            fragmentTransaction.show(meFragment);
        }
        fragmentTransaction.commitAllowingStateLoss();
        mCurrentMainPosition = position;
    }

    @Override
    public void onRoomExit() {
        closeOpenRoomAnimation();
        DaemonService.stop(MainActivity.this);
    }


    private int xDelta;
    private int yDelta;


    @Override
    public boolean onTouch(View view, MotionEvent event) {//拖动 悬浮窗
        final int x = (int) event.getRawX();
        final int y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:

                mDownTimeMillis = System.currentTimeMillis();
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();


                xDelta = x - params.leftMargin;
                yDelta = y - params.topMargin;
                break;
            case MotionEvent.ACTION_MOVE:

                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();
                int width = layoutParams.width;
                int height = layoutParams.height;


                int xDistance = x - xDelta;
                int yDistance = y - yDelta;


                int outX = (mWidthPixels - width) - 10;
                if (xDistance > outX) {
                    xDistance = outX;
                }

                int outY = mHeightPixels - height - mMainTabHeight;
                if (yDistance > outY) {
                    yDistance = outY;
                }

                if (yDistance < 100) {
                    yDistance = 100;
                }
                if (xDistance < 10) {
                    xDistance = 10;
                }


                layoutParams.leftMargin = xDistance;
                layoutParams.topMargin = yDistance;
                view.setLayoutParams(layoutParams);
                break;
            case MotionEvent.ACTION_UP:
                if (System.currentTimeMillis() - mDownTimeMillis < 150) {
                    RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
                    if (roomInfo != null) {
                        RoomFrameActivity.start(MainActivity.this, roomInfo.getUid(), RoomInfo.ROOMTYPE_HOME_PARTY);
                    }
                }
                break;
            default:
                break;
        }
        mViewGroup.invalidate();
        return true;
    }


    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetRoomInfo(RoomInfo roomInfo, int pageType) {
        getDialogManager().dismissDialog();
        if (roomInfo != null) {
            if (roomInfo.isValid()) {
                // todo 开启房间要传入房间类型
                RoomFrameActivity.start(this, roomInfo.getUid(), RoomInfo.ROOMTYPE_HOME_PARTY);
            } else {
                openRoom();
            }
        } else {
            openRoom();
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetRoomInfoFail(int errorno, String error, int pageType) {
        getDialogManager().dismissDialog();
        if (errorno == IMError.USER_REAL_NAME_NEED_PHONE
                || errorno == IMError.USER_REAL_NAME_AUDITING
                || errorno == IMError.USER_REAL_NAME_NEED_VERIFIED) {
            VerifiedDialog verifiedDialog = VerifiedDialog.newInstance(error, errorno);
            verifiedDialog.show(getSupportFragmentManager(), "verifiedDialog");
        } else {
            SingleToastUtil.showToast(error);
        }
    }

    private void openRoom() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            CoreManager.getCore(IRoomCore.class).openRoom(userInfo.getUid(), RoomInfo.ROOMTYPE_HOME_PARTY);
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onOpenRoom(RoomInfo roomInfo) {
        // todo 开启房间要传入房间类型
        RoomFrameActivity.start(this, roomInfo.getUid(), RoomInfo.ROOMTYPE_HOME_PARTY);
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onOpenRoomFail(int errorno, String error) {
        if (errorno == IMError.USER_REAL_NAME_NEED_PHONE
                || errorno == IMError.USER_REAL_NAME_AUDITING
                || errorno == IMError.USER_REAL_NAME_NEED_VERIFIED) {
            VerifiedDialog verifiedDialog = VerifiedDialog.newInstance(error, errorno);
            verifiedDialog.show(getSupportFragmentManager(), "verifiedDialog");
        } else {
            SingleToastUtil.showToast(error);
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onAlreadyOpenedRoom() {
        getDialogManager().showOkCancelDialog("您的房间已开启，是否立即进入？", true, new DialogManager.OkCancelDialogListener() {
            @Override
            public void onCancel() {

            }

            @Override
            public void onOk() {
                long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                // todo 开启房间要传入房间类型
                RoomFrameActivity.start(MainActivity.this, uid, RoomInfo.ROOMTYPE_HOME_PARTY);
                finish();
            }
        });
    }


}
