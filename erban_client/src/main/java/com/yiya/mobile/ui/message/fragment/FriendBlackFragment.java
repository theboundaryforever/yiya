package com.yiya.mobile.ui.message.fragment;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.msg.BlackListInfo;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.message.MainMessagePresenter;
import com.yiya.mobile.presenter.message.MainMessageView;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.message.adapter.FriendBlackAdapter;
import com.yiya.mobile.ui.widget.emptyView.DefaultEmptyEnum;

import butterknife.BindView;

/**
 * 黑名单列表
 *
 * @author zwk
 */
@CreatePresenter(MainMessagePresenter.class)
public class FriendBlackFragment extends BaseMvpFragment<MainMessageView, MainMessagePresenter> implements MainMessageView,
        BaseQuickAdapter.OnItemChildClickListener, OnRefreshListener, OnLoadMoreListener {

    @BindView(R.id.smart_refresh_layout)
    SmartRefreshLayout mSmartRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
//    @BindView(R.id.empty_content_tv)
//    TextView mEmptyContentTv;
//    @BindView(R.id.content_empty_ll)
//    LinearLayout mEmptyLinearLayout;

    private FriendBlackAdapter mBlackAdapter;

    private int mPageSize = 20;
    private int mPageNum = 1;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_black_list;
    }

    @Override
    public void onFindViews() {
        mBlackAdapter = new FriendBlackAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBlackAdapter.setEmptyView(getEmptyView(recyclerView, DefaultEmptyEnum.EMPTY_BLACK_LIST));
        recyclerView.setAdapter(mBlackAdapter);
    }

    @Override
    public void onSetListener() {
        mBlackAdapter.setOnItemChildClickListener(this);

        mSmartRefreshLayout.setOnRefreshListener(this);

        mSmartRefreshLayout.setEnableLoadMore(true);
        mSmartRefreshLayout.setOnLoadMoreListener(this);

    }

    @Override
    public void initiate() {
        getData();
    }

    private void getData() {
        getMvpPresenter().getBlacklist(mPageNum, mPageSize);
    }


    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        if (mBlackAdapter != null && !ListUtils.isListEmpty(mBlackAdapter.getData())) {
            BlackListInfo.ListBean listBean = mBlackAdapter.getData().get(position);
            getDialogManager().showOkCancelDialog("是否将" + listBean.getNick() + "移除黑名单列表？"
                    , true, new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {

                        }

                        @Override
                        public void onOk() {
                            removeBlacklist(listBean.getBlacklistUid());
                        }
                    });
        }
    }

    private void removeBlacklist(int uid) {
        getMvpPresenter().removeBlacklist(uid + "");

    }

    public static Fragment newInstance() {
        return new FriendBlackFragment();
    }


    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        mPageNum = 1;
        getData();
    }

    @Override
    public void onLoadMore(RefreshLayout refreshlayout) {
        mPageNum++;
        getData();
    }

    @Override
    public void onGetBlacklist(BlackListInfo info) {
        handleListData(info);
    }

    private void handleListData(BlackListInfo info) {
        if (info != null && info.getList() != null) {
            if (mPageNum == 1) {
                mBlackAdapter.setNewData(info.getList());
            } else {
                mBlackAdapter.addData(info.getList());
            }
        }

//        if (mBlackAdapter.getData() == null || mBlackAdapter.getData().size() == 0) {
//            mEmptyLinearLayout.setVisibility(View.VISIBLE);
//        } else {
//            mEmptyLinearLayout.setVisibility(View.GONE);
//        }

        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadmore();
    }


    @Override
    public void onGetBlacklistFailed(String message) {
        toast(message);
    }

    @Override
    public void onRemoveBlacklist() {

        onRefresh(mSmartRefreshLayout);
    }

    @Override
    public void onRemoveBlacklistFailed(String message) {
        toast(message);
    }
}
