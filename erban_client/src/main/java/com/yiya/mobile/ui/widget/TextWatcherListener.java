package com.yiya.mobile.ui.widget;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/7
 * 描述        输入框事件监听回调
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public abstract class TextWatcherListener implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
