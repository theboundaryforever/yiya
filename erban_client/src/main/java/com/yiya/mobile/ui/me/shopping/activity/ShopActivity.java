package com.yiya.mobile.ui.me.shopping.activity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.base.adapter.BaseIndicatorAdapter;
import com.yiya.mobile.presenter.shopping.DressUpPresenter;
import com.yiya.mobile.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.yiya.mobile.ui.me.shopping.dialog.UseRulesDialog;
import com.yiya.mobile.ui.me.shopping.fragment.ShopFragment;
import com.yiya.mobile.ui.widget.HeadWearImageView;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.UIUtil;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.juxiao.library_ui.widget.AppToolBar;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DESUtils;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.tongdaxing.xchat_framework.util.util.DESUtils.giftCarSecret;

/**
 * @author Administrator
 * @date 2018/3/30
 */

@CreatePresenter(DressUpPresenter.class)
public class ShopActivity extends BaseMvpActivity<IMvpBaseView, DressUpPresenter> implements IMvpBaseView {

    @BindView(R.id.toolbar)
    AppToolBar mToolBar;
    @BindView(R.id.vp_shop)
    ViewPager vpShop;
    @BindView(R.id.mi_dress_type)
    MagicIndicator shopIndicator;
    @BindView(R.id.hw_iv)
    HeadWearImageView mHwIv;
    @BindView(R.id.svga_imageview)
    SVGAImageView svgaImageview;

    private boolean showVgg = false;
    private UserInfo userInfo;

    private boolean isMySelf;
    private long targetUid;

    public static final int DRESS_HEADWEAR = 0;
    public static final int DRESS_CAR = 1;

    /**
     * @param isMySelf  是否是自己
     * @param targetUid 赠送人的uid
     */
    public static void start(Context context, boolean isMySelf, long targetUid) {
        Intent intent = new Intent(context, ShopActivity.class);
        intent.putExtra("targetUid", targetUid);
        intent.putExtra("isMySelf", isMySelf);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "商城");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "商城页");
        initBundle(getIntent());
        setContentView(R.layout.activity_car);
        ButterKnife.bind(this);
        findViews();
        initUserInfo();
        CommonNavigator commonNavigator = new CommonNavigator(this);
        CommonMagicIndicatorAdapter magicIndicatorAdapter = new CommonMagicIndicatorAdapter(this, getMvpPresenter().getTabInfos(), UIUtil.dip2px(this, 4));
        magicIndicatorAdapter.setSize(18);
        magicIndicatorAdapter.setTextStyle(Typeface.defaultFromStyle(Typeface.BOLD));
        magicIndicatorAdapter.setOnItemSelectListener(position -> vpShop.setCurrentItem(position));
        magicIndicatorAdapter.setSelectColorId(R.color.mm_theme);
        commonNavigator.setAdapter(magicIndicatorAdapter);
        BaseIndicatorAdapter baseIndicatorAdapter = new BaseIndicatorAdapter(getSupportFragmentManager(), getFragmentsList());
        commonNavigator.setAdjustMode(true);
        shopIndicator.setNavigator(commonNavigator);
        vpShop.setAdapter(baseIndicatorAdapter);
        ViewPagerHelper.bind(shopIndicator, vpShop);

    }

    private void initBundle(Intent intent) {
        targetUid = intent.getLongExtra("targetUid", 0);
        isMySelf = intent.getBooleanExtra("isMySelf", false);
    }

    private void findViews() {
        mToolBar.setOnBackBtnListener(view -> finish());

        mToolBar.setOnRightBtnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UseRulesDialog().show(getSupportFragmentManager(), "UseRulesDialog");
            }
        });
        initSvgaImageView();
    }

    private void initSvgaImageView() {
        svgaImageview.setVisibility(View.GONE);
        svgaImageview.setClearsAfterStop(true);
        svgaImageview.setLoops(1);
        svgaImageview.setCallback(new SVGACallback() {
            @Override
            public void onPause() {

            }

            @Override
            public void onFinished() {
                showVgg = false;
                svgaImageview.setVisibility(View.GONE);
                svgaImageview.clearAnimation();
            }

            @Override
            public void onRepeat() {

            }

            @Override
            public void onStep(int i, double v) {

            }
        });
    }

    private void initUserInfo() {
        if (isMySelf) {
            targetUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        }
        if (targetUid == 0) {
            return;
        }
        userInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(targetUid, true);
        showUserHeadWear();
    }

    private void showUserHeadWear() {
        if (userInfo != null) {
            if (!StringUtil.isEmpty(userInfo.getAvatar())) {
                mHwIv.setAvatar(userInfo.getAvatar());
            }
            if (!StringUtil.isEmpty(userInfo.getHeadwearUrl())) {
                mHwIv.setHeadWear(userInfo.getHeadwearUrl());
            }
        }
    }

    private void drawSvgaEffect(String url) throws Exception {
        if (showVgg) {
            SingleToastUtil.showToast(getString(R.string.txt_dress_up_car_try));
        }
        if (StringUtils.isNotEmpty(url) && svgaImageview != null) {
            showVgg = true;
            SVGAParser parser = new SVGAParser(this);
            try {
                url = DESUtils.DESAndBase64Decrypt(url, giftCarSecret);
                parser.decodeFromURL(new URL(url), new SVGAParser.ParseCompletion() {
                    @Override
                    public void onComplete(@NotNull SVGAVideoEntity videoItem) {
                        SVGADrawable drawable = new SVGADrawable(videoItem);
                        svgaImageview.setImageDrawable(drawable);
                        svgaImageview.startAnimation();
                        svgaImageview.setVisibility(View.VISIBLE);
                        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(svgaImageview, "alpha", 0.0F, 2.0F).setDuration(800);
                        objectAnimator1.setInterpolator(new AccelerateDecelerateInterpolator());
                        objectAnimator1.start();
                    }

                    @Override
                    public void onError() {
                        showVgg = false;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void showCarAnim(String carUrl) {
        try {
            drawSvgaEffect(carUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 展示用户选中的头饰
     */
    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void showHeadWear(String headWear) {
        if (!StringUtil.isEmpty(headWear)) {
            mHwIv.setHeadWear(headWear);
        }
    }

    public List<Fragment> getFragmentsList() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(ShopFragment.newInstance(DRESS_CAR, isMySelf, targetUid));
        fragments.add(ShopFragment.newInstance(DRESS_HEADWEAR, isMySelf, targetUid));
        return fragments;
    }

}
