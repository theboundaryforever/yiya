package com.yiya.mobile.ui.login.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.databinding.ActivityRegisterXcBinding;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.utils.Logger;
import com.tongdaxing.xchat_core.utils.VerifyPhoneUtils;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

/**
 * Created by zhouxiangfeng on 17/3/5.
 */

public class RegisterActivity extends BaseActivity implements View.OnClickListener {


    private static final String TAG = "RegisterActivity";
    private String errorStr;
    private String phone;
    private String psw;
    private ActivityRegisterXcBinding registerBinding;
    private CodeDownTimer timer;
    private String validateStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "手机号注册");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "注册页");
        registerBinding = DataBindingUtil.setContentView(this, R.layout.activity_register_xc);
        validateStr = getIntent().getStringExtra("validateStr");
        onFindViews();
        onSetListener();
    }


    public void onFindViews() {
        registerBinding.etPassword.addTextChangedListener(etPasswordTextWatcher);
    }

    private TextWatcher etPasswordTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() < 6 || editable.length() > 12) {
                registerBinding.btnRegist.setEnabled(false);
            } else {
                registerBinding.btnRegist.setEnabled(true);
            }
        }
    };

    public void onSetListener() {
        registerBinding.setClick(this);
        registerBinding.login.setOnClickListener(this);
        registerBinding.btnRegist.setOnClickListener(this);
        registerBinding.login.setOnClickListener(view -> finish());
        registerBinding.toolbar.setOnBackBtnListener(view -> finish());
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onRegisterFail(String error) {
        toast(error);
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onRegister() {
        toast("注册成功！");
        // GrowingIO 账号密码埋点
        try {
            registerBinding.etCode.clearFocus();
            registerBinding.etPhone.clearFocus();
            registerBinding.etPassword.clearFocus();
            registerBinding.btnRegist.setFocusableInTouchMode(true);
        } catch (Exception e){
            e.printStackTrace();
            LogUtil.i("view is null");
        }
        CoreManager.getCore(IAuthCore.class).login(phone, psw, "");
        getDialogManager().dismissDialog();
        finish();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onSmsFail(String error) {
        toast(error);
        Logger.error(TAG, "获取短信失败!");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (registerBinding.etPassword != null) {
            registerBinding.etPassword.removeTextChangedListener(etPasswordTextWatcher);
        }
        getDialogManager().dismissDialog();
    }

    @Override
    public void onClick(View v) {
        phone = registerBinding.etPhone.getText().toString();
        switch (v.getId()) {
            case R.id.btn_regist:
                psw = registerBinding.etPassword.getText().toString();
                String smsCode = registerBinding.etCode.getText().toString();
                if (!StringUtils.isEmpty(smsCode)) {
                    if (isOK(phone, psw)) {
                        getDialogManager().showProgressDialog(RegisterActivity.this, "正在注册...");
                        CoreManager.getCore(IAuthCore.class).register(validateStr, phone, smsCode, psw);
                    } else {
                        toast(errorStr);
                    }
                } else {
                    toast("验证码不能为空");
                }

                break;
            case R.id.btn_get_code:
//                if (phone.length() == VerifyPhoneUtils.PHONE_LENGTH && VerifyPhoneUtils.isMatch(phone)) {
                if (phone.length() == VerifyPhoneUtils.PHONE_LENGTH) {
                    timer = new CodeDownTimer(registerBinding.btnGetCode, 60000, 1000);
                    timer.start();
                    CoreManager.getCore(IAuthCore.class).requestSMSCode(phone, 1);
                } else {
                    toast("手机号码不正确");
                }
                break;
            default:
                break;
        }
    }

    private boolean isOK(String phone, String psw) {
        if (StringUtils.isEmpty(psw) && psw.length() < 6) {
            errorStr = "请核对密码！";
            return false;
        }
        if (StringUtils.isEmpty(phone)) {
            errorStr = "请填写手机号码！";
            return false;
        }
        return true;
    }


}
