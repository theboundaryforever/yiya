package com.yiya.mobile.ui.home.adpater;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.HomeRoom;

/**
 * <p> 首页非热门adapter </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class HotFeaturedAdapter extends BaseQuickAdapter<HomeRoom, BaseViewHolder> {

    public HotFeaturedAdapter() {
        super(R.layout.item_home_hot_featrued);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, HomeRoom homeRoom) {
        baseViewHolder.setText(R.id.tv_room_people,homeRoom.getOnlineNum()+"人");
        baseViewHolder.setText(R.id.tv_room_title,homeRoom.getTitle());
        baseViewHolder.setText(R.id.tv_room_id,"ID:" + homeRoom.getErbanNo());
        ImageLoadUtils.loadGifImage(mContext,R.drawable.ic_meet_you,baseViewHolder.getView(R.id.iv_room_feature_move));
        ImageLoadUtils.loadImage(mContext,homeRoom.tagPict,baseViewHolder.getView(R.id.iv_room_tab));
        ImageLoadUtils.loadCircleImage(mContext,homeRoom.getAvatar(),baseViewHolder.getView(R.id.iv_room_pic),R.drawable.nim_avatar_default);
    }
}
