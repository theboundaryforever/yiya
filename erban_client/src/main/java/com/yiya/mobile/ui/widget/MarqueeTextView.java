package com.yiya.mobile.ui.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Administrator on 2014/7/22.
 */
public class MarqueeTextView extends TextView{

    private boolean isMarquee=true;

    public MarqueeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction,Rect previouslyFocusedRect) {
        if(focused) {
            super.onFocusChanged(focused, direction, previouslyFocusedRect);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if(hasWindowFocus){
            super.onWindowFocusChanged(hasWindowFocus);
        }
    }
    @Override
    public boolean isFocused() {
        if (isMarquee){
            return true;
        }else {
            return  false;
        }
    }
    public  void setMarquee(boolean isMarquee){
        this.isMarquee=isMarquee;
        this.setFocusable(isMarquee);
        this.setFocusableInTouchMode(isMarquee);
        this.setHorizontallyScrolling(isMarquee);
    }
}
