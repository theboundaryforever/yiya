package com.yiya.mobile.ui.me.user.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.model.user.UserModel;
import com.yiya.mobile.ui.me.user.dialog.InputCodeDialog;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.presenter.user.IUserInviteView;
import com.yiya.mobile.presenter.user.UserInvitePresenter;
import com.yiya.mobile.ui.me.wallet.activity.BinderPhoneActivity;
import com.yiya.mobile.ui.me.withdraw.RedPacketWithdrawActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.ui.widget.dialog.ShareDialog;
import com.netease.nim.uikit.common.util.sys.ClipboardUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.log.MLog;
import com.tongdaxing.xchat_framework.view.DrawableTextView;

import cn.sharesdk.framework.Platform;

/**
 * 文件描述：邀请好友页面
 *
 * @auther：zwk
 * @data：2019/3/6
 */
@CreatePresenter(UserInvitePresenter.class)
public class InviteFriendActivity extends BaseMvpActivity<IUserInviteView, UserInvitePresenter> implements IUserInviteView, View.OnClickListener, ShareDialog.OnShareDialogItemClick {
    private AppToolBar mToolBar;
    private LinearLayout rlInviteNum;
    private TextView tvInviteNum;
    private LinearLayout rlInviteReward;
    private TextView tvInviteReward;
    private LinearLayout rlFillCode;
    private TextView tvFillCode;

    private TextView tvMyInviteCode;
    private ImageView tvCopyMyCode;

    private DrawableTextView dtvWithdraw;
    private DrawableTextView dtvShare;


    public static void start(Context mContext) {
        Intent intent = new Intent(mContext, InviteFriendActivity.class);
        mContext.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GrowingIO.getInstance().setPageName(this, "邀请好友");
        setContentView(R.layout.activity_friend_invite);
        initView();
        initClickListener();
        initState();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getMvpPresenter().getInviteRedPacketInfo();
    }

    private void initView() {
        rlInviteNum = (LinearLayout) findViewById(R.id.rl_invite_num);
        tvInviteNum = (TextView) findViewById(R.id.tv_has_invite_num);
        rlInviteReward = (LinearLayout) findViewById(R.id.rl_invite_reward);
        tvInviteReward = (TextView) findViewById(R.id.tv_has_invite_reward);
        rlFillCode = (LinearLayout) findViewById(R.id.rl_fill_invite_code);
        tvFillCode = (TextView) findViewById(R.id.tv_filled_invite_code);
        tvCopyMyCode = (ImageView) findViewById(R.id.tv_invite_code_copy);
        tvMyInviteCode = (TextView) findViewById(R.id.tv_my_invite_code);
        dtvWithdraw = (DrawableTextView) findViewById(R.id.dtv_withdraw);
        dtvShare = (DrawableTextView) findViewById(R.id.dtv_invite_friends);
        mToolBar = (AppToolBar) findViewById(R.id.toolbar);
        mToolBar.setOnBackBtnListener(view -> finish());
    }

    private void initClickListener() {
        tvCopyMyCode.setOnClickListener(this);
        rlInviteNum.setOnClickListener(this);
        rlInviteReward.setOnClickListener(this);
        rlFillCode.setOnClickListener(this);
        dtvWithdraw.setOnClickListener(this);
        dtvShare.setOnClickListener(this);
    }

    private void initState() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            tvMyInviteCode.setText(userInfo.getErbanNo() + "");
        }
    }

    @Override
    public void onUserInviteRedPacketSuc(RedPacketInfo data) {
        if (data != null) {
            tvInviteNum.setText(getString(R.string.invite_reward_people, data.getRegisterCout()));
            double charge = data.getPacketNum();
            tvInviteReward.setText(getString(R.string.invite_reward_money, charge));
            if (charge >= 100) {
                dtvWithdraw.setEnabled(true);
            } else {
                dtvWithdraw.setEnabled(false);
            }
            if (StringUtils.isNotEmpty(data.getShareUid())) {
                tvFillCode.setText(data.getShareUid());
                rlFillCode.setClickable(false);
            } else {
                rlFillCode.setClickable(true);
            }
        }
//        dtvWithdraw.setEnabled(true);
    }

    @Override
    public void isBindPhoneSuc() {
        startActivity(new Intent(this, RedPacketWithdrawActivity.class));
    }

    @Override
    public void isBindPhomeFail() {
        startActivity(new Intent(this, BinderPhoneActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_invite_code_copy:
                if (StringUtils.isNotEmpty(tvMyInviteCode.getText())) {
                    ClipboardUtil.clipboardCopyText(this, tvMyInviteCode.getText());
                    SingleToastUtil.showToast("复制成功");
                }
                break;

            case R.id.rl_invite_num://已成功邀请到
                openWebView(BaseUrl.MY_INVITE_PEOPLE + "?uid=" + CoreManager.getCore(IAuthCore.class).getCurrentUid());
                break;

            case R.id.rl_invite_reward://已获得邀请奖励
                openWebView(BaseUrl.MY_INVITE_PERCENTAGE + "?uid=" + CoreManager.getCore(IAuthCore.class).getCurrentUid());
                break;

            case R.id.rl_fill_invite_code:
//                startActivityForResult(new Intent(this, InviteCodeFillActivity.class), 0);
                final InputCodeDialog dialog = new InputCodeDialog();
                dialog.show(getSupportFragmentManager());
                dialog.setOnDialogContentClickListener(new InputCodeDialog.OnDialogContentClickListener() {
                    @Override
                    public void onDialogContentClick(String code) {
                        dialog.dismiss();
                        checkCode(code);
                    }
                });
                break;
            case R.id.dtv_withdraw:
                getMvpPresenter().isBindPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
                break;
            case R.id.dtv_invite_friends:
                ShareDialog shareDialog = new ShareDialog(this);
                shareDialog.setOnShareDialogItemClick(this);
//                shareDialog.setHasInvite(true);
                shareDialog.setShareType(ShareDialog.SHARE_INVITE_FRIEND);
                shareDialog.show();
                break;
        }
    }

    private void openWebView(String url) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        Intent intent = new Intent(this, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    /**
     * 验证邀请验证码
     *
     * @param code
     */
    private void checkCode(final String code) {
        if (TextUtils.isEmpty(code)) {
            SingleToastUtil.showToast("邀请码不能为空哦！");
            return;
        }
        getDialogManager().showProgressDialog(this, "正在保存...");
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            new UserModel().userUpdateInviteCode(code, new OkHttpManager.MyCallBack<ServiceResult<UserInfo>>() {

                @Override
                public void onError(Exception e) {

                    getDialogManager().dismissDialog();
                    toast(e.getMessage());
                    rlFillCode.setClickable(true);
                }

                @Override
                public void onResponse(ServiceResult<UserInfo> response) {
                    if (response != null && response.isSuccess()) {
                        getDialogManager().dismissDialog();
                        tvFillCode.setText(code);
                        rlFillCode.setClickable(false);

                    } else {
                        getDialogManager().dismissDialog();
                        toast(response == null ? "数据异常" : response.getMessage());
                        rlFillCode.setClickable(true);
                    }
                }
            });
        }
    }


    @Override
    public void onSharePlatformClick(Platform platform) {
        WebViewInfo webViewInfo = new WebViewInfo();
        webViewInfo.setTitle(getString(R.string.share_h5_title));
        webViewInfo.setImgUrl(BaseUrl.SHARE_DEFAULT_LOGO);
        webViewInfo.setDesc(getString(R.string.share_h5_desc));
        webViewInfo.setShowUrl(BaseUrl.SHARE_DOWNLOAD);
        CoreManager.getCore(IShareCore.class).shareH5(webViewInfo, platform);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            MLog.info(this, "return is not ok,resultCode=%d", resultCode);
            return;
        }
        if (requestCode == 0) {
            if (null != data) {
                String inviteCode = data.getStringExtra("inviteCode");
                if (StringUtils.isNotEmpty(inviteCode))
                    tvFillCode.setText(inviteCode);
                rlFillCode.setClickable(false);
            }
        }
    }
}
