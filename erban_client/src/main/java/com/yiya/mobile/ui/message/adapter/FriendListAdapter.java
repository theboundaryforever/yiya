package com.yiya.mobile.ui.message.adapter;

import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.databinding.ListItemFriendBinding;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.yiya.mobile.base.bindadapter.BaseAdapter;
import com.yiya.mobile.utils.UIHelper;

/**
 * Created by chenran on 2017/10/3.
 */

public class FriendListAdapter extends BaseAdapter<NimUserInfo, ListItemFriendBinding> {

    public FriendListAdapter() {
        super(R.layout.list_item_friend);
    }

    @Override
    protected void convert(ListItemFriendBinding binding, NimUserInfo item) {
        binding.setUserInfo(item);
        binding.imageView.setOnClickListener(view -> {
            if ("90000000".equals(item.getAccount())) {
                return;
            }
            UIHelper.showUserInfoAct(mContext, JavaUtil.str2long(item.getAccount()));
        });
    }
}
