package com.yiya.mobile.ui.me.mywallet.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.ui.home.adpater.LiveStreamRecyclerAdapter;
import com.yiya.mobile.ui.me.mywallet.activity.WalletActivity;
import com.yiya.mobile.ui.widget.LiveTabView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.List;
import java.util.Locale;

import static com.yiya.mobile.ui.me.mywallet.activity.WalletActivity.WalletInfo.ITEM_TYPE_1;
import static com.yiya.mobile.ui.me.mywallet.activity.WalletActivity.WalletInfo.ITEM_TYPE_2;
import static com.yiya.mobile.ui.me.mywallet.activity.WalletActivity.WalletInfo.ITEM_TYPE_3;

public class WalletRecyclerViewAdapter extends BaseMultiItemQuickAdapter<WalletActivity.WalletInfo, BaseViewHolder> {

    private Context context;
    private String bonus = "";

    public interface OnItemClickListener {
        void onItemClick(int itemType, View view, int position, LiveTabView.TabInfo tabInfo);
    }

    private LiveStreamRecyclerAdapter.OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(LiveStreamRecyclerAdapter.OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    public WalletRecyclerViewAdapter(List<WalletActivity.WalletInfo> data) {
        super(data);
        addItemType(ITEM_TYPE_1,R.layout.item_walletrecycleview_layout);
        addItemType(ITEM_TYPE_2,R.layout.item_walletrecycleview_layout);
        addItemType(ITEM_TYPE_3,R.layout.item_walletrecycleview_layout);
    }

    private int mItemRoomWidth = 0;

    public WalletRecyclerViewAdapter(List<WalletActivity.WalletInfo> data, Context context) {
        this(data);
        this.context = context;
        mItemRoomWidth = (DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context, 30) - DisplayUtils.dip2px(context, 10)) / 2;
    }

    public void setData(List<WalletActivity.WalletInfo> data) {
        setNewData(data);
    }

    public void setbonus(String bonus){
        this.bonus = bonus;
        notifyDataSetChanged();
    }

    @Override
    protected void convert(BaseViewHolder holder, WalletActivity.WalletInfo item) {
        switch(item.getItemType()){
            case ITEM_TYPE_1:
                holderGoldCoin(holder,item);
                break;
            case ITEM_TYPE_2:
                holderDiamonds(holder,item);
                break;
            case ITEM_TYPE_3:
                holderBonus(holder,item);
                break;
        }
    }

    /**金币*/
    private void holderGoldCoin(BaseViewHolder baseViewHolder, WalletActivity.WalletInfo info){
        ((ImageView)baseViewHolder.getView(R.id.iv_1)).setImageResource(R.mipmap.bg_wallet1);
        ((ImageView)baseViewHolder.getView(R.id.iv_2)).setImageResource(R.mipmap.ic_chongzhi);
        ((TextView)baseViewHolder.getView(R.id.tv_title)).setText("金币余额");//CoreManager.getCore(IPayCore.class).setCurrentWalletInfo(data.getData());
        /*String str = (String)(CoreManager.getCore(IPayCore.class).getCurrentWalletInfo().getGoldNum());*/
        ((TextView)baseViewHolder.getView(R.id.tv_number)).setText(String.format(Locale.getDefault(), "%.1f", (CoreManager.getCore(IPayCore.class).getCurrentWalletInfo().goldNum)));
        baseViewHolder.getView(R.id.iv_2).setOnClickListener(v ->{
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(info.getItemType(),null,getData().indexOf(info),null);
            }
        });
        baseViewHolder.getView(R.id.iv_1).setOnClickListener(v ->{
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(info.getItemType(),null,getData().indexOf(info),null);
            }
        });
    }

    /**钻石*/
    private void holderDiamonds(BaseViewHolder baseViewHolder, WalletActivity.WalletInfo info){
        ((ImageView)baseViewHolder.getView(R.id.iv_1)).setImageResource(R.mipmap.bg_wallet2);
        ((ImageView)baseViewHolder.getView(R.id.iv_2)).setImageResource(R.mipmap.ic_tixian);
        ((ImageView)baseViewHolder.getView(R.id.iv_3)).setVisibility(View.VISIBLE);
        ((TextView)baseViewHolder.getView(R.id.tv_title)).setText("钻石余额");
        ((TextView)baseViewHolder.getView(R.id.tv_number)).setText(String.format(Locale.getDefault(), "%.1f", (CoreManager.getCore(IPayCore.class).getCurrentWalletInfo().diamondNum)));
        baseViewHolder.getView(R.id.iv_2).setOnClickListener(v ->{
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(info.getItemType(),null,1,null);
            }
        });
        baseViewHolder.getView(R.id.iv_3).setOnClickListener(v ->{
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(info.getItemType(),null,2,null);
            }
        });
        baseViewHolder.getView(R.id.iv_1).setOnClickListener(v ->{
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(info.getItemType(),null,3,null);
            }
        });
    }

    /**奖励金*/
    private void holderBonus(BaseViewHolder baseViewHolder, WalletActivity.WalletInfo info){
        ((ImageView)baseViewHolder.getView(R.id.iv_1)).setImageResource(R.mipmap.bg_wallet3);
        ((ImageView)baseViewHolder.getView(R.id.iv_2)).setImageResource(R.mipmap.ic_tiqu);
        ((TextView)baseViewHolder.getView(R.id.tv_title)).setText("红包余额"); //CoreManager.getCore(IRedPacketCore.class).getRedPacketInfo();
        ((TextView)baseViewHolder.getView(R.id.tv_number)).setText(bonus);
        ((TextView)baseViewHolder.getView(R.id.tv_tips)).setVisibility(View.VISIBLE);
        baseViewHolder.getView(R.id.iv_2).setOnClickListener(v ->{
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(info.getItemType(),null,getData().indexOf(info),null);
            }
        });
        baseViewHolder.getView(R.id.iv_1).setOnClickListener(v ->{
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(info.getItemType(),null,getData().indexOf(info),null);
            }
        });
    }

}
