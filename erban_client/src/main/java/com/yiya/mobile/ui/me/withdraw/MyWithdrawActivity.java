package com.yiya.mobile.ui.me.withdraw;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.me.bills.activity.OrmosiaBillsActivity;
import com.yiya.mobile.ui.me.bills.activity.WithdrawBillsActivity;
import com.yiya.mobile.ui.me.wallet.activity.BinderPhoneActivity;
import com.yiya.mobile.ui.me.wallet.activity.ExchangeGoldActivity;
import com.yiya.mobile.ui.me.wallet.presenter.IncomePresenter;
import com.yiya.mobile.ui.me.wallet.view.IIncomeView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangsongzhou
 * @date 2019/4/11
 */
@CreatePresenter(IncomePresenter.class)
public class MyWithdrawActivity extends BaseMvpActivity<IIncomeView, IncomePresenter> implements IIncomeView {

    public static final boolean isOpenAliPay = true;//屏蔽支付宝提现

    @BindView(R.id.toolbar)
    AppToolBar mAppToolBar;

    @BindView(R.id.diamond_num_tv)
    TextView mDiamondNumTv;
    @BindView(R.id.detailed_tv)
    TextView mDetailedTv;

    @BindView(R.id.withdrawal_btn)
    Button mWithdrawalBtn;

    private boolean isRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_withdraw);

        ButterKnife.bind(this);

        mAppToolBar.setOnBackBtnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//        if (userInfo != null) {
//            //4 为 工会主播 没有提现功能
//            if (userInfo.getAnchorStatus() == 4) {
//                mWithdrawalBtn.setVisibility(View.GONE);
//            } else {
//                mWithdrawalBtn.setVisibility(View.VISIBLE);
//            }
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMvpPresenter().refreshWalletInfo(true);
    }

    /**
     * 兑换金币
     *
     * @param view
     */
    @OnClick(R.id.exchange_gold_btn)
    void onClickExchangeGold(View view) {
        startActivity(new Intent(this, ExchangeGoldActivity.class));
    }

    /**
     * 提现
     *
     * @param view
     */
    @OnClick(R.id.withdrawal_btn)
    void onClickWithdrawal(View view) {
        isRequest = true;
        getMvpPresenter().hasBindPhone();

    }

    /**
     * 明细
     *
     * @param view
     */
    @OnClick(R.id.detailed_tv)
    void onClickDetailed(View view) {
        startActivity(new Intent(this, WithdrawBillsActivity.class));
    }

    /**
     * 咿呀账单
     *
     * @param view
     */
    @OnClick(R.id.ormosia_tv)
    void onClickOrmosia(View view) {
        startActivity(new Intent(this, OrmosiaBillsActivity.class));
    }


    @Override
    public void handleClick(int id) {

    }

    @Override
    public void hasBindPhone() {
        if (!isRequest) {
            return;
        }
        isRequest = false;
        startActivity(new Intent(this, WithdrawActivity.class));
    }

    @Override
    public void hasBindPhoneFail(String error) {
        if (!isRequest) {
            return;
        }
        startActivity(new Intent(this, BinderPhoneActivity.class));
    }

    @Override
    public void setupUserWalletBalance(WalletInfo walletInfo) {
        mDiamondNumTv.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.diamondNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        mDiamondNumTv.setText("0");
    }
}
