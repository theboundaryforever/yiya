package com.yiya.mobile.ui.home;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.mission.CheckInMissionInfo;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/30.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class SignInRewardDialog extends BaseTransparentDialogFragment {

    @BindView(R.id.iv_reward)
    ImageView ivReward;
    @BindView(R.id.tv_gold)
    TextView tvGold;

    private static final String KEY_INFO = "KEY_INFO";
    private OnConfirmClickListener mListener;
    private boolean mIsConfirm;

    public static SignInRewardDialog newInstance(CheckInMissionInfo checkInMissionInfo) {
        SignInRewardDialog dialog = new SignInRewardDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_INFO, checkInMissionInfo);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_sign_in_reward;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            CheckInMissionInfo checkInMissionInfo = getArguments().getParcelable(KEY_INFO);
            if (checkInMissionInfo != null) {
                tvGold.setText("豆币+" + checkInMissionInfo.getMcoinNum());
                ImageLoadUtils.loadImage(getContext(), checkInMissionInfo.getPicUrl(), ivReward);
            }
        }
    }

    public interface OnConfirmClickListener {
        void onConfirmClick();

        void onDismiss();
    }

    public SignInRewardDialog setOnConfirmClickListener(OnConfirmClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mListener != null && !mIsConfirm) {
            //跳转任务中心就不回调
            mListener.onDismiss();
        }
    }

    @OnClick(R.id.dtv_confirm)
    public void onViewClicked() {
        if (mListener != null) {
            mListener.onConfirmClick();
        }
        mIsConfirm = true;
        dismiss();
    }
}
