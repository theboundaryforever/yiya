package com.yiya.mobile.ui.me.user.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.user.bean.MultiInfo;
import com.yiya.mobile.utils.ImageLoadUtils;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MedalAdapter extends BaseQuickAdapter<MultiInfo, BaseViewHolder> {

    public MedalAdapter() {
        super(R.layout.item_user_medal);
    }

    @Override
    protected void convert(BaseViewHolder helper, MultiInfo item) {
        switch (item.getItemType()) {
            case MultiInfo.TYPE_PIC_URL:
                ImageLoadUtils.loadImage(mContext, item.getPicUrl(), helper.getView(R.id.iv_medal));
                break;
            case MultiInfo.TYPE_RES_ID:
                ImageLoadUtils.loadImageRes(mContext, item.getResId(), helper.getView(R.id.iv_medal));
                break;
        }
    }

}
