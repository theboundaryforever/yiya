package com.yiya.mobile.ui.home.adpater;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.user.bean.NewUserBean;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;

import org.jetbrains.annotations.NotNull;

public class NewUserAdapter extends BaseQuickAdapter<NewUserBean, BaseViewHolder> {
    final static int OFFLINE = 0;// 离线（刚刚来过）
    final static int ONLINE = OFFLINE+1;// 在线不在房间（新人报道）
    final static int INROOM = ONLINE+1;// 在线在房间（热聊中...）

    public NewUserAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, NewUserBean item) {
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), helper.getView(R.id.iv_new_user_avatar), R.drawable.nim_avatar_default);
        helper.setText(R.id.tv_new_user_name, item.getNick());
        ImageView ivGender = helper.getView(R.id.iv_new_gender);
        if (item.getGender() == 1) {
            ivGender.setImageResource(R.drawable.icon_mic_male);
        } else {
            ivGender.setImageResource(R.drawable.icon_mic_female);
        }
        TextView tvState = helper.getView(R.id.tv_new_user_state);

        ImageView ivStatus = helper.getView(R.id.iv_status);
        SVGAImageView svgaStatus = helper.getView(R.id.svga_status);
        ivStatus.setVisibility(View.GONE);
        svgaStatus.setVisibility(View.GONE);

        switch (item.getUserStatus()){
            case INROOM:
                SVGAParser parser = new SVGAParser(mContext);
                parser.decodeFromAssets("microredwave.svga", new SVGAParser.ParseCompletion() {
                    @Override
                    public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                        SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                        svgaStatus.setImageDrawable(drawable);
                        svgaStatus.setClearsAfterStop(true);
                        svgaStatus.startAnimation();
                    }

                    @Override
                    public void onError() {

                    }
                });
                tvState.setText("热聊中");
                tvState.setTextColor(0xffFA56A0);
                svgaStatus.setVisibility(View.VISIBLE);
                break;
            case ONLINE:
                tvState.setText("新人报到");
                tvState.setTextColor(0xff999999);
                ivStatus.setImageResource(R.mipmap.ic_new_user_online);
                ivStatus.setVisibility(View.VISIBLE);
                break;
            case OFFLINE:
                tvState.setText("刚刚来过");
                tvState.setTextColor(0xff999999);
                ivStatus.setImageResource(R.mipmap.ic_new_user_offline);
                ivStatus.setVisibility(View.VISIBLE);
                break;
        }

        //等级
        ImageView ivNewUser = helper.getView(R.id.iv_new_user);
        ImageView ivUserLevel = helper.getView(R.id.iv_user_level);
        ImageView ivExperLevel = helper.getView(R.id.iv_exper_level);
        ImageView ivCharmLevel = helper.getView(R.id.iv_charm_level);

        //萌新
        if (ivNewUser != null) {
            if (StringUtils.isNotEmpty(item.getIsNewUserPic())) {
                ivNewUser.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, item.getIsNewUserPic(), ivNewUser);
            } else {
                ivNewUser.setVisibility(View.GONE);
            }
        }

        //用户等级
        if (ivUserLevel != null) {
            if (StringUtils.isNotEmpty(item.getVideoRoomExperLevelPic())) {
                ivUserLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, item.getVideoRoomExperLevelPic(), ivUserLevel);
            } else {
                ivUserLevel.setVisibility(View.GONE);
            }
        }

        // 财富等级
        if (ivExperLevel != null) {
            if (StringUtils.isNotEmpty(item.getExperLevelPic())) {
                ivExperLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(mContext, item.getExperLevelPic(), ivExperLevel);
            } else {
                ivExperLevel.setVisibility(View.GONE);
            }
        }

//        // 魅力等级
//        if (ivCharmLevel != null) {
//            if (StringUtils.isNotEmpty(item.getCharmLevelPic())) {
//                ivCharmLevel.setVisibility(View.VISIBLE);
//                ImageLoadUtils.loadImage(mContext, item.getCharmLevelPic(), ivCharmLevel);
//            } else {
//                ivCharmLevel.setVisibility(View.GONE);
//            }
//        }

        helper.setOnClickListener(R.id.rl_new_user, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.showUserInfoAct(mContext, item.getUid());
//                if (item.getUserInRoom() != null) {
//                    // todo 开启房间要传入房间类型
//                    RoomFrameActivity.start(mContext, item.getUserInRoom().getUid(), RoomInfo.ROOMTYPE_HOME_PARTY);
//                } else {
//                    UIHelper.showUserInfoAct(mContext,item.getUid());
//                }
            }
        });
    }
}
