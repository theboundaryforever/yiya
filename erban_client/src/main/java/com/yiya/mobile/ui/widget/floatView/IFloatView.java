package com.yiya.mobile.ui.widget.floatView;

import android.view.ViewGroup;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/6/19.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface IFloatView {

    ViewGroup.LayoutParams getParams();

}
