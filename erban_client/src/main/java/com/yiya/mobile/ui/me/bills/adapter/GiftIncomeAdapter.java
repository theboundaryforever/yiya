package com.yiya.mobile.ui.me.bills.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.IncomeInfo;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;
import com.yiya.mobile.utils.ImageLoadUtils;

import java.util.List;

/**
 * Created by Seven on 2017/9/11.
 */

public class GiftIncomeAdapter extends BillBaseAdapter {

    private int mDataType = 1; //1：钻石 2：咿呀

    public GiftIncomeAdapter(List<BillItemEntity> billItemEntityList) {
        super(billItemEntityList);
        addItemType(BillItemEntity.ITEM_NORMAL, R.layout.list_expend_gift_item);
    }

    public void setDataType(int type) {
        mDataType = type;

    }

    @Override
    public void convertNormal(BaseViewHolder baseViewHolder, BillItemEntity billItemEntity) {
        IncomeInfo incomeInfo = billItemEntity.mGiftInComeInfo;
        if (incomeInfo == null) {
            return;
        }
        if (mDataType == 1) {
            baseViewHolder.setText(R.id.tv_gift_income, "+" + incomeInfo.getDiamondNum()+" 钻石")
                    .setImageResource(R.id.image, R.drawable.ic_with_draw_diamond);

        } else {
            baseViewHolder.setText(R.id.tv_gift_income, "+" + incomeInfo.getRedbeanAmount())
                    .setImageResource(R.id.image, R.drawable.icon_ormosia);
        }

        baseViewHolder.setText(R.id.tv_send_name, "送礼人: " + incomeInfo.getTargetNick())
                .setText(R.id.tv_user_name, incomeInfo.getGiftName())
                .setText(R.id.gift_date, TimeUtils.getDateTimeString(incomeInfo.getRecordTime(), "HH:mm:ss"))
                .setTextColor(R.id.tv_gift_income, mContext.getResources().getColor(R.color.color_theme));
        ImageView avatar = baseViewHolder.getView(R.id.img_avatar);
        ImageLoadUtils.loadImage(mContext, incomeInfo.getGiftPic(), avatar);
    }
}
