package com.yiya.mobile.ui.home;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.ui.home.adpater.SignInMissionAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.mission.Missions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/29.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class SignInMissionDialog extends BaseTransparentDialogFragment {

    private static final String KEY_LIST = "KEY_LIST";
    @BindView(R.id.rv_mission)
    RecyclerView rvMission;
    private OnConfirmClickListener mListener;
    private boolean mIsConfirm;

    public static SignInMissionDialog newInstance(List<Missions.CheckInMissions> list) {
        SignInMissionDialog dialog = new SignInMissionDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY_LIST, (ArrayList<? extends Parcelable>) list);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_sign_in_mission;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            List<Missions.CheckInMissions> list = getArguments().getParcelableArrayList(KEY_LIST);
            rvMission.setLayoutManager(new GridLayoutManager(getContext(), 4));
            rvMission.setHasFixedSize(true);
            SignInMissionAdapter adapter = new SignInMissionAdapter(list);
            rvMission.setAdapter(adapter);
        }
    }

    public interface OnConfirmClickListener {

        void onConfirmClick();

        void onDismiss();
    }

    public SignInMissionDialog setOnConfirmClickListener(OnConfirmClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mListener != null && !mIsConfirm) {
            //签到就不回调
            mListener.onDismiss();
        }
    }

    @OnClick(R.id.dtv_confirm)
    public void onViewClicked() {
        if (mListener != null) {
            mListener.onConfirmClick();
        }
        mIsConfirm = true;
        dismiss();
    }

}
