package com.yiya.mobile.ui.newfind.fragment;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;
import com.tongdaxing.xchat_core.bean.OnlinesFriendsBean;
import com.tongdaxing.xchat_core.bean.SquareLoopInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.home.RoomAttentionInfo;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.ui.home.adpater.LiveStreamRecyclerAdapter;
import com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment;
import com.yiya.mobile.ui.newfind.adapter.NewFindFriendsRecyclerAdapter;
import com.yiya.mobile.ui.newfind.presenter.NewFindFriendsPresenter;
import com.yiya.mobile.ui.newfind.view.NewFindFriendsView;
import com.yiya.mobile.ui.square.SquareActivity;
import com.yiya.mobile.ui.widget.LiveTabView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_1;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_2;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_3;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_4;
import static com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment.NewFindFriendsinfo.ITEM_TYPE_5;

@CreatePresenter(NewFindFriendsPresenter.class)
public class NewFindFriendsFragment extends BaseMvpFragment<NewFindFriendsView, NewFindFriendsPresenter> implements NewFindFriendsView, OnRefreshListener, OnLoadMoreListener {

    @BindView(R.id.newfindfriends_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.smart_refresh_layout)
    SmartRefreshLayout mSmartRefreshLayout;

    private NewFindFriendsRecyclerAdapter newFindFriendsRecyclerAdapter;
    private GridLayoutManager mLayoutManager;
    private int mPageSize = 20;
    private int mPageNum = 1;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_newfindfriends;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        GrowingIO.getInstance().setPageVariable(this, "jiaoyoubiaoti", "发现List页");

        mLayoutManager = new GridLayoutManager(mContext, 1);
        newFindFriendsRecyclerAdapter = new NewFindFriendsRecyclerAdapter(null,mContext);
        newFindFriendsRecyclerAdapter.setSpanSizeLookup(new com.chad.library.adapter.base.BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                int value = 1;
                return value;
            }
        });
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(newFindFriendsRecyclerAdapter);
        //recyclerView.addItemDecoration(new MyItemDecoration());

        mSmartRefreshLayout.setOnRefreshListener(this::onRefresh);
        mSmartRefreshLayout.setOnLoadMoreListener(this::onLoadMore);

        newFindFriendsRecyclerAdapter.setOnItemClickListener(new LiveStreamRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int itemType, View view, int position, LiveTabView.TabInfo tabInfo) {
                switch (itemType){
                    case ITEM_TYPE_2:
                        break;
                    case ITEM_TYPE_3:
                        break;
                    case ITEM_TYPE_5:
                        SquareActivity.start(mContext);
                        break;
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (newFindFriendsRecyclerAdapter != null) {
            if (newFindFriendsRecyclerAdapter.getData() == null || newFindFriendsRecyclerAdapter.getData().size() == 0) {
                getDate(mPageNum=1);
            }
        }
    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {  //下啦
        mPageNum = 1;
        getDate(mPageNum);
    }

    @Override
    public void onLoadMore(RefreshLayout refreshlayout) {  //上拉
        getMvpPresenter().onLoad(mPageNum+1,mPageSize);
       // getDate(mPageNum+1);
    }

    /**请求页面数据*/
    private void getDate(int pageNum){
        mPageNum = pageNum;
        getMvpPresenter().getFriendsHomeList(mPageNum,mPageSize);
    }

    @Override
    public void onSucceed(int pageNum, List<NewFindFriendsinfo> data) {
        Log.d("交友广场配置数据","Nlist.size="+data.size()+",pageNum="+pageNum);
        if (pageNum == 1) {
            newFindFriendsRecyclerAdapter.setData(data);
        } else {
            newFindFriendsRecyclerAdapter.notifyItemRangeChanged(newFindFriendsRecyclerAdapter.getData().size(),data.size());
        }
        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
    }

    @Override
    public void onFailed(String message) {
        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
    }

    public static class NewFindFriendsinfo implements MultiItemEntity {

        public static final int ITEM_TYPE_1 = 1;  //第一列文本样式
        public static final int ITEM_TYPE_2 = 2;  //第二列已在语聊房间的用户头像列表样式（已关注）
        public static final int ITEM_TYPE_3 = 3;  //第三列公聊大厅样式
        public static final int ITEM_TYPE_4 = 4;  //第四列文本样式
        public static final int ITEM_TYPE_5 = 5;  //第五列语聊房间列表样式（在线）
        private int itemType;
        private int roomIndex = 0;

        private List<OnlinesFriendsBean> onlinesFriendsBeanList;
        private List<SquareLoopInfo> squareLoopInfoList;
        private HomeYuChatListInfo homeYuChatListInfo;

        public NewFindFriendsinfo(){};

        public NewFindFriendsinfo(int type){
            itemType = type;
        }

        public void setItemType(int itemType) {
            this.itemType = itemType;
        }

        public int getRoomIndex() {
            return roomIndex;
        }

        public void setRoomIndex(int roomIndex) {
            this.roomIndex = roomIndex;
        }

        public List<OnlinesFriendsBean> getOnlinesFriendsBeanList() {
            return onlinesFriendsBeanList;
        }

        public void setOnlinesFriendsBeanList(List<OnlinesFriendsBean> onlinesFriendsBeanList) {
            this.onlinesFriendsBeanList = onlinesFriendsBeanList;
        }

        public List<SquareLoopInfo> getSquareLoopInfoList() {
            return squareLoopInfoList;
        }

        public NewFindFriendsinfo setSquareLoopInfoList(List<SquareLoopInfo> squareLoopInfoList) {
            this.squareLoopInfoList = squareLoopInfoList;
            return this;
        }

        public HomeYuChatListInfo getHomeYuChatListInfo() {
            return homeYuChatListInfo;
        }

        public void setHomeYuChatListInfo(HomeYuChatListInfo homeYuChatListInfo) {
            this.homeYuChatListInfo = homeYuChatListInfo;
        }

        @Override
        public int getItemType() {
            return itemType;
        }
    }

    private int mOffsetPadding = 0;
    private int mOffsetContext = 0;

    private class MyItemDecoration extends RecyclerView.ItemDecoration {

        private MyItemDecoration() {
            mOffsetPadding = DisplayUtils.dip2px(mContext, 15);
            mOffsetContext = DisplayUtils.dip2px(mContext, 10);
        }

        @Override
        public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.onDraw(c, parent, state);
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            int pos = parent.getChildAdapterPosition(view);
            NewFindFriendsinfo info = newFindFriendsRecyclerAdapter.getData().get(pos);
            boolean tmpBool = false;
            if (newFindFriendsRecyclerAdapter.getData() != null && info != null) {
                if (info.getItemType() == ITEM_TYPE_5) {
                    tmpBool = true;
                    if (info.getRoomIndex() % 2 == 0) {
                        outRect.left = mOffsetPadding;
                        outRect.right = mOffsetContext;
                    } else {
                        outRect.left = mOffsetContext / 3;
                        outRect.right = mOffsetPadding;
                    }
                    outRect.left = mOffsetPadding;
                    outRect.right = mOffsetContext;
                    outRect.bottom = mOffsetContext / 5 * 4;

                }
                if(info.getItemType() == ITEM_TYPE_2){

                }
            }
            outRect.top = 0;
            if (!tmpBool) {
                outRect.right = 0;
                outRect.left = 0;
                outRect.bottom = 0;
            }
        }
    }
}
