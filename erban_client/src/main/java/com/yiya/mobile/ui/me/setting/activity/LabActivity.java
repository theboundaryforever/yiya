package com.yiya.mobile.ui.me.setting.activity;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.yiya.mobile.ChatApplicationLike;
import com.yiya.mobile.base.activity.BaseActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.Env;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.pref.CommonPref;

/**
 *
 * Created by chenran on 2017/10/16.
 */
public class LabActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab);

        int enviroment = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("enviroment");
        //根据ID找到RadioGroup实例
        RadioGroup group = (RadioGroup) this.findViewById(R.id.radioGroup);
        RadioButton button = (RadioButton) findViewById(R.id.product);
        RadioButton button1 = (RadioButton) findViewById(R.id.test);
        if (enviroment == 0) {
            button.setChecked(true);
        } else {
            button1.setChecked(true);
        }
        //绑定一个匿名监听器
        group.setOnCheckedChangeListener((arg0, arg1) -> {
            //获取变更后的选中项的ID
            if (arg1 == R.id.test) {
                CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("enviroment", 1);
            } else {
                CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).putInt("enviroment", 0);
            }
            Env.instance().init();
            ChatApplicationLike.initRxNet(BasicConfig.INSTANCE.getAppContext(), UriProvider.JAVA_WEB_URL);
            CoreManager.getCore(IAuthCore.class).logout();
            finish();
        });
    }
}
