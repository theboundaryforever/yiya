package com.yiya.mobile.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.jude.rollviewpager.HintView;
import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.Util;
import com.jude.rollviewpager.hintview.ShapeHintView;

/**
 * Created by huangmeng1 on 2018/1/6.
 */

public class Banner extends RollPagerView {


    public Banner(Context context) {
        super(context);
        setHintView(null);
    }

    public Banner(Context context, AttributeSet attrs) {
        super(context, attrs);
        setHintView(null);
    }

    public Banner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setHintView(null);
    }


    private boolean isRequestDisallow = false;
    // 滑动距离及坐标 归还父控件焦点
    private float xDistance, yDistance, xLast, yLast, mLeft;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (isRequestDisallow) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xDistance = yDistance = 0f;
                xLast = ev.getX();
                yLast = ev.getY();
                mLeft = ev.getX();
                break;
            case MotionEvent.ACTION_MOVE:
                final float curX = ev.getX();
                final float curY = ev.getY();

                xDistance += Math.abs(curX - xLast);
                yDistance += Math.abs(curY - yLast);
                xLast = curX;
                yLast = curY;
                if (isRequestDisallow) {
                    if (mLeft < 100 || xDistance < yDistance) {
                        getParent().requestDisallowInterceptTouchEvent(false);
                    } else {
                        getParent().requestDisallowInterceptTouchEvent(true);
                    }
                }

                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void setHintView(HintView hintview) {
        super.setHintView(new ColorPointHintView(getContext(), Color.parseColor("#661E1E1E"), Color.parseColor("#33000000")));
    }

    private class ColorPointHintView extends ShapeHintView {
        private int focusColor;
        private int normalColor;

        public ColorPointHintView(Context context, int focusColor, int normalColor) {
            super(context);
            this.focusColor = focusColor;
            this.normalColor = normalColor;
        }

        @Override
        public Drawable makeFocusDrawable() {
            GradientDrawable dot_focus = new GradientDrawable();
            dot_focus.setColor(focusColor);
            dot_focus.setCornerRadius(Util.dip2px(getContext(), 3));
            dot_focus.setSize(Util.dip2px(getContext(), 11), Util.dip2px(getContext(), 6));
            return dot_focus;
        }

        @Override
        public Drawable makeNormalDrawable() {
            GradientDrawable dot_normal = new GradientDrawable();
            dot_normal.setColor(normalColor);
            dot_normal.setCornerRadius(Util.dip2px(getContext(), 3));
            dot_normal.setSize(Util.dip2px(getContext(), 6), Util.dip2px(getContext(), 6));
            return dot_normal;
        }
    }

}
