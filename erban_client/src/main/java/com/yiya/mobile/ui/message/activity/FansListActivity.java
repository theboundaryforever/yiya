package com.yiya.mobile.ui.message.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.message.fragment.FansListFragment;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.Constants;

/**
 * 我的粉丝
 *
 * @author dell
 */
public class FansListActivity extends BaseActivity {

    private AppToolBar mToolBar;

    public static void start(Context context) {
        Intent intent = new Intent(context, FansListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "消息-粉丝页");

        setContentView(R.layout.activity_fans);
        mToolBar = findView(R.id.toolbar);
        mToolBar.getTvTitle().getPaint().setFakeBoldText(true);
        mToolBar.setOnBackBtnListener(view -> finish());

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, FansListFragment.newInstance(Constants.FAN_NO_MAIN_PAGE_TYPE),
                        FansListFragment.class.getSimpleName())
                .commitAllowingStateLoss();


    }
}
