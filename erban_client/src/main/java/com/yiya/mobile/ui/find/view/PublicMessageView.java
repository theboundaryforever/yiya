package com.yiya.mobile.ui.find.view;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.room.avroom.other.ScrollSpeedLinearLayoutManger;
import com.yiya.mobile.ui.widget.itemdecotion.DividerItemDecoration;
import com.yiya.mobile.ui.widget.marqueeview.Utils;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.NimUIKit;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PublicChatRoomAttachment;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_framework.im.IMReportRoute;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * 直播间消息界面
 *
 * @author chenran
 * @date 2017/7/26
 */
public class PublicMessageView extends FrameLayout {
    private RecyclerView messageListView;
    private ImageView ivBottomTip;
    private MessageAdapter mMessageAdapter;
    private List<ChatRoomMessage> tempMessages = new ArrayList<>();
    private ScrollSpeedLinearLayoutManger layoutManger;
    private CompositeDisposable compositeDisposable;

    public PublicMessageView(Context context) {
        this(context, null);
    }

    public PublicMessageView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public PublicMessageView(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        init(context);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (compositeDisposable == null)
            return;
        //新消息
        Disposable crMsgDisposable = IMNetEaseManager.get().getChatRoomMsgFlowable()
                .subscribe(messages -> {
                    if (messages.size() == 0) return;
                    ChatRoomMessage chatRoomMessage = messages.get(0);
                    if (checkNeedMsg(chatRoomMessage)) {
                        if (NimUIKit.isMySelf(chatRoomMessage.getImChatRoomMember().getAccount())) {
                            //自己发的消息
                            addNewMsgs(messages);
                        } else {
                            //他人发的消息
                            int lastCompletelyVisibleItemPosition = layoutManger.findLastCompletelyVisibleItemPosition();
                            if (lastCompletelyVisibleItemPosition != mMessageAdapter.getItemCount() - 1) {
                                //不是最底部则不滚动
                                ivBottomTip.setVisibility(VISIBLE);
                                onCurrentRoomReceiveNewMsg(messages);
                            } else {
                                //当前在最底部则自动滚动
                                addNewMsgs(messages);
                            }
                        }
                    }
                });
        compositeDisposable.add(crMsgDisposable);
        //历史消息
        compositeDisposable.add(IMNetEaseManager.get().getChatRoomEventObservable()
                .subscribe(roomEvent -> {
                    if (roomEvent == null ||
                            roomEvent.getEvent() != RoomEvent.RECEIVE_MSG) return;
                    ChatRoomMessage chatRoomMessage = roomEvent.getChatRoomMessage();
                    if (checkNeedMsg(chatRoomMessage)) {
                        tempMessages.clear();
                        tempMessages.add(chatRoomMessage);
                        addNewMsgs(tempMessages);
                    }
                }));
    }

    private void addNewMsgs(List<ChatRoomMessage> messages) {
        onCurrentRoomReceiveNewMsg(messages);
        messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
    }

    /**
     * 检查是否是公屏需要的消息
     */
    private boolean checkNeedMsg(ChatRoomMessage chatRoomMessage) {
        if (IMReportRoute.sendPublicMsgNotice.equalsIgnoreCase(chatRoomMessage.getRoute())) {
            IMCustomAttachment attachment = (IMCustomAttachment) chatRoomMessage.getAttachment();
            return attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM;//公屏消息
        }
        return false;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    public void clear() {
        if (mMessageAdapter != null) {
            mMessageAdapter.getData().clear();
            mMessageAdapter.notifyDataSetChanged();
        }
    }

    private void init(Context context) {
        compositeDisposable = new CompositeDisposable();
        // 内容区域
        initMessageView(context);
        // 底部有新消息
        initBottomTips(context);
    }

    private void initBottomTips(Context context) {
        ivBottomTip = new ImageView(context);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        ivBottomTip.setLayoutParams(params);
        ivBottomTip.setImageResource(R.drawable.bg_square_bottom_new_msg);
        ivBottomTip.setOnClickListener(v -> {
            ivBottomTip.setVisibility(GONE);
            messageListView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
        });
        ivBottomTip.setVisibility(GONE);
        addView(ivBottomTip);
    }

    private void initMessageView(Context context) {
        layoutManger = new ScrollSpeedLinearLayoutManger(context);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.rightMargin = Utils.dip2px(context, 5);
        messageListView = new RecyclerView(context);
        messageListView.setLayoutParams(params);
//        messageListView.setOverScrollMode(OVER_SCROLL_NEVER);
        messageListView.setHorizontalScrollBarEnabled(false);
        messageListView.setLayoutManager(layoutManger);
        messageListView.addItemDecoration(new DividerItemDecoration(context, layoutManger.getOrientation(), 3, R.color.transparent));
        mMessageAdapter = new MessageAdapter();
        mMessageAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            //跳转个人主页
            ChatRoomMessage chatRoomMessage = (ChatRoomMessage) adapter.getData().get(position);
            String account = chatRoomMessage.getImChatRoomMember().getAccount();
            if (!TextUtils.isEmpty(account)) {
                UIHelper.showUserInfoAct(context, Long.parseLong(account));
            }
        });
        messageListView.setAdapter(mMessageAdapter);

        messageListView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (layoutManger.findLastVisibleItemPosition() == mMessageAdapter.getItemCount() - 1) {
                        ivBottomTip.setVisibility(GONE);
                    }
                }
            }
        });
        addView(messageListView);
    }

    public void onCurrentRoomReceiveNewMsg(List<ChatRoomMessage> messages) {
        if (ListUtils.isListEmpty(messages)) {
            return;
        }
        mMessageAdapter.addData(messages);
    }

    private static class MessageAdapter extends BaseQuickAdapter<ChatRoomMessage, BaseViewHolder> {

        MessageAdapter() {
            super(R.layout.list_item_public_chatrrom_msg);
        }

        @Override
        protected void convert(BaseViewHolder helper, ChatRoomMessage chatRoomMessage) {
            if (chatRoomMessage == null) return;
            if (IMReportRoute.sendPublicMsgNotice.equalsIgnoreCase(chatRoomMessage.getRoute())) {
                IMCustomAttachment imCustomAttachment = chatRoomMessage.getAttachment();
                if (imCustomAttachment instanceof PublicChatRoomAttachment) {
                    PublicChatRoomAttachment attachment = (PublicChatRoomAttachment) imCustomAttachment;
                    if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM) {
                        IMChatRoomMember imChatRoomMember = chatRoomMessage.getImChatRoomMember();
                        if (imChatRoomMember == null) {
                            return;
                        }
                        ImageView userIcon = helper.getView(R.id.iv_user_icon_chat_room);
                        TextView userNick = helper.getView(R.id.tv_user_nick_chat_room);
                        TextView content = helper.getView(R.id.tv_content_chat_room);
                        ImageView ivNewUser = helper.getView(R.id.iv_new_user);
                        ImageView ivExper = helper.getView(R.id.iv_exper_level);
                        ImageView ivCharm = helper.getView(R.id.iv_charm_level);

                        helper.setGone(R.id.iv_exper_level, !TextUtils.isEmpty(imChatRoomMember.getExper_level_pic()))
                                .setGone(R.id.iv_charm_level, !TextUtils.isEmpty(imChatRoomMember.getCharm_level_pic()))
                                .addOnClickListener(R.id.iv_user_icon_chat_room);

                        //萌新
                        if (ivNewUser != null) {
                            if (StringUtils.isNotEmpty(imChatRoomMember.getNew_user_pic())) {
                                ivNewUser.setVisibility(View.VISIBLE);
                                ImageLoadUtils.loadImage(mContext, imChatRoomMember.getNew_user_pic(), ivNewUser);
                                LogUtil.i("ivNewUser: "+imChatRoomMember.getNew_user_pic());
                            } else {
                                ivNewUser.setVisibility(View.GONE);
                            }
                        }

                        ImageLoadUtils.loadCircleImage(mContext, imChatRoomMember.getAvatar(), userIcon, R.drawable.nim_avatar_default);
                        ImageLoadUtils.loadImage(mContext, imChatRoomMember.getExper_level_pic(), ivExper);
                        ImageLoadUtils.loadImage(mContext, imChatRoomMember.getCharm_level_pic(), ivCharm);
                        userNick.setText(imChatRoomMember.getNick());
                        content.setText(Html.fromHtml(attachment.getHtml()));
                        content.setTextColor(Color.parseColor(StringUtils.isNotEmpty(attachment.getTxtColor()) ? attachment.getTxtColor() : "#FF495C"));
                    }
                }
            }
        }
    }

    public List<ChatRoomMessage> getChatRoomMessages() {
        return mMessageAdapter.getData();
    }

}
