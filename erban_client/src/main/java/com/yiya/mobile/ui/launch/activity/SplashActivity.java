package com.yiya.mobile.ui.launch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.model.safetychecker.SafetyCheckerModel;
import com.yiya.mobile.ui.MainActivity;
import com.juxiao.safetychecker.SafetyChecker;
import com.juxiao.safetychecker.bean.SafetyCheckResultBean;
import com.netease.nim.uikit.glide.GlideApp;
import com.tinkerpatch.sdk.TinkerPatch;
import com.tongdaxing.erban.BuildConfig;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.initial.IInitView;
import com.tongdaxing.xchat_core.initial.InitInfo;
import com.tongdaxing.xchat_core.initial.InitPresenter;
import com.tongdaxing.xchat_core.manager.RtcEngineManager;
import com.tongdaxing.xchat_core.utils.ThreadUtil;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;

import java.lang.ref.WeakReference;

/**
 * @author xiaoyu
 * @date 2017/12/30
 */
@CreatePresenter(InitPresenter.class)
public class SplashActivity extends BaseMvpActivity<IInitView, InitPresenter> implements IInitView, View.OnClickListener {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private ImageView ivActivity;
    private InitInfo mLocalSplashVo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            Intent mainIntent = getIntent();
            String action = mainIntent.getAction();
            if (mainIntent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action)) {
                finish();
                return;
            }
        }
        setContentView(R.layout.activity_splash);
        ivActivity = (ImageView) findViewById(R.id.iv_activity);
        ivActivity.setOnClickListener(this);
        setSwipeBackEnable(false);
        if (savedInstanceState != null) {
            // 从堆栈恢复，不再重复解析之前的intent
            setIntent(new Intent());
        }
        initiate();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    protected void initiate() {
        mHandler = new MHandler(this);

        showSplash();


        getMvpPresenter().init();
        RtcEngineManager.get().initRtcEngine();// 声网引擎初始化

        safetyCheck();
        if (BuildConfig.TINKER_ENABLE) {
            TinkerPatch.with().fetchPatchUpdate(true);
        }
//
//        // 测试时直接进入首页
//        MainActivity.start(this);
//        finish();
    }

    private void showSplash() {
//        if (true) {
//            MainActivity.start(this);
//            return;
//        }

        // 不过期的，并且已经下载出来图片的闪屏页数据
        mLocalSplashVo = getMvpPresenter().getLocalSplashVo();
        if (mLocalSplashVo != null && mLocalSplashVo.getSplashVo() != null
                && !TextUtils.isEmpty(mLocalSplashVo.getSplashVo().getPict())) {
            GlideApp.with(this)
                    .load(mLocalSplashVo.getSplashVo().getPict())
                    .into(ivActivity);
            animation();
        } else {
            // 其他情况显示默认图（style里面已经配好了）
            MainActivity.start(this);
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
//        Log.d(TAG, "start finish: ----->");
//        mHandler.sendEmptyMessageAtTime(1, 5 * 1000);
    }


    private boolean needJump = false;

    private void animation() {
        ivActivity.setAlpha(0.4F);
        ivActivity.animate().alpha(1).setDuration(1000).setStartDelay(0).start();
        ivActivity.animate().setDuration(2000).withEndAction(() -> {
            if (needJump) {
                return;
            }
            MainActivity.start(this);
            finish();
        }).setStartDelay(1000).start();
    }

    /**
     * 安全检测
     */
    private void safetyCheck() {
        ThreadUtil.getThreadPool().execute(() -> {
            SafetyCheckResultBean checkResult = SafetyChecker.getInstance().check(this.getApplicationContext());
            //不安全的，上报
            if (checkResult.getCheckStatus() != 0) {
                ThreadUtil.runOnUiThread(() -> {
                    SafetyCheckerModel safetyCheckerModel = new SafetyCheckerModel();
                    safetyCheckerModel.reportSafetyCheckResult(checkResult);
                });
            }
        });
    }

    @Override
    public void onInitSuccess(InitInfo data) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_activity) {
            if (mLocalSplashVo == null || mLocalSplashVo.getSplashVo() == null) {
                return;
            }
            String link = mLocalSplashVo.getSplashVo().getLink();
            int type = mLocalSplashVo.getSplashVo().getType();
            if (TextUtils.isEmpty(link) || type == 0) {
                return;
            }
            needJump = true;
            Intent intent = new Intent();
            intent.putExtra("url", link);
            intent.putExtra("type", type);
            MainActivity.start(this, intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (ivActivity != null) {
            ivActivity.clearAnimation();
        }
    }

    private MHandler mHandler;

    private static class MHandler extends Handler {

        private WeakReference<SplashActivity> mWeakReference;

        private MHandler(SplashActivity activity) {
            mWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                SplashActivity activity = mWeakReference.get();
                if (activity != null) {
//                    Log.d(TAG, "start finish  handler: ----->");
                    activity.finish();
                }
            }
        }
    }

}
