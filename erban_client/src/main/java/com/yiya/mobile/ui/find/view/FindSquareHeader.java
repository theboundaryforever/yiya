package com.yiya.mobile.ui.find.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.ui.me.shopping.activity.ShopActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.find.family.SquareMemberInfo;

/**
 * 创建者      Created by dell
 * 创建时间    2018/12/6
 * 描述        ${}
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class FindSquareHeader extends FrameLayout implements View.OnClickListener {

    private View mRootView;
    private TextView mCount;
//    private FrameLayout enterChat;
//    private RecyclerView mRecyclerView;
    private ImageView shop, gashapon;

//    private LinearLayout defaultAvatar;

//    private SquareMemberAdapter mAdapter;

    public FindSquareHeader(@NonNull Context context) {
        super(context);
        initiate(context);
        setOnListener();
    }

    private void initiate(Context context) {
        mRootView = View.inflate(context, R.layout.fragment_find_square_header, this);

//        mRecyclerView = mRootView.findViewById(R.id.avatar_container);
//        defaultAvatar = mRootView.findViewById(R.id.default_avatar);
//        enterChat = mRootView.findViewById(R.id.enter_chat);
        gashapon = mRootView.findViewById(R.id.ic_gashapon);
        shop = mRootView.findViewById(R.id.ic_shop);
        mCount = mRootView.findViewById(R.id.count);

        int width = (ScreenUtil.getDisplayWidth() - ScreenUtil.dip2px(44)) / 2;

        //调整宽高
        ViewGroup.LayoutParams gashaponParams = gashapon.getLayoutParams();
//        ViewGroup.LayoutParams chatParams = enterChat.getLayoutParams();
        ViewGroup.LayoutParams shopParams = shop.getLayoutParams();

        gashaponParams.width = shopParams.width = width;
        gashaponParams.height = shopParams.height = width / 2;
//        chatParams.height = ScreenUtil.dip2px(81);

        gashapon.setLayoutParams(gashaponParams);
//        enterChat.setLayoutParams(chatParams);
        shop.setLayoutParams(shopParams);

//        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
//        mAdapter = new SquareMemberAdapter();
////        mRecyclerView.setAdapter(mAdapter);
    }

    private void setOnListener() {
        shop.setOnClickListener(this);
//        enterChat.setOnClickListener(this);
        gashapon.setOnClickListener(this);
//        mRecyclerView.setOnClickListener(this);
    }

    /**
     * 设置头像与人数
     */
    public void setAvatarData(SquareMemberInfo memberInfo) {
//        if (memberInfo == null) {
//            mAdapter.setNewData(null);
//            mRecyclerView.setVisibility(GONE);
//            defaultAvatar.setVisibility(VISIBLE);
//        } else {
//            mCount.setText(String.valueOf(memberInfo.getCount() + "人正在聊天"));
//            if (ListUtils.isListEmpty(memberInfo.getUlist())) {
//                mAdapter.setNewData(null);
//                mRecyclerView.setVisibility(GONE);
//                defaultAvatar.setVisibility(VISIBLE);
//            } else {
//                defaultAvatar.setVisibility(GONE);
//                mRecyclerView.setVisibility(VISIBLE);
//                List<SquareMemberInfo.Avatar> avatars = memberInfo.getUlist();
//                avatars.add(null);
//                mAdapter.setNewData(avatars);
//            }
//        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.avatar_container:
//            case R.id.enter_chat:
//                SquareActivity.start(getContext());
//                break;
            case R.id.ic_shop:
                ShopActivity.start(getContext(), true, 0);
                break;
            case R.id.ic_gashapon:
                CommonWebViewActivity.start(getContext(), BaseUrl.GASHAPON);
                break;
            default:
                break;
        }
    }
}
