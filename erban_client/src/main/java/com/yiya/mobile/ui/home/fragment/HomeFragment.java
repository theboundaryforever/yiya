package com.yiya.mobile.ui.home.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tongdaxing.xchat_core.PreferencesUtils;
import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.presenter.home.HomePresenter;
import com.yiya.mobile.presenter.home.IHomeView;
import com.yiya.mobile.ui.home.adpater.HomeIndicatorAdapter;
import com.yiya.mobile.ui.search.SearchActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.bean.LoginConf;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.config.SpEvent;
import com.tongdaxing.xchat_framework.util.util.SpUtils;
import com.zyyoona7.lib.EasyPopup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.OnClick;

/**
 * 首页布局界面
 * 刷新布局SwipeRefreshLayout + 协调布局CoordinatorLayout + ViewPager
 *
 * @zwk
 */
@CreatePresenter(HomePresenter.class)
public class HomeFragment extends BaseMvpFragment<IHomeView, HomePresenter> implements IHomeView, HomeIndicatorAdapter.OnItemSelectListener, BaseQuickAdapter.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, ViewPager.OnPageChangeListener {
    public static final String TAG = "HomeFragment";

    public static final String ACTION_SCROLL_TO_YU_CHAT = "action_scroll_to_chat";

    //滑动分类
    private ViewPager vpHome;
    private MagicIndicator indicator;
    private int currentPosition = 0;

    private CommonNavigator mCommonNavigator;
    private EasyPopup mEasyPopup;
    private List<Fragment> mFragmentList;

    private HomeReceiver mReceiver;
    private ImageView ivNew;
    private boolean mIsCreateRoomClicked;//创建房间是否已经点过

    List<TabInfo> tabInfoList;
    private List<TabInfo> TabList;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_home_main;
    }

    @Override
    public void onFindViews() {
        vpHome = mView.findViewById(R.id.vp_home_content);
        vpHome.setOffscreenPageLimit(3);
        currentPosition = 0;
        indicator = mView.findViewById(R.id.mi_home_indicator);
        ivNew = mView.findViewById(R.id.iv_new);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSetListener() {

        vpHome.addOnPageChangeListener(this);
    }

    @Override
    public void initiate() {
//        GrowingIO.getInstance().setPageName(this, "首页");
        mReceiver = new HomeReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_SCROLL_TO_YU_CHAT);

        mIsCreateRoomClicked = (boolean) SpUtils.get(BasicConfig.INSTANCE.getAppContext(), SpEvent.isCreateRoomClicked, false);
//        ivNew.setVisibility(mIsCreateRoomClicked ? View.GONE : View.VISIBLE);

        LocalBroadcastManager.getInstance(mContext).registerReceiver(mReceiver, intentFilter);
    }


    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
    }

    @Override
    public void onItemSelect(int position) {
        currentPosition = position;
        vpHome.setCurrentItem(position);
    }

    public void initTabLayout(List<TabInfo> tabs) {
        if (mFragmentList == null) {
            mFragmentList = new ArrayList<>();
        } else {
            mFragmentList.clear();
        }
        if(tabInfoList == null){
            tabInfoList = new ArrayList<>();
        }else{
            tabInfoList.clear();
        }
        if(tabs!=null&&tabs.size()>0){
            for (int i = 0; i < tabs.size(); i++) {
                tabInfoList.add(new TabInfo(tabs.get(i).getId(), tabs.get(i).getName()));
                HomeLiveStreamFragment hot = new HomeLiveStreamFragment();
                Bundle bundle = new Bundle();
                bundle.putString("tabId", String.valueOf(tabs.get(i).getId()));
                hot.setArguments(bundle);
                mFragmentList.add(hot);
            }
        }

       /* //关注
        HomeFriendsFragment attention = new HomeFriendsFragment();*/

        //直播
       /* HomeLiveStreamFragment hot = new HomeLiveStreamFragment();*/
//        HomeLiveStreamMainFragment hot = new HomeLiveStreamMainFragment();

       /* //语聊
        HomeYuChatFragment chatFragment = new HomeYuChatFragment();*/

        /*mFragmentList.add(hot);*/
        /*mFragmentList.add(chatFragment);
        mFragmentList.add(attention);*/


        HomeIndicatorAdapter mMsgIndicatorAdapter = new HomeIndicatorAdapter(mContext, tabInfoList);
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        mCommonNavigator = new CommonNavigator(mContext);
        //不要默认等分
//        commonNavigator.setAdjustMode(true);
        mCommonNavigator.setAdapter(mMsgIndicatorAdapter);
        indicator.setNavigator(mCommonNavigator);
        LinearLayout titleContainer = mCommonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        vpHome.setAdapter(new BaseIndicatorStateAdapter(getChildFragmentManager(), mFragmentList));
        mCommonNavigator.onPageSelected(tabInfoList.size() > currentPosition ? currentPosition : 0);
        vpHome.setCurrentItem(tabInfoList.size() > currentPosition ? currentPosition : 0);
        ViewPagerHelper.bind(indicator, vpHome);
        //绑定完成才能控制选中

    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
//        if (accountInfo != null) {
//        }
//        try {
//            currentPosition = 1;
//            mCommonNavigator.onPageSelected(currentPosition);
//            vpHome.setCurrentItem(currentPosition, true);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }


    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        boolean first = (boolean) SpUtils.get(mContext, SpEvent.first_open, true);
//        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
//        UserInfo currentUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(myUid);
        List<LoginConf> loginConfs = DemoCache.readLoginConf();
        if (first && ListUtils.isNotEmpty(loginConfs) && userInfo != null) {//第一次启动的状态
            SpUtils.put(mContext, SpEvent.first_open, false);
            LoginConf loginConf = null;
            for (LoginConf lcTmp : loginConfs) {
                if (lcTmp.getGender() == userInfo.getGender()){
                    loginConf = lcTmp;
                }
            }
            if (loginConf != null){
                currentPosition = loginConf.getPageCode();
            }
        }

        mCommonNavigator.onPageSelected(tabInfoList.size() > currentPosition ? currentPosition : 0);
        vpHome.setCurrentItem(tabInfoList.size() > currentPosition ? currentPosition : 0);
    }

    @OnClick({R.id.user_scan_iv, R.id.home_more_iv})
    void onClickView(View view) {
        switch (view.getId()) {
            case R.id.user_scan_iv:
                UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getHomeSearchBtn());
                SearchActivity.start(mContext);
                break;

            case R.id.home_more_iv:
                //幸运抽奖
                CommonWebViewActivity.start(mContext, BaseUrl.LUCK_DRAW);
                break;
            default:
        }
    }

    @Override
    public void onRefresh() {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        currentPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onDestroy() {
        if (mReceiver != null) {
            LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReceiver);
        }
        super.onDestroy();

    }

    /**获取标题数据*/
    @Override
    public void setTitle(List<TabInfo> data) {
        initTabLayout(data);
    }


    private class HomeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() == ACTION_SCROLL_TO_YU_CHAT) {
                try {
                    currentPosition = 2;
                    mCommonNavigator.onPageSelected(currentPosition);
                    vpHome.setCurrentItem(currentPosition, true);

                    ((HomeYuChatFragment) mFragmentList.get(2)).setPagerScrollByTag("热门");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
