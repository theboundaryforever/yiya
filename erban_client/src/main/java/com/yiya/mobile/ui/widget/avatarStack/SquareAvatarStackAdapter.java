package com.yiya.mobile.ui.widget.avatarStack;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseViewHolder;
import com.juxiao.library_utils.DisplayUtils;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/12.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class SquareAvatarStackAdapter extends AvatarStackAdapter {
    @Override
    protected void convert(BaseViewHolder helper, String url) {
        super.convert(helper, url);
        getAvatarRiv().setBorderColor(Color.WHITE);
        getAvatarRiv().getLayoutParams().height = DisplayUtils.dip2px(mContext, 40);
        getAvatarRiv().getLayoutParams().width = DisplayUtils.dip2px(mContext, 40);
    }
}
