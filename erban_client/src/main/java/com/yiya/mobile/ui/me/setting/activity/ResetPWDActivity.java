package com.yiya.mobile.ui.me.setting.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.yiya.mobile.ui.me.MePresenter;
import com.yiya.mobile.ui.me.task.view.IMeView;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangsongzhou
 * @date 2019/5/6
 */
@CreatePresenter(MePresenter.class)
public class ResetPWDActivity extends BaseMvpActivity<IMeView, MePresenter> implements IMeView {

    public static void start(Context context) {
        Intent intent = new Intent(context, ResetPWDActivity.class);
        context.startActivity(intent);
    }


    private UserInfo mUserInfo;

    private RootClass mRooViewClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GrowingIO.getInstance().setPageName(this, "重置密码");
        setContentView(R.layout.activity_reset_pwd);


        mUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();


        ((AppToolBar) findViewById(R.id.toolbar)).setOnBackBtnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ViewStub viewStub;
        viewStub = (ViewStub) findViewById(R.id.view_stub_reset);
        View rootView = viewStub.inflate();
        mRooViewClass = new ResetLayoutClass(rootView);

//        if (TextUtils.isEmpty(mUserInfo.getPhone())) {
//            viewStub = (ViewStub) findViewById(R.id.view_stub_setting);
//            View rootView = viewStub.inflate();
//
//            new SettingLayoutClass(rootView);
//        } else {
//            viewStub = (ViewStub) findViewById(R.id.view_stub_reset);
//            View rootView = viewStub.inflate();
//            mRooViewClass = new ResetLayoutClass(rootView);
//        }

    }

    private void changeState(ImageView eyeIv, EditText pwdEdt, boolean state) {
        if (state) {
            eyeIv.setImageResource(R.drawable.icon_eye_selector);
            pwdEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            eyeIv.setImageResource(R.drawable.icon_eye_normal);
            pwdEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRooViewClass != null) {
            if (((ResetLayoutClass) mRooViewClass).mCodeDownTimer != null) {
                ((ResetLayoutClass) mRooViewClass).mCodeDownTimer.cancel();
                ((ResetLayoutClass) mRooViewClass).mCodeDownTimer = null;
            }
        }
    }

    @Override
    public void callbackSendSms() {
        if (mRooViewClass != null) {
            mRooViewClass.callbackSendSms();
        }

    }

    @Override
    public void callbackSendSmsFail(String message) {
        if (mRooViewClass != null) {
            mRooViewClass.callbackSendSmsFail(message);
        }
    }

    @Override
    public void callbackUserSetPwd(int code, String message) {
        getDialogManager().dismissDialog();
        toast(message);
        if (code == 200) {
            finish();
        }

    }

    @Override
    public void callbackUserSetPwdFail(int code, String message) {
        getDialogManager().dismissDialog();
        toast(message);
    }

    public class RootClass {

        public void callbackSendSms() {

        }

        public void callbackSendSmsFail(String message) {
        }
    }

    public class ResetLayoutClass extends RootClass implements TextWatcher {

        @BindView(R.id.phone_tv)
        EditText mPhoneEdt;
        @BindView(R.id.input_code_edt)
        EditText mInputCodeEdt;
        @BindView(R.id.get_code_btn)
        TextView mGetCodeBtn;
        @BindView(R.id.input_new_pwd_edt)
        EditText mInputNewPwdEdt;
        /*@BindView(R.id.input_new_pwd_iv)
        ImageView mInputNewPwdIv;
        @BindView(R.id.input_pwd_ensure_edt)
        EditText mInputEnsurePwdEdt;
        @BindView(R.id.input_pwd_ensure_iv)
        ImageView mInputEnsurePwdIv;*/

        @BindView(R.id.commit_btn)
        Button mCommitBtn;


        public CodeDownTimer mCodeDownTimer;

        private boolean isShowNewPwd, isShowEnsurePwd;


        public ResetLayoutClass(View rootView) {
            ButterKnife.bind(this, rootView);

            mPhoneEdt.setText(mUserInfo.getPhone());

            GrowingIO.getInstance().trackEditText(mPhoneEdt);
            GrowingIO.getInstance().trackEditText(mInputCodeEdt);
            GrowingIO.getInstance().trackEditText(mInputNewPwdEdt);
            /*GrowingIO.getInstance().trackEditText(mInputEnsurePwdEdt);*/

            mGetCodeBtn.setEnabled(true);
            mPhoneEdt.addTextChangedListener(this);
            mInputCodeEdt.addTextChangedListener(this);
            mInputNewPwdEdt.addTextChangedListener(this);
           /* mInputEnsurePwdEdt.addTextChangedListener(this);*/
        }


        private void checkCommitEnClick() {
            if (!TextUtils.isEmpty(mInputCodeEdt.getText()) && !TextUtils.isEmpty(mInputNewPwdEdt.getText())/* && !TextUtils.isEmpty(mInputEnsurePwdEdt.getText())*/) {
                mCommitBtn.setEnabled(true);
            } else {
                mCommitBtn.setEnabled(false);
            }
            if(mPhoneEdt.getText().toString().length() == 11){
                mGetCodeBtn.setEnabled(true);
            }else{
                mGetCodeBtn.setEnabled(false);
            }
        }

        /**
         * 获取验证码
         *
         * @param view
         */
        @OnClick(R.id.get_code_btn)
        void onClickGetCode(View view) {
            mCodeDownTimer = new CodeDownTimer(mGetCodeBtn, 60000, 1000);
            mCodeDownTimer.setColorId(getResources().getColor(R.color.color_A8A8A8));
            mCodeDownTimer.start();
            String phoneStr = mPhoneEdt.getText().toString();
            if (TextUtils.isEmpty(phoneStr)) {
                toast("手机号不能为空");
                return;
            }
            getMvpPresenter().getSendSms(mPhoneEdt.getText().toString());
        }

        @Override
        public void callbackSendSms() {

        }

        @Override
        public void callbackSendSmsFail(String message) {
            toast(message);
        }

        /**
         * @param view
         */
        /*@OnClick(R.id.input_new_pwd_iv)
        void onClickNewPwdEye(View view) {
            isShowNewPwd = !isShowNewPwd;
            changeState(mInputNewPwdIv, mInputNewPwdEdt, isShowNewPwd);
        }*/

       /* @OnClick(R.id.input_pwd_ensure_iv)
        void onClickEnsurePwdEye(View view) {
            isShowEnsurePwd = !isShowEnsurePwd;
            changeState(mInputEnsurePwdIv, mInputEnsurePwdEdt, isShowEnsurePwd);
        }*/


        @OnClick(R.id.commit_btn)
        void onClickCommit(View view) {
            String pwdStr = mInputNewPwdEdt.getText().toString();
            /*String confirmPwdStr = mInputEnsurePwdEdt.getText().toString();*/

           /* if (!pwdStr.equals(confirmPwdStr)) {
                toast("两次密码不一样");
                return;
            }*/

            getMvpPresenter().userSetPwd(mPhoneEdt.getText().toString(), pwdStr, pwdStr, mInputCodeEdt.getText().toString());


        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkCommitEnClick();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    /**
     * 设置密码
     */
    public class SettingLayoutClass implements TextWatcher {

        @BindView(R.id.setting_pwd_edt)
        EditText mSettingPwdEdt;
        @BindView(R.id.setting_pwd_iv)
        ImageView mSettingPwdIv;
        @BindView(R.id.setting_ensure_pwd_edt)
        EditText mSettingEnsurePwdEdt;
        @BindView(R.id.setting_ensure_pwd_iv)
        ImageView mSettingEnsurePwdIv;

        @BindView(R.id.commit_btn)
        Button mCommitBtn;

        private boolean isShowSetting, isShowEnsure;


        public SettingLayoutClass(View rootView) {
            ButterKnife.bind(this, rootView);

            GrowingIO.getInstance().trackEditText(mSettingPwdEdt);
            GrowingIO.getInstance().trackEditText(mSettingEnsurePwdEdt);

            mSettingPwdEdt.addTextChangedListener(this);
            mSettingEnsurePwdEdt.addTextChangedListener(this);
        }

        @OnClick(R.id.setting_pwd_iv)
        void onClickSettingPwdEye(View view) {

            isShowSetting = !isShowSetting;
            changeState(mSettingPwdIv, mSettingPwdEdt, isShowSetting);

        }

        @OnClick(R.id.setting_ensure_pwd_iv)
        void onClickSettingEnsurePwdEye(View view) {

            isShowEnsure = !isShowEnsure;
            changeState(mSettingEnsurePwdIv, mSettingEnsurePwdEdt, isShowEnsure);
        }

        @OnClick(R.id.commit_btn)
        void onClickCommit(View view) {
        }

        private void checkCommit() {
            if (!TextUtils.isEmpty(mSettingPwdEdt.getText()) && !TextUtils.isEmpty(mSettingEnsurePwdEdt.getText())) {
                mCommitBtn.setEnabled(true);
            } else {
                mCommitBtn.setEnabled(false);
            }
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkCommit();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
