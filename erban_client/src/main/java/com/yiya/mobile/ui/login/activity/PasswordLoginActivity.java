package com.yiya.mobile.ui.login.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.utils.UIHelper;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangsongzhou
 * @date 2019/7/31
 */
public class PasswordLoginActivity extends BaseActivity {


    public static void start(Context context, String validateStr) {
        Intent intent = new Intent(context, PasswordLoginActivity.class);
        intent.putExtra("validateStr", validateStr);
        context.startActivity(intent);
    }


    @BindView(R.id.account_edt)
    EditText mAccountEdt;
    @BindView(R.id.pwd_edt)
    EditText mPwdEdt;
    @BindView(R.id.next_btn)
    Button mNextBtn;


    private String mValidateStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_login);
        ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        com.tongdaxing.xchat_framework.util.util.LogUtil.d("PasswordLoginActivity", "OnDestroy");
    }

    @OnClick(R.id.forget_pwd_tv)
    void onClickForgetPwd(View view) {
        UIHelper.showForgetPswAct(this);
    }

    @OnClick(R.id.exit_fl)
    void onClickExit(View view) {
        finish();
    }

    @OnClick(R.id.next_btn)
    void onClickNext(View view) {
        String accountStr = mAccountEdt.getText().toString();
        String pwdStr = mPwdEdt.getText().toString();

        if (accountStr.length() > 12) {
            toast("账号长度大于12位数");
            return;
        }
        if (pwdStr.length() < 6) {
            toast("密码长度小于6位数");
            return;
        }

        getDialogManager().showProgressDialog(this, "正在登录...");
        CoreManager.getCore(IAuthCore.class).login(accountStr, pwdStr, mValidateStr);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        getDialogManager().dismissDialog();
        finish();
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLoginFail(String error) {
        LogUtil.i("onLogin", error);
        getDialogManager().dismissDialog();
        toast(error);
    }

    private void checkEnable() {
        if (!TextUtils.isEmpty(mAccountEdt.getText().toString()) && !TextUtils.isEmpty(mPwdEdt.getText().toString())) {

            mNextBtn.setEnabled(true);
        } else {
            mNextBtn.setEnabled(false);
        }
    }

    private void init() {

        mValidateStr = getIntent().getStringExtra("validateStr");

        mAccountEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkEnable();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPwdEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkEnable();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


}
