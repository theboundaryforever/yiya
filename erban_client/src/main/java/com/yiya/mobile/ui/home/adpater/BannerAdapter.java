package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.yiya.mobile.room.avroom.other.RoomFrameHandle;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.List;

/**
 * @author Administrator
 * @date 2017/8/7
 */

public class BannerAdapter extends StaticPagerAdapter {
    private Context context;
    private List<BannerInfo> bannerInfoList;
    private LayoutInflater mInflater;

    public BannerAdapter(List<BannerInfo> bannerInfoList, Context context) {
        this.context = context;
        this.bannerInfoList = bannerInfoList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(ViewGroup container, int position) {
        final BannerInfo bannerInfo = bannerInfoList.get(position);
        ImageView imgBanner = (ImageView) mInflater.inflate(R.layout.banner_page_item, container, false);
        ImageLoadUtils.loadImage(context, bannerInfo.getBannerPic(), imgBanner, R.drawable.ic_home_banner_placeholder);
        imgBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (bannerInfo.getSkipType()) {
                    case BannerInfo.SKIP_TO_APP:
                        break;
                    case BannerInfo.SKIP_TO_AUDIO_ROOM:
                        RoomFrameHandle.start(context, JavaUtil.str2long(bannerInfo.getSkipUri()), RoomInfo.ROOMTYPE_HOME_PARTY);
                        break;
                    case BannerInfo.SKIP_TO_H5:
                        Intent intent = new Intent(context, CommonWebViewActivity.class);
                        intent.putExtra("url", bannerInfo.getSkipUri());
                        context.startActivity(intent);
                        break;
                    case BannerInfo.SKIP_TO_QQ:
                        Intent intentQQ = new Intent();
                        intentQQ.setData(Uri.parse(bannerInfo.getSkipUri()));
                        // 此Flag可根据具体产品需要自定义，如设置，则在加群界面按返回，返回手Q主界面，不设置，按返回会返回到呼起产品界面
                        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        try {
                            context.startActivity(intentQQ);
                        } catch (Exception e) {
                            // 未安装手Q或安装的版本不支持
                            SingleToastUtil.showToast("未安装手Q或安装的版本不支持!");
                        }
                        break;
                    case BannerInfo.SKIP_TO_VIDEO_ROOM:
                        RoomFrameHandle.start(context, JavaUtil.str2long(bannerInfo.getSkipUri()), RoomInfo.ROOMTYPE_VIDEO_LIVE);
                        break;
                }
            }
        });
        return imgBanner;
    }

    @Override
    public int getCount() {
        if (bannerInfoList == null) {
            return 0;
        } else
            return bannerInfoList.size();
    }

    public @Nullable
    List<BannerInfo> getBannerInfoList() {
        return bannerInfoList;
    }
}
