package com.yiya.mobile.ui.home.adpater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

/**
 * <p> 首页非热门adapter </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class HomeAttentionAdapter extends BaseQuickAdapter<HomeRoom, BaseViewHolder> {
    private boolean isCheckAttention = true;
    private int itemWidth = 0;

    public HomeAttentionAdapter(boolean isCheckAttention, int layoutId) {
        super(layoutId);
        this.isCheckAttention = isCheckAttention;
        this.itemWidth = (DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 10)) / 2;
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, HomeRoom homeRoom) {
        baseViewHolder.setText(R.id.tv_room_title, homeRoom.getTitle());
        TextView attention = baseViewHolder.getView(R.id.tv_room_attention);
        if (itemWidth > 0) {
            LinearLayout llContent = baseViewHolder.getView(R.id.ll_attention_item);
            ViewGroup.LayoutParams vl = llContent.getLayoutParams();
            vl.width = itemWidth;
            llContent.setLayoutParams(vl);
        }
        if (isCheckAttention)
            attention.setSelected(homeRoom.getStatus() == 1);
        ImageLoadUtils.loadCircleImage(mContext, homeRoom.getAvatar(), baseViewHolder.getView(R.id.iv_room_pic), R.drawable.nim_avatar_default);
        attention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // todo 开启房间要传入房间类型
                RoomFrameActivity.start(mContext, homeRoom.getUid(), RoomInfo.ROOMTYPE_HOME_PARTY);
            }
        });
    }
}
