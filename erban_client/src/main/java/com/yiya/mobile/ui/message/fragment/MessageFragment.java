package com.yiya.mobile.ui.message.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.message.MainMessagePresenter;
import com.yiya.mobile.presenter.message.MainMessageView;
import com.yiya.mobile.ui.message.activity.AttentionListActivity;
import com.yiya.mobile.ui.message.activity.BlackFriendActivity;
import com.yiya.mobile.ui.message.activity.FansListActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 消息页
 *
 * @author Zhangsongzhou
 * @date 2019/3/22
 */

@CreatePresenter(MainMessagePresenter.class)
public class MessageFragment extends BaseMvpFragment<MainMessageView, MainMessagePresenter> implements MainMessageView {

    public static final String TAG = "MessageFragment";

    @BindView(R.id.viewpager_message)
    ViewPager mViewPager;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_message;
    }


    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
    }

//    @CoreEvent(coreClientClass = IIMLoginClient.class)
//    public void onImLoginSuccess(LoginInfo loginInfo) {
//        updateChatPoint();
//    }
//
//    @CoreEvent(coreClientClass = IUserClient.class)
//    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
//        updateChatPoint();
//    }
//
//    @CoreEvent(coreClientClass = IIMMessageCoreClient.class)
//    public void onReceiveRecentContactChanged(List<RecentContact> imMessages) {
//        updateChatPoint();
//    }

//    private void updateChatPoint() {
//        if (mMsgIndicatorAdapter != null) {
//            int unreadCount = CoreManager.getCore(IIMMessageCore.class).queryUnreadMsg();
//            View view = mMsgIndicatorAdapter.getChildView(1, R.id.view_point);
//            if (view != null) {
//                view.setVisibility(unreadCount > 0 ? View.VISIBLE : View.INVISIBLE);
//            }
//        }
//    }

    @Override
    public void initiate() {
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "消息中心页");

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new PrivateChatListFragment());
        mViewPager.setAdapter(new BaseIndicatorStateAdapter(getChildFragmentManager(), fragmentList));
    }

    @OnClick({R.id.tv_attention, R.id.tv_fans, R.id.tv_black_list})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_attention:
                //跳转关注页
                AttentionListActivity.start(getContext());
                break;
            case R.id.tv_fans:
                //跳转粉丝页
                FansListActivity.start(getContext());
                break;
            case R.id.tv_black_list:
                //跳转黑名单
                BlackFriendActivity.start(getContext());
                break;
        }
    }
}
