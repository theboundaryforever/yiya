package com.yiya.mobile.ui.rank.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.base.adapter.BaseIndicatorAdapter;
import com.yiya.mobile.ui.rank.fragment.RankingChatListFragment;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 语聊房贡献榜
 * @author Zhangsongzhou
 * @date 2019/4/8
 */
public class RankingChatListActivity extends BaseActivity implements ViewPager.OnPageChangeListener {
    /**
     * dateType       类型：1，日；2，周；3，总榜
     */
    public static final int DATA_TYPE_DAY = 1, DATA_TYPE_MONTH = 2, DATA_TYPE_ALL = 3;
    public static final int DATA_TYPE_CHARM = 2, DATA_TYPE_TREASURE = 1;

    public static final String BUNDLE_KEY_ROOM_ID = "bundle_room_id";
    public static final String BUNDLE_KEY_UID = "bundle_uid";
    public static final String BUNDLE_KEY_TYPE = "bundle_type";
    public static final String BUNDLE_KEY_DATA_TYPE = "bundle_data_type";


    @BindViews({R.id.top_ranking_bt, R.id.charm_ranking_bt})
    List<Button> mRankingBtns;

    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    private List<Fragment> mFragmentList;
    private BaseIndicatorAdapter mViewPagerAdapter;

    private String mUid = CoreManager.getCore(IAuthCore.class).getCurrentUid() + "";

//    public static void start(Context context, String uid) {
//        Intent intent = new Intent(context, RankingChatListActivity.class);
//        intent.putExtra("uid", uid);
//        context.startActivity(intent);
//    }

    public static void start(Context context, String uid, String roomId) {
        Intent intent = new Intent(context, RankingChatListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_ROOM_ID, roomId);
        bundle.putString(BUNDLE_KEY_UID, uid);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking_chat_list);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        mUid = bundle.getString(BUNDLE_KEY_UID);
        String roomID = bundle.getString(BUNDLE_KEY_ROOM_ID);

        mFragmentList = new ArrayList<>();
        mFragmentList.add(RankingChatListFragment.newInstance(DATA_TYPE_TREASURE, mUid, roomID));
        mFragmentList.add(RankingChatListFragment.newInstance(DATA_TYPE_CHARM, mUid, roomID));

        mViewPagerAdapter = new BaseIndicatorAdapter(getSupportFragmentManager(), mFragmentList);
        mViewPager.setAdapter(mViewPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);

        mViewPager.addOnPageChangeListener(this);


        selectTitle(R.id.top_ranking_bt);
    }

    @OnClick({R.id.top_ranking_bt, R.id.charm_ranking_bt})
    void onClickTitleIndicator(View view) {
//        switch (view.getId()) {
//            case R.id.top_ranking_bt:
//                break;
//            case R.id.charm_ranking_bt:
//                break;
//        }
        selectTitle(view.getId());

    }

    private void selectTitle(int viewId) {
        int selectValue = 0;
        for (int i = 0; i < mRankingBtns.size(); i++) {
            if (mRankingBtns.get(i).getId() == viewId) {
                selectValue = i;
                mRankingBtns.get(i).setBackgroundResource(R.drawable.icon_ranking_chat_select);
            } else {
                mRankingBtns.get(i).setBackground(null);
            }
        }
        mViewPager.setCurrentItem(selectValue, true);

    }

    @OnClick(R.id.arrow_back)
    void onClickBack(View view) {
        finish();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        int viewId = R.id.top_ranking_bt;

        switch (position) {
            case 0:
                viewId = R.id.top_ranking_bt;
                break;
            case 1:
                viewId = R.id.charm_ranking_bt;
                break;
        }
        selectTitle(viewId);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
