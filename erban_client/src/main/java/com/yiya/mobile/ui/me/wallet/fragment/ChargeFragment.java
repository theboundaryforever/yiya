package com.yiya.mobile.ui.me.wallet.fragment;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.base.view.TitleBar;
import com.yiya.mobile.ui.me.wallet.adapter.ChargeAdapter;
import com.yiya.mobile.ui.me.wallet.presenter.ChargePresenter;
import com.yiya.mobile.ui.me.wallet.view.IChargeView;
import com.pingplusplus.android.Pingpp;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.count.IChargeClient;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pay.bean.ChargeBean;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;

/**
 * 新皮：充值页面
 *
 * @author zwk 2018/5/30
 */
@CreatePresenter(ChargePresenter.class)
public class ChargeFragment extends BaseMvpFragment<IChargeView, ChargePresenter> implements IChargeView, View.OnClickListener {

    @BindView(R.id.tv_gold)
    TextView mTv_gold;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindString(R.string.charge)
    String activityTitle;
    @BindView(R.id.title_bar)
    TitleBar mTitleBar;
    @BindView(R.id.tv_charge)
    TextView btnCharge;
    private ChargeAdapter mChargeAdapter;

    private ChargeBean mSelectChargeBean;

    @Override
    public int getRootLayoutId() {
        return R.layout.activity_charge;
    }

    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
        btnCharge.setOnClickListener(this);
    }

    @Override
    public void initiate() {
        mTitleBar.setVisibility(View.GONE);
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3, OrientationHelper.VERTICAL, false));
        mChargeAdapter = new ChargeAdapter();
        mRecyclerView.setAdapter(mChargeAdapter);
        mChargeAdapter.setOnItemClickListener((baseQuickAdapter, view, position) -> {
            List<ChargeBean> list = mChargeAdapter.getData();
            if (ListUtils.isListEmpty(list)) {
                return;
            }
            mSelectChargeBean = list.get(position);
            int size = list.size();
            for (int i = 0; i < size; i++) {
                list.get(i).isSelected = position == i;
            }
            mChargeAdapter.notifyDataSetChanged();
        });
        getMvpPresenter().getChargeList();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getMvpPresenter().refreshWalletInfo(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_charge:
                getMvpPresenter().showChargeOptionsDialog();
                break;
            default:
                break;
        }
    }

    @Override
    public void setupUserInfo(UserInfo userInfo) {

    }

    @Override
    public void setupUserWalletBalance(WalletInfo walletInfo) {
        Log.i(TAG, "setupUserWalletBalance: " + walletInfo.goldNum);
        mTv_gold.setText(String.format(Locale.getDefault(), "%.1f", walletInfo.goldNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        toast(error);
    }

    @Override
    public void buildChargeList(List<ChargeBean> chargeBeanList) {
        if (chargeBeanList != null && chargeBeanList.size() > 0) {
            for (int i = 0; i < chargeBeanList.size(); i++) {
                ChargeBean chargeBean = chargeBeanList.get(i);
                chargeBean.isSelected = chargeBean.getMoney() == 48;
                if (48 == chargeBean.getMoney()) {
                    mSelectChargeBean = chargeBean;
                }
            }
            mChargeAdapter.setNewData(chargeBeanList);
        }
    }

    @Override
    public void getChargeListFail(String error) {
        toast(error);
    }

    @Override
    public void displayChargeOptionsDialog() {
        if (mSelectChargeBean == null || mContext == null) {
            return;
        }
        ButtonItem buttonItem = new ButtonItem(getString(R.string.charge_alipay), () -> getMvpPresenter().requestCharge(mContext,
                String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_ALIPAY));
        ButtonItem buttonItem1 = new ButtonItem(getString(R.string.charge_webchat), () -> getMvpPresenter().requestCharge(mContext,
                String.valueOf(mSelectChargeBean.chargeProdId), Constants.CHARGE_WX));

        List<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(buttonItem);
        buttonItems.add(buttonItem1);
        ((BaseActivity) mContext).getDialogManager().showCommonPopupDialog(buttonItems, getString(R.string.cancel), false);
    }

    @Override
    public void getChargeOrOrderInfo(String data, boolean isHJ, boolean isHC) {
        if (data != null) {
            if(isHJ){
                getMvpPresenter().joinPay(getActivity(), data);
            } else {
                Pingpp.createPayment(this, data);
            }
        }
    }

    @Override
    public void getChargeOrOrderInfo(String data, int payType) {

    }

    @Override
    public void getChargeOrOrderInfoFail(String error) {
        toast("发起充值失败" + error);
    }

    @CoreEvent(coreClientClass = IPayCoreClient.class)
    public void onWalletInfoUpdate(WalletInfo walletInfo) {
        if (walletInfo != null) {
            mTv_gold.setText(getString(R.string.charge_gold, walletInfo.getGoldNum()));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: ");
        //支付页面返回处理
        if (requestCode == Pingpp.REQUEST_CODE_PAYMENT) {
            if (data != null && data.getExtras() != null) {
                String result = data.getExtras().getString("pay_result");
                if (result != null) {

                    /* 处理返回值
                     * "success" - 支付成功
                     * "fail"    - 支付失败
                     * "cancel"  - 取消支付
                     * "invalid" - 支付插件未安装（一般是微信客户端未安装的情况）
                     * "unknown" - app进程异常被杀死(一般是低内存状态下,app进程被杀死)
                     */
                    // 错误信息
                    String errorMsg = data.getExtras().getString("error_msg");
                    String extraMsg = data.getExtras().getString("extra_msg");
                    MLog.error(TAG, "errorMsg:" + errorMsg + "extraMsg:" + extraMsg);
                    CoreManager.notifyClients(IChargeClient.class, IChargeClient.chargeAction, result);
                    if ("success".equals(result)) {
                        getMvpPresenter().refreshWalletInfo(true);
                        toast("支付成功！");
                    } else if ("cancel".equals(result)) {
                        toast("支付被取消！");
                    } else {
                        toast("支付失败！");
                    }
                }
            }
        }
    }
}
