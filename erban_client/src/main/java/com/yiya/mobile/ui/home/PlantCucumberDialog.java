package com.yiya.mobile.ui.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.yiya.mobile.base.fragment.BaseTransparentDialogFragment;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.HomeMelon;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/2.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantCucumberDialog extends BaseTransparentDialogFragment {

    @BindView(R.id.iv_btn)
    ImageView ivBtn;

    private static final String KEY = "KEY";

    public static PlantCucumberDialog newInstance(HomeMelon homeMelon) {
        PlantCucumberDialog dialog = new PlantCucumberDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY, homeMelon);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.dialog_plant_cucumber;
    }

    @Override
    protected int getWindowGravity() {
        return Gravity.TOP;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getWindow() != null) {
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() == null) {
            return;
        }
        HomeMelon homeMelon = getArguments().getParcelable(KEY);
        if (homeMelon == null) {
            return;
        }
        ivBtn.setBackgroundResource(homeMelon.getSeedMelonType() == 1 ? R.drawable.bg_cumcuber_free_btn : R.drawable.bg_cumcuber_btn);
    }

    @OnClick({R.id.iv_close, R.id.iv_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_close:
                dismiss();
                break;
            case R.id.iv_btn:
                //跳转种瓜H5
                CommonWebViewActivity.start(getContext(), BaseUrl.PLANT_CUCUMBER);
                dismiss();
                break;
        }
    }
}
