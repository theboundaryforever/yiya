package com.yiya.mobile.ui.rank.adapter;

import android.content.Context;

import com.yiya.mobile.ui.home.adpater.CommonImageIndicatorAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/26
 * 描述        排行榜顶部指示适配器
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class ImageIndicatorAdapter extends CommonImageIndicatorAdapter {

    public ImageIndicatorAdapter(Context context, List<TabInfo> titleList) {
        super(context, titleList);
    }

    @Override
    protected int getDrawable(int tabId) {
        return R.drawable.icon_find_family_indicator_tag;
    }
}
