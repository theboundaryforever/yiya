package com.yiya.mobile.ui.find.adapter;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.find.FindInfo;

/**
 * 创建者      Created by dell
 * 创建时间    2018/12/8
 * 描述        ${}
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class FindActivityAdapter extends BaseQuickAdapter<FindInfo, BaseViewHolder> {

    private final int mHeight;

    public FindActivityAdapter() {
        super(R.layout.item_find);
        mHeight = ScreenUtil.dip2px(80);
    }

    @Override
    protected void convert(BaseViewHolder helper, FindInfo item) {
        LinearLayout rootView = helper.getView(R.id.rootView);
        ViewGroup.LayoutParams params = rootView.getLayoutParams();
        params.height = mHeight;
        rootView.setLayoutParams(params);

        ImageView ivActivity = helper.getView(R.id.iv_activity);
        ImageLoadUtils.loadBannerRoundBackground(ivActivity.getContext(), item.getAdvIcon(), ivActivity, R.dimen.dp_10);
    }
}
