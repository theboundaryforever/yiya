package com.yiya.mobile.ui.me.user.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayoutManager;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.user.bean.Medal;
import com.tongdaxing.xchat_core.user.bean.MultiInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.user.IInnerUserInfoView;
import com.yiya.mobile.presenter.user.InnerUserInfoPresenter;
import com.yiya.mobile.ui.me.user.adapter.MedalAdapter;
import com.yiya.mobile.ui.me.user.adapter.UserContributorAdapter;
import com.yiya.mobile.utils.UIHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.yiya.mobile.ui.rank.activity.RankingListActivity.DATE_TYPE_MONTH;

/**
 * ProjectName:
 * Description:个人主页-资料
 * Created by BlackWhirlwind on 2019/9/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
@CreatePresenter(InnerUserInfoPresenter.class)
public class UserInfoFragment extends BaseMvpFragment<IInnerUserInfoView, InnerUserInfoPresenter> implements IInnerUserInfoView {

    @BindView(R.id.rv_medal)
    RecyclerView rvMedal;
    @BindView(R.id.rv_contributors)
    RecyclerView rvContributors;
    @BindView(R.id.tv_signature)
    TextView tvSignature;
    @BindView(R.id.tv_contribution)
    TextView tvContribution;

    public static UserInfoFragment newInstance(UserInfo userInfo) {
        UserInfoFragment fragment = new UserInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(UserInfo.KEY_USER_INFO, userInfo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_user_info;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        getMvpPresenter().getGiftRankingList(DATE_TYPE_MONTH);
    }

    @Override
    public void initiate() {
        if (getArguments() == null) {
            return;
        }
        UserInfo userInfo = getArguments().getParcelable(UserInfo.KEY_USER_INFO);
        if (userInfo == null) {
            return;
        }

        if (!TextUtils.isEmpty(userInfo.getUserDesc())) {
            tvSignature.setText(userInfo.getUserDesc());
        }


        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        rvMedal.setLayoutManager(layoutManager);
        MedalAdapter medalAdapter = new MedalAdapter();

        ArrayList<MultiInfo> list = new ArrayList<>();
        if (!TextUtils.isEmpty(userInfo.getExperLevelPic())) {
            list.add(new MultiInfo(MultiInfo.TYPE_PIC_URL)
                    .setPicUrl(userInfo.getExperLevelPic()));
        }
        if (!TextUtils.isEmpty(userInfo.getCharmLevelPic())) {
            list.add(new MultiInfo(MultiInfo.TYPE_PIC_URL)
                    .setPicUrl(userInfo.getCharmLevelPic()));
        }
        Medal medal = userInfo.getMedal();
        if (medal != null) {
            if (!TextUtils.isEmpty(medal.getPicUrl())) {
                list.add(new MultiInfo(MultiInfo.TYPE_PIC_URL)
                        .setPicUrl(medal.getPicUrl()));
            }
        }
        if (userInfo.getGender() != 0) {
            list.add(new MultiInfo(MultiInfo.TYPE_RES_ID)
                    .setResId(userInfo.getGender() == 1 ? R.drawable.ic_user_male : R.drawable.ic_user_female));
        }
        rvMedal.setAdapter(medalAdapter);
        medalAdapter.setNewData(list);

    }

    @Override
    public void showGiftRankingList(RankingXCInfo rankingXCInfo) {
        tvContribution.setText(String.valueOf(rankingXCInfo.getTotal()));

        rvContributors.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        UserContributorAdapter adapter = new UserContributorAdapter();
        rvContributors.setAdapter(adapter);
        addDisposable(Observable.fromIterable(rankingXCInfo.getList())
                .take(3)
                .subscribeOn(Schedulers.io())
                .map(RankingXCInfo.ListBean::getAvatar)
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((avatarList, throwable) ->
                        adapter.setNewData(avatarList)));
    }


    @OnClick(R.id.iv_contribution_bg)
    public void onViewClicked() {
        //跳转个人主页贡献榜
        UIHelper.showUserContributionH5(getContext());
    }
}
