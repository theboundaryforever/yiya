package com.yiya.mobile.ui.message.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.netease.nim.uikit.recent.RecentContactsFragment;
import com.netease.nim.uikit.session.helper.TeamNotificationHelper;
import com.netease.nim.uikit.uinfo.UserInfoHelper;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.attachment.NotificationAttachment;
import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.netease.nimlib.sdk.uinfo.model.UserInfo;
import com.tongdaxing.erban.R;

import java.util.ArrayList;
import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/2.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PrivateChatListAdapter extends BaseQuickAdapter<RecentContact, BaseViewHolder> {

    public PrivateChatListAdapter(@Nullable List<RecentContact> data) {
        super(R.layout.item_private_chat, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, RecentContact recent) {
        if (recent == null) {
            return;
        }
        //头像
        UserInfo userInfo = NimUIKit.getUserInfoProvider().getUserInfo(recent.getContactId());
        if (userInfo != null && !TextUtils.isEmpty(userInfo.getAvatar())) {
            ImageLoadUtils.loadCircleImage(mContext, userInfo.getAvatar(), helper.getView(R.id.iv_avatar), R.drawable.nim_avatar_default);
        }
        //名字
        helper.setText(R.id.tv_gift_name, UserInfoHelper.getUserTitleName(recent.getContactId(), recent.getSessionType()));
        //消息内容
        helper.setText(R.id.tv_content, descOfMsg(recent));
        //时间
        helper.setText(R.id.tv_time, TimeUtil.getTimeShowString(recent.getTime(), true));
        //未读
        int unreadNum = recent.getUnreadCount();
        helper.setGone(R.id.dt_unread_count, unreadNum > 0)
                .setText(R.id.dt_unread_count, unreadCountShowRule(unreadNum));
        //是否置顶
        helper.itemView.setBackgroundResource((recent.getTag() & RecentContactsFragment.RECENT_TAG_STICKY) == 0
                ? com.netease.nim.uikit.R.drawable.bg_common_touch_while
                : com.netease.nim.uikit.R.drawable.nim_recent_contact_sticky_selecter);
        //分割线
//        helper.setGone(R.id.common_divider, helper.getAdapterPosition() != getItemCount() - 1)
//                .setGone(R.id.bottom_divider, helper.getAdapterPosition() == getItemCount() - 1);
    }

    protected String descOfMsg(RecentContact recent) {
        if (recent.getMsgType() == MsgTypeEnum.text) {
            return recent.getContent();
        } else if (recent.getMsgType() == MsgTypeEnum.tip) {
            return getDefaultDigest(null, recent);
        } else if (recent.getAttachment() != null) {
            return getDefaultDigest(recent.getAttachment(), recent);
        }
        return "";
    }

    protected String unreadCountShowRule(int unread) {
        if (unread > 99) {
            return "99+";
        }
        return String.valueOf(unread);
    }

    // SDK本身只记录原始数据，第三方APP可根据自己实际需求，在最近联系人列表上显示缩略消息
    // 以下为一些常见消息类型的示例。
    private String getDefaultDigest(MsgAttachment attachment, RecentContact recent) {
        switch (recent.getMsgType()) {
            case text:
                return recent.getContent();
            case image:
                return "[图片]";
            case video:
                return "[视频]";
            case audio:
                return "[语音消息]";
            case location:
                return "[位置]";
            case file:
                return "[文件]";
            case tip:
                List<String> uuids = new ArrayList<>();
                uuids.add(recent.getRecentMessageId());
                List<IMMessage> messages = NIMClient.getService(MsgService.class).queryMessageListByUuidBlock(uuids);
                if (messages != null && messages.size() > 0) {
                    return messages.get(0).getContent();
                }
                return "[通知提醒]";
            case notification:
                return TeamNotificationHelper.getTeamNotificationText(recent.getContactId(),
                        recent.getFromAccount(),
                        (NotificationAttachment) recent.getAttachment());
            case avchat:
            case robot:
                return "[机器人消息]";
            default:
                return "[自定义消息] ";
        }
    }


}
