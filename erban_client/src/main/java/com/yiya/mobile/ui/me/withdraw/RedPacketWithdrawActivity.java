package com.yiya.mobile.ui.me.withdraw;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.me.wallet.adapter.WithdrawRedListAdapter;
import com.juxiao.library_ui.widget.AppToolBar;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCore;
import com.tongdaxing.xchat_core.redpacket.IRedPacketCoreClient;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;
import com.tongdaxing.xchat_core.redpacket.bean.WithdrawRedListInfo;
import com.tongdaxing.xchat_core.redpacket.bean.WithdrawRedSucceedInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCoreClient;
import com.tongdaxing.xchat_core.withdraw.bean.RefreshInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * 红包提现
 */
public class RedPacketWithdrawActivity extends BaseActivity implements View.OnClickListener, WithdrawWayDialog.OnWithdrawWayChangeListener {

    private AppToolBar mToolBar;
    private TextView redNum;
    private RelativeLayout binder;
    private RelativeLayout binderSucceed;
    private TextView alipayAccount;
    private TextView mPayNameTv;
    private TextView mUnbindingContentTv;
    private ImageView mPayLogoIv;
    private RecyclerView recyclerView;
    private Button btnWithdraw;
    private WithdrawInfo mWithdrawInfo;
    private WithdrawRedListAdapter mRedListAdapter;
    private WithdrawRedListInfo mSelectRedInfo;
//    private boolean hasAlipay = false;//弃用

    private int mCurrentPayWay = -1;

    private String mAliPayPhone = "";
    private String mAliPayName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_red_packet_withdraw);
        EventBus.getDefault().register(this);
        initTitleBar(getString(R.string.red_packet_withdraw));
        initView();
        initData();
        setListener();
    }

    private void initView() {

        mPayLogoIv = (ImageView) findViewById(R.id.zhifubao);
        mPayNameTv = (TextView) findViewById(R.id.tv_user_zhifubao_name);
        redNum = (TextView) findViewById(R.id.tv_red_num);
        binder = (RelativeLayout) findViewById(R.id.rly_binder);
        binderSucceed = (RelativeLayout) findViewById(R.id.rly_binder_succeed);
        alipayAccount = (TextView) findViewById(R.id.tv_user_zhifubao);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        btnWithdraw = (Button) findViewById(R.id.btn_withdraw);
        mToolBar = (AppToolBar) findViewById(R.id.toolbar);

        mUnbindingContentTv = (TextView) findViewById(R.id.unbinding_content_tv);

    }


    private void initData() {
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3, OrientationHelper.VERTICAL, false));
        mRedListAdapter = new WithdrawRedListAdapter();
        recyclerView.setAdapter(mRedListAdapter);
        mRedListAdapter.setOnItemClickListener((adapter, view, position) -> {
            List<WithdrawRedListInfo> list = mRedListAdapter.getData();
            if (ListUtils.isListEmpty(list)) {
                return;
            }
            mSelectRedInfo = list.get(position);
            int size = list.size();
            for (int i = 0; i < size; i++) {
                list.get(i).isSelected = position == i;
            }
            mRedListAdapter.notifyDataSetChanged();
            isWithdraw();
        });
        loadAlipayInfo();
        loadingData();
        loadingListData();
    }

    private void loadAlipayInfo() {
        CoreManager.getCore(IWithdrawCore.class).getWithdrawUserInfo(
                CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfo(WithdrawInfo withdrawInfo) {
        if (withdrawInfo != null) {
            mWithdrawInfo = withdrawInfo;

            //只有支付宝提现
            if (TextUtils.isEmpty(withdrawInfo.alipayAccount) || withdrawInfo.alipayAccount.equals("null")) {
                setNormalView();
            } else {
                setAliView();
            }


//            if (mWithdrawInfo.withDrawType == 1) {
//                if (mWithdrawInfo.hasWx) {
//                    setWeixinView();
//                } else if (StringUtils.isNotEmpty(mWithdrawInfo.alipayAccount) && !"null".equals(mWithdrawInfo.alipayAccount)) {
//                    setAliView();
//                } else {
//                    setNormalView();
//                }
//
//            } else {
//                if (StringUtils.isNotEmpty(mWithdrawInfo.alipayAccount) && !"null".equals(mWithdrawInfo.alipayAccount)) {
//                    setAliView();
//                } else if (mWithdrawInfo.hasWx) {
//                    setWeixinView();
//                } else {
//                    setNormalView();
//                }
//
//            }

        }
    }

    private void setAliView() {

        mCurrentPayWay = 2;
        notifyListState(mRedListAdapter.getData());
        mRedListAdapter.notifyDataSetChanged();
        binder.setVisibility(View.GONE);
        binderSucceed.setVisibility(View.VISIBLE);

        mPayLogoIv.setImageResource(R.drawable.ic_alipay_logo);
        alipayAccount.setText(getString(R.string.txt_withdraw_alipay_account, mWithdrawInfo.alipayAccount));
        mPayNameTv.setText(getString(R.string.txt_withdraw_alipay_name, mWithdrawInfo.alipayAccountName));
        mPayNameTv.setVisibility(View.VISIBLE);

    }

    private void setWeixinView() {
        mCurrentPayWay = 1;
        notifyListState(mRedListAdapter.getData());
        mRedListAdapter.notifyDataSetChanged();
        binder.setVisibility(View.GONE);
        binderSucceed.setVisibility(View.VISIBLE);

        mPayLogoIv.setImageResource(R.drawable.ic_weichat_pay);
        if (!TextUtils.isEmpty(mWithdrawInfo.weixinName)) {
            mPayNameTv.setText("微信名：" + mWithdrawInfo.weixinName);
        } else {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo != null && !TextUtils.isEmpty(userInfo.getNick())) {
                mPayNameTv.setText("微信名：" + userInfo.getNick());
            }

        }

//        mPayNameTv.setText("微信名：" + mWithdrawInfo.weixinName);
        alipayAccount.setText("电话：" + mWithdrawInfo.phone);
//        mPayNameTv.setVisibility(View.GONE);
    }

    private void setNormalView() {
        mCurrentPayWay = -1;
        binder.setVisibility(View.VISIBLE);
        binderSucceed.setVisibility(View.GONE);
        mUnbindingContentTv.setText("请先绑定您的支付宝后进行提现!");
//        if (MyWithdrawActivity.isOpenAliPay) {
//            mUnbindingContentTv.setText("请先绑定您的支付宝后进行提现!");
//        } else {
//            mUnbindingContentTv.setText("请先绑定微信账号后进行提现!");
//        }
    }


    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfoFail(String error) {
        LogUtil.i("onGetWithdrawUserInfoFail", error);
        toast(error);
    }

    private void loadingListData() {
        CoreManager.getCore(IRedPacketCore.class).getRedList();
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetRedList(List<WithdrawRedListInfo> withdrawRedListInfos) {
        if (!withdrawRedListInfos.isEmpty()) {
            notifyListState(withdrawRedListInfos);
            mRedListAdapter.setNewData(withdrawRedListInfos);
        }
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetRedListError(String error) {
        LogUtil.i("onGetRedListError", error);
        toast(error);
    }

    private void notifyListState(List<WithdrawRedListInfo> list) {
        if (mCurrentPayWay == -1) {
            return;
        }
        if (list != null && !list.isEmpty()) {
            for (WithdrawRedListInfo info : list) {
                info.setWd(true);
            }
        }
    }

    private void loadingData() {
        CoreManager.getCore(IRedPacketCore.class).getRedPacketInfo();
    }

    private RedPacketInfo redPacketInfos;

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetRedInfo(RedPacketInfo redPacketInfo) {
        if (null != redPacketInfo) {
            redPacketInfos = redPacketInfo;
            Double packetNum = redPacketInfo.getPacketNum();
            packetNum = (double) Math.round(packetNum * 100) / 100;
            redNum.setText(String.valueOf(packetNum));
        }
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetRedInfoError(String error) {
        toast(error);
        LogUtil.i("onGetRedInfoError", error);
    }


    private void setListener() {
        binder.setOnClickListener(this);
        btnWithdraw.setOnClickListener(this);
        binderSucceed.setOnClickListener(this);
        mToolBar.setOnBackBtnListener(view -> finish());
    }

    private Intent intent;
    private Bundle mBundle;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //未绑定支付宝时可点---绑定支付宝账号
            case R.id.rly_binder:
//                if (MyWithdrawActivity.isOpenAliPay) {
//                    WithdrawWayDialog withdrawWayDialog = WithdrawWayDialog.newInstance(mWithdrawInfo);
//                    withdrawWayDialog.setOnWithdrawWayChangeListener(this);
//                    withdrawWayDialog.showDialog(getSupportFragmentManager(), "WithdrawWayDialog2");
//                } else {
//                    BinderWeixinPayActivity.start(RedPacketWithdrawActivity.this, mWithdrawInfo);
////                    ChangeWeixinPayActivity.start(RedPacketWithdrawActivity.this);
//                }

                intent = new Intent(RedPacketWithdrawActivity.this, BinderAlipayActivity.class);
                mBundle = new Bundle();
                mBundle.putSerializable("withdrawInfo", mWithdrawInfo);
                intent.putExtras(mBundle);
                startActivity(intent);


                break;
            //绑定成功支付宝后可点---更改支付宝账号
            case R.id.rly_binder_succeed:
//                intent = new Intent(RedPacketWithdrawActivity.this, BinderAlipayActivity.class);
//                mBundle = new Bundle();
//                mBundle.putSerializable("withdrawInfo", mWithdrawInfo);
//                intent.putExtras(mBundle);
//                startActivity(intent);

                ChangeAlipayActivity.start(RedPacketWithdrawActivity.this, mWithdrawInfo);


//                if (MyWithdrawActivity.isOpenAliPay) {
//                    WithdrawWayDialog withdrawWayDialogSuccess = WithdrawWayDialog.newInstance(mWithdrawInfo);
//                    withdrawWayDialogSuccess.setOnWithdrawWayChangeListener(this);
//                    withdrawWayDialogSuccess.showDialog(getSupportFragmentManager(), "withdrawWayDialogSuccess");
//                } else {
//                    //不用处理点击事件
//                }

                break;
            case R.id.btn_withdraw:
                redPackWithdraw();
                break;
            default:
                break;
        }
    }

    //eventbus监听绑定支付宝页面是否绑定成功，然后刷新红包提现页面
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshEvent(RefreshInfo refreshInfo) {
        loadAlipayInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void redPackWithdraw() {
        if (redPacketInfos == null) {
            toast("暂无提现信息，请稍后重试");
            return;
        }
        if (mSelectRedInfo == null) {
            toast("提现失败");
            return;
        }
        if (isWithdraw()) {
            //有账号才能提现
            if (mCurrentPayWay == -1) {
                toast("提现失败");
                return;
            }
            //发起兑换
            getDialogManager().showOkCancelDialog(
                    getString(R.string.withdraw_the_money, mSelectRedInfo.getPacketNum()), true, new DialogManager.OkCancelDialogListener() {

                        @Override
                        public void onCancel() {
                            getDialogManager().dismissDialog();
                        }

                        @Override
                        public void onOk() {
                            getDialogManager().dismissDialog();
                            if (TextUtils.isEmpty(mAliPayName)) {
                                CoreManager.getCore(IRedPacketCore.class).getRedWithdraw(
                                        CoreManager.getCore(IAuthCore.class).getCurrentUid(), mSelectRedInfo.getPacketId(), mCurrentPayWay);
                            } else {
                                CoreManager.getCore(IRedPacketCore.class).getRedWithdraw(
                                        CoreManager.getCore(IAuthCore.class).getCurrentUid(), mSelectRedInfo.getPacketId(), mCurrentPayWay, mAliPayName, mAliPayPhone);
                            }

                        }
                    });
        }
    }


    public boolean isWithdraw() {
        if (!(mWithdrawInfo != null && mWithdrawInfo.isNotBoundPhone)) {
            if (mSelectRedInfo != null && redPacketInfos != null) {
                //用户的钻石余额 > 选中金额的钻石数时
                if (redPacketInfos.getPacketNum() >= mSelectRedInfo.getPacketNum()) {
                    btnWithdraw.setEnabled(true);
                    return true;
                } else {
                    btnWithdraw.setEnabled(false);
                    return true;
                }
            }
        } else {
            return false;
        }
        //如果选中position不为空的时候
        return false;
    }

    private boolean isAlipayValid() {
        return mWithdrawInfo != null && !TextUtils.isEmpty(mWithdrawInfo.alipayAccount);
    }

    private boolean isWeixinValid() {
        return mWithdrawInfo != null && mWithdrawInfo.hasWx;
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetWithdraw(WithdrawRedSucceedInfo succeedInfo) {
        toast("提现成功");
        if (null != succeedInfo) {
            Double packetNum = succeedInfo.getPacketNum();
            packetNum = (double) Math.round(packetNum * 100) / 100;
            redNum.setText(String.valueOf(packetNum));
        }
    }

    @CoreEvent(coreClientClass = IRedPacketCoreClient.class)
    public void onGetWithdrawError(String error) {
        LogUtil.i("onGetWithdrawError", error);
        toast(error);
    }

    @Override
    public void onChangeListener(WithdrawInfo withdrawInfo) {
        onGetWithdrawUserInfo(withdrawInfo);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        Bundle bundle = data.getExtras();
        if (requestCode == 1001) {
            if (bundle != null) {
                mAliPayName = bundle.getString(WithdrawActivity.BUNDLE_KEY_ALIPAY_NAME, "");
                mAliPayPhone = bundle.getString(WithdrawActivity.BUNDLE_KEY_ALIPAY_PHONE, "");

                mWithdrawInfo.alipayAccountName = mAliPayName;
                mWithdrawInfo.alipayAccount = mAliPayPhone;
                setAliView();
            }

        }

//        if (requestCode == 1000) {
//            Bundle bundle = data.getExtras();
//            if (bundle != null) {
//                mWeixinName = bundle.getString(BUNDLE_KEY_WEIXIN_NAME, "");
//                mWeixinOpenId = bundle.getString(BUNDLE_KEY_WEIXIN_OPEN_ID, "");
//                mWeixinPhone = bundle.getString(BUNDLE_KEY_WEIXIN_PHONE, "");
//
//                alipayAccount.setText("电话: " + mWeixinPhone);
//                alipayName.setText("微信名: " + mWeixinName);
//                ivWdLogo.setImageResource(R.drawable.ic_weichat_pay);
//
//                withdrawInfos.weixinName = mWeixinName;
//                withdrawInfos.phone = mWeixinPhone;
//                setWeixinWithdrawAccount();
//
//            }
//        }
    }
}
