package com.yiya.mobile.ui.home.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.fragment.BaseMvpListFragment;
import com.yiya.mobile.presenter.home.HomeFriendsPresenter;
import com.yiya.mobile.presenter.home.HomeFriendsView;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.home.adpater.HomeFriendsAdapter;
import com.yiya.mobile.ui.square.SquareActivity;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.HomeFriendsInfo;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/6/28
 */
@CreatePresenter(HomeFriendsPresenter.class)
public class HomeFriendsFragment extends BaseMvpListFragment<HomeFriendsAdapter, HomeFriendsView, HomeFriendsPresenter> implements HomeFriendsView, BaseQuickAdapter.OnItemClickListener {

    public static final String TAG = "HomeFriendsFragment";

    /**
     * 交友广场轮播list
     *
     * @param homeFriendsInfo
     */
    @Override
    public void showSquareList(HomeFriendsInfo homeFriendsInfo) {
        srlRefresh.finishRefresh();
        mAdapter.addData(0, homeFriendsInfo);
    }

    /**
     * 语聊房list
     *
     * @param homeFriendsInfos
     */
    @Override
    public void showRoomList(List<HomeFriendsInfo> homeFriendsInfos) {
        if (mPage == defaultSize) {
            srlRefresh.finishRefresh();
        } else {
            srlRefresh.finishLoadMore();
        }
        mAdapter.addData(homeFriendsInfos);
    }

    @Override
    public void initiate() {
        super.initiate();
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "交友List页");
    }

    @Override
    protected void initMyView() {
        hasDefualLoadding = false;
        srlRefresh.setEnableRefresh(true);
    }

    @Override
    protected RecyclerView.LayoutManager initManager() {
        return new LinearLayoutManager(getContext());
    }

    @Override
    protected HomeFriendsAdapter initAdapter() {
        return new HomeFriendsAdapter(null);
    }

    @Override
    protected void initClickListener() {
        mAdapter.setOnItemClickListener(this);
    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        getData();
    }

    private void getData() {
        if (mAdapter != null && !mAdapter.getData().isEmpty()) {
            mAdapter.getData().clear();
        }
        getMvpPresenter().getRoomListByTabId(8, mPage, pageSize);
        getMvpPresenter().publicTitle();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        super.onRefresh(refreshlayout);
        getData();
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        super.onLoadmore(refreshlayout);
        getMvpPresenter().getRoomListByTabId(8, mPage, pageSize);
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        getData();
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        HomeFriendsInfo item = (HomeFriendsInfo) adapter.getData().get(position);
        switch (item.getItemType()) {
            case HomeFriendsInfo.TYPE_ENTER_ROOM:
                long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
                RoomFrameActivity.start(getContext(), currentUid, RoomInfo.ROOMTYPE_HOME_PARTY);
                break;
            case HomeFriendsInfo.TYPE_ITEM:
                HomeYuChatListInfo info = item.getHomeYuChatListInfo();
                RoomFrameActivity.start(getActivity(), info.getUid(), info.getType());
                break;
            case HomeFriendsInfo.TYPE_SQUARE:
                //交友广场
                SquareActivity.start(getContext());
                break;
        }
    }
}
