package com.yiya.mobile.ui.home.adpater;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.ui.widget.avatarStack.AvatarStackView;
import com.yiya.mobile.ui.widget.avatarStack.WhiteAvatarStackAdapter;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.view.htmlTextView.HtmlTextView;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.glide.GlideApp;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.HomeFriendsInfo;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;
import com.tongdaxing.xchat_core.bean.SquareLoopInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/3.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class HomeFriendsAdapter extends BaseMultiItemQuickAdapter<HomeFriendsInfo, BaseViewHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public HomeFriendsAdapter(List<HomeFriendsInfo> data) {
        super(data);
        addItemType(HomeFriendsInfo.TYPE_SQUARE, R.layout.item_home_friends_square);
        addItemType(HomeFriendsInfo.TYPE_ENTER_ROOM, R.layout.item_home_friends_enter_room);
        addItemType(HomeFriendsInfo.TYPE_EMPTY, R.layout.item_empty_room);
        addItemType(HomeFriendsInfo.TYPE_ITEM, R.layout.item_home_friends_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeFriendsInfo item) {
        if (item == null) {
            return;
        }
        switch (item.getItemType()) {
            case HomeFriendsInfo.TYPE_ITEM:
                setHomeFriendsItem(helper, item.getHomeYuChatListInfo());
                break;
            case HomeFriendsInfo.TYPE_SQUARE:
                setHomeFriendsSquare(helper, item.getSquareLoopInfoList());
                break;
        }
    }

    private void setHomeFriendsSquare(BaseViewHolder helper, List<SquareLoopInfo> squareLoopInfoList) {
        LogUtil.i("HomeFriendsAdapter : "+squareLoopInfoList.size());
        ViewFlipper vf = helper.getView(R.id.vf_square);
        vf.removeAllViews();
        for (int i = 0; i < squareLoopInfoList.size(); i++) {
            if (i % 2 == 0) {
                ArrayList<SquareLoopInfo> list = new ArrayList<>();
                list.add(squareLoopInfoList.get(i));
                if (i + 1 < squareLoopInfoList.size()) {
                    list.add(squareLoopInfoList.get(i + 1));
                    vf.addView(generateFlipperView(list));
                }
            }
        }
        vf.startFlipping();
    }

    private View generateFlipperView(List<SquareLoopInfo> squareLoopInfos) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.laytout_home_friends_square_flipper, null);
        AvatarStackView asvUser = view.findViewById(R.id.asv_user);
        asvUser.setAdapter(new WhiteAvatarStackAdapter());
        Observable.fromIterable(squareLoopInfos)
                .map(squareLoopInfo -> squareLoopInfo.getMember().getAvatar())
                .toList()
                .subscribe((avatars, throwable) -> asvUser.setAvatarList(avatars));
        HtmlTextView tvTop = view.findViewById(R.id.tv_content_top);
        tvTop.setHtmlText(squareLoopInfos.get(0).getCustom().getData().getLoopHtml());
        HtmlTextView tvBottom = view.findViewById(R.id.tv_content_bottom);
        tvBottom.setHtmlText(squareLoopInfos.get(1).getCustom().getData().getLoopHtml());
        return view;
    }

    private void setHomeFriendsItem(BaseViewHolder helper, HomeYuChatListInfo item) {
        String onlineCount = StringUtils.transformToW(item.getOnlineNum());

        String title = !TextUtils.isEmpty(item.getTitle()) ? item.getTitle() : item.getNick();
        helper.setText(R.id.tv_gift_name, title)
                .setText(R.id.tv_id, "ID:" + item.getErbanNo())
                .setText(R.id.tv_num, onlineCount + "人");
//                    .setGone(R.id.tv_state, item.isOnline());//本地写死“在线”
        //头像
        ImageLoadUtils.loadAvatar(mContext, item.getAvatar(), helper.getView(R.id.riv_avatar), true);
        //首标签
        ImageView ivTag = helper.getView(R.id.iv_tag);
        if (TextUtils.isEmpty(item.getTagPict())) {
            ivTag.setVisibility(View.GONE);
        } else {
            loadRatioImg(ivTag, item.getTagPict());
        }
        //次标签
        ImageView ivBadge = helper.getView(R.id.iv_badge);
        if (TextUtils.isEmpty(item.getBadge())) {
            ivBadge.setVisibility(View.GONE);
        } else {
            loadRatioImg(ivBadge, item.getBadge());
        }
        //声浪
        SVGAImageView svgaWave = helper.getView(R.id.svga_wave);
        if (svgaWave.getDrawable() != null) {
            if (!svgaWave.isAnimating()) {
                svgaWave.startAnimation();
            }
        } else {
            SVGAParser svgaParser = new SVGAParser(mContext);
            svgaParser.decodeFromAssets("microredwave.svga", new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                    svgaWave.setVideoItem(svgaVideoEntity);
                    if (!svgaWave.isAnimating()) {
                        svgaWave.startAnimation();
                    }
                }

                @Override
                public void onError() {
                    LogUtil.e("svgaWave onError");
                }
            });
        }
    }

    private void loadRatioImg(ImageView iv, String url) {
        GlideApp.with(mContext)
                .load(url)
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Drawable> target, boolean b) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable drawable, Object o, Target<Drawable> target, DataSource dataSource, boolean b) {
                        resetIvRatio(drawable, iv);
                        return true;
                    }
                })
                .into(iv);
    }

    private void resetIvRatio(Drawable drawable, ImageView iv) {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) iv.getLayoutParams();
        params.dimensionRatio = String.valueOf((float) drawable.getIntrinsicWidth() / drawable.getIntrinsicHeight());
        iv.setLayoutParams(params);
        iv.setImageDrawable(drawable);
        iv.setVisibility(View.VISIBLE);
    }
}
