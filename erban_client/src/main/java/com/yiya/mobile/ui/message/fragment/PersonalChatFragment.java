package com.yiya.mobile.ui.message.fragment;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.message.MessagePersonalChatPresenter;
import com.yiya.mobile.presenter.message.MessagePersonalChatView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_framework.util.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Zhangsongzhou
 * @date 2019/3/22
 */

@CreatePresenter(MessagePersonalChatPresenter.class)
public class PersonalChatFragment extends BaseMvpFragment<MessagePersonalChatView, MessagePersonalChatPresenter> implements MessagePersonalChatView, BaseQuickAdapter.OnItemLongClickListener {

    @BindView(R.id.srl_home_refresh)
    SmartRefreshLayout mSmartRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;


    private PersonalChatRecyclerAdapter mAdapter;

    private List<ChatInfo> mData = new ArrayList<>();

    @Override

    public int getRootLayoutId() {
        return R.layout.fragment_home_live_stream;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();

    }

    @Override
    public void initiate() {
        mAdapter = new PersonalChatRecyclerAdapter(mContext, R.layout.item_message_chat);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);
        //test Data
        for (int i = 0; i < 10; i++) {
            ChatInfo info = new ChatInfo();
            mData.add(info);
        }
        mAdapter.setData(mData);

        mAdapter.setOnItemLongClickListener(this);

    }

    @Override
    public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {

        LogUtils.d(TAG, "press long: " + position);
        return false;
    }

    private class PersonalChatRecyclerAdapter extends BaseQuickAdapter<ChatInfo, BaseViewHolder> {

        private Context mContext;

        public PersonalChatRecyclerAdapter(int layoutResId) {
            super(layoutResId);
        }

        public PersonalChatRecyclerAdapter(Context context, int layoutResId) {
            this(layoutResId);
            mContext = context;
        }

        public void setData(List<ChatInfo> data) {
            setNewData(data);
        }

        @Override
        protected void convert(BaseViewHolder helper, ChatInfo item) {

        }
    }

    public static class ChatInfo implements MultiItemEntity {


        @Override
        public int getItemType() {
            return 0;
        }
    }


}
