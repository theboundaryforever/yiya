package com.yiya.mobile.ui.newfind.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;

import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.ui.home.adpater.HomeIndicatorAdapter;
import com.yiya.mobile.ui.newfind.presenter.NewFindPresenter;
import com.yiya.mobile.ui.newfind.view.NewFindView;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/***
 * 咿呀版发现页面
 * */
@CreatePresenter(NewFindPresenter.class)
public class NewFindFragment extends BaseMvpFragment<NewFindView, NewFindPresenter> implements NewFindView, HomeIndicatorAdapter.OnItemSelectListener, ViewPager.OnPageChangeListener {
    public static final String TAG = "NewFindFragment";

    @BindView(R.id.newfind_indicator)
    MagicIndicator indicator;
    @BindView(R.id.newfind_viewpager)
    ViewPager viewPager;

    private List<TabInfo> tabInfoList;//标题列表
    private List<Fragment> mFragmentList; //加载界面列表
    private CommonNavigator mCommonNavigator;
    private int currentPosition = 0;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_newfind;
    }

    @Override
    public void onFindViews() {
        viewPager.setOffscreenPageLimit(2);
        currentPosition = 0;
        initTabLayout();
    }

    @Override
    public void onSetListener() {
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void initiate() {

    }

    private void initTabLayout(){
        if (mFragmentList == null) {
            mFragmentList = new ArrayList<>();
        } else {
            mFragmentList.clear();
        }
        if(tabInfoList == null){
            tabInfoList = new ArrayList<>();
        }else{
            tabInfoList.clear();
        }
        tabInfoList.add(new TabInfo(1,"交友广场"));
        tabInfoList.add(new TabInfo(2,"萌新列表"));
        NewFindFriendsFragment newFindFriendsFragment = new NewFindFriendsFragment(); //交友广场
        SproutingNewFragment sproutingNewFragment = new SproutingNewFragment();  //萌新列表
        mFragmentList.add(newFindFriendsFragment);
        mFragmentList.add(sproutingNewFragment);
        HomeIndicatorAdapter mMsgIndicatorAdapter = new HomeIndicatorAdapter(mContext, tabInfoList);  //标题适配器
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        mCommonNavigator = new CommonNavigator(mContext);
        //不要默认等分
//        commonNavigator.setAdjustMode(true);
        mCommonNavigator.setAdapter(mMsgIndicatorAdapter);
        indicator.setNavigator(mCommonNavigator);
        LinearLayout titleContainer = mCommonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        viewPager.setAdapter(new BaseIndicatorStateAdapter(getChildFragmentManager(), mFragmentList));
        mCommonNavigator.onPageSelected(tabInfoList.size() > currentPosition ? currentPosition : 0);
        viewPager.setCurrentItem(tabInfoList.size() > currentPosition ? currentPosition : 0);
        ViewPagerHelper.bind(indicator, viewPager);
        //绑定完成才能控制选中

    }

    @Override
    public void onItemSelect(int position) {
        currentPosition = position;
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        currentPosition = i;
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
