package com.yiya.mobile.ui.login;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;


/**
 * Created by zhouxiangfeng on 2017/5/2.
 */

public class CodeDownTimer extends CountDownTimer {
    private TextView mTextView;
    private int mColorId = Color.RED;
    private OnTickListener mOnTickListener;

    /**
     * @param textView          The TextView
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receiver
     *                          {@link #onTick(long)} callbacks.
     */
    public CodeDownTimer(TextView textView, long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);
        this.mTextView = textView;
    }

    public CodeDownTimer(TextView textView, long millisInFuture, long countDownInterval, OnTickListener listener) {
        super(millisInFuture, countDownInterval);
        this.mTextView = textView;
        mOnTickListener = listener;
    }


    public void setColorId(int colorId) {
        mColorId = colorId;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        if (mOnTickListener != null) {
            mOnTickListener.onTick(mTextView, millisUntilFinished);
        } else {
            mTextView.setClickable(false); //设置不可点击
            mTextView.setText(millisUntilFinished / 1000 + "");  //设置倒计时时间
            /**
             * 超链接 URLSpan
             * 文字背景颜色 BackgroundColorSpan
             * 文字颜色 ForegroundColorSpan
             * 字体大小 AbsoluteSizeSpan
             * 粗体、斜体 StyleSpan
             * 删除线 StrikethroughSpan
             * 下划线 UnderlineSpan
             * 图片 ImageSpan
             * http://blog.csdn.net/ah200614435/article/details/7914459
             */
            SpannableString spannableString = new SpannableString(mTextView.getText().toString());  //获取按钮上的文字
            ForegroundColorSpan span = new ForegroundColorSpan(mColorId);
            /**
             * public void setSpan(Object what, int start, int end, int flags) {
             * 主要是start跟end，start是起始位置,无论中英文，都算一个。
             * 从0开始计算起。end是结束位置，所以处理的文字，包含开始位置，但不包含结束位置。
             */
            try {
                spannableString.setSpan(span, 0, 2, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);//将倒计时的时间设置为红色
                mTextView.setText(spannableString);
            } catch (Exception e) {

            }

        }


    }

    @Override
    public void onFinish() {

//        mTextView.setBackgroundResource(R.drawable.shape_blue_radius_15dp);  //还原背景色
        if (mOnTickListener != null) {
            mOnTickListener.onFinish(mTextView);
        } else {
            mTextView.setText("重新获取");
            mTextView.setClickable(true);//重新获得点击
        }
    }

    public interface OnTickListener {
        void onTick(TextView textView, long time);

        void onFinish(TextView textView);
    }
}
