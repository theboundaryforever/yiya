package com.yiya.mobile.ui.newfind.mode;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.Map;

public class NewFindFriendsMode extends BaseMvpModel {

    /**
     * 获取关注房间列表
     *
     * @param myCallBack
     */
    public void getRoomAttentionList(OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = getDefaultParams();
        OkHttpManager.getInstance().getRequest(UriProvider.getRoomAttentionList(), params, myCallBack);
    }

    /**
     * 获取公聊大厅列表
     *
     * @param roomId roomId
     */
    public void getpublicChat(long roomId, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = getDefaultParams();
        params.put("room_id", String.valueOf(roomId));
        postRequest(UriProvider.publicTitle(), params, myCallBack);
    }

    public void publicTitle(long roomId, HttpRequestCallBack callBack) {
        Map<String, String> params = getDefaultParams();
        params.put("room_id", String.valueOf(roomId));
        postRequest(UriProvider.publicTitle(), params, callBack);
    }

    /**
     * 获取交友广场list
     *
     * @param pageNum pageNum
     * @param pageSize pageSize
     */
    public void getFindFriendList(int pageNum,int pageSize, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = getDefaultParams();
        params.put("tagId", String.valueOf(8));
        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", String.valueOf(Constants.PAGE_SIZE));
        OkHttpManager.getInstance().getRequest(UriProvider.getMainDataByTab(), params, myCallBack);
    }

}
