package com.yiya.mobile.ui.login.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.ui.common.widget.dialog.SelectAvatarPopupDialog;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.yiya.mobile.ui.common.permission.PermissionActivity;
import com.yiya.mobile.ui.me.user.activity.ModifyInfoActivity;
import com.yiya.mobile.ui.widget.dialog.UserDatePickerDialog;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.databinding.ActivityAddinfoXcBinding;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.auth.ThirdUserInfo;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.linked.ILinkedCore;
import com.tongdaxing.xchat_core.linked.LinkedInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventId;
import com.tongdaxing.xchat_framework.thirdsdk.umeng.UmengEventUtil;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.file.JXFileUtils;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author zhouxiangfeng
 * @date 2017/5/12
 */

public class AddUserInfoActivity extends TakePhotoActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {


    private static final String CAMERA_PREFIX = "picture_";
    private static final String BMP_TEMP_NAME = "bmp_temp_name";
    private static final String TAG = "AddUserInfoActivity";

    private UserDatePickerDialog datePickerDialog;
    private String avatarUrl;
    private String avatarUrlWX;
    private String userBrith;

    private String mCameraCapturingName;
    private File cameraOutFile;
    private File photoFile;
    private ActivityAddinfoXcBinding addinfoBinding;
    private int sexCode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "信息填写页");
        addinfoBinding = DataBindingUtil.setContentView(this, R.layout.activity_addinfo_xc);
        addinfoBinding.setClick(this);
        setSwipeBackEnable(false);
        initView();
        addWXUserInfo();
        setOnListener();
    }

    private void initView() {
        addinfoBinding.etNick.setText("hd" + System.currentTimeMillis());
        addinfoBinding.etNick.setSelection(addinfoBinding.etNick.getText().toString().length());
    }

    @Override
    protected boolean needSteepStateBar() {
        return true;
    }

    private void addWXUserInfo() {
        ThirdUserInfo thirdUserInfo = CoreManager.getCore(IAuthCore.class).getThirdUserInfo();
        if (thirdUserInfo != null) {
            avatarUrlWX = thirdUserInfo.getUserIcon();
            if (!StringUtil.isEmpty(thirdUserInfo.getUserGender())) {
                if (thirdUserInfo.getUserGender().equals("m")) {
                    sexCode = 1;
//                    addinfoBinding.rbMan.setChecked(true);
                    addinfoBinding.imManXc.setImageResource(R.drawable.nim_avatar_man_press);
                } else {
                    sexCode = 2;
//                    addinfoBinding.rbFemale.setChecked(true);
                    addinfoBinding.imManLadyXc.setImageResource(R.drawable.nim_avatar_lady_press);
                }
            } else {
                sexCode = 1;
//                addinfoBinding.rbMan.setChecked(true);
                addinfoBinding.imManXc.setImageResource(R.drawable.nim_avatar_man_press);
            }
            String nick = thirdUserInfo.getUserName();
            if (!StringUtil.isEmpty(nick) && nick.length() > 15) {
                addinfoBinding.etNick.setText(nick.substring(0, 15));
            } else {
                addinfoBinding.etNick.setText(nick);
            }
            ImageLoadUtils.loadCircleImage(this, avatarUrlWX, addinfoBinding.civAvatar, R.drawable.nim_avatar_default);
        } else {
            handleAvatarToFile(R.drawable.avatar_man);
        }
    }

    private void setOnListener() {
        addinfoBinding.toolbar.setOnBackBtnListener(view -> {
            CoreManager.getCore(IAuthCore.class).logout();
            finish();
        });
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = UserDatePickerDialog.newInstanceUser(this, 1995,
                Calendar.JANUARY, 01, true);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case /*R.id.rb_female*/ R.id.im_man_lady_xc:
                sexCode = 2;
                addinfoBinding.imManLadyXc.setImageResource(R.drawable.nim_avatar_lady_press);
                addinfoBinding.imManXc.setImageResource(R.drawable.nim_avatar_man_normal);
                break;
            case /*R.id.rb_man*/ R.id.im_man_xc:
                sexCode = 1;
                addinfoBinding.imManLadyXc.setImageResource(R.drawable.nim_avatar_lady_normal);
                addinfoBinding.imManXc.setImageResource(R.drawable.nim_avatar_man_press);
                break;
            case R.id.tv_birth:
                if (datePickerDialog.isAdded()) {
                    if (getWindow() != null) {
                        try {
                            datePickerDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    datePickerDialog.setVibrate(true);
                    datePickerDialog.setYearRange(1945, 2018);
                    datePickerDialog.show(getSupportFragmentManager(), "DATEPICKER_TAG");
                }
                break;

            case R.id.ok_btn:
                UmengEventUtil.getInstance().onEvent(BasicConfig.INSTANCE.getAppContext(), UmengEventId.getRegisterUserInfoCommit());
                String nick = addinfoBinding.etNick.getText().toString();
                if (nick.trim().isEmpty()) {
                    Snackbar.make(addinfoBinding.getRoot(), "昵称不能为空！", Snackbar.LENGTH_SHORT).show();
                    return;
                } else if (sexCode == 0) {
                    Snackbar.make(addinfoBinding.getRoot(), "请选择性别", Snackbar.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(userBrith)) {
                    Snackbar.make(addinfoBinding.getRoot(), "请选择生日", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                if (photoFile != null) {
                    getDialogManager().showProgressDialog(AddUserInfoActivity.this, "正在上传请稍后...");
                    CoreManager.getCore(IFileCore.class).upload(photoFile);
                    return;
                }

                //用户如果自己拍照作为头像就上传，如果为空就代表没拍照，直接拿微信头像上传
                if (avatarUrlWX != null) {
                    avatarUrl = avatarUrlWX;
                } else if (StringUtils.isEmpty(avatarUrl)) {
                    Snackbar.make(addinfoBinding.getRoot(), "请上传头像！", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                commit();
                break;
            case R.id.rl_change_avatar:
                ButtonItem upItem = new ButtonItem("拍照上传", this::checkPermissionAndStartCamera);

                ButtonItem localItem = new ButtonItem("本地相册", () -> {
                    String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
//                    File cameraOutFile = JXFileUtils.getTempFile(AddUserInfoActivity.this, mCameraCapturingName);
                    File cameraOutFile = JXFileUtils.getTempFile(mCameraCapturingName);
                    if (!cameraOutFile.getParentFile().exists()) {
                        cameraOutFile.getParentFile().mkdirs();
                    }
                    Uri uri = Uri.fromFile(cameraOutFile);
                    CompressConfig compressConfig = new CompressConfig.Builder().create();
                    getTakePhoto().onEnableCompress(compressConfig, true);
                    CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
                    getTakePhoto().onPickFromGalleryWithCrop(uri, options);
                });
                List<ButtonItem> buttonItemList = new ArrayList<>();
                buttonItemList.add(upItem);
                buttonItemList.add(localItem);
                getDialogManager().showSelectAvatarPopupDialog(buttonItemList, "取消", true, new SelectAvatarPopupDialog.OnContentClickListener() {
                    @Override
                    public void onContentClick(View view, int scrId) {

                        handleAvatarToFile(scrId);
                    }
                });
                break;
            default:
        }
    }

    private void handleAvatarToFile(int srcId) {
        InputStream stream = getResources().openRawResource(srcId);
        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        String tmpFileName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
        File tmpFile = JXFileUtils.getTempFile(tmpFileName);
        if (tmpFile.getParentFile().exists()) {
            tmpFile.getParentFile().mkdirs();
        }
        try {
            tmpFile.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(tmpFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            photoFile = tmpFile;

            ImageLoadUtils.loadCircleImage(AddUserInfoActivity.this, photoFile.getAbsolutePath(), addinfoBinding.civAvatar, R.drawable.nim_avatar_default);

            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void commit() {
        String nick = addinfoBinding.etNick.getText().toString();
        UserInfo userInfo = new UserInfo();
        userInfo.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        userInfo.setNick(nick);
        userInfo.setAvatar(avatarUrl);
        userInfo.setGender(sexCode);
        userInfo.setBirthStr(userBrith);
        getDialogManager().showProgressDialog(AddUserInfoActivity.this, "请稍后...");
        LinkedInfo linkedInfo = CoreManager.getCore(ILinkedCore.class).getLinkedInfo();

        String channel = "";
        String roomUid = "";
        String uid = "";
        if (linkedInfo != null) {
            channel = linkedInfo.getChannel();
            roomUid = linkedInfo.getRoomUid();
            uid = linkedInfo.getUid();
        }
        String inviteCode = "";
//        if (addinfoBinding.etInviteCode.getText() != null) {
//            inviteCode = addinfoBinding.etInviteCode.getText().toString();
//        }
        CoreManager.getCore(IUserCore.class).requestCompleteUserInfo(userInfo, channel, uid, roomUid, inviteCode);
    }

    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
        @Override
        public void superPermission() {
            mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
            cameraOutFile = JXFileUtils.getTempFile(mCameraCapturingName);
            if (!cameraOutFile.getParentFile().exists()) {
                cameraOutFile.getParentFile().mkdirs();
            }
            Uri uri = Uri.fromFile(cameraOutFile);
            getTakePhoto().onEnableCompress(CompressConfig.ofDefaultConfig(), true);
            getTakePhoto().onPickFromCapture(uri);
        }
    };
    // 旧
//    PermissionActivity.CheckPermListener checkPermissionListener = new PermissionActivity.CheckPermListener() {
//        @Override
//        public void superPermission() {
//            mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
//            cameraOutFile = JXFileUtils.getTempFile(AddUserInfoActivity.this, mCameraCapturingName);
//            if (!cameraOutFile.getParentFile().exists()) {
//                cameraOutFile.getParentFile().mkdirs();
//            }
//            Uri uri = Uri.fromFile(cameraOutFile);
//            getTakePhoto().onEnableCompress(CompressConfig.ofDefaultConfig(), true);
//            getTakePhoto().onPickFromCapture(uri);
//        }
//    };

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, android.Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String path = null;
        MLog.debug(this, "PictureTaskerAct.onActivityResult, resultCode = " + resultCode);
        if (resultCode != Activity.RESULT_OK) {
            MLog.info(this, "return is not ok,resultCode=%d", resultCode);
            return;
        }

        if (requestCode == Method.NICK) {
            if (null != data) {
                String nick = data.getStringExtra(ModifyInfoActivity.CONTENTNICK);
                addinfoBinding.etNick.setText(nick);
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        datePickerDialog = null;
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        Log.d(TAG, "onUpload: 这是添加用户更改上传" + url);
        avatarUrl = url;
        getDialogManager().dismissDialog();
        ImageLoadUtils.loadCircleImage(this, avatarUrlWX, addinfoBinding.civAvatar, R.drawable.nim_avatar_default);
        commit();
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        toast("上传失败");
        getDialogManager().dismissDialog();
    }


    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoComplete(UserInfo userInfo) {
        getDialogManager().dismissDialog();

        if(userInfo != null){
            String birth = TimeUtil.getDateTimeString(userInfo.getBirth(), "yyyy-MM-dd");
            int age = TimeUtil.calAge(birth);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("age", age);
                jsonObject.put("gender", userInfo.getGender());
                GrowingIO.getInstance().setVisitor(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        CoreManager.getCore(IAuthCore.class).setThirdUserInfo(null);
        finish();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoCompleteFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {

        String monthstr;
        if ((month + 1) < 10) {
            monthstr = "0" + (month + 1);
        } else {
            monthstr = String.valueOf(month + 1);
        }
        String daystr;
        if ((day) < 10) {
            daystr = "0" + (day);
        } else {
            daystr = String.valueOf(day);
        }
        String birth = String.valueOf(year) + "-" + monthstr + "-" + daystr;
        addinfoBinding.tvBirth.setText(year + "年" + monthstr + "月" + daystr + "日");
        userBrith = birth;
    }

    public interface Method {
        int NICK = 2;
    }

    @Override
    public void takeSuccess(TResult result) {
        photoFile = new File(result.getImage().getCompressPath());
        ImageLoadUtils.loadCircleImage(this, photoFile.getAbsolutePath(), addinfoBinding.civAvatar,
                R.drawable.nim_avatar_default);
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
