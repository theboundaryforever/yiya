package com.yiya.mobile.ui.widget.avatarStack;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/6/28.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class CrownAvatarStackAdapter extends AvatarStackAdapter {

    @Override
    protected void convert(BaseViewHolder helper, String url) {
        super.convert(helper, url);
        //戴皇冠
        helper.setVisible(R.id.iv_crown, helper.getAdapterPosition() == 0);
        //头像border
        getAvatarRiv().setBorderColor(helper.getAdapterPosition() == 0 ? Color.parseColor("#FFED49") : Color.WHITE);
    }
}
