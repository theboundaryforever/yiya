package com.yiya.mobile.ui.me.shopping.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;

import java.util.List;

/**
 * Created by chenran on 2017/10/17.
 */

public class GiftWallAdapter extends RecyclerView.Adapter<GiftWallAdapter.GiftWallHolder> {
    private List<GiftWallInfo> giftWallInfoList;
    private Context context;

    public GiftWallAdapter(Context context) {
        this.context = context;
    }

    public void setGiftWallInfoList(List<GiftWallInfo> giftWallInfoList) {
        this.giftWallInfoList = giftWallInfoList;
        notifyDataSetChanged();
    }

    @Override
    public GiftWallHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.list_item_gift_wall_info, parent, false);
        return new GiftWallHolder(item);
    }

    @Override
    public void onBindViewHolder(GiftWallHolder holder, int position) {
        GiftWallInfo giftWallInfo = giftWallInfoList.get(position);
        holder.giftName.setText(giftWallInfo.getGiftName());
        holder.giftNum.setText(giftWallInfo.getReciveCount()+"");
        ImageLoadUtils.loadImage(context, giftWallInfo.getPicUrl(), holder.giftPic);
    }

    @Override
    public int getItemCount() {
        if (giftWallInfoList == null)
            return 0;
        else {
            return giftWallInfoList.size();
        }
    }


    public class GiftWallHolder extends RecyclerView.ViewHolder {
        private ImageView giftPic;
        private TextView giftName;
        private TextView giftNum;

        public GiftWallHolder(View itemView) {
            super(itemView);
            giftPic = (ImageView) itemView.findViewById(R.id.gift_img);
            giftName = (TextView) itemView.findViewById(R.id.gift_name);
            giftNum = (TextView) itemView.findViewById(R.id.gift_num);
        }

    }
}
