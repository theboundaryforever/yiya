package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.ui.widget.magicindicator.buildins.ArgbEvaluatorHolder;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * <p> 公共多个滑动tab样式 </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public abstract class CommonImageIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;

    private int normalColorId = R.color.color_AEACAF;
    private int selectColorId = R.color.color_8C63F6;
    private boolean isWrap = false;

    public CommonImageIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mContext = context;
        mTitleList = titleList;
    }

    private int size = 16;

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    public void setNormalColorId(@ColorRes int normalColorId) {
        this.normalColorId = normalColorId;
    }

    public void setSelectColorId(@ColorRes int selectColorId) {
        this.selectColorId = selectColorId;
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {

        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.img_indicator_layout);

        final ImageView indicator = pagerTitleView.findViewById(R.id.indicator);
        indicator.setImageResource(getDrawable(mTitleList.get(i).getId()));
        final TextView indicatorText = pagerTitleView.findViewById(R.id.indicator1);
        indicatorText.setTextSize(size);

        indicatorText.setText(mTitleList.get(i).getName());
        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {

            @Override
            public void onSelected(int index, int totalCount) {
                indicatorText.getPaint().setFakeBoldText(true);
            }

            @Override
            public void onDeselected(int index, int totalCount) {
                indicatorText.getPaint().setFakeBoldText(false);
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
                // 实现颜色渐变
                int color = ArgbEvaluatorHolder.eval(leavePercent,
                        ContextCompat.getColor(mContext, selectColorId),
                        ContextCompat.getColor(mContext, normalColorId));
                indicatorText.setTextColor(color);
                indicatorText.setScaleX(1.0f + (0.9f - 1.0f) * leavePercent);
                indicatorText.setScaleY(1.0f + (0.9f - 1.0f) * leavePercent);
                if (leavePercent > 0.95) {
                    indicator.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
                // 实现颜色渐变
                int color = ArgbEvaluatorHolder.eval(enterPercent,
                        ContextCompat.getColor(mContext, normalColorId),
                        ContextCompat.getColor(mContext, selectColorId));
                indicatorText.setTextColor(color);
                indicatorText.setScaleX(0.9f + (1.0f - 0.9f) * enterPercent);
                indicatorText.setScaleY(0.9f + (1.0f - 0.9f) * enterPercent);
                if (enterPercent > 0.95) {
                    indicator.setVisibility(View.VISIBLE);
                }
            }
        });
        pagerTitleView.setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }
        });
        return pagerTitleView;
    }


    @Override
    public IPagerIndicator getIndicator(Context context) {
        return null;
    }

    /**
     * 设置图片数据
     */
    protected abstract int getDrawable(int tabId);

    private OnItemSelectListener mOnItemSelectListener;

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}