package com.yiya.mobile.ui.me.user.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.ui.me.user.dialog.SelectSexDialog;
import com.jph.takephoto.app.TakePhotoActivity;
import com.jph.takephoto.compress.CompressConfig;
import com.jph.takephoto.model.CropOptions;
import com.jph.takephoto.model.TResult;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.room.audio.activity.AudioRecordActivity;
import com.yiya.mobile.ui.common.permission.PermissionActivity;
import com.yiya.mobile.ui.me.user.adapter.UserPhotoAdapter;
import com.yiya.mobile.ui.widget.dialog.UserDatePickerDialog;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;
import com.netease.nim.uikit.common.util.sys.TimeUtil;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.file.IFileCore;
import com.tongdaxing.xchat_core.file.IFileCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;
import com.tongdaxing.xchat_framework.util.util.file.JXFileUtils;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 编辑页面
 *
 * @author zhouxiangfeng
 * @date 2017/5/23
 */
public class UserInfoModifyActivity extends TakePhotoActivity
        implements View.OnClickListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, UserPhotoAdapter.ImageClickListener {

    private static final String CAMERA_PREFIX = "picture_";
    private String avatar;
    private ImageView civAvatar;
    private UserDatePickerDialog datePickerDialog;
    private TextView tvBirth;
    private TextView tvNick;
    private TextView tvSex;
    private TextView tvDesc;
    private UserInfo mUserInfo;
    private long userId;
    //    private String audioFileUrl;
//    private AudioPlayAndRecordManager audioManager;
//    private AudioPlayer audioPlayer;
    //    private RecyclerView photosRecyclerView;
    private String birth;
    private AppToolBar mToolBar;

    private boolean growing = false;// 判断是否上传事件

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        GrowingIO.getInstance().setPageName(this, "编辑资料");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "编辑资料页");
        setContentView(R.layout.activity_user_info_modify_xc);
        setStatusBar();
        findViews();
        init();
        onSetListener();

        userId = getIntent().getLongExtra("userId", 0);
        mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId);
        if (mUserInfo != null) {
            uploadGrowing(mUserInfo);
            initData(mUserInfo);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == userId) {
            mUserInfo = info;
            initData(mUserInfo);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        if (info.getUid() == userId) {
            mUserInfo = info;
            initData(mUserInfo);
            getDialogManager().dismissDialog();
        }
    }

    private int playState = PlayState.NORMAL;

    @Override
    public void click(int position, UserPhoto userPhoto) {
        if (userPhoto != null) {
            UIHelper.showModifyPhotosAct(UserInfoModifyActivity.this, userId);
        }
    }

    @Override
    public void addClick() {

    }


    private interface PlayState {
        int PLAYING = 1;
        int NORMAL = 0;
    }

    private void onSetListener() {

        mToolBar.setOnBackBtnListener(view -> finish());
    }

//    private void voiceClicked(TextView tvVoice) {
//        if (playState == PlayState.NORMAL) {
//            playState = PlayState.PLAYING;
//            setTextViewLeftDrawable(tvVoice, R.drawable.icon_play_stop);
//            if (!StringUtils.isEmpty(audioFileUrl)) {
//                audioPlayer.setDataSource(audioFileUrl);
//                audioManager.play();
//            }
//
//        } else if (playState == PlayState.PLAYING) {
//            playState = PlayState.NORMAL;
//            setTextViewLeftDrawable(tvVoice, R.drawable.icon_play);
//            audioManager.stopPlay();
//
//        }
//    }

    private void setTextViewLeftDrawable(TextView tvVoice, int drawId) {
        Drawable drawablePlay = getResources().getDrawable(drawId);
        drawablePlay.setBounds(0, 0, drawablePlay.getMinimumWidth(), drawablePlay.getMinimumHeight());
        tvVoice.setCompoundDrawables(drawablePlay, null, null, null);
    }

    private void initData(UserInfo userInfo) {
        if (null != userInfo) {
//            audioFileUrl = userInfo.getUserVoice();
            ImageLoadUtils.loadCircleImage(this, userInfo.getAvatar(), civAvatar, R.drawable.nim_avatar_default);
            birth = TimeUtil.getDateTimeString(userInfo.getBirth(), "yyyy-MM-dd");
            tvBirth.setText(birth);
            tvNick.setText(userInfo.getNick());
            if (!StringUtils.isEmpty(userInfo.getUserDesc())) {
                tvDesc.setText(userInfo.getUserDesc());
            }

            if (userInfo.getGender() == 1) {
                tvSex.setText("男");
            } else {
                tvSex.setText("女");
            }
            if (userInfo.isNewRegister()) {
                tvSex.setTextColor(getResources().getColor(R.color.color_383950));
            } else {
                tvSex.setTextColor(getResources().getColor(R.color.color_A8A8A8));
            }
        }
    }

    private void findViews() {
        civAvatar = (ImageView) findViewById(R.id.civ_avatar);
        tvBirth = (TextView) findViewById(R.id.tv_birth);
        tvNick = (TextView) findViewById(R.id.tv_nick);
        tvSex = (TextView) findViewById(R.id.tv_sex);
        tvDesc = (TextView) findViewById(R.id.tv_desc);
        findViewById(R.id.layout_avatar).setOnClickListener(this);
        findViewById(R.id.layout_birth).setOnClickListener(this);
        findViewById(R.id.layout_nick).setOnClickListener(this);
        findViewById(R.id.layout_desc).setOnClickListener(this);
        findViewById(R.id.layout_sex).setOnClickListener(this::onClick);
//        findViewById(R.id.layout_audiorecord).setOnClickListener(this);
//        findViewById(R.id.layout_photos).setOnClickListener(this);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mToolBar = (AppToolBar) findViewById(R.id.toolbar);
        mToolBar.getTvTitle().getPaint().setFakeBoldText(true);
    }

    private void init() {
//        audioManager = AudioPlayAndRecordManager.getInstance();
//        audioPlayer = audioManager.getAudioPlayer(UserInfoModifyActivity.this, null, mOnPlayListener);
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = UserDatePickerDialog.newInstanceUser(this, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
    }

    private void showByPlayState() {

    }

    // 修改日期成功
    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        String monthstr;
        if ((month + 1) < 10) {
            monthstr = "0" + (month + 1);
        } else {
            monthstr = String.valueOf(month + 1);
        }
        String daystr;
        if ((day) < 10) {
            daystr = "0" + (day);
        } else {
            daystr = String.valueOf(day);
        }
        String birth = String.valueOf(year) + "-" + monthstr + "-" + daystr;
        UserInfo user = new UserInfo();
        user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        user.setBirthStr(birth);

        growing = true;

        requestUpdateUserInfo(user);
    }

    private void requestUpdateUserInfo(UserInfo user) {
        CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user);
    }


    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {

    }

    public interface Method {
        /**
         * 录音
         */
        int AUDIO = 2;
        /**
         * 昵称
         */
        int NICK = 3;
        /**
         * 个人介绍
         */
        int DESC = 4;
    }


    PermissionActivity.CheckPermListener checkPermissionListener = this::takePhoto;

    private void takePhoto() {
        String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
//        File cameraOutFile = JXFileUtils.getTempFile(UserInfoModifyActivity.this, mCameraCapturingName);
        File cameraOutFile = JXFileUtils.getTempFile(mCameraCapturingName);
        if (!cameraOutFile.getParentFile().exists()) {
            cameraOutFile.getParentFile().mkdirs();
        }
        Uri uri = Uri.fromFile(cameraOutFile);
        CompressConfig compressConfig = new CompressConfig.Builder().create();
        getTakePhoto().onEnableCompress(compressConfig, false);
        CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
        getTakePhoto().onPickFromCaptureWithCrop(uri, options);
    }

    private void checkPermissionAndStartCamera() {
        //低版本授权检查
        checkPermission(checkPermissionListener, R.string.ask_camera, android.Manifest.permission.CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            MLog.info(this, "return is not ok,resultCode=%d", resultCode);
            return;
        }

        if (requestCode == Method.NICK) {
            String stringExtra = data.getStringExtra(ModifyInfoActivity.CONTENTNICK);
            tvNick.setText(stringExtra);
            UserInfo user = new UserInfo();
            user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            user.setNick(stringExtra);

            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user);
            toast("修改成功");

        }

        if (requestCode == Method.DESC) {
            String stringExtra = data.getStringExtra(ModifyInfoActivity.CONTENT);
            tvDesc.setText(stringExtra);
            UserInfo user = new UserInfo();
            user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            user.setUserDesc(stringExtra);

            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user);
            toast("修改成功");
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_avatar:
                ButtonItem buttonItem = new ButtonItem("拍照上传", this::checkPermissionAndStartCamera);
                ButtonItem buttonItem1 = new ButtonItem("本地相册", () -> {
                    String mCameraCapturingName = CAMERA_PREFIX + System.currentTimeMillis() + ".jpg";
//                    旧
//                    File cameraOutFile = JXFileUtils.getTempFile(UserInfoModifyActivity.this, mCameraCapturingName);
                    File cameraOutFile = JXFileUtils.getTempFile(mCameraCapturingName);
                    if (!cameraOutFile.getParentFile().exists()) {
                        cameraOutFile.getParentFile().mkdirs();
                    }
                    Uri uri = Uri.fromFile(cameraOutFile);
                    CompressConfig compressConfig = new CompressConfig.Builder().create();
                    getTakePhoto().onEnableCompress(compressConfig, true);
                    CropOptions options = new CropOptions.Builder().setWithOwnCrop(true).create();
                    getTakePhoto().onPickFromGalleryWithCrop(uri, options);

                });
                List<ButtonItem> buttonItems = new ArrayList<>();
                buttonItems.add(buttonItem);
                buttonItems.add(buttonItem1);
                getDialogManager().showCommonPopupDialog(buttonItems, "取消", false);

                isAvatar = true;
                break;
            case R.id.layout_birth:
                if (mUserInfo != null) {
                    int year = TimeUtils.getYear(mUserInfo.getBirth());
                    int month = TimeUtils.getMonth(mUserInfo.getBirth());
                    int day = TimeUtils.getDayOfMonth(mUserInfo.getBirth());
                    datePickerDialog = UserDatePickerDialog.newInstanceUser(this, year, (month - 1), day, true);
                }
                datePickerDialog.setVibrate(true);
                datePickerDialog.setYearRange(1945, 2018);
                datePickerDialog.show(getSupportFragmentManager(), "DATEPICKER_TAG_1");
                break;
            case R.id.layout_nick:
                UIHelper.showModifyInfoAct(this, Method.NICK, "昵称");
                break;
            case R.id.layout_desc:
                UIHelper.showModifyInfoAct(this, Method.DESC, "个性签名");
                break;
            case R.id.layout_audiorecord:
                checkPermission(() -> {
                    Intent intent = new Intent(this, AudioRecordActivity.class);
                    UserInfoModifyActivity.this.startActivityForResult(intent, Method.AUDIO);
                    isAvatar = false;
                }, R.string.ask_again, Manifest.permission.RECORD_AUDIO);

                break;
            case R.id.layout_photos:
                UIHelper.showModifyPhotosAct(UserInfoModifyActivity.this, userId);
                break;
            case R.id.layout_sex:
                if (mUserInfo.isNewRegister()) {
                    SelectSexDialog dialog = SelectSexDialog.newInstance(mUserInfo.getGender());
                    dialog.setOnContentClickListener(new SelectSexDialog.OnContentClickListener() {
                        @Override
                        public void onContentClick(int viewId, int sexValue) {
                            if (sexValue > 0 && sexValue != mUserInfo.getGender()) {
                                growing = true;
                                getDialogManager().showProgressDialog(UserInfoModifyActivity.this, "请稍后");
                                UserInfo info = new UserInfo();
                                info.setUid(mUserInfo.getUid());
                                info.setNewRegister(mUserInfo.isNewRegister());
                                info.setGender(sexValue);
                                requestUpdateUserInfo(info);
                            }
                        }
                    });
                    dialog.show(getSupportFragmentManager(), "SelectSexDialog");
                } else {
//                    toast("已经不能修改性别");
                }
                break;
            default:
        }
    }


    private boolean isAvatar = false;

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUpload(String url) {
        if (isAvatar) {
            UserInfo user = new UserInfo();
            avatar = url;
            user.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            user.setAvatar(avatar);
            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(user);
        }
    }

    @CoreEvent(coreClientClass = IFileCoreClient.class)
    public void onUploadFail() {
        toast("上传失败");
        getDialogManager().dismissDialog();
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdate(UserInfo userInfo) {
        if (isAvatar) {
            toast("上传图片成功，等待审核");
            isAvatar = false;
        }

        if (growing) {
            growing = false;
            uploadGrowing(userInfo);
        }

        CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(userId, true);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfoUpdateError(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void takeSuccess(TResult result) {
        getDialogManager().showProgressDialog(UserInfoModifyActivity.this, "请稍后");
        Log.d("Zasko", "takeSuccess: -------->" + result.getImage().getCompressPath());
        CoreManager.getCore(IFileCore.class).upload(new File(result.getImage().getCompressPath()));
    }

    @Override
    public void takeFail(TResult result, String msg) {
        toast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mOnPlayListener = null;
//        audioManager.release();
    }

    private void uploadGrowing(UserInfo userInfo) {
//        if (userInfo == null) return;
//        int age = TimeUtil.calAge(TimeUtil.getDateTimeString(userInfo.getBirth(), "yyyy-MM-dd"));

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("age", userInfo.getAge());
            jsonObject.put("gender", userInfo.getGender());
            GrowingIO.getInstance().setPeopleVariable(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    private OnPlayListener mOnPlayListener = new OnPlayListener() {
//        @Override
//        public void onPrepared() {
//
//        }
//
//        @Override
//        public void onCompletion() {
//            playState = PlayState.NORMAL;
//            showByPlayState();
//        }
//
//        @Override
//        public void onInterrupt() {
//            playState = PlayState.NORMAL;
//            showByPlayState();
//        }
//
//        @Override
//        public void onError(String s) {
//            playState = PlayState.NORMAL;
//            showByPlayState();
//        }
//
//        @Override
//        public void onPlaying(long l) {
//
//        }
//    };
}
