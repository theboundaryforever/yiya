package com.yiya.mobile.ui.widget.emptyView;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/10/12.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class DefaultEmptyView extends LinearLayout {

    @BindView(R.id.no_data_icon)
    ImageView noDataIcon;
    @BindView(R.id.no_data_text)
    TextView noDataText;

    public DefaultEmptyView(Context context) {
        this(context, null);
    }

    public DefaultEmptyView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DefaultEmptyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initData();
    }

    private void initData() {
        inflate(getContext(), R.layout.layout_empty_view, this);
        ButterKnife.bind(this);
    }

    public void setImgRes(int resId) {
        noDataIcon.setImageResource(resId);
    }

    public void setTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            noDataText.setText(title);
        }
    }

    public static class Builder {

        private DefaultEmptyEnum emptyEnum;

        public Builder(DefaultEmptyEnum emptyEnum) {
            this.emptyEnum = emptyEnum;
        }

        public DefaultEmptyView build(Context context) {
            DefaultEmptyView emptyView = new DefaultEmptyView(context);
            emptyView.setImgRes(emptyEnum.getResId());
            emptyView.setTitle(emptyEnum.getTitle());
            return emptyView;
        }
    }
}
