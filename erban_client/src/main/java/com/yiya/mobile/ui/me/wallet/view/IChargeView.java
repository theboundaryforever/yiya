package com.yiya.mobile.ui.me.wallet.view;

import com.tongdaxing.xchat_core.pay.bean.ChargeBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.List;

/**
 * Created by MadisonRong on 05/01/2018.
 */

public interface IChargeView extends IPayView {

    public void setupUserInfo(UserInfo userInfo);

    public void buildChargeList(List<ChargeBean> chargeBeanList);

    public void getChargeListFail(String error);

    public void displayChargeOptionsDialog();

    /**
     *
     * @param data
     * @param isHJ 是否是汇聚支付
     */
    public void getChargeOrOrderInfo(String data, boolean isHJ, boolean isHC);
    public void getChargeOrOrderInfo(String data, int payType);

    public void getChargeOrOrderInfoFail(String error);
}
