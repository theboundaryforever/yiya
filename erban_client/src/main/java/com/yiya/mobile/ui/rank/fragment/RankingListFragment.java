package com.yiya.mobile.ui.rank.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;

import com.yiya.mobile.base.adapter.BaseIndicatorAdapter;
import com.yiya.mobile.base.fragment.BaseLazyFragment;
import com.yiya.mobile.ui.home.adpater.CommonImageIndicatorAdapter;
import com.yiya.mobile.ui.rank.adapter.ImageIndicatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/26
 * 描述        用户排行榜fragment
 * <p>
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class RankingListFragment extends BaseLazyFragment implements CommonImageIndicatorAdapter.OnItemSelectListener {

    private ViewPager mViewPager;
    private MagicIndicator mIndicator;

    public static final int RANKING_TYPE_CHARM = 0, RANKING_TYPE_TYCOON = 1;



    List<Fragment> mTabs;
    private int type = RANKING_TYPE_CHARM;
    private BaseIndicatorAdapter mTabAdapter;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_ranking_list;
    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        if (bundle != null) {
            type = bundle.getInt("type", 0);
        }
    }

    @Override
    public void onFindViews() {
        mIndicator = mView.findViewById(R.id.indicator);
        mViewPager = mView.findViewById(R.id.viewPager);
    }

    @Override
    public void initiate() {

    }

    @Override
    public void onItemSelect(int position) {
        mViewPager.setCurrentItem(position);
    }

    @Override
    protected void onLazyLoadData() {
        mTabs = new ArrayList<>();
        mTabs.add(RankingItemFragment.newInstance(type, 0));
        mTabs.add(RankingItemFragment.newInstance(type, 1));
        mTabs.add(RankingItemFragment.newInstance(type, 2));

        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(1, getString(R.string.ranking_list_for_day)));
        tabInfoList.add(new TabInfo(2, getString(R.string.ranking_list_for_week)));
        tabInfoList.add(new TabInfo(3, getString(R.string.ranking_list_for_all)));
        ImageIndicatorAdapter indicatorAdapter = new ImageIndicatorAdapter(getActivity(), tabInfoList);
        indicatorAdapter.setOnItemSelectListener(this);
        indicatorAdapter.setSize(18);
        CommonNavigator commonNavigator = new CommonNavigator(getActivity());
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(indicatorAdapter);
        mIndicator.setNavigator(commonNavigator);

        // must after setNavigator
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        mTabAdapter = new BaseIndicatorAdapter(getChildFragmentManager(), mTabs);
        mViewPager.setAdapter(mTabAdapter);
        mViewPager.setOffscreenPageLimit(3);
        ViewPagerHelper.bind(mIndicator, mViewPager);
    }

    @Override
    public void onSetListener() {

    }

    public static Fragment newInstance(int type) {
        Bundle bundle = new Bundle();
        RankingListFragment fragment = new RankingListFragment();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }
}
