package com.yiya.mobile.ui.me.withdraw;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.presenter.withdraw.IWithdrawView;
import com.yiya.mobile.presenter.withdraw.WithdrawPresenter;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.juxiao.library_ui.widget.AppToolBar;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.withdraw.bean.RefreshInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * @author Zhangsongzhou
 * @date 2019/4/23
 */
@CreatePresenter(WithdrawPresenter.class)
public class ChangeWeixinPayActivity extends BaseMvpActivity<IWithdrawView, WithdrawPresenter> implements IWithdrawView {


    @BindView(R.id.toolbar)
    AppToolBar mAppToolBar;
    @BindView(R.id.et_phone_number)
    EditText mPhoneNumberEdt;
    @BindView(R.id.et_smscode)
    EditText mCodeNumberEdt;
    @BindView(R.id.btn_get_code)
    TextView mGetCodeTv;
    @BindView(R.id.btn_binder)
    Button mBinderBtn;


    private CodeDownTimer mCodeDownTimer;
    private Platform wechat;

    public static void start(Context context) {
        Intent intent = new Intent(context, ChangeWeixinPayActivity.class);
        ((Activity) context).startActivityForResult(intent, 1000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_weixin_pay);
        ButterKnife.bind(this);

        GrowingIO.getInstance().trackEditText(mPhoneNumberEdt);
        GrowingIO.getInstance().trackEditText(mCodeNumberEdt);

        initData();

        setListener();
    }

    private void initData() {


        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();

        if (userInfo != null && !TextUtils.isEmpty(userInfo.getPhone())) {
            mPhoneNumberEdt.setText(userInfo.getPhone());
            try {
                mPhoneNumberEdt.setSelection(mPhoneNumberEdt.getText().length());
            } catch (Exception e) {

            }
        }
        mBinderBtn.setEnabled(false);
    }

    private void setListener() {
        mAppToolBar.setOnBackBtnListener(view -> finish());
        mPhoneNumberEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!TextUtils.isEmpty(mCodeNumberEdt.getText().toString()) && !TextUtils.isEmpty(s.toString())) {
                    mBinderBtn.setEnabled(true);
                } else {
                    mBinderBtn.setEnabled(false);
                }

            }
        });
        mCodeNumberEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(mPhoneNumberEdt.getText().toString()) && !TextUtils.isEmpty(s.toString())) {
                    mBinderBtn.setEnabled(true);
                } else {
                    mBinderBtn.setEnabled(false);
                }

            }
        });
    }

    /**
     * 获取验证码
     *
     * @param view
     */
    @OnClick(R.id.btn_get_code)
    void onClickCodeGet(View view) {
        String phone = mPhoneNumberEdt.getText().toString();
        if (StringUtil.isEmpty(phone)) {
            toast("手机号码不能为空哦！");
            return;
        }
        if (phone.length() < 11) {
            toast("请填写完整的手机号码");
            return;
        }
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null && StringUtils.isNotEmpty(userInfo.getPhone())) {

            if (phone.equals(userInfo.getPhone()) || phone.contains(userInfo.getPhone().substring(5))) {//188992716
                if (mCodeDownTimer == null) {
                    mCodeDownTimer = new CodeDownTimer(mGetCodeTv, 60000, 1000);
                    mCodeDownTimer.setColorId(Color.WHITE);
                }

                mCodeDownTimer.start();
                getMvpPresenter().getInviteCode(CoreManager.getCore(IAuthCore.class).getCurrentUid(), CoreManager.getCore(IAuthCore.class).getTicket());
            } else {
                toast("填写的手机号与已绑定的手机号不一致！");
            }
        }
    }

    @OnClick(R.id.btn_binder)
    void onClickBinder(View view) {
        String phone2 = mPhoneNumberEdt.getText().toString();
        String code = mCodeNumberEdt.getText().toString();
        if (StringUtil.isEmpty(phone2)) {
            toast("手机号码不能为空哦！");
            return;
        }
        if (StringUtil.isEmpty(code)) {
            toast("邀请码不能为空哦！");
            return;
        }
        getMvpPresenter().checkCode(CoreManager.getCore(IAuthCore.class).getCurrentUid(), CoreManager.getCore(IAuthCore.class).getTicket(), code);
    }


    @Override
    public void onRemindToastError(String error) {
        toast(error);
    }

    @Override
    public void onRemindToastSuc() {
        toast("验证码获取成功！");
    }

    @Override
    public void onCheckCodeFailToast(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void onCheckCodeSucWeixinLogin() {
        getDialogManager().showProgressDialog(this, "加载中...");
        wechat = ShareSDK.getPlatform(Wechat.NAME);
        if (!wechat.isClientValid()) {
            toast("未安装微信");
            getDialogManager().dismissDialog();
            return;
        }
        if (wechat.isAuthValid()) {
            wechat.removeAccount(true);
        }
        wechat.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(final Platform platform, int i, HashMap<String, Object> hashMap) {
                if (i == Platform.ACTION_USER_INFOR) {
                    String openid = platform.getDb().getUserId();
                    String unionid = platform.getDb().get("unionid");
                    String access_token = platform.getDb().getToken();
                    String name = platform.getDb().getUserName();


                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString(WithdrawActivity.BUNDLE_KEY_WEIXIN_PHONE, mPhoneNumberEdt.getText().toString());
                    bundle.putString(WithdrawActivity.BUNDLE_KEY_WEIXIN_OPEN_ID, openid);
                    bundle.putString(WithdrawActivity.BUNDLE_KEY_WEIXIN_NAME, name);
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    finish();
//                    getMvpPresenter().bindWithdrawWeixin(CoreManager.getCore(IAuthCore.class).getCurrentUid(), CoreManager.getCore(IAuthCore.class).getTicket(), access_token, openid, unionid);
                } else {
                    getDialogManager().dismissDialog();
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                getDialogManager().dismissDialog();
                toast("微信授权错误");
            }

            @Override
            public void onCancel(Platform platform, int i) {
                getDialogManager().dismissDialog();
                toast("取消微信授权");
            }
        });
        wechat.SSOSetting(false);
        wechat.showUser(null);
    }

    @Override
    public void onRemindBindWeixinSucFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void onRemindBindWeixinSucToast() {
        getDialogManager().dismissDialog();
        toast("绑定成功");
        EventBus.getDefault().post(new RefreshInfo());
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCodeDownTimer != null) {
            mCodeDownTimer.cancel();
            mCodeDownTimer = null;
        }
        if (wechat != null) {
            wechat.setPlatformActionListener(null);
        }
    }
}
