package com.yiya.mobile.ui.me.wallet.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrwaListInfo;
import com.yiya.mobile.ui.me.withdraw.WithdrawActivity;

/**
 * <p>  钻石提现界面</p>
 *
 * @author Administrator
 * @date 2017/11/21
 */
public class WithdrawJewelAdapter extends BaseQuickAdapter<WithdrwaListInfo, BaseViewHolder> {
    private int mWithdrawType = WithdrawActivity.TYPE_DIAMOND;

    public WithdrawJewelAdapter() {
        super(R.layout.withdraw_item);
    }

    public void setWithdrawType(int type) {
        mWithdrawType = type;
    }


    @Override
    protected void convert(BaseViewHolder baseViewHolder, WithdrwaListInfo withdrwaListInfo) {
        if (withdrwaListInfo == null) {
            return;
        }
        ImageView bg = baseViewHolder.itemView.findViewById(R.id.select_withdraw);

        TextView gold = baseViewHolder.getView(R.id.list_name);

        TextView amount = baseViewHolder.getView(R.id.tv_wd_money);


        if (mWithdrawType == WithdrawActivity.TYPE_DIAMOND) {
            try {
                String[] split = withdrwaListInfo.cashProdName.split("钻=");
                baseViewHolder.setText(R.id.list_name, split[1]);
                baseViewHolder.setText(R.id.tv_wd_money, "" + split[0] + "钻石");
            } catch (Exception e) {
                baseViewHolder.setText(R.id.list_name, withdrwaListInfo.cashProdName);
            }
        } else {

            try {
                String[] split = withdrwaListInfo.cashProdName.split("咿呀=");
                baseViewHolder.setText(R.id.list_name, split[1]);
                baseViewHolder.setText(R.id.tv_wd_money, "" + split[0] + "豆");
            } catch (Exception e) {
                baseViewHolder.setText(R.id.list_name, withdrwaListInfo.cashProdName);
            }


        }
        bg.setSelected(withdrwaListInfo.isSelected);

        gold.setSelected(withdrwaListInfo.isSelected);

        amount.setSelected(withdrwaListInfo.isSelected);

        if(withdrwaListInfo.isSelected){
            gold.setTextColor(mContext.getResources().getColor(R.color.white));
            amount.setTextColor(mContext.getResources().getColor(R.color.white));
        }else{
            gold.setTextColor(mContext.getResources().getColor(R.color.color_ff6a99));
            amount.setTextColor(mContext.getResources().getColor(R.color.color_222222));
        }

        amount.setEnabled(withdrwaListInfo.isWd());

    }
}
