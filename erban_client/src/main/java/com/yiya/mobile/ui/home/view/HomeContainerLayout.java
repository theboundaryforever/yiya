package com.yiya.mobile.ui.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * 文件描述：自定义协调布局在分发事件中添加整个外层布局的上滑和下滑时间，
 * 用于控制首页的功能按钮的显示和隐藏
 *
 * @auther：zwk
 * @data：2019/1/18
 */
public class HomeContainerLayout extends RelativeLayout {
    private float actionY = 0;
    private float moveY = 0;
    private float moveX = 0;
    private float actionX = 0;
    private OnHomeFunctionHideListener onHomeFunctionHideListener;

    public HomeContainerLayout(Context context) {
        super(context);
    }

    public HomeContainerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HomeContainerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                actionX = ev.getX();
                actionY = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                moveX = ev.getX() - actionX;
                moveY = ev.getY() - actionY;
                break;
            case MotionEvent.ACTION_SCROLL:
                break;
            case MotionEvent.ACTION_UP:
                if (Math.abs(moveX) < Math.abs(moveY)) {
                    if (moveY > 10) {//下拉移动大过10
                        if (onHomeFunctionHideListener != null) {
                            onHomeFunctionHideListener.showAnim();
                        }
                    } else if (moveY < -10) {
                        if (onHomeFunctionHideListener != null) {
                            onHomeFunctionHideListener.hideAnim();
                        }
                    }
                }
                actionY = 0;
                moveY = 0;

                moveX = 0;
                actionX = 0;
                break;
        }
        try {
            return super.dispatchTouchEvent(ev);
        } catch (IllegalArgumentException e) {//#20104 java.lang.IllegalArgumentException  pointerIndex out of range  暂时未找到触发这个异常的情形
            e.printStackTrace();
            return true;
        }
    }

    public void setOnHomeFunctionHideListener(OnHomeFunctionHideListener onHomeFunctionHideListener) {
        this.onHomeFunctionHideListener = onHomeFunctionHideListener;
    }

    public interface OnHomeFunctionHideListener {
        void showAnim();

        void hideAnim();
    }
}
