package com.yiya.mobile.ui.me.user.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.user.bean.DressUpBean;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/30
 * 描述        ${}
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class UserDressAdapter extends BaseQuickAdapter<DressUpBean, BaseViewHolder> {

    public UserDressAdapter() {
        super(R.layout.user_dress_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, DressUpBean item) {
        ImageView iv_dress = helper.getView(R.id.iv_dress);
        ImageLoadUtils.loadAvatar(iv_dress.getContext(), item.getPicUrl(), iv_dress, false);
    }
}
