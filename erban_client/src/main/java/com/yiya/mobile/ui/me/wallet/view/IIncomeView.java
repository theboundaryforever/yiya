package com.yiya.mobile.ui.me.wallet.view;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public interface IIncomeView extends IPayView {

    public void handleClick(int id);

    public void hasBindPhone();

    public void hasBindPhoneFail(String error);
}
