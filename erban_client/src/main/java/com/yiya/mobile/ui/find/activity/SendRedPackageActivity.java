package com.yiya.mobile.ui.find.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.presenter.find.ISendRedPackageView;
import com.yiya.mobile.presenter.find.SendRedPackagePresenter;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.me.wallet.activity.MyWalletActivity;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.listener.SingleClickListener;
import com.tongdaxing.xchat_core.bean.RoomRedPackageInfo;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 文件描述：发送红包的功能界面
 *
 * @auther：zdw
 * @data：2019/5/17
 */
@CreatePresenter(SendRedPackagePresenter.class)
public class SendRedPackageActivity extends BaseMvpActivity<ISendRedPackageView, SendRedPackagePresenter> implements ISendRedPackageView, View.OnClickListener {
    private static final String TAG = SendRedPackageActivity.class.getSimpleName();

    private static int MAXREDPACKETGOLDSUM = 100000;// 红包金额上限
    private static int MAXREDPACKETNUM = 100; // 红包数量上限

    @BindView(R.id.tv_send_red_package)
    TextView tv_send_red_package;
    @BindView(R.id.tv_my_gold_num)
    TextView tvMyGold;
    @BindView(R.id.et_input_red_package_gold)
    EditText etInputGold;
    @BindView(R.id.et_input_red_package)
    EditText etInputNum;
    @BindView(R.id.iv_rule_close)
    ImageView iv_rule_close;

    @BindView(R.id.rl_rule)
    RelativeLayout rl_rule;

    @BindView(R.id.toolbar)
    AppToolBar atbTitle;

    @BindView(R.id.tv_redpack_limit)
    TextView tv_redpack_limit;
    @BindView(R.id.tv_redpack_display)
    TextView tv_redpack_display;

    private long roomId = 0;
    private boolean isRefreshWallet = false;

    public static void start(Context context, long roomid) {
        Intent intent = new Intent(context, SendRedPackageActivity.class);
        intent.putExtra("roomId", roomid);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_red_package);
        ButterKnife.bind(this);
        initIntent();
        initListener();
    }

    private void initIntent() {
        roomId = getIntent().getLongExtra("roomId", 0);
    }

    private void initListener() {
        tv_redpack_limit.setTag(false);
        tv_redpack_limit.setClickable(false);
        tv_redpack_display.setTag(false);
        tv_redpack_display.setClickable(false);

        etInputGold.addTextChangedListener(etInputGoldWatcher);
        GrowingIO.getInstance().trackEditText(etInputGold);
        GrowingIO.getInstance().trackEditText(etInputNum);
        atbTitle.setOnRightBtnClickListener(v -> {
            if (!isRefreshWallet) {
                getMvpPresenter().refreshWalletInfo(true);
                isRefreshWallet = true;
            }
        });

        atbTitle.setOnBackBtnListener(v -> finish());

        tv_send_red_package.setOnClickListener(new SingleClickListener() {
            @Override
            public void singleClick(View v) {
                sendRedPack();
            }
        });
    }

    @OnClick({R.id.ll_to_recharge,
            R.id.tv_redpack_limit, R.id.tv_redpack_display,
            R.id.dtv_rule, R.id.iv_rule_close
    })
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_to_recharge:
                MyWalletActivity.start(SendRedPackageActivity.this,1);
                break;
            case R.id.tv_redpack_limit:
                redPacketLimit();
                break;
            case R.id.tv_redpack_display:
                redPacketDisplay();
                break;
            case R.id.dtv_rule:
                rl_rule.setVisibility(View.VISIBLE);
                break;
            case R.id.iv_rule_close:
                rl_rule.setVisibility(View.INVISIBLE);
                break;
        }
    }

    // 红包限制领取
    private void redPacketLimit() {
        if ((Boolean) tv_redpack_limit.getTag()) {
            tv_redpack_limit.setTag(false);
            setTextViewLeftDrawable(tv_redpack_limit, R.mipmap.ic_redpack_uncheck);
        } else {
            tv_redpack_limit.setTag(true);
            setTextViewLeftDrawable(tv_redpack_limit, R.mipmap.ic_redpack_check);
        }
    }

    // 红包显示控制
    private void redPacketDisplay() {
        if ((Boolean) tv_redpack_display.getTag()) {
            tv_redpack_display.setTag(false);
            setTextViewLeftDrawable(tv_redpack_display, R.mipmap.ic_redpack_uncheck);
        } else {
            tv_redpack_display.setTag(true);
            setTextViewLeftDrawable(tv_redpack_display, R.mipmap.ic_redpack_check);
        }
    }

    // 发送红包
    private void sendRedPack() {
        if (etInputNum.getText() == null || etInputGold.getText() == null) {
            SingleToastUtil.showToast("输入结果异常！");
            return;
        }
        String redpackNum = etInputNum.getText().toString();
        String goldNum = etInputGold.getText().toString();
        int redpackType = RoomRedPackageInfo.REDPACKETTYPE_ALL;
        int noticeAll = RoomRedPackageInfo.REDPACKETDISPLAY_NO;

        boolean isValid = redPackageValidate(redpackNum, goldNum);
        if (isValid) {
            if ((Boolean) tv_redpack_limit.getTag()) {// 已勾选仅麦上用户可领取红包
                redpackType = RoomRedPackageInfo.REDPACKETTYPE_OMMIC;
            } else {
                redpackType = RoomRedPackageInfo.REDPACKETTYPE_ALL;
            }
            if ((Boolean) tv_redpack_display.getTag()) {
                noticeAll = RoomRedPackageInfo.REDPACKETDISPLAY_ALL;
            } else {
                noticeAll = RoomRedPackageInfo.REDPACKETDISPLAY_NO;
            }
            getDialogManager().showProgressDialog(this, "", false);
            getMvpPresenter().sendRedPakcageFromMyself(roomId + "", goldNum, redpackNum, redpackType, noticeAll);
        } else {
            return;
        }
    }

    // 红包校验
    private boolean redPackageValidate(String redpackNum, String goldNum) {
        if (StringUtils.isEmpty(redpackNum) || StringUtils.isEmpty(goldNum)) {
            SingleToastUtil.showToast("红包个数或者金币总数不能为空！");
            return false;
        }

        // 红包金额校验
        Long d_num = Long.valueOf(goldNum);
        if (d_num < 1) {
            SingleToastUtil.showToast("每个红包不能小于1金币哦！");
            return false;
        } else if (d_num > MAXREDPACKETGOLDSUM) {
            etInputGold.setText(String.valueOf(MAXREDPACKETGOLDSUM));
            SingleToastUtil.showToast("红包金额最多不能大于" + MAXREDPACKETGOLDSUM + "哦！");
            return false;
        }

        // 红包个数校验
        Long p_num = Long.valueOf(redpackNum);
        if (p_num < 1) {
            SingleToastUtil.showToast("发送红包不能小于1个！");
            return false;
        } else if (p_num > d_num) {
            etInputNum.setText(String.valueOf(d_num));
            SingleToastUtil.showToast("发送红包个数不能大于红包金额！");
            return false;
        } else if (p_num > MAXREDPACKETNUM) {
            etInputNum.setText(String.valueOf(MAXREDPACKETNUM));
            SingleToastUtil.showToast("发送红包个数不能大于" + MAXREDPACKETNUM + "个哦！");
            return false;
        }

        return true;
    }

    private TextWatcher etInputGoldWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String goldNum = s.toString();

            if (StringUtils.isEmpty(goldNum)) {
                return;
            }

            Long i_goldNum = Long.valueOf(goldNum);
            Log.d(TAG, "afterTextChanged: " + i_goldNum);
            if (i_goldNum >= 1000) {
                setTextViewLeftDrawable(tv_redpack_limit, R.mipmap.ic_redpack_uncheck);
                tv_redpack_limit.setClickable(true);
            } else {
                setTextViewLeftDrawable(tv_redpack_limit, R.mipmap.ic_redpack_unclickable);
                tv_redpack_limit.setClickable(false);
            }

            if (i_goldNum >= 10000) {
                setTextViewLeftDrawable(tv_redpack_display, R.mipmap.ic_redpack_uncheck);
                tv_redpack_display.setClickable(true);
            } else {
                setTextViewLeftDrawable(tv_redpack_display, R.mipmap.ic_redpack_unclickable);
                tv_redpack_display.setClickable(false);
            }
        }
    };

    private void setTextViewLeftDrawable(TextView textView, int drawId) {
        Drawable drawable = getResources().getDrawable(drawId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        textView.setCompoundDrawables(drawable, null, null, null);
    }


    @Override
    public void onUpdateGoldNumView(WalletInfo walletInfo) {
        isRefreshWallet = false;
        tvMyGold.setText(String.valueOf(walletInfo.getGoldNum()));
    }

    @Override
    public void onUpdateGoldNumFail(String error) {
        isRefreshWallet = false;
        tvMyGold.setText("0.00");
    }

    @Override
    public void onSendSucFinishPage() {
        dismissDialog();
        getDialogManager().showOkDialog("发送成功", true, new DialogManager.OkDialogListener() {
            @Override
            public void onOk() {
                finish();
            }
        });
    }

    @Override
    public void onSendFailToastRemind(String error) {
        dismissDialog();
        SingleToastUtil.showToast(error);
    }

    @Override
    public void onSendRPNotEnoughMoneyRemind() {
        dismissDialog();
        getDialogManager().showOkCancelDialog("余额不足，是否充值", true,
                new DialogManager.OkCancelDialogListener() {
                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onOk() {
                        MyWalletActivity.start(SendRedPackageActivity.this,1);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMvpPresenter().refreshWalletInfo(true);
    }
}
