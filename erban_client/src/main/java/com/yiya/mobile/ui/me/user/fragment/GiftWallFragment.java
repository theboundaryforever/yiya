package com.yiya.mobile.ui.me.user.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.yiya.mobile.base.fragment.BaseFragment;
import com.yiya.mobile.ui.me.user.adapter.GiftWallAdapter;
import com.yiya.mobile.ui.widget.emptyView.DefaultEmptyEnum;

import java.util.List;

import butterknife.BindView;

/**
 * ProjectName:
 * Description:个人主页-礼物墙
 * Created by BlackWhirlwind on 2019/9/17.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class GiftWallFragment extends BaseFragment {

    @BindView(R.id.rv_gift_wall)
    RecyclerView rvGiftWall;

    private static final String KEY_USER_ID = "KEY_USER_ID";
    private GiftWallAdapter giftWallAdapter;
    private GridLayoutManager layoutManager;

    public static GiftWallFragment newInstance(long userId) {
        Bundle bundle = new Bundle();
        bundle.putLong(KEY_USER_ID, userId);
        GiftWallFragment fragment = new GiftWallFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_gift_wall;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        if (getArguments() == null) {
            return;
        }
        long userId = getArguments().getLong(KEY_USER_ID);
        if (userId == 0) {
            return;
        }
        giftWallAdapter = new GiftWallAdapter();
        rvGiftWall.setAdapter(giftWallAdapter);
        layoutManager = new GridLayoutManager(mContext, 1);
        rvGiftWall.setLayoutManager(layoutManager);
        giftWallAdapter.setEmptyView(getEmptyView(rvGiftWall, DefaultEmptyEnum.EMPTY_GIFT_WALL));
        CoreManager.getCore(IUserCore.class).requestUserGiftWall(userId, 2);
    }


    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestGiftWall(List<GiftWallInfo> giftWallInfoList) {
        if (ListUtils.isListEmpty(giftWallInfoList)) {
            return;
        }
        layoutManager.setSpanCount(4);
        giftWallAdapter.setNewData(giftWallInfoList);

    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestGiftWallFail(String msg) {

    }


}
