package com.yiya.mobile.ui.home.adpater;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.ui.me.shopping.activity.ShopActivity;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.mengcoin.MengCoinBean;

public class MengCoinSignAdapter extends BaseQuickAdapter<MengCoinBean, BaseViewHolder> {


    public MengCoinSignAdapter() {
        super(R.layout.item_rv_meng_coin_sign);
    }

    @Override
    protected void convert(BaseViewHolder helper, MengCoinBean item) {
        RelativeLayout llContent = helper.getView(R.id.ll_mb_sign_content);
        ImageView ivSignBg = helper.getView(R.id.iv_mb_sign_state);
        TextView tvReceive = helper.getView(R.id.tv_mb_sign_have_received);
        TextView tvCoinCount = helper.getView(R.id.tv_mb_sign_received_coin_count);
        if (helper.getAdapterPosition() == 6) {
            if (item.getMissionStatus() == 3) {
                llContent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShopActivity.start(mContext, true, 0);
                    }
                });
            }
            if (tvReceive.getVisibility() == View.VISIBLE) {
                tvReceive.setVisibility(View.GONE);
            }
            if (tvCoinCount.getVisibility() == View.VISIBLE) {
                tvCoinCount.setVisibility(View.GONE);
            }
            ImageLoadUtils.loadImage(mContext,item.getPicUrl(),ivSignBg);
        } else {
            if (tvReceive.getVisibility() == View.GONE) {
                tvReceive.setVisibility(View.VISIBLE);
            }
            if (tvCoinCount.getVisibility() == View.GONE) {
                tvCoinCount.setVisibility(View.VISIBLE);
            }
            ivSignBg.setImageResource(R.drawable.selector_meng_coin_sign);
        }
        helper.setText(R.id.tv_mb_sign_received_coin_count, item.getMcoinAmount() + "");
        if (item.getMissionStatus() == 3) {
            ivSignBg.setSelected(true);
            if (tvReceive.getVisibility() == View.GONE) {
                tvReceive.setVisibility(View.VISIBLE);
            }
            tvCoinCount.setSelected(true);
        } else {
            ivSignBg.setSelected(false);
            if (tvReceive.getVisibility() == View.VISIBLE) {
                tvReceive.setVisibility(View.GONE);
            }
            tvCoinCount.setSelected(false);
        }
    }
}
