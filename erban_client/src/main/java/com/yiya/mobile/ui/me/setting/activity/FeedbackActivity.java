package com.yiya.mobile.ui.me.setting.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.me.MePresenter;
import com.yiya.mobile.ui.me.task.view.IMeView;
import com.juxiao.library_ui.widget.AppToolBar;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;

@CreatePresenter(MePresenter.class)
public class FeedbackActivity extends BaseMvpActivity<IMeView, MePresenter> implements IMeView {

    public static final String TAG = "FeedbackActivity";

    private EditText edtContent;
    private EditText edtContact;
    private AppToolBar titleBar;

    private Button mCommitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "意见反馈");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "问题反馈页");
        setContentView(R.layout.activity_feedback);
        initView();
        initData();
        SetListener();
    }

    private void SetListener() {
        titleBar.setOnBackBtnListener(view -> finish());
        titleBar.setOnRightBtnClickListener(view -> {
            String etContent = edtContent.getText().toString().trim();
            if (StringUtil.isEmpty(etContent)) {
                toast("请输入您要反馈的内容");
                return;
            }
            String etContact = edtContact.getText().toString().trim();
            if (StringUtil.isEmpty(etContact)) {
                toast("请输入您的微信或QQ号码");
                return;
            }
            getMvpPresenter().commitFeedback(etContent, etContact);
        });

        findViewById(R.id.btn_commit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String etContent = edtContent.getText().toString().trim();
                if (StringUtil.isEmpty(etContent)) {
                    toast("请输入您要反馈的内容");
                    return;
                }
                String etContact = edtContact.getText().toString().trim();
                if (StringUtil.isEmpty(etContact)) {
                    toast("请输入您的微信或QQ号码");
                    return;
                }
                getMvpPresenter().commitFeedback(etContent, etContact);
            }
        });

    }

    private void initData() {


    }

    private void initView() {
        edtContent = (EditText) findViewById(R.id.edt_content);
        GrowingIO.getInstance().trackEditText(edtContent);
        edtContact = (EditText) findViewById(R.id.edt_contact);
        GrowingIO.getInstance().trackEditText(edtContact);
        titleBar = (AppToolBar) findViewById(R.id.toolbar);
    }

    @Override
    public void commitFeedback() {
        getDialogManager().showProgressDialog(FeedbackActivity.this, "正在上传请稍后...");
        getMvpPresenter().uploadLog();
        //日志文件提交
        LogUtil.i("日志文件提交");
    }

    @Override
    public void commitFeedbackFail(String errorMsg) {
        toast(errorMsg);
    }


}
