package com.yiya.mobile.ui.widget.dialog;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.fourmob.datetimepicker.date.DatePickerDialog;

/**
 * 文件描述：解决该控件库 show出dialog时如下错误
 * #20907 java.lang.IllegalStateException
 * Can not perform this action after onSaveInstanceState
 * UserInfoModifyActivity.onClick(UserInfoModifyActivity.java:374)
 *
 * @auther：zwk
 * @data：2019/3/11
 */
public class UserDatePickerDialog extends DatePickerDialog {

    public static UserDatePickerDialog newInstanceUser(OnDateSetListener onDateSetListener, int year, int month, int day, boolean vibrate) {
        UserDatePickerDialog datePickerDialog = new UserDatePickerDialog();
        datePickerDialog.initialize(onDateSetListener, year, month, day, vibrate);
        return datePickerDialog;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 在activity显示使用上面的方法会导致
     *  dialog出现几次，activity需要多返回几次才能finish
     *  将fragment添加到stack中dismiss是因为mBackStackId=-1没有退栈
     * @param fragmentManager
     * @param tag
     */
    public void showDialog(FragmentManager fragmentManager, String tag){
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag);
        transaction.commitAllowingStateLoss();
        //或者
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.addToBackStack(null);
//        show(transaction,tag);
    }


}
