package com.yiya.mobile.ui.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Switch;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.yiya.mobile.base.fragment.BaseMvpDialogFragment;
import com.yiya.mobile.room.avroom.adapter.MicroApplyListAdapter;
import com.yiya.mobile.room.presenter.IMicroApplyListView;
import com.yiya.mobile.room.presenter.MicroApplyListPresenter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo.MicroApplyInfo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.tongdaxing.xchat_core.manager.AvRoomDataManager.MIC_POSITION_BY_VIDEO_GUEST;

/**
 * ProjectName:
 * Description:视频放连麦列表dialog
 * Created by BlackWhirlwind on 2019/6/6.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
@CreatePresenter(MicroApplyListPresenter.class)
public class MicroApplyListDialog extends BaseMvpDialogFragment<IMicroApplyListView, MicroApplyListPresenter> implements IMicroApplyListView, BaseQuickAdapter.OnItemChildClickListener {

    @BindView(R.id.tv_num)
    TextView tvNum;
    @BindView(R.id.switch_micro)
    Switch switchMicro;
    @BindView(R.id.rv_apply_list)
    RecyclerView rvApplyList;

    private MicroApplyListAdapter mAdapter;
    private Unbinder unbinder;
    private TextView tvTips;

    public static MicroApplyListDialog newInstance() {
        return new MicroApplyListDialog();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        View rootView = inflater.inflate(R.layout.dialog_micro_apply_list, window.findViewById(android.R.id.content), false);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setWindowAnimations(R.style.WindowBottomAnimationStyle);
        window.setGravity(Gravity.BOTTOM);

        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public int getRootLayoutId() {
        return 0;
    }

    @Override
    public void onFindViews() {
        mAdapter = new MicroApplyListAdapter();
        rvApplyList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvApplyList.setAdapter(mAdapter);
        mAdapter.setEmptyView(R.layout.layout_empty_micro_apply_list, rvApplyList);
        tvTips = mAdapter.getEmptyView().findViewById(R.id.tv_micro_apply_default);
        //是否开启连麦开关
        boolean isCanConnectMic = AvRoomDataManager.get().getIsCanConnectMic();
        //设置连麦开关和缺省ui
        switchMicro.setChecked(isCanConnectMic);
        tvTips.setText(isCanConnectMic ? "暂无人发起连线互动" : "未开启连麦申请");
    }

    @Override
    public void onSetListener() {
        mAdapter.setOnItemChildClickListener(this);
        switchMicro.setOnCheckedChangeListener((buttonView, isChecked) -> {
            getMvpPresenter().setRoomConnectMic(isChecked);
        });
    }

    @Override
    public void initiate() {
        getDialogManager().showProgressDialog(getContext());
        getMvpPresenter().getMicApplyList();
        subscribeChatRoomEventObservable();
    }

    /**
     * 监听房间事件
     */
    public void subscribeChatRoomEventObservable() {
        IMNetEaseManager.get().subscribeChatRoomEventObservable(this::receiveRoomEvent, this);
    }

    /**
     * 接收房间事件
     */
    public void receiveRoomEvent(RoomEvent roomEvent) {
        if (roomEvent == null) {
            return;
        }
        int event = roomEvent.getEvent();
        switch (event) {
            case RoomEvent.DOWN_MIC://下麦
                getMvpPresenter().getMicApplyList();
                break;
            case RoomEvent.CODE_GUEST_APPLY_MICRO://嘉宾申请连麦
            case RoomEvent.CODE_GUEST_CANCEL_MICRO://嘉宾取消连麦申请
                showMicroApplyList(roomEvent.getRoomMicroApplyInfo());
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        MicroApplyInfo item = (MicroApplyInfo) adapter.getData().get(position);
        switch (item.getItemType()) {
            case MicroApplyInfo.MICRO_APPLY_NORMAL:
                switch (view.getId()) {
                    case R.id.iv_accept:
                        //接受连麦
                        boolean isHaveMemberOnMicro = AvRoomDataManager.get().isHaveMemberOnMicro();
                        if (isHaveMemberOnMicro) {
                            //主播和嘉宾连麦中
                            toast("正在连线互动中，请先挂断");
                            return;
                        }
                        getMvpPresenter().agreeMicroApply(item);
                        break;
                    case R.id.iv_reject:
                        //拒绝
                        getMvpPresenter().rejectMicroApply(item, position);
                        break;
                }
                break;
            case MicroApplyInfo.MICRO_APPLY_CONNECTING:
                if (R.id.iv_shut_down == view.getId()) {
                    //挂断
                    getMvpPresenter().downMicro(MIC_POSITION_BY_VIDEO_GUEST, null);
                }
                break;
        }
    }

    @Override
    public void setRoomConnectMicSuccess(boolean isCanConnectMic) {
        tvTips.setText(isCanConnectMic ? "暂无人发起连线互动" : "未开启连麦申请");
        if (!isCanConnectMic) {
            //关闭连麦开关则刷新列表
            getMvpPresenter().getMicApplyList();
        }
    }

    @Override
    public void showMicroApplyList(RoomMicroApplyInfo roomMicroApplyInfo) {
        //刷新主播连麦按钮状态
        IMNetEaseManager.get().getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.CODE_BROADCASTER_REFRESH_MICRO_APPLY)
                        .setRoomMicroApplyInfo(roomMicroApplyInfo));
        if (roomMicroApplyInfo == null) {
            return;
        }
        String num = String.format("申请人数:%s", roomMicroApplyInfo.getTotal());
        tvNum.setText(num);
        mAdapter.setNewData(roomMicroApplyInfo.getApplyList());
    }

    @Override
    public void finish() {

    }

    @Override
    public void dismissDialog() {
        getDialogManager().dismissDialog();
    }

}
