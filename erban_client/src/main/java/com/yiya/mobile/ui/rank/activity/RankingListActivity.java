package com.yiya.mobile.ui.rank.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.base.adapter.BaseIndicatorAdapter;
import com.yiya.mobile.ui.rank.adapter.RankingIndicatorAdapter;
import com.yiya.mobile.ui.rank.fragment.RankingItemFragment;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.yiya.mobile.ui.rank.fragment.RankingListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/26
 * 描述        排行榜
 * <p>
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    视频房排行榜
 *
 * @author dell
 */
public class RankingListActivity extends BaseActivity implements RankingIndicatorAdapter.OnItemSelectListener {

    /**
     * dateType       类型：1，日；2，周；3，总榜
     */
    public static final int DATE_TYPE_DAY = 1, DATE_TYPE_MONTH = 2, DATE_TYPE_ALL = 3;

    //来源类型 1：房间 2：个人
    public static final int FROM_TYPE_PERSONAL = 1, FROM_TYPE_ROOM = 2;
    public static final String BUNDLE_KEY_FROM_TYPE = "fromType";

    public static final String BUNDLE_KEY_TYPE = "type";
    public static final String BUNDLE_KEY_DATA_TYPE = "dateType";
    //个人（自己 或者 别人）  房间
    public static final String BUNDLE_KEY_UID = "uid";
    public static final String BUNDLE_KEY_QUERY_UID = "queryUid";


    @BindView(R.id.arrow_back)
    ImageView mArrowBackIv;
    @BindView(R.id.bg_rl)
    RelativeLayout mBgRl;
    @BindView(R.id.title_bg_rl)
    RelativeLayout mTitleBgRl;
    @BindView(R.id.title_name_tv)
    TextView mTitleNameTv;

    @BindView(R.id.ranking_content_empty_ll)
    LinearLayout mContentEmptyLl;

    @BindView(R.id.indicator)
    MagicIndicator mIndicator;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    List<Fragment> mTabs;
    private BaseIndicatorAdapter mTabAdapter;

    public static void start(Context context, String uid, String queryUid) {
        Intent intent = new Intent(context, RankingListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_KEY_UID, uid);
        bundle.putString(BUNDLE_KEY_QUERY_UID, queryUid);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_rank);
        ButterKnife.bind(this);
        initView();
        setOnListener();
    }

    private void initView() {
//        GrowingIO.getInstance().setPageName(this, "礼物贡献榜");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "礼物贡献榜页");

//        mBgRl.setBackgroundResource(R.drawable.bg_ranking_top);
//        mTitleBgRl.setBackgroundResource(R.drawable.bg_ranking_top);
        mTitleNameTv.setTextColor(getResources().getColor(R.color.color_white));
        mContentEmptyLl.setVisibility(View.GONE);

        mTabs = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        String uid = bundle.getString(BUNDLE_KEY_UID);
        String queryUid = bundle.getString(BUNDLE_KEY_QUERY_UID);

        mTabs.add(RankingItemFragment.newInstance(RankingListFragment.RANKING_TYPE_TYCOON, DATE_TYPE_DAY, uid, queryUid));
        mTabs.add(RankingItemFragment.newInstance(RankingListFragment.RANKING_TYPE_TYCOON, DATE_TYPE_MONTH, uid, queryUid));
        mTabs.add(RankingItemFragment.newInstance(RankingListFragment.RANKING_TYPE_TYCOON, DATE_TYPE_ALL, uid, queryUid));

        List<TabInfo> tabInfoList = new ArrayList<>();
        tabInfoList.add(new TabInfo(1, "日榜"));
        tabInfoList.add(new TabInfo(2, "周榜"));
        tabInfoList.add(new TabInfo(2, "总榜"));
        RankingIndicatorAdapter indicatorAdapter = new RankingIndicatorAdapter(this, tabInfoList);

        indicatorAdapter.setOnItemSelectListener(this);

        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(indicatorAdapter);
        mIndicator.setNavigator(commonNavigator);

        // must after setNavigator
        LinearLayout titleContainer = commonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        mTabAdapter = new BaseIndicatorAdapter(getSupportFragmentManager(), mTabs);
        mViewPager.setAdapter(mTabAdapter);
        mViewPager.setOffscreenPageLimit(2);
        ViewPagerHelper.bind(mIndicator, mViewPager);
    }

    @Override
    public void onItemSelect(int position) {
        mViewPager.setCurrentItem(position);
    }

    @OnClick(R.id.arrow_back)
    void onClickBack(View view) {
        finish();

    }

    private void setOnListener() {

    }
}
