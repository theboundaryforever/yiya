package com.yiya.mobile.ui.login.information.activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.login.information.presenter.InformationSupplmentPresenter;
import com.yiya.mobile.ui.login.information.view.InformationSupplmentView;
import com.yiya.mobile.ui.more.MoreActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@CreatePresenter(InformationSupplmentPresenter.class)
public class InformationSupplmentActivity extends BaseMvpActivity<InformationSupplmentView, InformationSupplmentPresenter> implements InformationSupplmentView{

    public static void start(Context context) {
        Intent intent = new Intent(context, MoreActivity.class);
        context.startActivity(intent);
    }

    @BindView(R.id.exit_fl)
    FrameLayout exit_fl;

    @BindView(R.id.iv_headportrait)
    ImageView iv_headportrait;

    @BindView(R.id.nickname_edt)
    EditText nickname_edt;

    @BindView(R.id.layout_nickrandom)
    LinearLayout layout_nickrandom;

    @BindView(R.id.layout_date)
    LinearLayout layout_date;

    @BindView(R.id.iv_sexboy)
    TextView iv_sexboy;

    @BindView(R.id.iv_sexgirl)
    TextView iv_sexgirl;

    @BindView(R.id.btn_next)
    Button btn_next;

    private Boolean sexBoolean = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_supplement);
        ButterKnife.bind(this);
        init();
    }

    private void init(){
        exit_fl.setOnClickListener(v -> finish());
        nickname_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                 if(!TextUtils.isEmpty(nickname_edt.getText().toString())){
                     btn_next.setEnabled(true);
                 }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick({R.id.iv_sexboy,R.id.iv_sexgirl})
    void onclick(View v){
        switch (v.getId()){
            case R.id.iv_sexboy:
                if(sexBoolean){
                    toast("我是小哥哥");
                }else{
                    sexBoolean = true;
                    iv_sexboy.setEnabled(true);
                    iv_sexgirl.setEnabled(false);
                }
                break;
            case R.id.iv_sexgirl:
                if(sexBoolean){
                    sexBoolean = false;
                    iv_sexboy.setEnabled(false);
                    iv_sexgirl.setEnabled(true);
                }else{
                    toast("我是小解解");
                }
                break;
        }
    }

    @Override
    public String getHeadPortrait() {
        return null;
    }

    @Override
    public String getNickName() {
        return null;
    }

    @Override
    public String getDate() {
        return null;
    }

    @Override
    public String getSex() {
        return null;
    }
}
