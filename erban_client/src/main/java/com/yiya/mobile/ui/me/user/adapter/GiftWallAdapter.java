package com.yiya.mobile.ui.me.user.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/17.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class GiftWallAdapter extends BaseQuickAdapter<GiftWallInfo, BaseViewHolder> {

    public GiftWallAdapter() {
        super(R.layout.item_gift_wall);
    }

    @Override
    protected void convert(BaseViewHolder helper, GiftWallInfo item) {
        if (item == null) {
            return;
        }
        ImageLoadUtils.loadImage(mContext, item.getPicUrl(),
                helper.getView(R.id.iv_gift));
        helper.setText(R.id.tv_name, item.getGiftName())
                .setText(R.id.tv_num, "x".concat(String.valueOf(item.getReciveCount())));


    }
}
