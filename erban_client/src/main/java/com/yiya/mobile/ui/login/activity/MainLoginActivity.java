package com.yiya.mobile.ui.login.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.utils.JPushHelper;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nis.captcha.Captcha;
import com.netease.nis.captcha.CaptchaListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.coremanager.IAppInfoCore;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.config.SpEvent;
import com.tongdaxing.xchat_framework.util.constant.AppKey;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.SpUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangsongzhoubtn_login
 * @date 2019/3/19
 */
public class MainLoginActivity extends BaseActivity {


    private static final String TAG = "MainLoginActivity";
    /**
     * 基本权限管理
     */
    private final String[] BASIC_PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO,
    };


    public static void start(Context context) {
        Intent intent = new Intent(context, MainLoginActivity.class);
        context.startActivity(intent);
    }

    private String validateStr = "";
    private int mNextSetupViewId;
    //避免重复点击多吃执行
    private boolean captchaIsShowing = false;

    private Captcha mCaptcha;

    private boolean isResume;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "注册-登录页");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "注册-登录页");

        setContentView(R.layout.activity_main_login);
        ButterKnife.bind(this);

        initImgCheck();
        checkPermission(() -> {
        }, R.string.ask_again, BASIC_PERMISSIONS);
        setSwipeBackEnable(false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        isResume = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isResume = false;
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLoginFail(String error) {
        getDialogManager().dismissDialog();
        LogUtil.i("onLogin", "login fail error = " + error);
        if (isResume) {
            toast(error);
        }

    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        CoreManager.getCore(IAppInfoCore.class).checkBanned(true);
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        //这个接口是覆盖逻辑，而不是增量逻辑。即新的调用会覆盖之前的设置 -- 所以不用考虑登陆需要删除之前的alias
        if (uid > 0) {
            JPushHelper.getInstance().onAliasAction(this, uid + "", JPushHelper.ACTION_SET_ALIAS);
        }
        getDialogManager().dismissDialog();
        finish();
    }

    /**
     * 网易图形验证码初始化
     */
    private void initImgCheck() {
        //初始化验证码SDK相关参数，设置CaptchaId、Listener最后调用start初始化。
        if (mCaptcha == null) {
            mCaptcha = new Captcha(this);
        }
        mCaptcha.setCaptchaId(AppKey.getCaptchaId());
        mCaptcha.setCaListener(myCaptchaListener);
        //可选：开启debug
        mCaptcha.setDebug(false);
        //可选：设置超时时间
        mCaptcha.setTimeout(10000);
    }

    /**
     * 10分钟内操作是否是要验证的
     *
     * @return
     */
    private boolean cleckLoginTimeNeedCaptcha() {
        long l = (Long) SpUtils.get(this, SpEvent.cleckLoginTime, 0L);
        long currentTimeMillis = System.currentTimeMillis();
        int i = 600000;
        if (BasicConfig.isDebug) {
            i = 60000;
        }
        if (currentTimeMillis - l > i) {
            SpUtils.put(this, SpEvent.cleckLoginTime, currentTimeMillis);
            return false;
        }
        return true;
    }

    @OnClick({R.id.iv_phone, R.id.iv_qq, R.id.iv_wechat, R.id.tv_tips})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_qq:
            case R.id.iv_wechat:
                mNextSetupViewId = view.getId();
                if (captchaIsShowing) {
                    return;
                }
                if (cleckLoginTimeNeedCaptcha()) {
                    captchaIsShowing = true;
                    mCaptcha.start();
                    //简单测试是否填写captcha 是否初始化，包括captchaId与设置监听对象
                    if (mCaptcha.checkParams()) {
                        if (this == null || this.isDestroyed() || this.isFinishing()) {
                            return;
                        }
                        mCaptcha.Validate();
                    } else {
                        nextSetup(mNextSetupViewId);
                    }
                } else {
                    nextSetup(mNextSetupViewId);
                }
                break;
            case R.id.iv_phone:
                nextSetup(R.id.iv_phone);
                break;

            case R.id.tv_tips:
                Intent intent = new Intent(this, CommonWebViewActivity.class);
                String url = UriProvider.JAVA_WEB_URL + "/front/agreement/index.html";
                intent.putExtra("url", url);
                startActivity(intent);
                break;
        }
    }

    private void nextSetup(int viewId) {
        switch (viewId) {
            case R.id.iv_phone:
                //手机登录
                VerificationLoginActivity.start(this, validateStr);
                break;
            case R.id.iv_qq:
                //qq登录
                getDialogManager().showProgressDialog(this, "请稍候");
                CoreManager.getCore(IAuthCore.class).qqLogin(validateStr);
                break;
            case R.id.iv_wechat:
                //微信登录
                getDialogManager().showProgressDialog(this, "请稍候");
                CoreManager.getCore(IAuthCore.class).wxLogin(validateStr);
                break;
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 网易图形验证码操作回调
     */
    CaptchaListener myCaptchaListener = new CaptchaListener() {

        @Override
        public void onValidate(String result, String validate, String message) {
            captchaIsShowing = false;
            //验证结果，valiadte，可以根据返回的三个值进行用户自定义二次验证
            //验证成功
            if (validate.length() > 0) {
                validateStr = validate;
                nextSetup(mNextSetupViewId);
            } else {
                SingleToastUtil.showToast(MainLoginActivity.this, "验证失败：" + message);
            }
        }

        @Override
        public void closeWindow() {
            //请求关闭页面
            captchaIsShowing = false;
        }

        @Override
        public void onError(String errormsg) {
            //出错
            captchaIsShowing = false;
        }

        @Override
        public void onCancel() {
            captchaIsShowing = false;
        }

        @Override
        public void onReady(boolean ret) {
            //该为调试接口，ret为true表示加载Sdk完成
        }

    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCaptcha != null) {
            myCaptchaListener = null;
            mCaptcha.setCaListener(null);
        }
    }

}
