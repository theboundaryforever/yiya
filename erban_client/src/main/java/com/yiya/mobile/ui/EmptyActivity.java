package com.yiya.mobile.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import com.yiya.mobile.ChatApplicationLike;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.launch.activity.SplashActivity;
import com.yiya.mobile.ui.launch.presenter.EmptyPresenter;
import com.juxiao.library_utils.SystemUtil;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.Stack;

/**
 * 空的activity，做跳转
 */
public class EmptyActivity extends BaseMvpActivity<IMvpBaseView, EmptyPresenter> implements IMvpBaseView {

    public static void startAvRoom(Context context, long roomUid, int type) {
        Intent intent = new Intent(context, EmptyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.TYPE, 1);
        intent.putExtra(Constants.ROOM_UID, roomUid);
        intent.putExtra(Constants.ROOM_TYPE, type);
        context.startActivity(intent);
    }


    public static void startHome(Context context) {
        Intent intent = new Intent(context, EmptyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.TYPE, 2);
        context.startActivity(intent);
    }

    private int mType = 0;
    private int mRoomType = 3;
    private long mRoomUid = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_transparent);
        View contentView = findViewById(R.id.content_layout);
        contentView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                finish();
                return false;
            }
        });
        Intent intent = getIntent();
        mType = intent.getIntExtra(Constants.TYPE, 0);
        mRoomType = getIntent().getIntExtra(Constants.ROOM_TYPE, 3);
        mRoomUid = getIntent().getLongExtra(Constants.ROOM_UID, 0);

        //兼容oppo
        oppoDispose();
        //兼容华为
        huaweiDispose();

        //如果在应用内直接跑下面
        int appStatus = SystemUtil.getAppAliveStatus(this, getPackageName());
        LogUtil.i("EmptyActivity", "appStatus: " + appStatus + "\t" + CoreManager.getCore(IAuthCore.class).isLogin());
        switch (appStatus) {
            case 0://应用未启动
                startActivity(new Intent(this, SplashActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            case 2://应用在后台
            case 1://应用在前台
                Stack<Activity> activities = ChatApplicationLike.getApplicationLike().getActivityStack();
                boolean isMainInTask = false;
                for (Activity activity : activities) {
                    if (activity instanceof MainActivity) {
                        isMainInTask = true;
                        break;
                    }
                }
                if (CoreManager.getCore(IAuthCore.class).isLogin() && isMainInTask) {
                    if (mType == 1) {
                        RoomFrameActivity.start(this, mRoomUid, mRoomType);
                    } else {
                        MainActivity.start(this);
                    }
                } else {//未登录，先去登录
                    startActivity(new Intent(this, SplashActivity.class));
                }
                break;
        }

    }

    /**
     * {
     * "rom_type":4,
     * "n_content":"ios看到消息",
     * "n_extras":{
     * "apnsProduction":"ture",
     * "roomUid":"2915",
     * "roomType":"6"
     * },
     * "n_title":"Android看到的消息",
     * "msg_id":38280607204946233
     * }
     */
    private void oppoDispose() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String tmpJsonStr = bundle.getString("JMessageExtra");
            LogUtil.i("EmptyActivity", "JMessageExtra: " + tmpJsonStr);
            try {
                Json tmpJson = new Json(tmpJsonStr);
                Json n_extrasJson = tmpJson.json("n_extras");
                mRoomType = n_extrasJson.num("roomType");
                mRoomUid = n_extrasJson.num("roomUid");
                mType = 1;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * {"n_extras":{"apnsProduction":"ture","roomType":"6","roomUid":"2915"},"msg_id":9007208129581509,"rom_type":2,"n_content":"ios看到消息","n_title":"Android看到的消息"}
     */
    private void huaweiDispose() {
        try {
            LogUtil.i("EmptyActivity", "huaweiDispose: " + getIntent().getData());
            Json json = new Json(getIntent().getData().toString());
            Json n_extrasJson = json.json("n_extras");
            mRoomType = n_extrasJson.num("roomType");
            mRoomUid = n_extrasJson.num("roomUid");
            mType = 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected boolean needSteepStateBar() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        finish();
    }
}
