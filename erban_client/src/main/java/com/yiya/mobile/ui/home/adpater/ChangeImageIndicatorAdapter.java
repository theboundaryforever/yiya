package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.yiya.mobile.ui.widget.magicindicator.buildins.ArgbEvaluatorHolder;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> 公共多个滑动tab样式 </p>
 *
 * @author Administrator
 * @date 2017/11/15
 */
public class ChangeImageIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;

    private int normalColorId = R.color.color_AEACAF;
    private int selectColorId = R.color.color_8C63F6;
    private boolean isWrap = false;

    private List<CommonPagerTitleView> mCommonPagerTitleViewList;

    public ChangeImageIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mContext = context;
        mTitleList = titleList;
        mCommonPagerTitleViewList = new ArrayList<>();
        mCommonPagerTitleViewList.clear();
    }

    private int size = 16;

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    public void setNormalColorId(@ColorRes int normalColorId) {
        this.normalColorId = normalColorId;
    }

    public void setSelectColorId(@ColorRes int selectColorId) {
        this.selectColorId = selectColorId;
    }

    @Override
    public IPagerTitleView getTitleView(Context context, final int i) {
        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.ly_indicator_home);
//        final ImageView indicator = pagerTitleView.findViewById(R.id.indicator);
//        indicator.setImageResource(ImageResUtils.getLocalImg(false,mTitleList.get(i).getId()));
//        final TextView indicatorText = pagerTitleView.findViewById(R.id.indicator1);

//        indicatorText.setText(mTitleList.get(i).getName());

        final TextView titleTv = pagerTitleView.findViewById(R.id.tv_name_ind);

        final View lineView = pagerTitleView.findViewById(R.id.view_ind);

        final View pointView = pagerTitleView.findViewById(R.id.view_point);

        titleTv.setText(mTitleList.get(i).getName());


        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {

            @Override
            public void onSelected(int index, int totalCount) {
                titleTv.getPaint().setFakeBoldText(true);
                lineView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onDeselected(int index, int totalCount) {
                titleTv.getPaint().setFakeBoldText(false);
                lineView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
                int colorId = ArgbEvaluatorHolder.eval(leavePercent, ContextCompat.getColor(mContext, R.color.color_333333)
                        , ContextCompat.getColor(mContext, R.color.color_C3C3C3));
                titleTv.setTextColor(colorId);
                titleTv.setScaleX(1.0f + (0.8f - 1.0f) * leavePercent);
                titleTv.setScaleY(1.0f + (0.8f - 1.0f) * leavePercent);

            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
                int colorId = ArgbEvaluatorHolder.eval(enterPercent, ContextCompat.getColor(mContext, R.color.color_C3C3C3)
                        , ContextCompat.getColor(mContext, R.color.color_333333));
                titleTv.setTextColor(colorId);
                titleTv.setScaleX(0.8f + (1.0f - 0.8f) * enterPercent);
                titleTv.setScaleY(0.8f + (1.0f - 0.8f) * enterPercent);
            }
        });
        pagerTitleView.setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }
        });
        mCommonPagerTitleViewList.add(pagerTitleView);
        return pagerTitleView;
    }

    public View getChildView(int index, int viewId) {
        View tmpView = null;
        if (mCommonPagerTitleViewList.size() > index) {
            tmpView = mCommonPagerTitleViewList.get(index).findViewById(viewId);
        }
        return tmpView;
    }


    @Override
    public IPagerIndicator getIndicator(Context context) {
        return null;
    }

    private OnItemSelectListener mOnItemSelectListener;

    public void setOnItemSelectListener(OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}