package com.yiya.mobile.ui.rank.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.adapter.BaseIndicatorAdapter;
import com.yiya.mobile.base.fragment.BaseLazyFragment;
import com.tongdaxing.erban.R;


import java.util.ArrayList;
import java.util.List;

import static com.yiya.mobile.ui.rank.activity.RankingChatListActivity.BUNDLE_KEY_ROOM_ID;
import static com.yiya.mobile.ui.rank.activity.RankingChatListActivity.BUNDLE_KEY_TYPE;
import static com.yiya.mobile.ui.rank.activity.RankingChatListActivity.BUNDLE_KEY_UID;

/**
 * @author Zhangsongzhou
 * @date 2019/4/8
 */
public class RankingChatListFragment extends BaseLazyFragment implements ViewPager.OnPageChangeListener {

    private TextView mTopDayTv, mTopWeekTv, mTopAllTv;
    private ImageView mTopDayIv, mTopWeekIv, mTopAllIv;

    private List<TextView> mTopTvList;
    private List<ImageView> mTopIvList;

    private ViewPager mViewPager;
    private BaseIndicatorAdapter mAdapter;
    private List<Fragment> mFragmentList;

    private int mType = 0;
    private String mRoomId;
    private String mUid;


    @Override
    protected void onLazyLoadData() {

    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_ranking_chat_list;
    }

    @Override
    public void onFindViews() {
        mFragmentList = new ArrayList<>();
        mTopTvList = new ArrayList<>();
        mTopIvList = new ArrayList<>();

        mTopDayTv = mView.findViewById(R.id.top_day_tv);
        mTopDayIv = mView.findViewById(R.id.top_day_iv);

        mTopWeekTv = mView.findViewById(R.id.top_week_tv);
        mTopWeekIv = mView.findViewById(R.id.top_week_iv);

        mTopAllTv = mView.findViewById(R.id.top_all_tv);
        mTopAllIv = mView.findViewById(R.id.top_all_iv);

        mTopDayTv.setOnClickListener(this);
        mTopWeekTv.setOnClickListener(this);
        mTopAllTv.setOnClickListener(this);


        mTopIvList.add(mTopDayIv);
        mTopIvList.add(mTopWeekIv);
        mTopIvList.add(mTopAllIv);

        mTopTvList.add(mTopDayTv);
        mTopTvList.add(mTopWeekTv);
        mTopTvList.add(mTopAllTv);

//        String uid = CoreManager.getCore(IAuthCore.class).getCurrentUid() + "";
        mFragmentList.add(RankingChatItemFragment.newInstance(mType, 1, mUid, mRoomId));
        mFragmentList.add(RankingChatItemFragment.newInstance(mType, 2, mUid, mRoomId));
        mFragmentList.add(RankingChatItemFragment.newInstance(mType, 3, mUid, mRoomId));
        mViewPager = mView.findViewById(R.id.viewpager);


        mAdapter = new BaseIndicatorAdapter(getChildFragmentManager(), mFragmentList);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setOffscreenPageLimit(3);

        selectPager(0);
    }


    private void selectPager(int pos) {
        for (int i = 0; i < mTopIvList.size(); i++) {
            if (pos == i) {
                mTopIvList.get(i).setVisibility(View.VISIBLE);
            } else {
                mTopIvList.get(i).setVisibility(View.INVISIBLE);
            }
        }

        mViewPager.setCurrentItem(pos, true);
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        int valueId = -1;
        switch (view.getId()) {
            case R.id.top_day_tv:
                valueId = 0;
                break;
            case R.id.top_week_tv:
                valueId = 1;
                break;
            case R.id.top_all_tv:
                valueId = 2;
                break;

        }

        if (valueId != -1) {
            selectPager(valueId);
        }

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {

    }

    public static Fragment newInstance(int type, String uid, String roomId) {
        Bundle bundle = new Bundle();
        RankingChatListFragment fragment = new RankingChatListFragment();
        bundle.putInt(BUNDLE_KEY_TYPE, type);
        bundle.putString(BUNDLE_KEY_UID, uid);
        bundle.putString(BUNDLE_KEY_ROOM_ID, roomId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (bundle != null) {
            mType = bundle.getInt(BUNDLE_KEY_TYPE);
            mRoomId = bundle.getString(BUNDLE_KEY_ROOM_ID);
            mUid = bundle.getString(BUNDLE_KEY_UID);
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        selectPager(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
