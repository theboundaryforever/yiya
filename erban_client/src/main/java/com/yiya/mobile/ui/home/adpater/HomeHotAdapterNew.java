package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.ui.widget.Banner;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

/**
 * <p> 首页热门adapter </p>
 *
 * @author Administrator
 * @date 2017/11/16
 */
public class HomeHotAdapterNew extends BaseMultiItemQuickAdapter<HomeRoom, BaseViewHolder> {
    private int itemWidth = 0;
    private int radius = 20;


    public HomeHotAdapterNew(Context context) {
        super(null);
        this.mContext = context;
        addItemType(0, R.layout.item_home_hot_room);
        addItemType(1, R.layout.item_room_hot_list_banner);
        itemWidth = (DisplayUtils.getScreenWidth(context) - DisplayUtils.dip2px(context,45))/2;
        radius = DisplayUtils.dip2px(context,10);
    }

    @Override
    protected void convert(BaseViewHolder holder, HomeRoom homeItem) {
        if (homeItem == null) {
            return;
        }
        switch (homeItem.getItemType()) {
            case 0:
                setNormalData(holder, homeItem);
            break;
            case 1:
                setListBanner(holder, homeItem);
                break;
        }
    }

    private void setNormalData(BaseViewHolder holder, HomeRoom homeItem) {
        RelativeLayout rlContent = holder.getView(R.id.rl_hot_content);
        ViewGroup.LayoutParams vp = rlContent.getLayoutParams();
        vp.height = itemWidth;
        rlContent.setLayoutParams(vp);
        TextView title = holder.getView(R.id.tv_hot_room_title);
        title.setText(homeItem.getTitle());
        title.setSelected(true);
        holder.setText(R.id.tv_hot_room_nick,homeItem.getNick());
        holder.setText(R.id.tv_home_hot_people,homeItem.getOnlineNum()+"人");
        ImageLoadUtils.loadGifImage(mContext,R.drawable.ic_home_room_move,holder.getView(R.id.iv_home_room_list_move));
        ImageLoadUtils.loadSmallRoundBackground(mContext,homeItem.getAvatar(),holder.getView(R.id.iv_hot_room_logo),radius);
        ImageLoadUtils.loadImage(mContext,homeItem.tagPict,holder.getView(R.id.iv_home_tag));
        ImageView ivBage = holder.getView(R.id.iv_home_badge);
        if (StringUtils.isNotEmpty(homeItem.badge)) {
            ivBage.setVisibility(View.VISIBLE);
            ImageLoadUtils.loadImage(mContext, homeItem.badge, ivBage);
        }else {
            ivBage.setVisibility(View.VISIBLE);
        }
    }

    private void setListBanner(BaseViewHolder holder, HomeRoom homeItem) {
        Banner banner = holder.getView(R.id.iv_room_hot_list_banner);
        BannerAdapter bannerAdapter = new BannerAdapter(homeItem.bannerInfoList, mContext);
//        bannerAdapter.notifyDataSetChanged();
        banner.setAdapter(bannerAdapter);
//        ImageLoadUtils.loadSmallRoundBackground(application,homeItem.getBackPicUrl(),holder.getView(R.id.iv_room_hot_list_banner),radius);
    }

    //变换底部占满 footview
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
        if (manager instanceof GridLayoutManager) {
            final GridLayoutManager g = (GridLayoutManager) manager;
            g.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return 1 == getItemViewType(position) ? 2 : 1;
                }
            });
        }
    }
}
