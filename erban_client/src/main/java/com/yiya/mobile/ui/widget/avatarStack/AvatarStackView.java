package com.yiya.mobile.ui.widget.avatarStack;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.tongdaxing.erban.R;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/6/28.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class AvatarStackView extends FrameLayout {

    private AvatarStackAdapter adapter;
    private RecyclerView rvAvatar;

    public AvatarStackView(Context context) {
        this(context, null);
    }

    public AvatarStackView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AvatarStackView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        inflate(context, R.layout.layout_avatar_stack, this);
        rvAvatar = findViewById(R.id.rv_avatar);
        rvAvatar.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvAvatar.setOnTouchListener((v, event) -> {
            if (getParent() instanceof ViewGroup) {
                return ((ViewGroup) getParent()).onTouchEvent(event);
            }
            return false;
        });
    }

    public void setAdapter(AvatarStackAdapter adapter) {
        this.adapter = adapter;
    }

    public void setAvatarList(List<String> avatarList) {
        rvAvatar.setAdapter(adapter);
        adapter.setNewData(avatarList);
    }


}
