package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment;
import com.yiya.mobile.ui.widget.Banner;
import com.yiya.mobile.ui.widget.LiveItemView;
import com.yiya.mobile.ui.widget.LiveTabView;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.juxiao.library_utils.log.LogUtil;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.HomeLiveInfo;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/22
 */
public class LiveStreamRecyclerAdapter extends BaseMultiItemQuickAdapter<HomeLiveStreamFragment.LiveStreamInfo, BaseViewHolder> {


    public interface OnItemClickListener {
        void onItemClick(int itemType, View view, int position, LiveTabView.TabInfo tabInfo);
    }

    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
    }


    private Context mContext;

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public LiveStreamRecyclerAdapter(List<HomeLiveStreamFragment.LiveStreamInfo> data) {
        super(data);
        addItemType(HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_BANNER, R.layout.item_home_live_master_banner);
        addItemType(HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_RANK, R.layout.item_home_live_rank);
        addItemType(HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_HOT_TAG, R.layout.item_home_live_room_hot);
        addItemType(HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_HOT, R.layout.item_home_live_room);
        addItemType(HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_TAG, R.layout.item_home_live_room_tag);
        addItemType(HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_CHAT, R.layout.item_home_live_room);

    }

    private int mItemRoomWidth = 0;

    public LiveStreamRecyclerAdapter(Context context, List<HomeLiveStreamFragment.LiveStreamInfo> data) {
        this(data);
        mContext = context;
        mItemRoomWidth = (DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 30) - DisplayUtils.dip2px(mContext, 10)) / 2;
    }

    public void setData(List<HomeLiveStreamFragment.LiveStreamInfo> data) {
        setNewData(data);
    }


    @Override
    protected void convert(BaseViewHolder helper, HomeLiveStreamFragment.LiveStreamInfo info) {
        switch (info.getItemType()) {
            case HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_BANNER: //广告
                handleItemBanner(helper, info);
                break;
            case HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_RANK:    //榜单
                handleItemRank(helper, info);
                break;
            case HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_HOT_TAG:    //
                break;
            case HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_HOT:    //推荐房间
                handleItemHotRoom(helper, info);
                break;
            case HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_TAG:    //房间标签
                /*handleItemRoomTag(helper, info);*/
                break;
            case HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_CHAT:   //底部房间
                handleItemRoomChat(helper, info);
                break;
        }
    }

    /**
     * 广告页
     *
     * @param helper
     * @param info
     */
    private void handleItemBanner(BaseViewHolder helper, HomeLiveStreamFragment.LiveStreamInfo info) {
        Banner banner = helper.getView(R.id.item_banner);
        BannerAdapter bannerAdapter = new BannerAdapter(info.getBannerInfoList(), mContext);
        banner.setAdapter(bannerAdapter);
        banner.setPlayDelay(3 * 1000);
    }


    /**
     * 榜单
     *
     * @param helper
     * @param info
     */
    private void handleItemRank(BaseViewHolder helper, HomeLiveStreamFragment.LiveStreamInfo info) {
        helper.getView(R.id.item_bg_fl).findViewById(R.id.iv_ranking).setOnClickListener(v -> {
            if (mItemClickListener != null) {  //排行榜
                mItemClickListener.onItemClick(info.getItemType(), null, 1, null);
            }
        });
        helper.getView(R.id.item_bg_fl).findViewById(R.id.iv_luckdraw).setOnClickListener(v -> {
            if (mItemClickListener != null) {  //大转盘
                mItemClickListener.onItemClick(info.getItemType(), null, 2, null);
            }
        });
        helper.getView(R.id.item_bg_fl).findViewById(R.id.iv_shoppingmall).setOnClickListener(v -> {
            if (mItemClickListener != null) {  //商城
                mItemClickListener.onItemClick(info.getItemType(), null, 3, null);
            }
        });

        /*HomeLiveStreamFragment.LiveStreamInfo.RankBean rankBean = info.getRankBean();
        ViewFlipper flipper = helper.getView(R.id.item_rank_flipper);
        int tmp = 0;
        if (!ListUtils.isListEmpty(rankBean.getWealthRankVoList())) {
            View wealthView = View.inflate(mContext, R.layout.item_home_live_rank_wealth_flipper, null);
            flipper.addView(wealthView);
            ((TextView) wealthView.findViewById(R.id.rank_name_tv)).setText("土豪榜");
            wealthView.findViewById(R.id.item_bg_fl).setOnClickListener(v -> {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(info.getItemType(), null, getData().indexOf(info), null);
                }
            });
            tmp++;
            try {
                ImageView firstIv = wealthView.findViewById(R.id.rank_first_iv);
                ImageView secondIv = wealthView.findViewById(R.id.rank_second_iv);
                ImageView thirdIv = wealthView.findViewById(R.id.rank_third_iv);
                ImageLoadUtils.loadCircleImage(mContext, rankBean.getWealthRankVoList().get(0).getAvatar(), thirdIv, R.drawable.nim_avatar_default);
                ImageLoadUtils.loadCircleImage(mContext, rankBean.getWealthRankVoList().get(1).getAvatar(), secondIv, R.drawable.nim_avatar_default);
                ImageLoadUtils.loadCircleImage(mContext, rankBean.getWealthRankVoList().get(2).getAvatar(), firstIv, R.drawable.nim_avatar_default);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!ListUtils.isListEmpty(rankBean.getCharmRankVoList())) {
            View charmView = View.inflate(mContext, R.layout.item_home_live_rank_charm_flipper, null);
            flipper.addView(charmView);
            ((TextView) charmView.findViewById(R.id.rank_name_tv)).setText("心动榜");
            charmView.findViewById(R.id.item_bg_fl).setOnClickListener(v -> {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(info.getItemType(), null, getData().indexOf(info), null);
                }
            });
            tmp++;
            try {
                ImageView firstIv = charmView.findViewById(R.id.rank_first_iv);
                ImageView secondIv = charmView.findViewById(R.id.rank_second_iv);
                ImageView thirdIv = charmView.findViewById(R.id.rank_third_iv);
                ImageLoadUtils.loadCircleImage(mContext, rankBean.getCharmRankVoList().get(0).getAvatar(), thirdIv, R.drawable.nim_avatar_default);
                ImageLoadUtils.loadCircleImage(mContext, rankBean.getCharmRankVoList().get(1).getAvatar(), secondIv, R.drawable.nim_avatar_default);
                ImageLoadUtils.loadCircleImage(mContext, rankBean.getCharmRankVoList().get(2).getAvatar(), firstIv, R.drawable.nim_avatar_default);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tmp > 1) {
//            flipper.setOnTouchListener(new FlipperTouch(flipper));
            flipper.startFlipping();
        }*/
    }


    /**
     * 房间推荐
     *
     * @param helper
     * @param info
     */
    private void handleItemHotRoom(BaseViewHolder helper, HomeLiveStreamFragment.LiveStreamInfo info) {
        ViewGroup.LayoutParams bgParams = helper.getView(R.id.item_bg_fl).getLayoutParams();
        bgParams.height = mItemRoomWidth;
        bgParams.width = mItemRoomWidth;
        helper.getView(R.id.item_bg_fl).setLayoutParams(bgParams);
        LiveItemView liveItemView = helper.getView(R.id.item_live_item_view);
        ViewGroup.LayoutParams layoutParams = liveItemView.getLayoutParams();
        layoutParams.width = mItemRoomWidth;
        layoutParams.height = mItemRoomWidth;
        liveItemView.setLayoutParams(layoutParams);

        ImageLoadUtils.loadImage(mContext, info.getHomeHotRoomList().getAvatar(), liveItemView.getBgView());
        //ImageLoadUtils.loadImage(mContext, info.getHomeYuChatListInfo().getTagPict(), liveItemView.getTagIv());
        ImageLoadUtils.loadImage(mContext, info.getHomeHotRoomList().getTagPict(), liveItemView.getTopTagIv());
        liveItemView.setCountNum(info.getHomeHotRoomList().getOnlineNum());
        liveItemView.setNameStr((info.getHomeHotRoomList().getTitle()));
        liveItemView.setLocalStr(info.getHomeHotRoomList().getCity());

        //声浪
        SVGAImageView svgaWave = liveItemView.getSvga();
        if (svgaWave.getDrawable() != null) {
            if (!svgaWave.isAnimating()) {
                svgaWave.startAnimation();
            }
        } else {
            SVGAParser svgaParser = new SVGAParser(mContext);
            svgaParser.decodeFromAssets("audio.svga", new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                    svgaWave.setVideoItem(svgaVideoEntity);
                    if (!svgaWave.isAnimating()) {
                        svgaWave.startAnimation();
                    }
                }

                @Override
                public void onError() {
                    LogUtil.e("svgaWave onError");
                }
            });
        }

        helper.getView(R.id.item_bg_fl).setOnClickListener(v -> {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(info.getItemType(), null, getData().indexOf(info), null);
            }
        });
    }

    /**
     * 房间标签页
     *
     * @param helper
     * @param info
     */
    private void handleItemRoomTag(BaseViewHolder helper, HomeLiveStreamFragment.LiveStreamInfo info) {
       /* LiveTabView liveTabView = helper.getView(R.id.item_live_tab_view);
        List<LiveTabView.TabInfo> tmpData = new ArrayList<>();
        for (HomeLiveInfo.VideoRoomTagListBean bean : info.getVideoRoomTagList()) {
            tmpData.add(new LiveTabView.TabInfo().setTitle(bean.getName()).setAvaUrl(bean.getPict()).setId(bean.getId()));
        }
        liveTabView.setOnTabClickListener(new LiveTabView.OnTabClickListener() {
            @Override
            public void onTabClick(LiveTabView.TabInfo tabInfo) {
                if (mItemClickListener != null) {
                    mItemClickListener.onItemClick(info.getItemType(), null, getData().indexOf(info), tabInfo);
                }
            }
        });
        liveTabView.setData(tmpData);*/
    }

    /**
     * 底部房间item样式
     *
     * @param helper
     * @param info
     */
    private void handleItemRoomChat(BaseViewHolder helper, HomeLiveStreamFragment.LiveStreamInfo info) {
        ViewGroup.LayoutParams bgParams = helper.getView(R.id.item_bg_fl).getLayoutParams();
        bgParams.height = mItemRoomWidth;
        bgParams.width = mItemRoomWidth;
        helper.getView(R.id.item_bg_fl).setLayoutParams(bgParams);
        LiveItemView liveItemView = helper.getView(R.id.item_live_item_view);
        ViewGroup.LayoutParams layoutParams = liveItemView.getLayoutParams();
        layoutParams.width = mItemRoomWidth;
        layoutParams.height = mItemRoomWidth;
        liveItemView.setLayoutParams(layoutParams);

        ImageLoadUtils.loadImage(mContext, info.getHomeYuChatListInfo().getAvatar(), liveItemView.getBgView());
        //ImageLoadUtils.loadImage(mContext, info.getHomeYuChatListInfo().getTagPict(), liveItemView.getTagIv());
        ImageLoadUtils.loadImage(mContext, info.getHomeYuChatListInfo().getTagPict(), liveItemView.getTopTagIv());
        liveItemView.setCountNum(info.getHomeYuChatListInfo().getOnlineNum());
        liveItemView.setNameStr((!TextUtils.isEmpty(info.getHomeYuChatListInfo().getTitle()) ? info.getHomeYuChatListInfo().getTitle() : info.getHomeYuChatListInfo().getNick() ));
        liveItemView.setLocalStr(info.getHomeYuChatListInfo().getCity());

        //声浪
        SVGAImageView svgaWave = liveItemView.getSvga();
        if (svgaWave.getDrawable() != null) {
            if (!svgaWave.isAnimating()) {
                svgaWave.startAnimation();
            }
        } else {
            SVGAParser svgaParser = new SVGAParser(mContext);
            svgaParser.decodeFromAssets("audio.svga", new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                    svgaWave.setVideoItem(svgaVideoEntity);
                    if (!svgaWave.isAnimating()) {
                        svgaWave.startAnimation();
                    }
                }

                @Override
                public void onError() {
                    LogUtil.e("svgaWave onError");
                }
            });
        }

        helper.getView(R.id.item_bg_fl).setOnClickListener(v -> {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(info.getItemType(), null, getData().indexOf(info), null);
            }
        });
    }
}
