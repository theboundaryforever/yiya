package com.yiya.mobile.ui.home.adpater;

import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.mission.Missions;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/29.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class SignInMissionAdapter extends BaseQuickAdapter<Missions.CheckInMissions, BaseViewHolder> {

    public SignInMissionAdapter(@Nullable List<Missions.CheckInMissions> data) {
        super(R.layout.item_sign_in_mission, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Missions.CheckInMissions item) {
        helper.setText(R.id.tv_num, "+" + item.getMcoinAmount())
                .setTextColor(R.id.tv_num, ContextCompat.getColor(mContext, item.isSignIn() ? R.color.color_FF495C : R.color.color_FF495C_20))
                .setGone(R.id.iv_tick, item.isSignIn())
                .setGone(R.id.tv_day, !item.isSignIn())
                .setText(R.id.tv_day, item.getMissionName());
        ImageLoadUtils.loadImage(mContext, item.getPicUrl(), helper.getView(R.id.iv_pic));
    }
}
