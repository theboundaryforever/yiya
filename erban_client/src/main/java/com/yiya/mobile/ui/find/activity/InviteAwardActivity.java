package com.yiya.mobile.ui.find.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.ui.me.wallet.activity.BinderPhoneActivity;
import com.yiya.mobile.ui.me.withdraw.RedPacketWithdrawActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.ui.widget.dialog.ShareDialog;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.databinding.FragmentInviteAwardBinding;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.redpacket.bean.RedDrawListInfo;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;
import com.tongdaxing.xchat_core.redpacket.bean.WebViewInfo;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.sharesdk.framework.Platform;

/**
 * @author dell
 */
public class InviteAwardActivity extends BaseActivity implements ShareDialog.OnShareDialogItemClick,
        View.OnTouchListener {

    private FragmentInviteAwardBinding redbagBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        redbagBinding = DataBindingUtil.setContentView(this, R.layout.fragment_invite_award);
        initView();
        initData();
    }

    private void initView() {
        redbagBinding.setClick(this);
        redbagBinding.awardRule.setOnTouchListener(this);
        redbagBinding.toolbar.setOnBackBtnListener(view -> finish());
        redbagBinding.toolbar.setOnRightBtnClickListener(view -> {
            ShareDialog shareDialog = new ShareDialog(this);
//            shareDialog.setOnShareDialogItemClick(this);
            shareDialog.show();
        });
    }

    private void initData() {
        Map<String,String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().getRequest(UriProvider.getRedPacket(), param, new OkHttpManager.MyCallBack<ServiceResult<RedPacketInfo>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<RedPacketInfo> data) {
                if (null != data && data.isSuccess()) {
                    if (data.getData() != null) {
                        redbagBinding.setRedPacketInfo(data.getData());
                    }
                }
            }
        });

        Map<String,String> param2 = CommonParamUtil.getDefaultParam();
        OkHttpManager.getInstance().getRequest(UriProvider.getRedDrawList(), param2, new OkHttpManager.MyCallBack<ServiceResult<List<RedDrawListInfo>>>() {
            @Override
            public void onError(Exception e) {
                showDefaultData();
            }

            @Override
            public void onResponse(ServiceResult<List<RedDrawListInfo>> data) {
                if (null != data && data.isSuccess()) {
                    onGetRedDrawList(data.getData());
                } else {
                    showDefaultData();
                }
            }
        });
    }

    public void onGetRedDrawList(List<RedDrawListInfo> redDrawListInfos) {
        if (redDrawListInfos != null && redDrawListInfos.size() > 0) {
            List<String> info = new ArrayList<>();
            for (int i = 0; i < redDrawListInfos.size(); i++) {
                String content = redDrawListInfos.get(i).getNick() + "刚刚提现了" + redDrawListInfos.get(i).getPacketNum();
                info.add(content);
            }
            redbagBinding.marqueeView.startWithList(info);
        } else {
            showDefaultData();
        }
    }

    private void showDefaultData() {
        List<String> info = new ArrayList<>();
        info.add("article一分钟前提现了200");
        info.add("木马一分钟前提现了200");
        info.add("此乳胸险一分钟前提现了100");
        info.add("xiaoshihou一分钟前提现了100");
        info.add("夜袭寡妇一分钟前提现了200");
        info.add("玮哥一分钟前提现了200");
        info.add("xiaoSeSe一分钟前提现了400");
        info.add("一生懵逼一分钟前提现了600");
        info.add("小可爱一分钟前提现了100");
        info.add("ai人一分钟前提现了200");
        info.add("大叔一分钟前提现了400");
        info.add("小凳子一分钟前提现了800");
        info.add("小逍遥一分钟前提现了300");
        info.add("清新一分钟前提现了100");
        info.add("荔枝一分钟前提现了200");
        info.add("小明哥哥一分钟前提现了100");
        info.add("薇薇一分钟前提现了300");
        info.add("红高粱一分钟前提现了400");
        redbagBinding.marqueeView.startWithList(info);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        String url = null;
        switch (view.getId()) {
            //邀请奖励
            //case R.id.rly_red_rank:
            //    url = BaseUrl.INVITE_REWARD_RANK + "?uid=" + CoreManager.getCore(IAuthCore.class).getCurrentUid();
            //    break;
            //邀请大作战
            case R.id.rly_red_rule:
                redbagBinding.awardRule.setVisibility(View.VISIBLE);
                break;
            //邀请人数
            case R.id.rly_people:

                break;
            //邀请分成
            case R.id.rly_share_bonus:
                url = BaseUrl.MY_INVITE_PERCENTAGE + "?uid=" + CoreManager.getCore(IAuthCore.class).getCurrentUid();
                break;
            case R.id.img_withdraw:
                CoreManager.getCore(IAuthCore.class).isPhone(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                break;
            default:
                break;
        }
        if (TextUtils.isEmpty(url)) {
            return;
        }
        openWebView(url);
    }

    private void openWebView(String url) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        Intent intent = new Intent(this, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onIsphoneFail(String error) {
        startActivity(new Intent(this, BinderPhoneActivity.class));
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onIsPhone() {
        startActivity(new Intent(this, RedPacketWithdrawActivity.class));
    }

    @Override
    public void onSharePlatformClick(Platform platform) {
        WebViewInfo webViewInfo = new WebViewInfo();
        webViewInfo.setTitle(getString(R.string.share_h5_title));
        webViewInfo.setImgUrl(BaseUrl.SHARE_DEFAULT_LOGO);
        webViewInfo.setDesc(getString(R.string.share_h5_desc));
        webViewInfo.setShowUrl(BaseUrl.SHARE_DOWNLOAD);
        CoreManager.getCore(IShareCore.class).shareH5(webViewInfo, platform);

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            redbagBinding.awardRule.setVisibility(View.GONE);
            return true;
        }
        return false;
    }
}
