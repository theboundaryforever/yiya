package com.yiya.mobile.ui.me.mywallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.tongdaxing.erban.libcommon.listener.SingleClickListener;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.me.mywallet.adapter.WalletRecyclerViewAdapter;
import com.yiya.mobile.ui.me.user.activity.InviteFriendActivity;
import com.yiya.mobile.ui.me.wallet.activity.BinderPhoneActivity;
import com.yiya.mobile.ui.me.wallet.activity.ExchangeGoldActivity;
import com.yiya.mobile.ui.me.wallet.activity.MyWalletActivity;
import com.yiya.mobile.ui.me.withdraw.WithdrawActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.me.mywallet.adapter.WalletRecyclerViewAdapter;
import com.yiya.mobile.ui.me.wallet.activity.BinderPhoneActivity;
import com.yiya.mobile.ui.me.wallet.activity.ExchangeGoldActivity;
import com.yiya.mobile.ui.me.wallet.activity.MyWalletActivity;
import com.yiya.mobile.ui.me.withdraw.WithdrawActivity;
import com.yiya.mobile.utils.UIHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.yiya.mobile.ui.me.mywallet.activity.WalletActivity.WalletInfo.ITEM_TYPE_1;
import static com.yiya.mobile.ui.me.mywallet.activity.WalletActivity.WalletInfo.ITEM_TYPE_2;
import static com.yiya.mobile.ui.me.mywallet.activity.WalletActivity.WalletInfo.ITEM_TYPE_3;

public class WalletActivity extends BaseActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, WalletActivity.class));
    }

    RecyclerView recyclerView;
    private WalletRecyclerViewAdapter walletRecyclerViewAdapter;
    private String bonus;
    private List<WalletInfo> walletInfoList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_layout);
        initdata();
    }

    private void initdata(){
        recyclerView = (RecyclerView)this.findViewById(R.id.wallet_recycler);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        walletRecyclerViewAdapter = new WalletRecyclerViewAdapter(null,this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(walletRecyclerViewAdapter);
        WalletInfo walletInfo1 = new WalletInfo();
        walletInfo1.setItemType(ITEM_TYPE_1);
        WalletInfo walletInfo2 = new WalletInfo();
        walletInfo2.setItemType(ITEM_TYPE_2);
        walletInfoList.add(walletInfo1);
        walletInfoList.add(walletInfo2);
        walletRecyclerViewAdapter.setData(walletInfoList);
        walletRecyclerViewAdapter.setOnItemClickListener((itemType, view, position, tabInfo) -> {
            switch(itemType){
                case ITEM_TYPE_1:
                    //跳转到金币充值页面
                    MyWalletActivity.start(this,1);
                    break;
                case ITEM_TYPE_2:
                    if(position==1){  //提现
                        hasBindPhone();
                    }else if(position==2){  //兑换
                        startActivity(new Intent(this, ExchangeGoldActivity.class));
                    }else{
                        MyWalletActivity.start(this,2);
                    }
                    break;
                case ITEM_TYPE_3:
                    //跳转到奖励金提现页面
                    UIHelper.showInvitationH5(this);
                    break;
            }
        });
        getwallet();
    }

    public static class WalletInfo implements MultiItemEntity {

        public static final int ITEM_TYPE_1 = 1;  //第一列金币
        public static final int ITEM_TYPE_2 = 2;  //第二列砖石
        public static final int ITEM_TYPE_3 = 3;  //第三列红包余额

        private int itemType;
        private int roomIndex = 0;

        private List<String> mList1;
        private List<String> mList2;
        private List<String> mList3;

        public void setItemType(int itemType) {
            this.itemType = itemType;
        }

        public int getRoomIndex() {
            return roomIndex;
        }

        public void setRoomIndex(int roomIndex) {
            this.roomIndex = roomIndex;
        }

        public List<String> getmList1() {
            return mList1;
        }

        public void setmList1(List<String> mList1) {
            this.mList1 = mList1;
        }

        public List<String> getmList2() {
            return mList2;
        }

        public void setmList2(List<String> mList2) {
            this.mList2 = mList2;
        }

        public List<String> getmList3() {
            return mList3;
        }

        public void setmList3(List<String> mList3) {
            this.mList3 = mList3;
        }

        @Override
        public int getItemType() {
            return itemType;
        }
    }

    public void hasBindPhone() {
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().getRequest(UriProvider.isPhones(), params, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(ServiceResult<String> response) {
                if (response != null && response.isSuccess()) {
                    startActivity(new Intent(WalletActivity.this, WithdrawActivity.class));
                } else {
                    if (response != null)
                        startActivity(new Intent(WalletActivity.this, BinderPhoneActivity.class));
                }
            }
        });
    }

    private void getwallet(){
        Map<String,String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getWalletInfos(), param, new OkHttpManager.MyCallBack<ServiceResult<com.tongdaxing.xchat_core.pay.bean.WalletInfo>>() {
            @Override
            public void onError(Exception e) {
                LogUtils.e(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<com.tongdaxing.xchat_core.pay.bean.WalletInfo> response) {
                if(response!=null){
                    if(response.getData()!=null){
                        com.tongdaxing.xchat_core.pay.bean.WalletInfo walletInfo = response.getData();
                        Log.d("奖励金余额",""+walletInfo.getPacketNum());
                        if(walletInfo.getPacketNum()>0){
                            WalletInfo walletInfo3 = new WalletInfo();
                            walletInfo3.setItemType(ITEM_TYPE_3);
                            walletInfoList.add(walletInfo3);
                            walletRecyclerViewAdapter.setbonus(String.valueOf(walletInfo.getPacketNum()));
                        }
                    }
                }
            }
        });
    }

}
