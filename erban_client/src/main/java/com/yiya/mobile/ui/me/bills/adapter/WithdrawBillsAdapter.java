package com.yiya.mobile.ui.me.bills.adapter;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bills.bean.BillItemEntity;
import com.tongdaxing.xchat_core.bills.bean.IncomeInfo;
import com.tongdaxing.xchat_framework.util.util.TimeUtils;

import java.util.List;

/**
 * IncomeInfo
 * Created by Seven on 2017/9/17.
 */

public class WithdrawBillsAdapter extends BillBaseAdapter {

    public WithdrawBillsAdapter(List<BillItemEntity> billItemEntityList) {
        super(billItemEntityList);
        addItemType(BillItemEntity.ITEM_NORMAL, R.layout.list_withdraw_bills_item);
    }

    private int mDataType = 1;//1：钻石 2：咿呀

    public void setDataType(int type) {
        mDataType = type;
    }

    @Override
    public void convertNormal(BaseViewHolder baseViewHolder, BillItemEntity billItemEntity) {
        IncomeInfo incomeInfo = billItemEntity.mWithdrawInfo;
        if (incomeInfo == null) return;

        if (mDataType == 1) {
            baseViewHolder.setText(R.id.tv_date, TimeUtils.getDateTimeString(incomeInfo.getRecordTime(), "HH:mm:ss"))
                    .setText(R.id.tv_diamondNum, "提取" + incomeInfo.getDiamondNum() + "钻石")
                    .setText(R.id.tv_money, "+" + incomeInfo.getMoney() + "元");
        } else {
            baseViewHolder.setText(R.id.tv_date, TimeUtils.getDateTimeString(incomeInfo.getRecordTime(), "HH:mm:ss"))
                    .setText(R.id.tv_diamondNum, "" + incomeInfo.getRedbeanNum() + "咿呀")
                    .setText(R.id.tv_money, "+" + incomeInfo.getMoney() + "元");
        }

    }
}
