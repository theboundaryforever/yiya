package com.yiya.mobile.ui.me.withdraw;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.juxiao.library_ui.widget.AppToolBar;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCore;
import com.tongdaxing.xchat_core.withdraw.IWithdrawCoreClient;
import com.tongdaxing.xchat_core.withdraw.bean.ExchangerInfo;
import com.tongdaxing.xchat_core.withdraw.bean.RefreshInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrawInfo;
import com.tongdaxing.xchat_core.withdraw.bean.WithdrwaListInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.me.wallet.activity.WalletActivity;
import com.yiya.mobile.ui.me.wallet.adapter.WithdrawJewelAdapter;
import com.yiya.mobile.ui.web.CommonWebViewActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Locale;

/**
 * 钻石提现
 *
 * @author dell
 */
public class WithdrawActivity extends BaseActivity implements WithdrawWayDialog.OnWithdrawWayChangeListener {

    public static final String BUNDLE_KEY_WEIXIN_NAME = "weixin_name";
    public static final String BUNDLE_KEY_WEIXIN_OPEN_ID = "weixin_open_id";
    public static final String BUNDLE_KEY_WEIXIN_PHONE = "weixin_phone";

    //需要增加支付宝切换
    public static final String BUNDLE_KEY_ALIPAY_NAME = "alipay_name";
    public static final String BUNDLE_KEY_ALIPAY_PHONE = "alipay_phone";


    // 提现类型 1、钻石；2、咿呀
    public static final int TYPE_DIAMOND = 1, TYPE_ORMOSIA = 2;

    private TextView mWithDrawBalanceTv;
    private TextView mResidueNumTitleTv;
    private Button mWithDraw;
    private AppToolBar mTitleBar;
    private TextView alipayAccount;
    private TextView alipayName;
    private TextView mUnbindingContentTv;
    private TextView diamondNumWithdraw;
    private ImageView ivWdLogo;
    private FrameLayout binder;
    private RelativeLayout binderSucceed;
    private RecyclerView mRecyclerView;
    private WithdrawJewelAdapter mJewelAdapter;
    public WithdrwaListInfo checkedPosition;
    private WithdrawInfo withdrawInfos = new WithdrawInfo();
    private boolean isWd = false;
    private int currentWdType = -1;
    private int mApiWithDrawType = TYPE_DIAMOND;// 提现类型 1、钻石；2、咿呀

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);
        EventBus.getDefault().register(this);
        initView();
        setListener();
        initData();
    }

    private void initData() {

//        //工会主播没有咿呀提现
//        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//        if (userInfo != null) {
//            //4 为 工会主播 没有提现功能
//            if (userInfo.getAnchorStatus() == 4) {
//                mWithDrawBalanceTv.setVisibility(View.GONE);
//            } else {
//                mWithDrawBalanceTv.setVisibility(View.VISIBLE);
//            }
//        }

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3, OrientationHelper.VERTICAL, false));
        mJewelAdapter = new WithdrawJewelAdapter();
        mJewelAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (mJewelAdapter != null && !ListUtils.isListEmpty(mJewelAdapter.getData())) {
                checkedPosition = mJewelAdapter.getData().get(position);
                if (!isWithdraw()) {
                    return;
                }
                int size = mJewelAdapter.getData().size();
                for (int i = 0; i < size; i++) {
                    mJewelAdapter.getData().get(i).isSelected = position == i;
                }
                mJewelAdapter.notifyDataSetChanged();
            }
        });
        mRecyclerView.setAdapter(mJewelAdapter);
        loadAlipayInfo();
        loadRecyclerViewData(mApiWithDrawType);
    }

    private void loadAlipayInfo() {
        CoreManager.getCore(IWithdrawCore.class).getWithdrawUserInfo(
                CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfo(WithdrawInfo withdrawInfo) {
        if (withdrawInfo != null) {
            withdrawInfos = withdrawInfo;
            //优先以后端历史状态为主
            if (withdrawInfo.withDrawType == 2) {//支付宝提现
                if (StringUtils.isNotEmpty(withdrawInfo.alipayAccount) && !"null".equals(withdrawInfo.alipayAccount)) {//有支付宝账号改变状态
                    setAliWithdrawAccount(withdrawInfo.alipayAccount, withdrawInfo.alipayAccountName);
                } else {//否则默认微信
                    if (withdrawInfo.hasWx) {
                        setWeixinWithdrawAccount();
                    } else {
                        setNoWithdrawAccount();
                    }
                }
            } else {//不是支付宝提现
                if (withdrawInfo.hasWx) {
                    setWeixinWithdrawAccount();
                } else {
                    if (StringUtils.isNotEmpty(withdrawInfo.alipayAccount) && !"null".equals(withdrawInfo.alipayAccount)) {//有支付宝账号
                        setAliWithdrawAccount(withdrawInfo.alipayAccount, withdrawInfo.alipayAccountName);
                    } else {
                        setNoWithdrawAccount();
                    }
                }
            }
            if (mApiWithDrawType == TYPE_ORMOSIA) {
                diamondNumWithdraw.setText(String.format(Locale.getDefault(), "%.2f", withdrawInfo.redbeanNum));
            } else {
                diamondNumWithdraw.setText(String.format(Locale.getDefault(), "%.2f", withdrawInfo.diamondNum));
            }

        }
    }

    /**
     * 设置支付宝账号的布局
     */
    private void setAliWithdrawAccount(String account, String name) {
        currentWdType = 2;
        isWd = true;
        if (alipayName.getVisibility() == View.GONE) {
            alipayName.setVisibility(View.VISIBLE);
        }
        /*ivWdLogo.setImageResource(R.drawable.ic_alipay_logo);*/
        binderSucceed.setBackgroundResource(R.mipmap.bg_diamond_alipay);
        binder.setVisibility(View.GONE);
        binderSucceed.setVisibility(View.VISIBLE);

        notifyListState(mJewelAdapter.getData());
        mJewelAdapter.notifyDataSetChanged();
        alipayAccount.setText(getString(R.string.txt_withdraw_alipay_account, account));
        alipayName.setText(getString(R.string.txt_withdraw_alipay_name, name));
    }

    /**
     * 设置微信账号的布局
     */
    private void setWeixinWithdrawAccount() {
        currentWdType = 1;
        isWd = true;
        /*ivWdLogo.setImageResource(R.drawable.ic_weichat_pay);*/
        binderSucceed.setBackgroundResource(R.mipmap.bg_diamond_wechat);
        binder.setVisibility(View.GONE);
        binderSucceed.setVisibility(View.VISIBLE);

        notifyListState(mJewelAdapter.getData());
        mJewelAdapter.notifyDataSetChanged();

        if (!TextUtils.isEmpty(withdrawInfos.weixinName)) {
            alipayName.setText("微信名：" + withdrawInfos.weixinName);
        } else {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo != null && !TextUtils.isEmpty(userInfo.getNick())) {
                alipayName.setText("微信名：" + userInfo.getNick());
            }

        }

        alipayAccount.setText("电话：" + withdrawInfos.phone);
//        alipayAccount.setText("已绑定微信提现");
//        alipayName.set
//        if (alipayName.getVisibility() == View.VISIBLE) {
//            alipayName.setVisibility(View.GONE);

//        }
    }

    /**
     * 设置没有账号的布局
     */
    private void setNoWithdrawAccount() {
        currentWdType = -1;
        isWd = false;
        binder.setVisibility(View.VISIBLE);
        binderSucceed.setVisibility(View.GONE);
        if (MyWithdrawActivity.isOpenAliPay) {
            mUnbindingContentTv.setText("请先绑定您的支付宝或微信账号后进行提现!");
        } else {
            mUnbindingContentTv.setText("请先绑定微信账号后进行提现!");
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawUserInfoFail(String error) {
        LogUtil.i("onGetWithdrawUserInfoFail", error);
        toast(error);
    }

    private void loadRecyclerViewData(int type) {
        CoreManager.getCore(IWithdrawCore.class).getWithdrawList(type);
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawList(List<WithdrwaListInfo> withdrwaListInfo) {
        if (withdrwaListInfo != null && withdrwaListInfo.size() > 0) {
            notifyListState(withdrwaListInfo);
            mJewelAdapter.setWithdrawType(mApiWithDrawType);
            mJewelAdapter.setNewData(withdrwaListInfo);
        }
        getDialogManager().dismissDialog();

    }

    private void notifyListState(List<WithdrwaListInfo> withdrwaListInfo) {
        if (withdrwaListInfo != null && !withdrwaListInfo.isEmpty()) {
            if (!isWd) {
                return;
            }
            for (WithdrwaListInfo info : withdrwaListInfo) {
                info.setWd(true);
            }
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onGetWithdrawListFail(String error) {
        getDialogManager().dismissDialog();
        toast("获取收益提取列表失败");
        LogUtil.i("onGetWithdrawListFail", error);
    }


    private void setListener() {
        //钻石 or 咿呀提现
        mWithDrawBalanceTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialogManager().showProgressDialog(WithdrawActivity.this);
                Drawable iconDrawable = null;
                if (mApiWithDrawType == TYPE_DIAMOND) {
                    mApiWithDrawType = TYPE_ORMOSIA;
                    mWithDrawBalanceTv.setText("钻石余额");
                    diamondNumWithdraw.setText(String.format(Locale.getDefault(), "%.2f", withdrawInfos.redbeanNum));
                    mResidueNumTitleTv.setText("咿呀余额");

                    iconDrawable = getResources().getDrawable(R.drawable.icon_ormosia);


                    //工会主播没有咿呀提现
                    UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                    if (userInfo != null) {
                        //4 为 工会主播 没有提现功能
                        if (userInfo.getAnchorStatus() == 4) {
                            mWithDraw.setEnabled(false);
                        } else {
                            mWithDraw.setEnabled(true);
                        }
                    }
                } else {
                    mApiWithDrawType = TYPE_DIAMOND;
                    mWithDrawBalanceTv.setText("咿呀余额");
                    diamondNumWithdraw.setText(String.format(Locale.getDefault(), "%.2f", withdrawInfos.diamondNum));
                    mResidueNumTitleTv.setText("钻石余额");

                    iconDrawable = getResources().getDrawable(R.drawable.ic_with_draw_diamond);

                    mWithDraw.setEnabled(true);
                }
                if (iconDrawable != null) {
                    iconDrawable.setBounds(0, 0, iconDrawable.getMinimumWidth(), iconDrawable.getMinimumHeight());
                    mResidueNumTitleTv.setCompoundDrawables(iconDrawable, null, null, null);
                }

                loadRecyclerViewData(mApiWithDrawType);
            }
        });

        //用户点击绑定支付宝
        binder.setOnClickListener(v -> {
            //跳转绑定手机号码，绑定成功以后显示bindersucceed
//            BinderWeixinPayActivity.start(WithdrawActivity.this, withdrawInfos);
            if (MyWithdrawActivity.isOpenAliPay) {
                WithdrawWayDialog withdrawWayDialog = WithdrawWayDialog.newInstance(withdrawInfos);
                withdrawWayDialog.setOnWithdrawWayChangeListener(this);
                withdrawWayDialog.showDialog(getSupportFragmentManager(), "WithdrawWayDialog2");
            } else {
//                BinderWeixinPayActivity.start(WithdrawActivity.this, withdrawInfos);
                ChangeWeixinPayActivity.start(WithdrawActivity.this);
            }

        });
        //用户点击修改支付宝信息
        binderSucceed.setOnClickListener(v -> {
            if (MyWithdrawActivity.isOpenAliPay) {
                WithdrawWayDialog withdrawWayDialog = WithdrawWayDialog.newInstance(withdrawInfos);
                withdrawWayDialog.setOnWithdrawWayChangeListener(this);
                withdrawWayDialog.showDialog(getSupportFragmentManager(), "WithdrawWayDialog");
            } else {
                //绑定成功，就可以切换其他的微信
                ChangeWeixinPayActivity.start(WithdrawActivity.this);

            }

        });
        mTitleBar.setOnBackBtnListener(view -> finish());
        mTitleBar.setOnRightBtnClickListener(view -> CommonWebViewActivity.start(this, BaseUrl.WITTH_DRAW));
        mWithDraw.setOnClickListener(view -> {
            if (checkedPosition == null) {
                toast("未选择提取金额");
                return;
            }
            //发起兑换
            getDialogManager().showOkCancelDialog("您将要兑换" + checkedPosition.getCashProdName(), true,
                    new DialogManager.OkCancelDialogListener() {

                        @Override
                        public void onCancel() {
                            getDialogManager().dismissDialog();
                        }

                        @Override
                        public void onOk() {
                            if (currentWdType == -1) {
                                toast("请先选择提取方式！");
                                return;
                            }
                            getDialogManager().dismissDialog();
                            if (checkedPosition != null) {
                                //  /withDraw/v2/withDrawCash
                                //切换微信 需要更新用户的提现微信
                                if (!TextUtils.isEmpty(mWeixinOpenId) && currentWdType == 1) {
                                    CoreManager.getCore(IWithdrawCore.class).requestExchange(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                            CoreManager.getCore(IAuthCore.class).getTicket(),
                                            checkedPosition.cashProdId, currentWdType, mApiWithDrawType, mWeixinName, mWeixinOpenId);
                                } else {

                                    //新增：支付宝也要按照微信这样切换
                                    if (!TextUtils.isEmpty(mAlipayName) && currentWdType == 2) {
                                        CoreManager.getCore(IWithdrawCore.class).requestExchange(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                                CoreManager.getCore(IAuthCore.class).getTicket(),
                                                checkedPosition.cashProdId, currentWdType, mApiWithDrawType, mAlipayName, mAlipayPhone);
                                    } else {
                                        CoreManager.getCore(IWithdrawCore.class).requestExchange(CoreManager.getCore(IAuthCore.class).getCurrentUid(),
                                                CoreManager.getCore(IAuthCore.class).getTicket(),
                                                checkedPosition.cashProdId, currentWdType, mApiWithDrawType);
                                    }

                                }

                            } else {
                                toast("兑换失败");
                            }
                        }
                    });
        });
    }

    private boolean isWithdraw() {
        if (withdrawInfos != null && !withdrawInfos.isNotBoundPhone) {
            if (checkedPosition != null) {
                if (mApiWithDrawType == TYPE_DIAMOND) {
                    //用户的钻石余额 > 选中金额的钻石数时
                    if (withdrawInfos.diamondNum >= checkedPosition.diamondNum) {
                        return true;
                    } else {
                        toast("您的钻石不足以进行此次提取！");
                        return false;
                    }
                } else {
                    if (withdrawInfos.redbeanNum >= checkedPosition.diamondNum) {
                        return true;
                    } else {
                        toast("您的咿呀不足以进行此次提取！");
                        return false;
                    }
                }

            }
        } else {
            return false;
        }
        //如果选中position不为空的时候
        return false;
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onRequestExchange(ExchangerInfo exchangerInfo) {
        if (exchangerInfo != null) {
            WalletActivity.isRefresh = true;
            if (mApiWithDrawType == TYPE_DIAMOND) {
                diamondNumWithdraw.setText(String.format(Locale.getDefault(), "%.2f", exchangerInfo.diamondNum));
                if (withdrawInfos != null) {
                    withdrawInfos.diamondNum = exchangerInfo.diamondNum;
                }
            } else {
                diamondNumWithdraw.setText(String.format(Locale.getDefault(), "%.2f", exchangerInfo.redBeanNum));
                if (withdrawInfos != null) {
                    withdrawInfos.redbeanNum = exchangerInfo.redBeanNum;
                }
            }


            toast("收益提取成功");
        }
    }

    @CoreEvent(coreClientClass = IWithdrawCoreClient.class)
    public void onRequestExchangeFail(String error) {
        toast(error);
        LogUtil.i("onRequestExchangeFail", error);
    }

    private void initView() {
        mResidueNumTitleTv = (TextView) findViewById(R.id.tv_diamondNum_withdraw);
        diamondNumWithdraw = (TextView) findViewById(R.id.tv_diamondNums);
        mUnbindingContentTv = (TextView) findViewById(R.id.unbinding_content_tv);
        binder = (FrameLayout) findViewById(R.id.rly_binder);
        binderSucceed = (RelativeLayout) findViewById(R.id.rly_binder_succeed);
        alipayAccount = (TextView) findViewById(R.id.tv_user_zhifubao);
        alipayName = (TextView) findViewById(R.id.tv_user_zhifubao_name);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mTitleBar = (AppToolBar) findViewById(R.id.toolbar);
        mWithDraw = (Button) findViewById(R.id.btn_withdraw);
        ivWdLogo = (ImageView) findViewById(R.id.zhifubao);
        mWithDrawBalanceTv = (TextView) findViewById(R.id.withdraw_balance_tv);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshEvent(RefreshInfo refreshInfo) {
        loadAlipayInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onChangeListener(WithdrawInfo withdrawInfo) {
        onGetWithdrawUserInfo(withdrawInfo);
    }


    private String mWeixinName;
    private String mWeixinOpenId;
    private String mWeixinPhone;

    private String mAlipayName;
    private String mAlipayPhone;

    /**
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        Bundle bundle = data.getExtras();
        if (requestCode == 1000) {  //微信
            if (bundle != null) {
                mWeixinName = bundle.getString(BUNDLE_KEY_WEIXIN_NAME, "");
                mWeixinOpenId = bundle.getString(BUNDLE_KEY_WEIXIN_OPEN_ID, "");
                mWeixinPhone = bundle.getString(BUNDLE_KEY_WEIXIN_PHONE, "");

                alipayAccount.setText("电话: " + mWeixinPhone);
                alipayName.setText("微信名: " + mWeixinName);
                /*ivWdLogo.setImageResource(R.drawable.ic_weichat_pay);*/
                binderSucceed.setBackgroundResource(R.mipmap.bg_diamond_wechat);
                withdrawInfos.weixinName = mWeixinName;
                withdrawInfos.phone = mWeixinPhone;
                setWeixinWithdrawAccount();

            }
        } else if (requestCode == 1001) { //支付宝
            if (bundle != null) {
                mAlipayName = bundle.getString(BUNDLE_KEY_ALIPAY_NAME, "");
                mAlipayPhone = bundle.getString(BUNDLE_KEY_ALIPAY_PHONE, "");

                withdrawInfos.alipayAccount = mAlipayPhone;
                withdrawInfos.alipayAccountName = mAlipayName;
                setAliWithdrawAccount(withdrawInfos.alipayAccount, withdrawInfos.alipayAccountName);
            }
        }
    }
}
