package com.yiya.mobile.ui.newfind.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.tongdaxing.xchat_core.bean.OnlinesFriendsBean;
import com.tongdaxing.xchat_core.home.RoomAttentionInfo;
import com.yiya.mobile.ui.common.widget.CircleImageView;
import com.tongdaxing.erban.R;
import com.yiya.mobile.utils.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

public class HeadportraitGirdViewAdapter extends BaseAdapter {

    private Context context;
    private int count;
    private List<OnlinesFriendsBean> list = new ArrayList<>();

    public HeadportraitGirdViewAdapter(Context context, List<OnlinesFriendsBean> Rlist) {
        this.context = context;
        this.count = Rlist.size();
        if(list.size()>0){
            list.removeAll(list);
            list.addAll(Rlist);
        }else{
            this.list.addAll(Rlist);
        }
    }

    public List<OnlinesFriendsBean> getList() {
        return list;
    }

    public void setList(List<OnlinesFriendsBean> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView ==null){
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.item_headportrait,null);
            viewHolder.circleImageView = convertView.findViewById(R.id.headportrait_circle);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        ImageLoadUtils.loadImage(context, list.get(position).getAvatar(), viewHolder.circleImageView);
        return convertView;
    }

    private final class ViewHolder{
        CircleImageView circleImageView;
    }
}
