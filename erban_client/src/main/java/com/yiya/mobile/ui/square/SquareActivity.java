package com.yiya.mobile.ui.square;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.ui.square.fragment.SquareFragment;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SquareActivity extends BaseActivity {

    @BindView(R.id.vp_container)
    ViewPager vpContainer;
    @BindView(R.id.toolbar)
    AppToolBar toolbar;

    public static void start(Context context) {
        context.startActivity(new Intent(context, SquareActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_square);
        ButterKnife.bind(this);

        initEvent();
    }

    private void initEvent() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(SquareFragment.newInstance());
        vpContainer.setAdapter(new BaseIndicatorStateAdapter(getSupportFragmentManager(), fragments));

        toolbar.setOnBackBtnListener(v -> finish());
    }

}