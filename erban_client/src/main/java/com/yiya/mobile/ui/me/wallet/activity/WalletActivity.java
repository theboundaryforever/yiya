package com.yiya.mobile.ui.me.wallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.me.bills.activity.BillActivity;
import com.yiya.mobile.ui.me.bills.activity.WithdrawBillsActivity;
import com.yiya.mobile.ui.me.wallet.presenter.IncomePresenter;
import com.yiya.mobile.ui.me.wallet.view.IIncomeView;
import com.yiya.mobile.ui.me.withdraw.WithdrawActivity;
import com.netease.nim.uikit.common.util.sys.ScreenUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 新皮：我的钱包页面
 *
 * @author zwk 2018/5/30
 */
@CreatePresenter(IncomePresenter.class)
public class WalletActivity extends BaseMvpActivity<IIncomeView, IncomePresenter> implements IIncomeView, View.OnClickListener {

    @BindView(R.id.toolbar)
    AppToolBar mToolBar;
    @BindView(R.id.bill)
    TextView payBill;
    @BindView(R.id.payBillContainer)
    RelativeLayout billContainer;
    @BindView(R.id.tv_gold)
    TextView tvGold;
    @BindView(R.id.charge)
    TextView charge;
    @BindView(R.id.withdraw)
    TextView payWithdraw;
    @BindView(R.id.payWithdrawContainer)
    RelativeLayout withdrawContainer;
    @BindView(R.id.tv_dimand)
    TextView tvDimand;
    @BindView(R.id.putForward)
    TextView putForward;
    @BindView(R.id.exchange)
    TextView exchange;

    private boolean isRequest;
    public static boolean isRefresh = false;

    public static void start(Context context) {
        Intent intent = new Intent(context, WalletActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        ButterKnife.bind(this);

        int width = ScreenUtil.getDisplayWidth() - ScreenUtil.dip2px(20);
        ViewGroup.LayoutParams bill = billContainer.getLayoutParams();
        ViewGroup.LayoutParams withdraw = withdrawContainer.getLayoutParams();

        bill.width = withdraw.width = width;
        bill.height = withdraw.height = width / 2;

        billContainer.setLayoutParams(bill);
        withdrawContainer.setLayoutParams(withdraw);

        getMvpPresenter().refreshWalletInfo(true);
        initListener();
    }

    private void initListener() {
        payBill.setOnClickListener(this);
        charge.setOnClickListener(this);
        payWithdraw.setOnClickListener(this);
        putForward.setOnClickListener(this);
        exchange.setOnClickListener(this);

        mToolBar.setOnBackBtnListener(view -> finish());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isRefresh) {
            isRefresh = false;
            getMvpPresenter().loadWalletInfo();
        }
    }

    @Override
    public void setupUserWalletBalance(WalletInfo walletInfo) {
        tvGold.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.goldNum));
        tvDimand.setText(String.format(Locale.getDefault(), "%.2f", walletInfo.diamondNum));
    }

    @Override
    public void getUserWalletInfoFail(String error) {
        tvGold.setText("0");
        tvDimand.setText("0");
        toast(error);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bill:
                startActivity(new Intent(this, BillActivity.class));
                break;
            case R.id.charge:
                ChargeActivity.start(this);
                break;
            case R.id.withdraw:
                startActivity(new Intent(this, WithdrawBillsActivity.class));
                break;
            case R.id.putForward:
                isRequest = true;
                getMvpPresenter().hasBindPhone();
                break;
            case R.id.exchange:
                startActivity(new Intent(this, ExchangeGoldActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public void handleClick(int id) {

    }

    @Override
    public void hasBindPhone() {
        if (!isRequest) {
            return;
        }
        isRequest = false;
        startActivity(new Intent(this, WithdrawActivity.class));
    }

    @Override
    public void hasBindPhoneFail(String error) {
        if (!isRequest) {
            return;
        }
        startActivity(new Intent(this, BinderPhoneActivity.class));
    }
}
