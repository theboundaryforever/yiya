package com.yiya.mobile.ui.web;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.yiya.mobile.constant.AppConstant;
import com.yiya.mobile.ui.widget.dialog.ShareDialog;
import com.juxiao.library_ui.widget.AppToolBar;
import com.juxiao.library_utils.storage.StorageType;
import com.juxiao.library_utils.storage.StorageUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.share.IShareCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import cn.sharesdk.framework.Platform;

/**
 * @author dell
 */
public class CommonWebViewActivity extends BaseWebViewActivity implements ShareDialog.OnShareDialogItemClick {

    private AppToolBar mToolBar;
    private ProgressBar mProgressBar;

    private static final String POSITION = "position";
    private int mPosition;
    private int mProgress;

    private Runnable mProgressRunnable = new Runnable() {
        @Override
        public void run() {
            if (mProgress < 96) {
                mProgress += 3;
                mProgressBar.setProgress(mProgress);
                mHandler.postDelayed(mProgressRunnable, 10);
            }
        }
    };

    public static void start(Context context, String url) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        context.startActivity(intent);
    }

    /**
     * 开启支付localStorage
     */
    public static void start(Context context, String url, boolean startLocalStorage) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("startLocalStorage", startLocalStorage);
        context.startActivity(intent);
    }

    /**
     * 排行榜专用
     */
    public static void start(Context context, String url, int position) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra(POSITION, position);
        context.startActivity(intent);
    }

    /**
     * 状态主题
     */
    public static void start(Context context, int titleType, String url) {
        Intent intent = new Intent(context, CommonWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("titleType", titleType);
        context.startActivity(intent);
    }

    @Override
    protected void releaseHandler(Handler handler) {
        super.releaseHandler(handler);
        handler.removeCallbacks(mProgressRunnable);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_common_web_view;
    }

    @Override
    protected void getMyIntent(Intent intent) {
        super.getMyIntent(intent);
        mPosition = intent.getIntExtra(POSITION, 0) + 1;
    }

    @Override
    protected void initView() {
        super.initView();

        boolean startLocalStorage = getIntent().getBooleanExtra("startLocalStorage", false);
        if (startLocalStorage){// 开启webview缓存
            webView.getSettings().setDomStorageEnabled(true);
            webView.getSettings().setAppCacheMaxSize(1024*1024);
//            String appCachePath = getApplicationContext().getCacheDir().getAbsolutePath();
            String appCachePath = StorageUtil.getDirectoryByDirType(StorageType.TYPE_APPLICATION_CACHE);
            webView.getSettings().setAppCachePath(appCachePath);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setAppCacheEnabled(true);
        }

        mToolBar = (AppToolBar) findViewById(R.id.toolbar);
        int type = getIntent().getIntExtra("titleType", 0);
        if (type == AppConstant.WEB_TITLE_TYPE_APP_THEME) {
            mToolBar.setBackgroundColor(0xff7f47f8);
            mToolBar.setLeftImgRes(R.drawable.arrow_left_white);
            mToolBar.setTitleColor(ContextCompat.getColor(this, R.color.white));
            mToolBar.setRightBtnImage(R.drawable.ic_share_white);
            mToolBar.setLineBackgroundColor(0xff7f47f8);
        }
//        } else if (type == WEB_TITLE_TYPE_RNAKING_THEME) {
//            mToolBar.setBackgroundResource(R.drawable.bg_ranking_title);
//        }
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }


    //调用系统按键返回上一层
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                return super.onKeyDown(keyCode, event);
            }
        }
        return true;
    }

    @Override
    public void onSharePlatformClick(Platform platform) {
        if (webViewInfo != null) {
            CoreManager.getCore(IShareCore.class).shareH5(webViewInfo, platform);
        }
    }

    public AppToolBar getToolbar() {
        return mToolBar;
    }

    @Override
    protected void setListener() {
        mToolBar.setOnBackBtnListener(view -> {
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                finish();
            }
        });
        mToolBar.setOnRightBtnClickListener(view -> {
            ShareDialog shareDialog = new ShareDialog(this);
            shareDialog.setOnShareDialogItemClick(this);
            shareDialog.show();
        });
    }

    @Override
    protected WebView getWebView() {
        return (WebView) findViewById(R.id.webview);
    }

    @Override
    protected void initData() {
        mHandler.post(mProgressRunnable);
        super.initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //通知H5原生WebView将要显示
        jsInterface.onWebViewWillAppear();
    }

    @Override
    protected void setJsInterface(JSInterface jsInterface) {
        jsInterface.setPosition(mPosition);
    }

    @Override
    protected void onPageFinished(WebView view, int progress) {
        mProgressBar.setProgress(100);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onReceivedTitle(WebView view, String title) {
        mToolBar.setTitle(title);
    }
}