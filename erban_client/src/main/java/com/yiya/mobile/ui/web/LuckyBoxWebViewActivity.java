package com.yiya.mobile.ui.web;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;

import com.tongdaxing.erban.R;

public class LuckyBoxWebViewActivity extends BaseWebViewActivity {

    private View progressBar;

    public static void start(Context context, String url) {
        Intent intent = new Intent(context, LuckyBoxWebViewActivity.class);
        intent.putExtra("url", url);
        context.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_transparent_web_view;
    }

    @Override
    protected void initView() {
        super.initView();
        progressBar = findView(R.id.layout_progress);
        getWebView().setBackgroundColor(0);
    }

    @Override
    protected WebView getWebView() {
        return (WebView) findViewById(R.id.webview);
    }

    @Override
    protected void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        progressBar.setVisibility(View.GONE);
        getWebView().setVisibility(View.GONE);
        finish();
    }

    @Override
    protected void onPageFinished(WebView view, int progress) {
        super.onPageFinished(view, progress);
        progressBar.setVisibility(View.GONE);
        getWebView().setVisibility(View.VISIBLE);
    }
}
