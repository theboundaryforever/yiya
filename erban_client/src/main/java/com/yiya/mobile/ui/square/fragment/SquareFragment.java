package com.yiya.mobile.ui.square.fragment;

import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.find.FindSquarePresenter;
import com.yiya.mobile.presenter.find.IFindSquareView;
import com.yiya.mobile.ui.find.view.PublicMessageView;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachParser;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PublicChatRoomAttachment;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.ReUsedSocketManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.user.VersionsCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.im.IMProCallBack;
import com.tongdaxing.xchat_framework.im.IMReportBean;
import com.tongdaxing.xchat_framework.im.IMReportRoute;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.ChatUtil;
import com.tongdaxing.xchat_framework.util.util.ImeUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.SpUtils;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 新版的公聊大厅 -- 广场
 *
 * @zwk 2018/11/23
 */
@CreatePresenter(FindSquarePresenter.class)
public class SquareFragment extends BaseMvpFragment<IFindSquareView, FindSquarePresenter> implements IFindSquareView, View.OnClickListener {

    @BindView(R.id.message_view)
    PublicMessageView mMessageView;
    @BindView(R.id.input_edit)
    EditText mInputEdit;
    @BindView(R.id.input_send)
    ImageView mInputSend;
    @BindView(R.id.tv_count_down)
    TextView mTvCountDown;

    public static final long devRoomId = 2;//测试公聊
    public static final long formalRoomId = 4;//线上
    public static final long checkDevRoomId = 1;//线下审核
    public static final long checkRoomId = 3;//线上审核
    private long roomId = formalRoomId;
    public long cacheTime = 0;
    private String cacheNameKey = "cacheNameKey";
    private CountDownTimer mCountDownTimer;

    //避免重复提交
    private boolean isReport = true;
    //是否进房成功
    private boolean isEnterRoomSuc = false;

    public static Fragment newInstance() {
        return new SquareFragment();
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_square;
    }

    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
//        ivInvite.setOnClickListener(this);
        mInputSend.setOnClickListener(this);
        mTvCountDown.setOnClickListener(this);
    }

    @Override
    public void initiate() {
        IMNetEaseManager.get().subscribeChatRoomEventObservable(roomEvent -> {
            if (roomEvent == null) return;
            int event = roomEvent.getEvent();
            switch (event) {
                case RoomEvent.SOCKET_IM_RECONNECT_LOGIN_SUCCESS://IM重连成功 -- 重新进聊天室
                    com.netease.nim.uikit.common.util.log.LogUtil.d(AvRoomDataManager.TAG, "大厅重连...");
                    getMvpPresenter().enterRoom(roomId + "");
                    break;
                default:
                    break;
            }
        }, this);
    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        showLoading();
        getMvpPresenter().getSquareRoomId();
    }

    @Override
    public void getSquareRoomIdSuccess(boolean audit) {
        if (mMessageView != null) {
            mMessageView.clear();
        }
        if (!audit) {
            if (BasicConfig.isDebug) {
                roomId = devRoomId;
            } else {
                roomId = formalRoomId;
            }
        } else {
            if (BasicConfig.isDebug) {
                roomId = checkDevRoomId;
            } else {
                roomId = checkRoomId;
            }
        }
        checkCountDown();
        getMvpPresenter().enterRoom(roomId + "");
    }

    @Override
    public void resetSquareLayout() {
        hideStatus();
    }

    @Override
    public void enterPublicRoomSuccess(IMReportBean imReportBean) {
        isEnterRoomSuc = true;
        com.netease.nim.uikit.common.util.log.LogUtil.d(AvRoomDataManager.TAG, "大厅 ---- enterPublicRoomSuccess");
        //历史记录的重进房间后补发消息id的标记
        long msgId = -1;
        if (mMessageView != null && !ListUtils.isListEmpty(mMessageView.getChatRoomMessages())) {
            ChatRoomMessage msg = mMessageView.getChatRoomMessages().get(mMessageView.getChatRoomMessages().size() - 1);
            if (msg != null &&
                    msg.getAttachment() != null
                    && msg.getAttachment().getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM
                    && msg.getAttachment() instanceof PublicChatRoomAttachment) {
                msgId = ((PublicChatRoomAttachment) msg.getAttachment()).getServer_msg_id();
            }
        }
        Json str = imReportBean.getReportData().data;
        List<Json> his_list = str.jlist("his_list");
        if (ListUtils.isListEmpty(his_list)) {
            return;
        }

        long finalMsgId = msgId;
        addDisposable(Observable.fromIterable(his_list)
                .subscribeOn(Schedulers.io())
                .compose(bindToLifecycle())
                .map(this::parse2ChatRoomMsg)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(message -> {
                    IMCustomAttachment imCustomAttachment = message.getAttachment();
                    if (imCustomAttachment == null) {
                        return;
                    }
                    if (imCustomAttachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM
                            && imCustomAttachment instanceof PublicChatRoomAttachment) {//如果是文本历史记录补发
                        if (((PublicChatRoomAttachment) imCustomAttachment).getServer_msg_id() > finalMsgId) {
                            IMNetEaseManager.get().addMessagesImmediately(message);
                        }
                    }
                }));
    }

    @NotNull
    private ChatRoomMessage parse2ChatRoomMsg(Json json) {
        ChatRoomMessage message = new ChatRoomMessage();
        message.setRoute(IMReportRoute.sendPublicMsgNotice);
        String custom = json.json_ok("custom").toString();
        if (!TextUtils.isEmpty(custom)) {
            IMCustomAttachment imCustomAttachment = IMCustomAttachParser.parse(custom);
            message.setAttachment(imCustomAttachment);
        }
        String member = json.json_ok("member").toString();
        if (!TextUtils.isEmpty(member)) {
            IMChatRoomMember imChatRoomMember = new Gson().fromJson(member, IMChatRoomMember.class);
            message.setImChatRoomMember(imChatRoomMember);
        }
        return message;
    }

    @Override
    public void enterPublicRoomFail(String error) {
        isEnterRoomSuc = true;
        showNoData(error);
    }

    @Override
    public void onReloadData() {
        super.onReloadData();
        cacheTime = System.currentTimeMillis();
        SpUtils.put(getContext(), cacheNameKey, cacheTime);
        onLazyLoadData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.input_send:
                sendMsg();
                break;
//            case R.id.tv_count_down:
//                toast("请稍等");
//                break;
        }
    }

    @Override
    public void reportSuccess() {
        isReport = false;
    }

    public void sendMsg() {
//        if (AvRoomDataManager.get().getRoomInfo() == null) {
//            toast("发广播需要在房间里面哦");
//            return;
//        }
        if (System.currentTimeMillis() - cacheTime < 10000) {
            toast("广播发送间隔为10秒，请稍等");
            return;
        }
        if (ChatUtil.checkBanned())
            return;
        String trim = mInputEdit.getText().toString().trim();
        if (StringUtils.isEmpty(trim))
            return;
        String sensitiveWordData = CoreManager.getCore(VersionsCore.class).getSensitiveWordData();
        if (!TextUtils.isEmpty(sensitiveWordData) && !TextUtils.isEmpty(trim)) {
            if (trim.matches(sensitiveWordData)) {
                SingleToastUtil.showToast(this.getString(R.string.sensitive_word_data));
                return;
            }
        }
        if (trim.length() > 40) {
            trim = trim.substring(0, 40);
        }
        getMvpPresenter().sendMessage(roomId + "", trim);
        mInputEdit.setText("");
        ImeUtil.hideIME(getActivity());
        cacheTime = System.currentTimeMillis();
        SpUtils.put(getContext(), cacheNameKey, cacheTime);
        checkCountDown();
        if (isReport) {
            getMvpPresenter().getReportState();
        }
    }


    @Override
    public void sendMessageSuccess() {

    }

    @Override
    public void sendMessageFail(String error) {
        toast(error);
    }

    public void checkCountDown() {
        long time = System.currentTimeMillis();
        long l = time - cacheTime;
        l = 10000 - l;
        if (l <= 10000 && l > 0) {
            disableSendMsg();
            mCountDownTimer = new CountDownTimer(l, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    int countDownTime = (int) (millisUntilFinished / 1000);
                    mTvCountDown.setText(countDownTime + "S");
                }

                @Override
                public void onFinish() {
                    enableSendMsg();
                }
            };
            mCountDownTimer.start();
        } else {
            enableSendMsg();
        }
    }

    private void disableSendMsg() {
        mTvCountDown.setVisibility(View.VISIBLE);
    }

    private void enableSendMsg() {
        mTvCountDown.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        isReport = true;
        //这里不能退出，目前房间内
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogout() {//用户进入该页面才会有回调
        //退出后离开公聊
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        isReport = true;
        isEnterRoomSuc = false;
        if (mMessageView != null) {
            mMessageView.clear();
        }
        ReUsedSocketManager.get().exitPublicRoom(roomId, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {

            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

}
