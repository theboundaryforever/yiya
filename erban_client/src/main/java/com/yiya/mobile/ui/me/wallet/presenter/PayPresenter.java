package com.yiya.mobile.ui.me.wallet.presenter;

import com.yiya.mobile.ui.me.wallet.model.PayModel;
import com.yiya.mobile.ui.me.wallet.view.IPayView;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * Created by MadisonRong on 08/01/2018.
 */

public class PayPresenter<T extends IPayView> extends AbstractMvpPresenter<T> {

    protected PayModel payModel;
    protected WalletInfo walletInfo;

    public PayPresenter() {
        this.payModel = new PayModel();
    }

    /**
     * 刷新钱包信息
     */
    public void refreshWalletInfo(boolean force) {
       payModel.refreshWalletInfo(force,new OkHttpManager.MyCallBack<ServiceResult<WalletInfo>>() {
           @Override
           public void onError(Exception e) {
               if (getMvpView() != null && e != null)
                   getMvpView().getUserWalletInfoFail(e.getMessage());
           }

           @Override
           public void onResponse(ServiceResult<WalletInfo> response) {
               if (null != response && response.isSuccess() && response.getData() != null) {
                   walletInfo = response.getData();
                   if (getMvpView() != null)
                       getMvpView().setupUserWalletBalance(response.getData());
               } else {
                   if (getMvpView() != null && response != null)
                       getMvpView().getUserWalletInfoFail(response.getErrorMessage());
               }
           }
       });
    }
}
