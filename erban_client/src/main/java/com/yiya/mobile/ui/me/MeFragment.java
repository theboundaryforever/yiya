package com.yiya.mobile.ui.me;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.MeInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.constant.AppConstant;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.me.adapter.MeAdapter;
import com.yiya.mobile.ui.me.mywallet.activity.WalletActivity;
import com.yiya.mobile.ui.me.setting.activity.AboutActivity;
import com.yiya.mobile.ui.me.task.view.IMeView;
import com.yiya.mobile.ui.message.activity.AttentionListActivity;
import com.yiya.mobile.ui.message.activity.FansListActivity;
import com.yiya.mobile.ui.rank.activity.RankingListActivity;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.utils.UIHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

/**
 * ProjectName:
 * Description:个人中心fragment
 * Created by BlackWhirlwind on 2019/6/28.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
@CreatePresenter(MePresenter.class)
public class MeFragment extends BaseMvpFragment<IMeView, MePresenter> implements IMeView, BaseQuickAdapter.OnItemClickListener {
    public static final String TAG = "MeFragment";

    @BindView(R.id.rv_me)
    RecyclerView rvMe;

    private MeAdapter mMeAdapter;

    @Override
    public void onFindViews() {
        rvMe.setLayoutManager(new LinearLayoutManager(getContext()));
        mMeAdapter = new MeAdapter(getMvpPresenter().generateList());
        rvMe.setAdapter(mMeAdapter);
    }

    @Override
    public void onSetListener() {
        mMeAdapter.setOnItemClickListener(this);
        mMeAdapter.setOnItemChildClickListener(this::onItemChildClick);
    }

    @Override
    public void initiate() {
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "个人中心页");
    }

    @Override
    public void onResume() {
        super.onResume();
        getMvpPresenter().initUserData();
        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_me_main;
    }

    //关注更新用户资料
    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long uid) {
        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    //取消关注更新用户资料
    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid, boolean showNotice) {
        CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        updateUserInfoUI(userInfo);
    }

    private void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        MeInfo meInfo = (MeInfo) adapter.getData().get(position);
        if (MeInfo.TYPE_TOP == meInfo.getItemType()) {
            UserInfo userInfo = meInfo.getUserInfo();
            switch (view.getId()) {
                case R.id.tv_user_info:
                    //主页
                    if (userInfo != null) {
                        UIHelper.showUserInfoAct(getContext(), userInfo.getUid());
                    }
                    break;
                case R.id.tv_attentions:
                    //跳转关注页
                    AttentionListActivity.start(getContext());
                    break;
                case R.id.tv_fans:
                    //跳转粉丝页
                    FansListActivity.start(getContext());
                    break;
                /*case R.id.tv_wallet:
                    //跳转钱包页
                    MyWalletActivity.start(getContext());
                    break;*/
               /* case R.id.tv_my_level:
                    //跳转等级页
                    CommonWebViewActivity.start(getContext(), WEB_TITLE_TYPE_RNAKING_THEME, BaseUrl.MY_LEVEL);
                    break;*/
                case R.id.tv_mission:
                    //任务中心
                    CommonWebViewActivity.start(getContext(), BaseUrl.MISSION_CENTER);
                    break;
              /*  case R.id.tv_shop:
                    //跳转商城页
                    ShopActivity.start(getContext(), true, 0);
                    break;*/
                case R.id.iv_room:
                    //开启语聊直播间
                    RoomFrameActivity.start(mContext, CoreManager.getCore(IAuthCore.class).getCurrentUid(), RoomInfo.ROOMTYPE_HOME_PARTY);
                    break;
                case R.id.tv_copy:
                    //ID复制  mContext.getString(R.string.me_user_id, userInfo.getErbanNo())
                    String userid = mContext.getString(R.string.me_user_id, userInfo.getErbanNo());
                    ClipboardManager cm = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData mClipData = ClipData.newPlainText("Labeid", userid.substring(userid.indexOf(":")+1));
                    cm.setPrimaryClip(mClipData);
                    toast("ID已复制到剪贴板");
                    break;
            }
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (uid == info.getUid()) {
            updateUserInfoUI(info);
        }
        getMvpPresenter().getGiftRankingList(1, RankingListActivity.DATE_TYPE_MONTH);
    }

    @Override
    public void updateUserInfoUI(UserInfo userInfo) {
        if (userInfo != null) {

            if (getMvpPresenter().checkBroadcast()) {
                mMeAdapter.setData(getMvpPresenter().generateList());
            } else {
                mMeAdapter.setData(getMvpPresenter().generateListNoBroadcast());
            }

            List<MeInfo> list = mMeAdapter.getData();
            addDisposable(Observable.fromIterable(list)
                    .filter(multiItemEntity -> MeInfo.TYPE_TOP == multiItemEntity.getItemType() ||
                            MeInfo.TYPE_NAME_VERIFY == multiItemEntity.getItemType() ||
                            MeInfo.TYPE_BROADCASTER_VERIFY == multiItemEntity.getItemType()
                    )
                    .subscribe(meInfo -> {
                        meInfo.setUserInfo(userInfo);
                        mMeAdapter.notifyItemChanged(meInfo.getItemType(), meInfo);
                    }));
        }
    }

    @Override
    public void showGiftRankingList(RankingXCInfo rankingXCInfo) {
        MeInfo meInfo = mMeAdapter.getData().get(MeInfo.TYPE_GIFT);
        meInfo.setRankingXCInfo(rankingXCInfo);
        mMeAdapter.notifyItemChanged(MeInfo.TYPE_GIFT, meInfo);

    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        MeInfo meInfo = (MeInfo) adapter.getData().get(position);
        switch (meInfo.getItemType()) {
            case MeInfo.TYPE_GIFT:
                //跳转礼物贡献榜
                CommonWebViewActivity.start(getContext(), BaseUrl.CONTRIBUTION);
//                RankingListActivity.start(mContext, CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
                break;
            case MeInfo.TYPE_NAME_VERIFY:
               /* //跳转实名验证
                CommonWebViewActivity.start(getContext(), BaseUrl.REAL_NAME);*/
                //跳转钱包页
                /*MyWalletActivity.start(getContext());*/
                WalletActivity.start(getContext());
                break;
            case MeInfo.TYPE_BROADCASTER_VERIFY:
                /*//跳转主播验证
                CommonWebViewActivity.start(getContext(), BaseUrl.ANCHOR_AUTHENTICATION);*/
                //跳转等级页
                CommonWebViewActivity.start(getContext(), AppConstant.WEB_TITLE_TYPE_RNAKING_THEME, BaseUrl.MY_LEVEL);
                break;
            case MeInfo.TYPE_FEEDBACK:
               /* //跳转问题反馈
                startActivity(new Intent(getContext(), FeedbackActivity.class));*/
                //跳转设置页
                UIHelper.showSettingAct(mContext);
                break;
            case MeInfo.TYPE_HOST_RECORD:
                /*//跳转主播记录
                CommonWebViewActivity.start(getContext(), BaseUrl.ANCHOR_RECORD);*/
                //跳转实名验证
                CommonWebViewActivity.start(getContext(), BaseUrl.REAL_NAME);
                break;
            case MeInfo.TYPE_INVITE:
               /* //跳转邀请分成
                InviteFriendActivity.start(getContext());*/
                //跳转问题反馈
                CommonWebViewActivity.start(getContext(), BaseUrl.FEED_BACK);
//                startActivity(new Intent(getContext(), FeedbackActivity.class));
                break;
            case MeInfo.TYPE_ABOUT:
                //跳转关于我们
                startActivity(new Intent(getContext(), AboutActivity.class));
                break;
        }
    }

    @OnClick({R.id.iv_setting, R.id.iv_scan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_scan:
                //扫一扫
                UIHelper.showScanAct(mContext);
                break;
            case R.id.iv_setting:
                //跳转设置页
                UIHelper.showSettingAct(mContext);
                break;
        }
    }
}
