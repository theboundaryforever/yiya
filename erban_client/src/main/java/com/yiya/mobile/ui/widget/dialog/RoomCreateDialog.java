package com.yiya.mobile.ui.widget.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.tongdaxing.erban.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * ProjectName:
 * Description: 创建房间dialog
 * Created by BlackWhirlwind on 2019/6/4.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomCreateDialog extends BottomSheetDialogFragment {

    Unbinder unbinder;
    private OnRoomClickListener mListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.RoomCreateDialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_home_create, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mListener != null) {
            mListener = null;
        }
        unbinder.unbind();
    }

    @OnClick({R.id.iv_video_live, R.id.iv_audio_chat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_video_live:
                if (mListener != null) {
                    mListener.turn2VideoLive();
                }
                break;
            case R.id.iv_audio_chat:
                if (mListener != null) {
                    mListener.turn2AudioChat();
                }
                break;
        }
        dismiss();
    }

    public void setOnRoomClickListener(OnRoomClickListener listener) {
        mListener = listener;
    }

    public interface OnRoomClickListener {

        void turn2VideoLive();

        void turn2AudioChat();
    }
}
