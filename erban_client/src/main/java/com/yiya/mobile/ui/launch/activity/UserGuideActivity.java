package com.yiya.mobile.ui.launch.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.MainActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.PreferencesUtils;
import com.tongdaxing.xchat_framework.util.config.SpEvent;
import com.tongdaxing.xchat_framework.util.util.SpUtils;


/**
 * @author dell
 */
public class UserGuideActivity extends BaseActivity {

    protected static final String TAG = "UserGuide";
    private ImageView enterHome;

    public static void start(Context context) {
        Intent intent = new Intent(context, UserGuideActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_guide);
        initView();
    }

    private void initView() {
        enterHome = (ImageView) findViewById(R.id.enter_home);

        enterHome.setOnClickListener(v -> {
            //b保存状态
            PreferencesUtils.setFristUser(false);

            //第一次启动的状态
            SpUtils.put(this, SpEvent.first_open, "open");

            MainActivity.start(UserGuideActivity.this);
            finish();
        });
    }
}
