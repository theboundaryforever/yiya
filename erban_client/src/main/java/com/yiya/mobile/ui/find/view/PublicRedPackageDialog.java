package com.yiya.mobile.ui.find.view;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.yiya.mobile.ui.find.adapter.PublicRedPackageAdapter;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.bean.ReceiveRedPackageInfo;
import com.tongdaxing.xchat_core.bean.SendRedPackageInfo;
import com.tongdaxing.xchat_core.initial.ClientConfigure;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 文件描述：
 * 大厅红包弹框提示
 * @auther：zdw
 * @data：2019/5/17
 */
public class PublicRedPackageDialog extends BaseDialogFragment implements View.OnClickListener {
    @BindView(R.id.iv_send_rp_avatar)
    ImageView ivAvatar;
    @BindView(R.id.tv_send_rp_nick)
    TextView tvNick;
    @BindView(R.id.tv_send_rp_gold_num)
    TextView tvGoldNum;
    @BindView(R.id.rv_receive_rp_record)
    RecyclerView rvRecord;
    @BindView(R.id.iv_receive_rp_close)
    ImageView ivClose;
    @BindView(R.id.tvRedPacketText)
    TextView tvRedPacketText;


    private SendRedPackageInfo sendRedPackageInfo;

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(this, tag).addToBackStack(null);
        return transaction.commitAllowingStateLoss();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            sendRedPackageInfo = (SendRedPackageInfo)getArguments().getSerializable("sendRedPackageInfo");
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getDialog() != null) {
            Window window = getDialog().getWindow();
            if (window != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        }
        return inflater.inflate(R.layout.dialog_public_red_package,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initView(view);
        initListener();
    }

    private void initView(View view) {
        rvRecord.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        PublicRedPackageAdapter mAdpater = new PublicRedPackageAdapter();
        rvRecord.setAdapter(mAdpater);

        ClientConfigure clientConfigure = DemoCache.readClientConfigure();
        if (clientConfigure != null){
            String lishi_show_text = clientConfigure.getLishi_show_text();
            tvRedPacketText.setText(lishi_show_text);
        }

        if (sendRedPackageInfo != null) {
            if (StringUtils.isNotEmpty(sendRedPackageInfo.getSenderAvatar())) {
                ImageLoadUtils.loadCircleImage(getContext(), sendRedPackageInfo.getSenderAvatar(), ivAvatar, R.drawable.nim_avatar_default);
            }
            String nick = sendRedPackageInfo.getSenderNick();
            if (StringUtils.isNotEmpty(nick)) {
                if (!TextUtils.isEmpty(nick) && nick.length() > 5) {
                    nick = nick.substring(0, 5) + "...";
                }
                tvNick.setText(nick+"的红包");
            }
            tvGoldNum.setText("我抢到"+sendRedPackageInfo.getGetCoin()+sendRedPackageInfo.getShowUnit());
            if(sendRedPackageInfo.getHisList() != null && sendRedPackageInfo.getHisList().size() > 0){
                for (ReceiveRedPackageInfo redPackageInfo: sendRedPackageInfo.getHisList()) {
                    redPackageInfo.setShowUnit(sendRedPackageInfo.getShowUnit());
                }
            }

            if (!ListUtils.isListEmpty(sendRedPackageInfo.getHisList())){
                mAdpater.setNewData(sendRedPackageInfo.getHisList());
            }else {
                //空布局
            }
        }
    }


    private void initListener() {
        ivClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
