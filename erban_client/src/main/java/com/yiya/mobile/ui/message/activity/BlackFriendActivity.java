package com.yiya.mobile.ui.message.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.message.fragment.FriendBlackFragment;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/22
 * 描述        黑名单
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class BlackFriendActivity extends BaseActivity {

    private AppToolBar mToolBar;

    public static void start(Context context) {
        Intent intent = new Intent(context, BlackFriendActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "消息-黑名单页");

        setContentView(R.layout.activity_black_friend);

        mToolBar = (AppToolBar) findViewById(R.id.toolbar);
        mToolBar.setOnBackBtnListener(view -> finish());
        mToolBar.getTvTitle().getPaint().setFakeBoldText(true);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, FriendBlackFragment.newInstance(),
                        FriendBlackFragment.class.getSimpleName()).commitAllowingStateLoss();
    }
}
