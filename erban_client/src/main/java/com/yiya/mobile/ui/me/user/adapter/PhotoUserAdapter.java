package com.yiya.mobile.ui.me.user.adapter;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/17.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PhotoUserAdapter extends BaseMultiItemQuickAdapter<UserPhoto, BaseViewHolder> {

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public PhotoUserAdapter(List<UserPhoto> data) {
        super(data);
        addItemType(0, R.layout.item_user_photo);
        addItemType(1, R.layout.item_user_photo_upload);
    }

    @Override
    protected void convert(BaseViewHolder helper, UserPhoto item) {
        if (item.getItemType() == UserPhoto.TYPE_PHOTO) {
            ImageLoadUtils.loadImage(mContext, item.getPhotoUrl(), helper.getView(R.id.iv_photo));
        }
    }

}
