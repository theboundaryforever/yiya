package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/30.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class YiYaRoomChatIndicatorAdapter extends CommonNavigatorAdapter {

    private Context mContext;
    private List<TabInfo> mTitleList;

    private int normalColorId = R.color.white_alpha_50;
    private int selectColorId = R.color.color_white;

    public YiYaRoomChatIndicatorAdapter(Context mContext, List<TabInfo> mTitleList) {
        this.mContext = mContext;
        this.mTitleList = mTitleList;
    }

    @Override
    public int getCount() {
        return mTitleList == null ? 0 : mTitleList.size();
    }

    @Override
    public IPagerTitleView getTitleView(Context context, int i) {
        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.layout_room_chat_indicator);

        ImageView ivIndicator = pagerTitleView.findViewById(R.id.iv_indicator);
        TextView tvTitle = pagerTitleView.findViewById(R.id.tv_title);

        tvTitle.setText(mTitleList.get(i).getName());
        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {
            @Override
            public void onSelected(int index, int totalCount) {
                ivIndicator.setVisibility(View.VISIBLE);
                tvTitle.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                tvTitle.setTextColor(ContextCompat.getColor(mContext, selectColorId));
            }

            @Override
            public void onDeselected(int index, int totalCount) {
                ivIndicator.setVisibility(View.INVISIBLE);
                tvTitle.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                tvTitle.setTextColor(ContextCompat.getColor(mContext, normalColorId));
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {

            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {

            }
        });
        pagerTitleView.findViewById(R.id.cl_indicator).setOnClickListener(view -> {
            if (mOnItemSelectListener != null) {
                mOnItemSelectListener.onItemSelect(i);
            }
        });
        return pagerTitleView;

    }

    @Override
    public IPagerIndicator getIndicator(Context context) {
        LinePagerIndicator indicator = new LinePagerIndicator(context);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        indicator.setColors(Color.TRANSPARENT);
        indicator.setLayoutParams(lp);
        return indicator;
    }

    private YiYaRoomChatIndicatorAdapter.OnItemSelectListener mOnItemSelectListener;

    public void setOnItemSelectListener(YiYaRoomChatIndicatorAdapter.OnItemSelectListener onItemSelectListener) {
        mOnItemSelectListener = onItemSelectListener;
    }

    public interface OnItemSelectListener {
        void onItemSelect(int position);
    }
}
