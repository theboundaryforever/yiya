package com.yiya.mobile.ui.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.jetbrains.annotations.NotNull;

/**
 * @author Zhangsongzhou
 * @date 2019/6/27
 */
public class LiveItemView extends FrameLayout {
    private Context mContext;

    public LiveItemView(Context context) {
        super(context);
        init(context);
    }

    public LiveItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    private FrameLayout mBgFl;
    private ImageView mBgIv;
    private ImageView mTopTagIv;
    private ImageView mTagIv;
    private TextView mLocalTv;
    private TextView mNameTv;
    private TextView mCountTv;
    private SVGAImageView svga_wave;

    private void init(Context context) {
        mContext = context;
        LayoutInflater.from(mContext).inflate(R.layout.view_live_item, this);

        mBgFl = findViewById(R.id.item_bg_fl);
        mBgIv = findViewById(R.id.avatar_bg_iv);
        mTopTagIv = findViewById(R.id.top_tag_iv);
        mTagIv = findViewById(R.id.tag_iv);
        mLocalTv = findViewById(R.id.local_tv);
        mNameTv = findViewById(R.id.name_tv);
        mCountTv = findViewById(R.id.count_tv);
        svga_wave = findViewById(R.id.svga_wave);
    }

    public void setLocalStr(String value) {
        mLocalTv.setText(value);
    }

    public void setNameStr(String value) {
        String tmp = value;
        if (!TextUtils.isEmpty(tmp) && tmp.length() > 7) {
            tmp = tmp.substring(0, 6) + "...";
        }
        mNameTv.setText(tmp);
    }

    public void setCountNum(int value) {
        mCountTv.setText(StringUtils.transformToW(value));
    }

    public ImageView getTopTagIv() {
        return mTopTagIv;
    }

    public ImageView getTagIv() {
        return mTagIv;
    }

    public ImageView getBgView() {
        return mBgIv;
    }

    public SVGAImageView getSvga(){
        return svga_wave;
    }

}
