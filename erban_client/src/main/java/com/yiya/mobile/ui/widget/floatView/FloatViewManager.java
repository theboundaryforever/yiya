package com.yiya.mobile.ui.widget.floatView;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.ViewCompat;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.yiya.mobile.room.avroom.widget.VideoGuestMicroView;


/**
 * @ClassName FloatViewManager
 * @Description 悬浮窗管理器
 * @Author Yunpeng Li
 * @Creation 2018/3/15 下午5:05
 * @Mender Yunpeng Li
 * @Modification 2018/3/15 下午5:05
 */
public class FloatViewManager {

    private VideoGuestMicroView mFloatView;
    private static volatile FloatViewManager mInstance;
    private ViewGroup mContainer;

    private FloatViewManager() {
    }

    public static FloatViewManager get() {
        if (mInstance == null) {
            synchronized (FloatViewManager.class) {
                if (mInstance == null) {
                    mInstance = new FloatViewManager();
                }
            }
        }
        return mInstance;
    }

    public void remove() {
        new Handler(Looper.getMainLooper()).post(() -> {
            if (mFloatView == null) {
                return;
            }
            if (ViewCompat.isAttachedToWindow(mFloatView) && mContainer != null) {
                mContainer.removeView(mFloatView);
            }
            mFloatView = null;
        });
    }

    private void addDragView(Context context) {
        synchronized (this) {
            if (mFloatView != null) {
                return;
            }
            mFloatView = new VideoGuestMicroView(context);
            mFloatView.setLayoutParams(mFloatView.getParams());
            addViewToWindow(mFloatView);
        }
    }

    public FloatViewManager add(Context context) {
        addDragView(context);
        return this;
    }

    public void attach(Activity activity) {
        attach(getActivityRoot(activity));
    }

    public void attach(ViewGroup container) {
        if (container == null || mFloatView == null) {
            mContainer = container;
            return;
        }
        if (mFloatView.getParent() == container) {
            return;
        }
        if (mContainer != null && mFloatView.getParent() == mContainer) {
            mContainer.removeView(mFloatView);
        }
        mContainer = container;
        container.addView(mFloatView);
    }

    public void detach(Activity activity) {
        detach(getActivityRoot(activity));
    }

    public void detach(ViewGroup container) {
        if (mFloatView != null && container != null && ViewCompat.isAttachedToWindow(mFloatView)) {
            container.removeView(mFloatView);
        }
        mContainer = null;
    }

    public VideoGuestMicroView getView() {
        return mFloatView;
    }

    private void addViewToWindow(final VideoGuestMicroView view) {
        if (mContainer == null) {
            return;
        }
        mContainer.addView(view);
    }

    private FrameLayout getActivityRoot(Activity activity) {
        if (activity == null) {
            return null;
        }
        try {
            return (FrameLayout) activity.getWindow().getDecorView().findViewById(android.R.id.content);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}