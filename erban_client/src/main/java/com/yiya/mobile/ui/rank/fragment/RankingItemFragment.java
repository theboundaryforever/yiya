package com.yiya.mobile.ui.rank.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.rankinglist.IRankingListView;
import com.yiya.mobile.presenter.rankinglist.RankingListPresenter;
import com.yiya.mobile.ui.rank.adapter.RankingListAdapter;
import com.yiya.mobile.utils.UIHelper;

import java.util.List;

import static com.yiya.mobile.ui.rank.activity.RankingListActivity.BUNDLE_KEY_DATA_TYPE;
import static com.yiya.mobile.ui.rank.activity.RankingListActivity.BUNDLE_KEY_QUERY_UID;
import static com.yiya.mobile.ui.rank.activity.RankingListActivity.BUNDLE_KEY_TYPE;
import static com.yiya.mobile.ui.rank.activity.RankingListActivity.BUNDLE_KEY_UID;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/26
 * 描述        对应排行榜
 * <p>
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
@CreatePresenter(RankingListPresenter.class)
public class RankingItemFragment extends BaseMvpFragment<IRankingListView, RankingListPresenter> implements IRankingListView {

    private SwipeRefreshLayout refreshLayout;
    private RankingListAdapter adapter;
    private RecyclerView recyclerView;
    private View headerView;

    private LinearLayout mEmptyContentLl;
    private TextView mEmptyContentTv;

    private int mType;
    private int mDataType;
    private String mUid;
    private String mQueryUid;


    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_ranking_item;
    }

    @Override
    public void onFindViews() {
        refreshLayout = mView.findViewById(R.id.refresh_layout);
        recyclerView = mView.findViewById(R.id.recycler_view);

        mEmptyContentLl = mView.findViewById(R.id.ranking_content_empty_ll);
        mEmptyContentTv = mView.findViewById(R.id.ranking_content_empty_tv);


        headerView = View.inflate(mContext, R.layout.layout_ranking_list_header_xc, null);
    }

    @Override
    protected void onLazyLoadData() {
        getData();
    }

    @Override
    public void onSetListener() {
        refreshLayout.setOnRefreshListener(() -> getData());
    }

    private void getData() {
        getMvpPresenter().getRankingList(mUid, mQueryUid, mType, mDataType);

    }


    @Override
    public void initiate() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RankingListAdapter(R.layout.item_ranking_list, mContext, mType);
        adapter.setOnItemClickListener((adapter, view, position) -> {
            RankingXCInfo.ListBean info = (RankingXCInfo.ListBean) adapter.getItem(position);
            if (info != null) {
                UIHelper.showUserInfoAct(getContext(), info.getCtrbUid());
            }
        });
        adapter.setHeaderView(headerView);
        adapter.setOnHeaderChildViewClickListener(view -> {
            Long uid = Long.valueOf(view.getTag().toString());
            UIHelper.showUserInfoAct(getContext(), uid);
        });
        recyclerView.setAdapter(adapter);

        if (!TextUtils.isEmpty(mUid) && !TextUtils.isEmpty(mQueryUid) && mUid.equals(mQueryUid)) {
            mEmptyContentTv.setText("还没有收到礼物");
        } else {
            mEmptyContentTv.setText("还没有人给Ta送礼");
        }
    }

    @Override
    public void setupFailView(String message) {
        toast(message);
        refreshLayout.setRefreshing(false);
        adapter.setNewData(null);
        mEmptyContentLl.setVisibility(View.VISIBLE);

    }

    @Override
    public void setupSuccessView(List<RankingXCInfo.ListBean> rankingList) {
        refreshLayout.setRefreshing(false);
        if (rankingList != null && rankingList.size() > 0) {
            recyclerView.post(() -> adapter.setNewData(rankingList));
            mEmptyContentLl.setVisibility(View.GONE);
        } else {
            mEmptyContentLl.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        if (bundle != null) {
            mType = bundle.getInt(BUNDLE_KEY_TYPE);
            mDataType = bundle.getInt(BUNDLE_KEY_DATA_TYPE);
            mUid = bundle.getString(BUNDLE_KEY_UID);
            mQueryUid = bundle.getString(BUNDLE_KEY_QUERY_UID);
        }
    }

    /**
     * @param type
     * @param dateType
     * @return
     * @deprecated
     */
    public static Fragment newInstance(int type, int dateType) {
        Bundle bundle = new Bundle();
        RankingItemFragment fragment = new RankingItemFragment();
        bundle.putInt("type", type);
        bundle.putInt("dateType", dateType);
        fragment.setArguments(bundle);
        return fragment;
    }


    public static Fragment newInstance(int type, int dateType, String uid, String queryUid) {
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_KEY_TYPE, type);
        bundle.putInt(BUNDLE_KEY_DATA_TYPE, dateType);
        bundle.putString(BUNDLE_KEY_UID, uid);
        bundle.putString(BUNDLE_KEY_QUERY_UID, queryUid);
        RankingItemFragment fragment = new RankingItemFragment();
        fragment.setArguments(bundle);
        return fragment;

    }
}
