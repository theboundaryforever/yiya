package com.yiya.mobile.ui.login.information.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

public interface InformationSupplmentView extends IMvpBaseView {

    /**获取头像*/
    String getHeadPortrait();

    /***获取昵称*/
    String getNickName();

    /**获取生日*/
    String getDate();

    /***获取性别*/
    String getSex();

}
