package com.yiya.mobile.ui.me.user.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.presenter.shopping.DressUpFragmentPresenter;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.home.view.HomeContainerLayout;
import com.yiya.mobile.ui.me.shopping.view.IDressUpFragmentView;
import com.yiya.mobile.ui.rank.activity.RankingListActivity;
import com.yiya.mobile.ui.widget.HeadWearImageView;
import com.yiya.mobile.ui.widget.ObservableScrollView;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCoreClient;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.RoomEvent;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.IRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.GiftWallInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.ColorUtil;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmList;

/**
 * 用户中心
 *
 * @author zhouxiangfeng
 * @date 2017/5/24
 */

@CreatePresenter(DressUpFragmentPresenter.class)
public class UserInfoActivity extends BaseMvpActivity<IDressUpFragmentView, DressUpFragmentPresenter> implements IDressUpFragmentView {

    private static final String TAG = "UserInfoActivity";

    public static void start(Context context, long userId) {
        Intent intent = new Intent(context, UserInfoActivity.class);
        intent.putExtra("userId", userId);
        context.startActivity(intent);
    }

    @BindView(R.id.home_container_cl)
    HomeContainerLayout mHomeContainerLayout;
    @BindView(R.id.observable_scroll_view)
    ObservableScrollView mObservableScrollView;

    @BindView(R.id.title_fl)
    FrameLayout mTitleFl;
    @BindView(R.id.title_tv)
    TextView mTitleTv;
    @BindView(R.id.iv_me_edit)
    ImageView ivMeEdit;

    @BindView(R.id.iv_avatar_bg)
    ImageView ivAvatarBg;
    //    @BindView(R.id.user_avatar_civ)
//    CircleImageView mUserAvatarCIv;
    @BindView(R.id.hw_iv)
    HeadWearImageView headWearImageView;
    @BindView(R.id.user_name_tv)
    TextView mUserNameTv;
    @BindView(R.id.user_id_tv)
    TextView mUserIdTv;
    @BindView(R.id.user_signature_tv)
    TextView mUserSignatureTv;
    @BindView(R.id.iv_gender)
    ImageView ivGender;
    @BindView(R.id.user_attention_num_tv)
    TextView mUserAttentionNumTv;

    @BindView(R.id.user_fans_num_tv)
    TextView mUserFansNumTv;

    @BindView(R.id.tv_gift_ranking_empty)
    TextView tvGiftRankingEmpty;

    @BindView(R.id.gift_ranking_recycler_view)
    RecyclerView mRankingRecyclerView;
    @BindView(R.id.photo_recyclerView)
    RecyclerView mPhotoRecyclerView;
    @BindView(R.id.photo_empty_tv)
    TextView mPhotoEmptyTv;
    @BindView(R.id.gift_wall_recycler_view)
    RecyclerView mGiftWallRecyclerView;
    @BindView(R.id.tv_gift_wall_content_empty)
    TextView tvGiftWallEmpty;

    @BindView(R.id.iv_user_level)
    ImageView ivUserLevel;
    @BindView(R.id.iv_exper_level)
    ImageView ivExperLevel;
    @BindView(R.id.iv_charm_level)
    ImageView ivCharmLevel;

    @BindView(R.id.iv_to_ta_room)
    ImageView ivToTaRoom;
    @BindView(R.id.iv_to_found_ta)
    ImageView ivToFoundTa;

    @BindView(R.id.iv_gift_ranking_more)
    ImageView icGiftRankingMore;

    @BindView(R.id.ll_follow)
    LinearLayout llFollow;
    @BindView(R.id.iv_follow)
    ImageView ivFollow;
    @BindView(R.id.tv_follow)
    TextView tvFollow;

    @BindView(R.id.ll_bottom)
    LinearLayout llBottom;

    private UserInfo mUserInfo;
    private RoomInfo mRoomInfo;

    private long mUserId;
    private boolean isBlacklist = false;
    private boolean isBothSides;
    private boolean inTgUserBlacklist = false;

    private int mHidColorY = 413;


    private GiftRankingAdapter mGiftRankingAdapter;
    private PhotoAdapter mPhotoAdapter;
    private List<UserPhoto> mUserPhotoList = new ArrayList<>();

    private GiftWallAdapter mGiftWallAdapter;

    private boolean isResume = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GrowingIO.getInstance().setPageName(this, "个人主页");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "用户资料页");
        setContentView(R.layout.activity_user_info_xc);
        ButterKnife.bind(this);

        initView();

        mUserId = getIntent().getLongExtra("userId", 0);
        mUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(mUserId, true);
        updateUI(mUserInfo);

        IMNetEaseManager.get().subscribeChatRoomEventObservable(roomEvent -> {
            if (roomEvent == null) {
                return;
            }
            int event = roomEvent.getEvent();
            switch (event) {
                case RoomEvent.KICK_OUT_ROOM:
                    if (roomEvent.getReason_no() == 3) {

                    }
                    break;
                default:
            }
        }, this);

        setListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResume = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isResume = false;
    }

    private void setListener() {
        ivToTaRoom.setOnClickListener(v -> {
            if (mUserInfo != null) {
                RoomFrameActivity.start(this, mUserInfo.getUid(), RoomInfo.ROOMTYPE_HOME_PARTY);
            }
        });

        ivToFoundTa.setOnClickListener(v -> {
            if (mRoomInfo != null) {
                getMvpPresenter().handleToFoundTa(UserInfoActivity.this, mUserId + "", mRoomInfo.getUid(), mRoomInfo.getType());
            }
        });

        icGiftRankingMore.setOnClickListener(v -> {
            if (mUserInfo != null) {
                RankingListActivity.start(this, CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", mUserInfo.getUid() + "");
            }
        });

        ivMeEdit.setOnClickListener(v -> {// 更改用户信息
            if (ivMeEdit.isSelected()) {
                UIHelper.showUserInfoModifyAct(UserInfoActivity.this, mUserId);
            } else {
                showTitleMoreDialog();
            }
        });
    }

    private void initView() {
        mHidColorY = DisplayUtils.dip2px(this, 130);
        mObservableScrollView.setScrollViewListener(new ObservableScrollView.ScrollViewListener() {
            @Override
            public void onScrollChanged(NestedScrollView view, int x, int y, int oldx, int oldy) {
                if (y <= mHidColorY + 2) {
                    float f = (float) (y) / (float) mHidColorY;
                    mTitleFl.setBackgroundColor(ColorUtil.changeColor(f));
                    mTitleTv.setText("");
                } else {
                    mTitleFl.setBackgroundColor(ContextCompat.getColor(UserInfoActivity.this, R.color.white));
                    if (mUserInfo != null) {
                        mTitleTv.setText(mUserInfo.getNick());
                    }
                }

            }
        });

        findViewById(R.id.arrow_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mRankingRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mGiftRankingAdapter = new GiftRankingAdapter(this, R.layout.item_user_info_gift_ranking);
        mRankingRecyclerView.setAdapter(mGiftRankingAdapter);

        mPhotoRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mPhotoAdapter = new PhotoAdapter(this, R.layout.item_user_info_photo_content);
        mPhotoRecyclerView.setAdapter(mPhotoAdapter);

        mGiftWallRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        mGiftWallAdapter = new GiftWallAdapter(this, R.layout.item_user_info_gift_wall);
        mGiftWallRecyclerView.setAdapter(mGiftWallAdapter);
    }

    private void updateUI(UserInfo userInfo) {
        if (userInfo == null) {
            return;
        }

        //头像与头饰
        headWearImageView.setAvatar(userInfo.getAvatar());
        headWearImageView.setHeadWear(userInfo.getHeadwearUrl());

        mUserNameTv.setText(userInfo.getNick());
        mUserIdTv.setText("ID:" + userInfo.getErbanNo());
        if (userInfo.getGender() == 1) {
            ivGender.setImageResource(R.mipmap.ic_ranking_male);
        } else {
            ivGender.setImageResource(R.mipmap.ic_ranking_female);
        }

        //关注数
        String followNum = String.valueOf(userInfo.getFollowNum());
        SpannableStringBuilder ssbFollow = new SpannableStringBuilder(String.format("关注:  %s", followNum));
        ssbFollow.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.color_383950)), ssbFollow.length() - followNum.length(), ssbFollow.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssbFollow.setSpan(new AbsoluteSizeSpan(22, true), ssbFollow.length() - followNum.length(), ssbFollow.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mUserAttentionNumTv.setText(ssbFollow);

        //粉丝数
        String fansNum = String.valueOf(userInfo.getFansNum());
        SpannableStringBuilder ssbFans = new SpannableStringBuilder(String.format("粉丝:  %s", fansNum));
        ssbFans.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.color_383950)), ssbFans.length() - fansNum.length(), ssbFans.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssbFans.setSpan(new AbsoluteSizeSpan(22, true), ssbFans.length() - fansNum.length(), ssbFans.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mUserFansNumTv.setText(ssbFans);

        //个性签名
        if (!TextUtils.isEmpty(userInfo.getUserDesc())) {
            mUserSignatureTv.setText(userInfo.getUserDesc());
        }

        //如果是自己
        if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == mUserId) {
            llBottom.setVisibility(View.GONE);
            ivToFoundTa.setVisibility(View.GONE);
            ivToTaRoom.setVisibility(View.GONE);
            ivAvatarBg.setVisibility(View.GONE);
            ivMeEdit.setSelected(true);
        } else {
            ivToFoundTa.setVisibility(View.VISIBLE);
            ivToTaRoom.setVisibility(View.VISIBLE);
            llBottom.setVisibility(View.VISIBLE);
            ivAvatarBg.setVisibility(View.VISIBLE);
            ivMeEdit.setSelected(false);
            checkBlacklist();
        }

        if (CoreManager.getCore(IAuthCore.class).getCurrentUid() != userInfo.getUid()) {
            CoreManager.getCore(IPraiseCore.class).isPraised(CoreManager.getCore(IAuthCore.class).getCurrentUid(), userInfo.getUid());
        }

        //用户等级
        if(ivUserLevel != null){
            if (StringUtils.isNotEmpty(userInfo.getVideoRoomExperLevelPic())) {
                ivUserLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(getBaseContext(), userInfo.getVideoRoomExperLevelPic(), ivUserLevel);
            } else {
                ivUserLevel.setVisibility(View.GONE);
            }
        }
        // 财富等级
        if(ivExperLevel != null){
            if (StringUtils.isNotEmpty(userInfo.getExperLevelPic())) {
                ivExperLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(getBaseContext(), userInfo.getExperLevelPic(), ivExperLevel);
            } else {
                ivExperLevel.setVisibility(View.GONE);
            }
        }

        // 魅力等级
        if(ivCharmLevel != null){
            if (StringUtils.isNotEmpty(userInfo.getCharmLevelPic())) {
                ivCharmLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(getBaseContext(), userInfo.getCharmLevelPic(), ivCharmLevel);
            } else {
                ivCharmLevel.setVisibility(View.GONE);
            }
        }

        requestRoomInfo(userInfo);
        CoreManager.getCore(IUserCore.class).requestUserGiftWall(mUserId, 2);
        getPhotoList(userInfo);
        getGiftRankingList(userInfo);
    }

    /**
     * 获取礼物贡献榜onGiftRankingListSucceed
     */
    private void getGiftRankingList(UserInfo userInfo) {
        getMvpPresenter().getGiftRankingList(2, userInfo.getUid() + "");
    }

    /**
     * 检查是否在黑名单
     */
    private void checkBlacklist() {
        getMvpPresenter().checkBlackListByUid(mUserId + "");
    }

    /***
     *获取个人相册
     */
    private void getPhotoList(UserInfo userInfo) {

        mUserPhotoList.clear();
        if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == mUserId) {
            mUserPhotoList.add(new UserPhoto(-2, ""));
        }

        RealmList<UserPhoto> tmpList = userInfo.getPrivatePhoto();
        mUserPhotoList.addAll(tmpList);

        if (ListUtils.isListEmpty(mUserPhotoList)) {
            mPhotoEmptyTv.setVisibility(View.VISIBLE);
        } else {
            mPhotoEmptyTv.setVisibility(View.GONE);
        }

        mPhotoAdapter.setNewData(mUserPhotoList);
    }

    private void refreshRoomState(RoomInfo roomInfo) {
        if (roomInfo == null) {
            //该用户还未开房间
            ivToFoundTa.setImageResource(R.mipmap.ic_to_find_ta_die);
            ivToFoundTa.setClickable(false);
            return;
        } else {
            ivToFoundTa.setImageResource(R.mipmap.ic_to_find_ta_live);
            ivToFoundTa.setClickable(true);
        }

        if (mRoomInfo != null && mRoomInfo.getRoomId() == roomInfo.getRoomId()) {
            mRoomInfo = roomInfo;
        } else if (mRoomInfo == null) {
            //打开新的activity的时候
            mRoomInfo = roomInfo;
        }
    }

    private void requestRoomInfo(UserInfo userInfo) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", mUserId + "");
        OkHttpManager.getInstance().getRequest(UriProvider.getUserRoomInfo(), param, new OkHttpManager.MyCallBack<ServiceResult<RoomInfo>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<RoomInfo> response) {
                if (response != null && response.isSuccess()) {
                    RoomInfo roomInfo = response.getData();
                    refreshRoomState(roomInfo);
                }
            }
        });
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestGiftWall(List<GiftWallInfo> giftWallInfoList) {

        List<GiftWallInfo> tmp = new ArrayList<>();

        if (giftWallInfoList != null && giftWallInfoList.size() >= 0) {
            tmp.addAll(giftWallInfoList);
        }
        if (tmp.size() > 0) {
            tvGiftWallEmpty.setVisibility(View.GONE);
        } else {
            tvGiftWallEmpty.setVisibility(View.VISIBLE);
        }
        mGiftWallAdapter.setNewData(tmp);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestGiftWallFail(String msg) {
        if (getDialogManager().isDialogShowing()) {
            getDialogManager().dismissDialog();
        }
        tvGiftWallEmpty.setVisibility(View.VISIBLE);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onRequestUserInfo(UserInfo info) {
        if (info.getUid() == mUserId) {
            mUserInfo = info;
            updateUI(mUserInfo);
        }
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        if (info.getUid() == mUserId) {
            mUserInfo = info;
            updateUI(mUserInfo);
        }
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void removeBlackListSuccess() {
        toast("已取消拉黑");
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void addBlackListSuccess() {
        toast("已拉黑");
    }

    private void updateAttention(boolean isAlready) {
        llFollow.setSelected(isAlready);
        if (isAlready) {
            ivFollow.setImageResource(R.mipmap.ic_me_has_follow);
            tvFollow.setTextColor(getResources().getColor(R.color.color_A8A8A8));
            tvFollow.setText("已关注");
        } else {
            ivFollow.setImageResource(R.mipmap.ic_me_follow);
            tvFollow.setTextColor(getResources().getColor(R.color.color_383950));
            tvFollow.setText("关注");
        }
    }


    @CoreEvent(coreClientClass = IRoomCoreClient.class)
    public void onGetUserRoomFail(String msg) {
        getDialogManager().dismissDialog();
        toast(msg);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(boolean islike, long uid) {
        updateAttention(islike);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLikedFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        getDialogManager().dismissDialog();

        updateAttention(true);
        if (isResume) {//泛滥调用
            toast("关注成功，相互关注可成为好友哦！");
        }

    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid, boolean showNotice) {
        if (showNotice) {
            toast("取消关注成功");
        }
        getDialogManager().dismissDialog();
        updateAttention(false);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraiseFaith(String error) {
        toast(error);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }


    /**
     * @param view
     */
    @OnClick({R.id.ll_follow, R.id.ll_chat})
    void onClickOthers(View view) {
        switch (view.getId()) {
            case R.id.ll_follow:    //关注
                if (mUserInfo == null) {
                    return;
                }

                if (llFollow.isSelected()) {// 已关注
                    boolean isMyFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(mUserInfo.getUid() + "");
                    String tip;
                    if (isMyFriend) {
                        tip = "取消关注将不再是好友关系，确定取消关注？";
                    } else {
                        tip = "确定取消关注？";
                    }
                    getDialogManager().showOkCancelDialog(tip, true, new DialogManager.OkCancelDialogListener() {
                        @Override
                        public void onCancel() {
                            getDialogManager().dismissDialog();
                        }

                        @Override
                        public void onOk() {
                            getDialogManager().dismissDialog();
                            getDialogManager().showProgressDialog(UserInfoActivity.this, "请稍后...");
                            CoreManager.getCore(IPraiseCore.class).cancelPraise(mUserInfo.getUid(), true);

                        }
                    });
                } else {
                    getDialogManager().showProgressDialog(UserInfoActivity.this, "请稍后...");
                    CoreManager.getCore(IPraiseCore.class).praise(mUserInfo.getUid());
                }

                break;
            case R.id.ll_chat://私聊
                if (mUserInfo == null) {
                    return;
                }
                boolean isFriends = CoreManager.getCore(IIMFriendCore.class).isMyFriend(mUserInfo.getUid() + "");
                getMvpPresenter().handlePrivateChat(this, isFriends, mUserId);
                break;
//            case R.id.title_more_fl://标题栏更多信息
//                showTitleMoreDialog();
//                break;
        }
    }

    private void showTitleMoreDialog() {
        String blacklistStr = isBlacklist ? "移除黑名单" : "拉黑";
        ButtonItem blackListItem = new ButtonItem(blacklistStr, new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                if (isBlacklist) {
                    getMvpPresenter().removeBlacklistByUid(mUserId + "");
                } else {
                    getMvpPresenter().addBlackListByUid(mUserId + "", 1);
                }

            }
        });
        ButtonItem informItem = new ButtonItem("举报", new ButtonItem.OnClickListener() {
            @Override
            public void onClick() {
                showInformDialog();
            }
        });
        List<ButtonItem> contentList = new ArrayList<>();
        contentList.add(blackListItem);
        contentList.add(informItem);
        getDialogManager().showCommonPopupDialog(contentList, "取消", true);
    }

    /**
     * 显示举报内容的dialog
     */
    private void showInformDialog() {
        ButtonItem buttonItem01 = new ButtonItem("政治敏感", new InformDialogItemClickImpl(1));
        ButtonItem buttonItem02 = new ButtonItem("色情低俗", new InformDialogItemClickImpl(2));
        ButtonItem buttonItem03 = new ButtonItem("广告骚扰", new InformDialogItemClickImpl(3));
        ButtonItem buttonItem04 = new ButtonItem("人身攻击", new InformDialogItemClickImpl(4));

        List<ButtonItem> contentList = new ArrayList<>();
        contentList.add(buttonItem01);
        contentList.add(buttonItem02);
        contentList.add(buttonItem03);
        contentList.add(buttonItem04);
        getDialogManager().showCommonPopupDialog(contentList, "取消", true);
    }

    /**
     * 检查是否黑名单
     *
     * @param checkBothSides 在自己的黑名单 or  在对方的黑名单
     * @param inBlacklist    在自己的黑名单
     */
    @Override
    public void onCheckBlacklist(boolean checkBothSides, boolean inBlacklist, boolean inTgUserBlacklist) {
        isBlacklist = inBlacklist;
        isBothSides = checkBothSides;
        this.inTgUserBlacklist = inTgUserBlacklist;
    }

    @Override
    public void onCheckBlacklistFailed(String message) {
        toast(message);
    }

    /**
     * 添加进黑名单
     */
    @Override
    public void onAddBlacklist() {
        toast("已拉黑");
        checkBlacklist();
    }

    @Override
    public void onAddBlacklistFailed(String message) {
        toast(message);
    }

    /**
     * 移除黑名单
     */
    @Override
    public void onRemoveBlacklist() {
        checkBlacklist();
    }

    @Override
    public void onRemoveBlacklistFailed(String message) {
        toast(message);
    }

    /**
     * 礼物贡献榜
     *
     * @param listBeans
     */
    @Override
    public void onGiftRankingListSucceed(List<RankingXCInfo.ListBean> listBeans) {
        if (ListUtils.isListEmpty(listBeans)) {
            mRankingRecyclerView.setVisibility(View.GONE);
            icGiftRankingMore.setVisibility(View.GONE);
            tvGiftRankingEmpty.setVisibility(View.VISIBLE);
        } else {
            mGiftRankingAdapter.setNewData(listBeans);

            mRankingRecyclerView.setVisibility(View.VISIBLE);
            icGiftRankingMore.setVisibility(View.VISIBLE);
            tvGiftRankingEmpty.setVisibility(View.GONE);
        }
    }

    /**
     * 礼物贡献榜
     *
     * @param message
     */
    @Override
    public void onGiftRankingListFailed(String message) {
        tvGiftRankingEmpty.setVisibility(View.VISIBLE);
    }

    private class InformDialogItemClickImpl implements ButtonItem.OnClickListener {
        private int mReportType;

        public InformDialogItemClickImpl(int reportType) {
            mReportType = reportType;
        }

        @Override
        public void onClick() {
            getMvpPresenter().commitReport(1, mUserId, mReportType);
        }
    }


    /**
     * 礼物贡献榜
     */
    private class GiftRankingAdapter extends BaseQuickAdapter<RankingXCInfo.ListBean, BaseViewHolder> {

        private Context mContext;

        public GiftRankingAdapter(Context context, int layoutId) {
            this(layoutId);
            mContext = context;
        }

        public GiftRankingAdapter(int layoutResId) {
            super(layoutResId);
        }

        @Override
        protected void convert(BaseViewHolder helper, RankingXCInfo.ListBean item) {
            ImageView rankingIv = helper.getView(R.id.item_avatar_ranking_iv);

            if (item.getRankingAvatarId() == -1) {
                rankingIv.setVisibility(View.INVISIBLE);
            } else {
                rankingIv.setImageResource(item.getRankingAvatarId());
                rankingIv.setVisibility(View.VISIBLE);
            }

            ImageLoadUtils.loadImage(mContext, item.getAvatar(), helper.getView(R.id.item_avatar_civ), R.drawable.icon_ranking_default);
        }
    }

    /**
     * 个人相册
     */
    private class PhotoAdapter extends BaseQuickAdapter<UserPhoto, BaseViewHolder> {
        private Context mContext;

        public PhotoAdapter(int layoutId) {
            super(layoutId);
        }

        public PhotoAdapter(Context context, int layoutId) {
            this(layoutId);
            mContext = context;
        }

        private Json getPhotoDataJson(ArrayList<UserPhoto> photos) {
            if (photos == null) {
                return null;
            }
            Json json = new Json();
            for (int i = 0; i < photos.size(); i++) {
                UserPhoto userPhoto = photos.get(i);
                Json j = new Json();
                j.set("pid", userPhoto.getPid());
                j.set("photoUrl", userPhoto.getPhotoUrl());
                json.set(i + "", j.toString());
            }
            return json;
        }

        @Override
        protected void convert(BaseViewHolder helper, UserPhoto item) {
            ImageView avatarIv = helper.getView(R.id.item_photo_avatar_iv);
            if (item.getPid() < 0 && TextUtils.isEmpty(item.getPhotoUrl())) {
                avatarIv.setImageResource(R.drawable.icon_user_info_photo_add);
//                ImageLoadUtils.loadImage(application, R.drawable.icon_user_info_photo_add, avatarIv);
            } else {
                ImageLoadUtils.loadImage(mContext, item.getPhotoUrl(), avatarIv);
            }
            avatarIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (item.getPid() < 0 && TextUtils.isEmpty(item.getPhotoUrl())) {
                        UIHelper.showModifyPhotosAct(UserInfoActivity.this, mUserId);
                    } else {
                        ArrayList<UserPhoto> tmpList = new ArrayList<>();
                        int pos = helper.getAdapterPosition();
                        tmpList.addAll(getData());
                        try {
                            if (tmpList.get(0).getPid() < 0 && TextUtils.isEmpty(tmpList.get(0).getPhotoUrl())) {
                                tmpList.remove(0);
                                pos--;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Json json = getPhotoDataJson(tmpList);
                        if (json != null) {
                            Intent intent = new Intent(UserInfoActivity.this, ShowPhotoActivity.class);
                            intent.putExtra("position", pos);
                            intent.putExtra("photoJsonData", json.toString());
                            startActivity(intent);
                        }
                    }
                }
            });
        }
    }

    private class GiftWallAdapter extends BaseQuickAdapter<GiftWallInfo, BaseViewHolder> {

        private Context mContext;

        public GiftWallAdapter(Context context, int layoutId) {
            this(layoutId);
            mContext = context;
        }

        public GiftWallAdapter(int layoutResId) {
            super(layoutResId);
        }

        @Override
        protected void convert(BaseViewHolder helper, GiftWallInfo item) {
            ImageLoadUtils.loadImage(mContext, item.getPicUrl(), helper.getView(R.id.item_pic_iv));
            TextView giftNameTv = helper.getView(R.id.gift_name_tv);
            giftNameTv.setText(item.getGiftName());
            TextView giftNumTv = helper.getView(R.id.gift_num_tv);
//            giftNumTv.setText(item.getReciveCount() + "");
            giftNumTv.setText(getString(R.string.userinfo_gift_wall, item.getReciveCount()));
        }
    }

}