package com.yiya.mobile.ui.find.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.HomeRoom;

/**
 * 创建者      Created by dell
 * 创建时间    2018/12/6
 * 描述        遇见界面item
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class MeetYouAdapter extends BaseQuickAdapter<HomeRoom, BaseViewHolder> {

    public MeetYouAdapter() {
        super(R.layout.fragment_meet_you_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeRoom item) {
        helper.setText(R.id.nickName, item.getNick())
                .setText(R.id.erban_no, "ID:" + item.getErbanNo())
                .setText(R.id.count, item.getOnlineNum() + "人");

        ImageView avatar = helper.getView(R.id.avatar);
        ImageLoadUtils.loadCircleImage(avatar.getContext(), item.getAvatar(), avatar, R.drawable.nim_avatar_default);

        ImageView icMeet = helper.getView(R.id.ic_meet);
        ImageLoadUtils.loadGifImage(icMeet.getContext(), R.drawable.ic_meet_you, icMeet);
    }
}
