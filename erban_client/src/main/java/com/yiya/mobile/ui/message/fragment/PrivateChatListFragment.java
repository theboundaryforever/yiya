package com.yiya.mobile.ui.message.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.cache.FriendDataCache;
import com.netease.nim.uikit.common.badger.Badger;
import com.netease.nim.uikit.common.ui.dialog.CustomAlertDialog;
import com.netease.nim.uikit.recent.RecentContactsCallback;
import com.netease.nim.uikit.recent.TeamMemberAitHelper;
import com.netease.nim.uikit.uinfo.UserInfoHelper;
import com.netease.nim.uikit.uinfo.UserInfoObservable;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.ResponseCode;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.msg.model.QueryDirectionEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.AccountInfo;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.message.PrivateChatListPresenter;
import com.yiya.mobile.presenter.message.PrivateChatListView;
import com.yiya.mobile.ui.message.adapter.PrivateChatListAdapter;
import com.yiya.mobile.ui.widget.emptyView.DefaultEmptyEnum;
import com.yiya.mobile.ui.widget.emptyView.DefaultEmptyView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

/**
 * @author Zhangsongzhou
 * @date 2019/5/20
 */
@CreatePresenter(PrivateChatListPresenter.class)
public class PrivateChatListFragment extends BaseMvpFragment<PrivateChatListView, PrivateChatListPresenter> implements PrivateChatListView {

    // 置顶功能可直接使用，也可作为思路，供开发者充分利用RecentContact的tag字段
    public static final long RECENT_TAG_STICKY = 1; // 联系人置顶tag
    private static Comparator<RecentContact> comp = (o1, o2) -> {
        // 先比较置顶tag
        long sticky = (o1.getTag() & RECENT_TAG_STICKY) - (o2.getTag() & RECENT_TAG_STICKY);
        if (sticky != 0) {
            return sticky > 0 ? -1 : 1;
        } else {
            long time = o1.getTime() - o2.getTime();
            return time == 0 ? 0 : (time > 0 ? -1 : 1);
        }
    };
    // view
    private RecyclerView recyclerView;
    //    private View emptyBg;
//    private TextView emptyHint;
    // data
    private List<RecentContact> items;
    private PrivateChatListAdapter privateChatListAdapter;
    Observer<IMMessage> statusObserver = new Observer<IMMessage>() {
        @Override
        public void onEvent(IMMessage message) {
            int index = getItemIndex(message.getUuid());
            if (index >= 0 && index < items.size()) {
                RecentContact item = items.get(index);
                item.setMsgStatus(message.getStatus());
                refreshViewHolderByIndex(index);
            }
        }
    };
    private RecentContactsCallback callback;
    Observer<RecentContact> deleteObserver = new Observer<RecentContact>() {
        @Override
        public void onEvent(RecentContact recentContact) {
            if (recentContact != null) {
                for (RecentContact item : items) {
                    if (TextUtils.equals(item.getContactId(), recentContact.getContactId())
                            && item.getSessionType() == recentContact.getSessionType()) {
                        items.remove(item);
                        refreshMessages(true);
                        break;
                    }
                }
            } else {
                items.clear();
                refreshMessages(true);
            }
        }
    };
    FriendDataCache.FriendDataChangedObserver friendDataChangedObserver = new FriendDataCache.FriendDataChangedObserver() {
        @Override
        public void onAddedOrUpdatedFriends(List<String> accounts) {
            refreshMessages(false);
        }

        @Override
        public void onDeletedFriends(List<String> accounts) {
            refreshMessages(false);
        }

        @Override
        public void onAddUserToBlackList(List<String> account) {
            refreshMessages(false);
        }

        @Override
        public void onRemoveUserFromBlackList(List<String> account) {
            refreshMessages(false);
        }
    };
    private UserInfoObservable.UserInfoObserver userInfoObserver;
    private List<RecentContact> loadedRecents;
    // 暂存消息，当RecentContact 监听回来时使用，结束后清掉
    private Map<String, Set<IMMessage>> cacheMessages = new HashMap<>();
    Observer<List<RecentContact>> messageObserver = (Observer<List<RecentContact>>) this::onRecentContactChanged;
    //监听在线消息中是否有@我
    private Observer<List<IMMessage>> messageReceiverObserver = (Observer<List<IMMessage>>) imMessages -> {

        if (imMessages != null) {
            for (IMMessage imMessage : imMessages) {

                if (!TeamMemberAitHelper.isAitMessage(imMessage)) {
                    continue;
                }
                Set<IMMessage> cacheMessageSet = cacheMessages.get(imMessage.getSessionId());
                if (cacheMessageSet == null) {
                    cacheMessageSet = new HashSet<>();
                    cacheMessages.put(imMessage.getSessionId(), cacheMessageSet);
                }
                cacheMessageSet.add(imMessage);
            }
        }
    };

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        findViews();
        initMessageList();
        requestMessages(true);
        registerObservers(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        registerObservers(false);
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.nim_recent_contacts;
    }

    /**
     * ********************** 收消息，处理状态变化 ************************
     */
    private void registerObservers(boolean register) {
        MsgServiceObserve service = NIMClient.getService(MsgServiceObserve.class);
        service.observeReceiveMessage(messageReceiverObserver, register);
        service.observeRecentContact(messageObserver, register);
        service.observeMsgStatus(statusObserver, register);
        service.observeRecentContactDeleted(deleteObserver, register);

        FriendDataCache.getInstance().registerFriendDataChangedObserver(friendDataChangedObserver, register);
        if (register) {
            registerUserInfoObserver();
        } else {
            unregisterUserInfoObserver();
        }
    }

    private void registerUserInfoObserver() {
        if (userInfoObserver == null) {
            userInfoObserver = accounts -> refreshMessages(false);
        }

        UserInfoHelper.registerObserver(userInfoObserver);
    }

    private void unregisterUserInfoObserver() {
        if (userInfoObserver != null) {
            UserInfoHelper.unregisterObserver(userInfoObserver);
        }
    }

    @CoreEvent(coreClientClass = IAuthClient.class)
    public void onLogin(AccountInfo accountInfo) {
        onLoginSuccess();
    }

    public void onLoginSuccess() {
        requestMessages(true);
    }

    public void requestMessages(boolean delay) {
      /*  if (msgLoaded) {
            return;
        }*/
        addDisposable(Observable.timer(delay ? 250 : 0, TimeUnit.MILLISECONDS).subscribe(aLong -> {
             /* if (msgLoaded) {
                    return;
                }*/
            // 查询最近联系人列表数据
            NIMClient.getService(MsgService.class).queryRecentContacts().setCallback(new RequestCallbackWrapper<List<RecentContact>>() {

                @Override
                public void onResult(int code, List<RecentContact> recents, Throwable exception) {
                    if (code != ResponseCode.RES_SUCCESS || recents == null) {
                        return;
                    }
                    loadedRecents = recents;
                    // 初次加载，更新离线的消息中是否有@我的消息
                    for (RecentContact loadedRecent : loadedRecents) {
                        if (loadedRecent.getSessionType() == SessionTypeEnum.Team) {
                            updateOfflineContactAited(loadedRecent);
                        }
                    }
                    // 此处如果是界面刚初始化，为了防止界面卡顿，可先在后台把需要显示的用户资料和群组资料在后台加载好，然后再刷新界面
                    //
//                        msgLoaded = true;
                    if (isAdded()) {
                        onRecentContactsLoaded();
                    }
                }
            });
        }));
    }

    private void updateOfflineContactAited(final RecentContact recentContact) {
        if (recentContact == null || recentContact.getSessionType() != SessionTypeEnum.Team
                || recentContact.getUnreadCount() <= 0) {
            return;
        }

        // 锚点
        List<String> uuid = new ArrayList<>(1);
        uuid.add(recentContact.getRecentMessageId());

        List<IMMessage> messages = NIMClient.getService(MsgService.class).queryMessageListByUuidBlock(uuid);

        if (messages == null || messages.size() < 1) {
            return;
        }
        final IMMessage anchor = messages.get(0);

        // 查未读消息
        NIMClient.getService(MsgService.class).queryMessageListEx(anchor, QueryDirectionEnum.QUERY_OLD,
                recentContact.getUnreadCount() - 1, false).setCallback(new RequestCallbackWrapper<List<IMMessage>>() {

            @Override
            public void onResult(int code, List<IMMessage> result, Throwable exception) {
                if (code == ResponseCode.RES_SUCCESS && result != null) {
                    result.add(0, anchor);
                    Set<IMMessage> messages = null;
                    // 过滤存在的@我的消息
                    for (IMMessage msg : result) {
                        if (TeamMemberAitHelper.isAitMessage(msg)) {
                            if (messages == null) {
                                messages = new HashSet<>();
                            }
                            messages.add(msg);
                        }
                    }

                    // 更新并展示
                    if (messages != null) {
                        TeamMemberAitHelper.setRecentContactAited(recentContact, messages);
                        notifyDataSetChanged();
                    }
                }
            }
        });

    }

    private void onRecentContactsLoaded() {
        items.clear();
        if (loadedRecents != null) {
            items.addAll(loadedRecents);
            loadedRecents = null;
        }
        refreshMessages(true);

        if (callback != null) {
            callback.onRecentContactsLoaded();
        }
    }

    private void notifyDataSetChanged() {
        privateChatListAdapter.notifyDataSetChanged();
//        boolean empty = items.isEmpty()/* && msgLoaded*/;
//        emptyBg.setVisibility(empty ? View.VISIBLE : View.GONE);
//        emptyHint.setHint("很遗憾，暂无私信噢~");
    }

    private void refreshMessages(boolean unreadChanged) {
        sortRecentContacts(items);
        notifyDataSetChanged();

        if (unreadChanged) {

            // 方式一：累加每个最近联系人的未读（快）

            int unreadNum = 0;
            for (RecentContact r : items) {
                unreadNum += r.getUnreadCount();
            }

            // 方式二：直接从SDK读取（相对慢）
            //int unreadNum = NIMClient.getService(MsgService.class).getTotalUnreadCount();

            if (callback != null) {
                callback.onUnreadCountChange(unreadNum);
            }

            Badger.updateBadgerCount(unreadNum);
        }
    }

    /**
     * **************************** 排序 ***********************************
     */
    private void sortRecentContacts(List<RecentContact> list) {
        if (list.size() == 0) {
            return;
        }
        Collections.sort(list, comp);
    }

    /**
     * 查找页面控件
     */
    private void findViews() {
        recyclerView = mView.findViewById(R.id.recycler_view);
//        emptyBg = mView.findViewById(R.id.emptyBg);
//        emptyHint = mView.findViewById(R.id.message_list_empty_hint);
    }

    /**
     * 初始化消息列表
     */
    private void initMessageList() {

        // PrivateChatListAdapter
        privateChatListAdapter = new PrivateChatListAdapter(items = new ArrayList<>());
        privateChatListAdapter.setEmptyView(new DefaultEmptyView.Builder(DefaultEmptyEnum.EMPTY_MESSAGE).build(getContext()));
        privateChatListAdapter.setOnItemClickListener((adapter, view, position) -> {
            RecentContact recentContact = (RecentContact) adapter.getData().get(position);
            if (recentContact != null) {
                if (NimUIKit.isRobotUid(recentContact.getContactId())) {
                    start2Chat(recentContact);
                } else {
                    //用户
                    getDialogManager().showProgressDialog(getContext());
                    getMvpPresenter().checkBlackList(recentContact.getContactId(), recentContact);
                }
            }
        });
        privateChatListAdapter.setOnItemLongClickListener((adapter, view, position) -> {
            RecentContact recentContact = (RecentContact) adapter.getData().get(position);
            if (recentContact != null) {
                showLongClickMenu(recentContact, position);
            }
            return false;
        });

        // recyclerView
        recyclerView.setAdapter(privateChatListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
    }

    private void start2Chat(RecentContact recent) {
        if (recent.getSessionType() == SessionTypeEnum.Team) {
            NimUIKit.startTeamSession(getActivity(), recent.getContactId());
        } else if (recent.getSessionType() == SessionTypeEnum.P2P) {
            NimUIKit.startP2PSession(getActivity(), recent.getContactId());
        }
    }

    @Override
    public void onCheckBlacklist(boolean value, RecentContact recent) {
        if (value) {
            toast("已拉黑，无法聊天");
        } else {
            if (recent != null) {
                start2Chat(recent);
            }
        }
    }

    @Override
    public void onCheckBlacklistFailed(String message) {
        if (!TextUtils.isEmpty(message)) {
            toast(message);
        }
    }

    private void showLongClickMenu(final RecentContact recent, final int position) {
        final CustomAlertDialog alertDialog = new CustomAlertDialog(getActivity());
        alertDialog.setTitle(UserInfoHelper.getUserTitleName(recent.getContactId(), recent.getSessionType()));
        String title = getString(R.string.main_msg_list_delete_chatting);
        alertDialog.addItem(title, () -> {
            // 删除会话，删除后，消息历史被一起删除
            NIMClient.getService(MsgService.class).deleteRecentContact(recent);
            NIMClient.getService(MsgService.class).clearChattingHistory(recent.getContactId(), recent.getSessionType());
            //java.lang.IndexOutOfBoundsException
            //Invalid index 4, size is 4
            //1 java.util.ArrayList.throwIndexOutOfBoundsException(ArrayList.java:260)
            //2 java.util.ArrayList.get(ArrayList.java:313)
            //3 com.netease.nim.uikit.common.ui.recyclerview.privateChatListAdapter.BaseQuickAdapter.remove(BaseQuickAdapter.java:292)
            //4 com.netease.nim.uikit.recent.RecentContactsFragment$5.onClick(RecentContactsFragment.java:253)
            //5 com.netease.nim.uikit.common.ui.dialog.CustomAlertDialog$2.onItemClick(CustomAlertDialog.java:95)
            if (privateChatListAdapter != null && !ListUtils.isListEmpty(privateChatListAdapter.getData()) && privateChatListAdapter.getData().size() > position)
                privateChatListAdapter.remove(position);
            if (isAdded()) {
                refreshMessages(true);
            }
        });

        title = (isTagSet(recent, RECENT_TAG_STICKY) ? getString(R.string.main_msg_list_clear_sticky_on_top) : getString(R.string.main_msg_list_sticky_on_top));
        alertDialog.addItem(title, () -> {
            if (isTagSet(recent, RECENT_TAG_STICKY)) {
                removeTag(recent, RECENT_TAG_STICKY);
            } else {
                addTag(recent, RECENT_TAG_STICKY);
            }
            NIMClient.getService(MsgService.class).updateRecent(recent);

            refreshMessages(false);
        });
        alertDialog.show();
    }

    private void addTag(RecentContact recent, long tag) {
        tag = recent.getTag() | tag;
        recent.setTag(tag);
    }

    private void removeTag(RecentContact recent, long tag) {
        tag = recent.getTag() & ~tag;
        recent.setTag(tag);
    }

    private boolean isTagSet(RecentContact recent, long tag) {
        return (recent.getTag() & tag) == tag;
    }

    private void onRecentContactChanged(List<RecentContact> recentContacts) {
        int index;
        for (RecentContact r : recentContacts) {
            index = -1;
            for (int i = 0; i < items.size(); i++) {
                if (r.getContactId().equals(items.get(i).getContactId())
                        && r.getSessionType() == (items.get(i).getSessionType())) {
                    index = i;
                    break;
                }
            }

            if (index >= 0) {
                items.remove(index);
            }

            items.add(r);
            if (r.getSessionType() == SessionTypeEnum.Team && cacheMessages.get(r.getContactId()) != null) {
                TeamMemberAitHelper.setRecentContactAited(r, cacheMessages.get(r.getContactId()));
            }
        }

        cacheMessages.clear();

        refreshMessages(true);
    }

    private int getItemIndex(String uuid) {
        for (int i = 0; i < items.size(); i++) {
            RecentContact item = items.get(i);
            if (TextUtils.equals(item.getRecentMessageId(), uuid)) {
                return i;
            }
        }

        return -1;
    }

    protected void refreshViewHolderByIndex(final int index) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                privateChatListAdapter.notifyItemChanged(index);
            }
        });
    }

    public void setCallback(RecentContactsCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {

    }

}
