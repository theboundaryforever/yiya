package com.yiya.mobile.ui.me.user.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_core.user.bean.UserPhoto;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.yiya.mobile.base.fragment.BaseFragment;
import com.yiya.mobile.ui.me.user.adapter.PhotoUserAdapter;
import com.yiya.mobile.ui.widget.emptyView.DefaultEmptyEnum;
import com.yiya.mobile.utils.UIHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;

/**
 * ProjectName:
 * Description:个人主页-相册
 * Created by BlackWhirlwind on 2019/9/17.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class UserPhotoFragment extends BaseFragment {
    @BindView(R.id.rv_photos)
    RecyclerView rvPhotos;
    private UserInfo mUserInfo;
    private PhotoUserAdapter adapter;
    private StaggeredGridLayoutManager layoutManager;

    public static UserPhotoFragment newInstance(UserInfo userInfo) {
        UserPhotoFragment fragment = new UserPhotoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(UserInfo.KEY_USER_INFO, userInfo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_user_photo;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        if (getArguments() == null) {
            return;
        }
        mUserInfo = getArguments().getParcelable(UserInfo.KEY_USER_INFO);
        if (mUserInfo == null) {
            return;
        }

        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvPhotos.setLayoutManager(layoutManager);
        adapter = new PhotoUserAdapter(Collections.emptyList());
        adapter.setEmptyView(getEmptyView(rvPhotos, DefaultEmptyEnum.EMPTY_USER_PHOTO));
        rvPhotos.setAdapter(adapter);
        updatePhotoList(mUserInfo);
        adapter.setOnItemClickListener((helper, view, position) -> {
            List data = helper.getData();
            UserPhoto userPhoto = (UserPhoto) data.get(position);
            switch (userPhoto.getItemType()) {
                case UserPhoto.TYPE_PHOTO:
                    //预览图片
                    ArrayList<UserPhoto> tmpList = new ArrayList<>(data);
                    if (tmpList.get(0).getItemType() == UserPhoto.TYPE_UPLOAD) {
                        tmpList.remove(0);
                        position--;
                    }
                    Json json = getPhotoDataJson(tmpList);
                    UIHelper.showPhotoPreview(getContext(), position, json);
                    break;
                case UserPhoto.TYPE_UPLOAD:
                    //上传
                    UIHelper.showModifyPhotosAct(getContext(), mUserInfo.getUid());
                    break;
            }
        });

    }

    private Json getPhotoDataJson(List<UserPhoto> photos) {
        if (photos == null) {
            return null;
        }
        Json json = new Json();
        for (int i = 0; i < photos.size(); i++) {
            UserPhoto userPhoto = photos.get(i);
            Json j = new Json();
            j.set("pid", userPhoto.getPid());
            j.set("photoUrl", userPhoto.getPhotoUrl());
            json.set(i + "", j.toString());
        }
        return json;
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo info) {
        updatePhotoList(info);
    }

    private void updatePhotoList(UserInfo info) {
        if (info.getUid() == mUserInfo.getUid()) {
            mUserInfo = info;
            ArrayList<UserPhoto> tmpList = new ArrayList<>(info.getPrivatePhoto());
            if (CoreManager.getCore(IAuthCore.class).getCurrentUid() == info.getUid()) {
                //自己
                tmpList.add(0, new UserPhoto(UserPhoto.TYPE_UPLOAD));
                layoutManager.setSpanCount(2);
            } else {
                //他人
                layoutManager.setSpanCount(tmpList.isEmpty() ? 1 : 2);
            }
            adapter.setNewData(tmpList);
        }
    }

}
