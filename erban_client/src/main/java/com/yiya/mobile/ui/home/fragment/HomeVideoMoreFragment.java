package com.yiya.mobile.ui.home.fragment;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.home.HomeMorePresenter;
import com.yiya.mobile.presenter.home.HomeVideoMoreView;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.home.adpater.LiveMoreRecyclerAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.List;

import butterknife.BindView;

@CreatePresenter(HomeMorePresenter.class)
public class HomeVideoMoreFragment extends BaseMvpFragment<HomeVideoMoreView, HomeMorePresenter> implements HomeVideoMoreView, OnRefreshListener , OnLoadMoreListener {

    @BindView(R.id.smart_refresh_layout)
    SmartRefreshLayout mSmartRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.content_empty_ll)
    LinearLayout mContentEmptyLl;

    private LiveMoreRecyclerAdapter mAdapter;
    private GridLayoutManager mLayoutManager;

    private int mTabId = -1;
    private int mPageSize = 20;
    private int mPageNum = 1;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_home_video_more;
    }


    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        GrowingIO.getInstance().setPageName(this, "首页-直播-更多");
        mTabId = getArguments().getInt("tabId", -1);

        mLayoutManager = new GridLayoutManager(mContext, 2);
        mAdapter = new LiveMoreRecyclerAdapter(mContext, null);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new MyItemDecoration());

        mSmartRefreshLayout.setOnRefreshListener(this::onRefresh);
        mSmartRefreshLayout.setOnLoadMoreListener(this::onLoadMore);

        mAdapter.setOnItemClickListener((LiveMoreRecyclerAdapter.OnItemClickListener) (itemType, view, position, tabInfo) -> {
            HomeRoom homeRoom = mAdapter.getItem(position);
            RoomFrameActivity.start(getActivity(), homeRoom.getUid(), homeRoom.getType());

        });
    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        mPageNum = 1;
        getData(mPageNum);
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        mPageNum = 1;
        getData(mPageNum);
    }

    @Override
    public void onLoadMore(RefreshLayout refreshLayout) {
        getData(mPageNum+1);
    }

    private void getData(int pageNum) {
        this.mPageNum = pageNum;
        getMvpPresenter().getTagVideoRoom(mTabId, mPageNum);
    }

    @Override
    public void onTagVideoRoomSucceed(List<HomeRoom> data) {

        if(ListUtils.isListEmpty(data) && (mPageNum == 1)){
            mContentEmptyLl.setVisibility(View.VISIBLE);
//            mAdapter.loadMoreFail();
        } else {
            mContentEmptyLl.setVisibility(View.GONE);
//            mAdapter.loadMoreComplete();
            if (mPageNum == 1) {
                mAdapter.setData(data);
            } else {
                if(ListUtils.isNotEmpty(data)){
                    mAdapter.addData(data);
                }
            }
        }

        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
    }

    @Override
    public void onTagVideoRoomFailed(String message) {

    }

    @Override
    public void onMenuListSucceed(List<TabInfo> data) {

    }

    @Override
    public void onMenuListFailed(String message) {

    }

    private int mOffsetPadding = 0;
    private int mOffsetContext = 0;

    private class MyItemDecoration extends RecyclerView.ItemDecoration {

        private MyItemDecoration() {
            mOffsetPadding = DisplayUtils.dip2px(mContext, 15);
            mOffsetContext = DisplayUtils.dip2px(mContext, 10);
        }

        @Override
        public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.onDraw(c, parent, state);
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            int pos = parent.getChildAdapterPosition(view);
            if (mAdapter.getData().get(pos) != null) {
                if (pos % 2 == 0) {
                    outRect.left = mOffsetPadding;
                    outRect.right = mOffsetContext;
                } else {
                    outRect.left = mOffsetContext / 3;
                    outRect.right = mOffsetPadding;
                }
                outRect.bottom = mOffsetContext / 5 * 3;
            }
        }
    }
}
