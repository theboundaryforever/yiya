package com.yiya.mobile.ui.message.adapter;

import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.juxiao.library_utils.log.LogUtil;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.user.bean.AttentionInfo;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * <p> 关注列表 </p>
 *
 * @author Administrator
 * @date 2017/11/17
 */
public class AttentionListAdapter extends BaseQuickAdapter<AttentionInfo, BaseViewHolder> {

    public AttentionListAdapter(List<AttentionInfo> attentionInfoList) {
        super(R.layout.attention_item, attentionInfoList);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, final AttentionInfo attentionInfo) {
        if (attentionInfo == null) {
            return;
        }
        baseViewHolder.setText(R.id.tv_userName, attentionInfo.getNick())
                .setVisible(R.id.find_him_layout, attentionInfo.getUserInRoom() != null)
                .setOnClickListener(R.id.find_him_layout, v -> {
                    if (rylListener != null) {
                        rylListener.findHimListeners(attentionInfo);
                    }
                })
                .setOnClickListener(R.id.rly, v -> {
                    if (rylListener != null) {
                        rylListener.rylListeners(attentionInfo);
                    }
                });
        ImageView imageView = baseViewHolder.getView(R.id.imageView);
        ImageLoadUtils.loadCircleImage(mContext, attentionInfo.getAvatar(), imageView, R.drawable.nim_avatar_default);
        if (attentionInfo.isInRoom()) {
            baseViewHolder.getView(R.id.find_him_layout).setVisibility(View.VISIBLE);
            baseViewHolder.getView(R.id.svga_wave).setVisibility(View.VISIBLE);
            SVGAImageView svgaWave = baseViewHolder.getView(R.id.svga_wave);
            if (svgaWave.getDrawable() != null) {
                if (!svgaWave.isAnimating()) {
                    svgaWave.startAnimation();
                }
            } else {
                SVGAParser svgaParser = new SVGAParser(mContext);
                svgaParser.decodeFromAssets("microredwave.svga", new SVGAParser.ParseCompletion() {
                    @Override
                    public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                        svgaWave.setVideoItem(svgaVideoEntity);
                        if (!svgaWave.isAnimating()) {
                            svgaWave.startAnimation();
                        }
                    }

                    @Override
                    public void onError() {
                        LogUtil.e("svgaWave onError");
                    }
                });
            }

        } else {
            baseViewHolder.getView(R.id.find_him_layout).setVisibility(View.GONE);
            baseViewHolder.getView(R.id.svga_wave).setVisibility(View.GONE);
        }


    }

    private onClickListener rylListener;

    public interface onClickListener {
        void rylListeners(AttentionInfo attentionInfo);

        void findHimListeners(AttentionInfo attentionInfo);
    }

    public void setRylListener(onClickListener onClickListener) {
        rylListener = onClickListener;
    }
}
