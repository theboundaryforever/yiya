package com.yiya.mobile.ui.home.view;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.home.adpater.HomeAttentionAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.List;

public class HomeAttentionHeadView extends LinearLayout {
    private LinearLayout llEmpty;
    private RecyclerView rvAttention;
    private HomeAttentionAdapter mAdapter;


    public HomeAttentionHeadView(Context context) {
        super(context);
        inflate(context, R.layout.ly_home_attention_head, this);
        llEmpty = findViewById(R.id.ll_head_attention_empty);
        rvAttention = findViewById(R.id.rv_home_attention_room);
        int distance = DisplayUtils.dip2px(context, 10);
        rvAttention.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                if (parent.getChildAdapterPosition(view) == 0)
                    outRect.left = distance;
            }
        });
        rvAttention.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new HomeAttentionAdapter(false, R.layout.item_home_attention_head);
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                // todo 开启房间要传入房间类型
                RoomFrameActivity.start(context, mAdapter.getData().get(position).getUid(), RoomInfo.ROOMTYPE_HOME_PARTY);
            }
        });
        rvAttention.setAdapter(mAdapter);
    }

    /**
     * 根据数据更新布局
     *
     * @param data
     */
    public void setListData(List<HomeRoom> data) {
        if (mAdapter != null && !ListUtils.isListEmpty(data)) {
            if (rvAttention.getVisibility() == View.GONE) {
                rvAttention.setVisibility(View.VISIBLE);
            }
            if (llEmpty.getVisibility() == View.VISIBLE) {
                llEmpty.setVisibility(View.GONE);
            }
            mAdapter.setNewData(data);
        } else {
            if (rvAttention.getVisibility() == View.VISIBLE) {
                rvAttention.setVisibility(View.GONE);
            }
            if (llEmpty.getVisibility() == View.GONE) {
                llEmpty.setVisibility(View.VISIBLE);
            }
        }
    }

}
