package com.yiya.mobile.ui.me.wallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.me.MePresenter;
import com.yiya.mobile.ui.me.task.view.IMeView;
import com.yiya.mobile.ui.widget.TextWatcherListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.auth.IAuthClient;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/24
 * 描述        设置登录密码
 * <p>
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
@CreatePresenter(MePresenter.class)
public class SetPasswordActivity extends BaseMvpActivity<IMeView, MePresenter> implements IMeView {

    @BindView(R.id.toolbar)
    AppToolBar mToolBar;

    private TextWatcherListener mListener;
    private boolean isSetPassWord;//isSetPassWord ? "修改登录密码成功" : "设置登录密码成功"

    private UserInfo mUserInfo;

    private ResetLayoutClass mResetLayoutClass;

    public static void start(Context context, boolean isSetPassWord) {
        Intent intent = new Intent(context, SetPasswordActivity.class);
        intent.putExtra("isSetPassWord", isSetPassWord);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        isSetPassWord = getIntent().getBooleanExtra("isSetPassWord", false);
        ButterKnife.bind(this);

        mUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();

        mToolBar.setOnBackBtnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewStub viewStub;
        if (!isSetPassWord) {
            viewStub = (ViewStub) findViewById(R.id.view_stub_setting);
            View rootView = viewStub.inflate();

            new SettingLayoutClass(rootView);
        } else {
            viewStub = (ViewStub) findViewById(R.id.view_stub_reset);
            View rootView = viewStub.inflate();
            mResetLayoutClass = new ResetLayoutClass(rootView);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mResetLayoutClass != null) {
            if (mResetLayoutClass.mCodeDownTimer != null) {
                mResetLayoutClass.mCodeDownTimer.cancel();
                mResetLayoutClass.mCodeDownTimer = null;
            }
        }
    }

    private void changeState(ImageView eyeIv, EditText pwdEdt, boolean state) {
        if (state) {
            eyeIv.setImageResource(R.drawable.icon_eye_selector);
            pwdEdt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            eyeIv.setImageResource(R.drawable.icon_eye_normal);
            pwdEdt.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }

    public class ResetLayoutClass implements TextWatcher {

        @BindView(R.id.phone_tv)
        TextView mPhoneTv;
        @BindView(R.id.input_code_edt)
        EditText mInputCodeEdt;
        @BindView(R.id.get_code_btn)
        Button mGetCodeBtn;
        @BindView(R.id.input_new_pwd_edt)
        EditText mInputNewPwdEdt;/*
        @BindView(R.id.input_new_pwd_iv)
        ImageView mInputNewPwdIv;
        @BindView(R.id.input_pwd_ensure_edt)
        EditText mInputEnsurePwdEdt;
        @BindView(R.id.input_pwd_ensure_iv)
        ImageView mInputEnsurePwdIv;*/

        @BindView(R.id.commit_btn)
        Button mCommitBtn;


        public CodeDownTimer mCodeDownTimer;

        private boolean isShowNewPwd, isShowEnsurePwd;


        public ResetLayoutClass(View rootView) {
            ButterKnife.bind(this, rootView);

            GrowingIO.getInstance().trackEditText(mInputCodeEdt);
            GrowingIO.getInstance().trackEditText(mInputNewPwdEdt);
            /*GrowingIO.getInstance().trackEditText(mInputEnsurePwdEdt);*/

            mPhoneTv.setText(mUserInfo.getPhone());

            mGetCodeBtn.setClickable(true);
            mInputCodeEdt.addTextChangedListener(this);
            mInputNewPwdEdt.addTextChangedListener(this);
            /*mInputEnsurePwdEdt.addTextChangedListener(this);*/
        }


        private void checkCommitEnClick() {
            if (!TextUtils.isEmpty(mInputCodeEdt.getText()) && !TextUtils.isEmpty(mInputNewPwdEdt.getText()) /*&& !TextUtils.isEmpty(mInputEnsurePwdEdt.getText())*/
                    && mInputNewPwdEdt.getText().length() > 5 /*&& mInputEnsurePwdEdt.getText().length() > 5*/) {
                mCommitBtn.setEnabled(true);
            } else {
                mCommitBtn.setEnabled(false);
            }
        }


        /**
         * 获取验证码
         *
         * @param view
         */
        @OnClick(R.id.get_code_btn)
        void onClickGetCode(View view) {
            mCodeDownTimer = new CodeDownTimer(mGetCodeBtn, 60000, 1000);
            mCodeDownTimer.start();
            CoreManager.getCore(IAuthCore.class).requestSMSCode(mUserInfo.getPhone(), 3);
        }

        @CoreEvent(coreClientClass = IAuthClient.class)
        public void onSmsFail(String error) {
            toast(error);
        }

        /**
         * @param view
         */
       /* @OnClick(R.id.input_new_pwd_iv)
        void onClickNewPwdEye(View view) {
            isShowNewPwd = !isShowNewPwd;
            changeState(mInputNewPwdIv, mInputNewPwdEdt, isShowNewPwd);
        }

        @OnClick(R.id.input_pwd_ensure_iv)
        void onClickEnsurePwdEye(View view) {
            isShowEnsurePwd = !isShowEnsurePwd;
            changeState(mInputEnsurePwdIv, mInputEnsurePwdEdt, isShowEnsurePwd);
        }*/


        @OnClick(R.id.commit_btn)
        void onClickCommit(View view) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkCommitEnClick();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    /**
     * 设置密码
     */
    public class SettingLayoutClass implements TextWatcher {

        @BindView(R.id.setting_pwd_edt)
        EditText mSettingPwdEdt;
        @BindView(R.id.setting_pwd_iv)
        ImageView mSettingPwdIv;
        @BindView(R.id.setting_ensure_pwd_edt)
        EditText mSettingEnsurePwdEdt;
        @BindView(R.id.setting_ensure_pwd_iv)
        ImageView mSettingEnsurePwdIv;

        @BindView(R.id.commit_btn)
        Button mCommitBtn;

        private boolean isShowSetting, isShowEnsure;


        public SettingLayoutClass(View rootView) {
            ButterKnife.bind(this, rootView);

            GrowingIO.getInstance().trackEditText(mSettingPwdEdt);
            GrowingIO.getInstance().trackEditText(mSettingEnsurePwdEdt);

            mSettingPwdEdt.addTextChangedListener(this);
            mSettingEnsurePwdEdt.addTextChangedListener(this);
        }

        @OnClick(R.id.setting_pwd_iv)
        void onClickSettingPwdEye(View view) {

            isShowSetting = !isShowSetting;
            changeState(mSettingPwdIv, mSettingPwdEdt, isShowSetting);

        }

        @OnClick(R.id.setting_ensure_pwd_iv)
        void onClickSettingEnsurePwdEye(View view) {

            isShowEnsure = !isShowEnsure;
            changeState(mSettingEnsurePwdIv, mSettingEnsurePwdEdt, isShowEnsure);
        }

        @OnClick(R.id.commit_btn)
        void onClickCommit(View view) {
        }

        private void checkCommit() {
            if (!TextUtils.isEmpty(mSettingPwdEdt.getText()) && !TextUtils.isEmpty(mSettingEnsurePwdEdt.getText())
                    && mSettingPwdEdt.getText().length() > 5 && mSettingEnsurePwdEdt.getText().length() > 5) {

                mCommitBtn.setEnabled(true);
            } else {
                mCommitBtn.setEnabled(false);
            }
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkCommit();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

//    private void initView() {
//        if (!isSetPassWord) {
//            tvOldTitle.setVisibility(View.GONE);
//            pwContent.setVisibility(View.GONE);
//        }
//    }
//
//    private void setListener() {
//        listener = new TextWatcherListener() {
//            @Override
//            public void afterTextChanged(Editable editable) {
//                if (checkInput()) {
//                    btnSave.setEnabled(true);
//                } else {
//                    btnSave.setEnabled(false);
//                }
//            }
//        };
//        oldPassword.addTextChangedListener(listener);
//        newPassword.addTextChangedListener(listener);
//        confirmPassword.addTextChangedListener(listener);
//
//        btnSave.setOnClickListener(view -> {
//            if (!verifierInput()) {
//                return;
//            }
//            getDialogManager().showProgressDialog(this);
//            String old = oldPassword.getText().toString();
//            String newPwd = newPassword.getText().toString();
//            String confirm = confirmPassword.getText().toString();
//            if (isSetPassWord) {
//                getMvpPresenter().modifyPassword(old, newPwd, confirm, UriProvider.modifyPwd());
//            } else {
//                getMvpPresenter().modifyPassword(old, newPwd, confirm, UriProvider.setPassWord());
//            }
//        });
//        mToolBar.setOnBackBtnListener(view -> finish());
//        forget.setOnClickListener(view -> {
//            ForgetPswActivity.start(this);
//            finish();
//        });
//
//    }
//
//    private boolean checkInput() {
//        if (isSetPassWord) {
//            return oldPassword.getText() != null && oldPassword.getText().length() > 5 &&
//                    newPassword.getText() != null && newPassword.getText().length() > 5 &&
//                    confirmPassword.getText() != null && confirmPassword.getText().length() > 5;
//        }
//        return newPassword.getText() != null && newPassword.getText().length() > 5 &&
//                confirmPassword.getText() != null && confirmPassword.getText().length() > 5;
//    }
//
//    private boolean verifierInput() {
//        if (isSetPassWord) {
//            if (!newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
//                toast("新密码与确认密码不一致，请重新输入");
//                return false;
//            }
//            if (newPassword.getText().toString().equals(oldPassword.getText().toString())) {
//                toast("新密码与当前密码一致，请重新输入");
//                return false;
//            }
//        } else {
//            if (!newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
//                toast("新密码与确认密码不一致，请重新输入");
//                return false;
//            }
//        }
//        return true;
//    }
//
//    @Override
//    public void modifyPassword() {
//        getDialogManager().dismissDialog();
//        toast(isSetPassWord ? "修改登录密码成功" : "设置登录密码成功");
//        finish();
//    }
//
//    @Override
//    public void modifyPasswordFail(String msg) {
//        getDialogManager().dismissDialog();
//        toast(msg);
//    }
//
//    @Override
//    protected void onDestroy() {
//        if (oldPassword != null) {
//            oldPassword.removeTextChangedListener(listener);
//        }
//        if (newPassword != null) {
//            newPassword.removeTextChangedListener(listener);
//        }
//        if (confirmPassword != null) {
//            confirmPassword.removeTextChangedListener(listener);
//        }
//        super.onDestroy();
//    }
}
