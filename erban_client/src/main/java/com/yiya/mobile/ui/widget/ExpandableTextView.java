package com.yiya.mobile.ui.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by huangmeng1 on 2018/1/6.
 */

public class ExpandableTextView extends LinearLayout {
    private TextView mTextView;
    private ImageView mOpenBtn;
    private boolean isOpen = false;
    private int foldLines = 1; //大于1行的时候折叠
    private int lineCounts;

    public ExpandableTextView(Context context) {
        this(context, null);
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExpandableTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    private void initView() {

        lineCounts = mTextView.getLineCount();
        if (lineCounts <= foldLines) {
            mOpenBtn.setVisibility(GONE);
        }
        if (isOpen && mTextView.getHeight() != lineCounts * mTextView.getLineHeight()) {
            mTextView.setHeight(mTextView.getLineHeight() * mTextView.getLineCount());
        } else if (!isOpen && mTextView.getHeight() != foldLines * mTextView.getLineHeight()) {
            mTextView.setHeight(mTextView.getLineHeight() * foldLines);
        }
        mTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                lineCounts = mTextView.getLineCount();
                if (lineCounts <= foldLines) {
                    post(() -> mOpenBtn.setVisibility(GONE));
                } else {
                    post(() -> mOpenBtn.setVisibility(VISIBLE));
                }
            }
        });

        setOnClickListener(v -> {
            ValueAnimator va;
            if (isOpen) {
                va = ValueAnimator.ofInt(mTextView.getLineHeight() * mTextView.getLineCount(), mTextView.getLineHeight() * foldLines);
                RotateAnimation animation = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                animation.setDuration(200);
                animation.setFillAfter(true);
                mOpenBtn.startAnimation(animation);
            } else {
                va = ValueAnimator.ofInt(mTextView.getLineHeight() * foldLines, mTextView.getLineHeight() * mTextView.getLineCount());
                RotateAnimation animation = new RotateAnimation(180, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                animation.setDuration(200);
                animation.setFillAfter(true);
                mOpenBtn.startAnimation(animation);
            }
            isOpen = !isOpen;
            va.addUpdateListener(valueAnimator -> {
                int h = (Integer) valueAnimator.getAnimatedValue();
                getLayoutParams().height = h;
                requestLayout();
            });
            va.setDuration(200);
            va.start();
        });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        initView();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (mTextView == null || mOpenBtn == null) {
            mTextView = (TextView) getChildAt(0);
            mOpenBtn = (ImageView) getChildAt(1);
        }

    }
}
