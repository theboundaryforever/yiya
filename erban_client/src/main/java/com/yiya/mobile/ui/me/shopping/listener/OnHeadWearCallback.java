package com.yiya.mobile.ui.me.shopping.listener;

public interface OnHeadWearCallback {
    void onHeadWearChangeListener(String url);
    void onCarTryListener(String url);
}
