package com.yiya.mobile.ui.me.setting.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.yiya.mobile.ui.me.MePresenter;
import com.yiya.mobile.ui.me.task.view.IMeView;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Zhangsongzhou
 * @date 2019/5/6
 */
@CreatePresenter(MePresenter.class)
public class ResetBindPhoneActivity extends BaseMvpActivity<IMeView, MePresenter> implements IMeView {


    @BindView(R.id.toolbar)
    AppToolBar mAppToolBar;

    @BindView(R.id.et_phone)
    EditText mPhoneEdt;
    @BindView(R.id.et_smscode)
    EditText mSmsCodeEdt;


    @BindView(R.id.btn_get_code)
    Button mGetCodeBtn;

    /*@BindView(R.id.btn_binder)
    Button mUnBinderBtn;*/
    @BindView(R.id.btn_binder_request)
    Button mBinderBtn;


    private CodeDownTimer mTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GrowingIO.getInstance().setPageName(this, "绑定手机号");
        setContentView(R.layout.activity_binder_phone);
        ButterKnife.bind(this);

        GrowingIO.getInstance().trackEditText(mPhoneEdt);
        GrowingIO.getInstance().trackEditText(mSmsCodeEdt);

        mAppToolBar.setOnBackBtnListener(v -> finish());

        mSmsCodeEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(mPhoneEdt.getText()) && !TextUtils.isEmpty(mSmsCodeEdt.getText())) {
                    /*mBinderBtn.setVisibility(View.VISIBLE);*/
                    /*mUnBinderBtn.setVisibility(View.GONE);*/
                } else {

                    /*mBinderBtn.setVisibility(View.GONE);*/
                    /*mUnBinderBtn.setVisibility(View.VISIBLE);*/
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;

        }
    }

    @OnClick(R.id.btn_get_code)
    void onClickGetCode(View view) {
        if (TextUtils.isEmpty(mPhoneEdt.getText())) {
            toast("手机号不能为空");
            return;
        }
        mTimer = new CodeDownTimer(mGetCodeBtn, 60 * 1000, 1 * 1000);
        mTimer.start();
        getMvpPresenter().getSendSms(mPhoneEdt.getText().toString());


    }


    @OnClick(R.id.btn_binder_request)
    void onClickBinder(View view) {
        String phoneStr = mPhoneEdt.getText().toString();
        String codeStr = mSmsCodeEdt.getText().toString();
        if (TextUtils.isEmpty(phoneStr) || TextUtils.isEmpty(codeStr)) {
            toast("信息不能为空");
            return;
        }

        getDialogManager().showProgressDialog(this, "加载中");
        getMvpPresenter().boundPhone(phoneStr, codeStr);
    }

    @Override
    public void callbackBoundPhone(int code, boolean value) {
        getDialogManager().dismissDialog();
        if (code == 200 && value) {
            ResetPWDActivity.start(this);
            finish();
        }
    }

    @Override
    public void callbackBoundPhoneFail(String message) {
        getDialogManager().dismissDialog();
        toast(message);
    }
}
