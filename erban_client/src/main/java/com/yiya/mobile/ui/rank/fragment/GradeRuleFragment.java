package com.yiya.mobile.ui.rank.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.rankinglist.IRankingListView;
import com.yiya.mobile.presenter.rankinglist.RankingListPresenter;
import com.yiya.mobile.ui.rank.activity.GradeRuleActivity;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.databinding.FragmentGradeRuleBinding;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.bean.UserLevelInfo;

/**
 * 创建者      Created by dell
 * 创建时间    2018/12/3
 * 描述        等级排行
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
@CreatePresenter(RankingListPresenter.class)
public class GradeRuleFragment extends BaseMvpFragment<IRankingListView, RankingListPresenter> implements
        View.OnClickListener, IRankingListView {

    private boolean isCharm;
    private FragmentGradeRuleBinding mBinding;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_grade_rule;
    }

    @Override
    protected void onInitArguments(Bundle bundle) {
        if (bundle != null) {
            isCharm = bundle.getBoolean("isCharm", false);
        }
    }

    @Override
    public void onFindViews() {
        mBinding = DataBindingUtil.bind(mView);
    }

    @Override
    public void onSetListener() {
        mBinding.setClick(this);
        mBinding.setIsCharm(isCharm);

        mBinding.seekBar.setOnTouchListener((view, motionEvent) -> true);
    }

    @Override
    public void initiate() {
        getMvpPresenter().getUserLevel(isCharm ? UriProvider.levelCharmGet() : UriProvider.levelExeperienceGet());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.grade_detail:
                GradeRuleActivity.start(getActivity(), isCharm);
                break;
            default:
                break;
        }
    }

    @Override
    public void getUserLevelSuccess(UserLevelInfo info) {
        mBinding.setLevelInfo(info);
        mBinding.seekBar.setProgress((int) (info.getLevelPercent() * 100));
        mBinding.notifyMsg.setText("离升级还需消耗" + info.getLeftGoldNum() + "金币");
    }

    @Override
    public void getUserLevelFail(String msg) {
        toast(msg);
    }

    public static Fragment newInstance(boolean isCharm) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isCharm", isCharm);
        GradeRuleFragment fragment = new GradeRuleFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

}
