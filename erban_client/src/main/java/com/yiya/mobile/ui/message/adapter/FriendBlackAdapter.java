package com.yiya.mobile.ui.message.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.msg.BlackListInfo;

/**
 * @author huangmeng1
 * @date 2018/1/18
 */

public class FriendBlackAdapter extends BaseQuickAdapter<BlackListInfo.ListBean, BaseViewHolder> {

    public FriendBlackAdapter() {
        super(R.layout.item_rv_friend_black_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, BlackListInfo.ListBean item) {
        helper.setText(R.id.tv_userName, item.getNick()).addOnClickListener(R.id.remove);
        ImageLoadUtils.loadCircleImage(mContext, item.getAvatar(), helper.getView(R.id.imageView), R.drawable.nim_avatar_default);
    }

}
