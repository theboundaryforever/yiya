package com.yiya.mobile.ui.me.user.activity;

import android.view.View;
import android.widget.ImageView;

import com.yiya.mobile.presenter.user.IUserInfoView;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.common.widget.dialog.DialogManager;
import com.yiya.mobile.ui.home.adpater.HomeIndicatorAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.widget.ButtonItem;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCoreClient;
import com.tongdaxing.xchat_core.praise.IPraiseClient;
import com.tongdaxing.xchat_core.praise.IPraiseCore;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class UserInfoOthersActivity extends BaseUserInfoActivity implements IUserInfoView, HomeIndicatorAdapter.OnItemSelectListener {

    @BindView(R.id.iv_follow)
    ImageView ivFollow;
    @BindView(R.id.iv_room)
    ImageView ivRoom;
    private boolean isBlacklist = false;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_user_info_others;
    }

    @Override
    public void initToolBar() {
        super.initToolBar();
        mToolBar.setOnRightImgBtnClickListener(this::showTitleMoreDialog);
    }

    @Override
    protected void initData() {
        super.initData();
        checkBlacklist();
        //获取用户房间信息
//        getMvpPresenter().getUserRoomInfo(mUserId);
        //是否关注
        CoreManager.getCore(IPraiseCore.class).isPraised(CoreManager.getCore(IAuthCore.class).getCurrentUid(), mUserId);
    }

    private void showTitleMoreDialog() {
        String blacklistStr = isBlacklist ? "移除黑名单" : "拉黑";
        ButtonItem blackListItem = new ButtonItem(blacklistStr, () -> {
            if (isBlacklist) {
                getMvpPresenter().removeBlacklistByUid(mUserId + "");
            } else {
                getMvpPresenter().addBlackListByUid(mUserId + "", 1);
            }

        });
        ButtonItem informItem = new ButtonItem("举报", this::showInformDialog);
        List<ButtonItem> contentList = new ArrayList<>();
        contentList.add(blackListItem);
        contentList.add(informItem);
        getDialogManager().showCommonPopupDialog(contentList, "取消", true);
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void removeBlackListSuccess() {
        toast("已取消拉黑");
    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void addBlackListSuccess() {
        toast("已拉黑");
    }

    /**
     * 检查是否在黑名单
     */
    private void checkBlacklist() {
        getMvpPresenter().checkBlackListByUid(mUserId + "");
    }

    /**
     * 检查是否黑名单
     *
     * @param checkBothSides 在自己的黑名单 or  在对方的黑名单
     * @param inBlacklist    在自己的黑名单
     */
    @Override
    public void onCheckBlacklist(boolean checkBothSides, boolean inBlacklist, boolean inTgUserBlacklist) {
        isBlacklist = inBlacklist;
    }

    @Override
    public void onCheckBlacklistFailed(String message) {
        toast(message);
    }

    /**
     * 添加进黑名单
     */
    @Override
    public void onAddBlacklist() {
        toast("已拉黑");
        checkBlacklist();
    }

    @Override
    public void onAddBlacklistFailed(String message) {
        toast(message);
    }

    /**
     * 移除黑名单
     */
    @Override
    public void onRemoveBlacklist() {
        checkBlacklist();
    }

    @Override
    public void onRemoveBlacklistFailed(String message) {
        toast(message);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLiked(boolean islike, long uid) {
        updateAttention(islike);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onIsLikedFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraise(long likedUid) {
        getDialogManager().dismissDialog();

        updateAttention(true);
//        if (isResume) {//泛滥调用
        toast("关注成功，相互关注可成为好友哦！");
//        }

    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraise(long likedUid, boolean showNotice) {
        if (showNotice) {
            toast("取消关注成功");
        }
        getDialogManager().dismissDialog();
        updateAttention(false);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onCanceledPraiseFaith(String error) {
        toast(error);
    }

    @CoreEvent(coreClientClass = IPraiseClient.class)
    public void onPraiseFaith(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    private void updateAttention(boolean isAlready) {
        ivFollow.setSelected(isAlready);
    }

    /**
     * 显示举报内容的dialog
     */
    private void showInformDialog() {
        ButtonItem buttonItem01 = new ButtonItem("政治敏感", new InformDialogItemClickImpl(1));
        ButtonItem buttonItem02 = new ButtonItem("色情低俗", new InformDialogItemClickImpl(2));
        ButtonItem buttonItem03 = new ButtonItem("广告骚扰", new InformDialogItemClickImpl(3));
        ButtonItem buttonItem04 = new ButtonItem("人身攻击", new InformDialogItemClickImpl(4));

        List<ButtonItem> contentList = new ArrayList<>();
        contentList.add(buttonItem01);
        contentList.add(buttonItem02);
        contentList.add(buttonItem03);
        contentList.add(buttonItem04);
        getDialogManager().showCommonPopupDialog(contentList, "取消", true);
    }

    @OnClick({R.id.iv_room, R.id.iv_follow, R.id.iv_chat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_room:
                //跳转用户语聊房
                RoomFrameActivity.start(this, mUserId, RoomInfo.ROOMTYPE_HOME_PARTY);
                break;
            case R.id.iv_follow:
                //关注或取消关注
                followUser();
                break;
            case R.id.iv_chat:
                //私聊
                turn2chat();
                break;
        }
    }

    private void turn2chat() {
        if (mUserInfo == null) {
            return;
        }
        boolean isFriends = CoreManager.getCore(IIMFriendCore.class).isMyFriend(mUserInfo.getUid() + "");
        getMvpPresenter().handlePrivateChat(this, isFriends, mUserId);
    }

    private void followUser() {
        if (mUserInfo == null) {
            return;
        }
        if (ivFollow.isSelected()) {// 已关注
            boolean isMyFriend = CoreManager.getCore(IIMFriendCore.class).isMyFriend(mUserInfo.getUid() + "");
            String tip;
            if (isMyFriend) {
                tip = "取消关注将不再是好友关系，确定取消关注？";
            } else {
                tip = "确定取消关注？";
            }
            getDialogManager().showOkCancelDialog(tip, true, new DialogManager.OkCancelDialogListener() {
                @Override
                public void onCancel() {
                    getDialogManager().dismissDialog();
                }

                @Override
                public void onOk() {
                    getDialogManager().dismissDialog();
                    getDialogManager().showProgressDialog(UserInfoOthersActivity.this, "请稍候...");
                    CoreManager.getCore(IPraiseCore.class).cancelPraise(mUserInfo.getUid(), true);

                }
            });
        } else {
            getDialogManager().showProgressDialog(UserInfoOthersActivity.this, "请稍候...");
            CoreManager.getCore(IPraiseCore.class).praise(mUserInfo.getUid());
        }
    }

    private class InformDialogItemClickImpl implements ButtonItem.OnClickListener {
        private int mReportType;

        private InformDialogItemClickImpl(int reportType) {
            mReportType = reportType;
        }

        @Override
        public void onClick() {
            getMvpPresenter().commitReport(1, mUserId, mReportType);
        }
    }

}
