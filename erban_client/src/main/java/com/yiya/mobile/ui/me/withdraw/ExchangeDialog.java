package com.yiya.mobile.ui.me.withdraw;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.R;

public class ExchangeDialog extends BaseDialogFragment implements View.OnClickListener {

    private String txt_count;
    private OnExchangeChangeListener onChangeListener;

    private TextView tv_confirm,tv_count;


    public static ExchangeDialog newInstance(String str) {
        ExchangeDialog exchangeDialog = new ExchangeDialog();
        Bundle bundle = new Bundle();
        bundle.putString("exchange", str);
        exchangeDialog.setArguments(bundle);
        return exchangeDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            txt_count = bundle.getString("exchange");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams params = window.getAttributes();
            params.width = WindowManager.LayoutParams.WRAP_CONTENT;
            params.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(params);
        }
        setCancelable(true);
        return inflater.inflate(R.layout.dialog_exchange_select, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_confirm = (TextView) view.findViewById(R.id.tv_confirm);//确认
        tv_count = (TextView)view.findViewById(R.id.tv_count);//文本
        tv_count.setText(txt_count);
        tv_confirm.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_confirm:
                onChangeListener.onChangeListener();
                dismiss();
                break;
        }
    }

    public void setOnExchangeChangeListener(OnExchangeChangeListener onExchangeChangeListener) {
        this.onChangeListener = onExchangeChangeListener;
    }

    public interface OnExchangeChangeListener {
        void onChangeListener();
    }

}
