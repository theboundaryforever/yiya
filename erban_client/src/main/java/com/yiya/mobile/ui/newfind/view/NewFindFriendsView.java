package com.yiya.mobile.ui.newfind.view;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment;
import com.yiya.mobile.ui.newfind.fragment.NewFindFriendsFragment;

import java.util.List;

public interface NewFindFriendsView extends IMvpBaseView {

    /**刷新或加载*/
    void onSucceed(int pageNum, List<NewFindFriendsFragment.NewFindFriendsinfo> data);

    /**停止刷新或加载*/
    void onFailed(String message);

}
