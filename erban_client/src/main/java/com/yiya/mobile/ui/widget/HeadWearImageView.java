package com.yiya.mobile.ui.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/6.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class HeadWearImageView extends ConstraintLayout {

    @BindView(R.id.iv_head)
    ImageView ivAvatar;
    @BindView(R.id.iv_headwear)
    ImageView ivHeadwear;

    public HeadWearImageView(Context context) {
        this(context, null);
    }

    public HeadWearImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HeadWearImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        inflate(getContext(), R.layout.layout_avatar_with_headwear, this);
        ButterKnife.bind(this);
    }

    public void setAvatar(String url) {
        ImageLoadUtils.loadAvatar(getContext(), url, ivAvatar, true);
    }

    public void setHeadWear(String url) {
        if (TextUtils.isEmpty(url)) {
            ivHeadwear.setVisibility(INVISIBLE);
        } else {
            ImageLoadUtils.loadImage(getContext(), url, ivHeadwear);
        }
    }
}
