package com.yiya.mobile.ui.widget;


import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tongdaxing.erban.R;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> 底部tab导航 </p>
 * Created by Administrator on 2017/11/14.
 */
public class MainTabLayout extends LinearLayout implements View.OnClickListener {


    private MainTab mHomeTab, mAttentionTab, mMeTab,mMsgTab;
    private ImageView mHomePointIv, mMsgPointIv, mMePointIv;
    private LinearLayout mHomeLl, mMeLl;
    private FrameLayout mMessageFl;

    private List<ImageView> mPointViewList;

 /*   private MainRedPointTab mMsgTab;*/
    private int mLastPosition = -1;

    public MainTabLayout(Context context) {
        this(context, null);
    }

    public MainTabLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainTabLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        setOrientation(HORIZONTAL);
        inflate(context, R.layout.main_tab_layout, this);

        mHomePointIv = findViewById(R.id.main_home_point_iv);
        mMsgPointIv = findViewById(R.id.main_msg_point_iv);
        mMePointIv = findViewById(R.id.main_me_point_iv);

        mPointViewList = new ArrayList<>();
        mPointViewList.add(mHomePointIv);
        mPointViewList.add(mMsgPointIv);
        mPointViewList.add(mMePointIv);


        mHomeTab = (MainTab) findViewById(R.id.main_home_tab);
        mAttentionTab = (MainTab) findViewById(R.id.main_attention_tab);
        mMsgTab = (MainTab) findViewById(R.id.main_msg_tab);
        mMeTab = (MainTab) findViewById(R.id.main_me_tab);

        mHomeLl = findViewById(R.id.main_home_ll);
        mMessageFl = findViewById(R.id.main_message_fl);
        mMeLl = findViewById(R.id.main_me_ll);


        mHomeTab.setOnClickListener(this);
        mAttentionTab.setOnClickListener(this);
        mMsgTab.setOnClickListener(this);
        mMeTab.setOnClickListener(this);

        mHomeLl.setOnClickListener(this);
        mMessageFl.setOnClickListener(this);
        mMeLl.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_home_tab:
            case R.id.main_home_ll:
                select(0);
                break;
            case R.id.main_attention_tab:
                select(1);
                break;
            case R.id.main_msg_tab:
            case R.id.main_message_fl:
                select(2);
                break;
            case R.id.main_me_tab:
            case R.id.main_me_ll:
                select(3);
                break;
        }
    }

    public void setMsgNum(int number) {
//        mMsgTab.setNumber(number);
        mMsgPointIv.setVisibility(number > 0 ? VISIBLE : INVISIBLE);
    }

    public void select(int position) {
        if (mLastPosition == position) return;
        mHomeTab.select(position == 0);
        mAttentionTab.select(position == 1);
        mMsgTab.select(position == 2);
        mMeTab.select(position == 3);

//        if (position == 0) {
//            selectPoint(mHomePointIv);
//        } else if (position == 2) {
//            selectPoint(mMsgPointIv);
//        } else if (position == 3) {
//            selectPoint(mMePointIv);
//        }

        if (mOnTabClickListener != null) {
            mOnTabClickListener.onTabClick(position);
        }
        mLastPosition = position;
    }

    private OnTabClickListener mOnTabClickListener;

    public void setOnTabClickListener(OnTabClickListener onTabClickListener) {
        mOnTabClickListener = onTabClickListener;
    }

    public interface OnTabClickListener {
        void onTabClick(int position);
    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);

    }
}
