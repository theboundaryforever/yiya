package com.yiya.mobile.ui.message.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.yiya.mobile.base.fragment.BaseFragment;
import com.yiya.mobile.constant.Extras;
import com.yiya.mobile.room.avroom.activity.RoomChatActivity;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.recent.RecentContactsCallback;
import com.netease.nim.uikit.recent.RecentContactsFragment;
import com.netease.nimlib.sdk.msg.attachment.AudioAttachment;
import com.netease.nimlib.sdk.msg.attachment.MsgAttachment;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.NoticeAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RedPacketAttachment;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfoV2;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;

import static com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment.CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI;


/**
 * 最近消息聊天好友列表
 *
 * @author chenran
 * @date 2017/9/18
 */
public class RecentListFragment extends BaseFragment {
    private boolean isRoomPrivate = false;
    private RecentContactsFragment recentContactsFragment;

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (getArguments() != null){
            isRoomPrivate = getArguments().getBoolean(Extras.EXTRA_ROOM_PRIVATE,false);
        }
    }

    @Override
    public void onFindViews() {
        recentContactsFragment = new RecentContactsFragment();
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.recent_container, recentContactsFragment).commitAllowingStateLoss();
    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
        recentContactsFragment.setCallback(new RecentContactsCallback() {

            @Override
            public void onRecentContactsLoaded() {

            }

            @Override
            public void onUnreadCountChange(int unreadCount) {

            }

            @Override
            public void onItemClick(RecentContact recent) {
                if (mContext != null && mContext instanceof RoomChatActivity){
                    ((RoomChatActivity) mContext).showPrivateChat(recent.getContactId());
                }else {
                    if (recent.getSessionType() == SessionTypeEnum.Team) {
                        NimUIKit.startTeamSession(getActivity(), recent.getContactId());
                    } else if (recent.getSessionType() == SessionTypeEnum.P2P) {
                        NimUIKit.startP2PSession(getActivity(), recent.getContactId());
                    }
                }
            }

            @Override
            public String getDigestOfAttachment(RecentContact recent, MsgAttachment attachment) {
                if (attachment instanceof CustomAttachment) {
                    CustomAttachment customAttachment = (CustomAttachment) attachment;
                    if (customAttachment.getFirst() == CUSTOM_MSG_HEADER_TYPE_OPEN_ROOM_NOTI) {
                        return "您关注的TA上线啦，快去围观吧~~~";
                    } else if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
                        return "[礼物]";
                    } else if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_NOTICE) {
                        NoticeAttachment noticeAttachment = (NoticeAttachment) attachment;
                        return noticeAttachment.getTitle();
                    } else if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_PACKET) {
                        RedPacketAttachment redPacketAttachment = (RedPacketAttachment) attachment;
                        RedPacketInfoV2 redPacketInfo = redPacketAttachment.getRedPacketInfo();
                        if (redPacketInfo == null) {
                            return "您收到一个红包哦!";
                        }
                        return "您收到一个" + redPacketInfo.getPacketName() + "红包哦!";
                    } else if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_HEADER_TYPE_LOTTERY) {
                        return "恭喜您，获得抽奖机会";
                    } else if (customAttachment.getFirst() == CustomAttachment.CUSTOM_MSG_SHARE_FANS) {
                        return "[房间邀请]";
                    }
                } else if (attachment instanceof AudioAttachment) {
                    return "[语音]";
                }
                return null;
            }

            @Override
            public String getDigestOfTipMsg(RecentContact recent) {
                return null;
            }
        });
    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_recent_list;
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        recentContactsFragment.requestMessages(true);
    }
}
