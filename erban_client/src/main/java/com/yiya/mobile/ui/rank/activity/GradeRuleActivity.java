package com.yiya.mobile.ui.rank.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.rank.adapter.GradeRuleAdapter;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.databinding.ActivityGradeRuleBinding;

import java.util.Arrays;
import java.util.List;

/**
 * 创建者      Created by dell
 * 创建时间    2018/12/3
 * 描述        等级规则
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class GradeRuleActivity extends BaseActivity {

    private boolean isCharm;
    private GradeRuleAdapter mAdapter;
    private ActivityGradeRuleBinding mBinding;

    public static void start(Context context, boolean isCharm) {
        Intent intent = new Intent(context, GradeRuleActivity.class);
        intent.putExtra("isCharm", isCharm);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isCharm = getIntent().getBooleanExtra("isCharm", false);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_grade_rule);
        mBinding.setIsCharm(isCharm);
        //设置标题
        mBinding.toolbar.setTitle(isCharm ? getString(R.string.charm_grade_rule) : getString(R.string.gold_grade_rule));
        mBinding.toolbar.setOnBackBtnListener(view -> finish());

        initiate();
    }

    private void initiate() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(layoutManager);

        mAdapter = new GradeRuleAdapter(isCharm);
        mBinding.recyclerView.setAdapter(mAdapter);

        List<String> ruleItems = Arrays.asList(getResources().getStringArray(R.array.grade_rule));
        mAdapter.setNewData(ruleItems);
    }
}
