package com.yiya.mobile.ui.login.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import com.yiya.mobile.base.activity.BaseActivity;
import com.juxiao.library_ui.widget.AppToolBar;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * ProjectName:
 * Description: 完善资料
 * Created by BlackWhirlwind on 2019/8/30.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class CompleteInfoActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    AppToolBar toolbar;
    @BindView(R.id.rg_gender)
    RadioGroup rgGender;

    private int mGender = 1;//性别 1:男 2：女 0 ：未知
    private long mUid;


    public static void start(Context context) {
        Intent intent = new Intent(context, CompleteInfoActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_info);
        ButterKnife.bind(this);

        initEvent();
        initData();
    }

    private void initData() {
        mUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }

    private void initEvent() {
        toolbar.setLeftImageVisibility(View.GONE);
        rgGender.setOnCheckedChangeListener((group, checkedId) -> {
            mGender = checkedId == R.id.rb_male ? 1 : 2;
        });

    }

    @OnClick(R.id.dtv_confirm)
    public void onViewClicked() {
        if (mUid != 0) {
            UserInfo userInfo = new UserInfo();
            userInfo.setGender(mGender);
            userInfo.setUid(mUid);
            userInfo.setNewRegister(true);
            CoreManager.getCore(IUserCore.class).requestUpdateUserInfo(userInfo);
        }
        finish();
    }

    @Override
    public void onBackPressed() {
    }
}
