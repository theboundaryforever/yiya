package com.yiya.mobile.ui.me;

import com.yiya.mobile.model.log.LogModel;
import com.yiya.mobile.model.rank.RankingListModel;
import com.yiya.mobile.ui.me.task.view.IMeView;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.MeInfo;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.log.MLog;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.tongdaxing.xchat_core.user.bean.MeInfo.TYPE_ABOUT;
import static com.tongdaxing.xchat_core.user.bean.MeInfo.TYPE_BROADCASTER_VERIFY;
import static com.tongdaxing.xchat_core.user.bean.MeInfo.TYPE_FEEDBACK;
import static com.tongdaxing.xchat_core.user.bean.MeInfo.TYPE_HOST_RECORD;
import static com.tongdaxing.xchat_core.user.bean.MeInfo.TYPE_INVITE;
import static com.tongdaxing.xchat_core.user.bean.MeInfo.TYPE_NAME_VERIFY;
import static com.tongdaxing.xchat_core.user.bean.MeInfo.TYPE_TOP;

/**
 * Created by MadisonRong on 04/01/2018.
 */

public class MePresenter extends AbstractMvpPresenter<IMeView> {

    private UserMvpModel userMvpModel;
    private RankingListModel mRankingListModel;
    private UserInfo mUserInfo;

    public MePresenter() {
        userMvpModel = new UserMvpModel();
        mRankingListModel = new RankingListModel();
    }

    public List<MeInfo> generateList() {
        List<MeInfo> list = new ArrayList<>();
        list.add(new MeInfo(TYPE_TOP, -1, null));
        list.add(new MeInfo(TYPE_NAME_VERIFY, R.mipmap.ic_qianbao, "我的钱包"));
        list.add(new MeInfo(TYPE_BROADCASTER_VERIFY, R.mipmap.ic_dengji, "我的等级"));
        list.add(new MeInfo(TYPE_ABOUT, -1, null));
        list.add(new MeInfo(TYPE_HOST_RECORD, R.mipmap.ic_renzhegn, "实名认证"));
        list.add(new MeInfo(TYPE_INVITE, R.mipmap.ic_wentifankui, "问题反馈"));
        list.add(new MeInfo(TYPE_FEEDBACK, R.mipmap.ic_shezhi, "设置"));
        return list;
    }

    public List<MeInfo> generateListNoBroadcast() {
        List<MeInfo> list = new ArrayList<>();
        list.add(new MeInfo(TYPE_TOP, -1, null));
        list.add(new MeInfo(TYPE_NAME_VERIFY, R.mipmap.ic_qianbao, "我的钱包"));
        list.add(new MeInfo(TYPE_BROADCASTER_VERIFY, R.mipmap.ic_dengji, "我的等级"));
        list.add(new MeInfo(TYPE_ABOUT, -1, null));
        list.add(new MeInfo(TYPE_HOST_RECORD, R.mipmap.ic_renzhegn, "实名认证"));
        list.add(new MeInfo(TYPE_INVITE, R.mipmap.ic_wentifankui, "问题反馈"));
        list.add(new MeInfo(TYPE_FEEDBACK, R.mipmap.ic_shezhi, "设置"));
        return list;
    }

    public void initUserData() {
        mUserInfo = userMvpModel.getUserDate();
        setupUserData();
    }

    public void setupUserData() {
        if (mUserInfo == null) {
            MLog.error(this, "用户信息不存在！");
        }
        getMvpView().updateUserInfoUI(mUserInfo);
    }

    // 检查主播认证状态
    public boolean checkBroadcast(){
        if(mUserInfo.getAnchorStatus() == 4 || mUserInfo.getAnchorStatus() == 2){// 已验证
            return true;
        } else {
            return false;
        }
    }

//    public void clickBy(int viewId, Context context) {
//        switch (viewId) {
//            default:
//                break;
//            case R.id.iv_user_info_more:
//                if (mUserInfo != null) {
//                    UIHelper.showUserInfoAct(context, mUserInfo.getUid());
//                }
//                break;
//            case R.id.me_item_feedback:
//                context.startActivity(new Intent(context, FeedbackActivity.class));
//                break;
//            case R.id.me_item_yumeng:
//                context.startActivity(new Intent(context, AboutActivity.class));
//                break;
//            case R.id.me_item_agreement:
//                UIHelper.showUsinghelp(context);
//                break;
//            case R.id.tv_user_attentions_ll:
//                context.startActivity(new Intent(context, AttentionListActivity.class));
//                break;
//            case R.id.user_fans_ll:
//                context.startActivity(new Intent(context, FansListActivity.class));
//                break;
//            case R.id.me_item_wallet:
//                MyWalletActivity.start(context);
//                break;
//            case R.id.me_item_level:
//                CommonWebViewActivity.start(context, WEB_TITLE_TYPE_RNAKING_THEME, BaseUrl.MY_LEVEL);
//                break;
//            case R.id.me_item_create_my_room: //收益
//                break;
//            case R.id.me_item_reward:
//                context.startActivity(new Intent(context, InviteFriendActivity.class));
//                break;
//            case R.id.me_car:
//                ShopActivity.start(context, true, 0);
//                break;
//            case R.id.btn_login_out:
//                CoreManager.getCore(IAuthCore.class).logout();
//                PreferencesUtils.setFristQQ(true);
//                break;
//            case R.id.me_item_setting:
//                UIHelper.showSettingAct(context);
//                break;
//            case R.id.me_item_verified:
//                CommonWebViewActivity.start(context, BaseUrl.REAL_NAME);
//                break;
//
//            case R.id.me_item_anchor_verified:
//                CommonWebViewActivity.start(context, BaseUrl.ANCHOR_AUTHENTICATION);
//                break;
//
//        }
//    }

    /**
     * 加测是否绑定手机
     */
    public void isPhone() {

        userMvpModel.isPhones(new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onIsBindPhoneFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().onIsBindPhone();
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onIsBindPhoneFail(response.getMessage());
                    }
                }
            }
        });
    }


    /**
     * 检测是否设置过密码
     */
    public void checkPwd() {
        userMvpModel.checkPwd(new OkHttpManager.MyCallBack<Json>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onIsSetPwdFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    if (getMvpView() != null) {
                        getMvpView().onIsSetPwd(response.boo("data"));
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onIsSetPwdFail(response.str("message"));
                    }
                }
            }
        });
    }

    public void checkPwdV2() {
        userMvpModel.checkPwdV2(new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().callbackCheckPwd(response.num("isSetPwd"));
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().callbackCheckPwdFail(msg);
                }
            }

        });
    }

    /**
     * 绑定手机
     */
    public void boundPhone(String phone, String code) {
//        userMvpModel.boundPhone(phone, code, new HttpRequestCallBack<Json>() {
//            @Override
//            public void onSuccess(String message, Json response) {
//                if (getMvpView() != null) {
//                    int code = response.num("code");
//                    boolean dataValue = response.boo("data");
//                    getMvpView().callbackBoundPhone(code, dataValue);
//                }
//            }
//
//            @Override
//            public void onFailure(int code, String msg) {
//                if (getMvpView() != null) {
//                    getMvpView().callbackBoundPhoneFail(msg);
//                }
//
//            }
//        });
        userMvpModel.boundPhone(phone, code, new OkHttpManager.MyCallBack<Json>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().callbackBoundPhoneFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (getMvpView() != null) {
                    int code = response.num("code");
                    boolean dataValue = response.boo("data");
                    getMvpView().callbackBoundPhone(code, dataValue);
                }
            }
        });

    }

    /**
     * @param phone
     */
    public void getSendSms(String phone) {
        userMvpModel.getSendSms(phone, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                LogUtil.i(response.toString());
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().callbackSendSmsFail(msg);

                }
            }
        });
    }

    public void userSetPwd(String phone, String pwd, String confirmPwd, String code) {
        userMvpModel.userSetPwd(phone, pwd, confirmPwd, code, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().callbackUserSetPwdFail(-1, e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (getMvpView() != null) {
                    getMvpView().callbackUserSetPwd(response.num("code"), response.str("message"));
                }

            }
        });
    }


    /**
     * 修改或设置登录密码
     */
    public void modifyPassword(String oldPwd, String newPwd, String confirmPwd, String url) {

        userMvpModel.modifyPassword(oldPwd, newPwd, confirmPwd, url, new OkHttpManager.MyCallBack<ServiceResult>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().modifyPasswordFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().modifyPassword();
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().modifyPasswordFail(response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * 获取修改旧手机验证码
     */
    public void getModifyPhoneSMSCode(String phoneNumber, String type) {

        userMvpModel.getModifyPhoneSMSCode(phoneNumber, type, new OkHttpManager.MyCallBack<ServiceResult>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().getModifyPhoneSMSCodeFail("换绑手机失败!");
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {

                } else {
                    if (getMvpView() != null) {
                        getMvpView().getModifyPhoneSMSCodeFail(response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * //绑定手机
     */
    public void getSMSCode(String phone) {

        userMvpModel.getSMSCode(phone, new OkHttpManager.MyCallBack<ServiceResult>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().getModifyPhoneSMSCodeFail("绑定手机失败!");
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {

                } else {
                    if (getMvpView() != null) {
                        getMvpView().getModifyPhoneSMSCodeFail(response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * 验证手机
     */
    public void getPwSmsCode(String phone) {
        userMvpModel.getPwSmsCode(phone, new OkHttpManager.MyCallBack<ServiceResult>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().getModifyPhoneSMSCodeFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {

                } else {
                    if (getMvpView() != null) {
                        getMvpView().getModifyPhoneSMSCodeFail(response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * 换绑手机
     */
    public void modifyBinderPhone(String phone, String smsCode, String url) {

        userMvpModel.modifyBinderPhone(phone, smsCode, url, new OkHttpManager.MyCallBack<ServiceResult>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onMoidfyOnBinnerFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    IUserCore core = CoreManager.getCore(IUserCore.class);
                    if (core != null && core.getCacheLoginUserInfo() != null) {
                        core.requestUserInfo(core.getCacheLoginUserInfo().getUid());
                    }
                    if (getMvpView() != null) {
                        getMvpView().onModifyOnBinner();
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onMoidfyOnBinnerFail(response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * 验证手机
     */
    public void verifierPhone(String phone, String smsCode) {

        userMvpModel.verifierPhone(phone, smsCode, new OkHttpManager.MyCallBack<ServiceResult>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().verifierPhoneFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().verifierPhone();
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().verifierPhoneFail(response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * 绑定手机
     */
    public void binderPhone(String phone, String smsCode) {

        userMvpModel.bindPhone(phone, smsCode, new OkHttpManager.MyCallBack<ServiceResult>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onBinderPhoneFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    IUserCore core = CoreManager.getCore(IUserCore.class);
                    if (core != null && core.getCacheLoginUserInfo() != null) {
                        core.requestUserInfo(core.getCacheLoginUserInfo().getUid());
                    }
                    if (getMvpView() != null) {
                        getMvpView().onBinderPhone();
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onBinderPhoneFail(response.getMessage());
                    }
                }
            }
        });
    }


    /**
     * 用户提交反馈
     */
    public void commitFeedback(String feedbackDesc, String contact) {

        userMvpModel.commitFeedback(feedbackDesc, contact, new OkHttpManager.MyCallBack<ServiceResult>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().commitFeedbackFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        if (getMvpView() != null) {
                            getMvpView().commitFeedback();
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().commitFeedbackFail(response.getMessage());
                        }
                    }
                }
            }
        });
    }

    private LogModel logModel;

    public void uploadLog() {
        if (logModel == null) {
            logModel = new LogModel();
        }
        logModel.uploadLog(new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean isSuccess) {
                if (getMvpView() != null) {
                    if (isSuccess) {
                        getMvpView().toast("提交成功");
                    }
                    getMvpView().dismissDialog();
                    getMvpView().finish();
                }
            }
        }, -1);
    }

    @Override
    public void onDestroyPresenter() {
        super.onDestroyPresenter();
        if (logModel != null) {
            logModel.finishUpload();
        }
    }

    public void getGiftRankingList(int type, int dateType) {
        mRankingListModel.getXCRankingList(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", type, dateType, new OkHttpManager.MyCallBack<ServiceResult<RankingXCInfo>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<RankingXCInfo> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        getMvpView().showGiftRankingList(response.getData());
                    }
                }
            }
        });
    }


    public void checkCode(String code) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("qrCode", code);
        OkHttpManager.getInstance().getRequest(UriProvider.getAccCheckQrCode(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().checkCodeState(-1, e.getMessage());
                }

            }


            @Override
            public void onResponse(Json response) {
                if (getMvpView() != null) {
                    try {
                        getMvpView().checkCodeState(response.getInt("code"), response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }


    /**
     * 扫一扫 二维码
     *
     * @param code
     */
    public void accessCode(String code) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("qrCode", code);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        OkHttpManager.getInstance().doPostRequest(UriProvider.getAccQrCodeScan(), params, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().accessCodeState(-1, e.getMessage());
                }

            }

            @Override
            public void onResponse(Json response) {
                try {
                    getMvpView().accessCodeState(response.getInt("code"), response.getString("message"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void hasBindPhone() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().getRequest(UriProvider.isPhones(), params, new OkHttpManager.MyCallBack<ServiceResult<String>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null && e != null)
                    getMvpView().hasBindPhoneFail(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<String> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null)
                        getMvpView().hasBindPhone();
                } else {
                    if (getMvpView() != null && response != null)
                        getMvpView().hasBindPhoneFail(response.getErrorMessage());
                }
            }
        });
    }

}
