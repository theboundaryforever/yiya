package com.yiya.mobile.ui.newfind.adapter;


import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.xchat_core.room.bean.NewUserBean;
import com.yiya.mobile.ui.home.adpater.BannerAdapter;
import com.yiya.mobile.ui.home.adpater.LiveStreamRecyclerAdapter;
import com.yiya.mobile.ui.newfind.fragment.SproutingNewFragment;
import com.yiya.mobile.ui.widget.Banner;
import com.yiya.mobile.ui.widget.LiveTabView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.HomeLiveInfo;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.ArrayList;
import java.util.List;

public class SproutingNewRecyclerAdapter extends BaseMultiItemQuickAdapter<SproutingNewFragment.SproutingNewinfo,BaseViewHolder> {

    private Context mContext;

    public interface OnItemClickListener {
        void onItemClick(int itemType, View view, int position, LiveTabView.TabInfo tabInfo);
    }

    private LiveStreamRecyclerAdapter.OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(LiveStreamRecyclerAdapter.OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    public SproutingNewRecyclerAdapter(List<SproutingNewFragment.SproutingNewinfo> data) {
        super(data);
        addItemType(SproutingNewFragment.SproutingNewinfo.ITEM_TYPE_1, R.layout.item_home_live_master_banner);  //广告栏
        addItemType(SproutingNewFragment.SproutingNewinfo.ITEM_TYPE_2, R.layout.item_sproutingnew_information);  //人员信息栏
    }

    private int mItemRoomWidth = 0;

    public SproutingNewRecyclerAdapter(List<SproutingNewFragment.SproutingNewinfo> data, Context mContext) {
        this(data);
        this.mContext = mContext;
        mItemRoomWidth = (DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 30) - DisplayUtils.dip2px(mContext, 10)) / 2;
    }

    public void setData(List<SproutingNewFragment.SproutingNewinfo> data) {
        setNewData(data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SproutingNewFragment.SproutingNewinfo item) {
        switch (item.getItemType()){
            case SproutingNewFragment.SproutingNewinfo.ITEM_TYPE_1:
                handleItemBanner(helper,item);
                break;
            case SproutingNewFragment.SproutingNewinfo.ITEM_TYPE_2:
                handleItemNewUser(helper,item);
                break;
        }
    }

    /**
     * 广告页
     *
     * @param helper
     * @param info
     */
    private void handleItemBanner(BaseViewHolder helper, SproutingNewFragment.SproutingNewinfo info) {
        Banner banner = helper.getView(R.id.item_banner);
        List<BannerInfo> bannerInfoList = new ArrayList<>();
        for (BannerInfo bean : info.getmList1()) {
            BannerInfo bannerInfo = new BannerInfo();
            bannerInfo.setBannerName(bean.getBannerName());
            bannerInfo.setBannerPic(bean.getBannerPic());
            bannerInfo.setSkipType(bean.getSkipType());
            bannerInfo.setSkipUri(bean.getSkipUri());
            bannerInfoList.add(bannerInfo);
        }
        BannerAdapter bannerAdapter = new BannerAdapter(bannerInfoList, mContext);
        banner.setAdapter(bannerAdapter);
        banner.setPlayDelay(3 * 1000);
    }

    /**
     * 萌新列表
     *
     * @param helper
     * @param info
     */
    private void handleItemNewUser(BaseViewHolder helper, SproutingNewFragment.SproutingNewinfo info) {
        helper.getView(R.id.sproutingnew_circleimg);
        TextView tv_name =  helper.getView(R.id.tv_name);
        ImageView img_gender =  helper.getView(R.id.img_gender);
        Button btn_sayhello =  helper.getView(R.id.btn_sayhello);

        tv_name.setText(info.getNewUserBean().getNick());
        if(info.getNewUserBean().getGender()==1){ //1:男
            img_gender.setImageResource(R.mipmap.ic_boy);
        }else if(info.getNewUserBean().getGender()==2){ //2：女
            img_gender.setImageResource(R.mipmap.ic_girl);
        }else{ // 0:未知
            img_gender.setVisibility(View.INVISIBLE);
        }

        helper.getView(R.id.btn_sayhello).setOnClickListener(v -> {
            btn_sayhello.setEnabled(false);
        });

    }
}
