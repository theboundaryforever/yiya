package com.yiya.mobile.ui.widget.avatarStack;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseViewHolder;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/12.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class WhiteAvatarStackAdapter extends AvatarStackAdapter {
    @Override
    protected void convert(BaseViewHolder helper, String url) {
        super.convert(helper, url);
        getAvatarRiv().setBorderColor(Color.WHITE);
    }
}
