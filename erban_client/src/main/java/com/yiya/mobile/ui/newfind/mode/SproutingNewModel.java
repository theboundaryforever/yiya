package com.yiya.mobile.ui.newfind.mode;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

import static com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil.getDefaultParam;

public class SproutingNewModel extends BaseMvpModel {

    /**获取萌新列表数据*/
    public void getmengxin(int mPageNum,int mPageSize,OkHttpManager.MyCallBack myCallBack){
        Map<String, String> params = getDefaultParam();
        params.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("gender", "");
        params.put("pageNum", String.valueOf(mPageNum));
        params.put("pageSize",String.valueOf(mPageSize));
        OkHttpManager.getInstance().doPostRequest(UriProvider.getMengXin(), params,myCallBack);
    }

}
