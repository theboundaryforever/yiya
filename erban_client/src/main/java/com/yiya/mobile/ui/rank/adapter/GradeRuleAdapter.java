package com.yiya.mobile.ui.rank.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;

/**
 * 创建者      Created by dell
 * 创建时间    2018/12/3
 * 描述        ${}
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class GradeRuleAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    private boolean isCharm;

    public GradeRuleAdapter(boolean isCharm) {
        super(R.layout.grade_rule_item);
        this.isCharm = isCharm;
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        ImageView rule = helper.getView(R.id.iv_rule);
        ImageLoadUtils.loadImageRes(rule.getContext(), getDrawable(helper.getAdapterPosition()), rule);
    }

    private int getDrawable(int position) {
        switch (position) {
            case 1:
                return isCharm ? R.drawable.ic_charm_rule_1 : R.drawable.ic_gold_rule_1;
            case 2:
                return isCharm ? R.drawable.ic_charm_rule_2 : R.drawable.ic_gold_rule_2;
            case 3:
                return isCharm ? R.drawable.ic_charm_rule_3 : R.drawable.ic_gold_rule_3;
            case 4:
                return isCharm ? R.drawable.ic_charm_rule_4 : R.drawable.ic_gold_rule_4;
            case 5:
                return isCharm ? R.drawable.ic_charm_rule_5 : R.drawable.ic_gold_rule_5;
            case 6:
                return isCharm ? R.drawable.ic_charm_rule_6 : R.drawable.ic_gold_rule_6;
            case 7:
                return isCharm ? R.drawable.ic_charm_rule_7 : R.drawable.ic_gold_rule_7;
            default:
                return isCharm ? R.drawable.ic_charm_rule_0 : R.drawable.ic_gold_rule_0;
        }
    }

}
