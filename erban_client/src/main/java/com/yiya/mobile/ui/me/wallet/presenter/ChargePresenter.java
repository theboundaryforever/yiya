package com.yiya.mobile.ui.me.wallet.presenter;

import android.content.Context;

import com.google.gson.JsonObject;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.ui.me.wallet.model.PayModel;
import com.yiya.mobile.ui.me.wallet.view.IChargeView;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.pay.bean.ChargeBean;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.NetworkUtils;

import java.util.List;
import java.util.Map;

import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_BANK;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_HC;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_HJ;
import static com.tongdaxing.xchat_core.Constants.CHARGE_TYPE_PPP;

/**
 * Created by MadisonRong on 05/01/2018.
 */
public class ChargePresenter extends PayPresenter<IChargeView> {

    private static final String TAG = "ChargePresenter";

    public ChargePresenter() {
        this.payModel = new PayModel();
    }

    /**
     * 加载用户信息(显示用户账号和钱包余额)
     */
    public void loadUserInfo() {
        UserInfo userInfo = payModel.getUserInfo();
        if (userInfo != null) {
            getMvpView().setupUserInfo(userInfo);
        }
        refreshWalletInfo(false);
    }

    /**
     * 获取充值产品列表
     */
    public void getChargeList() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("channelType", "1");
        OkHttpManager.getInstance().getRequest(UriProvider.getChargeList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<ChargeBean>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().getChargeListFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<ChargeBean>> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().buildChargeList(response.getData());
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().getChargeListFail(response == null ? "数据异常" : response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * 显示充值选项(支付宝或者微信)
     */
    public void showChargeOptionsDialog() {
        if (getMvpView() != null)
            getMvpView().displayChargeOptionsDialog();

    }
    /**
     * 发起充值
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_ALIPAY} 和
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_WX}
     */
    public void requestCharge(Context context, String chargeProdId, String payChannel) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("clientIp", NetworkUtils.getIPAddress(context));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.requestCharge(), param, new OkHttpManager.MyCallBack<ServiceResult<JsonObject>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().getChargeOrOrderInfoFail(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfo(response.getData().toString(), false, false);
                } else {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfoFail(response == null ? "数据异常" : response.getMessage());
                }
            }
        });
    }

    /**
     * 发起充值
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_ALIPAY} 和
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_WX}
     */
    public void requestCharge(Context context, String chargeProdId, String payChannel, int payType) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        String url = null;
        switch (payType){
            case CHARGE_TYPE_PPP:
                url = UriProvider.requestCharge();
                break;
            case CHARGE_TYPE_HC:
                url = UriProvider.requestChargeHC();
                break;
            case CHARGE_TYPE_HJ:
                url = UriProvider.requestChargeHJ();
                break;
            case CHARGE_TYPE_BANK:
                param.put("successUrl", BaseUrl.BANK_PAY);
                url = UriProvider.requestChargeBank();
                break;
        }

        OkHttpManager.getInstance().doPostRequest(url, param, new OkHttpManager.MyCallBack<ServiceResult<JsonObject>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().getChargeOrOrderInfoFail(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfo(response.getData().toString(), payType);
                } else {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfoFail(response == null ? "数据异常" : response.getMessage());
                }
            }
        });
    }

    /**
     * 发起汇聚充值
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道
     *
     */
    public void requestChargeHJ(Context context, String chargeProdId, String payChannel) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", chargeProdId);
        param.put("payChannel", payChannel);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.requestChargeHJ(), param, new OkHttpManager.MyCallBack<ServiceResult<JsonObject>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().getChargeOrOrderInfoFail(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfo(response.getData().toString(), true, false);
                } else {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfoFail(response == null ? "数据异常" : response.getMessage());
                }
            }
        });
    }

    /**
     * 发起汇潮
     * @param context context
     * @param chargeProdId 充值产品 ID
     * @param payChannel 充值渠道
     */
    public void requestChargeHC(Context context, String chargeProdId, String payChannel) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("chargeProdId", chargeProdId);
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().doPostRequest(UriProvider.requestChargeHC(), param, new OkHttpManager.MyCallBack<ServiceResult<JsonObject>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().getChargeOrOrderInfoFail(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfo(response.getData().toString(), false, true);
                } else {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfoFail(response == null ? "数据异常" : response.getMessage());
                }
            }
        });
    }

    //启动汇聚支付
    public void joinPay(Context context, String data) {
        try {
            Json json = new Json(data);
            String appId = json.getString("appId");
            String prepayId = json.getString("prepayId");
            String partnerId = json.getString("partnerId");
            String nonceStr = json.getString("nonceStr");
            String timeStamp = json.getString("timeStamp");
            String sign = json.getString("hmac");
            PayReq request = new PayReq();
            request.appId = appId;
            request.partnerId = partnerId;
            request.prepayId = prepayId;
            request.packageValue = "Sign=WXPay";
            request.nonceStr = nonceStr;
            request.timeStamp = timeStamp;
            request.sign = sign;

            IWXAPI api = WXAPIFactory.createWXAPI(context, appId, false);
            api.registerApp(appId);
            api.sendReq(request);
        } catch (Exception e) {
            e.printStackTrace();
            getMvpView().toast("拉起微信支付失败");
        }
    }
}
