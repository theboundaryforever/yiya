package com.yiya.mobile.ui.me.user.adapter;

import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class UserContributorAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public UserContributorAdapter() {
        super(R.layout.item_user_contributor);
    }

    @Override
    protected void convert(BaseViewHolder helper, String picUrl) {
        int resId = -1;
        switch (helper.getAdapterPosition()) {
            case 0:
                resId = R.drawable.bg_crown_gold;
                break;
            case 1:
                resId = R.drawable.bg_crown_silver;
                break;
            case 2:
                resId = R.drawable.bg_crown_bronze;
                break;
        }
        helper.setVisible(R.id.iv_crown, resId != -1);
        if (resId != -1) {
            helper.setImageResource(R.id.iv_crown, resId);
        }
        if (TextUtils.isEmpty(picUrl)) {
            return;
        }
        ImageLoadUtils.loadAvatar(mContext, picUrl, helper.getView(R.id.iv_avatar), true);
    }
}
