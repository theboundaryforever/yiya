package com.yiya.mobile.ui.me.user.activity;

import com.yiya.mobile.ui.home.adpater.HomeIndicatorAdapter;
import com.yiya.mobile.utils.UIHelper;
import com.tongdaxing.erban.R;

public class YiYaUserInfoActivity extends BaseUserInfoActivity implements HomeIndicatorAdapter.OnItemSelectListener {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_user_info;
    }

    @Override
    public void initToolBar() {
        super.initToolBar();
        mToolBar.setOnRightImgBtnClickListener(() ->
                UIHelper.showUserInfoModifyAct(this, mUserId));

    }

}
