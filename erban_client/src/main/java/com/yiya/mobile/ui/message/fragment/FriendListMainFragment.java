package com.yiya.mobile.ui.message.fragment;

import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.message.MessageFriendListPresenter;
import com.yiya.mobile.presenter.message.MessageFriendListView;
import com.yiya.mobile.ui.message.adapter.FriendListAdapter;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCore;
import com.tongdaxing.xchat_core.im.friend.IIMFriendCoreClient;
import com.tongdaxing.xchat_core.user.IUserClient;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;

/**
 * @author Zhangsongzhou
 * @date 2019/3/22
 */

@CreatePresenter(MessageFriendListPresenter.class)
public class FriendListMainFragment extends BaseMvpFragment<MessageFriendListView, MessageFriendListPresenter> implements MessageFriendListView, BaseQuickAdapter.OnItemClickListener {


    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.emptyBg)
    LinearLayout mEmptyBgLl;


    private FriendListAdapter mAdapter;


    private FriendHandler mHandler = new FriendHandler(this);
    private String mItemClickUid;


    @Override

    public int getRootLayoutId() {
        return R.layout.fragment_friend_list_main;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void initiate() {
        GrowingIO.getInstance().setPageName(this, "消息-好友");
//        mAdapter = new FriendListRecyclerAdapter(application, R.layout.item_message_chat);
        mAdapter = new FriendListAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(), 500);

//        List<NimUserInfo> userInfos = CoreManager.getCore(IIMFriendCore.class).getMyFriends();
//        setData(userInfos);

    }

    @CoreEvent(coreClientClass = IIMFriendCoreClient.class)
    public void onFriendListUpdate(List<NimUserInfo> userInfos) {
        setData(userInfos);
    }

    @CoreEvent(coreClientClass = IUserClient.class)
    public void onCurrentUserInfoUpdate(UserInfo userInfo) {
        showLoading();
        mHandler.sendMessageDelayed(mHandler.obtainMessage(), 500);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (mAdapter == null || ListUtils.isListEmpty(mAdapter.getData())) {
            toast("数据异常请稍后重试");
            return;
        }
        if (TextUtils.isEmpty(mItemClickUid)) {
            mItemClickUid = mAdapter.getData().get(position).getAccount();
            getMvpPresenter().checkBlackList(mItemClickUid);
        }
//            NimUIKit.startP2PSession(mContext, mAdapter.getData().get(position).getAccount());
    }

    @Override
    public void onCheckBlacklist(boolean checkBothSides) {
        if (checkBothSides) {
            toast("已拉黑，无法聊天");
        } else {
            NimUIKit.startP2PSession(mContext, mItemClickUid);
        }
        mItemClickUid = "";
    }

    @Override
    public void onCheckBlacklistFailed(String message) {
        toast(message);
        mItemClickUid = "";
    }

    static class FriendHandler extends Handler {

        private WeakReference<FriendListMainFragment> mReference;

        FriendHandler(FriendListMainFragment fragment) {
            this.mReference = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (mReference == null || mReference.get() == null) {
                return;
            }
            List<NimUserInfo> userInfos = CoreManager.getCore(IIMFriendCore.class).getMyFriends();
            mReference.get().setData(userInfos);
        }
    }

    private void setData(List<NimUserInfo> userInfos) {
        if (userInfos != null) {
            mAdapter.setNewData(userInfos);
        }
        if (userInfos == null || userInfos.size() == 0) {
            mEmptyBgLl.setVisibility(View.VISIBLE);
        } else {
            mEmptyBgLl.setVisibility(View.GONE);
        }
    }


}
