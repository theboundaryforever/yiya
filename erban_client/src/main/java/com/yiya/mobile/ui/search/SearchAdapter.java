package com.yiya.mobile.ui.search;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yiya.mobile.base.callback.BaseAdapterItemListener;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;

/**
 * Created by chenran on 2017/10/3.
 */

public class SearchAdapter extends BaseQuickAdapter<HomeRoom, BaseViewHolder> {

    private Context context;

    public SearchAdapter(Context context) {
        super(R.layout.list_item_search);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder holder, HomeRoom item) {
        ImageView avatar = holder.getView(R.id.avatar);
        ImageView iv_gender = holder.getView(R.id.iv_gender);
        TextView roomType = holder.getView(R.id.room_type);
        ImageLoadUtils.loadCircleImage(avatar.getContext(), item.getAvatar(), avatar,R.drawable.nim_avatar_default);
        Log.d("搜索页面查询的人物信息","HomeRoom :"+item.toString());
        if(item.getGender()==1){//男
            iv_gender.setImageResource(R.mipmap.ic_boy);
        }else if(item.getGender()==2){//1:男 2:女 0:未知
            iv_gender.setImageResource(R.mipmap.ic_girl);
        }
        Log.d("搜索页面查询的人物信息","状态信息:isValid="+item.isValid()+",Type="+item.getType());
        if(item.isValid() && (item.getType() == RoomInfo.ROOMTYPE_HOME_PARTY) ){
            roomType.setVisibility(View.VISIBLE);
            roomType.setText("房间中");
        } else if(item.isValid() && (item.getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE)){
            roomType.setVisibility(View.VISIBLE);
            roomType.setText("直播中");
        } else {
            roomType.setText("关注");
        }
        holder.getView(R.id.room_type).setOnClickListener(v->{
            RoomFrameActivity.start(context, item.getUid(), item.getType());
        });

        holder.setText(R.id.user_name, item.getNick())
                .setText(R.id.erban_no, "ID:" + item.getErbanNo())
                .setOnClickListener(R.id.container, view -> {
                    UIHelper.showUserInfoAct(context, item.getUid());
                    if (baseAdapterItemListener != null) {
                        baseAdapterItemListener.onItemClick(holder.getAdapterPosition());
                    }
                });
    }

    public BaseAdapterItemListener baseAdapterItemListener;
}
