package com.yiya.mobile.ui.home.fragment;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.constant.AppConstant;
import com.yiya.mobile.constant.BaseUrl;
import com.yiya.mobile.presenter.home.HomeYuChatPresenter;
import com.yiya.mobile.presenter.home.HomeYuChatView;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.home.adpater.BannerAdapter;
import com.yiya.mobile.ui.home.adpater.ChangeImageIndicatorAdapter;
import com.yiya.mobile.ui.home.adpater.YuChatIndicatorAdapter;
import com.yiya.mobile.ui.web.CommonWebViewActivity;
import com.yiya.mobile.ui.widget.Banner;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.home.RankInfo;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.home.YuChatFeatureInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */
@CreatePresenter(HomeYuChatPresenter.class)
public class HomeYuChatFragment extends BaseMvpFragment<HomeYuChatView, HomeYuChatPresenter> implements HomeYuChatView, ChangeImageIndicatorAdapter.OnItemSelectListener, View.OnClickListener, AppBarLayout.OnOffsetChangedListener, OnRefreshListener {

    private static final String TAG = "HomeYuChatFragment";

    @BindView(R.id.srl_home_refresh)
    SmartRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.abl_home_head)
    AppBarLayout mAppBarLayout;

    @BindView(R.id.vp_home_content)
    ViewPager mViewPager;
    @BindView(R.id.item_feature_banner_include)
    LinearLayout mFeatureBannerInclude;

    @BindView(R.id.banner_yu_chat_title_card_view)
    CardView mBannerCardView;
    @BindView(R.id.banner_yu_chat_title)
    Banner mBanner;
    @BindView(R.id.item_rank_flipper)
    ViewFlipper mViewFlipper;


    //指示器
    @BindView(R.id.home_yu_chat_indicator)
    MagicIndicator mMagicIndicator;

    @BindView(R.id.item_feature_banner_recycler_view)
    RecyclerView mFeatureRecyclerView;

    private FeatureRecyclerAdapter mFeatureRecyclerAdapter;


    private BaseIndicatorStateAdapter mIndicatorStateAdapter;
    private YuChatIndicatorAdapter mYuChatIndicatorAdapter;
    private CommonNavigator mCommonNavigator;
    private List<Fragment> mFragmentList = new ArrayList<>();
    private List<TabInfo> mTabInfoList = new ArrayList<>();


    private int mPosition = 0;

    private int mItemPadding = 0;
    private int mItemWidth = 0;

    private String mLastSelectTabValue = "";


    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_home_yu_chat;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mFragmentList == null || mFragmentList.size() == 0) {
            getMvpPresenter().getYuChatBanner();
            getMvpPresenter().getFeatureBannerList(Constants.PAGE_START);
            getMvpPresenter().getYuChatMenuList();
        }
    }

    @Override
    public void onSetListener() {
        mAppBarLayout.addOnOffsetChangedListener(this);

        mSwipeRefreshLayout.setEnableLoadMore(false);
        mSwipeRefreshLayout.setEnableFooterTranslationContent(false);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initiate() {
//        GrowingIO.getInstance().setPageName(this, "首页-语聊");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "语聊List页");

        mItemWidth = (DisplayUtils.getScreenWidth(mContext) - DisplayUtils.dip2px(mContext, 44)) / 3;

        mFeatureRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        mFeatureRecyclerAdapter = new FeatureRecyclerAdapter(-1);
        mFeatureRecyclerView.setAdapter(mFeatureRecyclerAdapter);
        mFeatureRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                mItemPadding = (parent.getMeasuredWidth() - (mItemWidth * 3)) / 4;
                outRect.bottom = mItemPadding * 2;
                outRect.right = 0;
                outRect.left = 0;
                outRect.top = 0;
//                LogUtil.d(TAG, "ItemWidth:" + DisplayUtils.px2dip(mContext, view.getMeasuredWidth()) + "\t" + "mItemPadding:" + mItemPadding + "\t" + "mItemWidth:" + mItemWidth);
                int pos = parent.getChildAdapterPosition(view);


                if ((pos + 1) % 3 == 1) {//
                    outRect.right = mItemPadding / 2;
                } else if ((pos + 1) % 3 == 0) {//
                    outRect.left = mItemPadding * 3 / 2;
                } else {
                    outRect.right = mItemPadding / 2;
                    outRect.left = mItemPadding;
                }

            }
        });

        mFeatureRecyclerAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                YuChatFeatureInfo info = mFeatureRecyclerAdapter.getData().get(position);
                if (info == null) {
                    return;
                }
                if (info.isWaitingForRoom()) {
                    CommonWebViewActivity.start(getActivity(), info.getWaitingForRoomUrl());
                } else {
                    RoomFrameActivity.start(getActivity(), info.getUid(), mFeatureRecyclerAdapter.getData().get(position).getType());
                }

            }
        });
    }


    private void initIndicator(List<TabInfo> tabInfoList) {
        if (mFragmentList.size() > 0) {
            for (Fragment fragment : mFragmentList) {
                fragment = null;
            }
            System.gc();
        }

        mFragmentList.clear();
        mTabInfoList.clear();

        List<TabInfo> tmpTabInfoList = new ArrayList<>();

        for (TabInfo tabInfo : tabInfoList) {
            HomeYuChatListFragment chatListFragment = new HomeYuChatListFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("tabId", tabInfo.getId());
            chatListFragment.setArguments(bundle);
            tmpTabInfoList.add(tabInfo);
            mFragmentList.add(chatListFragment);
            mTabInfoList.add(tabInfo);
            if (!TextUtils.isEmpty(mLastSelectTabValue) && mLastSelectTabValue.equals(tabInfo.getName())) {
                mPosition = tabInfoList.indexOf(tabInfo);
            }
        }

        if (mYuChatIndicatorAdapter != null) {
            mYuChatIndicatorAdapter = null;
        }
        mYuChatIndicatorAdapter = new YuChatIndicatorAdapter(mContext, tmpTabInfoList);
        mYuChatIndicatorAdapter.setOnIndicatorSelectListener(new YuChatIndicatorAdapter.OnIndicatorSelectListener() {
            @Override
            public void onIndicatorSelect(TabInfo info, int position) {
                mPosition = position;
                mViewPager.setCurrentItem(mPosition, true);
                mLastSelectTabValue = info.getName();
            }
        });

        if (mCommonNavigator != null) {
            mCommonNavigator = null;
        }
        mCommonNavigator = new CommonNavigator(mContext);
        mCommonNavigator.setAdapter(mYuChatIndicatorAdapter);
        mCommonNavigator.setAdjustMode(false);
        mMagicIndicator.setNavigator(mCommonNavigator);
        LinearLayout titleContainer = mCommonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);

        if (mIndicatorStateAdapter != null) {
            mIndicatorStateAdapter = null;
        }
        mIndicatorStateAdapter = new BaseIndicatorStateAdapter(getChildFragmentManager(), mFragmentList);
        mViewPager.setAdapter(mIndicatorStateAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                try {
                    mPosition = i;
                    mLastSelectTabValue = tabInfoList.get(i).getName();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        ViewPagerHelper.bind(mMagicIndicator, mViewPager);

        mMagicIndicator.onPageSelected(mPosition);
        mViewPager.setCurrentItem(mPosition, false);

        mSwipeRefreshLayout.finishRefresh();

    }

    public void setPagerScrollByTag(String tag) {
        try {
            for (TabInfo tabInfo : mTabInfoList) {
                if (!TextUtils.isEmpty(tabInfo.getName()) && tabInfo.getName().equals(tag)) {
                    mViewPager.setCurrentItem(mTabInfoList.indexOf(tabInfo), true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        getData();
    }

    @Override
    public void onItemSelect(int position) {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        boolean value = false;
        if (verticalOffset >= 0) {
            value = true;
        } else {
            value = false;
        }
        mSwipeRefreshLayout.setEnabled(value);

    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        getData();
    }

    private void getData() {
        getMvpPresenter().getYuChatBanner();
        getMvpPresenter().getFeatureBannerList(Constants.PAGE_START);
        getMvpPresenter().getRankList();
        getMvpPresenter().getYuChatMenuList();
    }


    @Override
    public void onTitleBannerSucceed(List<BannerInfo> data) {

        updateBannerView(data);

    }

    @Override
    public void onTitleBannerFailed(String message) {
        updateBannerView(null);

    }

    private void updateBannerView(List<BannerInfo> data) {
        if (data == null || data.size() == 0) {
            mBannerCardView.setVisibility(View.GONE);
        } else {
            mBannerCardView.setVisibility(View.VISIBLE);
            mBanner.setPlayDelay(3000);//开始轮播动画
        }
        if (data != null) {
            BannerAdapter bannerAdapter = new BannerAdapter(data, mContext);
            mBanner.setAdapter(bannerAdapter);
        }
    }

    @Override
    public void onFeatureSucceed(List<YuChatFeatureInfo> data) {
        if (data == null || data.size() <= 0) {
            mFeatureBannerInclude.setVisibility(View.GONE);
        } else {
            mFeatureRecyclerAdapter.setNewData(data);
            mFeatureBannerInclude.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFeatureFailed(String message) {
        mFeatureBannerInclude.setVisibility(View.GONE);

    }

    @Override
    public void onMenuListSucceed(List<TabInfo> data) {
        initIndicator(data);


    }

    @Override
    public void onMenuListFailed(String message) {

    }

    @Override
    public void onRankList(RankInfo info) {
        setRankState(info);

    }

    private void setRankState(RankInfo info) {
        if (info == null) {
            mViewFlipper.setVisibility(View.GONE);
            return;
        }
        mViewFlipper.removeAllViews();
        mViewFlipper.setVisibility(View.VISIBLE);
        int tmp = 0;
        if (!ListUtils.isListEmpty(info.getWealthRank())) {
            View wealthView = View.inflate(mContext, R.layout.item_home_live_rank_wealth_flipper, null);
            mViewFlipper.addView(wealthView);
            ((TextView) wealthView.findViewById(R.id.rank_name_tv)).setText("土豪榜");
            wealthView.setOnClickListener(v -> CommonWebViewActivity.start(mContext, AppConstant.WEB_TITLE_TYPE_RNAKING_THEME, BaseUrl.ROOM_RANK));
            tmp++;
            try {
                ImageView firstIv = wealthView.findViewById(R.id.rank_first_iv);
                ImageView secondIv = wealthView.findViewById(R.id.rank_second_iv);
                ImageView thirdIv = wealthView.findViewById(R.id.rank_third_iv);
                ImageLoadUtils.loadCircleImage(mContext, info.getWealthRank().get(0).getAvatar(), thirdIv, R.drawable.nim_avatar_default);
                ImageLoadUtils.loadCircleImage(mContext, info.getWealthRank().get(1).getAvatar(), secondIv, R.drawable.nim_avatar_default);
                ImageLoadUtils.loadCircleImage(mContext, info.getWealthRank().get(2).getAvatar(), firstIv, R.drawable.nim_avatar_default);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!ListUtils.isListEmpty(info.getCharmRank())) {
            View charmView = View.inflate(mContext, R.layout.item_home_live_rank_charm_flipper, null);
            mViewFlipper.addView(charmView);
            ((TextView) charmView.findViewById(R.id.rank_name_tv)).setText("心动榜");
            charmView.setOnClickListener(v -> CommonWebViewActivity.start(mContext, AppConstant.WEB_TITLE_TYPE_RNAKING_THEME, BaseUrl.ROOM_RANK));
            tmp++;
            try {
                ImageView firstIv = charmView.findViewById(R.id.rank_first_iv);
                ImageView secondIv = charmView.findViewById(R.id.rank_second_iv);
                ImageView thirdIv = charmView.findViewById(R.id.rank_third_iv);
                ImageLoadUtils.loadCircleImage(mContext, info.getCharmRank().get(0).getAvatar(), thirdIv, R.drawable.nim_avatar_default);
                ImageLoadUtils.loadCircleImage(mContext, info.getCharmRank().get(1).getAvatar(), secondIv, R.drawable.nim_avatar_default);
                ImageLoadUtils.loadCircleImage(mContext, info.getCharmRank().get(2).getAvatar(), firstIv, R.drawable.nim_avatar_default);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tmp > 1) {
            mViewFlipper.startFlipping();
        }

    }


    @Override
    public void onRankListFailed(String message) {
        toast(message);
        setRankState(null);
    }

    private class FeatureRecyclerAdapter extends BaseQuickAdapter<YuChatFeatureInfo, BaseViewHolder> {

        public FeatureRecyclerAdapter(int layoutResId) {
            super(R.layout.item_yu_chat_feature_layout);

        }

        @Override
        protected void convert(BaseViewHolder helper, YuChatFeatureInfo item) {


            SVGAImageView svgaWave = helper.getView(R.id.svga_wave);
            String source = "audio.svga";
            SVGAParser parser = new SVGAParser(mContext);
            parser.decodeFromAssets(source, new SVGAParser.ParseCompletion() {
                @Override
                public void onComplete(@NotNull SVGAVideoEntity svgaVideoEntity) {
                    SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                    svgaWave.setImageDrawable(drawable);
                    svgaWave.startAnimation();
                }

                @Override
                public void onError() {

                }
            });

            ImageView avatarIv = helper.getView(R.id.item_avatar_riv);

            ViewGroup.LayoutParams avatarParams = avatarIv.getLayoutParams();
            avatarParams.width = mItemWidth;
            avatarParams.height = mItemWidth;
            avatarIv.setLayoutParams(avatarParams);

            FrameLayout bgFrameLayout = helper.getView(R.id.item_bg_rl);
            GridLayoutManager.LayoutParams bgParams = (GridLayoutManager.LayoutParams) bgFrameLayout.getLayoutParams();
            bgParams.width = avatarParams.width;
            bgParams.height = bgParams.width;
            bgFrameLayout.setLayoutParams(bgParams);

            ((TextView) helper.getView(R.id.item_name_tv)).setText(item.getTitle());
            ImageLoadUtils.loadImage(mContext, item.getAvatar(), avatarIv);
            ImageLoadUtils.loadImage(mContext, item.getTagPict(), helper.getView(R.id.item_tag_iv));
            if (item.isWaitingForRoom()) {
                helper.getView(R.id.svga_wave).setVisibility(View.INVISIBLE);
                helper.getView(R.id.item_num_tv).setVisibility(View.INVISIBLE);

            } else {
                helper.getView(R.id.svga_wave).setVisibility(View.VISIBLE);
                helper.getView(R.id.item_num_tv).setVisibility(View.VISIBLE);
            }
            ((TextView) helper.getView(R.id.item_num_tv)).setText(StringUtils.transformToW(item.getOnlineNum()) + "人");


        }
    }

}
