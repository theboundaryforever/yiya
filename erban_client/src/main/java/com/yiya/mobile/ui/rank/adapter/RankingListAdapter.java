package com.yiya.mobile.ui.rank.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> 排行榜adapter </p>
 *
 * @author dell
 */
public class RankingListAdapter extends BaseQuickAdapter<RankingXCInfo.ListBean, BaseViewHolder> {

    private Context context;
    private View headerView;
    //头部数据个数
    private final static int HEADER_VIEW_DATA_COUNT = 3;
    private int rankingType;

    private View.OnClickListener onHeaderChildViewClickListener;

    public RankingListAdapter(int layoutRes, Context context, int rankingType) {
        super(layoutRes);
        this.context = context;
        this.rankingType = rankingType;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
    }

    @Override
    protected void convert(BaseViewHolder baseViewHolder, RankingXCInfo.ListBean info) {
        if (info == null) {
            return;
        }
        ((TextView) baseViewHolder.getView(R.id.ranking_number)).setText(String.valueOf(
                HEADER_VIEW_DATA_COUNT + (baseViewHolder.getAdapterPosition() - getHeaderLayoutCount() + 1)));
        ImageLoadUtils.loadAvatar(context, info.getAvatar(), baseViewHolder.getView(R.id.avatar));
        ((TextView) baseViewHolder.getView(R.id.nickname)).setText(info.getNick());

        TextView numView = (TextView) baseViewHolder.getView(R.id.number);
        if(info.getSumGold() > 0){
            numView.setText(StringUtils.UnitConversion(info.getSumGold()));
        } else {
            numView.setText("0");
        }

        ((ImageView)baseViewHolder.getView(R.id.gender)).
                setImageDrawable(info.getGender() == 1 ?
                                context.getResources().getDrawable(R.mipmap.ic_ranking_male) :
                                context.getResources().getDrawable(R.mipmap.ic_ranking_female));

        //等级
        ImageView ivUserLevel = baseViewHolder.getView(R.id.iv_user_level);
//        ImageView ivExperLevel = baseViewHolder.getView(R.id.iv_exper_level);
//        ImageView ivCharmLevel = baseViewHolder.getView(R.id.iv_charm_level);

        //用户等级
        if(ivUserLevel != null){
            if (StringUtils.isNotEmpty(info.getVideoRoomExperLevelPic())) {
                ivUserLevel.setVisibility(View.VISIBLE);
                ImageLoadUtils.loadImage(context, info.getVideoRoomExperLevelPic(), ivUserLevel);
            } else {
                ivUserLevel.setVisibility(View.GONE);
            }
        }
//        // 财富等级
//        if(ivExperLevel != null){
//            if (StringUtils.isNotEmpty(info.getExperLevelPic())) {
//                ivExperLevel.setVisibility(View.VISIBLE);
//                ImageLoadUtils.loadImage(context, info.getExperLevelPic(), ivExperLevel);
//            } else {
//                ivExperLevel.setVisibility(View.GONE);
//            }
//        }

//        // 魅力等级
//        if(ivCharmLevel != null){
//            if (StringUtils.isNotEmpty(info.getCharmLevelPic())) {
//                ivCharmLevel.setVisibility(View.VISIBLE);
//                ImageLoadUtils.loadImage(context, info.getCharmLevelPic(), ivCharmLevel);
//            } else {
//                ivCharmLevel.setVisibility(View.GONE);
//            }
//        }

//        if (info.getDistance() == 0) {
//            ((TextView) baseViewHolder.getView(R.id.number)).setText("----");
//        } else {
//            ((TextView) baseViewHolder.getView(R.id.number)).setText(NumberFormatUtils.formatDoubleDecimalPoint(info.getDistance()));
//        }
    }

    @Override
    public int setHeaderView(View headerView) {
        this.headerView = headerView;
        return super.setHeaderView(headerView);
    }

    @Override
    public void setNewData(@Nullable List<RankingXCInfo.ListBean> dataList) {
        //这里拆分头部和adapter部分显示
        List<RankingXCInfo.ListBean> headerDataList = new ArrayList<>();
        List<RankingXCInfo.ListBean> adapterDataList = new ArrayList<>();
        if (dataList != null) {
            //头部显示不完
            if (dataList.size() > HEADER_VIEW_DATA_COUNT) {
                headerDataList.addAll(dataList.subList(0, HEADER_VIEW_DATA_COUNT));
                adapterDataList.addAll(dataList.subList(HEADER_VIEW_DATA_COUNT, dataList.size()));
            } else {
                headerDataList.addAll(dataList);
            }
        }
        setupHeaderView(headerDataList);
        super.setNewData(adapterDataList);
    }

    /**
     * 显示头部
     * （数据空的时候各项显示为空，并不是都隐藏）
     */
    private void setupHeaderView(List<RankingXCInfo.ListBean> headerDataList) {
        setupHeaderChildView(headerDataList != null && headerDataList.size() > 0 ? headerDataList.get(0) : null,
                R.id.item_header_first_avatar_civ, R.id.item_header_first_name_tv, R.id.item_header_first_user_level_view,R.id.item_header_first_exper_level_view,
                R.id.item_header_first_gender,R.id.item_header_first_num_tv);

        setupHeaderChildView(headerDataList != null && headerDataList.size() > 1 ? headerDataList.get(1) : null,
                R.id.item_header_second_avatar_civ, R.id.item_header_second_name_tv, R.id.item_header_second_user_level_view,R.id.item_header_second_exper_level_view,
                R.id.item_header_second_gender,R.id.item_header_second_num_tv);


        setupHeaderChildView(headerDataList != null && headerDataList.size() > 2 ? headerDataList.get(2) : null,
                R.id.item_header_third_avatar_civ, R.id.item_header_third_name_tv, R.id.item_header_third_user_level_view,R.id.item_header_third_exper_level_view,
                R.id.item_header_third_gender,R.id.item_header_third_num_tv);
    }

    /**
     * 显示头部每一个子view
     */
    private void setupHeaderChildView(RankingXCInfo.ListBean info, int avatarId, int nicknameId, int userLevelViewId, int experLevelViewId, int genderId,int numberId) {
        try {
            if (info != null) {
                ImageLoadUtils.loadAvatar(context, info.getAvatar(), headerView.findViewById(avatarId));
                headerView.findViewById(avatarId).setTag(info.getCtrbUid());

                headerView.findViewById(avatarId).setOnClickListener(onHeaderChildViewClickListener);

                TextView tvNick = (TextView)headerView.findViewById(nicknameId);
                if(tvNick != null && info.getNick() != null){
                    tvNick.setText(info.getNick().length() > 5 ? info.getNick().substring(0, 5).concat("...") : info.getNick());
                }

                ((TextView) headerView.findViewById(numberId)).setText(StringUtils.transformToW(info.getGoldSum() == -1 ? info.getSumGold() : info.getGoldSum()));

                ((ImageView)headerView.findViewById(genderId)).
                        setImageDrawable(info.getGender() == 1 ?
                                context.getResources().getDrawable(R.mipmap.ic_ranking_male) :
                                context.getResources().getDrawable(R.mipmap.ic_ranking_female));
                //等级
                ImageView ivUserLevel = headerView.findViewById(userLevelViewId);
                ImageView ivExperLevel = headerView.findViewById(experLevelViewId);
//                ImageView ivCharmLevel = headerView.findViewById(R.id.iv_charm_level);

                //用户等级
                if(ivUserLevel != null){
                    if (StringUtils.isNotEmpty(info.getVideoRoomExperLevelPic())) {
                        ivUserLevel.setVisibility(View.VISIBLE);
                        ImageLoadUtils.loadImage(mContext, info.getVideoRoomExperLevelPic(), ivUserLevel);
                    } else {
                        ivUserLevel.setVisibility(View.GONE);
                    }
                }
//                // 财富等级
//                if(ivExperLevel != null){
//                    if (StringUtils.isNotEmpty(info.getExperLevelPic())) {
//                        ivExperLevel.setVisibility(View.VISIBLE);
//                        ImageLoadUtils.loadImage(mContext, info.getExperLevelPic(), ivExperLevel);
//                    } else {
//                        ivExperLevel.setVisibility(View.GONE);
//                    }
//                }

//                // 魅力等级
//                if(ivCharmLevel != null){
//                    if (StringUtils.isNotEmpty(info.getCharmLevelPic())) {
//                        ivCharmLevel.setVisibility(View.VISIBLE);
//                        ImageLoadUtils.loadImage(mContext, info.getCharmLevelPic(), ivCharmLevel);
//                    } else {
//                        ivCharmLevel.setVisibility(View.GONE);
//                    }
//                }

//            if (info.getDistance() == 0) {
//                ((TextView) headerView.findViewById(numberId)).setText("----");
//            } else {
//                ((TextView) headerView.findViewById(numberId)).setText(NumberFormatUtils.formatDoubleDecimalPoint(info.getDistance()));
//            }
            } else {//数据空的时候各项显示为空
//                ImageLoadUtils.loadImage(context, R.drawable.nim_avatar_default, headerView.findViewById(avatarId));
                headerView.findViewById(avatarId).setOnClickListener(null);
                ((TextView) headerView.findViewById(nicknameId)).setText("");
                headerView.findViewById(userLevelViewId).setVisibility(View.GONE);
                ((TextView) headerView.findViewById(numberId)).setText("0");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOnHeaderChildViewClickListener(View.OnClickListener onHeaderChildViewClickListener) {
        this.onHeaderChildViewClickListener = onHeaderChildViewClickListener;
    }
}
