package com.yiya.mobile.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yiya.mobile.utils.ImageLoadUtils;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/7/2
 */
public class LiveTabView extends FrameLayout {
    private static final String TAG = "LiveTabView";


    public LiveTabView(Context context) {
        super(context);
        init(context);
    }

    private OnTabClickListener mListener;
    private LinearLayout mParentLinearLayout;
    private List<LinearLayout> mLinearLayoutList = new ArrayList<>();
    private List<TabInfo> mData = new ArrayList<>();
    private Context mContext;

    private int mTwoFontWidth = 0;
    private int mThreeFontWidth = 0;
    private int mPaddingRight = 0;

    public LiveTabView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    private void init(Context context) {
        mContext = context;

        mTwoFontWidth = DisplayUtils.dip2px(mContext, 54);
        mThreeFontWidth = DisplayUtils.dip2px(mContext, 67);
        mPaddingRight = DisplayUtils.dip2px(mContext, 10);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mParentLinearLayout = new LinearLayout(mContext);
        mParentLinearLayout.setLayoutParams(params);
        mParentLinearLayout.setOrientation(LinearLayout.VERTICAL);
        addView(mParentLinearLayout);
    }

    public void setData(List<TabInfo> data) {

        mData.clear();
        mLinearLayoutList.clear();

        if (data.size() > 9) {
            mData.addAll(data.subList(0, 9));
        } else {
            mData.addAll(data);
        }


        mParentLinearLayout.removeAllViews();


        int index = -1;
        for (int i = 0; i < mData.size(); i++) {
            boolean isAdd = false;

            if (i % 5 == 0) {
                LinearLayout childLinearLayout = new LinearLayout(mParentLinearLayout.getContext());
                childLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                childLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                mLinearLayoutList.add(childLinearLayout);
                index++;
                mParentLinearLayout.addView(childLinearLayout);

            }
            isAdd = (i + 1) % 5 != 0;
            View tabView = LayoutInflater.from(mContext).inflate(R.layout.item_home_live_tab, null);
            mLinearLayoutList.get(index).addView(tabView);
            ImageView avatarIv = tabView.findViewById(R.id.item_avatar_iv);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) avatarIv.getLayoutParams();
            layoutParams.width = mData.get(i).getTitle().length() > 2 ? mThreeFontWidth : mTwoFontWidth;
            LinearLayout.LayoutParams tabViewLayoutParams = (LinearLayout.LayoutParams) tabView.getLayoutParams();
            tabViewLayoutParams.rightMargin = isAdd ? mPaddingRight : 0;
            avatarIv.setLayoutParams(layoutParams);
            TabInfo tmpInfo = mData.get(i);
            ImageLoadUtils.loadImage(mContext, tmpInfo.getAvaUrl(), avatarIv);

            avatarIv.setOnClickListener(v -> {
                if (mListener != null) {
                    mListener.onTabClick(tmpInfo);
                }
            });
        }

        if (mData.size() == 5) {
            LinearLayout childLinearLayout = new LinearLayout(mParentLinearLayout.getContext());
            childLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            childLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
            mLinearLayoutList.add(childLinearLayout);
            index++;
            mParentLinearLayout.addView(childLinearLayout);
        }

        //添加更多的标签
        TabInfo moreInfo = new TabInfo();
        moreInfo.setTitle("更多");
        moreInfo.setId(-1);
        moreInfo.setSourceId(R.mipmap.icon_home_live_tab_more);
        mData.add(moreInfo);

        View moreView = LayoutInflater.from(mContext).inflate(R.layout.item_home_live_tab, null);
        ImageView avatarIv = moreView.findViewById(R.id.item_avatar_iv);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) avatarIv.getLayoutParams();
        layoutParams.width = mTwoFontWidth;
        avatarIv.setLayoutParams(layoutParams);
        avatarIv.setImageResource(moreInfo.getSourceId());
        avatarIv.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onTabClick(moreInfo);
            }
        });

        mLinearLayoutList.get(index).addView(moreView);

    }


    public void setOnTabClickListener(OnTabClickListener listener) {
        mListener = listener;
    }


    public static class TabInfo {
        private String title;
        private String avaUrl;
        private int sourceId;
        private int id;

        public TabInfo setTitle(String value) {
            title = value;
            return this;
        }

        public TabInfo setId(int id) {
            this.id = id;
            return this;
        }

        public int getId() {
            return id;
        }


        public String getTitle() {
            return title;
        }

        public TabInfo setAvaUrl(String url) {
            avaUrl = url;
            return this;
        }

        public String getAvaUrl() {
            return avaUrl;
        }

        public TabInfo setSourceId(int id) {
            sourceId = id;
            return this;
        }

        public int getSourceId() {
            return sourceId;
        }
    }

    public interface OnTabClickListener {
        void onTabClick(TabInfo info);
    }


}
