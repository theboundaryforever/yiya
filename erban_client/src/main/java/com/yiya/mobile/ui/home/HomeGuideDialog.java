package com.yiya.mobile.ui.home;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.juxiao.library_ui.widget.DrawableTextView;
import com.yiya.mobile.base.fragment.BaseDialogFragment;
import com.tongdaxing.erban.R;

/**
 * 创建者      Created by dell
 * 创建时间    2019/2/28
 * 描述        ${}
 * <p>
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class HomeGuideDialog extends BaseDialogFragment implements View.OnClickListener {

    private boolean isAuth;

    public static HomeGuideDialog newInstance(boolean isAuth) {
        HomeGuideDialog withdrawWayDialog = new HomeGuideDialog();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isAuth", isAuth);
        withdrawWayDialog.setArguments(bundle);
        return withdrawWayDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            isAuth = bundle.getBoolean("isAuth", false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            window.setWindowAnimations(R.style.ErbanCommonWindowAnimationStyle);
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            window.setGravity(Gravity.CENTER);
            setCancelable(true);
        }
        return inflater.inflate(R.layout.dialog_home_guide, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView msg = view.findViewById(R.id.msg);
        TextView title = view.findViewById(R.id.title);
        LinearLayout buttons = view.findViewById(R.id.buttons);
        TextView videoRule = view.findViewById(R.id.video_rule);
        DrawableTextView cancel = view.findViewById(R.id.cancel);
        DrawableTextView confirm = view.findViewById(R.id.confirm);
        DrawableTextView confirm_ = view.findViewById(R.id.confirm_);
        if (isAuth) {
            title.setText("视频指引");
            msg.setText("视频直播开启");
//            msg.setTextColor(Color.parseColor("#FF8737FF"));
//            videoRule.setVisibility(View.VISIBLE);
//            confirm_.setVisibility(View.VISIBLE);
            buttons.setVisibility(View.GONE);
            confirm.setText("确认");
        }
        cancel.setOnClickListener(this);
        confirm.setOnClickListener(this);
        confirm_.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                if (mOKCancelDialogListener != null) {
                    mOKCancelDialogListener.onCancel();
                }
                dismissTryCrash();
                break;
            case R.id.confirm_:
            case R.id.confirm:
                if (mOKCancelDialogListener != null) {
                    mOKCancelDialogListener.onOk();
                }
                dismissTryCrash();
                break;
            default:
                break;
        }
    }

    private void dismissTryCrash() {
        try {
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mOKCancelDialogListener != null) {
            mOKCancelDialogListener = null;
        }
    }

    public void setOKCancelDialogListener(OKCancelDialogListener listener) {
        this.mOKCancelDialogListener = listener;
    }

    private OKCancelDialogListener mOKCancelDialogListener;

    public interface OKCancelDialogListener {
        void onOk();

        default void onCancel() {

        }
    }
}
