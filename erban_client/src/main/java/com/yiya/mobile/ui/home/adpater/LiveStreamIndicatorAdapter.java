package com.yiya.mobile.ui.home.adpater;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/5/7
 */
public class LiveStreamIndicatorAdapter extends CommonNavigatorAdapter {

    private List<TabInfo> mData;
    private Context mContext;

    private OnIndicatorSelectListener mListener;

    public void setOnIndicatorSelectListener(OnIndicatorSelectListener listener) {
        mListener = listener;
    }

    public interface OnIndicatorSelectListener {
        void onIndicatorSelect(TabInfo info, int position);
    }


    public LiveStreamIndicatorAdapter(Context context, List<TabInfo> titleList) {
        mContext = context;
        mData = titleList;
    }


    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public IPagerTitleView getTitleView(Context context, int index) {
        CommonPagerTitleView pagerTitleView = new CommonPagerTitleView(context);
        pagerTitleView.setContentView(R.layout.indicator_live_stream);

        TextView titleName = pagerTitleView.findViewById(R.id.tv_name_ind);
        titleName.setText(mData.get(index).getName());

//        CardView cardView = pagerTitleView.findViewById(R.id.indicator_card_view);
//        ImageView titleBgIv = pagerTitleView.findViewById(R.id.indicator_bg_iv);
        if (mData.get(index).getIsHot() == 0) {
            pagerTitleView.findViewById(R.id.indicator_hot_left_iv).setVisibility(View.INVISIBLE);
            pagerTitleView.findViewById(R.id.indicator_hot_right_iv).setVisibility(View.INVISIBLE);
        } else {
            pagerTitleView.findViewById(R.id.indicator_hot_left_iv).setVisibility(View.VISIBLE);
            pagerTitleView.findViewById(R.id.indicator_hot_right_iv).setVisibility(View.VISIBLE);
        }


        pagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {

            @Override
            public void onSelected(int index, int totalCount) {
                titleName.setTextColor(mContext.getResources().getColor(R.color.color_333333));
//                titleBgIv.setVisibility(View.VISIBLE);

            }


            @Override
            public void onDeselected(int index, int totalCount) {
//                titleBgIv.setVisibility(View.INVISIBLE);
                titleName.setTextColor(mContext.getResources().getColor(R.color.color_C3C3C3));
            }

            @Override
            public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {

            }

            @Override
            public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {

            }
        });

        pagerTitleView.setOnClickListener(view -> {
            if (mListener != null) {
                mListener.onIndicatorSelect(mData.get(index), index);
            }
        });
        return pagerTitleView;
    }

    @Override
    public IPagerIndicator getIndicator(Context context) {
        return null;
    }
}
