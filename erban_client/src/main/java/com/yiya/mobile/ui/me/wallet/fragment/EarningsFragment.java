package com.yiya.mobile.ui.me.wallet.fragment;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.growingio.android.sdk.collection.GrowingIO;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.ui.me.bills.activity.OrmosiaBillsActivity;
import com.yiya.mobile.ui.me.bills.activity.WithdrawBillsActivity;
import com.yiya.mobile.ui.me.wallet.activity.BinderPhoneActivity;
import com.yiya.mobile.ui.me.wallet.activity.ExchangeGoldActivity;
import com.yiya.mobile.ui.me.wallet.presenter.IncomePresenter;
import com.yiya.mobile.ui.me.wallet.view.IIncomeView;
import com.yiya.mobile.ui.me.withdraw.WithdrawActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Zhangsongzhou
 * @date 2019/5/5
 */
@CreatePresenter(IncomePresenter.class)
public class EarningsFragment extends BaseMvpFragment<IIncomeView, IncomePresenter> implements IIncomeView {


    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private EarningsRecyclerAdapter mAdapter;


    private boolean isRequest;
    private int mScreenWidth = 0;


    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_earnings;
    }

    @Override
    public void onFindViews() {

    }

    @Override
    public void onSetListener() {

    }

    @Override
    public void initiate() {
//        GrowingIO.getInstance().setPageName(this, "我的钱包-收益");
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "钱包提现页");
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new EarningsRecyclerAdapter(R.layout.item_earnings_card);
        mRecyclerView.setAdapter(mAdapter);

        mScreenWidth = DisplayUtils.getScreenWidth(mContext);
    }

    @Override
    public void handleClick(int id) {
    }

    @Override
    public void hasBindPhone() {
        if (!isRequest) {
            return;
        }
        isRequest = false;
        startActivity(new Intent(mContext, WithdrawActivity.class));

    }

    @Override
    public void hasBindPhoneFail(String error) {
        if (!isRequest) {
            return;
        }
        startActivity(new Intent(mContext, BinderPhoneActivity.class));
    }

    @Override
    public void setupUserWalletBalance(WalletInfo walletInfo) {
        List<EarningsCardInfo> tmpList = new ArrayList<>();

       /* EarningsCardInfo goldInfo = new EarningsCardInfo();
        goldInfo.setCountNum(walletInfo.redbeanNum);
        goldInfo.setMarkName("咿呀账单");
        goldInfo.setName("我的咿呀");

        tmpList.add(goldInfo);*/

        EarningsCardInfo diamondInfo = new EarningsCardInfo();
        diamondInfo.setCountNum(walletInfo.diamondNum);
        diamondInfo.setName("钻石余额");
        diamondInfo.setMarkName("钻石账单");

        tmpList.add(diamondInfo);

        mAdapter.setNewData(tmpList);


    }

    @Override
    public void getUserWalletInfoFail(String error) {

    }

    @Override
    public void onResume() {
        super.onResume();
        getMvpPresenter().refreshWalletInfo(true);
    }

    /**
     * 兑换金币
     *
     * @param view
     */
    @OnClick(R.id.exchange_gold_btn)
    void onClickExchangeGold(View view) {
        startActivity(new Intent(mContext, ExchangeGoldActivity.class));
    }

    /**
     * 提现
     *
     * @param view
     */
    @OnClick(R.id.withdrawal_btn)
    void onClickWithdrawal(View view) {
        isRequest = true;
        getMvpPresenter().hasBindPhone();

    }

    private class EarningsRecyclerAdapter extends BaseQuickAdapter<EarningsCardInfo, BaseViewHolder> {


        public EarningsRecyclerAdapter(int layoutResId) {
            super(layoutResId);
        }

        @Override
        protected void convert(BaseViewHolder helper, EarningsCardInfo item) {

            TextView markTv = ((TextView) helper.getView(R.id.item_bill_tv));
            markTv.setText(item.getMarkName());
            markTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (item.getName().contains("咿呀")) {
                        startActivity(new Intent(mContext, OrmosiaBillsActivity.class));

                    } else if (item.getName().contains("钻石")) {
                        startActivity(new Intent(mContext, WithdrawBillsActivity.class));
                    }

                }
            });
            ((TextView) helper.getView(R.id.item_name_tv)).setText(item.getName());
            ((TextView) helper.getView(R.id.item_num_tv)).setText(String.format(Locale.getDefault(), "%.2f", item.countNum));
            View bgView = helper.getView(R.id.item_bg_rl);


            ViewGroup.LayoutParams itemParams = bgView.getLayoutParams();
            int width = (int) (mScreenWidth / 1.07f);
            itemParams.width = width;
            bgView.setLayoutParams(itemParams);

            if (item.getName().contains("咿呀")) {
                bgView.setBackgroundResource(R.drawable.bg_item_earnings_red);
            } else {
                bgView.setBackgroundResource(R.mipmap.bg_diamonds);
            }
        }
    }

    public static class EarningsCardInfo {

        private String name;
        private String markName;
        private double countNum;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMarkName() {
            return markName;
        }

        public void setMarkName(String markName) {
            this.markName = markName;
        }

        public double getCountNum() {
            return countNum;
        }

        public void setCountNum(double countNum) {
            this.countNum = countNum;
        }
    }
}
