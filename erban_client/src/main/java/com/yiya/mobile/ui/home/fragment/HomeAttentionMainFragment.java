package com.yiya.mobile.ui.home.fragment;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.home.HomeAttentionMainPresenter;
import com.yiya.mobile.presenter.home.HomeAttentionXCView;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.ui.home.adpater.HomeAttentionMainAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.home.RoomAttentionInfo;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;

import java.util.List;

import butterknife.BindView;
import lombok.Data;

import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ATTENTION;
import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ATTENTION_EMPTY;
import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_REC_LIST;
import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ROOM;
import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ROOM_HOT_TITLE;

/**
 * @author Zhangsongzhou
 * @date 2019/3/20
 */
@CreatePresenter(HomeAttentionMainPresenter.class)
public class HomeAttentionMainFragment extends BaseMvpFragment<HomeAttentionXCView, HomeAttentionMainPresenter> implements HomeAttentionXCView, OnRefreshListener {

    public static final String TAG = "HomeAttentionMainFragment";

    @BindView(R.id.srl_base_refresh)
    SmartRefreshLayout mSmartRefreshLayout;
    @BindView(R.id.rv_base_list)
    RecyclerView mRecyclerView;

    private HomeAttentionMainAdapter mAdapter;


    private int mPageNum = 1;
    private int mPageSize = 20;

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_home_attention;
    }

    @Override
    public void onFindViews() {
    }

    @Override
    public void onSetListener() {
        mSmartRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void initiate() {
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "关注页");

        mAdapter = new HomeAttentionMainAdapter(mContext, null);
        mAdapter.setSpanSizeLookup(new BaseQuickAdapter.SpanSizeLookup() {
            @Override
            public int getSpanSize(GridLayoutManager gridLayoutManager, int position) {
                int value = 1;
                try {
                    int type = mAdapter.getData().get(position).getItemType();
                    if (type == ITEM_TYPE_ROOM_HOT_TITLE || type == ITEM_TYPE_REC_LIST || type == ITEM_TYPE_ATTENTION_EMPTY) {
                        value = 2;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return value;
            }
        });
        mRecyclerView.addItemDecoration(new MyItemDecoration());
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new HomeAttentionMainAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, HomeAttentionInfo info, int pos, int index) {
                if (info != null) {
                    switch (info.getItemType()) {
                        case ITEM_TYPE_ATTENTION:
                            RoomFrameActivity.start(mContext, info.getAttention().getUid(), info.getAttention().getType());
                            break;
                        case ITEM_TYPE_ROOM:
                            RoomFrameActivity.start(mContext, info.getRoomBean().getUid(), info.getRoomBean().getType());
                            break;
                        case ITEM_TYPE_REC_LIST:
                            RoomAttentionInfo.RoomAttentionRecBean bean = info.getRecBeanList().get(index);
                            if (view.getId() == R.id.item_avatar_civ) {//点击头像直接打开房间
                                RoomFrameActivity.start(mContext, bean.getUid(), bean.getType());
                            } else if (view.getId() == R.id.item_attention_btn) {//点击关注
                                getMvpPresenter().attentionRoom(bean.getRoomId());
                            }
                            break;
                    }
                }
            }
        });
        mSmartRefreshLayout.setEnableLoadMore(false);
        mSmartRefreshLayout.setEnableRefresh(true);
    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        getData();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        mPageNum = 1;
        getData();
    }

//    @Override
//    public void onLoadMore(RefreshLayout refreshlayout) {
//        mPageNum++;
//        getData();
//    }

    private void getData() {
        getMvpPresenter().getRoomAttention(mPageNum, mPageSize);
    }

    @Override
    public void onHomeAttentionListSuccess(List<HomeAttentionInfo> data) {
        if (mPageNum == 1) {
            mAdapter.setData(data);
        } else {
            mAdapter.addData(data);
        }

        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
    }

    @Override
    public void onHomeAttentionListFailed(String message) {

        mSmartRefreshLayout.finishRefresh();
        mSmartRefreshLayout.finishLoadMore();
    }

    private int mOffsetPadding = 0;
    private int mOffsetContext = 0;

    private class MyItemDecoration extends RecyclerView.ItemDecoration {

        private MyItemDecoration() {
            mOffsetPadding = DisplayUtils.dip2px(mContext, 15);
            mOffsetContext = DisplayUtils.dip2px(mContext, 10);
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            int pos = parent.getChildAdapterPosition(view);
            boolean isAdd = false;
            if (mAdapter.getData() != null && mAdapter.getData().get(pos) != null) {
                HomeAttentionInfo info = mAdapter.getItem(pos);
                if (info.getItemType() == ITEM_TYPE_ATTENTION || info.getItemType() == ITEM_TYPE_ROOM) {
                    isAdd = true;
                    if (info.getIndex() % 2 == 0) { //左边
                        outRect.left = mOffsetPadding;
                        outRect.right = mOffsetContext;
                    } else {
                        outRect.left = mOffsetContext / 3;
                        outRect.right = mOffsetPadding;
                    }
                    outRect.bottom = mOffsetContext / 5 * 4;
                }
                outRect.top = 0;
            }
            if (!isAdd) {
                outRect.right = 0;
                outRect.left = 0;
                outRect.bottom = 0;
            }


        }
    }


    @Data
    public static class HomeAttentionInfo implements MultiItemEntity {
        public static final int ITEM_TYPE_ATTENTION_EMPTY = 5;//关注列表为空
        public final static int ITEM_TYPE_ATTENTION = 1;// 我关注的房间列表
        public final static int ITEM_TYPE_REC_LIST = 2;// 为你推荐语音
        public final static int ITEM_TYPE_ROOM = 3;// 为你推荐视频
        public final static int ITEM_TYPE_ROOM_HOT_TITLE = 4; //为你推荐标题
        public final static int ITEM_TYPE_ROOM_HOT_TAG = 6;

        private int itemType;
        private int index;

        private RoomAttentionInfo.Attentions attention;
        private RoomAttentionInfo.RoomAttentionRecBean roomBean;
        private List<RoomAttentionInfo.RoomAttentionRecBean> recBeanList;
    }
}
