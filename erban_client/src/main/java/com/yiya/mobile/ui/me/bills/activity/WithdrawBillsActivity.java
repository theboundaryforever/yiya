package com.yiya.mobile.ui.me.bills.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.yiya.mobile.base.activity.BaseActivity;
import com.yiya.mobile.ui.home.adpater.CommonMagicIndicatorAdapter;
import com.yiya.mobile.ui.me.bills.adapter.WithdrawAdapter;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 提现账单记录
 *
 * @author dell
 */
public class WithdrawBillsActivity extends BaseActivity implements View.OnClickListener, CommonMagicIndicatorAdapter.OnItemSelectListener {

    private MagicIndicator mMagicIndicator;
    private ViewPager mViewPager;
    private ImageView iv_arrow;
   /* private AppToolBar mToolBar;*/
   private CommonMagicIndicatorAdapter mMsgIndicatorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_bills);
        initView();
        initData();
        setListener();
    }

    private void initView() {
        mMagicIndicator = (MagicIndicator) findViewById(R.id.magic_indicator2);
        /*mToolBar = (AppToolBar) findViewById(R.id.toolbar);*/
        mViewPager = (ViewPager) findViewById(R.id.vPager);
        iv_arrow = (ImageView)findViewById(R.id.iv_arrow);
    }

    private void initData() {
        mViewPager.setAdapter(new WithdrawAdapter(getSupportFragmentManager()));
        initMagicIndicator2();
    }

    private void setListener() {
        /*mToolBar.setOnBackBtnListener(view -> finish());*/
        iv_arrow.setOnClickListener(v -> finish());
    }

    private void initMagicIndicator2() {
        final String[] titles = getResources().getStringArray(R.array.bill_withdraw_titles);
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        List<TabInfo> tabInfoList = new ArrayList<>(2);
        for (int i = 0; i < titles.length; i++) {
            tabInfoList.add(new TabInfo(i, titles[i]));
        }
        mMsgIndicatorAdapter = new CommonMagicIndicatorAdapter(this, tabInfoList, 0);
        mMsgIndicatorAdapter.setSize(20);
        /*CommonMagicIndicatorAdapter adapter = new CommonMagicIndicatorAdapter(this, tabInfoList, 0);*/
        /*mMsgIndicatorAdapter.setSelectColorId(R.color.color_FA5D70);*/
        mMsgIndicatorAdapter.setOnItemSelectListener(this);
        commonNavigator.setAdapter(mMsgIndicatorAdapter);
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    @Override
    public void onItemSelect(int position) {
        mViewPager.setCurrentItem(position);
    }
}
