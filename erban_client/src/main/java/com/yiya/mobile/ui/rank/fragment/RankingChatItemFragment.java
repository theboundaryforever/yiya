package com.yiya.mobile.ui.rank.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.yiya.mobile.base.fragment.BaseMvpFragment;
import com.yiya.mobile.presenter.rankinglist.IRankingListView;
import com.yiya.mobile.presenter.rankinglist.RankingListPresenter;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;

import java.util.List;

import static com.yiya.mobile.ui.rank.activity.RankingChatListActivity.BUNDLE_KEY_DATA_TYPE;
import static com.yiya.mobile.ui.rank.activity.RankingChatListActivity.BUNDLE_KEY_ROOM_ID;
import static com.yiya.mobile.ui.rank.activity.RankingChatListActivity.BUNDLE_KEY_TYPE;
import static com.yiya.mobile.ui.rank.activity.RankingChatListActivity.BUNDLE_KEY_UID;
import static com.yiya.mobile.ui.rank.activity.RankingChatListActivity.DATA_TYPE_CHARM;
import static com.yiya.mobile.ui.rank.activity.RankingChatListActivity.DATA_TYPE_TREASURE;


/**
 * @author Zhangsongzhou
 * @date 2019/4/8
 */
@CreatePresenter(RankingListPresenter.class)
public class RankingChatItemFragment extends BaseMvpFragment<IRankingListView, RankingListPresenter> implements IRankingListView, SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RankingAdapter mAdapter;
    private LinearLayout mEmptyContentLl;

    private int mType;
    private int mDataType;
    private String mUid;
    private String mRoomId;

    @Override
    protected void onInitArguments(Bundle bundle) {
        super.onInitArguments(bundle);
        if (bundle != null) {
            mType = bundle.getInt(BUNDLE_KEY_TYPE);
            mDataType = bundle.getInt(BUNDLE_KEY_DATA_TYPE);
            mUid = bundle.getString(BUNDLE_KEY_UID);
            mRoomId = bundle.getString(BUNDLE_KEY_ROOM_ID);
        }
    }

    public static Fragment newInstance(int type, int dataType, String uid, String roomId) {
        Bundle bundle = new Bundle();
        RankingChatItemFragment fragment = new RankingChatItemFragment();
        bundle.putInt(BUNDLE_KEY_TYPE, type);
        bundle.putInt(BUNDLE_KEY_DATA_TYPE, dataType);
        bundle.putString(BUNDLE_KEY_UID, uid);
        bundle.putString(BUNDLE_KEY_ROOM_ID, roomId);
        fragment.setArguments(bundle);
        return fragment;
    }

    //
//    public static Fragment newInstance(int type, int dateType, String uid, int fromType) {
//        Bundle bundle = new Bundle();
//        RankingChatItemFragment fragment = new RankingChatItemFragment();
//        bundle.putInt("type", type);
//        bundle.putInt("dateType", dateType);
//        bundle.putString(BUNDLE_KEY_UID, uid);
//        bundle.putInt(BUNDLE_KEY_FROM_TYPE, fromType);
//        fragment.setArguments(bundle);
//        return fragment;
//    }

    @Override
    public int getRootLayoutId() {
        return R.layout.fragment_ranking_chat_item;
    }

    @Override
    public void onFindViews() {
        mSwipeRefreshLayout = mView.findViewById(R.id.refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        mAdapter = new RankingAdapter(R.layout.item_chat_ranking_list, mType);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                RankingXCInfo.ListBean info = (RankingXCInfo.ListBean) adapter.getItem(position);
                if (info != null) {
                    UIHelper.showUserInfoAct(getContext(), info.getCtrbUid());
                }
            }
        });

        mEmptyContentLl = mView.findViewById(R.id.ranking_content_empty_ll);


    }

    @Override
    public void onSetListener() {

    }

    @Override
    protected void onLazyLoadData() {
        super.onLazyLoadData();
        getData();
    }

    @Override
    public void initiate() {

    }

    private void getData() {
        getMvpPresenter().getRoomRankingList(mUid, mRoomId, mType, mDataType);
    }

    @Override
    public void setupFailView(String message) {
        toast(message);
        mSwipeRefreshLayout.setRefreshing(false);
        mAdapter.setNewData(null);
        mEmptyContentLl.setVisibility(View.VISIBLE);
    }

    @Override
    public void setupSuccessView(List<RankingXCInfo.ListBean> rankingList) {
        mSwipeRefreshLayout.setRefreshing(false);
        if (rankingList != null && rankingList.size() > 0) {
            mSwipeRefreshLayout.post(() -> mAdapter.setNewData(rankingList));
            mEmptyContentLl.setVisibility(View.GONE);
        } else {
            mEmptyContentLl.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onRefresh() {
        getData();
    }

    private class RankingAdapter extends BaseQuickAdapter<RankingXCInfo.ListBean, BaseViewHolder> {

        private int rankingType;

        public RankingAdapter(int layoutResId) {
            super(layoutResId);
        }

        public RankingAdapter(int layoutResId, int type) {
            this(layoutResId);
            rankingType = type;
        }

        @Override
        protected void convert(BaseViewHolder baseViewHolder, RankingXCInfo.ListBean info) {
            if (info == null) {
                return;
            }
            ((TextView) baseViewHolder.getView(R.id.ranking_number)).setText(String.valueOf((baseViewHolder.getAdapterPosition() - getHeaderLayoutCount() + 1)));
            ((TextView) baseViewHolder.getView(R.id.number)).setText(info.getSumGold() + "");
            ImageLoadUtils.loadAvatar(mContext, info.getAvatar(), baseViewHolder.getView(R.id.avatar));
            ((TextView) baseViewHolder.getView(R.id.nickname)).setText(info.getNick());

            ((ImageView) baseViewHolder.getView(R.id.gender)).
                    setImageDrawable(info.getGender() == 1 ?
                            mContext.getResources().getDrawable(R.mipmap.ic_ranking_male) :
                            mContext.getResources().getDrawable(R.mipmap.ic_ranking_female));

            //等级
            ImageView ivUserLevel = baseViewHolder.getView(R.id.iv_user_level);
            //            //用户等级
//            if (ivUserLevel != null) {
//                if (StringUtils.isNotEmpty(info.getVideoRoomExperLevelPic())) {
//                    ivUserLevel.setVisibility(View.VISIBLE);
//                    ImageLoadUtils.loadImage(mContext, info.getVideoRoomExperLevelPic(), ivUserLevel);
//                } else {
//                    ivUserLevel.setVisibility(View.GONE);
//                }
//            }

            if(mType == DATA_TYPE_TREASURE){// 财富榜
                ImageView ivExperLevel = baseViewHolder.getView(R.id.iv_exper_level);
                // 财富等级
                if (ivExperLevel != null) {
                    if (StringUtils.isNotEmpty(info.getExperLevelPic())) {
                        ivExperLevel.setVisibility(View.VISIBLE);
                        ImageLoadUtils.loadImage(mContext, info.getExperLevelPic(), ivExperLevel);
                    } else {
                        ivExperLevel.setVisibility(View.GONE);
                    }
                }
            } else if(mType == DATA_TYPE_CHARM){// 魅力榜
                ImageView ivCharmLevel = baseViewHolder.getView(R.id.iv_charm_level);
                // 魅力等级
                if (ivCharmLevel != null) {
                    if (StringUtils.isNotEmpty(info.getCharmLevelPic())) {
                        ivCharmLevel.setVisibility(View.VISIBLE);
                        ImageLoadUtils.loadImage(mContext, info.getCharmLevelPic(), ivCharmLevel);
                    } else {
                        ivCharmLevel.setVisibility(View.GONE);
                    }
                }
            }




        }
    }


}
