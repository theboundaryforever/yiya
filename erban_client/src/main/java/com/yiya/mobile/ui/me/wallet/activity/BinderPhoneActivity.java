package com.yiya.mobile.ui.me.wallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.growingio.android.sdk.collection.GrowingIO;
import com.juxiao.library_ui.widget.AppToolBar;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.ui.login.CodeDownTimer;
import com.yiya.mobile.ui.me.MePresenter;
import com.yiya.mobile.ui.me.task.view.IMeView;
import com.yiya.mobile.ui.widget.TextWatcherListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.text.NumberFormat;

/*
    copy绑定支付宝页面，控件ID没有改变 详情请看布局
 */
@CreatePresenter(MePresenter.class)
public class BinderPhoneActivity extends BaseMvpActivity<IMeView, MePresenter> implements IMeView {
    private EditText etAlipayAccount;
    private EditText etSmsCode;
    private TextView btnGetCode;
   /* private Button btnBinder;*/
    private Button btnBinderRquest;
    private TextWatcherListener textWatcher;

    public static final String hasBand = "hasBand";
    public static final String validatePhone = "validatePhone";
    public static final String isSetPassword = "isSetPassword";
    public static final int modifyBand = 1;
    private boolean isModifyBand = false;
    private TextInputLayout mTilPhone;
    private int mHasBandType;
    private boolean mIsValidatePhone;
    private boolean mIsSetPassword;
    private CodeDownTimer mTimer;
    private String mSubmitPhone;
    private AppToolBar mAppToolBar;

    public static void start(Context context) {
        Intent intent = new Intent(context, BinderPhoneActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GrowingIO.getInstance().setPageName(this, "绑定手机");
        mHasBandType = getIntent().getIntExtra(hasBand, 0);
        mIsSetPassword = getIntent().getBooleanExtra(isSetPassword, false);
        mIsValidatePhone = getIntent().getBooleanExtra(validatePhone, false);
        if (mHasBandType == modifyBand) {
            isModifyBand = true;
        }
        setContentView(R.layout.activity_binder_phone);
        initView();
        mAppToolBar.setTitle(isModifyBand ? "更换绑定手机号码" : mIsValidatePhone ? "验证手机号码" : "绑定手机号码");
        initData();
        setListener();
        if (mHasBandType == modifyBand) {
            UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
//            etAlipayAccount.setText(userInfo.getPhone());
            setMode(isModifyBand);
        } else {
            /*btnBinder.setText(isModifyBand ? "确认替换" : mIsValidatePhone ? "立即验证" : "确认绑定");*/
            btnBinderRquest.setText(isModifyBand ? "确认替换" : mIsValidatePhone ? "立即验证" : "确认绑定");
        }

    }

    private void initView() {
        mAppToolBar = (AppToolBar) findViewById(R.id.toolbar);
        /*mTilPhone = (TextInputLayout) findViewById(R.id.til_phone);*/
        etAlipayAccount = (EditText) findViewById(R.id.et_phone);
        GrowingIO.getInstance().trackEditText(etAlipayAccount);
        etSmsCode = (EditText) findViewById(R.id.et_smscode);
        GrowingIO.getInstance().trackEditText(etSmsCode);
        btnGetCode = (TextView) findViewById(R.id.btn_get_code);
       /* btnBinder = (Button) findViewById(R.id.btn_binder);*/
        btnBinderRquest = (Button) findViewById(R.id.btn_binder_request);
        editlinear();
    }

    private void setMode(boolean isModifyBand) {
        etAlipayAccount.setHint(isModifyBand ? "请输入已绑定的手机号码" : "请输入新绑定的手机号码");
        mAppToolBar.setTitle(isModifyBand ? "更换绑定手机号码" : "绑定手机号码");
       /* btnBinder.setText(isModifyBand ? "确认替换" : "确认绑定");*/
    }

    private void initData() {
        if (isModifyBand) {
            try {
                UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
                etAlipayAccount.setText(userInfo.getPhone());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**edit输入框输入监听*/
    private void editlinear(){
        //手机号输入框
        etAlipayAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etAlipayAccount.getText().toString().length() == 11){
                    btnGetCode.setEnabled(true);
                    if(etSmsCode.getText().toString().length()>3){
                        btnBinderRquest.setEnabled(true);
                    }
                }else{
                    btnGetCode.setEnabled(false);
                    btnBinderRquest.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //验证码输入框
        etSmsCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etSmsCode.getText().toString().length()>3){
                    if(etAlipayAccount.getText().toString().length() == 11){
                        btnBinderRquest.setEnabled(true);
                    }else{
                        btnBinderRquest.setEnabled(false);
                    }
                }else{
                    btnBinderRquest.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setListener() {
        //获取绑定手机验证码
        btnGetCode.setOnClickListener(v -> {
            String phoneNumber = etAlipayAccount.getText().toString().trim();
            Number parse = null;
            try {
                parse = NumberFormat.getIntegerInstance().parse(phoneNumber);
            } catch (Exception e) {
                Log.e("bind phone", e.getMessage(), e);
            }
            if (parse == null || parse.intValue() == 0 || phoneNumber.length() != 11) {
                toast("请输入正确的手机号码");
                btnGetCode.setEnabled(true);
                return;
            }else{
                mTimer = new CodeDownTimer(btnGetCode, 60 * 1000, 1000, new CodeDownTimer.OnTickListener() {
                    @Override
                    public void onTick(TextView textView, long time) {
                        textView.setEnabled(false);
                        textView.setText(time / 1000 + "s");
                    }

                    @Override
                    public void onFinish(TextView textView) {
                        textView.setEnabled(true);
                        textView.setText("重新获取");
                    }
                });
                mTimer.setColorId(getResources().getColor(R.color.color_A8A8A8));
                mTimer.start();
            }
            if (mHasBandType == modifyBand) {
                if (isModifyBand) {
                    //确认久手机type=3
                    getMvpPresenter().getModifyPhoneSMSCode(phoneNumber, "3");
                } else {
                    //确认久手机type=2
                    getMvpPresenter().getModifyPhoneSMSCode(phoneNumber, "2");
                }
            } else {
                if (mIsValidatePhone) {
                    //验证手机号
                    getMvpPresenter().getPwSmsCode(phoneNumber);
                } else {
                    //绑定手机
                    getMvpPresenter().getSMSCode(phoneNumber);
                }
            }
        });

        //请求绑定手机号码
        btnBinderRquest.setOnClickListener(v -> {
            getDialogManager().showProgressDialog(BinderPhoneActivity.this, "正在验证请稍后...");
            String phone = etAlipayAccount.getText().toString();
            String smsCode = etSmsCode.getText().toString();
            if (mHasBandType == modifyBand) {
                if (isModifyBand) {
                    getMvpPresenter().modifyBinderPhone(phone, smsCode, UriProvider.modifyBinderPhone());
                } else {
                    mSubmitPhone = phone;
                    getMvpPresenter().modifyBinderPhone(phone, smsCode, UriProvider.modifyBinderNewPhone());
                }
            } else {
                if (mIsValidatePhone) {
                    //验证手机号
                    getMvpPresenter().verifierPhone(phone, smsCode);
                } else {
                    //绑定手机
                    getMvpPresenter().binderPhone(phone, smsCode);
                }
            }
        });
       /* //输入框监听改变
        textWatcher = new TextWatcherListener() {

            @Override
            public void afterTextChanged(Editable editable) {
                if (etAlipayAccount.getText() != null && etAlipayAccount.getText().length() > 0
                        && etSmsCode.getText() != null && etSmsCode.getText().length() > 0) {
                    btnBinder.setVisibility(View.GONE);
                    btnBinderRquest.setVisibility(View.VISIBLE);
                } else {
                    btnBinder.setVisibility(View.VISIBLE);
                    btnBinderRquest.setVisibility(View.GONE);
                }
            }
        };

        etAlipayAccount.addTextChangedListener(textWatcher);
        etSmsCode.addTextChangedListener(textWatcher);*/

        mAppToolBar.setOnBackBtnListener(view -> finish());
    }

    @Override
    public void verifierPhone() {
        getDialogManager().dismissDialog();
        SetPasswordActivity.start(this, false);
        toast("手机验证成功");
        finish();
    }

    @Override
    public void verifierPhoneFail(String msg) {
        getDialogManager().dismissDialog();
        toast(msg);
    }

    @Override
    public void onBinderPhone() {
        getDialogManager().dismissDialog();
        toast("手机绑定成功");
        //是否是设置登录密码
        if (mIsSetPassword) {
            SetPasswordActivity.start(this, false);
        }
        finish();
    }

    @Override
    public void onBinderPhoneFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void onModifyOnBinner() {
        getDialogManager().dismissDialog();
        if (isModifyBand) {
            isModifyBand = false;
            mIsValidatePhone = false;
            setMode(false);
            etSmsCode.setText("");
            etAlipayAccount.setText("");
            etAlipayAccount.setFocusable(true);
            btnGetCode.setText("获取验证码");
            if (mTimer != null) {
                mTimer.cancel();
                mTimer.onFinish();
            }
        } else {
            CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().setPhone(mSubmitPhone);
            toast("绑定成功");
            finish();
        }
    }

    @Override
    public void onMoidfyOnBinnerFail(String error) {
        getDialogManager().dismissDialog();
        toast(error);
    }

    @Override
    public void getModifyPhoneSMSCodeFail(String msg) {
        toast(msg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
        if (etAlipayAccount != null) {
            etAlipayAccount.removeTextChangedListener(textWatcher);
        }
        if (etSmsCode != null) {
            etSmsCode.removeTextChangedListener(textWatcher);
        }
    }
}
