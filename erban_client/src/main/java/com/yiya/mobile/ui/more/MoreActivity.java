package com.yiya.mobile.ui.more;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.growingio.android.sdk.collection.GrowingIO;
import com.yiya.mobile.base.activity.BaseMvpActivity;
import com.yiya.mobile.base.adapter.BaseIndicatorStateAdapter;
import com.yiya.mobile.presenter.home.HomeMorePresenter;
import com.yiya.mobile.presenter.home.HomeVideoMoreView;
import com.yiya.mobile.ui.home.adpater.ChangeImageIndicatorAdapter;
import com.yiya.mobile.ui.home.adpater.YuChatIndicatorAdapter;
import com.yiya.mobile.ui.home.fragment.HomeVideoMoreFragment;
import com.yiya.mobile.ui.widget.magicindicator.MagicIndicator;
import com.yiya.mobile.ui.widget.magicindicator.ViewPagerHelper;
import com.yiya.mobile.ui.widget.magicindicator.buildins.commonnavigator.CommonNavigator;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.factory.CreatePresenter;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@CreatePresenter(HomeMorePresenter.class)
public class MoreActivity extends BaseMvpActivity<HomeVideoMoreView, HomeMorePresenter>
        implements HomeVideoMoreView, ChangeImageIndicatorAdapter.OnItemSelectListener,
        OnRefreshListener {

    public static void start(Context context, int tabId) {
        Intent intent = new Intent(context, MoreActivity.class);
        intent.putExtra("tabId", tabId);
        context.startActivity(intent);
    }

    @BindView(R.id.vp_home_content)
    ViewPager mViewPager;

    @BindView(R.id.arrow_back)
    ImageView arrowBack;

    //指示器
    @BindView(R.id.home_yu_chat_indicator)
    MagicIndicator mMagicIndicator;

    private BaseIndicatorStateAdapter mIndicatorStateAdapter;
    private YuChatIndicatorAdapter mYuChatIndicatorAdapter;
    private CommonNavigator mCommonNavigator;

    private List<Fragment> mFragmentList = new ArrayList<>();
    private List<TabInfo> mTabInfoList = new ArrayList<>();

    private int mPosition = 0;
    private int mTabId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GrowingIO.getInstance().setPageVariable(this, "yemianbiaoti", "直播分类页");

        setContentView(R.layout.activity_list_more);
        ButterKnife.bind(this);
        initView();
        setListener();
        initData();
    }

    private void setListener() {
        arrowBack.setOnClickListener(v -> finish());
    }

    private void initData() {
        mTabId = getIntent().getIntExtra("tabId", 0);
    }

    private void initView() {
        getData();
    }

    private void initIndicator(List<TabInfo> tabInfoList) {
        if (mFragmentList.size() > 0) {
            for (Fragment fragment : mFragmentList) {
                fragment = null;
            }
            System.gc();
        }

        mFragmentList.clear();
        mTabInfoList.clear();

        List<TabInfo> tmpTabInfoList = new ArrayList<>();

        for (TabInfo tabInfo : tabInfoList) {
            HomeVideoMoreFragment homeVideoMoreFragment = new HomeVideoMoreFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("tabId", tabInfo.getId());
            homeVideoMoreFragment.setArguments(bundle);
            tmpTabInfoList.add(tabInfo);
            mFragmentList.add(homeVideoMoreFragment);
            mTabInfoList.add(tabInfo);
        }

        if (mYuChatIndicatorAdapter != null) {
            mYuChatIndicatorAdapter = null;
        }
        mYuChatIndicatorAdapter = new YuChatIndicatorAdapter(getBaseContext(), tmpTabInfoList);
        mYuChatIndicatorAdapter.setOnIndicatorSelectListener(new YuChatIndicatorAdapter.OnIndicatorSelectListener() {
            @Override
            public void onIndicatorSelect(TabInfo info, int position) {
                mPosition = position;
                mViewPager.setCurrentItem(mPosition, true);
            }
        });

        if (mCommonNavigator != null) {
            mCommonNavigator = null;
        }
        mCommonNavigator = new CommonNavigator(getBaseContext());
        mCommonNavigator.setAdapter(mYuChatIndicatorAdapter);
        mCommonNavigator.setAdjustMode(false);
        mMagicIndicator.setNavigator(mCommonNavigator);
        LinearLayout titleContainer = mCommonNavigator.getTitleContainer();
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);

        if (mIndicatorStateAdapter != null) {
            mIndicatorStateAdapter = null;
        }

        mIndicatorStateAdapter = new BaseIndicatorStateAdapter(getSupportFragmentManager(), mFragmentList);
        mViewPager.setAdapter(mIndicatorStateAdapter);
        mViewPager.setOffscreenPageLimit(3);

        int index = 0;
        for (index = 0; index < tabInfoList.size(); index++) {
            TabInfo tabInfo = tabInfoList.get(index);
            if (tabInfo.getId() == mTabId) {
                break;
            }
        }
//        mCommonNavigator.onPageSelected(tabInfoList.size() > mPosition ? mPosition : 0);
        mCommonNavigator.onPageSelected(tabInfoList.size() > index ? index : 0);
        mViewPager.setCurrentItem(tabInfoList.size() > index ? index : 0);

        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    @Override
    public void onTagVideoRoomSucceed(List<HomeRoom> data) {

    }

    @Override
    public void onTagVideoRoomFailed(String message) {
        toast(message);
    }

    @Override
    public void onMenuListSucceed(List<TabInfo> data) {
        initIndicator(data);
    }

    @Override
    public void onMenuListFailed(String message) {
        toast(message);
    }

    private void getData() {
        getMvpPresenter().getMenuList();
    }

    @Override
    public void onItemSelect(int position) {

    }

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        getData();
    }
}
