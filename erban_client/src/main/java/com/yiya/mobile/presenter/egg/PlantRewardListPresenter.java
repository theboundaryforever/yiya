package com.yiya.mobile.presenter.egg;

import android.text.TextUtils;

import com.yiya.mobile.model.egg.PoundEggModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;
import com.tongdaxing.xchat_core.gift.EggGiftInfos;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantRewardListPresenter extends AbstractMvpPresenter<IPlantRewardListView> {
    private PoundEggModel poundEggModel;

    public PlantRewardListPresenter() {
        poundEggModel = new PoundEggModel();
    }

    /**
     * 获取砸蛋礼物列表
     */
    public void getPoundEggGifts() {
        poundEggModel.getPoundEggGifts(new HttpRequestCallBack<EggGiftInfos>() {
            @Override
            public void onSuccess(String message, EggGiftInfos eggGiftInfos) {
                if (getMvpView() != null) {
                    if (eggGiftInfos != null) {
                        ServiceResult<List<EggGiftInfo>> serviceResult = new ServiceResult<>();
                        serviceResult.setCodeSuccess();
                        serviceResult.setData(eggGiftInfos.getGiftList());
                        getMvpView().showPoundEggGifts(serviceResult);
                    } else {
                        getMvpView().showPoundEggGifts(null);
                    }
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().showPoundEggGifts(null);
                    if (TextUtils.isEmpty(msg)) {
                        getMvpView().toast(msg);
                    }
                }
            }
        });
    }
}
