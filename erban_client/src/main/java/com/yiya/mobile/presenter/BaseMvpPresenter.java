package com.yiya.mobile.presenter;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.Objects;

/**
 * 基础的presenter
 *
 * @author zeda
 */
public class BaseMvpPresenter<V extends IMvpBaseView> extends AbstractMvpPresenter<V> {

    public long getCurrentUserId() {
        return CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }

    /**
     * 此uid是否是我自己
     */
    public boolean isMyself(String currentUid) {
        return Objects.equals(currentUid, String.valueOf(getCurrentUserId()));
    }

    @Override
    public V getMvpView() {
        return super.getMvpView();
    }
}