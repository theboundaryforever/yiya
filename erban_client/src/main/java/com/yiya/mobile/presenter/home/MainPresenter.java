package com.yiya.mobile.presenter.home;

import android.text.TextUtils;
import android.view.View;

import com.yiya.mobile.model.log.LogModel;
import com.yiya.mobile.model.main.MainModel;
import com.yiya.mobile.model.message.MessageModel;
import com.yiya.mobile.room.model.RoomModel;
import com.netease.nim.uikit.NimUIKit;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.HomeMelon;
import com.tongdaxing.xchat_core.home.mission.CheckInMissionInfo;
import com.tongdaxing.xchat_core.home.mission.Missions;
import com.tongdaxing.xchat_core.home.view.IMainView;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;

public class MainPresenter extends AbstractMvpPresenter<IMainView> {
    private RoomModel roomModel;
    private MessageModel mMessageModel;
    private LogModel mLogModel;
    private MainModel mainModel;

    public MainPresenter() {
        if (mMessageModel == null) {
            mMessageModel = new MessageModel();
        }
        mainModel = new MainModel();
    }

    public void exitRoom() {
        if (roomModel == null) {
            roomModel = new RoomModel();
        }
        roomModel.exitRoom(null);
        if (getMvpView() != null) {
            getMvpView().onRoomExit();
        }
    }

    public void getCheckBlacklist(final View view, String tgUid) {
        if (mMessageModel == null) {
            mMessageModel = new MessageModel();
        }
        mMessageModel.checkUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklist(view, response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    public void uploadLogFile(int day) {
        if (mLogModel == null) {
            mLogModel = new LogModel();
        }
        mLogModel.uploadLog(null, day);
    }

    public void checkMessageImagePermission(int position) {
        mMessageModel.checkMessageImagePermission(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", CoreManager.getCore(IAuthCore.class).getTicket(), new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (NimUIKit.getCheckImageResultListener() != null) {
                    NimUIKit.getCheckImageResultListener().onCheckImageResult(1, position);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (code != 200) {
                    getMvpView().toast(msg);
                }

            }
        });
    }

    public void checkUserOperation(String tgUid, String content, String operation, OkHttpManager.MyCallBack<Json> callBack) {
        mMessageModel.checkUserOperation(tgUid, content, operation, callBack);
    }

    /**
     * 获取首页七天签到任务列表
     */
    public void getSignInMissions() {
        mainModel.getSignInMissions(new HttpRequestCallBack<Missions>() {
            @Override
            public void onSuccess(String message, Missions response) {
                if (getMvpView() != null && ListUtils.isNotEmpty(response.getCheckInMissions())) {
                    getMvpView().showSignInMissionDialog(response.getCheckInMissions());
                } else {
                    //获取种瓜
                    getMelonPopup();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                //获取种瓜
                getMelonPopup();
            }
        });
    }

    /**
     * 签到领取奖励
     */
    public void postSignInMission() {

        mainModel.postSignInMission(new HttpRequestCallBack<CheckInMissionInfo>() {
            @Override
            public void onFinish() {
                if (getMvpView() != null) {
                    getMvpView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(String message, CheckInMissionInfo response) {
                if (getMvpView() != null) {
                    getMvpView().showMissionRewardDialog(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (!TextUtils.isEmpty(msg)) {
                    getMvpView().toast(msg);
                }
                //获取种瓜
                getMelonPopup();
            }
        });

    }

    /**
     * 获取首页种瓜弹窗
     */
    public void getMelonPopup() {
        mainModel.getMelonPopup(new HttpRequestCallBack<HomeMelon>() {
            @Override
            public void onSuccess(String message, HomeMelon response) {
                if (response.isPopup()) {
                    getMvpView().showMelonDialog(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
            }
        });
    }


}
