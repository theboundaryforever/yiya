package com.yiya.mobile.presenter.user;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;

public interface IUserInviteView extends IMvpBaseView {
    void onUserInviteRedPacketSuc(RedPacketInfo data);

    void isBindPhoneSuc();
    void isBindPhomeFail();
}
