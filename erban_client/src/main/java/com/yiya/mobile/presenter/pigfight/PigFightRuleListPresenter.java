package com.yiya.mobile.presenter.pigfight;

import android.text.TextUtils;

import com.yiya.mobile.model.pigfight.PigFightModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

public class PigFightRuleListPresenter extends AbstractMvpPresenter<IPigFightRuleListView> {
    private PigFightModel pigFightModel;

    public PigFightRuleListPresenter() {
        this.pigFightModel = new PigFightModel();
    }

    /**
     * 获取猪猪大作战规则
     */
    public void getRuleList() {
        pigFightModel.getRuleList(new HttpRequestCallBack<List<String>>() {

            @Override
            public void onSuccess(String message, List<String> response) {
                if (getMvpView() != null) {
                    ServiceResult<List<String>> serviceResult = new ServiceResult<>();
                    serviceResult.setCodeSuccess();
                    serviceResult.setData(response);
                    getMvpView().showRuleList(serviceResult);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().showRuleList(null);
                    if (!TextUtils.isEmpty(msg)) {
                        getMvpView().toast(msg);
                    }
                }
            }
        });
    }
}
