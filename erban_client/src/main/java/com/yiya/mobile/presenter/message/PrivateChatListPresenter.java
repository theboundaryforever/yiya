package com.yiya.mobile.presenter.message;

import com.yiya.mobile.model.message.MessageModel;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/2.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PrivateChatListPresenter extends AbstractMvpPresenter<PrivateChatListView> {
    private MessageModel mMessageModel;

    public PrivateChatListPresenter() {
        mMessageModel = new MessageModel();
    }

    public void checkBlackList(String tgUid, RecentContact recent) {
        mMessageModel.checkUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onFinish() {
                if (getMvpView() != null) {
                    getMvpView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklist(response.boo("inTgUserBlacklist"), recent);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklistFailed(msg);
                }
            }
        });
    }
}
