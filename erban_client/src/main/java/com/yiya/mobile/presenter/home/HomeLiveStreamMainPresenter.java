package com.yiya.mobile.presenter.home;

import com.yiya.mobile.model.home.HomeModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/5/7
 */
public class HomeLiveStreamMainPresenter extends AbstractMvpPresenter<HomeLiveStreamMainView> {

    private HomeModel mHomeModel;


    public HomeLiveStreamMainPresenter() {
        if (mHomeModel == null) {
            mHomeModel = new HomeModel();
        }
    }

    public void getLiveStreamTagList() {
        mHomeModel.getRecommendTagList(6, new OkHttpManager.MyCallBack<ServiceResult<List<TabInfo>>>() {
            @Override
            public void onError(Exception e) {

                if (getMvpView() != null) {

                }
            }

            @Override
            public void onResponse(ServiceResult<List<TabInfo>> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        getMvpView().callbackRecommendTagList(response.getData());
                    } else {
                        getMvpView().callbackRecommendTagListFail("Error data.");
                    }
                }
            }
        });
    }


}
