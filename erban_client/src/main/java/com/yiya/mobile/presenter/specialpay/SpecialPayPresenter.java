package com.yiya.mobile.presenter.specialpay;

import android.content.Context;

import com.google.gson.JsonObject;
import com.yiya.mobile.model.specialpay.SpecialDiscountModel;
import com.yiya.mobile.room.model.RoomModel;
import com.yiya.mobile.ui.me.wallet.model.PayModel;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.bean.DiscountGift;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;

public class SpecialPayPresenter extends AbstractMvpPresenter<SpecialPayView> {
    private SpecialDiscountModel sdModel;

    protected PayModel payModel;
    protected WalletInfo walletInfo;
    protected RoomModel roomModel;

//    /**
//     * 刷新钱包信息
//     */
//    public void refreshWalletInfo(boolean force) {
//        payModel.refreshWalletInfo(force,new OkHttpManager.MyCallBack<ServiceResult<WalletInfo>>() {
//            @Override
//            public void onError(Exception e) {
//                if (getMvpView() != null && e != null)
//                    getMvpView().getUserWalletInfoFail(e.getMessage());
//            }
//
//            @Override
//            public void onResponse(ServiceResult<WalletInfo> response) {
//                if (null != response && response.isSuccess() && response.getData() != null) {
//                    walletInfo = response.getData();
//                    if (getMvpView() != null)
//                        getMvpView().setupUserWalletBalance(response.getData());
//                } else {
//                    if (getMvpView() != null && response != null)
//                        getMvpView().getUserWalletInfoFail(response.getErrorMessage());
//                }
//            }
//        });
//    }

    public SpecialPayPresenter() {
        if (sdModel == null) {
            sdModel = new SpecialDiscountModel();
        }

        if (payModel == null) {
            payModel = new PayModel();
        }
        if (roomModel == null) {
            roomModel = new RoomModel();
        }
    }

    /**
     * 视频房送礼物
     *
     * @param targetUid  单人时不能为0
     * @param targetUids 单人时为空、多人时不能为空
     */
    public void sendGift(long roomId, int giftId, long targetUid, List<Long> targetUids, int giftNum, long comboId, long comboCount, long comboFrequencyCount) {
        roomModel.sendGift(giftId, targetUid, targetUids, giftNum, comboId, comboCount, comboFrequencyCount, new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean needRefresh) {
                sdModel.reducePreferentialGift(roomId, giftId,  new HttpRequestCallBack<Object>(){
                    @Override
                    public void onSuccess(String message, Object response) {

                    }

                    @Override
                    public void onFailure(int code, String msg) {

                    }
                });
            }

            @Override
            public void onFail(int code, String error) {
                if (getMvpView() != null) {
                    switch (code) {
                        case 8018:
                            getMvpView().toast(error);
                            break;
                        case 2103:
                            getMvpView().toast(error);
                            break;
                        default:
                            getMvpView().toast(error);
                            break;
                    }
                }
            }
        });
    }

    /**
     * 发起充值
     *
     * @param context      context
     * @param chargeProdId 充值产品 ID
     * @param payChannel   充值渠道，目前只支持
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_ALIPAY} 和
     *                     {@link com.tongdaxing.xchat_core.Constants#CHARGE_WX}
     */
    public void requestCharge(Context context, String chargeProdId, long roomId, int giftId, String payChannel, int payType) {
        sdModel.requestCharge(context, chargeProdId, payChannel, payType, roomId, giftId, new OkHttpManager.MyCallBack<ServiceResult<JsonObject>>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().getChargeOrOrderInfoFail(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<JsonObject> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfo(response.getData().toString(), payType);
                } else {
                    if (getMvpView() != null)
                        getMvpView().getChargeOrOrderInfoFail(response == null ? "数据异常" : response.getMessage());
                }
            }
        });
    }

    //启动汇聚支付
    public void joinPay(Context context, String data) {
        try {
            Json json = new Json(data);
            String appId = json.getString("appId");
            String prepayId = json.getString("prepayId");
            String partnerId = json.getString("partnerId");
            String nonceStr = json.getString("nonceStr");
            String timeStamp = json.getString("timeStamp");
            String sign = json.getString("hmac");
            PayReq request = new PayReq();
            request.appId = appId;
            request.partnerId = partnerId;
            request.prepayId = prepayId;
            request.packageValue = "Sign=WXPay";
            request.nonceStr = nonceStr;
            request.timeStamp = timeStamp;
            request.sign = sign;

            IWXAPI api = WXAPIFactory.createWXAPI(context, appId, false);
            api.registerApp(appId);
            api.sendReq(request);
        } catch (Exception e) {
            e.printStackTrace();
            getMvpView().toast("拉起微信支付失败");
        }
    }

    /**
     * 获取要赠送的特惠礼物
     */
    public void getPreferentialGiftList(long roomId){
        sdModel.getPreferentialGiftList(roomId, new HttpRequestCallBack<List<DiscountGift>>() {

            @Override
            public void onSuccess(String message, List<DiscountGift> response) {
                if (ListUtils.isNotEmpty(response) && getMvpView() != null) {
                    getMvpView().getSpecialDiscountGiftSuccess(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if ( getMvpView() != null) {
                    getMvpView().getSpecialDiscountGiftFail(msg);
                }
            }
        });
    }
}
