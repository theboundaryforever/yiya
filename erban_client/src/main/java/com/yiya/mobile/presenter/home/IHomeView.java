package com.yiya.mobile.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.home.HomeRoomInfo;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;
import java.util.Map;

public interface IHomeView extends IMvpBaseView{

    void setTitle(List<TabInfo> data);
    //获取首页精选数据成功
    default void getHomeHotFeaturedSuccess(List<HomeRoom> result){}
    //获取精选数据失败
    default void getHomeHotFeaturedFail(String error){}

    default void getHomeTabListSuccess(List<TabInfo> tabs){}
    default void getHomeTabListFail(String errors){}

    default void getHomeBannerSuccess(List<BannerInfo> response){}
    default void getHomeBannerFail(String errors){}

    default void showHomeMengCoinBtn(boolean mcoinOption){}

    /*新项目回调*/
    default void onHomeListSuccessView(ServiceResult<List<HomeRoomInfo>> homeRoomList){}
    default void onHomeListFailView(Exception error){}

}

//public interface IHomeView extends IMvpBaseView{
//    //获取首页精选数据成功
//    void getHomeHotFeaturedSuccess(List<HomeRoom> result);
//    //获取精选数据失败
//    void getHomeHotFeaturedFail(String error);
//
//    void getHomeTabListSuccess(List<TabInfo> tabs);
//    void getHomeTabListFail(String errors);
//
//    void getHomeBannerSuccess(List<BannerInfo> response);
//    void getHomeBannerFail(String errors);
//
//    default void showHomeMengCoinBtn(boolean mcoinOption){}
//
//    void showHomeMengbiRemind(boolean hasRed);
//}
