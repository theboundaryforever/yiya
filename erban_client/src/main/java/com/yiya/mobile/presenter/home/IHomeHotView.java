package com.yiya.mobile.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.HomeInfo;

public interface IHomeHotView extends IMvpBaseView{


    //获取首页热门数据成功
    void getHomeHotRoomSuccess(HomeInfo result);
    //获取热门数据失败
    void getHomeHotRoomFail(Exception error);
}
