package com.yiya.mobile.presenter.home;

import android.util.Log;

import com.google.gson.Gson;
import com.tongdaxing.xchat_core.PreferencesUtils;
import com.tongdaxing.xchat_core.bean.HomeTalkInfo;
import com.yiya.mobile.model.home.HomeModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.LogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomePresenter extends AbstractMvpPresenter<IHomeView> {

    private HomeModel mHomeModel;

    public HomePresenter() {
        if (mHomeModel == null) {
            mHomeModel = new HomeModel();
        }
        getYuChatMenuList();
    }

    /**获取标题数据*/
    public void getYuChatMenuList() {
       mHomeModel.getHomeTalkRoomList("3", 10, 1, new OkHttpManager.MyCallBack<ServiceResult<HomeTalkInfo>>() {
           @Override
           public void onError(Exception e) {
               LogUtils.e(e.getMessage());
           }

           @Override
           public void onResponse(ServiceResult<HomeTalkInfo> response) {
               Log.d("首页-查询标签","返回值："+response.getCode()+",message:"+response.getMessage());
               if (getMvpView() != null) {
                   if (response != null) {
                       Log.d("首页-查询标签","返回值1");
                       List<TabInfo> Tlist = new ArrayList<>();
                       HomeTalkInfo info = response.getData();
                           Log.d("首页-查询标签","返回值2:"+info.getSearchTags().get(0).getId());
                           for (int i = 0; i < info.getSearchTags().size(); i++) {
                               Tlist.add(new TabInfo(info.getSearchTags().get(i).getId(),info.getSearchTags().get(i).getName()));
                           }
                           getMvpView().setTitle(Tlist);
                           String json = new Gson().toJson(info);
                           PreferencesUtils.saveHomeBanner(json);
                   } else {
                       LogUtils.e("Error data.");
                   }
               }
           }

       });
    }

}