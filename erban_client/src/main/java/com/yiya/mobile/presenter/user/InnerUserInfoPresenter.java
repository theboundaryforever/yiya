package com.yiya.mobile.presenter.user;

import com.yiya.mobile.model.rank.RankingListModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class InnerUserInfoPresenter extends AbstractMvpPresenter<IInnerUserInfoView> {

    private RankingListModel mRankingListModel;

    public InnerUserInfoPresenter() {
        mRankingListModel = new RankingListModel();
    }

    public void getGiftRankingList(int dateType) {
        mRankingListModel.getRankingList(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", dateType, new HttpRequestCallBack<RankingXCInfo>() {
            @Override
            public void onSuccess(String message, RankingXCInfo rankingXCInfo) {
                if (getMvpView() != null) {
                    getMvpView().showGiftRankingList(rankingXCInfo);
                }
            }

            @Override
            public void onFailure(int code, String msg) {

            }
        });

    }
}
