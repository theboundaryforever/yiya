package com.yiya.mobile.presenter.egg;

import android.text.TextUtils;

import com.yiya.mobile.model.egg.PoundEggModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantRankListPresenter extends AbstractMvpPresenter<IPlantRankListView> {

    private PoundEggModel poundEggModel;

    public PlantRankListPresenter() {
        poundEggModel = new PoundEggModel();
    }

    public void getRankList(long roomId, int type) {
        poundEggModel.getRankList(roomId, type, new HttpRequestCallBack<List<UserInfo>>() {
            @Override
            public void onSuccess(String message, List<UserInfo> response) {
                if (getMvpView() != null) {
                    getMvpView().showRankList(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (!TextUtils.isEmpty(msg)) {
                    if (getMvpView() != null) {
                        getMvpView().toast(msg);
                        getMvpView().showRankList(null);
                    }
                }
            }
        });
    }
}
