package com.yiya.mobile.presenter.withdraw;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/2/15
 */
public interface IWithdrawView extends IMvpBaseView {
    void onRemindToastError(String error);

    void onRemindToastSuc();

    void onCheckCodeFailToast(String error);

    void onCheckCodeSucWeixinLogin();

    void onRemindBindWeixinSucFail(String error);

    void onRemindBindWeixinSucToast();
}
