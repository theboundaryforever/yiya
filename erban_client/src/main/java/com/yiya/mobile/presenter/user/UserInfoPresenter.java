package com.yiya.mobile.presenter.user;

import android.content.Context;

import com.yiya.mobile.model.message.MessageModel;
import com.yiya.mobile.room.model.RoomModel;
import com.yiya.mobile.utils.HttpUtil;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/17.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class UserInfoPresenter extends AbstractMvpPresenter<IUserInfoView> {

    private MessageModel mMessageModel;

    private RoomModel mRoomModel;

    public UserInfoPresenter() {
        mMessageModel = new MessageModel();
        mRoomModel = new RoomModel();
    }

    /**
     * 私聊
     *
     * @param context
     * @param isFriends
     * @param uid
     */
    public void handlePrivateChat(Context context, boolean isFriends, long uid) {
        mMessageModel.checkUserBlacklist(uid + "", new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    if (response.boo("inTgUserBlacklist")) {
                        getMvpView().toast("已被拉黑");
                    } else {
                        HttpUtil.checkUserIsDisturb(context, isFriends, uid);
                    }
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }


    /**
     * 拉入私聊黑名单
     *
     * @param tgUid
     * @param value
     */
    public void addBlackListByUid(String tgUid, int value) {
        mMessageModel.addUserBlacklist(tgUid, value, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onAddBlacklist();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onAddBlacklistFailed(msg);
                }
            }
        });
    }

    public void checkBlackListByUid(String tgUid) {
        mMessageModel.checkUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklist(response.boo("checkBothSides"), response.boo("inBlacklist"), response.boo("inTgUserBlacklist"));
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklistFailed(msg);
                }
            }
        });
    }

    public void removeBlacklistByUid(String tgUid) {
        mMessageModel.removeUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onRemoveBlacklist();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onRemoveBlacklistFailed(msg);
                }
            }
        });
    }

    /**
     * 提交举报
     *
     * @param type       '类型 1. 用户 2. 房间',
     * @param targetUid  对方的uid（如果是房间，传房主UID）
     * @param reportType '举报类型 1.政治 2.色情 3.广告 4.人身攻击 ',
     */
    public void commitReport(int type, long targetUid, int reportType) {
        mRoomModel.commitReport(type, targetUid, reportType, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
                if (getMvpView() != null) {
                    getMvpView().toast("举报成功，我们会尽快为您处理");
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }
}
