package com.yiya.mobile.presenter.home;

import com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/20
 */
public interface HomeAttentionXCView extends IMvpBaseView {

    void onHomeAttentionListSuccess(List<HomeAttentionMainFragment.HomeAttentionInfo> info);

    void onHomeAttentionListFailed(String message);
}
