package com.yiya.mobile.presenter.home;

import com.yiya.mobile.model.attention.AttentionModel;
import com.yiya.mobile.room.model.RoomModel;
import com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.RoomAttentionInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.ArrayList;
import java.util.List;

import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ATTENTION;
import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ATTENTION_EMPTY;
import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_REC_LIST;
import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ROOM;
import static com.yiya.mobile.ui.home.fragment.HomeAttentionMainFragment.HomeAttentionInfo.ITEM_TYPE_ROOM_HOT_TITLE;

/**
 * @author Zhangsongzhou
 * @date 2019/3/20
 */
public class HomeAttentionMainPresenter extends AbstractMvpPresenter<HomeAttentionXCView> {

    private static final String TAG = "XCPresenter";
    private AttentionModel mAttentionModel;
    private RoomModel mRoomModel;

    public HomeAttentionMainPresenter() {
        if (mAttentionModel == null) {
            mAttentionModel = new AttentionModel();
        }
    }

    public void getRoomAttention(int pageNum, int pageSize) {
        mAttentionModel.getRoomAttentionByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", pageNum, pageSize, new HttpRequestCallBack<RoomAttentionInfo>() {
            @Override
            public void onSuccess(String message, RoomAttentionInfo response) {
                if ((getMvpView() != null) && (response != null)) {
                    List<HomeAttentionMainFragment.HomeAttentionInfo> tmpList = new ArrayList<>();
                    if (pageNum == 1) {
                        boolean isAddTitle = false;


                        //关注列表
                        if (!ListUtils.isListEmpty(response.getAttentions())) {
                            int index = 0;
                            for (RoomAttentionInfo.Attentions bean : response.getAttentions()) {
                                HomeAttentionMainFragment.HomeAttentionInfo homeAttentionInfo = new HomeAttentionMainFragment.HomeAttentionInfo();
                                homeAttentionInfo.setItemType(ITEM_TYPE_ATTENTION);
                                homeAttentionInfo.setAttention(bean);
                                homeAttentionInfo.setIndex(index++);
                                tmpList.add(homeAttentionInfo);
                            }
                        } else {
                            HomeAttentionMainFragment.HomeAttentionInfo emptyInfo = new HomeAttentionMainFragment.HomeAttentionInfo();
                            emptyInfo.setItemType(ITEM_TYPE_ATTENTION_EMPTY);
                            tmpList.add(emptyInfo);
                        }
                        //为你推荐列表
                        if (!ListUtils.isListEmpty(response.getRoomAttentionRecList())) {
                            HomeAttentionMainFragment.HomeAttentionInfo hotTitleInfo = new HomeAttentionMainFragment.HomeAttentionInfo();
                            hotTitleInfo.setItemType(ITEM_TYPE_ROOM_HOT_TITLE);
                            tmpList.add(hotTitleInfo);
                            isAddTitle = true;


                            HomeAttentionMainFragment.HomeAttentionInfo recListInfo = new HomeAttentionMainFragment.HomeAttentionInfo();
                            recListInfo.setItemType(ITEM_TYPE_REC_LIST);
                            recListInfo.setRecBeanList(response.getRoomAttentionRecList());
                            tmpList.add(recListInfo);
                        }

                        //底部房间列表
                        if (!ListUtils.isListEmpty(response.getVideoRoomList())) {
                            if (!isAddTitle) {
                                HomeAttentionMainFragment.HomeAttentionInfo hotTitleInfo = new HomeAttentionMainFragment.HomeAttentionInfo();
                                hotTitleInfo.setItemType(ITEM_TYPE_ROOM_HOT_TITLE);
                                tmpList.add(hotTitleInfo);
                                isAddTitle = true;
                            }

                            int index = 0;
                            for (RoomAttentionInfo.RoomAttentionRecBean bean : response.getVideoRoomList()) {
                                HomeAttentionMainFragment.HomeAttentionInfo roomInfo = new HomeAttentionMainFragment.HomeAttentionInfo();
                                roomInfo.setItemType(ITEM_TYPE_ROOM);
                                roomInfo.setRoomBean(bean);
                                roomInfo.setIndex(index++);
                                tmpList.add(roomInfo);
                            }
                        }

                    } else {

                    }
                    getMvpView().onHomeAttentionListSuccess(tmpList);
                } else {
                    if (response != null) {
                        getMvpView().onHomeAttentionListFailed(message);
                    } else {
                        getMvpView().onHomeAttentionListFailed("Error data.");
                    }

                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onHomeAttentionListFailed(msg);
                }
            }
        });
    }


    public void attentionRoom(long roomId) {
        if (mRoomModel == null) {
            mRoomModel = new RoomModel();
        }
        mRoomModel.roomAttention(CoreManager.getCore(IAuthCore.class).getCurrentUid(), roomId, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
                if (getMvpView() != null) {
                    getMvpView().toast("关注成功");
                }
            }

            @Override
            public void onFailure(int code, String msg) {

            }
        });
    }


}
