package com.yiya.mobile.presenter.message;

import com.yiya.mobile.model.message.MessageModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 * @author Zhangsongzhou
 * @date 2019/3/22
 */
public class MessageFriendListPresenter extends AbstractMvpPresenter<MessageFriendListView> {

    private MessageModel mMessageModel;

    public MessageFriendListPresenter() {
        if (mMessageModel == null) {
            mMessageModel = new MessageModel();
        }
    }

    public void checkBlackList(String tgUid) {
        mMessageModel.checkUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklist(response.boo("inTgUserBlacklist"));
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklistFailed(msg);
                }
            }
        });
    }


}
