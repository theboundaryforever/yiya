package com.yiya.mobile.presenter.egg;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface IPlantRewardListView extends IMvpBaseView {
    void showPoundEggGifts(ServiceResult<List<EggGiftInfo>> serviceResult);
}
