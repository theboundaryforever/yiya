package com.yiya.mobile.presenter.home;


import com.yiya.mobile.model.home.HomeModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

public class HomeMorePresenter extends AbstractMvpPresenter<HomeVideoMoreView> {
    private HomeModel mHomeModel;

    public HomeMorePresenter() {
        if (mHomeModel == null) {
            mHomeModel = new HomeModel();
        }
    }

    /**
     * 获取分类标签内的房间列表
     */
    public void getTagVideoRoom(final int tagId, final int pageNum) {
        mHomeModel.getTagVideoRoom(tagId, pageNum, new OkHttpManager.MyCallBack<ServiceResult<List<HomeRoom>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onTagVideoRoomFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<HomeRoom>> response) {
                if (getMvpView() != null) {
                    if ((response != null) && (response.getData() != null)) {
                        List<HomeRoom> data = response.getData();
                        for (HomeRoom homeRoom : data) {
                            homeRoom.setItemType(HomeRoom.ITEM_TYPE_VIDEO);
                        }

                        getMvpView().onTagVideoRoomSucceed(data);
                    } else {
                        getMvpView().onTagVideoRoomFailed("Error data.");
                    }
                }
            }
        });
    }

    public void getMenuList() {
        mHomeModel.getRecommendTagList(6, new OkHttpManager.MyCallBack<ServiceResult<List<TabInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onMenuListFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<TabInfo>> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        getMvpView().onMenuListSucceed(response.getData());
                    } else {
                        getMvpView().onMenuListFailed("Error data.");
                    }

                }

            }
        });
    }
}
