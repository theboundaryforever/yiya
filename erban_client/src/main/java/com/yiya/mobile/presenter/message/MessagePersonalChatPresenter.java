package com.yiya.mobile.presenter.message;

import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;

/**
 * @author Zhangsongzhou
 * @date 2019/3/22
 */
public class MessagePersonalChatPresenter extends AbstractMvpPresenter<MessagePersonalChatView> {
}
