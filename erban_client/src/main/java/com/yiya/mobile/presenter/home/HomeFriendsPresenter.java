package com.yiya.mobile.presenter.home;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.yiya.mobile.model.find.FindSquareModel;
import com.yiya.mobile.model.home.HomeModel;
import com.tongdaxing.erban.BuildConfig;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Constants;
import com.tongdaxing.xchat_core.bean.HomeFriendsInfo;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;
import com.tongdaxing.xchat_core.bean.SquareLoopInfo;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/3.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class HomeFriendsPresenter extends AbstractMvpPresenter<HomeFriendsView> {

    private HomeModel mHomeModel;
    private FindSquareModel mFindSquareModel;

    public HomeFriendsPresenter() {
        this.mHomeModel = new HomeModel();
        mFindSquareModel = new FindSquareModel();
    }

    public void getRoomListByTabId(int tagId, int pageNum, int pageSize) {
        mHomeModel.getHomeRoomListByTabId(tagId, pageNum, pageSize, new HttpRequestCallBack<List<HomeYuChatListInfo>>() {
            @SuppressLint("CheckResult")
            @Override
            public void onSuccess(String message, List<HomeYuChatListInfo> response) {
                Observable.fromIterable(response)
                        .subscribeOn(Schedulers.io())
                        .compose(bindToLifecycle())
                        .map(homeYuChatListInfo ->
                                new HomeFriendsInfo(HomeFriendsInfo.TYPE_ITEM).setHomeYuChatListInfo(homeYuChatListInfo))
                        .toList()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe((homeFriendsInfos, throwable) -> {
                            if (pageNum == Constants.PAGE_START) {
                                if (ListUtils.isListEmpty(homeFriendsInfos)) {
                                    homeFriendsInfos = HomeFriendsInfo.generateList();
                                } else {
                                    homeFriendsInfos.add(0, new HomeFriendsInfo(HomeFriendsInfo.TYPE_ENTER_ROOM));
                                }
                            }
                            if (getMvpView() != null) {
                                getMvpView().showRoomList(homeFriendsInfos);
                            }
                        });
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().showRoomList(HomeFriendsInfo.generateList());
                }
            }
        });
    }

    public void publicTitle() {
        mFindSquareModel.publicTitle(BuildConfig.DEBUG ? FindSquareModel.DEBUG_ROOM_ID : FindSquareModel.RELEASE_ROOM_ID,
                new HttpRequestCallBack<Json>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void onSuccess(String message, Json response) {

                        List<Json> his_list = response.jlist("his_list");
                        if (ListUtils.isListEmpty(his_list)) {
                            return;
                        }

                        Observable.fromIterable(his_list)
                                .subscribeOn(Schedulers.io())
                                .compose(bindToLifecycle())
                                .map(json -> new Gson().fromJson(json.toString(), SquareLoopInfo.class))
                                .toList()
                                .map(list ->
                                        new HomeFriendsInfo(HomeFriendsInfo.TYPE_SQUARE)
                                                .setSquareLoopInfoList(list)
                                )
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe((homeFriendsInfo, throwable) -> {
                                    if (getMvpView() != null) {
                                        getMvpView().showSquareList(homeFriendsInfo);
                                    }
                                });

                    }

                    @Override
                    public void onFailure(int code, String msg) {
                        if (!TextUtils.isEmpty(msg)) {
                            if (getMvpView() != null) {
                                getMvpView().toast(msg);
                            }
                        }
                    }
                });
    }
}
