package com.yiya.mobile.presenter.user;

import com.yiya.mobile.model.user.UserModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.redpacket.bean.RedPacketInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

public class UserInvitePresenter extends AbstractMvpPresenter<IUserInviteView> {
    private UserModel userModel;

    public UserInvitePresenter() {
        if (this.userModel == null) {
            this.userModel = new UserModel();
        }
    }


    /**
     * 获取邀请好友页面的信息
     */
    public void getInviteRedPacketInfo(){
        userModel.getInviteInfo(new OkHttpManager.MyCallBack<ServiceResult<RedPacketInfo>>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(ServiceResult<RedPacketInfo> data) {
                if (null != data && data.isSuccess()) {
                    if (getMvpView() != null){
                        getMvpView().onUserInviteRedPacketSuc(data.getData());
                    }
                }
            }
        });
    }


    /**
     * 判断是否绑定手机号
     * @param uid
     */
    public void isBindPhone(String uid){
        userModel.isBindPhone(new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null){
                    getMvpView().isBindPhomeFail();
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        if (getMvpView() != null){
                            getMvpView().isBindPhoneSuc();
                        }
                    } else {
                        if (getMvpView() != null){
                            getMvpView().isBindPhomeFail();
                        }
                    }
                }
            }
        });
    }


}
