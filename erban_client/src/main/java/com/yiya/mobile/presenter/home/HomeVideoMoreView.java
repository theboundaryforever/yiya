package com.yiya.mobile.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

public interface HomeVideoMoreView extends IMvpBaseView {
    void onTagVideoRoomSucceed(List<HomeRoom> data);
    void onTagVideoRoomFailed(String message);

    void onMenuListSucceed(List<TabInfo> data);
    void onMenuListFailed(String message);
}
