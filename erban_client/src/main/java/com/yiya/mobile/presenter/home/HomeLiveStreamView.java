package com.yiya.mobile.presenter.home;

import com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */
public interface HomeLiveStreamView extends IMvpBaseView {

    void onLiveVideoRoomSucceed(int pageNum, List<HomeLiveStreamFragment.LiveStreamInfo> data);

    void onLiveVideoRoomFailed(String message);

    String getTabId();
}
