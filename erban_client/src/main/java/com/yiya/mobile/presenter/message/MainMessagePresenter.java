package com.yiya.mobile.presenter.message;

import com.yiya.mobile.model.message.MessageModel;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.msg.BlackListInfo;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 * @author Zhangsongzhou
 * @date 2019/5/20
 */
public class MainMessagePresenter extends AbstractMvpPresenter<MainMessageView> {

    private MessageModel mMessageModel;

    public MainMessagePresenter() {
        if (mMessageModel == null) {
            mMessageModel = new MessageModel();
        }
    }


    public void checkBlackList(String tgUid, RecentContact recent) {
        mMessageModel.checkUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklist(response.boo("inTgUserBlacklist"), recent);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklistFailed(msg);
                }
            }
        });
    }

    public void getBlacklist(int pageNum, int pageSize) {
        mMessageModel.getUserBlacklist(pageNum, pageSize, new HttpRequestCallBack<BlackListInfo>() {
            @Override
            public void onSuccess(String message, BlackListInfo response) {
                if (getMvpView() != null) {
                    getMvpView().onGetBlacklist(response);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onGetBlacklistFailed(msg);
                }

            }
        });
    }

    public void removeBlacklist(String tgUid) {

        mMessageModel.removeUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onRemoveBlacklist();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onRemoveBlacklistFailed(msg);
                }
            }
        });
    }


}


