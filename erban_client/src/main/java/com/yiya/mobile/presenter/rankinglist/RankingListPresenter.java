package com.yiya.mobile.presenter.rankinglist;

import android.text.TextUtils;

import com.yiya.mobile.model.rank.RankingListModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.bean.UserLevelInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.List;
import java.util.Map;

public class RankingListPresenter extends AbstractMvpPresenter<IRankingListView> {
    private RankingListModel dataSource;

    public RankingListPresenter() {
        dataSource = new RankingListModel();
    }

    /**
     * 刷新数据
     *
     * @param type     排行榜类型 0巨星榜，1贵族榜，2房间榜
     * @param dateType 榜单周期类型 0日榜，1周榜，2总榜
     */
    public void refreshData(int type, int dateType, String roomId) {
        if (!TextUtils.isEmpty(roomId)) {
            getData(type, dateType, roomId);
        } else {
            getData(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", type, dateType);
        }
    }

    public void refreshDataById(int type, int dateType, String uid) {
        getData(uid, type, dateType);
    }


    public void getData(int type, int dateType, String roomUid) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("dataType", dateType + "");
        params.put("type", type + "");
        params.put("uid", roomUid);
//        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getRoomRankingList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<RankingXCInfo.ListBean>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<RankingXCInfo.ListBean>> response) {
                if (getMvpView() != null) {
                    getMvpView().setupSuccessView(response.getData());
                }
            }
        });
    }

    public void getRoomRankingList(String uid, String roomId, int type, int dataType) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("dataType", dataType + "");
        params.put("type", type + "");
        params.put("uid", uid);
        params.put("room_id", roomId);
        OkHttpManager.getInstance().getRequest(UriProvider.getRoomRankingList(), params, new OkHttpManager.MyCallBack<ServiceResult<List<RankingXCInfo.ListBean>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<RankingXCInfo.ListBean>> response) {
                if (getMvpView() != null) {
                    getMvpView().setupSuccessView(response.getData());
                }
            }
        });
    }


    private void getData(String queryUid, int type, int dateType) {
        dataSource.getXCRankingList(queryUid, type, dateType, new OkHttpManager.MyCallBack<ServiceResult<RankingXCInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RankingXCInfo> response) {
                if (getMvpView() != null) {
                    getMvpView().setupSuccessView(response.getData().getList());
                }
            }
        });
    }

    public void getRankingList(String uid, String queryUid, int type, int dateType) {
        dataSource.getXCRankingList(uid, queryUid, type, dateType, new OkHttpManager.MyCallBack<ServiceResult<RankingXCInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().setupFailView(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RankingXCInfo> response) {
                if (getMvpView() != null) {
                    if (response.getCode() == 200) {
                        getMvpView().setupSuccessView(response.getData().getList());
                    } else {
                        getMvpView().toast(response.getMessage());
                        getMvpView().setupSuccessView(null);
                    }

                }
            }
        });
    }

    /**
     * 获取用户等级与魅力
     */
    public void getUserLevel(String url) {

        dataSource.getUserLevel(url, new OkHttpManager.MyCallBack<ServiceResult<UserLevelInfo>>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().getUserLevelFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<UserLevelInfo> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().getUserLevelSuccess(response.getData());
                    }
                } else {
                    getMvpView().getUserLevelFail(response.getMessage());
                }
            }
        });
    }

}
