package com.yiya.mobile.presenter.message;

import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.msg.BlackListInfo;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/5/20
 */
public interface MainMessageView extends IMvpBaseView {

    default void onCheckBlacklist(boolean value, RecentContact recent) {

    }

    default void onCheckBlacklist(Json json) {
    }


    default void onCheckBlacklistFailed(String message) {
    }

    default void onGetBlacklist(BlackListInfo info) {

    }

    default void onGetBlacklistFailed(String message) {
    }

    default void onRemoveBlacklist() {
    }

    default void onRemoveBlacklistFailed(String message) {
    }


}
