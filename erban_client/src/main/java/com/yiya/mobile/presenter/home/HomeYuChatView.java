package com.yiya.mobile.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.home.RankInfo;
import com.tongdaxing.xchat_core.home.RankingInfo;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.home.YuChatFeatureInfo;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */
public interface HomeYuChatView extends IMvpBaseView {

    void onTitleBannerSucceed(List<BannerInfo> data);

    void onTitleBannerFailed(String message);


    void onFeatureSucceed(List<YuChatFeatureInfo> data);

    void onFeatureFailed(String message);


    void onMenuListSucceed(List<TabInfo> data);

    void onMenuListFailed(String message);

    void onRankList(RankInfo info);

    void onRankListFailed(String message);
}
