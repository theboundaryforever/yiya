package com.yiya.mobile.presenter.find;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;

/**
 * 文件描述：
 * 发送红包功能  执行view的回调接口
 * @auther：zwk
 * @data：2019/1/7
 */
public interface ISendRedPackageView extends IMvpBaseView {
    void onSendSucFinishPage();
    void onSendFailToastRemind(String error);
    void onSendRPNotEnoughMoneyRemind();

    void onUpdateGoldNumView(WalletInfo walletInfo);
    void onUpdateGoldNumFail(String error);
}
