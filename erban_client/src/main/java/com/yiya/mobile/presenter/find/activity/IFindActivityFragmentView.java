package com.yiya.mobile.presenter.find.activity;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.BannerInfo;

import java.util.List;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/20
 * 描述        发现界面
 *
 * 更新者      dell
 * 更新时间
 * 更新描述    发现页面的V
 */
public interface IFindActivityFragmentView extends IMvpBaseView {

    /**
     * 获取活动界面列表
     */
    void getFindActivityListSuccess(List<BannerInfo> bannerInfos);

    /**
     * 获取活动界面列表失败
     */
    void getFindActivityListFail(String e);

}
