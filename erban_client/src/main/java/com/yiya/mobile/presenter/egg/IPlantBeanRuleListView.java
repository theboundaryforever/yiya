package com.yiya.mobile.presenter.egg;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

public interface IPlantBeanRuleListView extends IMvpBaseView {
    void showRuleList(ServiceResult<List<String>> serviceResult);
}
