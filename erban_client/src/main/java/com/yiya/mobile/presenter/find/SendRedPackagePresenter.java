package com.yiya.mobile.presenter.find;

import com.yiya.mobile.model.redpackage.RedPackageModel;
import com.yiya.mobile.ui.me.wallet.model.PayModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 * 文件描述：
 * 红包相关数据和view之间的控制和管理
 * @auther：zwk
 * @data：2019/1/7
 */
public class SendRedPackagePresenter extends AbstractMvpPresenter<ISendRedPackageView> {
    private RedPackageModel redPackageModel;
    private PayModel payModel;

    public SendRedPackagePresenter() {
        if (redPackageModel == null) {
            this.redPackageModel = new RedPackageModel();
        }
        if (payModel == null){
            this.payModel = new PayModel();
        }
    }

    /**
     * 刷新钱包信息
     */
    public void refreshWalletInfo(boolean force) {
        payModel.refreshWalletInfo(force,new OkHttpManager.MyCallBack<ServiceResult<WalletInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null && e != null)
                    getMvpView().onUpdateGoldNumFail(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<WalletInfo> response) {
                if (null != response && response.isSuccess() && response.getData() != null) {
                    if (getMvpView() != null)
                        getMvpView().onUpdateGoldNumView(response.getData());
                } else {
                    if (getMvpView() != null && response != null)
                        getMvpView().onUpdateGoldNumFail(response.getErrorMessage());
                }
            }
        });
    }

    /**
     * 当前用户发送红包接口
     * @param roomid
     * @param amount
     * @param num
     */
    public void sendRedPakcageFromMyself(String roomid ,String amount,String num, int redpackType,int noticeAll){
        redPackageModel.sendRedPakcageFromMyself(roomid, amount, num, redpackType, noticeAll, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null){
                    getMvpView().onSendFailToastRemind(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null){
                    if (response.num("code") == 200){
                        if (getMvpView() != null){
                            getMvpView().onSendSucFinishPage();
                        }
                    }else if (response.num("code") == 2103){
                        if (getMvpView() != null){
                            getMvpView().onSendRPNotEnoughMoneyRemind();
                        }
                    }else {
                        if (getMvpView() != null){
                            getMvpView().onSendFailToastRemind(response.str("message"));
                        }
                    }
                }else {
                    if (getMvpView() != null){
                        getMvpView().onSendFailToastRemind("数据异常");
                    }
                }
            }
        });
    }

}
