package com.yiya.mobile.presenter.specialpay;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.pay.bean.WalletInfo;
import com.tongdaxing.xchat_core.room.bean.DiscountGift;

import java.util.List;

public interface SpecialPayView extends IMvpBaseView {

    /**
     * 查询特惠礼物成功
     */
    public void getSpecialDiscountGiftSuccess(List<DiscountGift> discountGifts);

    /**
     * 查询特惠礼物失败
     */
    public void getSpecialDiscountGiftFail(String error);

    public void getChargeOrOrderInfo(String data, int payType);

    public void getChargeOrOrderInfoFail(String error);
}
