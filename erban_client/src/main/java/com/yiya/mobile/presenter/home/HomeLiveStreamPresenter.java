package com.yiya.mobile.presenter.home;

import android.util.Log;

import com.google.gson.Gson;
import com.tongdaxing.xchat_core.PreferencesUtils;
import com.tongdaxing.xchat_core.bean.HomeTalkInfo;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.yiya.mobile.model.attention.AttentionModel;
import com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.HomeLiveInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.ArrayList;
import java.util.List;

import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_BANNER;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_RANK;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_CHAT;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_HOT;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_HOT_TAG;
import static com.yiya.mobile.ui.home.fragment.HomeLiveStreamFragment.LiveStreamInfo.ITEM_TYPE_ROOM_TAG;


/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */
public class HomeLiveStreamPresenter extends AbstractMvpPresenter<HomeLiveStreamView> {

    private AttentionModel mAttentionModel;
    private String tagid;

    public HomeLiveStreamPresenter() {
        if (mAttentionModel == null) {
            mAttentionModel = new AttentionModel();
        }
    }

    public void getHomeVideoRoomList(final int pageNum, int pageSize, final boolean isRefresh) {

       /* mAttentionModel.getVideoRoomList(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "", pageNum, pageSize, new OkHttpManager.MyCallBack<ServiceResult<HomeLiveInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onLiveVideoRoomFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<HomeLiveInfo> response) {
                if (getMvpView() != null) {
                    if (response != null && response.getData() != null) {
                        List<HomeLiveStreamFragment.LiveStreamInfo> tempList = new ArrayList<>();
                        HomeLiveInfo info = response.getData();
                        if (isRefresh) {
                            //广告
                            if (!ListUtils.isListEmpty(info.getMasterBanner())) {
                                HomeLiveStreamFragment.LiveStreamInfo masterBannerInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                masterBannerInfo.setItemType(ITEM_TYPE_BANNER);
                                masterBannerInfo.setMasterBanner(info.getMasterBanner());
                                tempList.add(masterBannerInfo);
                            }
                            //榜单
                            *//*if (!ListUtils.isListEmpty(info.getCharmRankVoList()) || !ListUtils.isListEmpty(info.getWealthRankVoList())) {
                                HomeLiveStreamFragment.LiveStreamInfo rankInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                rankInfo.setItemType(ITEM_TYPE_RANK);
                                HomeLiveStreamFragment.LiveStreamInfo.RankBean rankBean = new HomeLiveStreamFragment.LiveStreamInfo.RankBean();
                                rankBean.setCharmRankVoList(info.getCharmRankVoList());
                                rankBean.setWealthRankVoList(info.getWealthRankVoList());
                                rankInfo.setRankBean(rankBean);
                                tempList.add(rankInfo);
                            }*//*
                            HomeLiveStreamFragment.LiveStreamInfo rankInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                            rankInfo.setItemType(ITEM_TYPE_RANK);
                            tempList.add(rankInfo);
                            //为你推荐
                            if (!ListUtils.isListEmpty(info.getFontRoomList())) {
                                //标题
//                                HomeLiveStreamFragment.LiveStreamInfo hotRoomTitleInfo = new HomeLiveStreamFragment.LiveStreamInfo();
//                                hotRoomTitleInfo.setItemType(ITEM_TYPE_ROOM_HOT_TAG);
//                                tempList.add(hotRoomTitleInfo);

                                int index = 0;
                                for (HomeLiveInfo.RoomListBean bean : info.getFontRoomList()) {
                                    HomeLiveStreamFragment.LiveStreamInfo hotRoomInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                    hotRoomInfo.setItemType(ITEM_TYPE_ROOM_HOT);
                                    hotRoomInfo.setRoomListBean(bean);
                                    hotRoomInfo.setRoomIndex(index++);
                                    tempList.add(hotRoomInfo);
                                }
                            }
                            //标签
                            if (!ListUtils.isListEmpty(info.getVideoRoomTagList())) {
                                HomeLiveStreamFragment.LiveStreamInfo roomTagInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                roomTagInfo.setItemType(ITEM_TYPE_ROOM_TAG);
                                roomTagInfo.setVideoRoomTagList(info.getVideoRoomTagList());
                                tempList.add(roomTagInfo);
                            }
                            //语聊房 or 视频房
                            if (!ListUtils.isListEmpty(info.getRoomList())) {
                                int index = 0;
                                for (HomeLiveInfo.RoomListBean bean : info.getRoomList()) {
                                    HomeLiveStreamFragment.LiveStreamInfo roomInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                    roomInfo.setItemType(ITEM_TYPE_ROOM_CHAT);
                                    roomInfo.setRoomListBean(bean);
                                    roomInfo.setRoomIndex(index++);
                                    tempList.add(roomInfo);
                                }
                            }
                        } else {
                            //上拉加载更多，直接获取roomList的数据
                            //语聊房 or 视频房
                            if (!ListUtils.isListEmpty(info.getRoomList())) {
                                int index = 0;
                                for (HomeLiveInfo.RoomListBean bean : info.getRoomList()) {
                                    HomeLiveStreamFragment.LiveStreamInfo roomInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                    roomInfo.setItemType(ITEM_TYPE_ROOM_CHAT);
                                    roomInfo.setRoomListBean(bean);
                                    roomInfo.setRoomIndex(index++);
                                    tempList.add(roomInfo);
                                }
                            }
                        }
                        getMvpView().onLiveVideoRoomSucceed(pageNum, tempList);

                    } else {
                        if (response != null) {
                            getMvpView().toast(response.getCode() + ": " + response.getMessage());
                        } else {
                            getMvpView().onLiveVideoRoomFailed("Error data.");
                        }


                    }
                }
            }
        });*/
        tagid = getMvpView().getTabId();
        Log.d("首页-presenter-144", "标签id:"+tagid);
        if (tagid.equals("")) {
            Log.d("首页-presenter-141", "房间类型为空");
        } else {
            Log.d("首页-presenter-144", "标签id--123");
            mAttentionModel.getHomeTalkRoomList(tagid, pageNum, pageSize, new OkHttpManager.MyCallBack<ServiceResult<List<HomeYuChatListInfo>>>() {
                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        getMvpView().onLiveVideoRoomFailed(e.getMessage());
                    }
                }

                @Override
                public void onResponse(ServiceResult<List<HomeYuChatListInfo>> response) {
                        if (getMvpView() != null) {
                            List<HomeLiveStreamFragment.LiveStreamInfo> tempList = new ArrayList<>();
                            Log.d("首页-presenter-155", "数据解析1:");
                            if (isRefresh) {
                                Gson gson = new Gson();
                                HomeTalkInfo homeTalkInfo = gson.fromJson(PreferencesUtils.getHomeBanner(), HomeTalkInfo.class);
                                Log.d("首页-presenter-155", "数据解析1-1:" + homeTalkInfo.getSearchTags().get(0).getName());
                                //广告栏
                                if (!ListUtils.isListEmpty(homeTalkInfo.getBannerInfoList())) {
                                    HomeLiveStreamFragment.LiveStreamInfo masterBannerInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                    masterBannerInfo.setItemType(ITEM_TYPE_BANNER);
                                    masterBannerInfo.setBannerInfoList(homeTalkInfo.getBannerInfoList());
                                    Log.d("首页-presenter-155", "数据解析2:" + homeTalkInfo.getSearchTags().get(0).getName());
                                    tempList.add(masterBannerInfo);
                                }
                                if(tagid.equals("1")){
                                    //排行榜，大转盘，商城
                                    HomeLiveStreamFragment.LiveStreamInfo masterBannerInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                    masterBannerInfo.setItemType(ITEM_TYPE_RANK);
                                    tempList.add(masterBannerInfo);

                                    //是否有热门房间数据
                                    if (homeTalkInfo.getHomeHotRoomList() != null && homeTalkInfo.getHomeHotRoomList().size() > 0) {
                                        int index = 0;
                                        //热门房间标题
                                        HomeLiveStreamFragment.LiveStreamInfo masterBannerInfo2 = new HomeLiveStreamFragment.LiveStreamInfo();
                                        masterBannerInfo2.setItemType(ITEM_TYPE_ROOM_HOT_TAG);
                                        tempList.add(masterBannerInfo2);
                                        for (int i = 0; i < homeTalkInfo.getHomeHotRoomList().size(); i++) {
                                            HomeLiveStreamFragment.LiveStreamInfo hotRoomInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                            hotRoomInfo.setItemType(ITEM_TYPE_ROOM_HOT);
                                            hotRoomInfo.setHomeHotRoomList(homeTalkInfo.getHomeHotRoomList().get(i));
                                            hotRoomInfo.setRoomIndex(index++);
                                            tempList.add(hotRoomInfo);
                                        }
                                    }
                                }
                                if (response != null && response.getData() != null) {
                                    //语聊房 or 视频房
                                    List<HomeYuChatListInfo> Hlist = response.getData();
                                    if (Hlist.size()>0) {
                                        if (tagid.equals("1")) {
                                            //为你推荐标题
                                            HomeLiveStreamFragment.LiveStreamInfo masterBannerInfo3 = new HomeLiveStreamFragment.LiveStreamInfo();
                                            masterBannerInfo3.setItemType(ITEM_TYPE_ROOM_TAG);
                                            tempList.add(masterBannerInfo3);
                                        }
                                            int index = 0;
                                            //为你推荐房间
                                            for (HomeYuChatListInfo bean : Hlist) {
                                                HomeLiveStreamFragment.LiveStreamInfo roomInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                                roomInfo.setItemType(ITEM_TYPE_ROOM_CHAT);
                                                roomInfo.setHomeYuChatListInfo(bean);
                                                roomInfo.setRoomIndex(index++);
                                                tempList.add(roomInfo);
                                            }
                                     }
                                } else {
                                    if (response != null) {
                                        getMvpView().toast(response.getCode() + ": " + response.getMessage());
                                    } else {
                                        getMvpView().onLiveVideoRoomFailed("Error data.");
                                    }
                                }
                            } else {
                                //上拉加载更多，直接获取roomList的数据
                                //语聊房 or 视频房
                                List<HomeYuChatListInfo> Hlist = response.getData();
                                if (Hlist.size()>0) {
                                    int index = 0;
                                    for (HomeYuChatListInfo bean : Hlist) {
                                        HomeLiveStreamFragment.LiveStreamInfo roomInfo = new HomeLiveStreamFragment.LiveStreamInfo();
                                        roomInfo.setItemType(ITEM_TYPE_ROOM_CHAT);
                                        roomInfo.setHomeYuChatListInfo(bean);
                                        roomInfo.setRoomIndex(index++);
                                        tempList.add(roomInfo);
                                    }
                                }
                            }
                            Log.d("首页上拉加载更多","pageNum:"+pageNum+",list.size="+tempList.size());
                            getMvpView().onLiveVideoRoomSucceed(pageNum, tempList);
                        }
                }
            });
        }


    }


}
