package com.yiya.mobile.presenter.shopping;

import android.content.Context;

import com.yiya.mobile.model.message.MessageModel;
import com.yiya.mobile.model.shopping.DressUpModel;
import com.yiya.mobile.room.RoomFrameActivity;
import com.yiya.mobile.room.model.RoomModel;
import com.yiya.mobile.ui.me.shopping.view.IDressUpFragmentView;
import com.yiya.mobile.utils.HttpUtil;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.DressUpBean;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 装扮商城列表的Presenter
 */
public class DressUpFragmentPresenter extends AbstractMvpPresenter<IDressUpFragmentView> {

    private DressUpModel dressUpModel;

    private MessageModel mMessageModel;

    private RoomModel mRoomModel;


    public DressUpFragmentPresenter() {
        if (this.dressUpModel == null) {
            this.dressUpModel = new DressUpModel();
        }
        if (mMessageModel == null) {
            mMessageModel = new MessageModel();
        }
        if (mRoomModel == null) {
            mRoomModel = new RoomModel();
        }
    }

    /**
     * 装扮商城旧数据接口获取数据
     *
     * @param type 商品类型
     */
    public void getDressUpData(int type) {

        if (dressUpModel != null) {

            dressUpModel.getDressUpData(type, new OkHttpManager.MyCallBack<ServiceResult<List<DressUpBean>>>() {

                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        getMvpView().getDressUpListFail(e.getMessage());
                    }
                }

                @Override
                public void onResponse(ServiceResult<List<DressUpBean>> response) {
                    if (response != null && response.isSuccess()) {
                        if (getMvpView() != null) {
                            getMvpView().getDressUpList(response.getData());
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().getDressUpListFail(response.getMessage());
                        }
                    }
                }
            });
        }
    }

    /**
     * 获取装扮商城列表
     *
     * @param type
     * @param pageNum
     * @param pageSize
     */
    public void getDressUpData(boolean isMyself, int type, int pageNum, int pageSize) {
        if (dressUpModel != null) {
            dressUpModel.getDressUpData(isMyself, type, pageNum, pageSize, new OkHttpManager.MyCallBack<ServiceResult<List<DressUpBean>>>() {
                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        getMvpView().getDressUpListFail(e);
                    }
                }

                @Override
                public void onResponse(ServiceResult<List<DressUpBean>> response) {
                    if (getMvpView() != null) {
                        getMvpView().getDressUpListSuccess(response);
                    }
                }
            });
        }
    }

    /**
     * 获取装扮商城列表
     *
     * @param type 0 头饰，1座驾
     */
    public void getMyDressUpData(int type, long currentUid) {
        if (dressUpModel != null) {
            dressUpModel.getMyDressUpData(type, currentUid, new OkHttpManager.MyCallBack<ServiceResult<List<DressUpBean>>>() {
                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        if (type == 0) {
                            getMvpView().getHeadWearListSuccessFail(e.getMessage());
                        } else {
                            getMvpView().getCarListSuccessFail(e.getMessage());
                        }
                    }
                }

                @Override
                public void onResponse(ServiceResult<List<DressUpBean>> response) {
                    if (response != null && response.isSuccess()) {
                        if (getMvpView() != null) {
                            if (type == 0) {
                                getMvpView().getHeadWearListSuccess(response.getData());
                            } else {
                                getMvpView().getCarListSuccess(response.getData());
                            }
                        }
                    } else {
                        if (getMvpView() != null) {
                            if (type == 0) {
                                getMvpView().getHeadWearListSuccessFail(response.getMessage());
                            } else {
                                getMvpView().getCarListSuccessFail(response.getMessage());
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * 购买座驾
     *
     * @param type  type 0 头饰 1 座驾
     * @param carId
     */
    public void onPurseDressUp(int type, int purseType, int carId) {
        if (dressUpModel != null) {
            dressUpModel.onPurseDressUp(type, purseType, carId, new OkHttpManager.MyCallBack<Json>() {
                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        getMvpView().onPurseDressUpFail(e.getMessage());
                    }
                }

                @Override
                public void onResponse(Json response) {
                    if (response != null) {
                        if (response.num("code") == 200) {
                            if (getMvpView() != null) {
                                getMvpView().onPurseDressUpSuccess(purseType);
                            }
                        } else {
                            if (getMvpView() != null) {
                                getMvpView().onPurseDressUpFail(response.str("message"));
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * 修改装扮的使用状态
     *
     * @param type    type 0 头饰 1 座驾
     * @param dressId 装扮id
     */
    public void onChangeDressUpState(int type, int dressId) {
        if (dressUpModel != null) {
            dressUpModel.onChangeDressUpState(type, dressId, new OkHttpManager.MyCallBack<Json>() {
                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        getMvpView().onChangeDressUpStateFail(e.getMessage());
                    }
                }

                @Override
                public void onResponse(Json response) {
                    if (response != null) {
                        if (response.num("code") == 200) {
                            //更新用户信息
                            CoreManager.getCore(IUserCore.class).requestUserInfo(CoreManager.getCore(IAuthCore.class).getCurrentUid());
                            if (getMvpView() != null) {
                                getMvpView().onChangeDressUpStateSuccess(dressId);
                            }
                        } else {
                            if (getMvpView() != null) {
                                getMvpView().onChangeDressUpStateFail(response.str("message"));
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * 赠送礼物
     *
     * @param type      类型
     * @param targetUid 被赠送人
     * @param goodsId   礼物id
     */
    public void giveGift(int type, String targetUid, String goodsId) {

        if (dressUpModel != null) {

            dressUpModel.giveGift(type, targetUid, goodsId, new OkHttpManager.MyCallBack<ServiceResult>() {

                @Override
                public void onError(Exception e) {
                    if (getMvpView() != null) {
                        getMvpView().giftGiveFail(e.getMessage());
                    }
                }

                @Override
                public void onResponse(ServiceResult response) {
                    if (response != null && response.isSuccess()) {
                        if (getMvpView() != null) {
                            getMvpView().giftGiveSuccess();
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().giftGiveFail(response.getMessage());
                        }
                    }
                }
            });
        }
    }

    public void getGiftRankingList(int dateType, String queryUid) {
        //        Map<String, String> params = CommonParamUtil.getDefaultParam();
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("dateType", dateType + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("queryUid", queryUid);
//        String snString = SignUtils.getSign(UriProvider.getRankingXCList(), params, sn, System.currentTimeMillis() + "");
//        params.put("sn", snString);
        OkHttpManager.getInstance().getRequest(UriProvider.getRankingXCList(), params, new OkHttpManager.MyCallBack<ServiceResult<RankingXCInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onGiftRankingListFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RankingXCInfo> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        List<RankingXCInfo.ListBean> tmp = new ArrayList<>();
                        tmp.addAll(response.getData().getList());
                        if (tmp.size() > 0) {
                            tmp.get(0).setRankingAvatarId(R.mipmap.icon_user_ranking_first);
                        }
                        if (tmp.size() > 1) {
                            tmp.get(1).setRankingAvatarId(R.mipmap.icon_user_ranking_second);
                        }
                        if (tmp.size() > 2) {
                            tmp.get(2).setRankingAvatarId(R.mipmap.icon_user_ranking_third);
                        }

                        getMvpView().onGiftRankingListSucceed(response.getData().getList());
                    }
                }
            }
        });
    }


    /**
     * 拉入私聊黑名单
     *
     * @param tgUid
     * @param value
     */
    public void addBlackListByUid(String tgUid, int value) {
        mMessageModel.addUserBlacklist(tgUid, value, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onAddBlacklist();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onAddBlacklistFailed(msg);
                }
            }
        });
    }

    public void checkBlackListByUid(String tgUid) {
        mMessageModel.checkUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklist(response.boo("checkBothSides"), response.boo("inBlacklist"), response.boo("inTgUserBlacklist"));
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onCheckBlacklistFailed(msg);
                }
            }
        });
    }

    public void handleToFoundTa(Context context, String tgUid, long roomUid, int roomType) {
        mMessageModel.checkUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    if (response.boo("inTgUserBlacklist")) {
                        getMvpView().toast("已被拉黑");
                    } else {
                        RoomFrameActivity.start(context, roomUid, roomType);
                    }
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

    public void handlePrivateChat(Context context, boolean isFriends, long uid) {
        mMessageModel.checkUserBlacklist(uid + "", new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    if (response.boo("inTgUserBlacklist")) {
                        getMvpView().toast("已被拉黑");
                    } else {
                        HttpUtil.checkUserIsDisturb(context, isFriends, uid);
                    }
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }


    public void removeBlacklistByUid(String tgUid) {
        mMessageModel.removeUserBlacklist(tgUid, new HttpRequestCallBack<Json>() {
            @Override
            public void onSuccess(String message, Json response) {
                if (getMvpView() != null) {
                    getMvpView().onRemoveBlacklist();
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().onRemoveBlacklistFailed(msg);
                }
            }
        });
    }

    /**
     * 提交举报
     *
     * @param type       '类型 1. 用户 2. 房间',
     * @param targetUid  对方的uid（如果是房间，传房主UID）
     * @param reportType '举报类型 1.政治 2.色情 3.广告 4.人身攻击 ',
     */
    public void commitReport(int type, long targetUid, int reportType) {
        mRoomModel.commitReport(type, targetUid, reportType, new HttpRequestCallBack<Object>() {
            @Override
            public void onSuccess(String message, Object response) {
                if (getMvpView() != null) {
                    getMvpView().toast("举报成功，我们会尽快为您处理");
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().toast(msg);
                }
            }
        });
    }

}

