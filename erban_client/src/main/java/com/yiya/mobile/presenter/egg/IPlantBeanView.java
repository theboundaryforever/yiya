package com.yiya.mobile.presenter.egg;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;
import com.tongdaxing.xchat_core.room.bean.DrawCfg;

import java.util.List;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/3/7
 */
public interface IPlantBeanView extends IMvpBaseView {
    void showPoundEggReward(List<EggGiftInfo> eggGiftInfos, boolean playAnim);
    void showPoundEggErrorAndToast(String error);
    void showPoundEggFreeTimes(int times);
    void showPoundEggFreeBtn(boolean isShow);
    void refreshCfg(DrawCfg drawCfg);

    void refreshGold();
}
