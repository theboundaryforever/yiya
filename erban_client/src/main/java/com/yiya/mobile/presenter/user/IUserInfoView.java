package com.yiya.mobile.presenter.user;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/17.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface IUserInfoView extends IMvpBaseView {
    /**
     * 拉入私聊黑名单
     */
    default void onAddBlacklist() {

    }

    default void onAddBlacklistFailed(String message) {
    }

    /**
     * 检查是否在黑名单
     *
     * @param checkBothSides    互黑关系
     * @param inBlacklist       在自己的黑名单列表
     * @param inTgUserBlacklist 在他的黑名单列表
     */
    default void onCheckBlacklist(boolean checkBothSides, boolean inBlacklist, boolean inTgUserBlacklist) {
    }

    default void onCheckBlacklistFailed(String message) {

    }

    /**
     * 移除黑名单
     */
    default void onRemoveBlacklist() {

    }

    default void onRemoveBlacklistFailed(String message) {
    }

}
