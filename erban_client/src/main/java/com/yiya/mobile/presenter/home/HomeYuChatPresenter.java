package com.yiya.mobile.presenter.home;

import com.yiya.mobile.model.home.HomeModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.home.RankInfo;
import com.tongdaxing.xchat_core.home.TabInfo;
import com.tongdaxing.xchat_core.home.YuChatFeatureInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */
public class HomeYuChatPresenter extends AbstractMvpPresenter<HomeYuChatView> {

    private HomeModel mHomeModel;


    public HomeYuChatPresenter() {
        if (mHomeModel == null) {
            mHomeModel = new HomeModel();
        }
    }

    /**
     * 获取 title banner
     */
    public void getYuChatBanner() {
        mHomeModel.getHomeRoomBanner(new OkHttpManager.MyCallBack<ServiceResult<List<BannerInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onTitleBannerFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<BannerInfo>> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        getMvpView().onTitleBannerSucceed(response.getData());
                    } else {
                        getMvpView().onTitleBannerFailed("Error data.");
                    }
                }
            }
        });
    }

    public void getFeatureBannerList(int page) {
        mHomeModel.getHomeHotFeatrueList(page, new OkHttpManager.MyCallBack<ServiceResult<List<YuChatFeatureInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onFeatureFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<YuChatFeatureInfo>> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        getMvpView().onFeatureSucceed(response.getData());
                    } else {

                        getMvpView().onMenuListFailed("Error data.");
                    }

                }
            }
        });
    }

    public void getYuChatMenuList() {

        mHomeModel.getRecommendTagList(3, new OkHttpManager.MyCallBack<ServiceResult<List<TabInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onMenuListFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<TabInfo>> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        getMvpView().onMenuListSucceed(response.getData());
                    } else {
                        getMvpView().onMenuListFailed("Error data.");
                    }

                }

            }
        });
    }

    /**
     * 语聊房获取排行榜前三
     */
    public void getRankList() {
        mHomeModel.getYuChatRankList(new OkHttpManager.MyCallBack<ServiceResult<RankInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onRankListFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<RankInfo> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        getMvpView().onRankList(response.getData());
                    } else {
                        getMvpView().onRankListFailed("Error data.");
                    }

                }
            }
        });

    }


}
