package com.yiya.mobile.presenter.egg;

import android.text.TextUtils;

import com.yiya.mobile.model.egg.PoundEggModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/22.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PlantBeanRuleListPresenter extends AbstractMvpPresenter<IPlantBeanRuleListView> {
    private PoundEggModel poundEggModel;

    public PlantBeanRuleListPresenter() {
        poundEggModel = new PoundEggModel();
    }

    /**
     * 获取砸蛋礼物列表
     */
    public void getRuleList() {
        poundEggModel.getRuleList(new HttpRequestCallBack<List<String>>() {

            @Override
            public void onSuccess(String message, List<String> response) {
                if (getMvpView() != null) {
                    ServiceResult<List<String>> serviceResult = new ServiceResult<>();
                    serviceResult.setCodeSuccess();
                    serviceResult.setData(response);
                    getMvpView().showRuleList(serviceResult);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (getMvpView() != null) {
                    getMvpView().showRuleList(null);
                    if (!TextUtils.isEmpty(msg)) {
                        getMvpView().toast(msg);
                    }
                }
            }
        });
    }
}
