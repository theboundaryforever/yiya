package com.yiya.mobile.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

public interface IHomeAttentionView extends IMvpBaseView{
    void getRoomAttentionListSuccess(ServiceResult<List<HomeRoom>> datas);
    void getRoomAttentionListFail(String error);

    default void getRoomRecommendListSuccess(ServiceResult<List<HomeRoom>> response){};
    default void getRoomRecommendListFail(Exception error){};

    void roomAttentionSuccess();
    void roomAttentionFail(String error);

    void deleteAttentionSuccess();
    void deleteAttentionFail(String error);
}
