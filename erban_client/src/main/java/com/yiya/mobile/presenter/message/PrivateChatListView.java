package com.yiya.mobile.presenter.message;

import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/2.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface PrivateChatListView extends IMvpBaseView {

    void onCheckBlacklist(boolean value, RecentContact recent);

    void onCheckBlacklistFailed(String message);
}
