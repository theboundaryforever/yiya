package com.yiya.mobile.presenter.egg;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface IPlantRankListView extends IMvpBaseView {
    void showRankList(List<UserInfo> list);
}
