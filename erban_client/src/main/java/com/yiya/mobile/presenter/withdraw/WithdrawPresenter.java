package com.yiya.mobile.presenter.withdraw;

import com.yiya.mobile.model.user.UserModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

/**
 * 文件描述：提现功能的Presenter
 *
 * @auther：zwk
 * @data：2019/2/15
 */

public class WithdrawPresenter extends AbstractMvpPresenter<IWithdrawView> {
    private UserModel userModel;

    public WithdrawPresenter() {
        if (userModel == null) {
            userModel = new UserModel();
        }
    }

    /**
     * 获取微信提现绑定邀请码
     *
     * @param uid
     * @param ticket
     */
    public void getInviteCode(long uid, String ticket) {
//        userModel.getInviteCode(uid, ticket, new OkHttpManager.MyCallBack<ServiceResult>() {
//            @Override
//            public void onError(Exception e) {
//                if (getMvpView() != null) {
//                    getMvpView().onRemindToastError(e.getMessage());
//                }
//            }
//
//            @Override
//            public void onResponse(ServiceResult response) {
//                if (null != response) {
//                    if (response.isSuccess()) {
//                        if (getMvpView() != null) {
//                            getMvpView().onRemindToastSuc();
//                        }
//                    } else {
//                        if (getMvpView() != null) {
//                            getMvpView().onRemindToastError(response.getMessage());
//                        }
//                    }
//                } else {
//                    if (getMvpView() != null) {
//                        getMvpView().onRemindToastError("验证码获取异常");
//                    }
//                }
//            }
//        });
        userModel.getInviteCode(uid, ticket, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onRemindToastError(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        if (getMvpView() != null) {
                            getMvpView().onRemindToastSuc();
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().onRemindToastError(response.getMessage());
                        }
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onRemindToastError("验证码获取异常");
                    }
                }
            }
        });
    }

    /**
     * 验证微信提现绑定邀请码
     *
     * @param uid
     * @param ticket
     */
    public void checkCode(long uid, String ticket, String code) {
        userModel.getCheckCode(uid, ticket, code, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onRemindToastError(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        if (getMvpView() != null) {
                            getMvpView().onCheckCodeSucWeixinLogin();
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().onRemindToastError(response.getMessage());
                        }
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onRemindToastError("验证码验证异常");
                    }
                }
            }
        });
//        userModel.getCheckCode(uid, ticket, code, new OkHttpManager.MyCallBack<ServiceResult>() {
//            @Override
//            public void onError(Exception e) {
//                if (getMvpView() != null) {
//                    getMvpView().onRemindToastError(e.getMessage());
//                }
//            }
//
//            @Override
//            public void onResponse(ServiceResult response) {
//                if (null != response) {
//                    if (response.isSuccess()) {
//                        if (getMvpView() != null) {
//                            getMvpView().onCheckCodeSucWeixinLogin();
//                        }
//                    } else {
//                        if (getMvpView() != null) {
//                            getMvpView().onRemindToastError(response.getMessage());
//                        }
//                    }
//                } else {
//                    if (getMvpView() != null) {
//                        getMvpView().onRemindToastError("验证码验证异常");
//                    }
//                }
//            }
//        });
    }


    /**
     * 微信提现绑定
     *
     * @param uid
     * @param ticket
     */
    public void bindWithdrawWeixin(long uid, String ticket, String accessToken, String openId, String unionId) {
        userModel.bindWithdrawWeixin(uid, ticket, accessToken, openId, unionId, new OkHttpManager.MyCallBack<ServiceResult>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onRemindBindWeixinSucFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult response) {
                if (null != response) {
                    if (response.isSuccess()) {
                        if (getMvpView() != null) {
                            getMvpView().onRemindBindWeixinSucToast();
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().onRemindBindWeixinSucFail(response.getMessage());
                        }
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().onRemindBindWeixinSucFail("验证码获取异常");
                    }
                }
            }
        });
//        userModel.bindWithdrawWeixin(uid, ticket, accessToken, openId, unionId, new OkHttpManager.MyCallBack<ServiceResult>() {
//            @Override
//            public void onError(Exception e) {
//                if (getMvpView() != null) {
//                    getMvpView().onRemindBindWeixinSucFail(e.getMessage());
//                }
//            }
//
//            @Override
//            public void onResponse(ServiceResult response) {
//                if (null != response) {
//                    if (response.isSuccess()) {
//                        if (getMvpView() != null) {
//                            getMvpView().onRemindBindWeixinSucToast();
//                        }
//                    } else {
//                        if (getMvpView() != null) {
//                            getMvpView().onRemindBindWeixinSucFail(response.getMessage());
//                        }
//                    }
//                } else {
//                    if (getMvpView() != null) {
//                        getMvpView().onRemindBindWeixinSucFail("验证码获取异常");
//                    }
//                }
//            }
//        });
    }
}
