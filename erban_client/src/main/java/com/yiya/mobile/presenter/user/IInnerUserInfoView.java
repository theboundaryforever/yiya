package com.yiya.mobile.presenter.user;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/18.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface IInnerUserInfoView extends IMvpBaseView {

    void showGiftRankingList(RankingXCInfo rankingXCInfo);
}
