package com.yiya.mobile.presenter.find;

import com.google.gson.Gson;
import com.yiya.mobile.model.find.FindSquareModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.find.FindInfo;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.im.IMProCallBack;
import com.tongdaxing.xchat_framework.im.IMReportBean;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;

//import com.tongdaxing.xchat_core.find.family.SquareMemberInfo;

public class FindSquarePresenter extends AbstractMvpPresenter<IFindSquareView> {
    private FindSquareModel findSquareModel;
    private IMChatRoomMember imChatRoomMember;

    public FindSquarePresenter() {
        this.findSquareModel = new FindSquareModel();
    }

    /**
     * 获取公聊大厅房间id
     */
    public void getSquareRoomId() {
        findSquareModel.checkSquareRoomVersion(new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().resetSquareLayout();
                    getMvpView().enterPublicRoomFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                getMvpView().resetSquareLayout();
                if (response != null && response.num("code") == 200) {
                    Json data = response.json("data");
                    if (data != null) {
                        if (getMvpView() != null) {
                            getMvpView().getSquareRoomIdSuccess(data.boo("audit"));
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().enterPublicRoomFail(response.str("message", "进入广场失败，请稍后重试"));
                        }
                    }
                } else {
                    if (getMvpView() != null) {
                        if (response != null) {
                            getMvpView().enterPublicRoomFail(response.str("message", "进入广场失败，请稍后重试"));
                        } else {
                            getMvpView().enterPublicRoomFail("进入广场失败，请稍后重试");
                        }

                    }
                }
            }
        });
    }

    /**
     * 进入公聊房间
     */
    public void enterRoom(String roomId) {
        findSquareModel.enterPublicRoom(roomId, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {

                if (imReportBean != null && imReportBean.getReportData().errno == 0) {
                    imChatRoomMember = new Gson().fromJson(imReportBean.getReportData().data.str("member"), IMChatRoomMember.class);
                    if (getMvpView() != null) {
                        getMvpView().enterPublicRoomSuccess(imReportBean);
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().resetSquareLayout();
                        getMvpView().enterPublicRoomFail(
                                imReportBean != null ? imReportBean.getReportData().errmsg : "进入广场失败，请稍后重试");
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                // 登录失败
                if (getMvpView() != null) {
                    getMvpView().resetSquareLayout();
                    getMvpView().enterPublicRoomFail(errorMsg);
                }
            }
        });
    }

    /**
     * 上报
     */
    public void getReportState() {
        findSquareModel.checkReport(new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    if (getMvpView() != null) {
                        getMvpView().reportSuccess();
                    }
                }
            }
        });
    }

    /**
     * 发送广场的公聊消息
     */
    public void sendMessage(String roomId, String content) {
        if (imChatRoomMember == null) {
            return;
        }
        findSquareModel.senPublicMsg(roomId, content, imChatRoomMember, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                if (imReportBean != null && imReportBean.getReportData() != null) {
                    if (imReportBean.getReportData().errno == 0) {
                        if (getMvpView() != null) {
                            getMvpView().sendMessageSuccess();
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().sendMessageFail(imReportBean.getReportData().errno + " : " + imReportBean.getReportData().errmsg);
                        }
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().sendMessageFail("消息发送失败，请稍后重试！");
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (getMvpView() != null) {
                    getMvpView().sendMessageFail(errorCode + " : " + errorMsg);
                }
            }
        });
    }

    /**
     * 发现 -- 活动列表
     */
    public void findSquareActivity() {

        findSquareModel.findSquareActivity(new OkHttpManager.MyCallBack<ServiceResult<List<FindInfo>>>() {

            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().getFindActivityFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<FindInfo>> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().getFindActivity(response.getData());
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().getFindActivityFail(response == null ? "数据异常" : response.getMessage());
                    }
                }
            }
        });
    }

    /**
     * 获取交友列表数据
     */
    public void getMeetYouList(int currentPage) {
        findSquareModel.getMeetYouList(currentPage, new OkHttpManager.MyCallBack<ServiceResult<List<HomeRoom>>>() {

            @Override
            public void onError(Exception e) {
                getMvpView().getMeetYouListFail(e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<List<HomeRoom>> response) {
                if (response != null && response.isSuccess()) {
                    if (getMvpView() != null) {
                        getMvpView().getMeetYouList(response.getData());
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().getMeetYouListFail(response == null ? "数据异常" : response.getMessage());
                    }
                }
            }
        });
    }

}
