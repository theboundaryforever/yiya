package com.yiya.mobile.presenter.mengcoin;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.mengcoin.MengCoinTaskBean;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/1/15
 */
public interface IMengCoinView extends IMvpBaseView {

    default void showMengCoinTaskListView(MengCoinTaskBean mengCoinTaskBean) {
    }

    default void showMengCoinErrorView(String error) {
    }

    default void receiveMengCoinSucToast(int missionId) {
    }

    default void receiveMengCoinFailToast(String error) {
    }
}
