package com.yiya.mobile.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.bean.HomeFriendsInfo;

import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/3.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public interface HomeFriendsView extends IMvpBaseView {

    void showSquareList(HomeFriendsInfo homeFriendsInfo);

    void showRoomList(List<HomeFriendsInfo> homeFriendsInfos);
}
