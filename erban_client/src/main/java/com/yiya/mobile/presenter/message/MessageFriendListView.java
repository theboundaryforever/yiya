package com.yiya.mobile.presenter.message;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;

/**
 * @author Zhangsongzhou
 * @date 2019/3/22
 */
public interface MessageFriendListView extends IMvpBaseView {

    default void onCheckBlacklist(boolean checkBothSides) {

    }

    default void onCheckBlacklistFailed(String message) {
    }

}
