package com.yiya.mobile.presenter.home;

import com.yiya.mobile.model.attention.AttentionModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.home.HomeRoom;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;

public class HomeAttentionPresenter extends AbstractMvpPresenter<IHomeAttentionView> {
    private AttentionModel attentionModel;


    public HomeAttentionPresenter() {
        if (this.attentionModel == null)
            this.attentionModel = new AttentionModel();
    }

    /**
     * 点击关注按钮关注
     * @param roomId
     */
    public void roomAttention(String roomId){
        attentionModel.roomAttention(CoreManager.getCore(IAuthCore.class).getCurrentUid()+"", roomId, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null){
                    getMvpView().roomAttentionFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null){
                    if (response.num("code") == 200){
                        if (getMvpView() != null){
                            getMvpView().roomAttentionSuccess();
                        }
                    }else {
                        if (getMvpView() != null){
                            getMvpView().roomAttentionFail(response.str("message"));
                        }
                    }
                }else {
                    if (getMvpView() != null){
                        getMvpView().roomAttentionFail("数据异常！");
                    }
                }
            }
        });
    }

    /**
     * 检测是否关注过该房间
     * @param uid
     * @param roomId
     */
    public void checkAttention(String uid, String roomId){
        attentionModel.checkAttention(uid, roomId, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(Json response) {

            }
        });
    }

    /**
     * 取消房间的关注
     * @param roomId
     */
    public void deleteAttention( String roomId){
        attentionModel.deleteAttention(CoreManager.getCore(IAuthCore.class).getCurrentUid()+"", roomId, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null){
                    getMvpView().deleteAttentionFail(e.getMessage());
                }
            }

            @Override
            public void onResponse(Json response) {
                if (response != null){
                    if (response.num("code") == 200){
                        if (getMvpView() != null){
                            getMvpView().deleteAttentionSuccess();
                        }
                    }else {
                        if (getMvpView() != null){
                            getMvpView().deleteAttentionFail(response.str("message"));
                        }
                    }
                }else {
                    if (getMvpView() != null){
                        getMvpView().deleteAttentionFail("数据异常！");
                    }
                }
            }
        });
    }

    /**
     * 获取关注页面的关注房间列表
     * @param pageNum
     * @param pageSize
     */
    public void getRoomAttentionByUid(int pageNum,int pageSize){
        attentionModel.getRoomAttentionByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid()+"", pageNum, pageSize, new HttpRequestCallBack<List<HomeRoom>>() {
            @Override
            public void onSuccess(String message, List<HomeRoom> response) {

            }

            @Override
            public void onFailure(int code, String msg) {

            }
//            @Override
//            public void onError(Exception e) {
//                if (getMvpView() != null){
//                    getMvpView().getRoomAttentionListFail(e.getMessage());
//                }
//            }
//
//            @Override
//            public void onResponse(ServiceResult<List<HomeRoom>> response) {
//                if (response != null){
//                    if (response.isSuccess()){
//                        if (getMvpView() != null){
//                            getMvpView().getRoomAttentionListSuccess(response);
//                        }
//                    }else {
//                        if (getMvpView() != null){
//                            getMvpView().getRoomAttentionListFail(response.getMessage());
//                        }
//                    }
//                }else {
//                    if (getMvpView() != null){
//                        getMvpView().getRoomAttentionListFail("数据异常！");
//                    }
//                }
//            }
        });
//        attentionModel.getRoomAttentionByUid(CoreManager.getCore(IAuthCore.class).getCurrentUid()+"", pageNum, pageSize, new OkHttpManager.MyCallBack<ServiceResult<List<HomeRoom>>>() {
//            @Override
//            public void onError(Exception e) {
//                if (getMvpView() != null){
//                    getMvpView().getRoomAttentionListFail(e.getMessage());
//                }
//            }
//
//            @Override
//            public void onResponse(ServiceResult<List<HomeRoom>> response) {
//                if (response != null){
//                    if (response.isSuccess()){
//                        if (getMvpView() != null){
//                            getMvpView().getRoomAttentionListSuccess(response);
//                        }
//                    }else {
//                        if (getMvpView() != null){
//                            getMvpView().getRoomAttentionListFail(response.getMessage());
//                        }
//                    }
//                }else {
//                    if (getMvpView() != null){
//                        getMvpView().getRoomAttentionListFail("数据异常！");
//                    }
//                }
//            }
//        });
    }


    /**
     * 获取关注页面的关注房间列表
     * @param pageNum
     * @param pageSize
     */
    public void getRoomRecommendList(int pageNum,int pageSize){
        attentionModel.getRoomAttentionRecommendList(CoreManager.getCore(IAuthCore.class).getCurrentUid()+"", pageNum, pageSize, new OkHttpManager.MyCallBack<ServiceResult<List<HomeRoom>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null){
                    getMvpView().getRoomRecommendListFail(e);
                }
            }

            @Override
            public void onResponse(ServiceResult<List<HomeRoom>> response) {
                if (response != null){
                    if (response.isSuccess()){
                        if (getMvpView() != null){
                            getMvpView().getRoomRecommendListSuccess(response);
                        }
                    }else {
                        if (getMvpView() != null){
                            getMvpView().getRoomRecommendListFail(new Exception(response.getMessage()));
                        }
                    }
                }else {
                    if (getMvpView() != null){
                        getMvpView().getRoomRecommendListFail(new Exception("数据异常！"));
                    }
                }
            }
        });
    }
}
