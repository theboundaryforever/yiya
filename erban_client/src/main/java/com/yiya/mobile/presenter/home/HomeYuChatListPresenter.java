package com.yiya.mobile.presenter.home;

import com.yiya.mobile.model.home.HomeModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.bean.HomeYuChatHotInfo;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */
public class HomeYuChatListPresenter extends AbstractMvpPresenter<HomeYuChatListView> {
    private HomeModel mHomeModel;

    public HomeYuChatListPresenter() {
        if (mHomeModel == null) {
            mHomeModel = new HomeModel();
        }

    }

    public void getRoomListByTabId(int tagId, int pageNum, int pageSize) {
        mHomeModel.getHomeRoomListByTabId(tagId, pageNum, pageSize, new OkHttpManager.MyCallBack<ServiceResult<List<HomeYuChatListInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onRoomListFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<HomeYuChatListInfo>> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        getMvpView().onRoomListSucceed(response.getData());
                    } else {
                        getMvpView().onRoomListFailed("Error data.");
                    }

                }

            }
        });
    }

    public void getRoomHotList(int pageNum) {
        mHomeModel.getHomeHotRoomList(pageNum, new OkHttpManager.MyCallBack<ServiceResult<HomeYuChatHotInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().onRoomListFailed(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<HomeYuChatHotInfo> response) {
                if (getMvpView() != null) {
                    if (response != null) {
                        getMvpView().onRoomListSucceed(response.getData().getListRoom());
                    } else {
                        getMvpView().onRoomListFailed("Error data.");
                    }

                }
            }
        });

    }


}
