package com.yiya.mobile.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.bean.HomeYuChatListInfo;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 */
public interface HomeYuChatListView extends IMvpBaseView {

    void onRoomListSucceed(List<HomeYuChatListInfo> data);

    void onRoomListFailed(String message);
}
