package com.yiya.mobile.presenter.home;

import com.yiya.mobile.model.home.HomeModel;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.home.HomeInfo;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

public class HomeHotPresenter extends AbstractMvpPresenter<IHomeHotView> {
    private HomeModel homeModel;


    public HomeHotPresenter() {
        if (this.homeModel == null)
            this.homeModel = new HomeModel();
    }


    public void getHomeHotRoomList(int mPage){
        homeModel.getHomeHotRoomList(mPage, new OkHttpManager.MyCallBack<ServiceResult<HomeInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null)
                    getMvpView().getHomeHotRoomFail(e);
            }

            @Override
            public void onResponse(ServiceResult<HomeInfo> response) {
                if (response != null ){
                    if(response.isSuccess()) {
                        if (getMvpView() != null)
                            getMvpView().getHomeHotRoomSuccess(response.getData());
                    }else {
                        if (getMvpView() != null)
                            getMvpView().getHomeHotRoomFail(new Exception(response.getMessage()));
                    }
                }else {
                    if (getMvpView() != null)
                        getMvpView().getHomeHotRoomFail(new Exception("返回数据异常"));
                }
            }
        });
    }
}
