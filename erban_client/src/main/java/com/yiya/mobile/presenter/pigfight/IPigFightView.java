package com.yiya.mobile.presenter.pigfight;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.room.bean.PigRecordInfo;

import java.util.List;

public interface IPigFightView extends IMvpBaseView {
    void showPlaySuccess(List<?> infos);
    void showPlayFail(String error);

    void showPigFightRecord(List<PigRecordInfo> list);
}
