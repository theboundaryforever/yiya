package com.yiya.mobile.presenter.home;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.TabInfo;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/5/7
 */
public interface HomeLiveStreamMainView extends IMvpBaseView {

    /**
     * 获取推荐列表tag
     */
    default void callbackRecommendTagList(List<TabInfo> data) {

    }

    default void callbackRecommendTagListFail(String message) {

    }
}
