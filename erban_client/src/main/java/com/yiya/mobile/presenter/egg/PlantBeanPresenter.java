package com.yiya.mobile.presenter.egg;

import com.yiya.mobile.model.egg.PoundEggModel;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.gift.EggFreeGiftInfo;
import com.tongdaxing.xchat_core.gift.EggGiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.room.bean.DrawCfg;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;

/**
 * 文件描述：砸蛋模块的数据和view相互关联的中间类 事件决策者
 *
 * @auther：zwk
 * @data：2019/3/7
 */

public class PlantBeanPresenter extends AbstractMvpPresenter<IPlantBeanView> {
    private PoundEggModel poundEggModel;

    public PlantBeanPresenter() {
        poundEggModel = new PoundEggModel();
    }

    /**
     * 砸蛋
     */
    public void getPoundEggFreeReward(int type, boolean playAnim) {
        poundEggModel.poundEgg(type, new OkHttpManager.MyCallBack<ServiceResult<List<EggGiftInfo>>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().showPoundEggErrorAndToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<List<EggGiftInfo>> response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        if (getMvpView() != null) {
                            getMvpView().showPoundEggReward(response.getData(), playAnim);
                            getMvpView().refreshGold();
                            setLotteryGiftInfo(response.getData());
                            refreshGiftList();
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().showPoundEggErrorAndToast(response.getMessage());
                        }
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().showPoundEggErrorAndToast("数据异常");
                    }
                }
            }
        });
    }


    /**
     * 免费砸蛋
     *
     * @param playAnim
     */
    public void poundEggFree(boolean playAnim) {
        poundEggModel.poundEggFree(new OkHttpManager.MyCallBack<ServiceResult<EggFreeGiftInfo>>() {
            @Override
            public void onError(Exception e) {
                if (getMvpView() != null) {
                    getMvpView().showPoundEggErrorAndToast(e.getMessage());
                }
            }

            @Override
            public void onResponse(ServiceResult<EggFreeGiftInfo> response) {
                if (response != null) {
                    if (response.isSuccess()) {
                        if (getMvpView() != null) {
                            List<EggGiftInfo> giftInfoList = response.getData() != null ? response.getData().getFreeDrawGiftList() : null;
                            getMvpView().showPoundEggReward(giftInfoList, playAnim);
                            getMvpView().refreshGold();
                            setLotteryGiftInfo(giftInfoList);
                            refreshGiftList();
                            getMvpView().showPoundEggFreeTimes(response.getData() != null ? response.getData().getFreeDrawGiftTimes() : 0);
                        }
                    } else {
                        if (getMvpView() != null) {
                            getMvpView().showPoundEggErrorAndToast(response.getMessage());
                        }
                    }
                } else {
                    if (getMvpView() != null) {
                        getMvpView().showPoundEggErrorAndToast("数据异常");
                    }
                }
            }
        });
    }

    /**
     * 获取免费砸蛋次数
     */
    public void getPoundEggFreeTimes() {
        poundEggModel.getPoundEggFreeTimes(new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onResponse(Json response) {
                if (response != null && response.num("code") == 200) {
                    Json data = response.json("data");
                    if (data != null) {
                        if (getMvpView() != null) {
                            getMvpView().showPoundEggFreeBtn(data.num("switchBtn", 0) == 1);
                            getMvpView().showPoundEggFreeTimes(data.num("freeDrawTimes"));
                        }
                    }
                }
            }
        });
    }

    /**
     * 获取砸蛋配置
     */
    public void getPoundCfg() {
        poundEggModel.getPoundCfg(new HttpRequestCallBack<DrawCfg>() {
            @Override
            public void onSuccess(String message, DrawCfg drawCfg) {
                if (getMvpView() != null) {
                    getMvpView().refreshCfg(drawCfg);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                LogUtil.i("getPoundCfg fail," + code + " : " + msg);
            }
        });
    }

    /***
     * 更新礼物信息 普通礼物和神秘礼物
     * @param winningGifts 抽中的礼物集合
     */
    private void setLotteryGiftInfo(List<EggGiftInfo> winningGifts) {

        if (ListUtils.isListEmpty(winningGifts)) {
            return;
        }

        for (EggGiftInfo info : winningGifts) {
            if (info.getGoldPrice() < 500)
                continue;
            //发送本房种豆公屏消息
            CoreManager.getCore(IGiftCore.class).sendLotteryMeg(info, info.getGiftNum());
        }
    }

    /**
     * 更新缓存礼物
     */
    private void refreshGiftList() {
        CoreManager.getCore(IGiftCore.class).requestGiftInfos();
    }

}
