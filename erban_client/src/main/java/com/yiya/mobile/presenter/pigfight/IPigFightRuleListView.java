package com.yiya.mobile.presenter.pigfight;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;

import java.util.List;

public interface IPigFightRuleListView extends IMvpBaseView {
    void showRuleList(ServiceResult<List<String>> serviceResult);
}
