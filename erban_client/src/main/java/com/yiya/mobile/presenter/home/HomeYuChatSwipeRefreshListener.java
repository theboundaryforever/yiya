package com.yiya.mobile.presenter.home;

/**
 * @author Zhangsongzhou
 * @date 2019/3/21
 * <p>
 * 语聊  viewpager  recyclerView嵌套 滑动
 */
public interface HomeYuChatSwipeRefreshListener {
    void onYuChatSwipeRefreshCallback(int value);
}
