package com.yiya.mobile.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_framework.util.util.DisplayUtils;


/**
 * Created by Administrator on 2018/3/10.
 */

public class LevelView extends LinearLayout {

    ImageView mExperLevel;
    ImageView mCharmLevel;
    ImageView mUserLevel;
    private Context mContext;

    public LevelView(Context context) {
        this(context, null);
    }

    public ImageView getExperLevel() {

        return mExperLevel;
    }

    public ImageView getCharmLevel() {
        return mCharmLevel;
    }

    public LevelView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        View mInflate = View.inflate(context, R.layout.level_view, this);
        mCharmLevel = mInflate.findViewById(R.id.charm_level);
        mExperLevel = mInflate.findViewById(R.id.exper_level);
    }

    // 设置财富等级
    public void setExperLevel(int experLevel) {
        if (experLevel < 1) {
            mExperLevel.setVisibility(GONE);
//            seatView.setVisibility(GONE);
            return;
        }
        if (experLevel > 100) {
            experLevel = 100;
        }
        mExperLevel.setVisibility(VISIBLE);

        int drawableId = getResources().getIdentifier("lv" + experLevel, "drawable", mContext.getPackageName());
        mExperLevel.setBackgroundResource(drawableId);
    }

    // 设置财富等级
    public void setExperLevel(int experLevel, int w, int h, int marginEnd) {
        if (experLevel < 1) {
            mExperLevel.setVisibility(GONE);
            return;
        } else {
            mExperLevel.setVisibility(VISIBLE);
        }
        if (experLevel > 100) {
            experLevel = 100;
        }

        int drawableId = getResources().getIdentifier("lv" + experLevel, "drawable", mContext.getPackageName());
        mExperLevel.setBackgroundResource(drawableId);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.dip2px(getContext(), w), DisplayUtils.dip2px(getContext(), h));
        params.setMarginEnd(DisplayUtils.dip2px(getContext(), marginEnd));
        mExperLevel.setLayoutParams(params);
    }

    // 设置魅力等级
    public void setCharmLevel(int charmLevel) {
        if (charmLevel < 1) {
            mCharmLevel.setVisibility(GONE);
            return;
        } else {
            mCharmLevel.setVisibility(VISIBLE);
        }

        if (charmLevel >= 70) {
            charmLevel = 70;
        }

        int drawableId = getResources().getIdentifier("ml" + charmLevel, "drawable", mContext.getPackageName());

        mCharmLevel.setBackgroundResource(drawableId);
    }

    // 设置魅力等级
    public void setCharmLevel(int charmLevel, int w, int h, int marginEnd) {
        if (charmLevel < 1) {
            mCharmLevel.setVisibility(GONE);
            return;
        } else {
            mCharmLevel.setVisibility(VISIBLE);
        }

        if (charmLevel >= 70) {
            charmLevel = 70;
        }

        int drawableId = getResources().getIdentifier("ml" + charmLevel, "drawable", mContext.getPackageName());
        mCharmLevel.setBackgroundResource(drawableId);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DisplayUtils.dip2px(getContext(), w), DisplayUtils.dip2px(getContext(), h));
        params.setMarginEnd(DisplayUtils.dip2px(getContext(), marginEnd));
        mCharmLevel.setLayoutParams(params);
    }
}
