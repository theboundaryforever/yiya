package com.yiya.mobile.view;

import android.content.Context;
import android.util.AttributeSet;

import com.juxiao.library_scan.CustomScannerView;


import me.dm7.barcodescanner.core.IViewFinder;

/**
 * @author Zhangsongzhou
 * @date 2019/4/1
 */
public class ScanView extends CustomScannerView {
    public ScanView(Context context) {
        super(context);
    }

    public ScanView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected IViewFinder createViewFinderView(Context context) {
        return new ScanFinderView(context);
//        return super.createViewFinderView(context);
    }

}
