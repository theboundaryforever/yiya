package com.yiya.mobile.view.htmlTextView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.netease.nim.uikit.glide.GlideApp;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/7.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class HtmlTextView extends AppCompatTextView {

    public HtmlTextView(Context context) {
        this(context, null);
    }

    public HtmlTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HtmlTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    /**
     * 设置html富文本+图片
     *
     * @param htmlText
     */
    public void setHtmlText(String htmlText) {
//        String tmp = "<span style=\"color: #FFFFFF; font-size: 12; font-family: \"PingFangSC-Regular\";\"><img src=\"https://pic.lyy18.cn/pigFight5@3x.png\" alt=\"\" style=\"display: inline-block;vertical-align: top;width: 16px;height: 16px;\">5号红猪在打斗中取得了胜利 </span>";
        setText(Html.fromHtml(htmlText, new HtmlImgGetter(this), null));
    }

    public class HtmlImgGetter implements Html.ImageGetter {
        TextView container;

        public HtmlImgGetter(TextView text) {
            this.container = text;
        }

        @Override
        public Drawable getDrawable(String source) {
            final LevelListDrawable drawable = new LevelListDrawable();

            Glide.with(BasicConfig.INSTANCE.getAppContext()).asBitmap().load(source).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> glideAnimation) {
                    if (resource != null) {
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(resource);
                        drawable.addLevel(1, 1, bitmapDrawable);
                        drawable.setBounds(0, 0, resource.getWidth(), resource.getHeight());
                        drawable.setLevel(1);
                        container.invalidate();
                        container.setText(container.getText());
                    }
                }
            });
            return drawable;
        }
    }

}
