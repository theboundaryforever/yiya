package com.yiya.mobile.im.actions;

import com.netease.nim.uikit.session.actions.BaseAction;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.gift.ChargeListener;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.room.queue.bean.MicMemberInfo;
import com.yiya.mobile.room.gift.GiftDialog;
import com.yiya.mobile.ui.me.wallet.activity.WalletActivity;

import java.util.List;

/**
 * Created by chenran on 2017/10/2.
 */

public class GiftAction extends BaseAction implements GiftDialog.OnGiftDialogBtnClickListener, ChargeListener {


    public GiftAction() {
        super(R.drawable.icon_gift_action, R.string.gift_action);
    }

    transient private GiftDialog giftDialog;

    @Override
    public void onClick() {
        isShowingChargeDialog = false;
        if (giftDialog == null) {
            giftDialog = new GiftDialog(getActivity(), Long.valueOf(getAccount()));
            giftDialog.setGiftDialogBtnClickListener(this);
            giftDialog.setSinglePeople(true);
            giftDialog.setOnDismissListener(dialog -> giftDialog = null);
        }
        if (!giftDialog.isShowing()) {
            if (giftDialog != null) {
                giftDialog.show();
            }

        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long uid, int number) {
        if (giftInfo != null && uid > 0) {
//            CoreManager.getCore(IGiftCore.class).sendPersonalGiftToNIM(giftInfo.getGiftId(), uid, number, giftInfo.getGoldPrice(), new WeakReference<>(getContainer()), this);
        }
    }

    @Override
    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, long uid, int number, long comboId, long comboCount) {

    }

    @Override
    public void onUserInfoClick(long uid) {

    }

    private boolean isShowingChargeDialog;

    @Override
    public void onNeedCharge() {
        if (isShowingChargeDialog) {
            return;
        }
        isShowingChargeDialog = true;
        ChargeDialogFragment.instance("余额不足，是否充值", (view, fragment) -> {
            if (view.getId() == R.id.btn_cancel) {
                fragment.dismiss();
            } else if (view.getId() == R.id.btn_ok) {
                WalletActivity.start(getActivity());
                fragment.dismiss();
            }
            isShowingChargeDialog = false;
        }).show(getActivity().getFragmentManager(), "charge");

    }
}

//public class GiftAction extends BaseAction implements GiftDialog.OnGiftDialogBtnClickListener, ChargeListener {
//
//
//    public GiftAction() {
//        super(R.drawable.icon_gift_action, R.string.gift_action);
//    }
//
//    transient private GiftDialog giftDialog;
//
//    @Override
//    public void onClick() {
//        isShowingChargeDialog = false;
//        if (giftDialog == null) {
//            giftDialog = new GiftDialog(getActivity(), Long.valueOf(getAccount()), false);
//            giftDialog.setGiftDialogBtnClickListener(this);
//            giftDialog.setSinglePeople(true);
//            giftDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface dialog) {
//                    giftDialog = null;
//                }
//            });
//        }
//        if (!giftDialog.isShowing()) {
//            if (giftDialog != null) {
//                giftDialog.show();
//            }
//
//        }
//    }
//
//    @Override
//    public void onRechargeBtnClick() {
//        WalletActivity.start(getActivity());
//    }
//
//    @Override
//    public void onSendGiftBtnClick(GiftInfo giftInfo, long uid, int number) {
//        if (giftInfo != null) {
//            CoreManager.getCore(IGiftCore.class).sendPersonalGiftToNIM(giftInfo.getGiftId(), uid, number, giftInfo.getGoldPrice(), new WeakReference<>(getContainer()), this);
//        }
//    }
//
//    @Override
//    public void onSendGiftBtnClick(GiftInfo giftInfo, List<MicMemberInfo> micMemberInfos, int number) {
//    }
//
//    private boolean isShowingChargeDialog;
//
//    @Override
//    public void onNeedCharge() {
//        if (isShowingChargeDialog) {
//            return;
//        }
//        isShowingChargeDialog = true;
//        ChargeDialogFragment.instance("余额不足，是否充值", new ChargeDialogFragment.ChargeDialogListener() {
//            @Override
//            public void onClick(View view, ChargeDialogFragment fragment) {
//                if (view.getId() == R.id.btn_cancel) {
//                    fragment.dismiss();
//                } else if (view.getId() == R.id.btn_ok) {
//                    WalletActivity.start(getActivity());
//                    fragment.dismiss();
//                }
//                isShowingChargeDialog = false;
//            }
//        }).show(getActivity().getFragmentManager(), "charge");
//
//    }
//}