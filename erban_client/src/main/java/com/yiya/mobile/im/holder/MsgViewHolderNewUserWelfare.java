package com.yiya.mobile.im.holder;

import android.widget.TextView;

import com.yiya.mobile.model.message.MessageModel;
import com.netease.nim.uikit.NimUIKit;
import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.MsgService;
import com.tongdaxing.erban.R;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.im.custom.bean.NewUserWelfareAttachment;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chenran on 2017/9/21.
 */

public class MsgViewHolderNewUserWelfare extends MsgViewHolderBase {

    private TextView tvStatus;
    private TextView tvContent;
    private TextView tvCarName;

    private MessageModel mMessageModel = new MessageModel();

    public MsgViewHolderNewUserWelfare(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.layout_msg_view_holder_new_user_welfare;
    }

    @Override
    protected void inflateContentView() {
        tvContent = findViewById(R.id.tv_content);
        tvCarName = findViewById(R.id.tv_car_name);
        tvStatus = findViewById(R.id.tv_status);
    }

    @Override
    protected void bindContentView() {
        NewUserWelfareAttachment attachment = (NewUserWelfareAttachment) message.getAttachment();
        if (attachment == null) return;
        tvContent.setText(attachment.getMessage());
        tvCarName.setText(attachment.getGiftCarName());

        Map<String, Object> localExtension = message.getLocalExtension();
        final boolean status = localExtension != null && localExtension.containsKey("status") && (boolean) localExtension.get("status");
        refreshView(status);

        tvStatus.setOnClickListener(v -> {
            getCar();
        });
    }

    private void updateLocalState(boolean status){// attachment.isGet() 服务器写死为false
        HashMap<String, Object> localExtension = new HashMap<>();
        localExtension.put("status", status);
        message.setLocalExtension(localExtension);
        NIMClient.getService(MsgService.class).updateIMMessage(message);
    }

    private void refreshView(boolean status){
        if (status) {
            tvStatus.setSelected(true);
            tvStatus.setTextColor(0xFFA8A8A8);
            tvStatus.setText("已领取");
        } else {
            tvStatus.setSelected(false);
            tvStatus.setTextColor(0xFFFFFFFF);
            tvStatus.setText("领取");
        }
    }

    private void getCar(){
        Map<String,String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getNewUserWelfare(), param, new HttpRequestCallBack<Object>() {

            @Override
            public void onSuccess(String message, Object response) {
                updateLocalState(true);
                refreshView(true);

                if (NimUIKit.getCheckUserBlacklistListener() != null) {
                    NimUIKit.getCheckUserBlacklistListener().onTxtOperationResult("谢谢你啦~");
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                SingleToastUtil.showToast(msg);
                if (code == 1456){
                    refreshView(true);
                }
            }
        });
    }

    public void checkUserOperation(String tgUid, String content, String operation, OkHttpManager.MyCallBack<Json> callBack) {
        mMessageModel.checkUserOperation(tgUid, content, operation, callBack);
    }
}
