
package com.yiya.mobile.im.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.netease.nim.uikit.common.ui.recyclerview.adapter.BaseMultiItemFetchLoadAdapter;
import com.netease.nim.uikit.common.util.string.StringUtil;
import com.netease.nim.uikit.session.viewholder.MsgViewHolderBase;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.uinfo.UserService;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;
import com.tongdaxing.erban.R;
import com.tongdaxing.xchat_core.im.custom.bean.nim.SysMsgNewFansAttachment;
import com.yiya.mobile.utils.ImageLoadUtils;
import com.yiya.mobile.utils.UIHelper;

/**
 * ProjectName:
 * Description: 系统消息 - 关注了你 ViewHolder
 * Created by BlackWhirlwind on 2019/7/3.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MsgViewHolderNewFans extends MsgViewHolderBase implements View.OnClickListener {
    private ImageView avatar;
    private TextView nick;
    private View container;

    public MsgViewHolderNewFans(BaseMultiItemFetchLoadAdapter adapter) {
        super(adapter);
    }

    @Override
    protected int getContentResId() {
        return R.layout.layout_msg_view_holder_new_fans;
    }

    @Override
    protected void inflateContentView() {
        avatar = findViewById(R.id.avatar);
        nick = findViewById(R.id.tv_nick);
        container = findViewById(R.id.cl_container);
    }

    @Override
    protected void bindContentView() {
        SysMsgNewFansAttachment attachment = (SysMsgNewFansAttachment) message.getAttachment();
        if (attachment != null) {
            if (!StringUtil.isEmpty(attachment.getNick())) {
                nick.setText(attachment.getNick());
                ImageLoadUtils.loadAvatar(avatar.getContext(), attachment.getAvatar(), avatar);
            } else {
                NimUserInfo nimUserInfo = NIMClient.getService(UserService.class).getUserInfo(attachment.getUid() + "");
                if (nimUserInfo != null) {
                    nick.setText(nimUserInfo.getName());
                    ImageLoadUtils.loadAvatar(avatar.getContext(), nimUserInfo.getAvatar(), avatar);
                }
            }
            container.setOnClickListener(this);
        }
    }

    /**
     * 跳转用户中心
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        SysMsgNewFansAttachment attachment = (SysMsgNewFansAttachment) message.getAttachment();
        if (attachment != null && attachment.getUid() != 0) {
            UIHelper.showUserInfoAct(context, attachment.getUid());
        }
    }
}
