package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 * 文件描述：
 * 公聊大厅的红包消息
 * @auther：zwk
 * @data：2019/1/7
 */
public class PublicChatRedPackageAttachment extends IMCustomAttachment{
    private long redPackId;//红包id;
    private float getNum;//抢到的红包值  float保留两位小数
    private int server_msg_id;


    public PublicChatRedPackageAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        if (data.containsKey("server_msg_id"))
            server_msg_id = data.getIntValue("server_msg_id");
        if (data.containsKey("redPackId"))
            redPackId = data.getLongValue("redPackId");
        if (data.containsKey("getNum"))
            getNum = data.getFloatValue("getNum");

    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("redPackId", redPackId);
        jsonObject.put("getNum",getNum);
        jsonObject.put("server_msg_id",server_msg_id);
        return jsonObject;
    }

    /**
     * 发送消息因为发送位置是Json类型导致旧版方法返回的JSONObject被放入Json中时自带反斜杠，发送给后台
     * 会被转义多出两个\\出来而出现无法解析问题  --- json字符串格式不不正确
     * @return
     */
    @Override
    protected Json packData2() {
        Json jsonObject = new Json();
        jsonObject.set("redPackId", redPackId);
        jsonObject.set("getNum",getNum);
        jsonObject.set("server_msg_id",server_msg_id);
        return jsonObject;
    }


    public long getRedPackId() {
        return redPackId;
    }

    public void setRedPackId(long redPackId) {
        this.redPackId = redPackId;
    }

    public float getGetNum() {
        return getNum;
    }

    public void setGetNum(float getNum) {
        this.getNum = getNum;
    }

    public int getServer_msg_id() {
        return server_msg_id;
    }

    public void setServer_msg_id(int server_msg_id) {
        this.server_msg_id = server_msg_id;
    }
}
