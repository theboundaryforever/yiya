package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * 文件描述：房间提示消息 — 停留规定时间的提示关注 消息附件
 * @auther：zwk
 * @data：2019/1/4
 */
public class TipMsgGoAttentionAttachment extends IMCustomAttachment {
    private long uid;
    private int stayTime;//房间停留时间
    private boolean isAttention = false;


    public TipMsgGoAttentionAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        stayTime = data.getIntValue("minute");
        uid = data.getLongValue("uid");
    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("minute", stayTime);
        jsonObject.put("uid",uid);
        return jsonObject;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public int getStayTime() {
        return stayTime;
    }

    public void setStayTime(int stayTime) {
        this.stayTime = stayTime;
    }

    public boolean isAttention() {
        return isAttention;
    }

    public void setAttention(boolean attention) {
        isAttention = attention;
    }
}
