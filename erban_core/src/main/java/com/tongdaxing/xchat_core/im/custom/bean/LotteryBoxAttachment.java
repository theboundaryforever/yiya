package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSONObject;


/**
 * Created by Administrator on 2018/3/20.
 */

public class LotteryBoxAttachment extends IMCustomAttachment {
    private String giftName;
    private int count;
    private String nick;
    private int goldPrice;
    private boolean isFull = false;
    private long uid;

    public LotteryBoxAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        JSONObject param = data.getJSONObject("params");
        nick = param.getString("nick");
        giftName = param.getString("giftName");
        count = param.getIntValue("count");
        if (param.containsKey("goldPrice"))
            goldPrice = param.getIntValue("goldPrice");
        if (param.containsKey("isFull"))
            isFull = param.getBooleanValue("isFull");
        if (param.containsKey("uid")) {
            uid = param.getLongValue("uid");
        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        JSONObject params = new JSONObject();
        params.put("nick", nick);
        params.put("giftName", giftName);
        params.put("count", count);
        params.put("isFull", isFull);
        params.put("goldPrice", goldPrice);
        params.put("uid", uid);
        jsonObject.put("params", params);
        return jsonObject;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public boolean isFull() {
        return isFull;
    }

    public void setFull(boolean full) {
        isFull = full;
    }

    public int getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(int goldPrice) {
        this.goldPrice = goldPrice;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
