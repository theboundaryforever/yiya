package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.gift.GiftReceiveInfo;


/**
 * Created by chenran on 2017/7/28.
 */

public class GiftAttachment extends IMCustomAttachment {
    protected GiftReceiveInfo giftRecieveInfo;
    private String uid;

    public GiftAttachment(int first, int second) {
        super(first, second);
    }

    public GiftReceiveInfo getGiftRecieveInfo() {
        return giftRecieveInfo;
    }

    public void setGiftRecieveInfo(GiftReceiveInfo giftRecieveInfo) {
        this.giftRecieveInfo = giftRecieveInfo;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    //财富等级
    protected int experLevel;
    //魅力等级
    protected int charmLevel;

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    @Override
    protected void parseData(JSONObject data) {
        giftRecieveInfo = new Gson().fromJson(data.toJSONString(), GiftReceiveInfo.class);

//        giftRecieveInfo = new GiftReceiveInfo();
//        giftRecieveInfo.setUid(data.getLongValue("uid"));
//        giftRecieveInfo.setGiftId(data.getIntValue("giftId"));
//        giftRecieveInfo.setAvatar(data.getString("avatar"));
//        giftRecieveInfo.setNick(data.getString("nick"));
//        giftRecieveInfo.setTargetUid(data.getLongValue("targetUid"));
//        giftRecieveInfo.setGiftNum(data.getIntValue("giftNum"));
//        giftRecieveInfo.setTargetNick(data.getString("targetNick"));
//        giftRecieveInfo.setTargetAvatar(data.getString("targetAvatar"));
//        giftRecieveInfo.setComboId(data.getLongValue("comboId"));
//        giftRecieveInfo.setComboCount(data.getLongValue("comboCount"));
//        giftRecieveInfo.setComboFrequencyCount(data.getLongValue("comboFrequencyCount"));
//        giftRecieveInfo.setRoomType(data.getInteger("roomType"));
//        giftRecieveInfo.setGiftInfo(new Gson().fromJson(data.getString("giftInfo"), GiftInfo.class));
//
//        if (data.containsKey("giftSendTime")) {
//            giftRecieveInfo.setGiftSendTime(data.getLongValue("giftSendTime"));
//        } else {
//            giftRecieveInfo.setGiftSendTime(System.currentTimeMillis());
//        }
//        giftRecieveInfo.setRoomId(data.getString("roomId"));
//        giftRecieveInfo.setUserNo(data.getString("userNo"));
//        experLevel = data.getIntValue("experLevel");
//
//        if (data.containsKey("targetUids")) {
//            JSONArray jsonArray = data.getJSONArray("targetUids");
//            List<Long> targetUids = new ArrayList<>();
//            for (int i = 0; i < jsonArray.size(); i++) {
//                Long uid = jsonArray.getLong(i);
//                targetUids.add(uid);
//            }
//            giftRecieveInfo.setTargetUids(targetUids);
//        }
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();

        object.put("uid", giftRecieveInfo.getUid());
        object.put("giftId", giftRecieveInfo.getGiftId());
        object.put("avatar", giftRecieveInfo.getAvatar());
        object.put("nick", giftRecieveInfo.getNick());
        object.put("targetUid", giftRecieveInfo.getTargetUid());
        object.put("giftNum", giftRecieveInfo.getGiftNum());
        object.put("targetNick", giftRecieveInfo.getTargetNick());
        object.put("targetAvatar", giftRecieveInfo.getTargetAvatar());
        object.put("giftSendTime", giftRecieveInfo.getGiftSendTime());
        object.put("roomId", giftRecieveInfo.getRoomId());
        object.put("userNo", giftRecieveInfo.getUserNo());
        object.put("comboId", giftRecieveInfo.getComboId());
        object.put("comboCount", giftRecieveInfo.getComboCount());
        object.put("comboFrequencyCount", giftRecieveInfo.getComboFrequencyCount());
        object.put("experLevel", experLevel);
        object.put("roomType", giftRecieveInfo.getRoomType());
        object.put("giftInfo", giftRecieveInfo.getGiftInfo());

        if (!ListUtils.isListEmpty(giftRecieveInfo.getTargetUids())) {
            JSONArray jsonArray = new JSONArray();
            jsonArray.addAll(giftRecieveInfo.getTargetUids());
            object.put("targetUids", jsonArray);
        }
        return object;
    }

    @Override
    public String toString() {
        return "GiftAttachment{" +
                "giftRecieveInfo=" + giftRecieveInfo +
                ", uid='" + uid + '\'' +
                ", experLevel=" + experLevel +
                ", charmLevel=" + charmLevel +
                '}';
    }
}
