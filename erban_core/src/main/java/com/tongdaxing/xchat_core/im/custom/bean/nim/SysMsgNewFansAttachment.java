package com.tongdaxing.xchat_core.im.custom.bean.nim;

import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;

/**
 * ProjectName:
 * Description: 系统消息 - 关注了你
 * Created by BlackWhirlwind on 2019/7/3.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class SysMsgNewFansAttachment extends CustomAttachment {

    private long uid;
    private String nick;
    private String avatar;

    public SysMsgNewFansAttachment(int first, int second) {
        super(first, second);
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    @Override
    protected void parseData(JSONObject data) {
        uid = data.getLong("uid");
        nick = data.getString("nick");
        avatar = data.getString("avatar");

        JSONObject userInfo = data.getJSONObject("userVo");
        nick = userInfo.getString("nick");
        avatar = userInfo.getString("avatar");

    }

    @Override
    protected JSONObject packData() {
        return new JSONObject();
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
