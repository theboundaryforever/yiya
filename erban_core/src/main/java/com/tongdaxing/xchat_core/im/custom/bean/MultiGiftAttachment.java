package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_core.gift.MultiGiftReceiveInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenran on 2017/10/25.
 */

public class MultiGiftAttachment extends IMCustomAttachment {
    private MultiGiftReceiveInfo multiGiftRecieveInfo;
    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public MultiGiftAttachment(int first, int second) {
        super(first, second);
    }

    public MultiGiftReceiveInfo getMultiGiftRecieveInfo() {
        return multiGiftRecieveInfo;
    }

    public void setMultiGiftAttachment(MultiGiftReceiveInfo multiGiftRecieveInfo) {
        this.multiGiftRecieveInfo = multiGiftRecieveInfo;
    }

    //财富等级
    private int experLevel;
    //魅力等级
    private int charmLevel;

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }
    @Override
    protected void parseData(JSONObject data) {
        experLevel = data.getInteger("experLevel");
        multiGiftRecieveInfo = new MultiGiftReceiveInfo();
        multiGiftRecieveInfo.setUid(data.getLong("uid"));
        multiGiftRecieveInfo.setGiftId(data.getInteger("giftId"));
        multiGiftRecieveInfo.setAvatar(data.getString("avatar"));
        multiGiftRecieveInfo.setNick(data.getString("nick"));
        multiGiftRecieveInfo.setGiftNum(data.getIntValue("giftNum"));
        if (data.containsKey("giftSendTime")) {
            multiGiftRecieveInfo.setGiftSendTime(data.getLong("giftSendTime"));
        }else {
            multiGiftRecieveInfo.setGiftSendTime(System.currentTimeMillis());
        }
        JSONArray jsonArray = data.getJSONArray("targetUids");
        List<Long> targetUids = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            Long uid = jsonArray.getLong(i);
            targetUids.add(uid);
        }
        multiGiftRecieveInfo.setTargetUids(targetUids);
    }

    @Override
    protected JSONObject packData() {

        JSONObject object = new JSONObject();
        object.put("uid", multiGiftRecieveInfo.getUid());
        object.put("giftId", multiGiftRecieveInfo.getGiftId());
        object.put("avatar", multiGiftRecieveInfo.getAvatar());
        object.put("nick", multiGiftRecieveInfo.getNick());
        object.put("giftNum", multiGiftRecieveInfo.getGiftNum());
        object.put("giftSendTime",multiGiftRecieveInfo.getGiftSendTime());
        object.put("experLevel", experLevel);
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < multiGiftRecieveInfo.getTargetUids().size(); i++) {
            Long uid = multiGiftRecieveInfo.getTargetUids().get(i);
            jsonArray.add(uid);
        }
        object.put("targetUids", jsonArray);
        return object;
    }
}
