package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/9/4
 * @describe 新用户福利
 */

@Data
public class NewUserWelfareAttachment extends CustomAttachment {
    private String giftCarName;
    private String message;
    private boolean isGet;

    public NewUserWelfareAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        super.parseData(data);

        giftCarName = data.getString("giftCarName");
        message = data.getString("message");
        isGet = data.getBoolean("isGet");
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("giftCarName", giftCarName);
        object.put("message", message);
        object.put("isGet", isGet);
        return object;
    }

}
