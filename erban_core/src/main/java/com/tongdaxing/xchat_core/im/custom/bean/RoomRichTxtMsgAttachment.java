package com.tongdaxing.xchat_core.im.custom.bean;


import com.alibaba.fastjson.JSONObject;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 *  文件描述：通用的房间自定义富文本消息附件
 *  @auther：zwk
 *  @data：2019/1/17
 *
 */
public class RoomRichTxtMsgAttachment extends IMCustomAttachment {
    private String content;

    public RoomRichTxtMsgAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        content = data.getString("content");
    }

    @Override
    protected JSONObject packData() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("content", content);
        return jsonObject;
    }

    /**
     * 发送消息因为发送位置是Json类型导致旧版方法返回的JSONObject被放入Json中时自带反斜杠，发送给后台
     * 会被转义多出两个\\出来而出现无法解析问题  --- json字符串格式不不正确
     *
     * @return
     */
    @Override
    protected Json packData2() {
        Json jsonObject = new Json();
        jsonObject.set("content", content);
        return jsonObject;
    }

    @Override
    public int getCharmLevel() {
        return charmLevel;
    }

    @Override
    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    @Override
    public int getExperLevel() {
        return experLevel;
    }

    @Override
    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }
}
