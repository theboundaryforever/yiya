package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONObject;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/7.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomHtmlTextAttachment extends IMCustomAttachment {
    private String html;

    public RoomHtmlTextAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        if (data != null) {
            html = data.getString("html");
        }
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}
