package com.tongdaxing.xchat_core.im.custom.bean;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.tongdaxing.xchat_core.room.auction.bean.AuctionInfo;
import com.tongdaxing.xchat_core.room.auction.bean.AuctionUser;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by zhouxiangfeng on 2017/6/8.
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class LuckyGiftAttachment extends IMCustomAttachment {

    private long uid;
    private String nick;
    private double proportion;// 倍数
    private double outputGold;// 金币
    private String giftName;
    private long roomUid;
    private int roomType;

    public LuckyGiftAttachment(int first, int second) {
        super(first, second);
    }

    @Override
    protected void parseData(JSONObject data) {
        setUid(data.getLong("uid"));
        setNick(data.getString("nick"));
        setProportion(data.getDouble("proportion"));
        setOutputGold(data.getDouble("outputGold"));
        setGiftName(data.getString("giftName"));
        setRoomUid(data.getLong("roomUid"));
        setRoomType(data.getInteger("roomType"));
    }

    @Override
    protected JSONObject packData() {
        JSONObject object = new JSONObject();
        object.put("uid", getUid());
        object.put("nick", getNick());
        object.put("proportion", getProportion());
        object.put("outputGold", getOutputGold());
        object.put("giftName", getGiftName());
        object.put("roomUid", getRoomUid());
        object.put("roomType", getRoomType());

        return object;
    }
}
