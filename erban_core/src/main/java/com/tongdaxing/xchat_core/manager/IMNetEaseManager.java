package com.tongdaxing.xchat_core.manager;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.juxiao.library_utils.log.LogUtil;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.chatroom.ChatRoomService;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.listener.IDisposableAddListener;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.net.statistic.StatisticManager;
import com.tongdaxing.xchat_core.PreferencesUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomMemberComeInfo;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.bean.RoomRedPackageInfo;
import com.tongdaxing.xchat_core.bean.UserExperienceUpdateInfo;
import com.tongdaxing.xchat_core.bean.VideoRoomMessage;
import com.tongdaxing.xchat_core.bean.attachmsg.RoomQueueMsgAttachment;
import com.tongdaxing.xchat_core.gift.GiftInfo;
import com.tongdaxing.xchat_core.gift.IGiftCore;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachParser;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LuckyGiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.MicInListAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PkCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.PublicChatRoomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomHtmlTextAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomRuleAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.RoomTipAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.TipMsgGoAttentionAttachment;
import com.tongdaxing.xchat_core.manager.log.LogEvent;
import com.tongdaxing.xchat_core.pk.bean.PkVoteInfo;
import com.tongdaxing.xchat_core.room.bean.CPKInviteBean;
import com.tongdaxing.xchat_core.room.bean.CPKMsgBean;
import com.tongdaxing.xchat_core.room.bean.CPKResultBean;
import com.tongdaxing.xchat_core.room.bean.HeadLineStarAnnounce;
import com.tongdaxing.xchat_core.room.bean.HeadLineStarDistance;
import com.tongdaxing.xchat_core.room.bean.HeadLineStarLimit;
import com.tongdaxing.xchat_core.room.bean.PigFightBean;
import com.tongdaxing.xchat_core.room.bean.PigFightCrtBean;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyBoxAddGold;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyRewardInfo;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyTips;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo;
import com.tongdaxing.xchat_core.room.bean.RoomPlantCucumberInfo;
import com.tongdaxing.xchat_core.room.bean.WaitQueuePersonInfo;
import com.tongdaxing.xchat_core.room.face.IFaceCore;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.im.IMError;
import com.tongdaxing.xchat_framework.im.IMKey;
import com.tongdaxing.xchat_framework.im.IMProCallBack;
import com.tongdaxing.xchat_framework.im.IMReportBean;
import com.tongdaxing.xchat_framework.im.IMReportRoute;
import com.tongdaxing.xchat_framework.im.SocketManager;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.SingleToastUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.json.JsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.Constants;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_HTML_FIRST;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_HTML_SECOND_PUB_OFF;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_HTML_SECOND_PUB_ON;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_FIRST_LuckyGift;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_FIRST_ROOM_CHARM;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_FIRST_ROOM_RED_PACKAGE;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_FIRST_ROOM_TIP;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_FIRST_TYPE_COIN;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_FACE;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MATCH;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_PK_FIRST;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_LOTTERY_BOX;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_MIC_IN_LIST;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_SECOND_ALL_LuckyGift;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_SECOND_PRIVATE_LuckyGift;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_SECOND_PUBLIC_ROOM_RECEIVE;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_SECOND_PUBLIC_ROOM_SEND;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_CHARM_UPDATE;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_SEND_RED_PACKAGE;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_GO_ATTENTION_ROOM;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_SECOND_TYPE_COIN_ALL;
import static com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment.CUSTOM_MSG_TYPE_BURST_GIFT;
import static com.tongdaxing.xchat_core.manager.AvRoomDataManager.MIC_POSITION_BY_OWNER;
import static com.tongdaxing.xchat_core.manager.RoomEvent.RECEIVE_LUCKY_GIFT_ALL;
import static com.tongdaxing.xchat_core.manager.RtcEngineManager.MUTE_STATUS_DEFAULT;
import static com.tongdaxing.xchat_core.manager.RtcEngineManager.MUTE_STATUS_MUTE_ALL;
import static com.tongdaxing.xchat_core.manager.RtcEngineManager.MUTE_STATUS_MUTE_AUDIO;
import static com.tongdaxing.xchat_core.manager.RtcEngineManager.MUTE_STATUS_MUTE_VIDEO;
import static com.tongdaxing.xchat_core.manager.RtcEngineManager.MUTE_STATUS_NONE;

/**
 * <p>聊天室管理，一个全局的Model </p>
 *
 * @author jiahui
 * @date 2017/12/12
 */
public final class IMNetEaseManager {
    private static final Object SYNC_OBJECT = new Object();
    private static volatile IMNetEaseManager sManager;
    public List<ChatRoomMessage> messages;//这里保存的消息不等于messageView显示的消息（messageView显示的消息还经过了过滤，去掉了一些不需要显示的消息）
    private int beforeDisConnectionMuteStatus; // 0 默认状态 1是断网前静音，-1是断网前非静音
    private boolean imRoomConnection = true;//房间IM连接状态
    //本地最大保存多少条消息的限制
    public static final int MESSAGE_COUNT_LOCAL_LIMIT = 1500;

    public static IMNetEaseManager get() {
        if (sManager == null) {
            synchronized (SYNC_OBJECT) {
                if (sManager == null) {
                    sManager = new IMNetEaseManager();
                }
            }
        }
        return sManager;
    }

    private IMNetEaseManager() {
        registerInComingRoomMessage();
        messages = new ArrayList<>();
    }

    private void registerInComingRoomMessage() {
        ReUsedSocketManager.get().setImNoticeMsgListener(new SocketManager.IMNoticeMsgListener() {
            @Override
            public void onNotice(Json json) {
                dealChatMessage(json);
            }

            /**
             * 重连之后先登录im->进入房间成功回调调用的方法
             */
            @Override
            public void onDisConnectEnterRoomSuc() {
                LogUtil.i(AvRoomDataManager.TAG, "onReconnection success");
                imRoomConnection = true;
                if (beforeDisConnectionMuteStatus != MUTE_STATUS_DEFAULT && AvRoomDataManager.get().isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid())) {
                    //还原断网前的禁麦状态
                    restoreMuteStatus();
                }
                handler.removeMessages(WHAT_CHECK_ROOM_CONNECT_STATE);
                resetBeforeDisConnectionMuteStatus();
                noticeRoomConnect(AvRoomDataManager.get().getRoomInfo());
            }

            /**
             * 断开连接
             * @param isCloseSelf 是否主动断开
             */
            @Override
            public void onDisConnection(boolean isCloseSelf) {
                if (imRoomConnection) {
                    imRoomConnection = false;
                    if (!isCloseSelf) {
                        //异常断开im保存禁麦状态并强制禁麦（重连成功onDisConnectEnterRoomSuc后还原）
                        saveCurrMuteStatusAndForceMute();
                        handler.removeMessages(WHAT_CHECK_ROOM_CONNECT_STATE);
                        handler.sendEmptyMessageDelayed(WHAT_CHECK_ROOM_CONNECT_STATE, 50 * 1000);
                        LogUtil.i(AvRoomDataManager.TAG, "isCloseSelf onDisConnection");
                    } else {
                        LogUtil.i(AvRoomDataManager.TAG, "onDisConnection");
                    }
                }
            }

            @Override
            public void onLoginError(int err_code, String reason) {
                if (err_code == IMError.IM_ERROR_LOGIN_AUTH_FAIL || err_code == IMError.IM_ERROR_GET_USER_INFO_FAIL) {
                    Json req_data = new Json();
                    req_data.set("errno", err_code);
                    req_data.set("errmsg", reason);
                    onKickOffLogin(req_data);
                } else if (err_code == IMError.IM_USER_IN_ROOM_BLACK_LIST) {
                    noticeKickOutChatMember(err_code, reason, CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
                    return;
                }
                SingleToastUtil.showToast(reason);
            }

            @Override
            public void onDisConntectIMLoginSuc() {
                noticeIMConnectLoginSuc();
            }
        });
    }

    /**
     * 保存当前禁麦状态，并强制禁麦
     */
    private void saveCurrMuteStatusAndForceMute() {
        beforeDisConnectionMuteStatus = RtcEngineManager.get().getCurrMuteStatus();
        if (AvRoomDataManager.get().getRoomInfo() != null) {
            int muteRoomType = AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0;
            RtcEngineManager.get().setMute(muteRoomType, true);
            if (muteRoomType == 1) {
                RtcEngineManager.get().muteLocalVideoStream(true);
            }
        }
    }

    /**
     * 还原禁麦状态
     */
    private void restoreMuteStatus() {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().getRoomQueueMemberInfoMyself();
        if (AvRoomDataManager.get().getRoomInfo() != null && roomQueueInfo != null && roomQueueInfo.mRoomMicInfo != null && !roomQueueInfo.mRoomMicInfo.isMicMute() && !roomQueueInfo.mRoomMicInfo.isMicLock()) {
            int muteRoomType = AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0;
            switch (beforeDisConnectionMuteStatus) {
                case MUTE_STATUS_NONE:
                    RtcEngineManager.get().setMute(muteRoomType, false);
                    if (muteRoomType == 1) {
                        RtcEngineManager.get().muteLocalVideoStream(false);
                    }
                    break;
                case MUTE_STATUS_MUTE_AUDIO:
                    RtcEngineManager.get().setMute(muteRoomType, true);
                    if (muteRoomType == 1) {
                        RtcEngineManager.get().muteLocalVideoStream(false);
                    }
                    break;
                case MUTE_STATUS_MUTE_VIDEO:
                    RtcEngineManager.get().setMute(muteRoomType, false);
                    if (muteRoomType == 1) {
                        RtcEngineManager.get().muteLocalVideoStream(true);
                    }
                    break;
                case MUTE_STATUS_MUTE_ALL:
                    RtcEngineManager.get().setMute(muteRoomType, true);
                    if (muteRoomType == 1) {
                        RtcEngineManager.get().muteLocalVideoStream(true);
                    }
                    break;
            }
        }
    }

    /**
     * 处理IM消息
     */
    private void dealChatMessage(Json json) {
        if (json != null) {
            LogUtil.i(AvRoomDataManager.TAG, json.toString());
            String route = json.str(IMKey.route);
            Json req_data = json.json(IMKey.req_data);
            if (StringUtils.isNotEmpty(route) && req_data != null) {
                String roomId = req_data.str(IMKey.room_id, "-1");
                ChatRoomMessage msg = new ChatRoomMessage();
                msg.setRoute(route);
                msg.setRoom_id(roomId);
                if (IMReportRoute.sendMessageReport.equalsIgnoreCase(route) || IMReportRoute.sendPublicMsgNotice.equalsIgnoreCase(route)) {//处理自定义消息
                    String custom = "";
                    custom = req_data.str(IMKey.custom);
                    IMCustomAttachment iMCustomAttachment = IMCustomAttachParser.parse(custom);
                    if (iMCustomAttachment == null) return;
                    if (req_data.has(IMKey.member)) {
                        parseCustomMember(req_data, msg);
                    }
                    msg.setAttachment(iMCustomAttachment);

                    // TODO: 2019/8/15 注意全服的需要放在这里
                    switch (iMCustomAttachment.getFirst()) {
                        case CUSTOM_MSG_FIRST_TYPE_COIN:
                            //豆币消息
                            if (iMCustomAttachment.getSecond() == CUSTOM_MSG_SECOND_TYPE_COIN_ALL) {
                                //豆币全服消息
                                addMessages(msg);
                            }
                            break;
                        case CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT://全服消息类型
                            ArrayList<ChatRoomMessage> messages2 = new ArrayList<>();
                            messages2.add(msg);
                            CoreManager.getCore(IGiftCore.class).onReceiveChatRoomMessages(messages2);
                            break;
                        case CUSTOM_MSG_FIRST_LuckyGift://幸运礼物
                            if (iMCustomAttachment.getSecond() == CUSTOM_MSG_SECOND_ALL_LuckyGift) {// 全服通知
                                dealWithLuckyGift(msg);
                            }
                            break;
                    }

                    //拦截非当前房间的消息(排除交友广场)
                    if (CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM != iMCustomAttachment.getFirst() && !"-1".equals(roomId) && !AvRoomDataManager.get().isCurrRoom(roomId)) {
                        return;
                    }

                    switch (iMCustomAttachment.getFirst()) {
                        case CUSTOM_MSG_HEADER_TYPE_QUEUE://麦序列表相关自定义消息
                            dealWithMicQueueMessage(iMCustomAttachment, msg);
                            break;
//                        case CUSTOM_MSG_HEADER_TYPE_AUCTION:
//                            dealWithFirstAuctionMessage(iMCustomAttachment, msg);
//                            break;
                        case CUSTOM_MSG_MIC_IN_LIST://排麦
                            dealWithMicInList(msg);
                            break;
                        case CUSTOM_MSG_HEADER_TYPE_FACE://表情
                            ArrayList<ChatRoomMessage> messages = new ArrayList<>();
                            messages.add(msg);
                            CoreManager.getCore(IFaceCore.class).onReceiveChatRoomMessages(messages);
                            break;
                        case CUSTOM_MSG_HEADER_TYPE_MATCH://速配
                            addMessages(msg);
                            CoreManager.getCore(IFaceCore.class).onReceiveRoomMatchFace(msg);
                            break;
                        case CUSTOM_MSG_HEADER_TYPE_GIFT://单个礼物消息
                        case CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT://多个和全麦礼物
                            addMessages(msg);// 添加到公屏
                            noticeLuckyGiftAddCoin(msg);// 是否显示加币动画
                            ArrayList<ChatRoomMessage> messages2 = new ArrayList<>();
                            messages2.add(msg);
                            CoreManager.getCore(IGiftCore.class).onReceiveChatRoomMessages(messages2);
                            break;
                        case CUSTOM_MSG_FIRST_ROOM_TIP://关注提示、分享提示
                            if (iMCustomAttachment.getSecond() == CUSTOM_MSG_SECOND_ROOM_TIP_GO_ATTENTION_ROOM) {//房间的提醒关注
                                if (iMCustomAttachment instanceof TipMsgGoAttentionAttachment
                                        && AvRoomDataManager.get().isOwner(((TipMsgGoAttentionAttachment) iMCustomAttachment).getUid())) {
                                    addMessages(msg);
                                }
                            } else if (iMCustomAttachment.getSecond() == IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_SHARE_ROOM) {//房间分享成功提示消息
                                addMessages(msg);
                            } else if (iMCustomAttachment.getSecond() == IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_ATTENTION_ROOM_OWNER) {//房间关注房主提示消息
                                addMessages(msg);
                            }
                            break;
                        case CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM://交友广场
                            //这种写法，避免旧版本没有当前消息second类型，但是消息列表会出现空的消息,有新的second需要在这里新增
                            if (iMCustomAttachment.getSecond() == CUSTOM_MSG_FIRST_PUBLIC_CHAT_ROOM
                                    || iMCustomAttachment.getSecond() == CUSTOM_MSG_SECOND_PUBLIC_ROOM_SEND
                                    || iMCustomAttachment.getSecond() == CUSTOM_MSG_SECOND_PUBLIC_ROOM_RECEIVE) {
                                addMessages(msg);
                            }
                            break;
                        case CUSTOM_MSG_LOTTERY_BOX://砸蛋
                        case CUSTOM_MSG_HEADER_TYPE_PK_FIRST://PK
                        case CUSTOM_MSG_TYPE_BURST_GIFT://爆出礼物
                            addMessages(msg);
                            break;
                        case CUSTOM_MSG_FIRST_LuckyGift://幸运礼物
                            if (iMCustomAttachment.getSecond() == CUSTOM_MSG_SECOND_PRIVATE_LuckyGift) {
                                dealWithLuckyGift(msg);
                            }
                            break;
                        case CUSTOM_MSG_FIRST_ROOM_RED_PACKAGE://房间红包消息
                            addMessages(msg);
                            if (iMCustomAttachment.getSecond() == CUSTOM_MSG_SECOND_ROOM_SEND_RED_PACKAGE) {
                                dealWithRedPackMessage(msg);
                            }
                            break;
                        case CUSTOM_MSG_FIRST_ROOM_CHARM:
                            if (iMCustomAttachment.getSecond() == CUSTOM_MSG_SECOND_ROOM_CHARM_UPDATE) {
                                if (iMCustomAttachment instanceof RoomCharmAttachment) {
                                    RoomCharmAttachment roomCharmAttachment = (RoomCharmAttachment) iMCustomAttachment;
                                    if (!AvRoomDataManager.get().isHasCharm()) {   //已经被弃用
                                        AvRoomDataManager.get().setHasCharm(true);
                                    }
                                    if (roomCharmAttachment.getTimestamps() > AvRoomDataManager.get().getCharmTimestamps()) {
                                        AvRoomDataManager.get().addMicRoomCharmInfo(((RoomCharmAttachment) iMCustomAttachment).getLatestCharm());
                                    }
                                    noticeCharmUpdate((RoomCharmAttachment) iMCustomAttachment);
                                }

                            }
                            break;
                        case CUSTOM_HTML_FIRST:// html消息
                            addMessages(msg);
                            if (iMCustomAttachment.getSecond() == CUSTOM_HTML_SECOND_PUB_ON || iMCustomAttachment.getSecond() == CUSTOM_HTML_SECOND_PUB_OFF) {// 开关公屏消息过滤
                                dealWithPublicMessage(msg);
                            }
                            break;
                    }
                } else {
                    if (IMReportRoute.LuckyTreasureChestWinningUser.equalsIgnoreCase(route)) { // TODO: 2019/8/15 注意全服的需要放在这里
                        //全服中奖用户提示
                        noticeLuckyReward(parse2RoomLuckyRewardInfo(req_data), RoomEvent.CODE_LUCKY_BOX_REWARD_USER);
                    } else if (IMReportRoute.LuckyTreasureChestWinningRoomUser.equalsIgnoreCase(route)) {
                        //全服中奖主播提示
                        noticeLuckyReward(parse2RoomLuckyRewardInfo(req_data), RoomEvent.CODE_LUCKY_BOX_REWARD_BROADCASTER);
                    }

                    //拦截非当前房间的消息
                    if (!"-1".equals(roomId) && !AvRoomDataManager.get().isCurrRoom(roomId)) {
                        return;
                    }

                    if (IMReportRoute.sendTextReport.equalsIgnoreCase(route)) {
                        // 公屏消息
                        dealWithTxtMessage(msg, req_data);
                    } else if (IMReportRoute.chatRoomMemberIn.equalsIgnoreCase(route)) {
                        chatRoomMemberIn(msg, req_data);
                    } else if (IMReportRoute.chatRoomMemberExit.equalsIgnoreCase(route)) {
                        chatRoomMemberExit(req_data);
                    } else if (IMReportRoute.QueueMemberUpdateNotice.equalsIgnoreCase(route)) {
                        chatRoomQueueChangeNotice(req_data);
                    } else if (IMReportRoute.ChatRoomMemberKicked.equalsIgnoreCase(route)) {
                        onKickedOutRoom(req_data);
                    } else if (IMReportRoute.QueueMicUpdateNotice.equalsIgnoreCase(route)) {
                        roomQueueMicUpdate(req_data);
                    } else if (IMReportRoute.ChatRoomInfoUpdated.equalsIgnoreCase(route)) {
                        chatRoomInfoUpdate(req_data);
                    } else if (IMReportRoute.ChatRoomMemberBlackAdd.equalsIgnoreCase(route)) {
                        //这里导致拉黑踢出不显示提示
                        addBlackList(req_data);
                    } else if (IMReportRoute.ChatRoomMemberBlackRemove.equalsIgnoreCase(route)) {

                    } else if (IMReportRoute.ChatRoomManagerAdd.equalsIgnoreCase(route)) {
                        addManagerMember(msg, req_data);
                    } else if (IMReportRoute.ChatRoomManagerRemove.equalsIgnoreCase(route)) {
                        dealWithRemoveManager(req_data);
                    } else if (IMReportRoute.kickoff.equalsIgnoreCase(route)) {
                        onKickOffLogin(req_data);
                    } else if (IMReportRoute.passmicNotice.equalsIgnoreCase(route)) {
                        noticeInviteUpMicOnVideo(req_data);
                    } else if (IMReportRoute.requestNoRoomUserNotice.equalsIgnoreCase(route)) {
                        noticeInviteEnterRoom(req_data);
                    } else if (IMReportRoute.applyWaitQueueNotice.equalsIgnoreCase(route)) {
                        noticeApplyWaitQueue(req_data);
                    } else if (IMReportRoute.chatRoomMemberMute.equalsIgnoreCase(route)) {
                        noticeReceiverBanSendMsg(req_data, true);
                    } else if (IMReportRoute.chatRoomMemberMuteCancel.equalsIgnoreCase(route)) {
                        noticeReceiverBanSendMsg(req_data, false);
                    }
                    //主播同意连麦
                    else if (IMReportRoute.roomClientMicAgree.equalsIgnoreCase(route)) {
                        noticeLianMicroMsg(true, "");
                    }
                    //主播拒绝/挂断连麦
                    else if (IMReportRoute.roomClientMicReject.equalsIgnoreCase(route)) {
                        String message = "";
                        Json custom = req_data.json(IMKey.custom);
                        if (custom != null) {
                            Json data = custom.json(IMKey.data);
                            if (data != null) {
                                message = data.str(IMKey.message);
                            }
                        }
                        noticeLianMicroMsg(false, message);
                    } else if (IMReportRoute.sendUploadLogNotice.equalsIgnoreCase(route)) {
                        Json customJson = req_data.json_ok(IMKey.custom);
                        int day = customJson.json_ok(IMKey.data).num("day");
                        noticeUploadLogfile(day);
                    }
                    //有嘉宾申请连麦
                    else if (IMReportRoute.roomClientMicApply.equalsIgnoreCase(route)) {
                        noticeGuestMicroApply(parse2RoomMicroApplyInfo(req_data));
                    }
                    //嘉宾取消连麦
                    else if (IMReportRoute.roomClientMicCancel.equalsIgnoreCase(route)) {
                        noticeGuestMicroCancel(parse2RoomMicroApplyInfo(req_data));
                    } else if (IMReportRoute.roomReceiveSum.equalsIgnoreCase(route)) { //视屏房收益通知
                        noticeRoomReceiveSum(req_data);
                    } else if (IMReportRoute.ChatRoomMemberWanMesg.equalsIgnoreCase(route)) {
                        //发送警告给主播
                        noticeBroadcasterWarn(req_data);
                    } else if (IMReportRoute.ChatRoomMemberBlock.equalsIgnoreCase(route)) {
                        //直播封禁
                        noticeBroadcasterBan(req_data);
                    } else if (IMReportRoute.RoomForceOnLiveNotice.equalsIgnoreCase(route)) {
                        noticeLiveExit(req_data);
                    } else if (IMReportRoute.activationTreasureChestMessage.equalsIgnoreCase(route)) {
                        //宝箱激活
                        noticeLuckyBoxActivate();
                    } else if (IMReportRoute.LuckyTreasureChestWinning.equalsIgnoreCase(route)) {
                        //幸运提示
                        noticeLuckyTips(parse2RoomLuckyTips(req_data));
                    } else if (IMReportRoute.BarrageMessage.equalsIgnoreCase(route)) {
                        noticeBarrageMessage(req_data);
                    } else if (IMReportRoute.pushUserExperienceUpdate.equalsIgnoreCase(route)) {
                        // 用户等级更新
                        noticeUserExperienceUpdate(req_data);
                    } else if (IMReportRoute.sendPkInvite.equalsIgnoreCase(route)) {
                        // 魅力值PK邀请
                        noticeCPKInvite(req_data);
                    } else if (IMReportRoute.sendPkResult.equalsIgnoreCase(route)) {
                        // 魅力值PK结果
                        noticeCPKResult(req_data);
                    } else if (IMReportRoute.sendPkMsg.equalsIgnoreCase(route)) {
                        // 魅力值PK消息
                        noticeCPKMsg(msg, req_data);
                    } else if (IMReportRoute.routePigFightResult.equalsIgnoreCase(route)) {
                        // 猪猪大作战结果
                        noticePFResult(msg, req_data);
                    } else if (IMReportRoute.openOrClosePigFight.equalsIgnoreCase(route)) {
                        // 猪猪大作战ICON控制
                        noticePFCRT(msg, req_data);
                    } else if (IMReportRoute.LuckyTreasureChestRoomAddGold.equalsIgnoreCase(route)) {
                        // 超级宝箱追加金币全服通知
                        noticeLuckyBoxAddGold(parse2RoomLuckyBoxAddGold(req_data));
                    } else if (IMReportRoute.headlineStarLimit.equalsIgnoreCase(route)) {
                        //头条之星分数超过两万
                        noticeHeadLineStarLimit(req_data);
                    } else if (IMReportRoute.headlineStarDistance.equalsIgnoreCase(route)) {
                        //头条之星距离上一名
                        noticeHeadLineStarDistance(req_data);
                    } else if (IMReportRoute.headlineStarPopup.equalsIgnoreCase(route)) {
                        //头条之星公告
                        noticeHeadLineStarAnnounce(req_data);
                    } else if (IMReportRoute.seedMelonGoldPool.equalsIgnoreCase(route)) {
                        noticePlantCucumberGold(parse2RoomPlantCucumberInfo(req_data));
                    }
                }
            }
        }
    }

    //******************************************************** 接收各种消息的处理逻辑 **********************************************//


    /**
     * 种瓜金币池
     *
     * @param roomPlantCucumberInfo
     */
    private void noticePlantCucumberGold(RoomPlantCucumberInfo roomPlantCucumberInfo) {
        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.ROOM_PLANT_CUCUMBER)
                        .setRoomPlantCucumberInfo(roomPlantCucumberInfo));
    }

    /**
     * 头条之星公告
     */
    private void noticeHeadLineStarAnnounce(Json req_data) {
        Json data = parse2Data(req_data);
        if (data == null) {
            return;
        }
        HeadLineStarAnnounce announce = new Gson().fromJson(data.toString(), new TypeToken<HeadLineStarAnnounce>() {
        }.getType());
        if (announce == null) {
            return;
        }
        getChatRoomEventObservable()
                .onNext(new RoomEvent().setEvent(RoomEvent.HEAD_LINE_STAR_ANNOUNCE).setHeadLineStarAnnounce(announce));
    }

    /**
     * 头条之星距离上一名
     */
    private void noticeHeadLineStarDistance(Json req_data) {
        Json data = parse2Data(req_data);
        if (data == null) {
            return;
        }
        HeadLineStarDistance distance = new Gson().fromJson(data.toString(), new TypeToken<HeadLineStarDistance>() {
        }.getType());
        if (distance == null) {
            return;
        }
        getChatRoomEventObservable()
                .onNext(new RoomEvent().setEvent(RoomEvent.HEAD_LINE_STAR_DISTANCE).setHeadLineStarDistance(distance));
    }

    /**
     * 头条之星分数超过两万
     */
    private void noticeHeadLineStarLimit(Json req_data) {
        Json data = parse2Data(req_data);
        if (data == null) {
            return;
        }
        HeadLineStarLimit limit = new Gson().fromJson(data.toString(), new TypeToken<HeadLineStarLimit>() {
        }.getType());
        if (limit == null) {
            return;
        }
        getChatRoomEventObservable()
                .onNext(new RoomEvent().setEvent(RoomEvent.HEAD_LINE_STAR_LIMIT).setHeadLineStarLimit(limit));
    }

    /**
     * 主播追加金币
     *
     * @param roomLuckyBoxAddGold
     */
    private void noticeLuckyBoxAddGold(RoomLuckyBoxAddGold roomLuckyBoxAddGold) {
        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.RECEIVE_ADD_COIN)
                        .setRoomLuckyBoxAddGold(roomLuckyBoxAddGold));
    }

    /**
     * 幸运礼物
     *
     * @param msg
     */
    private void noticeLuckyGiftAddCoin(ChatRoomMessage msg) {
        IMCustomAttachment attachment = msg.getAttachment();
        if (attachment instanceof GiftAttachment) {
            GiftInfo giftInfo = ((GiftAttachment) attachment).getGiftRecieveInfo().getGiftInfo();
            if (giftInfo != null && giftInfo.getGiftType() == 6) {
                getChatRoomEventObservable()
                        .onNext(new RoomEvent()
                                .setEvent(RoomEvent.ROOM_LUCKY_GIFT));
            }
        }
    }

    /**
     * 猪猪大作战结果
     *
     * @param req_data
     */
    private void noticePFCRT(ChatRoomMessage msg, Json req_data) {
        Json data = parse2Data(req_data);
        if (data == null) {
            return;
        }
        PigFightCrtBean crtBean = new Gson().fromJson(data.toString(), new TypeToken<PigFightCrtBean>() {
        }.getType());
        if (crtBean == null) {
            return;
        }
        getChatRoomEventObservable()
                .onNext(new RoomEvent().setEvent(RoomEvent.ROOM_PF_CTR).setPigFightCrtBean(crtBean));
    }

    /**
     * 猪猪大作战结果
     *
     * @param req_data
     */
    private void noticePFResult(ChatRoomMessage msg, Json req_data) {
        Json data = parse2Data(req_data);
        if (data == null) {
            return;
        }
        PigFightBean pigFightBean = new Gson().fromJson(data.toString(), new TypeToken<PigFightBean>() {
        }.getType());
        if (pigFightBean == null) {
            return;
        }
        getChatRoomEventObservable()
                .onNext(new RoomEvent().setEvent(RoomEvent.ROOM_PF_RESULT).setPigFightBean(pigFightBean));
    }

    /**
     * 魅力值pk消息
     *
     * @param req_data 数据
     */
    private void noticeCPKMsg(ChatRoomMessage msg, Json req_data) {
        Json data = parse2Data(req_data);
        if (data == null) {
            return;
        }
        CPKMsgBean cpkMsgBean = new Gson().fromJson(data.toString(), new TypeToken<CPKMsgBean>() {
        }.getType());
        if (cpkMsgBean == null) {
            return;
        }

        if (cpkMsgBean.getType() == CPKMsgBean.CPKMSG_TYPE_NORMAL) {// 普通文本
            if (cpkMsgBean.getMessage() == null) {
                return;
            }
            msg.setContent(cpkMsgBean.getMessage());
            addMessages(msg);
        } else if (cpkMsgBean.getType() == CPKMsgBean.CPKMSG_TYPE_START) {// 比赛开始
            if (cpkMsgBean.getMessage() != null) {
                msg.setContent(cpkMsgBean.getMessage());
                addMessages(msg);
            }

            getChatRoomEventObservable()
                    .onNext(new RoomEvent().setEvent(RoomEvent.ROOM_CPK_MSG).setCpkMsgBean(cpkMsgBean));
        }
    }

    /**
     * 魅力值pk结果
     *
     * @param req_data 数据
     */
    private void noticeCPKResult(Json req_data) {
        Json data = parse2Data(req_data);
        if (data == null) {
            return;
        }
        Json dataBean = data.json("pkResult");
        if (dataBean == null) {
            return;
        }

        CPKResultBean cpkResultBean = new Gson().fromJson(dataBean.toString(), new TypeToken<CPKResultBean>() {
        }.getType());
        if (cpkResultBean == null) {
            return;
        }
        getChatRoomEventObservable()
                .onNext(new RoomEvent().setEvent(RoomEvent.ROOM_CPK_RESULT).setCPKResultBean(cpkResultBean));
    }

    /**
     * 魅力值pk邀请
     *
     * @param req_data 数据
     */
    private void noticeCPKInvite(Json req_data) {
        Json data = parse2Data(req_data);
        if (data == null) {
            return;
        }
        CPKInviteBean cpkInviteBean = new Gson().fromJson(data.toString(), new TypeToken<CPKInviteBean>() {
        }.getType());
        if (cpkInviteBean == null) {
            return;
        }
        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.ROOM_CPK_INVITE)
                        .setCpkInviteBean(cpkInviteBean));
    }

    /**
     * notice全服中奖
     *
     * @param roomLuckyRewardInfo
     */
    private void noticeLuckyReward(RoomLuckyRewardInfo roomLuckyRewardInfo, int event) {
        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(event)
                        .setLuckyReward(roomLuckyRewardInfo));
    }

    /**
     * notice幸运提示
     *
     * @param roomLuckyTips
     */
    private void noticeLuckyTips(RoomLuckyTips roomLuckyTips) {
        getChatRoomEventObservable()
                .onNext(new RoomEvent().setEvent(RoomEvent.CODE_LUCKY_TIPS)
                        .setRoomLuckyTips(roomLuckyTips));
    }

    /**
     * notice宝箱激活
     */
    private void noticeLuckyBoxActivate() {
        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.CODE_LUCKY_BOX_ACTIVATE));
    }

    /**
     * 用户等级更新
     */
    private void noticeUserExperienceUpdate(Json req_data) {
        Json customJson = req_data.json_ok(IMKey.custom);
        Json data = null;
        if (customJson != null) {
            data = customJson.json(IMKey.data);
        }

        UserExperienceUpdateInfo userExperienceUpdateInfo = new UserExperienceUpdateInfo();

        userExperienceUpdateInfo.setVideoRoomLevelSeq(data.num("videoRoomLevelSeq"));
        userExperienceUpdateInfo.setVideoRoomLevelPic(data.str("videoRoomLevelPic"));
        userExperienceUpdateInfo.setExperienceLevelSeq(data.num("experienceLevelSeq"));
        userExperienceUpdateInfo.setExperienceLevelPic(data.str("experienceLevelPic"));
        userExperienceUpdateInfo.setCharmLevelSeq(data.num("charmLevelSeq"));
        userExperienceUpdateInfo.setCharmLevelPic(data.str("charmLevelPic"));

        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.CODE_USER_EXP_UPDATE)
                        .setUserExperienceUpdateInfo(userExperienceUpdateInfo)
                );
    }

    private void noticeBarrageMessage(Json req_data) {
        Gson gson = new Gson();
        IMChatRoomMember member = null;
        try {
            member = gson.fromJson(req_data.json(IMKey.member).toString(), IMChatRoomMember.class);
            getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.CODE_BARRAGE_MESSAGE)
                    .setChatRoomMember(member).setBarrageMsg(req_data.str("barrageMsg")));
        } catch (Exception e) {

        }

    }


    /**
     * notice 主播强退
     *
     * @param req_data
     */
    private void noticeLiveExit(Json req_data) {
        Json custom = req_data.json(IMKey.custom);
        Json data = null;
        if (custom != null) {
            data = custom.json(IMKey.data);
        }

        if (data != null) {
            String clientType = data.str("clientType");
            String roomId = data.str("roomId");
            getChatRoomEventObservable()
                    .onNext(new RoomEvent()
                            .setEvent(RoomEvent.CODE_FORCE_ON_LIVE)
                            .setRoomId(roomId)
                            .setClientType(clientType)
                    );
        } else {
            LogUtil.i(AvRoomDataManager.TAG, "data == null");
        }
    }

    /**
     * notice主播警告
     *
     * @param req_data
     */
    private void noticeBroadcasterWarn(Json req_data) {
        String reason_msg = req_data.str("reason_msg");
        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.CODE_BROADCASTER_WARN)
                        .setReason_msg(reason_msg));
    }

    /**
     * notice主播封禁
     *
     * @param req_data
     */
    private void noticeBroadcasterBan(Json req_data) {
        String account = req_data.str("uid");
        String roomId = req_data.str("room_id");
        String reason_msg = req_data.str("reason_msg");
        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.CODE_BROADCASTER_BAN)
                        .setAccount(account)
                        .setRoomId(roomId)
                        .setReason_msg(reason_msg));
    }

    /**
     * 解析房间种瓜金币池
     *
     * @param json
     * @return
     */
    private RoomPlantCucumberInfo parse2RoomPlantCucumberInfo(Json json) {
        RoomPlantCucumberInfo roomPlantCucumberInfo = null;
        Json data = parse2Data(json);
        if (data != null) {
            try {
                roomPlantCucumberInfo = new Gson().fromJson(data.toString(), RoomPlantCucumberInfo.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return roomPlantCucumberInfo;
    }

    /**
     * 解析全服中奖奖励提示信息
     *
     * @param json
     * @return
     */
    private RoomLuckyRewardInfo parse2RoomLuckyRewardInfo(Json json) {
        RoomLuckyRewardInfo roomLuckyRewardInfo = null;
        Json data = parse2Data(json);
        if (data != null) {
            try {
                roomLuckyRewardInfo = new Gson().fromJson(data.toString(), RoomLuckyRewardInfo.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return roomLuckyRewardInfo;
    }


    /**
     * 解析超级宝箱追加金币
     *
     * @param req_data
     * @return
     */
    private RoomLuckyBoxAddGold parse2RoomLuckyBoxAddGold(Json req_data) {
        RoomLuckyBoxAddGold roomLuckyBoxAddGold = null;
        Json data = parse2Data(req_data);
        if (data != null) {
            try {
                roomLuckyBoxAddGold = new Gson().fromJson(data.toString(), RoomLuckyBoxAddGold.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return roomLuckyBoxAddGold;
    }

    /**
     * 解析幸运提示
     *
     * @param req_data
     * @return
     */
    private RoomLuckyTips parse2RoomLuckyTips(Json req_data) {
        RoomLuckyTips roomLuckyTips = null;
        Json data = parse2Data(req_data);
        if (data != null) {
            try {
                roomLuckyTips = new Gson().fromJson(data.toString(), RoomLuckyTips.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return roomLuckyTips;
    }

    /**
     * 解析连麦申请列表相关信息
     *
     * @param json
     * @return
     */
    private RoomMicroApplyInfo parse2RoomMicroApplyInfo(Json json) {
        RoomMicroApplyInfo roomMicroApplyInfo = null;
        Json data = parse2Data(json);
        if (data != null) {
            try {
                roomMicroApplyInfo = new Gson().fromJson(data.toString(), RoomMicroApplyInfo.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return roomMicroApplyInfo;
    }

    private Json parse2Data(Json json) {
        Json custom = parse2Custom(json);
        if (custom != null) {
            return custom.json(IMKey.data);
        }
        return null;
    }

    private Json parse2Custom(Json json) {
        return json.json(IMKey.custom);
    }


    /**
     * 解析消息相关的房间成员信息：发送者
     *
     * @param req_data
     * @param msg
     */
    public void parseCustomMember(Json req_data, ChatRoomMessage msg) {
        Gson gson = new Gson();
        IMChatRoomMember member = null;
        try {
            member = gson.fromJson(req_data.json(IMKey.member).toString(), IMChatRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member != null) {
            msg.setImChatRoomMember(member);
        }
    }

    /**
     * 处理麦序相关的消息接收逻辑
     *
     * @param IMCustomAttachment
     * @param msg
     */
    private void dealWithMicQueueMessage(IMCustomAttachment IMCustomAttachment, ChatRoomMessage msg) {
        RoomQueueMsgAttachment queueMsgAttachment = (RoomQueueMsgAttachment) IMCustomAttachment;
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        if (IMCustomAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE) {
            //邀請上麥
            if (Objects.equals(queueMsgAttachment.uid, String.valueOf(uid))) {
                noticeInviteUpMic(queueMsgAttachment.micPosition, queueMsgAttachment.uid);
            }
        } else if (IMCustomAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK) {
            //踢他下麥
            if (Objects.equals(queueMsgAttachment.uid, String.valueOf(uid))) {
                int micPosition = AvRoomDataManager.get().getMicPosition(uid);
                noticeKickDownMic(micPosition);
            }
            AvRoomDataManager.get().removeMicRoomCharmInfo(queueMsgAttachment.uid);
        } else if (IMCustomAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_CLOSE
                || IMCustomAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_REMOVE_MSG_FILTER_OPEN
                || IMCustomAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_OPEN
                || IMCustomAttachment.getSecond() == CUSTOM_MSG_HEADER_TYPE_SUB_GIFT_EFFECT_CLOSE) {
            addMessages(msg);
        }
    }

    /**
     * 处理排麦相关消息
     *
     * @param msg
     */
    private void dealWithMicInList(ChatRoomMessage msg) {
        MicInListAttachment micInListAttachment = (MicInListAttachment) msg.getAttachment();
        String params = micInListAttachment.getParams();
        String key = Json.parse(params).str("key");
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (!TextUtils.isEmpty(key) && mMicQueueMemberMap != null) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(Integer.parseInt(key));
            if (roomQueueInfo != null)
                if (!roomQueueInfo.mRoomMicInfo.isMicLock())
                    micInListToUpMic(key);
        }
    }

    /**
     * 处理幸运礼物返币消息
     *
     * @param msg
     */
    private void dealWithLuckyGift(ChatRoomMessage msg) {
        LuckyGiftAttachment attachment = (LuckyGiftAttachment) msg.getAttachment();
        if (attachment == null) return;
        if (attachment.getSecond() == CUSTOM_MSG_SECOND_ALL_LuckyGift) {
            addMessages(msg);
            noticeReceiverLuckyGiftAll(msg);
        } else {
            noticeReceiverLuckyGift(msg);
        }

    }

    private void dealWithPublicMessage(ChatRoomMessage msg) {
        RoomHtmlTextAttachment attachment = (RoomHtmlTextAttachment) msg.getAttachment();
        if (attachment == null) return;
        if (attachment.getSecond() == CUSTOM_HTML_SECOND_PUB_ON) {
            noticePublicUpdate(1);
        } else if (attachment.getSecond() == CUSTOM_HTML_SECOND_PUB_OFF) {
            noticePublicUpdate(0);
        }
    }

    /**
     * 处理红包消息
     *
     * @param msg
     */
    private void dealWithRedPackMessage(ChatRoomMessage msg) {
        PublicChatRoomAttachment redPackageAttachment = (PublicChatRoomAttachment) msg.getAttachment();

        if (redPackageAttachment != null) {
            RoomRedPackageInfo roomRedPackageInfo = new RoomRedPackageInfo();
            roomRedPackageInfo.setNick(redPackageAttachment.getNick());
            roomRedPackageInfo.setRedPackId(redPackageAttachment.getRedPackId());
            noticeReceiverRedPacket(roomRedPackageInfo);
        }
    }

    /**
     * 处理文本消息
     *
     * @param msg
     * @param req_data
     */
    private void dealWithTxtMessage(ChatRoomMessage msg, Json req_data) {
        Gson gson = new Gson();
        IMChatRoomMember member = null;
        try {
            member = gson.fromJson(req_data.json(IMKey.member).toString(), IMChatRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member == null) {
            return;
        }
        msg.setImChatRoomMember(member);
        msg.setContent(req_data.str(IMKey.content));
        addMessages(msg);
    }

    /**
     * 成员进入房间
     */
    private void chatRoomMemberIn(ChatRoomMessage msg, Json req_data) {
        Gson gson = new Gson();
        IMChatRoomMember member = null;
        try {
            member = gson.fromJson(req_data.json(IMKey.member).toString(), IMChatRoomMember.class);
            if (member != null) {
                int online_num = req_data.num("online_num");
                long timestamp = req_data.num_l("timestamp", 0);
                member.setOnline_num(online_num);
                member.setTimestamp(timestamp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member == null) return;
        msg.setImChatRoomMember(member);
        if (AvRoomDataManager.get().isOwner(member.getAccount())) {
            AvRoomDataManager.get().setMOwnerMember(member);
        }
        addMessages(msg);

        RoomMemberComeInfo memberComeInfo = new RoomMemberComeInfo();
        if (msg.getImChatRoomMember() != null) {
            memberComeInfo.setNickName(member.getNick());
            memberComeInfo.setExperLevel(member.getExperLevel());
            memberComeInfo.setCarName(member.getCar_name());
            memberComeInfo.setCarImgUrl(member.getCar_url());
            memberComeInfo.setVideoRoomExperLevel(member.getVideo_room_exper_level());
            memberComeInfo.setVideoRoomExperLevelPic(member.getVideo_room_exper_level_pic());
        }
        //加到处理队列（因为有可能fragment还没初始化，还不能show，所以加到队列，由fragment处理后再remove）
        AvRoomDataManager.get().addMemberComeInfo(memberComeInfo);

        noticeRoomMemberChange(true, member.getAccount(), member);
    }

    /**
     * 增加管理员
     *
     * @param message
     */
    private void addManagerMember(final ChatRoomMessage message, Json req_data) {
        Gson gson = new Gson();
        IMChatRoomMember member = null;
        try {
            member = gson.fromJson(req_data.json(IMKey.member).toString(), IMChatRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member != null) {
            message.setImChatRoomMember(member);
            AvRoomDataManager.get().addAdminMember(member);
            if (AvRoomDataManager.get().isOwner(member.getAccount())) {
                AvRoomDataManager.get().setMOwnerMember(member);
            }
            // 放在这里的原因是,只有管理员身份改变了才能发送通知
            noticeManagerChange(true, member.getAccount());
        }
    }

    /**
     * 处理移除管理员消息
     *
     * @param req_data
     */
    private void dealWithRemoveManager(Json req_data) {
        Gson gson = new Gson();
        IMChatRoomMember member = null;
        try {
            member = gson.fromJson(req_data.json(IMKey.member).toString(), IMChatRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member != null) {
            removeManagerMember(member.getAccount());
        }
    }

    /**
     * 移除管理员
     */
    private void removeManagerMember(String account) {
        if (AvRoomDataManager.get().removeManagerMember(account)) {
            noticeManagerChange(false, account);
        }
    }

    /**
     * 加入黑名单
     *
     * @param req_data
     */
    private void addBlackList(Json req_data) {
        Gson gson = new Gson();
        IMChatRoomMember member = null;
        try {
            member = gson.fromJson(req_data.json(IMKey.member).toString(), IMChatRoomMember.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (member != null && !AvRoomDataManager.get().isOwner(member.getAccount())) {
            noticeChatMemberBlackAdd(member.getAccount());
        }

    }

    /**
     * 退出房间处理
     */
    private void chatRoomMemberExit(Json req_data) {
        String account = req_data.str("uid");
        if (StringUtils.isNotEmpty(account)) {
            //用于判断退出数量
            IMChatRoomMember chatRoomMember = new IMChatRoomMember();
            chatRoomMember.setAccount(account);
            int online_num = req_data.num("online_num");
            long timestamp = req_data.num_l("timestamp", 0);
            chatRoomMember.setOnline_num(online_num);
            chatRoomMember.setTimestamp(timestamp);
            noticeRoomMemberChange(false, account, chatRoomMember);

            RoomInfo mCurrentRoomInfo = AvRoomDataManager.get().getRoomInfo();
            if (mCurrentRoomInfo == null) return;
            if (AvRoomDataManager.get().isOnMic(Long.valueOf(account))) {
                //在麦上的要退出麦
                LogUtil.i("nim_sdk", "chatRoomMemberExit     " + account);
                downMicroPhoneBySdk(AvRoomDataManager.get().getMicPosition(Long.valueOf(account)), null);
                SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
                int size = mMicQueueMemberMap.size();
                for (int i = 0; i < size; i++) {
                    RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                    if (roomQueueInfo.mChatRoomMember != null
                            && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), account)) {
                        roomQueueInfo.mChatRoomMember = null;
                        LogUtil.i("remove  mChatRoomMember", 3 + "");
                        noticeDownMic(String.valueOf(mMicQueueMemberMap.keyAt(i)), account);
                        break;
                    }
                }
            }

            AvRoomDataManager.get().removeMicRoomCharmInfo(account);
        }
    }

    /**
     * 踢出房间操作
     *
     * @param req_data
     */
    private void onKickedOutRoom(Json req_data) {
        String account = req_data.str("uid");
        if (AvRoomDataManager.get().isOwner(account)) {//是踢自己
            int reason_no = req_data.num("reason_no");
            String reason_msg = req_data.str("reason_msg");
            String roomId = req_data.str("room_id");
            // 提示被踢出的原因（聊天室已解散、被管理员踢出、被其他端踢出等
            noticeKickOutChatMember(reason_no, reason_msg, account, roomId);


            ReUsedSocketManager.get().exitRoom(Long.valueOf(roomId), new IMProCallBack() {
                @Override
                public void onSuccessPro(IMReportBean imReportBean) {
                }

                @Override
                public void onError(int errorCode, String errorMsg) {
                }
            });

            // 清空缓存数据 注意：因为是被踢出，若是语聊房在最小化的情况下view已经失效是没有办法接收踢出事件的
            // 只能在这里做数据清理
            AvRoomDataManager.get().release();
        }
    }

    /**
     * 踢出账号
     *
     * @param req_data
     */
    private void onKickOffLogin(Json req_data) {
        int reason_no = req_data.num("errno");
        String reason_msg = req_data.str("errmsg");
        // 提示被踢出的原因（聊天室已解散、被管理员踢出、被其他端踢出等
        noticeKickOutChatMember(reason_no, reason_msg, CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        // 清空缓存数据
        AvRoomDataManager.get().release();
        CoreManager.getCore(IAuthCore.class).logout();
        ReUsedSocketManager.get().destroy();
        PreferencesUtils.setFristQQ(true);
    }

    /**
     * 通知邀请上麦（视频房）
     */
    private void noticeInviteUpMicOnVideo(Json req_data) {
        try {
            IMChatRoomMember member = JsonParser.parseJsonObject(req_data.json(IMKey.member).toString(), IMChatRoomMember.class);
            IMChatRoomMember mother = JsonParser.parseJsonObject(req_data.json(IMKey.mother).toString(), IMChatRoomMember.class);
            int needGold = req_data.num(IMKey.need_gold);
            int position = req_data.num(IMKey.position);
            VideoRoomMessage message = new VideoRoomMessage();
            message.setNeedGold(needGold);
            message.setMotherMember(mother);
            message.setImChatRoomMember(member);
            message.setPosition(position);

            getChatRoomEventObservable().onNext(new RoomEvent()
                    .setEvent(RoomEvent.CODE_INVITE_MIC_ON_VIDEO_ROOM).setChatRoomMessage(message)
                    .setAccount(member.getAccount()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 通知邀请进房
     */
    private void noticeInviteEnterRoom(Json req_data) {
        try {
            IMChatRoomMember mother = JsonParser.parseJsonObject(req_data.json(IMKey.mother).toString(), IMChatRoomMember.class);
            long roomUid = req_data.num_l(IMKey.roomUid);
            int roomType = req_data.num(IMKey.type);
            VideoRoomMessage message = new VideoRoomMessage();
            message.setRoomUid(roomUid);
            message.setMotherMember(mother);
            message.setType(roomType);

            getChatRoomEventObservable().onNext(new RoomEvent()
                    .setEvent(RoomEvent.CODE_INVITE_ENTER_ROOM).setChatRoomMessage(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 通知有人申请上麦
     */
    private void noticeApplyWaitQueue(Json req_data) {
        try {
            WaitQueuePersonInfo waitQueuePersonInfo = new WaitQueuePersonInfo(req_data.num("manWaitQueueNum"), req_data.num("womenWaitQueueNum"), req_data.num("totalManNum"), req_data.num("totalWomenNum"));
            long roomUid = req_data.num_l(IMKey.roomUid);
            int roomType = req_data.num(IMKey.type);
            VideoRoomMessage message = new VideoRoomMessage();
            message.setRoomUid(roomUid);
            message.setWaitQueuePersonInfo(waitQueuePersonInfo);
            message.setType(roomType);
            getChatRoomEventObservable().onNext(new RoomEvent()
                    .setEvent(RoomEvent.CODE_APPLY_WAIT_QUEUE_NOTICE).setChatRoomMessage(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 队列麦坑状态更新
     *
     * @param req_data
     */
    private void roomQueueMicUpdate(Json req_data) {
        String micInfo = req_data.str("mic_info");
        LogUtil.i(AvRoomDataManager.TAG, "roomQueueMicUpdate ---> position 坑位 " +
                "posState ---> 0：开锁，1：闭锁" +
                "micStat --->  0：开麦，1：闭麦" +
                "\n micInfo = " + micInfo);
        if (!TextUtils.isEmpty(micInfo)) {
            Gson gson = new Gson();
            RoomMicInfo roomMicInfo = gson.fromJson(micInfo, RoomMicInfo.class);
            if (roomMicInfo != null) {
                int position = roomMicInfo.getPosition();
                RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(position);
                if (roomQueueInfo != null) {
                    //解锁麦位
                    roomQueueInfo.mRoomMicInfo = roomMicInfo;
                    //处理声网声音相关的
                    if (roomQueueInfo.mChatRoomMember != null) {
                        if (AvRoomDataManager.get().isOwner(roomQueueInfo.mChatRoomMember.getAccount()) && AvRoomDataManager.get().getRoomInfo() != null) {
                            RtcEngineManager.get().setRole(AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, roomQueueInfo.mRoomMicInfo.isMicMute() ? Constants.CLIENT_ROLE_AUDIENCE : Constants.CLIENT_ROLE_BROADCASTER);
                        }
                    }
                    noticeMicPosStateChange(position, roomQueueInfo);
                }
            }
        }
    }


    /**
     * 房间信息更新
     *
     * @param req_data -
     */
    private void chatRoomInfoUpdate(Json req_data) {
        Gson gson = new Gson();
        RoomInfo roomInfo = null;
        try {
            roomInfo = gson.fromJson(req_data.json("room_info").toString(), RoomInfo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (roomInfo != null) {
            AvRoomDataManager.get().setRoomInfo(roomInfo);
            LogUtil.i("RoomSettingActivity2", AvRoomDataManager.get().getRoomInfo() + "");
            noticeRoomInfoUpdate(roomInfo);
        }
    }

    /**
     * 麦序更新
     *
     * @param req_data
     */
    private void chatRoomQueueChangeNotice(Json req_data) {
        if (req_data == null)
            return;
        //排麦的key是用uid，所以length大于2
        int key = req_data.num("key");
        //麦位类型 0 语音、 1视屏（视频连麦房有语音连麦和视频连麦）
        int micType = req_data.num("connect_mic_type");
        boolean isMicInList = (key >= 10);
        Json content = req_data.json("value");
        switch (req_data.num("type")) {
            case 1:
                LogUtil.i(AvRoomDataManager.TAG, "chatRoomQueueChangeNotice ---> type(1：更新key 2：删除) = " + 1);
                if (isMicInList) {
                    addMicInList(key, content);
                } else {
                    upMicroQueue(content, key, micType);
                }
                break;
            case 2:
                LogUtil.i(AvRoomDataManager.TAG, "chatRoomQueueChangeNotice ---> type(1：更新key 2：删除) = " + 2);
                if (isMicInList) {
                    removeMicInList(key + "");
                } else {
                    downMicroQueue(key + "");
                }
                break;
            default:
        }
    }

    /**
     * 上麦
     *
     * @param content --
     */
    private synchronized void upMicroQueue(Json content, final int micPosition, final int micType) {
        final SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (content != null) {
            Gson gson = new Gson();
            IMChatRoomMember chatRoomMember = null;
            try {
                LogUtil.i(AvRoomDataManager.TAG, "upMicroQueue ---> content" + content.toString());
                chatRoomMember = gson.fromJson(content.toString(), IMChatRoomMember.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (chatRoomMember == null) {
                return;
            }
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(micPosition);
            if (roomQueueInfo == null) {
                return;
            }
//            IMChatRoomMember chatRoomMember = parseChatRoomMember(content, roomQueueInfo);
            int size = mMicQueueMemberMap.size();
            if (size > 0) {
                for (int j = 0; j < size; j++) {
                    RoomQueueInfo temp = mMicQueueMemberMap.valueAt(j);
                    if (temp.mChatRoomMember != null
                            && Objects.equals(temp.mChatRoomMember.getAccount(), chatRoomMember.getAccount())) {
                        //处理同一个人换坑问题
                        temp.mChatRoomMember = null;
                    }
                }
            }
            RoomQueueInfo tempRoomQueueInfo = mMicQueueMemberMap.get(micPosition);
            if (tempRoomQueueInfo != null && tempRoomQueueInfo.mChatRoomMember != null
                    && !Objects.equals(tempRoomQueueInfo.mChatRoomMember.getAccount(), chatRoomMember.getAccount())) {
                //被挤下麦的情况
                noticeDownCrowdedMic(micPosition, tempRoomQueueInfo.mChatRoomMember.getAccount());
            }
            roomQueueInfo.mChatRoomMember = chatRoomMember;
            //连麦类型
            roomQueueInfo.setOnMicType(micType);
            //重新更新队列，队列上是否还有自己
            if (!AvRoomDataManager.get().isOwnerOnMic()) {
                if (AvRoomDataManager.get().getRoomInfo() != null) {
                    //处理可能自己被挤下还能说话的情况
                    RtcEngineManager.get().setRole(AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, Constants.CLIENT_ROLE_AUDIENCE);
                }
            }
            if (AvRoomDataManager.get().isOwner(chatRoomMember.getAccount())) {
                if (!roomQueueInfo.mRoomMicInfo.isMicMute()) {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        //开麦
                        RtcEngineManager.get().setRole(AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, Constants.CLIENT_ROLE_BROADCASTER);
                        if (micType == 0) {
                            //语音连麦禁止视频流
                            RtcEngineManager.get().muteLocalVideoStream(true);
                        } else if (micType == 1) {
                            //视频连麦开启视频流
                            RtcEngineManager.get().muteLocalVideoStream(false);
                        }
                    }
                    //不需要开麦的话闭麦
                    if (!AvRoomDataManager.get().mIsNeedOpenMic) {
                        //闭麦
                        if (AvRoomDataManager.get().getRoomInfo() != null) {
                            RtcEngineManager.get().setMute(AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, true);
                        }
                        AvRoomDataManager.get().mIsNeedOpenMic = true;
                    }
                } else {
                    if (AvRoomDataManager.get().getRoomInfo() != null) {
                        RtcEngineManager.get().setRole(AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, Constants.CLIENT_ROLE_AUDIENCE);
                    }
                }
            }
            micInListToDownMic(chatRoomMember.getAccount());
            noticeUpMic(micPosition, chatRoomMember.getAccount());
        } else {
            if (AvRoomDataManager.get().getRoomInfo() != null) {
                RtcEngineManager.get().setRole(AvRoomDataManager.get().getRoomInfo().getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, Constants.CLIENT_ROLE_AUDIENCE);
            }
        }
    }

    /**
     * 下麦
     *
     * @param key --
     */
    private void downMicroQueue(String key) {
        LogUtil.i(AvRoomDataManager.TAG, "downMicroQueue ---> key(麦位) = " + key);
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        if (!TextUtils.isEmpty(key) && mMicQueueMemberMap != null) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.get(Integer.parseInt(key));
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                if (AvRoomDataManager.get().isOwner(account)) {
                    RoomInfo currRoomInfo = AvRoomDataManager.get().getRoomInfo();
                    if (currRoomInfo != null && !(currRoomInfo.getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE && AvRoomDataManager.get().isRoomOwner())) {
                        // 更新声网闭麦 ,发送状态信息
                        RtcEngineManager.get().setRole(currRoomInfo.getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, Constants.CLIENT_ROLE_AUDIENCE);
                    }
//                    RtcEngineManager.get().setMute(false);
                    AvRoomDataManager.get().mIsNeedOpenMic = true;
                }
                AvRoomDataManager.get().removeMicRoomCharmInfo(account);
                roomQueueInfo.mChatRoomMember = null;
                // 通知界面更新麦序信息
                noticeDownMic(key, account);
                //排麦
                if (!roomQueueInfo.mRoomMicInfo.isMicLock())
                    micInListToUpMic(key);
            }
        }
    }


    //******************************************************** 消息发送方法 ******************************************************//

    /**
     * 进入房间的提醒消息
     *
     * @return
     */
    private ChatRoomMessage getFirstMessageContent() {
        String content = "系统通知：平台倡导绿色交友，内容凡涉及黄赌毒政等将被封号处理，此外，请用户警惕诈骗行为，避免损失！";
        ChatRoomMessage message = new ChatRoomMessage();
        message.setRoute(IMReportRoute.ChatRoomTip);
        message.setContent(content);
        return message;
    }

    /**
     * 发送房间规则消息
     */
    public void sendRoomRulesMessage() {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (AvRoomDataManager.get().isSendRoomRule() || roomInfo == null || StringUtils.isEmpty(roomInfo.getPlayInfo()) || roomInfo.getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE) {// 视频房不需要进房提示
            return;
        }
        RoomRuleAttachment rules = new RoomRuleAttachment(IMCustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST, IMCustomAttachment.CUSTOM_MSG_TYPE_RULE_FIRST);
        rules.setRule(roomInfo.getPlayInfo());
        ChatRoomMessage message = new ChatRoomMessage();
        message.setRoute(IMReportRoute.sendMessageReport);
        message.setRoom_id(roomInfo.getRoomId() + "");
        message.setAttachment(rules);
        messages.add(message);
        noticeReceiverMessageImmediately(message);
        // 已经发送过进房规则的公告
        AvRoomDataManager.get().setSendRoomRule(true);
    }

    /**
     * 发送文本信息
     *
     * @param message -
     */
    public void sendTextMsg(long roomId, String message, final CallBack<String> callBack) {
        if (TextUtils.isEmpty(message) || TextUtils.isEmpty(message.trim()))
            return;
        final ChatRoomMessage chatRoomMessage = new ChatRoomMessage(String.valueOf(roomId), message);
        chatRoomMessage.setRoute(IMReportRoute.sendTextReport);
        chatRoomMessage.setImChatRoomMember(getCurrentChatMember());
        ReUsedSocketManager.get().sendTxtMessage(roomId + "", chatRoomMessage, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {
                if (callBack != null) {
                    callBack.onSuccess(data);
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (callBack != null) {
                    callBack.onFail(errorCode, errorMsg);
                }
            }
        });
    }

    /**
     * 注意: 这个消息是app本地发送的
     * <p>
     * 发送公屏上的Tip信息
     * 子协议一: 关注房主提示- IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_SHARE_ROOM
     * 子协议二: 分享房间成功的提示- IMCustomAttachment.CUSTOM_MSG_SECOND_ROOM_TIP_ATTENTION_ROOM_OWNER
     *
     * @param targetUid -
     * @param subType   -发送公屏上Tip信息的子协议
     */
    public void sendTipMsg(long targetUid, int subType) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        UserInfo targetUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(targetUid);
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        UserInfo myUserInfo = CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(myUid);
        if (roomInfo != null && targetUserInfo != null && myUserInfo != null) {
            RoomTipAttachment roomTipAttachment = new RoomTipAttachment(
                    CUSTOM_MSG_FIRST_ROOM_TIP,
                    subType
            );
            roomTipAttachment.setUid(myUid);
            roomTipAttachment.setNick(myUserInfo.getNick());
            roomTipAttachment.setTargetUid(targetUid);
            roomTipAttachment.setTargetNick(targetUserInfo.getNick());

            ChatRoomMessage chatRoomMessage = new ChatRoomMessage();
            chatRoomMessage.setRoom_id(roomInfo.getRoomId() + "");
            chatRoomMessage.setAttachment(roomTipAttachment);
            chatRoomMessage.setImChatRoomMember(getCurrentChatMember());
            ReUsedSocketManager.get().sendCustomMessage(roomInfo.getRoomId() + "", chatRoomMessage, new IMSendCallBack() {
                @Override
                public void onSuccess(String data) {

                }

                @Override
                public void onError(int errorCode, String errorMsg) {

                }
            });
        }
    }

    /**
     * 注意: 这个消息是app本地发送的
     *
     * @param key 密钥
     */
    private void sendMicInListNimMsg(final String key) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        MicInListAttachment micInListAttachment = new MicInListAttachment(CUSTOM_MSG_MIC_IN_LIST, CUSTOM_MSG_MIC_IN_LIST);
        Json json = new Json();
        json.set("key", key);
        micInListAttachment.setParams(json + "");
        final ChatRoomMessage message = new ChatRoomMessage();
        message.setRoom_id(roomInfo.getRoomId() + "");
        message.setAttachment(micInListAttachment);
        ReUsedSocketManager.get().sendCustomMessage(roomInfo.getRoomId() + "", message, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        micInListToUpMic(key);
                    }
                }, 300);
            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    /**
     * 注意: 这个消息是app本地发送的
     * <p>
     * 邀请上麦的自定义消息
     *
     * @param micUid   上麦用户uid
     * @param position 要上麦的位置
     */
    public void inviteMicroPhoneBySdk(final long micUid, final int position) {
        final RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        //上麦条件:游客不在麦上 或者是管理员，房主
        if ((!AvRoomDataManager.get().isOnMic(micUid)
                || AvRoomDataManager.get().isRoomOwner()
                || AvRoomDataManager.get().isRoomAdmin())
                && position != Integer.MIN_VALUE) {
            RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(CUSTOM_MSG_HEADER_TYPE_QUEUE,
                    CUSTOM_MSG_HEADER_TYPE_QUEUE_INVITE);
            queueMsgAttachment.uid = String.valueOf(micUid);
            queueMsgAttachment.micPosition = position;
            ChatRoomMessage message = new ChatRoomMessage();
            message.setRoom_id(roomInfo.getRoomId() + "");
            message.setAttachment(queueMsgAttachment);
            ReUsedSocketManager.get().sendCustomMessage(roomInfo.getRoomId() + "", message, new IMSendCallBack() {
                @Override
                public void onSuccess(String data) {

                }

                @Override
                public void onError(int errorCode, String errorMsg) {

                }
            });
        }
    }

    /**
     * 注意: 这个消息是app本地发送的
     * <p>
     * 踢人下麦
     *
     * @param micUid 被踢用户uid
     * @param roomId 房间ID
     */
    public void kickMicroPhoneBySdk(int micPosition, long micUid, long roomId) {
        IMNetEaseManager.get().downMicroPhoneBySdk(micPosition, null);
        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(CUSTOM_MSG_HEADER_TYPE_QUEUE,
                CUSTOM_MSG_HEADER_TYPE_QUEUE_KICK);
        queueMsgAttachment.uid = String.valueOf(micUid);
        ChatRoomMessage message = new ChatRoomMessage();
        message.setRoom_id(String.valueOf(roomId));
        message.setAttachment(queueMsgAttachment);
        ReUsedSocketManager.get().sendCustomMessage(roomId + "", message, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {

            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }


    /**
     * 注意: 这个消息是app本地发送的
     * <p>
     * 发送开启和关闭屏蔽小礼物特效消息和屏蔽公屏消息
     *
     * @param uid
     * @param second
     * @param micIndex
     */
    public void systemNotificationBySdk(long uid, int second, int micIndex) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();

        if (roomInfo == null) {
            return;
        }
        long roomId = roomInfo.getRoomId();
        RoomQueueMsgAttachment queueMsgAttachment = new RoomQueueMsgAttachment(CUSTOM_MSG_HEADER_TYPE_QUEUE,
                second);
        queueMsgAttachment.uid = String.valueOf(uid);
        if (micIndex != -1)
            queueMsgAttachment.micPosition = micIndex;
        final ChatRoomMessage message = new ChatRoomMessage();
        message.setRoom_id(String.valueOf(roomId));
        message.setAttachment(queueMsgAttachment);
        ReUsedSocketManager.get().sendCustomMessage(String.valueOf(roomId), message, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {
//                addMessagesImmediately(message);
            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    /**
     * 注意: 这个消息是app本地发送的
     * <p>
     * 发送Pk消息
     *
     * @param second
     * @param pkVoteInfo
     */
    public void sendPkNotificationBySdk(int first, int second, PkVoteInfo pkVoteInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo == null) {
            return;
        }
        String roomId = roomInfo.getRoomId() + "";
        PkCustomAttachment pkCustomAttachment = new PkCustomAttachment(first, second);
        pkCustomAttachment.setPkVoteInfo(pkVoteInfo);
        ChatRoomMessage message = new ChatRoomMessage();
        message.setRoom_id(String.valueOf(roomId));
        message.setAttachment(pkCustomAttachment);
        ReUsedSocketManager.get().sendCustomMessage(roomId, message, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {

            }

            @Override
            public void onError(int errorCode, String errorMsg) {

            }
        });
    }

    public void addMicInList(int key, Json content) {
//        if (TextUtils.isEmpty(key) || TextUtils.isEmpty(content))
//            return;
//        int keyInt = Integer.parseInt(key);
//        JsonParser jsonParser = new JsonParser();
//        JsonObject valueJsonObj = jsonParser.parse(content).getAsJsonObject();
//        if (valueJsonObj == null)
//            return;
//        SparseArray<Json> mMicInListMap = AvRoomDataManager.get().mMicInListMap;
//        Json json = mMicInListMap.get(keyInt);
//        if (json == null) {
//            json = new Json();
//        }
//        Set<String> strings = valueJsonObj.keySet();
//
//
//        for (String jsonKey : strings) {
//            json.set(jsonKey, valueJsonObj.get(jsonKey).getAsString());
//        }
//
//
//        AvRoomDataManager.get().addMicInListInfo(key, json);
//        noticeMicInList();
    }

    private void removeMicInList(String key) {
        AvRoomDataManager.get().removeMicListInfo(key);
        noticeMicInList();

    }

    /**
     * 进入聊天室
     */
    public void joinAvRoom() {
        LogUtil.i("joinAvRoom");
        RoomInfo curRoomInfo = AvRoomDataManager.get().getRoomInfo();
        AvRoomDataManager.get().setStartPlayFull(true);
        noticeEnterMessages(curRoomInfo);
        handler.removeMessages(0);
        handler.sendEmptyMessageDelayed(0, 60000);
    }

    public void addMessagesImmediately(ChatRoomMessage msg) {
        if (messages.size() == 0) {
            ChatRoomMessage firstMessageContent = getFirstMessageContent();
            messages.add(firstMessageContent);
            noticeReceiverMessageImmediately(firstMessageContent);
            sendRoomRulesMessage();
        }
        messages.add(msg);
        limitMessageMaxCount();
        noticeReceiverMessageImmediately(msg);
    }

    // 添加消息进入消息队列
    private void addMessages(ChatRoomMessage msg) {
        if (messages.size() == 0) {
            ChatRoomMessage firstMessageContent = getFirstMessageContent();
            messages.add(firstMessageContent);
            noticeReceiverMessage(firstMessageContent);
            sendRoomRulesMessage();
        }
        messages.add(msg);
        limitMessageMaxCount();
        noticeReceiverMessage(msg);
    }

    /**
     * 限制本地保存的最大消息数
     */
    private void limitMessageMaxCount() {
        if (messages.size() > MESSAGE_COUNT_LOCAL_LIMIT) {
            messages.remove(0);
        }
    }

    private PublishProcessor<RoomEvent> roomProcessor;
    private PublishSubject<ChatRoomMessage> msgProcessor;

    private PublishProcessor<LogEvent> logProcessor;

    @Deprecated
    /**
     * this method has been replaced by
     * {@code com.tongdaxing.xchat_core.manager.IMNetEaseManager.subscribeChatRoomMsgFlowable(Consumer<List<ChatRoomMessage>> chatMsg,IDisposableAddListener iDisposableAddListener)}
     */
    public Observable<List<ChatRoomMessage>> getChatRoomMsgFlowable() {
        return getChatRoomMsgPublisher().toFlowable(BackpressureStrategy.BUFFER)
                .toObservable().buffer(200, TimeUnit.MILLISECONDS, 20)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());


    }

    public void subscribeChatRoomMsgFlowable(boolean isImm, final Consumer<List<ChatRoomMessage>> chatMsg, IDisposableAddListener iDisposableAddListener) {
        Disposable disposable;
        if (isImm) {
            disposable = getChatRoomMsgPublisher().subscribe(new Consumer<ChatRoomMessage>() {
                @Override
                public void accept(ChatRoomMessage chatRoomMessage) throws Exception {
                    List<ChatRoomMessage> chatRoomMessages = new ArrayList<>();
                    chatRoomMessages.add(chatRoomMessage);
                    chatMsg.accept(chatRoomMessages);
                }
            });
        } else {
            disposable = getChatRoomMsgFlowable().subscribe(chatMsg);
        }
        if (iDisposableAddListener != null) {
            iDisposableAddListener.addDisposable(disposable);
        }

    }

//    public void subscribeChatRoomMsgFlowable(final Consumer<List<ChatRoomMessage>> chatMsg, IDisposableAddListener iDisposableAddListener) {
//        subscribeChatRoomMsgFlowable(false, chatMsg, iDisposableAddListener);
//    }

    private PublishSubject<ChatRoomMessage> getChatRoomMsgPublisher() {
        if (msgProcessor == null) {
            synchronized (IMNetEaseManager.class) {
                if (msgProcessor == null) {
                    msgProcessor = PublishSubject.create();
                    msgProcessor.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread());
                }
            }
        }
        return msgProcessor;
    }

    @Deprecated
    /**
     * this method has been replaced by
     * {@code com.tongdaxing.xchat_core.manager.IMNetEaseManager.subscribeChatRoomEventObservable(Consumer<RoomEvent> roomEvent,IDisposableAddListener iDisposableAddListener)}
     */
    public PublishProcessor<RoomEvent> getChatRoomEventObservable() {
        if (roomProcessor == null) {
            synchronized (IMNetEaseManager.class) {
                if (roomProcessor == null) {
                    roomProcessor = PublishProcessor.create();
                    roomProcessor.subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread());
                }
            }
        }
        return roomProcessor;
    }

    public void subscribeChatRoomEventObservable(Consumer<RoomEvent> roomEvent, IDisposableAddListener iDisposableAddListener) {
        Disposable disposable = getChatRoomEventObservable().subscribe(roomEvent);
        if (iDisposableAddListener != null)
            iDisposableAddListener.addDisposable(disposable);
    }

    public PublishProcessor<LogEvent> getLogEventObservable() {
        if (logProcessor == null) {
            synchronized (IMNetEaseManager.class) {
                if (logProcessor == null) {
                    logProcessor = PublishProcessor.create();
                    logProcessor.subscribeOn(AndroidSchedulers.mainThread())
                            .observeOn(AndroidSchedulers.mainThread());
                }
            }
        }
        return logProcessor;
    }

    public void subscribeLogEventObservable(Consumer<LogEvent> logEvent, IDisposableAddListener iDisposableAddListener) {
        Disposable disposable = getLogEventObservable().subscribe(logEvent);
        if (iDisposableAddListener != null)
            iDisposableAddListener.addDisposable(disposable);
    }


    public void clear() {
        messages.clear();
        resetBeforeDisConnectionMuteStatus();
        handler.removeCallbacksAndMessages(null);
//        Log.e(IMNetEaseManager.class.getSimpleName(), "清除房间消息....");
    }

    private MessageHandler handler = new MessageHandler();


    /**
     * 加入房间黑名单
     *
     * @param account 聊天室id
     * @param is_add  1添加，0移除
     */
    public void addRoomBlackList(String account, boolean is_add, HttpRequestCallBack myCallBack) {
        Map<String, String> params = getImDefaultParamsMap();
        if (params == null)
            return;
        params.put("account", account);
        params.put("is_add", is_add ? "1" : "0");
        OkHttpManager.getInstance().doPostRequest(UriProvider.addRoomBlackList(), params, myCallBack);
    }

    public void addRoomBlackList(String room_id, String account, boolean is_add, boolean synPersonBlackList, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = getImDefaultParamsMap();
        if (params == null)
            return;
        params.put("room_id", room_id);
        params.put("account", account);
        params.put("is_add", is_add ? "1" : "0");
        params.put("synPersonBlackList", synPersonBlackList ? "1" : "0");
        OkHttpManager.getInstance().doPostRequest(UriProvider.addRoomBlackList(), params, myCallBack);
    }

    /**
     * 加入私聊黑名单
     *
     * @param tgUid
     * @param callBack
     */
    public void addUserBlackList(String tgUid, HttpRequestCallBack<Json> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("tgUid", tgUid);
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("synRoomBlackList", "1");
        OkHttpManager.getInstance().doPostRequest(UriProvider.addUserBlackList(), params, callBack);
    }

    /**
     * 移除黑名单
     */
    public void removeUserBlackList(long tgUid, HttpRequestCallBack<Json> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("tgUid", tgUid + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.removeUserBlacklist(), params, callBack);
    }


    /**
     * 检查互黑关系
     *
     * @param tgUid
     * @param callBack
     */
    public void checkUserBlacklist(long tgUid, HttpRequestCallBack<Json> callBack) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("tgUid", tgUid + "");
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        params.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        OkHttpManager.getInstance().getRequest(UriProvider.getCheckBlacklist(), params, callBack);
    }


    public Map<String, String> getImDefaultParamsMap() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo == null) {
            return null;
        }
        IAuthCore iAuthCore = CoreManager.getCore(IAuthCore.class);
        long currentUid = iAuthCore.getCurrentUid();
        String ticket = iAuthCore.getTicket();
        long roomId = roomInfo.getRoomId();
        params.put("room_id", roomId + "");
        params.put("uid", currentUid + "");
        params.put("ticket", ticket);
        return params;
    }


    /**
     * 踢出房间
     *
     * @param account    被踢出用户uid
     * @param myCallBack
     */
    public void kickMember(String account, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = getImDefaultParamsMap();
        if (params == null)
            return;
        params.put("account", account);
        OkHttpManager.getInstance().doPostRequest(UriProvider.kickMember(), params, myCallBack);
    }

    private static final int WHAT_CHECK_ROOM_CONNECT_STATE = 1; //检查房间状态

    private static class MessageHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    IMNetEaseManager.get().sendStatistics();
                    sendEmptyMessageDelayed(0, 60000);
                    break;
                case WHAT_CHECK_ROOM_CONNECT_STATE:
                    if (!IMNetEaseManager.get().isImRoomConnection()) {
                        IMNetEaseManager.get().getChatRoomEventObservable().onNext(new RoomEvent()
                                .setEvent(RoomEvent.CODE_DISCONNECT_TIME_OUT));
                    }
                    break;
            }


        }

    }

    /************************云信聊天室 普通操作(每个人都可以使用的) start******************************/
    private void micInListToUpMic(final String key) {
        LogUtil.i("micInListToUpMic", "key:" + key);

        //房主不不能上
        if ("-1".equals(key))
            return;
        LogUtil.i("micInListToUpMic_!=-1", "key:" + key);
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo == null)
            return;
        //那之前更新一次队列
        final Json json = AvRoomDataManager.get().getMicInListTopInfo();
        if (json == null)
            return;

        final String micInListTopKey = json.str("uid");

        LogUtil.i("micInListToUpMic", micInListTopKey);

        checkMicInListUpMicSuccess(micInListTopKey, roomInfo.getRoomId(), key);

        if (!micInListTopKey.equals(CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo().getUid() + "")) {
            return;
        }

        remveMicInlistOrUpMic(key, roomInfo, micInListTopKey);
    }

    private void remveMicInlistOrUpMic(final String key, RoomInfo roomInfo, final String micInListTopKey) {
        removeMicInList(micInListTopKey, roomInfo.getRoomId() + "", new RequestCallback() {
            @Override
            public void onSuccess(Object param) {
                //移除成功报上麦,判断是否自己,如果是自动上麦

                LogUtil.i("micInListToUpMic", 1 + "");
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onMicInListToUpMic, Integer.parseInt(key), micInListTopKey);
                CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.micInListDismiss, "");

            }

            @Override
            public void onFailed(int code) {
                LogUtil.i("micInListLogUpMic", 2 + "code:" + code);
            }

            @Override
            public void onException(Throwable exception) {
                LogUtil.i("micInListLogUpMic", 3 + "");
            }
        });
    }

    private void checkMicInListUpMicSuccess(final String micInListTopKey, final long roomId, final String key) {
        String account = getFirstMicUid();

        long currentUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        boolean isKicker = (currentUid + "").equals(account);
        //如果是首个在麦上的人
        if (isKicker) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Json json = AvRoomDataManager.get().getMicInListTopInfo();
                    if (json == null)
                        return;

                    String topKey = json.str("uid", "null");
                    //1.5秒后判断排麦，首位跟换没有
                    if (micInListTopKey.equals(topKey)) {

//                        remveMicInlistOrUpMic(key, roomInfo, micInListTopKey);
                        IMNetEaseManager.get().removeMicInList(micInListTopKey, roomId + "", new RequestCallback() {
                            @Override
                            public void onSuccess(Object param) {
                                //移除成功后通知别人上麦
                                sendMicInListNimMsg(key);
                            }

                            @Override
                            public void onFailed(int code) {
                                LogUtil.i("micInListToUpMiconFailed", key);

                            }

                            @Override
                            public void onException(Throwable exception) {
                                LogUtil.i("micInListToUpMiconException", key);
                            }
                        });
                        LogUtil.i("checkMicInListUpMicSuccess", "kick");

                    }
                }
            }, 1500);
        }


    }

    private String getFirstMicUid() {
        String account = "";
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
//
//            LogUtil.i("checkMicInListUpMicSuccess", mMicQueueMemberMap.keyAt(i) + "");
            if (roomQueueInfo == null)
                continue;
            IMChatRoomMember mChatRoomMember = roomQueueInfo.mChatRoomMember;
            if (mChatRoomMember == null)
                continue;
            else {
                account = mChatRoomMember.getAccount();
                break;
            }
        }
        return account;
    }

    public void removeMicInList(String key, String roomId, RequestCallback requestCallback) {
        NIMClient.getService(ChatRoomService.class).pollQueue(roomId, key).setCallback(requestCallback);

    }

    /**
     * 检测用户是否有推流权限
     */
    public void checkPushAuth(OkHttpManager.MyCallBack callBack) {

        Map<String, String> paramsMap = getImDefaultParamsMap();
        if (paramsMap == null) {
            return;
        }
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        paramsMap.put("room_id", String.valueOf(roomInfo.getRoomId()));
        paramsMap.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        paramsMap.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        OkHttpManager.getInstance().doPostRequest(UriProvider.checkPushAuth(), paramsMap, callBack);
    }

    private void micInListToDownMic(String key) {

        if (!AvRoomDataManager.get().checkInMicInlist())
            return;
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        removeMicInList(key, roomInfo.getRoomId() + "", null);
    }

    /************************云信聊天室 普通操作 end******************************/

    /************************云信聊天室 房主/管理员操作 begin******************************/


    /**
     * 设置管理员
     *
     * @param is_add  1加，0移除
     * @param account 要设置的管理员id
     */
    public void markManager(final String account, final boolean is_add, final OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> params = getImDefaultParamsMap();
        if (params == null)
            return;
        params.put("is_add", is_add ? "1" : "0");
        params.put("account", account);
        OkHttpManager.getInstance().doPostRequest(UriProvider.markChatRoomManager(), params, myCallBack);
    }

    /**
     * 房间用户禁言接口
     *
     * @param account
     * @param isMute  1: 禁言   0: 解禁
     */
    public void markChatRoomMute(String account, int isMute, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = getImDefaultParamsMap();
        if (params == null)
            return;
        params.put("is_mute", isMute + "");
        params.put("account", account);
        OkHttpManager.getInstance().doPostRequest(UriProvider.markChatRoomMute(), params, callBack);
    }

    /**
     * 查询房间用户禁言接口
     *
     * @param search_uid
     * @param callBack
     */
    public void fetchChatRoomMuteList(long search_uid, OkHttpManager.MyCallBack callBack) {
        Map<String, String> params = getImDefaultParamsMap();
        if (params == null)
            return;
        params.put("search_uid", search_uid + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.fetchChatRoomMuteList(), params, callBack);

    }

    /**
     * 下麦
     *
     * @param micPosition -
     * @param callBack    -
     */
    public void downMicroPhoneBySdk(int micPosition, final CallBack<String> callBack) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo == null) return;

        if (micPosition < MIC_POSITION_BY_OWNER) {
            return;
        }
        //防止房主掉麦
        if (micPosition == MIC_POSITION_BY_OWNER) {
            String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            if (!AvRoomDataManager.get().isRoomOwner(currentUid)) {
                return;
            }
        }
        ReUsedSocketManager.get().pollQueue(String.valueOf(roomInfo.getRoomId()), micPosition, new IMSendCallBack() {
            @Override
            public void onSuccess(String data) {
                if (callBack != null)
                    callBack.onSuccess("下麦成功");
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (callBack != null) {
                    callBack.onFail(errorCode, "下麦失败:" + errorMsg);
                }
            }
        });
    }

    /**
     * 发送通用的当前用户信息
     *
     * @return
     */
    public IMChatRoomMember getCurrentChatMember() {
        return AvRoomDataManager.get().getMOwnerMember() == null ? new IMChatRoomMember() : AvRoomDataManager.get().getMOwnerMember();
    }

    /**
     * -------------------------通知begin-------------------------------
     */

    private void noticeRoomMemberChange(boolean isMemberIn, String account, IMChatRoomMember imChatRoomMember) {
        RoomEvent roomEvent = new RoomEvent();
        //进入房间的时候把imChatRoomMember传过去
        if (imChatRoomMember != null) {
            ChatRoomMessage chatRoomMessage = new ChatRoomMessage();
            chatRoomMessage.setImChatRoomMember(imChatRoomMember);
            roomEvent.setChatRoomMessage(chatRoomMessage);

            getChatRoomEventObservable().onNext(roomEvent
                    .setAccount(account)
                    .setEvent(isMemberIn ? RoomEvent.ROOM_MEMBER_IN : RoomEvent.ROOM_MEMBER_EXIT));
        } else {
            return;
        }
    }

    private void noticeManagerChange(boolean isAdd, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(isAdd ? RoomEvent.ROOM_MANAGER_ADD : RoomEvent.ROOM_MANAGER_REMOVE)
                .setAccount(account));
    }


    private void noticeReceiverMessageImmediately(ChatRoomMessage chatRoomMessage) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.RECEIVE_MSG)
                .setChatRoomMessage(chatRoomMessage)
        );
    }

    /**
     * 发送公屏消息开关通知
     *
     * @param publicChatSwitch 公屏消息开关
     */
    private void noticePublicUpdate(int publicChatSwitch) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ROOM_PUB_DEL).setPublicChatSwitch(publicChatSwitch));
    }

    /**
     * 发送魅力值更新通知
     *
     * @param charmAttachment
     */
    private void noticeCharmUpdate(RoomCharmAttachment charmAttachment) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ROOM_CHARM).setRoomCharmAttachment(charmAttachment));
    }

    /**
     * 禁言通知
     */
    private void noticeReceiverBanSendMsg(Json red_data, boolean isMute) {
        try {
            getChatRoomEventObservable().onNext(new RoomEvent()
                    .setEvent(RoomEvent.RECEIVE_BAN_SEND_MSG).setAccount(red_data.json_ok("member").str("account"))
                    .setBanSendMsg(isMute));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 连麦通知
     *
     * @param msg 文案
     */
    private void noticeLianMicroMsg(boolean isAgree, String msg) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.RECEIVE_LIAN_MICRO_SEND_MSG)
                .setReason_msg(msg)
                .setLianMicroAgree(isAgree));
    }

    private void noticeReceiverLuckyGift(ChatRoomMessage chatRoomMessage) {//
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.RECEIVE_LUCKY_GIFT)
                .setChatRoomMessage(chatRoomMessage));
    }

    private void noticeReceiverLuckyGiftAll(ChatRoomMessage chatRoomMessage) {//
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RECEIVE_LUCKY_GIFT_ALL)
                .setChatRoomMessage(chatRoomMessage));
    }

    private void noticeReceiverRedPacket(RoomRedPackageInfo roomRedPackageInfo) {//
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.RECEIVE_RED_PACKET)
                .setRoomRedPackageInfo(roomRedPackageInfo));
    }

    private void noticeReceiverMessage(ChatRoomMessage chatRoomMessage) {
        getChatRoomMsgPublisher().onNext(chatRoomMessage);
    }

    private void noticeEnterMessages(RoomInfo roomInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ENTER_ROOM).setRoomInfo(roomInfo));
    }

    private void noticeKickOutChatMember(int reason_no, String reason_msg, String account, String roomId) {
        LogUtil.i(AvRoomDataManager.TAG, account + ": noticeKickOutChatMember");
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.KICK_OUT_ROOM)
                .setReason_msg(reason_msg)
                .setReason_no(reason_no)
                .setAccount(account)
                .setRoomId(roomId));
    }

    public void noticeKickOutChatMember(int reason_no, String reason_msg, String account) {
        noticeKickOutChatMember(reason_no, reason_msg, account, null);
    }

    private void noticeKickDownMic(int position) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.KICK_DOWN_MIC)
                .setMicPosition(position));
    }

    private void noticeInviteUpMic(int position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.INVITE_UP_MIC)
                .setAccount(account)
                .setMicPosition(position));
    }

    public void noticeDownMic(String position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.DOWN_MIC)
                .setAccount(account)
                .setMicPosition(Integer.valueOf(position)));
    }


    private void noticeMicPosStateChange(int position, RoomQueueInfo roomQueueInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.MIC_QUEUE_STATE_CHANGE)
                .setMicPosition(position)
                .setRoomQueueInfo(roomQueueInfo));
    }


    private void noticeChatMemberBlackAdd(String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.ADD_BLACK_LIST)
                .setAccount(account));
    }


    private void noticeUpMic(int position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.UP_MIC)
                .setMicPosition(position)
                .setAccount(account)
        );
    }

    private void noticeMicInList() {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.MIC_IN_LIST_UPDATE)

        );
    }

    /**
     * 被挤下麦通知
     */
    private void noticeDownCrowdedMic(int position, String account) {
        getChatRoomEventObservable().onNext(new RoomEvent()
                .setEvent(RoomEvent.DOWN_CROWDED_MIC)
                .setMicPosition(position)
                .setAccount(account)
        );
    }

//    public void noticeEnterRoom(RoomInfo roomInfo) {
//        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ENTER_ROOM).setRoomInfo(roomInfo));
//    }

    public void noticeRoomInfoUpdate(RoomInfo roomInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ROOM_INFO_UPDATE).setRoomInfo(roomInfo));
    }

    private void noticeRoomConnect(RoomInfo roomInfo) {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.ROOM_RECONNECT).setRoomInfo(roomInfo));
    }

    private void noticeIMConnectLoginSuc() {
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.SOCKET_IM_RECONNECT_LOGIN_SUCCESS));
    }

    private void noticeUploadLogfile(int day) {
        getLogEventObservable().onNext(new LogEvent().setDay(day));
    }

    /**
     * notice有嘉宾申请连麦
     *
     * @param roomMicroApplyInfo
     */
    private void noticeGuestMicroApply(RoomMicroApplyInfo roomMicroApplyInfo) {
        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.CODE_GUEST_APPLY_MICRO)
                        .setRoomMicroApplyInfo(roomMicroApplyInfo));
    }

    /**
     * 视屏房收益通知
     *
     * @param data
     */
    private void noticeRoomReceiveSum(Json data) {
        Json customJson = data.json_ok(IMKey.custom);
        String sum = customJson.json_ok(IMKey.data).str("receiveSum");
        getChatRoomEventObservable().onNext(new RoomEvent().setEvent(RoomEvent.CODE_ROOM_RECEIVE_SUM).setReceiveSum(sum));

    }

    /**
     * notice嘉宾取消连麦
     *
     * @param roomMicroApplyInfo
     */
    private void noticeGuestMicroCancel(RoomMicroApplyInfo roomMicroApplyInfo) {
        getChatRoomEventObservable()
                .onNext(new RoomEvent()
                        .setEvent(RoomEvent.CODE_GUEST_CANCEL_MICRO)
                        .setRoomMicroApplyInfo(roomMicroApplyInfo));
    }

    private void sendStatistics() {
        RoomInfo curRoomInfo = AvRoomDataManager.get().getRoomInfo();
        if (curRoomInfo != null) {
            long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
            long roomUid = curRoomInfo.getUid();
            long roomId = curRoomInfo.getRoomId();
            String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
            Map<String, String> params = CommonParamUtil.getDefaultParam();
            params.put("uid", String.valueOf(uid));
            params.put("roomId", String.valueOf(roomId));
            params.put("roomUid", String.valueOf(roomUid));
            params.put("ticket", ticket);
            params.put("time", String.valueOf(System.currentTimeMillis()));
            StatisticManager.get().sendStatisticToService(UriProvider.roomStatistics(), params);
        }
    }

    public void resetBeforeDisConnectionMuteStatus() {
        this.beforeDisConnectionMuteStatus = MUTE_STATUS_DEFAULT;
    }

    public boolean isImRoomConnection() {
        return imRoomConnection;
    }

    public void setImRoomConnection(boolean imNetsConnection) {
        this.imRoomConnection = imNetsConnection;
    }
}