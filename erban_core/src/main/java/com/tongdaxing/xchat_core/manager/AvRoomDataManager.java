package com.tongdaxing.xchat_core.manager;

import android.graphics.Point;
import android.hardware.Camera;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.SparseArray;

import com.juxiao.library_utils.log.LogUtil;
import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomCharmInfo;
import com.tongdaxing.xchat_core.bean.RoomMemberComeInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.im.avroom.IAVRoomCoreClient;
import com.tongdaxing.xchat_core.room.bean.DiscountGiftSetting;
import com.tongdaxing.xchat_core.room.bean.HeadLineStarAnnounce;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.WaitQueuePersonInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;

import lombok.Data;

/**
 * <p> 全局变量，存储房间相关信息(周期与房间一样) </p>
 *
 * @author jiahui
 * @date 2017/12/11
 */

@Data
public final class AvRoomDataManager {
    public static final String TAG = "room_log ---> IM";
    private static volatile AvRoomDataManager mInstance;
    private static final Object SYNC_OBJECT = new Object();

    public static final int MIC_POSITION_BY_OWNER = -1;//房主的麦位
    public static final int MIC_POSITION_BY_VIDEO_GUEST = 0;//视频嘉宾（连麦者）的麦位

    public static final int MIC_TYPE_AUDIO = 0;//语音连麦
    public static final int MIC_TYPE_VIDEO = 1;//视频连麦

    public static final long MSG_SEND_MAX_LENGTH = 500;//发送文本消息的最大长度

    private int mCameraFacing = Camera.CameraInfo.CAMERA_FACING_FRONT;

    private volatile RoomInfo mCurrentRoomInfo;

    private String mCndUrl = "";

    //是否已经申请连麦
    private boolean isApplyLianMicro;

    //是否在连麦中
    private boolean isInLianMicro;

    private boolean isDialogShow;

    /**
     * 自己的实体
     */
    private IMChatRoomMember mOwnerMember;
    /**
     * 房间管理员集合
     */
    public List<IMChatRoomMember> mRoomManagerList;
    /**
     * 房间普通成员
     */
    public List<IMChatRoomMember> mRoomNormalMemberList;
    /**
     * 房间受限成员：禁言用户和黑名单用户都属于受限用户
     */
    public List<IMChatRoomMember> mRoomLimitMemberList;
    /**
     * 固定成员：包括创建者,管理员,普通用户,受限用户
     */
    public List<IMChatRoomMember> mRoomFixedMemberList;
    /**
     * 麦序位置信息：对应的位置，坑位信息（用户成员，坑位状态）
     */
    public SparseArray<RoomQueueInfo> mMicQueueMemberMap;
    /**
     * 记录每一个麦位的中心点位置
     */
    private SparseArray<Point> mMicPointMap;

    /**
     * 需要处理的 房间人员进入 消息
     */
    private ConcurrentLinkedQueue<RoomMemberComeInfo> mMemberComeMsgQueue;

    public SparseArray<Json> mMicInListMap;


    private Map<String, RoomCharmInfo> mMicCharmInfo;

    /**
     * 是否需要打开麦克风,用户自己的行为，不受房主管理员的管理
     * <p>
     * 这里应该被理解为是否主动开麦 --- 受邀上麦时默认不开麦 其他是开启的
     */
    public boolean mIsNeedOpenMic = true;

    private String room_rule = "";
    //是否开始播放全服
    private boolean isStartPlayFull = false;

    private long timestamp = 0;//房间人员进出时间戳

    //当前等待队列上麦的信息
    private WaitQueuePersonInfo waitQueuePersonInfo;

    //连麦类型
    private int mMicroType = MIC_TYPE_AUDIO;

    private long charmTimestamps = 0;
    private boolean hasCharm = false;//魅力值开关

    private int pigFight = 0;// 猪猪大作战ICON


    private boolean sendNewHandMsg = false;
    private long enterTimeStamp = 0;// 进入房间时的时间戳
    private boolean growingHasSend = false;// 礼物发送间隔
    private HeadLineStarAnnounce announce;// 头条之星的数据
    private DiscountGiftSetting discountGiftSetting;// 特惠礼物消息
    private boolean sendRoomRule;
    private boolean hasDelay = false;

    private AvRoomDataManager() {
        mRoomManagerList = new ArrayList<>();
        mRoomFixedMemberList = new ArrayList<>();
        mMicQueueMemberMap = new SparseArray<>();
        mRoomNormalMemberList = new ArrayList<>();
        mRoomLimitMemberList = new ArrayList<>();
        mMicInListMap = new SparseArray<>();
        mMemberComeMsgQueue = new ConcurrentLinkedQueue<>();
        mMicCharmInfo = new HashMap<>();
        isApplyLianMicro = false;
    }

    public static AvRoomDataManager get() {
        if (mInstance == null) {
            synchronized (SYNC_OBJECT) {
                if (mInstance == null) {
                    mInstance = new AvRoomDataManager();
                }
            }
        }
        return mInstance;
    }

    /**
     * 获取对应麦位的麦位信息
     *
     * @param micPosition 麦的位置
     * @return 对应队列信息
     */
    public RoomQueueInfo getRoomQueueMemberInfoByMicPosition(int micPosition) {
        if (micPosition >= mMicQueueMemberMap.size()) {
            return null;
        }
        return mMicQueueMemberMap.get(micPosition);
    }

    /**
     * 获取麦上的第一个异性（非房主）
     */
    public IMChatRoomMember getFirstOppositeSexOnMic() {
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null && mMicQueueMemberMap != null && mMicQueueMemberMap.size() > 1) {//这里要注意，过滤掉房主
            int oppositeSexGender = userInfo.getGender() == 1 ? 2 : 1;//异性性别
            for (int i = 1; i < mMicQueueMemberMap.size(); i++) {//这里要注意，过滤掉房主
                RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo.mChatRoomMember != null &&
                        Objects.equals(roomQueueInfo.mChatRoomMember.getGender(), oppositeSexGender)) {
                    return roomQueueInfo.mChatRoomMember;
                }
            }
        }
        return null;
    }

    /**
     * 获取自己在麦上队列信息
     *
     * @return 对应队列信息
     */
    public RoomQueueInfo getRoomQueueMemberInfoMyself() {
        return getRoomQueueMemberInfoByAccount(CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
    }

    /**
     * 获取对应account的麦上队列信息
     *
     * @param account 用户的id
     * @return 对应队列信息
     */
    public RoomQueueInfo getRoomQueueMemberInfoByAccount(String account) {
        if (TextUtils.isEmpty(account)) return null;
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo.mChatRoomMember != null &&
                    Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), account)) {
                return roomQueueInfo;
            }
        }
        return null;
    }

    /**
     * 获取麦上队列（不包含房主）
     *
     * @return 对应队列信息
     */
    public ArrayList<RoomQueueInfo> getRoomQueueMemberInfos() {
        ArrayList<RoomQueueInfo> roomQueueInfos = new ArrayList<>();
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if(roomQueueInfo != null
                    && roomQueueInfo.mChatRoomMember != null
                    && roomQueueInfo.mRoomMicInfo != null
                    && roomQueueInfo.mRoomMicInfo.getPosition() != MIC_POSITION_BY_OWNER){
                roomQueueInfos.add(roomQueueInfo);
            } else {
                LogUtil.i("onMic check fail");
            }
        }
        return roomQueueInfos;
    }

    public void addAdminMember(IMChatRoomMember chatRoomMember) {
        if (chatRoomMember == null || containsAdminMember(chatRoomMember.getAccount())) return;
        mRoomManagerList.add(chatRoomMember);
    }

    public boolean containsAdminMember(String uid) {
        for (IMChatRoomMember chatRoomMember : mRoomManagerList) {
            if (Objects.equals(chatRoomMember.getAccount(), String.valueOf(uid))) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断该用户是第一次进来还是切换房间
     *
     * @param roomUid 要进入房间的用户id
     */
    public boolean isFirstEnterRoomOrChangeOtherRoom(long roomUid, int roomType) {
        return mCurrentRoomInfo == null || mCurrentRoomInfo.getUid() != roomUid || mCurrentRoomInfo.getType() != roomType;
    }

    /***
     * 是否是房间创建者
     * @param currentUid
     * @return
     */
    public boolean isRoomOwner(String currentUid) {
        return mCurrentRoomInfo != null && Objects.equals(String.valueOf(mCurrentRoomInfo.getUid()), currentUid);
    }

    /***
     * 是否是房间创建者
     * @param currentUid
     * @return
     */
    public boolean isRoomOwner(long currentUid) {
        return mCurrentRoomInfo != null && mCurrentRoomInfo.getUid() == currentUid;
    }

    /**
     * 是否是自己
     */
    public boolean isOwner(String currentUid) {
        return Objects.equals(currentUid, String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
    }

    /**
     * 是否是自己
     */
    public boolean isOwner(long currentUid) {
        return currentUid == CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }

    /***
     * 是否是房间创建者
     */
    public boolean isRoomOwner() {
        return mCurrentRoomInfo != null && mCurrentRoomInfo.getUid() == CoreManager.getCore(IAuthCore.class).getCurrentUid();
    }

    public boolean isCurrRoom(String roomId){
        return mCurrentRoomInfo != null && mCurrentRoomInfo.getRoomId() == Long.valueOf(roomId);
    }

    /**
     * @return 是否做了移除
     */
    public boolean removeManagerMember(String account) {
        boolean isRemoved = false;
        if (ListUtils.isListEmpty(mRoomManagerList) || TextUtils.isEmpty(account)) return isRemoved;
        ListIterator<IMChatRoomMember> iterator = mRoomManagerList.listIterator();
        for (; iterator.hasNext(); ) {
            IMChatRoomMember chatRoomMember = iterator.next();
            if (Objects.equals(chatRoomMember.getAccount(), account)) {
                iterator.remove();
                isRemoved = true;
                break;
            }
        }
        if (AvRoomDataManager.get().isOwner(account) && mOwnerMember != null) {
            //自己是管理员被移除，恢复身份
            mOwnerMember.setMemberType(MemberType.NORMAL);
        }
        return isRemoved;
    }

    public boolean isGuess() {
        return !isRoomAdmin() && !isRoomOwner();
    }

    public boolean isGuess(String account) {
        return !isRoomAdmin(account) && !isRoomOwner(account);
    }

    /**
     * 是否是房间管理员
     *
     * @return -
     */
    public boolean isRoomAdmin() {
        return isRoomAdmin(String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
    }

    public boolean isRoomAdmin(String account) {
        if (ListUtils.isListEmpty(mRoomManagerList)) return false;
        for (IMChatRoomMember chatRoomMember : mRoomManagerList) {
            if (Objects.equals(chatRoomMember.getAccount(), account)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断指定用户id是否在麦上
     *
     * @param myUid 用户id
     * @return -
     */
    public boolean isOnMic(long myUid) {
        return isOnMic(String.valueOf(myUid));
    }

    /**
     * 判断指定用户id是否在麦上
     *
     * @param myUid 用户id
     * @return -
     */
    public boolean isOnMic(String myUid) {
        int size = mMicQueueMemberMap.size();
        for (int i = 0; i < size; i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null
                    && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), myUid)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断自己是否在麦上
     *
     * @return -
     */
    public boolean isOwnerOnMic() {
        return isOnMic(CoreManager.getCore(IAuthCore.class).getCurrentUid());
    }

    /**
     * 根据用户id去获取当前用户在麦上的位置
     */
    public int getMicPosition(long uid) {
        int size = mMicQueueMemberMap.size();
        for (int i = 0; i < size; i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null
                    && Objects.equals(roomQueueInfo.mChatRoomMember.getAccount(), String.valueOf(uid))) {
                return mMicQueueMemberMap.keyAt(i);
            }
        }
        //判断是否房主
        if (isRoomOwner(uid)) {
            return MIC_POSITION_BY_OWNER;
        }
        return Integer.MIN_VALUE;
    }

    /**
     * 即构是已streamID表示用户唯一信息
     *
     * @param streamID 流的格式  s-初始化时传入的用户uid-时间戳
     */
    public int getMicPositionByStreamID(String streamID) {
        if (StringUtils.isEmpty(streamID)) {
            return Integer.MIN_VALUE;
        }
        for (int index = 0; index < mMicQueueMemberMap.size(); index++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(index);
            if (roomQueueInfo != null && roomQueueInfo.mChatRoomMember != null
                    && streamID.contains(roomQueueInfo.mChatRoomMember.getAccount())) {
                return mMicQueueMemberMap.keyAt(index);
            }
        }
        //判断是否房主
        if (isRoomOwner(streamID)) {
            return MIC_POSITION_BY_OWNER;
        }
        return Integer.MIN_VALUE;
    }

    /**
     * 根据用户id去获取当前用户在麦上的位置
     */
    public int getMicPosition(String uid) {
        return getMicPosition(Long.valueOf(uid));
    }

    /**
     * 获取房主的用户信息
     */
    public UserInfo getRoomOwnerUserInfo() {
        if (mCurrentRoomInfo != null) {
            return CoreManager.getCore(IUserCore.class).getCacheUserInfoByUid(mCurrentRoomInfo.getUid(), true);
        } else {
            return null;
        }
    }

    /**
     * 获取坑上没人的位置
     */
    public int findFreePosition() {
        int size;
        if (mMicQueueMemberMap != null && (size = mMicQueueMemberMap.size()) > 0) {
            for (int i = 0; i < size; i++) {
                int key = mMicQueueMemberMap.keyAt(i);
                RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
                if (roomQueueInfo.mChatRoomMember == null) {
                    if (key == MIC_POSITION_BY_OWNER)
                        continue;
                    return key;
                }
            }
        }
        return Integer.MIN_VALUE;
    }

    //防止最小化时候这时候的进场消息是不能进入队列的
    private boolean isMinimize = false;//是否最小化 true 最小化

    public void addMemberComeInfo(RoomMemberComeInfo memberComeInfo) {
        if (!isMinimize) {
            mMemberComeMsgQueue.offer(memberComeInfo);
        }
    }

    public RoomMemberComeInfo getAndRemoveFirstMemberComeInfo() {
        return mMemberComeMsgQueue.poll();
    }

    public int getMemberComeSize() {
        if (mMemberComeMsgQueue == null) {
            return 0;
        }
        return mMemberComeMsgQueue.size();
    }


    public void removeMicListInfo(String key) {
        if (TextUtils.isEmpty(key)) return;
        Integer keyInt = Integer.valueOf(key);

        removeMicListInfo(keyInt);
    }

    public void removeMicListInfo(int key) {

        mMicInListMap.remove(key);
        CoreManager.notifyClients(IAVRoomCoreClient.class, IAVRoomCoreClient.onMicInListChange);
    }

    public boolean checkInMicInlist() {
        UserInfo cacheLoginUserInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (cacheLoginUserInfo == null)
            return false;
        long uid = cacheLoginUserInfo.getUid();

        if (mMicInListMap.get((int) uid) != null)
            return true;

        return false;

    }

    /**
     * 房主是视频房主播并且已经开播，禁止进入他人房间
     */
    public boolean checkVideoRoomStart() {
        if (mCurrentRoomInfo != null) {
            return isRoomOwner() && mCurrentRoomInfo.getType() == RoomInfo.ROOMTYPE_VIDEO_LIVE;
        }
        return false;
    }

    private TreeSet<Json> treeSet = new TreeSet<Json>(new Comparator<Json>() {
        @Override
        public int compare(Json o1, Json o2) {
            String time1 = o1.str("time");
            String time2 = o2.str("time");
            long l1 = Long.parseLong(time1);
            long l2 = Long.parseLong(time2);
            if (l1 > l2) {
                return 1;
            } else {
                return -1;
            }


        }
    });

    public Json getMicInListTopInfo() {

        if (mMicInListMap.size() < 1)
            return null;

        treeSet.clear();
        for (int i = 0; i < mMicInListMap.size(); i++) {
            treeSet.add(mMicInListMap.valueAt(i));
            LogUtil.i("micInListLog", "key:" + mMicInListMap.keyAt(i) + "   value:" + mMicInListMap.valueAt(i));
        }

        if (treeSet.size() > 0)
            return treeSet.first();
        return null;

    }


    public final static int MIC_FULL = -2;

    /**
     * 找到空的麦位
     *
     * @return 返回-2的话代表满了
     */
    public int findEmptyMic() {
        if (mMicQueueMemberMap == null) {
            return MIC_FULL;
        }

        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            int i1 = mMicQueueMemberMap.keyAt(i);
            if (roomQueueInfo.mChatRoomMember == null && i1 != MIC_POSITION_BY_OWNER && !roomQueueInfo.mRoomMicInfo.isMicLock()) {
                return i1;
            }

        }
        return MIC_FULL;
    }


    public void release() {
        IMNetEaseManager.get().setImRoomConnection(false);
        IMNetEaseManager.get().resetBeforeDisConnectionMuteStatus();
        RtcEngineManager.get().leaveChannel();
        clear();
    }

    private void clear() {
        clearMembers();

        isDialogShow = false;
        hasDelay = false;
        discountGiftSetting = null;
        announce = null;
        isInLianMicro = false;
        isApplyLianMicro = false;
        mMicroType = MIC_TYPE_AUDIO;
        if (mCurrentRoomInfo != null) {// 清空房间信息
            mCurrentRoomInfo = null;
        }
        sendNewHandMsg = false;
        hasCharm = false;
        charmTimestamps = 0;
        enterTimeStamp = 0;
        growingHasSend = false;
        mMicCharmInfo.clear();

        isMinimize = false;
        timestamp = 0;
        mMicInListMap.clear();
        mMicQueueMemberMap.clear();

        IMNetEaseManager.get().clear();
    }

    /**
     * 清除房间成员信息
     */
    private void clearMembers() {
        if (mOwnerMember != null) {
            mOwnerMember = null;
        }

        if (ListUtils.isNotEmpty(mRoomFixedMemberList)) {
            mRoomFixedMemberList.clear();
        }

        if (ListUtils.isNotEmpty(mRoomManagerList)) {
            mRoomManagerList.clear();
        }

        if (ListUtils.isNotEmpty(mRoomLimitMemberList)) {
            mRoomLimitMemberList.clear();
        }
        if (ListUtils.isNotEmpty(mRoomNormalMemberList)) {
            mRoomNormalMemberList.clear();
        }
        if (!mMemberComeMsgQueue.isEmpty()) {
            mMemberComeMsgQueue.clear();
        }
    }

    /**
     * 添加和更新魅力值列表
     *
     * @param updateInfo
     */
    public void addMicRoomCharmInfo(Map<String, RoomCharmInfo> updateInfo) {
        if (mMicCharmInfo == null) {
            mMicCharmInfo = new HashMap<>();
        }
        if (updateInfo != null && updateInfo.size() > 0) {
            for (Map.Entry<String, RoomCharmInfo> m : updateInfo.entrySet()) {
                mMicCharmInfo.put(m.getKey(), m.getValue());
            }
        }
    }

    /**
     * 添加和更新魅力值列表
     *
     * @param account
     */
    public void removeMicRoomCharmInfo(String account) {
        if (mMicCharmInfo == null) {
            return;
        }
        if (mMicCharmInfo.size() > 0) {
            mMicCharmInfo.put(account, new RoomCharmInfo(0, false));
        }
    }


    public void setRoom_rule(String room_rule) {
        this.room_rule = room_rule;
    }

    public String getRoom_rule() {
        return room_rule;
    }

    public void setMinimize(boolean minimize) {
        isMinimize = minimize;
    }

    public boolean isMinimize() {
        return isMinimize;
    }

    public void setStartPlayFull(boolean startPlayFull) {
        isStartPlayFull = startPlayFull;
    }

    public boolean isStartPlayFull() {
        return isStartPlayFull;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isApplyLianMicro() {
        return isApplyLianMicro;
    }

    public void setInLianMicro(boolean isInLianMicro) {
        this.isInLianMicro = isInLianMicro;
    }

    public boolean isInLianMicro() {
        return isInLianMicro;
    }

    public void setApplyLianMicro(boolean applyLianMicro) {
        isApplyLianMicro = applyLianMicro;
    }

    public SparseArray<Point> getMicPoint() {
        return mMicPointMap;
    }

    public void setMicPoint(SparseArray<Point> mMicPointMap) {
        this.mMicPointMap = mMicPointMap;
    }

    public int getMicroType() {
        return mMicroType;
    }

    public void setMicroType(int microType) {
        mMicroType = microType;
    }

    public void setCndUrl(String url) {
        mCndUrl = url;
    }

    public String getCndUrl() {
        return mCndUrl;
    }

    /**
     * 根据麦位position获取microView的position
     */
    public static int getMicroViewPositionByMicPosition(int micPosition) {
        return micPosition + 1;//麦位从-1开始
    }

    /**
     * 根据microView的position获取麦位position
     */
    public static int getMicPositionByMicroViewPosition(int microViewPosition) {
        return microViewPosition - 1;//麦位从-1开始
    }

    public @NonNull
    WaitQueuePersonInfo getWaitQueuePersonInfo() {
        if (waitQueuePersonInfo == null) {
            waitQueuePersonInfo = new WaitQueuePersonInfo(0, 0, 0, 0);
        }
        return waitQueuePersonInfo;
    }

    public void setWaitQueuePersonInfo(WaitQueuePersonInfo waitQueuePersonInfo) {
        this.waitQueuePersonInfo = waitQueuePersonInfo;
    }

    /**
     * 当前麦上是否有人（排除房主）
     */
    public boolean isHaveMemberOnMicro() {
        if (mMicQueueMemberMap != null && mMicQueueMemberMap.size() > 1) {//注意这里排除房主
            for (int i = 1; i < mMicQueueMemberMap.size(); i++) {//注意这里排除房主
                RoomQueueInfo queueInfo = mMicQueueMemberMap.valueAt(i);
                if (queueInfo != null && queueInfo.mChatRoomMember != null) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取房主默认信息
     */
    public IMChatRoomMember getRoomOwnerDefaultMemberInfo() {
        IMChatRoomMember member = null;
        if (AvRoomDataManager.get().getRoomInfo() != null) {
            member = new IMChatRoomMember();
            member.setAccount(String.valueOf(AvRoomDataManager.get().getRoomInfo().getUid()));
            member.setNick("");
            member.setAvatar("");
        }
        return member;
    }

    /**
     * 连麦开关是否开启
     *
     * @return
     */
    public boolean getIsCanConnectMic() {
        if (mCurrentRoomInfo == null) {
            return false;
        }
        return mCurrentRoomInfo.getIsCanConnectMic() == 1;
    }

    public RoomInfo getRoomInfo() {
        return mCurrentRoomInfo;
    }

    public void setRoomInfo(RoomInfo mCurrentRoomInfo) {
        this.mCurrentRoomInfo = mCurrentRoomInfo;
    }

    public int getCameraFacing() {
        return mCameraFacing;
    }

    public void setCameraFacing(int mCameraFacing) {
        this.mCameraFacing = mCameraFacing;
    }

    /**
     * 是否要增加旁路推流
     *
     * @return
     */
    public boolean isCanAddPublishStreamUrl() {
        //是主播且是视频房才需要旁路推流
        return mCurrentRoomInfo != null && RoomInfo.ROOMTYPE_VIDEO_LIVE == mCurrentRoomInfo.getType()
                && !TextUtils.isEmpty(AvRoomDataManager.get().getCndUrl());
    }


    public Map<String, RoomCharmInfo> getMicCharmInfo() {
        return mMicCharmInfo;
    }

    public void setMicCharmInfo(Map<String, RoomCharmInfo> mMicCharmInfo) {
        this.mMicCharmInfo = mMicCharmInfo;
    }


    public long getCharmTimestamps() {
        return charmTimestamps;
    }

    public void setCharmTimestamps(long charmTimestamps) {
        this.charmTimestamps = charmTimestamps;
    }

    public boolean isHasCharm() {
        return hasCharm;
    }

    public void setHasCharm(boolean hasCharm) {
        this.hasCharm = hasCharm;
    }

    public HeadLineStarAnnounce getAnnounce() {
        return announce;
    }

    public void setAnnounce(HeadLineStarAnnounce announce) {
        this.announce = announce;
    }
}