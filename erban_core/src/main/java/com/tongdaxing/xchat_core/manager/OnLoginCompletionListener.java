package com.tongdaxing.xchat_core.manager;

/**
 * 创建者      Created by dell
 * 创建时间    2018/12/11
 * 描述        即构或声网登录成功回调
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 */
public interface OnLoginCompletionListener {

    void onLoginCompletionFail(String msg);

}
