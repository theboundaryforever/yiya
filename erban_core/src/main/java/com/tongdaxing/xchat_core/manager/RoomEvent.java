package com.tongdaxing.xchat_core.manager;

import com.netease.nimlib.sdk.chatroom.model.ChatRoomKickOutEvent;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.bean.RoomRedPackageInfo;
import com.tongdaxing.xchat_core.bean.UserExperienceUpdateInfo;
import com.tongdaxing.xchat_core.im.custom.bean.RoomCharmAttachment;
import com.tongdaxing.xchat_core.room.auction.bean.AuctionInfo;
import com.tongdaxing.xchat_core.room.bean.CPKInviteBean;
import com.tongdaxing.xchat_core.room.bean.CPKMsgBean;
import com.tongdaxing.xchat_core.room.bean.CPKResultBean;
import com.tongdaxing.xchat_core.room.bean.HeadLineStarAnnounce;
import com.tongdaxing.xchat_core.room.bean.HeadLineStarDistance;
import com.tongdaxing.xchat_core.room.bean.HeadLineStarLimit;
import com.tongdaxing.xchat_core.room.bean.PigFightBean;
import com.tongdaxing.xchat_core.room.bean.PigFightCrtBean;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyBoxAddGold;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyRewardInfo;
import com.tongdaxing.xchat_core.room.bean.RoomLuckyTips;
import com.tongdaxing.xchat_core.room.bean.RoomMicroApplyInfo;
import com.tongdaxing.xchat_core.room.bean.RoomPlantCucumberInfo;
import com.zego.zegoavkit2.soundlevel.ZegoSoundLevelInfo;

import java.util.List;

import lombok.Data;

/**
 * 定义房间的操作
 *
 * @author xiaoyu
 * @date 2017/12/22
 */

@Data
public class RoomEvent {
    public static final int NONE = 0x00000000;
    public static final int ENTER_ROOM = 1;
    public static final int KICK_OUT_ROOM = 2;
    public static final int RECEIVE_MSG = 3;
    public static final int KICK_DOWN_MIC = 4;
    public static final int INVITE_UP_MIC = 5;
    public static final int DOWN_MIC = 6;
    /**
     * 坑的状态：1--锁，0--不锁
     */
    public static final int MIC_QUEUE_STATE_CHANGE = 7;
    public static final int ADD_BLACK_LIST = 8;
    public static final int UP_MIC = 9;
    public static final int MIC_IN_LIST_UPDATE = 32;
    public static final int ROOM_INFO_UPDATE = 10;
    public static final int ROOM_MANAGER_ADD = 11;
    public static final int ROOM_MANAGER_REMOVE = 12;
    /**
     * 即构声浪状态
     */
    public static final int SPEAK_ZEGO_STATE_CHANGE = 33;
    public static final int CURRENT_SPEAK_STATE_CHANGE = 30;
    public static final int ZEGO_RESTART_CONNECTION_EVENT = 31;
    public static final int ZEGO_AUDIO_DEVICE_ERROR = 32;

    public static final int SPEAK_STATE_CHANGE = 13;//声网的声浪
    /**
     * 推拉流失败次数超过10次数据
     */
    public static final int PLAY_OR_PUBLISH_NETWORK_ERROR = 23;
    public static final int FOLLOW = 14;
    public static final int UNFOLLOW = 15;
    public static final int RECHARGE = 16;
    public static final int ROOM_EXIT = 20;
    public static final int ENTER_ROOM_FAIL = 21;
    //房间成员进出
    public static final int ROOM_MEMBER_IN = 0x00000022;
    public static final int ROOM_MEMBER_EXIT = 0x00000023;
    /**
     * 用户被挤下麦
     */
    public static final int DOWN_CROWDED_MIC = 0x00000025;
    /**
     * 网络弱
     */
    public static final int RTC_ENGINE_NETWORK_BAD = 0x00000026;
    public static final int RTC_ENGINE_NETWORK_CLOSE = 0x00000027;
    public static final int METHOD_ON_AUDIO_MIXING_FINISHED = 0x00000028;
    //重连消息
    public static final int ROOM_RECONNECT = 0x00000029;

    public static final int SOCKET_IM_RECONNECT_LOGIN_SUCCESS = 0x00000030;
    //通知邀请上麦（视频房）
    public static final int CODE_INVITE_MIC_ON_VIDEO_ROOM = 0x00000031;
    //通知邀请进房
    public static final int CODE_INVITE_ENTER_ROOM = 0x00000032;
    //有人申请上麦
    public static final int CODE_APPLY_WAIT_QUEUE_NOTICE = 0x00000033;

    //接受红包消息
    public static final int RECEIVE_RED_PACKET = 0x00000033;

    //接受幸运礼物
    public static final int RECEIVE_LUCKY_GIFT = 0x00000034;

    // 接收到禁言
    public static final int RECEIVE_BAN_SEND_MSG = 0x00000035;

    // 连麦事件
    public static final int RECEIVE_LIAN_MICRO_SEND_MSG = 0x00000036;

    //有嘉宾申请上麦
    public static final int CODE_GUEST_APPLY_MICRO = 0x00000037;

    //有嘉宾取消连麦
    public static final int CODE_GUEST_CANCEL_MICRO = 0x00000038;

    //连麦申请列表刷新
    public static final int CODE_BROADCASTER_REFRESH_MICRO_APPLY = 0x00000039;

    //主播封禁
    public static final int CODE_BROADCASTER_BAN = 0x00000040;

    //视频房收益
    public static final int CODE_ROOM_RECEIVE_SUM = 0x00000041;

    //音视频第一帧
    public static final int CODE_FIRST_FRAME = 0x00000042;

    //被强退
    public static final int CODE_FORCE_ON_LIVE = 0x00000043;

    //主播警告
    public static final int CODE_BROADCASTER_WARN = 0x00000044;

    //断线超时
    public static final int CODE_DISCONNECT_TIME_OUT = 0x00000045;

    //摄像头开始捕获
    public static final int ON_CAPTURER_STARTED = 0x00000046;

    //宝箱激活
    public static final int CODE_LUCKY_BOX_ACTIVATE = 0x00000047;

    //弹幕
    public static final int CODE_BARRAGE_MESSAGE = 0x00000048;

    //幸运提示
    public static final int CODE_LUCKY_TIPS = 0x00000049;

    //超级宝箱中奖用户通知
    public static final int CODE_LUCKY_BOX_REWARD_USER = 0x00000050;

    //超级宝箱中奖主播通知
    public static final int CODE_LUCKY_BOX_REWARD_BROADCASTER = 0x00000051;

    // 用户等级更新
    public static final int CODE_USER_EXP_UPDATE = 0x00000052;

    //房间魅力值
    public static final int ROOM_CHARM = 0x00000053;

    //语聊房魅力值PK邀请
    public static final int ROOM_CPK_INVITE = 0x00000054;

    //语聊房魅力值PK结果
    public static final int ROOM_CPK_RESULT = 0x00000055;

    //语聊房魅力值PK消息
    public static final int ROOM_CPK_MSG = 0x00000056;

    //房间公屏消息开启与关闭处理
    public static final int ROOM_PUB_DEL = ROOM_CPK_MSG + 1;

    // 猪猪大作战结果
    public static final int ROOM_PF_RESULT = ROOM_PUB_DEL + 1;

    // 猪猪大作战icon显示与否
    public static final int ROOM_PF_CTR = ROOM_PF_RESULT + 1;

    //幸运礼物普通通知
    public static final int ROOM_LUCKY_GIFT = ROOM_PF_CTR + 1;

    // 幸运礼物全服通知
    public static final int RECEIVE_LUCKY_GIFT_ALL = ROOM_LUCKY_GIFT + 1;

    // 主播加币通知
    public static final int RECEIVE_ADD_COIN = RECEIVE_LUCKY_GIFT_ALL + 1;

    // 通知：头条之星分数超过两万
    public static final int HEAD_LINE_STAR_LIMIT = RECEIVE_ADD_COIN + 1;

    // 通知：头条之星距离上一名
    public static final int HEAD_LINE_STAR_DISTANCE = HEAD_LINE_STAR_LIMIT + 1;

    // 通知：头条之星公告
    public static final int HEAD_LINE_STAR_ANNOUNCE = HEAD_LINE_STAR_DISTANCE + 1;

    //种瓜金币池
    public static final int ROOM_PLANT_CUCUMBER = HEAD_LINE_STAR_ANNOUNCE + 1;

    private int event = NONE;
    private int micPosition = Integer.MIN_VALUE;
    private int posState = -1;
    private ChatRoomKickOutEvent reason;
    private String reason_msg;
    private int reason_no;
    private String account;
    private RoomInfo roomInfo;
    private boolean success;
    private AuctionInfo auctionInfo;
    public RoomQueueInfo roomQueueInfo;
    private int code;
    private String roomId;
    private String clientType;
    private String barrageMsg;

    private String receiveSum;
    private int publicChatSwitch;

    private ChatRoomMessage mChatRoomMessage;

    private IMChatRoomMember chatRoomMember;
    private RoomRedPackageInfo roomRedPackageInfo;

    private RoomLuckyTips roomLuckyTips;//幸运提示info

    private CPKResultBean CPKResultBean;// 魅力值PK结果
    private CPKInviteBean cpkInviteBean;// 魅力值PK邀请
    private CPKMsgBean cpkMsgBean;// 魅力值PK消息

    private UserExperienceUpdateInfo userExperienceUpdateInfo;

    private RoomCharmAttachment roomCharmAttachment;
    private PigFightBean pigFightBean;
    private PigFightCrtBean pigFightCrtBean;

    private RoomLuckyBoxAddGold roomLuckyBoxAddGold;

    private HeadLineStarLimit headLineStarLimit;

    private HeadLineStarDistance headLineStarDistance;

    private HeadLineStarAnnounce headLineStarAnnounce;

    /**
     * 是否禁言（禁止发送公屏消息）
     */
    private boolean isBanSendMsg;

    /**
     * 是否同意连麦
     */
    private boolean isLianMicroAgree;

    /**
     * 当前用户的麦位
     */
    private int currentMicPosition = Integer.MIN_VALUE;

    private float currentMicStreamLevel = 0;

    private List<Integer> micPositionList;

    //即构的说话队列
    protected List<ZegoSoundLevelInfo> speakQueueMembersPosition;
    private RoomMicroApplyInfo roomMicroApplyInfo;
    private RoomLuckyRewardInfo roomLuckyRewardInfo;
    private RoomPlantCucumberInfo roomPlantCucumberInfo;

    public int getEvent() {
        return event;
    }

    public RoomEvent setEvent(int event) {
        this.event = event;
        return this;
    }

    public UserExperienceUpdateInfo getUserExperienceUpdateInfo() {
        return userExperienceUpdateInfo;
    }

    public RoomEvent setUserExperienceUpdateInfo(UserExperienceUpdateInfo userExperienceUpdateInfo) {
        this.userExperienceUpdateInfo = userExperienceUpdateInfo;
        return this;
    }

    public ChatRoomKickOutEvent getReason() {
        return reason;
    }

    public RoomRedPackageInfo getRoomRedPackageInfo() {
        return roomRedPackageInfo;
    }

    public RoomEvent setRoomRedPackageInfo(RoomRedPackageInfo roomRedPackageInfo) {
        this.roomRedPackageInfo = roomRedPackageInfo;
        return this;
    }

    public RoomEvent setReason(ChatRoomKickOutEvent reason) {
        this.reason = reason;
        return this;
    }

    public RoomEvent setRoomQueueInfo(RoomQueueInfo roomQueueInfo) {
        this.roomQueueInfo = roomQueueInfo;
        return this;
    }

    public int getMicPosition() {
        return micPosition;
    }

    public RoomEvent setMicPosition(int micPosition) {
        this.micPosition = micPosition;
        return this;
    }

    public int getPosState() {
        return posState;
    }

    public RoomEvent setPosState(int posState) {
        this.posState = posState;
        return this;
    }

    public String getAccount() {
        return account;
    }

    public RoomEvent setAccount(String account) {
        this.account = account;
        return this;
    }

    public RoomEvent setReceiveSum(String sum) {
        receiveSum = sum;
        return this;
    }

    public String getRoomId() {
        return roomId;
    }

    public RoomEvent setRoomId(String roomId) {
        this.roomId = roomId;
        return this;
    }

    public String getClientType() {
        return clientType;
    }

    public RoomEvent setClientType(String clientType) {
        this.clientType = clientType;
        return this;
    }

    public RoomInfo getRoomInfo() {
        return roomInfo;
    }

    public RoomEvent setRoomInfo(RoomInfo roomInfo) {
        this.roomInfo = roomInfo;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public RoomEvent setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public AuctionInfo getAuctionInfo() {
        return auctionInfo;
    }

    public RoomEvent setAuctionInfo(AuctionInfo auctionInfo) {
        this.auctionInfo = auctionInfo;
        return this;
    }

    public ChatRoomMessage getChatRoomMessage() {
        return mChatRoomMessage;
    }

    public RoomEvent setChatRoomMessage(ChatRoomMessage chatRoomMessage) {
        mChatRoomMessage = chatRoomMessage;
        return this;
    }

    public IMChatRoomMember getChatRoomMember() {
        return chatRoomMember;
    }

    public RoomEvent setChatRoomMember(IMChatRoomMember chatRoomMember) {
        this.chatRoomMember = chatRoomMember;
        return this;
    }

    public String getReason_msg() {
        return reason_msg;
    }

    public RoomEvent setReason_msg(String reason_msg) {
        this.reason_msg = reason_msg;
        return this;
    }

    public RoomEvent setForceOnlive(String clientType, String roomId) {
        this.reason_msg = reason_msg;
        return this;
    }

    public int getReason_no() {
        return reason_no;
    }

    public RoomEvent setReason_no(int reason_no) {
        this.reason_no = reason_no;
        return this;
    }

    public int getCode() {
        return code;
    }

    public RoomEvent setCode(int code) {
        this.code = code;
        return this;
    }

    public String getBarrageMsg() {
        return barrageMsg;
    }

    public RoomEvent setBarrageMsg(String barrageMsg) {
        this.barrageMsg = barrageMsg;
        return this;
    }

    public List<Integer> getMicPositionList() {
        return micPositionList;
    }

    public RoomEvent setMicPositionList(List<Integer> micPositionList) {
        this.micPositionList = micPositionList;
        return this;
    }

    public int getCurrentMicPosition() {
        return currentMicPosition;
    }

    public RoomEvent setCurrentMicPosition(int currentMicPosition) {
        this.currentMicPosition = currentMicPosition;
        return this;
    }

    public float getCurrentMicStreamLevel() {
        return currentMicStreamLevel;
    }

    public RoomEvent setCurrentMicStreamLevel(float currentMicStreamLevel) {
        this.currentMicStreamLevel = currentMicStreamLevel;
        return this;
    }

    public List<ZegoSoundLevelInfo> getSpeakQueueMembersPosition() {
        return speakQueueMembersPosition;
    }

    public RoomEvent setSpeakQueueMembersPosition(List<ZegoSoundLevelInfo> speakQueueMembersPosition) {
        this.speakQueueMembersPosition = speakQueueMembersPosition;
        return this;
    }

    public boolean isBanSendMsg() {
        return isBanSendMsg;
    }

    public RoomEvent setBanSendMsg(boolean banSendMsg) {
        isBanSendMsg = banSendMsg;
        return this;
    }

    public boolean isLianMicroAgree() {
        return isLianMicroAgree;
    }

    public RoomEvent setLianMicroAgree(boolean lianMicroAgree) {
        isLianMicroAgree = lianMicroAgree;
        return this;
    }

    public RoomEvent setRoomMicroApplyInfo(RoomMicroApplyInfo roomMicroApplyInfo) {
        this.roomMicroApplyInfo = roomMicroApplyInfo;
        return this;
    }

    public RoomMicroApplyInfo getRoomMicroApplyInfo() {
        return roomMicroApplyInfo;
    }

    public RoomLuckyTips getRoomLuckyTips() {
        return roomLuckyTips;
    }

    public RoomEvent setRoomLuckyTips(RoomLuckyTips roomLuckyTips) {
        this.roomLuckyTips = roomLuckyTips;
        return this;
    }

    public RoomLuckyRewardInfo getRoomLuckyRewardInfo() {
        return roomLuckyRewardInfo;
    }

    public RoomEvent setLuckyReward(RoomLuckyRewardInfo roomLuckyRewardInfo) {
        this.roomLuckyRewardInfo = roomLuckyRewardInfo;
        return this;
    }


    public RoomCharmAttachment getRoomCharmAttachment() {
        return roomCharmAttachment;
    }

    public RoomEvent setRoomCharmAttachment(RoomCharmAttachment roomCharmAttachment) {
        this.roomCharmAttachment = roomCharmAttachment;
        return this;
    }

    public CPKResultBean getCPKResultBean() {
        return CPKResultBean;
    }

    public RoomEvent setCPKResultBean(CPKResultBean CPKResultBean) {
        this.CPKResultBean = CPKResultBean;
        return this;
    }

    public CPKInviteBean getCpkInviteBean() {
        return cpkInviteBean;
    }

    public RoomEvent setCpkInviteBean(CPKInviteBean cpkInviteBean) {
        this.cpkInviteBean = cpkInviteBean;
        return this;
    }

    public CPKMsgBean getCpkMsgBean() {
        return cpkMsgBean;
    }

    public RoomEvent setCpkMsgBean(CPKMsgBean cpkMsgBean) {
        this.cpkMsgBean = cpkMsgBean;
        return this;
    }

    public int getPublicChatSwitch() {
        return publicChatSwitch;
    }

    public RoomEvent setPublicChatSwitch(int publicChatSwitch) {
        this.publicChatSwitch = publicChatSwitch;
        return this;
    }

    public PigFightBean getPigFightBean() {
        return pigFightBean;
    }

    public RoomEvent setPigFightBean(PigFightBean pigFightBean) {
        this.pigFightBean = pigFightBean;
        return this;
    }

    public PigFightCrtBean getPigFightCrtBean() {
        return pigFightCrtBean;
    }

    public RoomEvent setPigFightCrtBean(PigFightCrtBean pigFightCrtBean) {
        this.pigFightCrtBean = pigFightCrtBean;
        return this;
    }

    public RoomLuckyBoxAddGold getRoomLuckyBoxAddGold() {
        return roomLuckyBoxAddGold;
    }

    public RoomEvent setRoomLuckyBoxAddGold(RoomLuckyBoxAddGold roomLuckyBoxAddGold) {
        this.roomLuckyBoxAddGold = roomLuckyBoxAddGold;
        return this;
    }

    public HeadLineStarLimit getHeadLineStarLimit() {
        return headLineStarLimit;
    }

    public RoomEvent setHeadLineStarLimit(HeadLineStarLimit headLineStarLimit) {
        this.headLineStarLimit = headLineStarLimit;
        return this;
    }

    public HeadLineStarDistance getHeadLineStarDistance() {
        return headLineStarDistance;
    }

    public RoomEvent setHeadLineStarDistance(HeadLineStarDistance headLineStarDistance) {
        this.headLineStarDistance = headLineStarDistance;
        return this;
    }

    public HeadLineStarAnnounce getHeadLineStarAnnounce() {
        return headLineStarAnnounce;
    }

    public RoomEvent setHeadLineStarAnnounce(HeadLineStarAnnounce headLineStarAnnounce) {
        this.headLineStarAnnounce = headLineStarAnnounce;
        return this;
    }

    public RoomPlantCucumberInfo getRoomPlantCucumberInfo() {
        return roomPlantCucumberInfo;
    }

    public RoomEvent setRoomPlantCucumberInfo(RoomPlantCucumberInfo roomPlantCucumberInfo) {
        this.roomPlantCucumberInfo = roomPlantCucumberInfo;
        return this;
    }
}