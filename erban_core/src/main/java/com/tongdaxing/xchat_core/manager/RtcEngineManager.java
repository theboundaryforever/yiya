package com.tongdaxing.xchat_core.manager;

import android.support.annotation.IntRange;

import com.tongdaxing.xchat_core.manager.zego.BaseRtcEngine;
import com.tongdaxing.xchat_core.manager.zego.IBaseRtcEngine;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import io.agora.rtc.mediaio.AgoraSurfaceView;
import io.agora.rtc.mediaio.IVideoSource;
import io.agora.rtc.video.VideoCanvas;

/**
 * 实时音视频代理类
 *
 * @author Edwin
 * @date 2018/11/08
 */
public final class RtcEngineManager implements IBaseRtcEngine {

    /**
     * 声波回调间隔
     */
    public final static int AUDIO_UPDATE_INTERVAL = 600;

    private static volatile RtcEngineManager sEngineManager;
    private static final Object SYNC_OBJECT = new Object();

    /**
     * 控制选用的语音类型 默认是声网
     */
    private int audioOrganization = AGORA;
    /**
     * 新增类型，需要在setAudioOrganization方法中设置注解范围
     */
    public static final int AGORA = 1;
    public static final int ZEGO = 2;

    /**
     * 禁麦状态
     * MUTE_STATUS_DEFAULT 默认/未知，MUTE_STATUS_NONE 声音视频都没禁，MUTE_STATUS_MUTE_AUDIO 只禁声音流，MUTE_STATUS_MUTE_VIDEO 只禁视频流，MUTE_STATUS_MUTE_ALL 声音流和视频流都禁了
     */
    public final static int MUTE_STATUS_DEFAULT = 0, MUTE_STATUS_NONE = -1, MUTE_STATUS_MUTE_AUDIO = 1, MUTE_STATUS_MUTE_VIDEO = 2, MUTE_STATUS_MUTE_ALL = 3;

    public static RtcEngineManager get() {
        if (sEngineManager == null) {
            synchronized (SYNC_OBJECT) {
                if (sEngineManager == null) {
                    sEngineManager = new RtcEngineManager();
                }
            }
        }
        return sEngineManager;
    }

    public void initRtcEngine() {
        AgoraRtcEngine.get().initRtcEngine();
        //项目暂没即构
//        ZegoRtcEngine.get().initRtcEngine();
    }

    @Override
    public void configEngine(int roomType, boolean isRoomOwner) {
        factory().configEngine(roomType, isRoomOwner);
    }

    @Override
    public void startPreview(boolean startPreview) {
        factory().startPreview(startPreview);
    }

    /**
     * 是否启动即构
     *
     * @param audioOrganization 根据定义的静态变量设置
     */
    public void setAudioOrganization(@IntRange(from = AGORA, to = ZEGO) int audioOrganization) {
        this.audioOrganization = audioOrganization;
    }

    @Override
    public void setOnLoginCompletionListener(OnLoginCompletionListener listener) {
        factory().setOnLoginCompletionListener(listener);
    }

    @Override
    public boolean setLocalView(AgoraSurfaceView roomOwnerView) {
        return factory().setLocalView(roomOwnerView);
    }

    @Override
    public int setVideoSource(IVideoSource var1) {
        return factory().setVideoSource(var1);
    }

    @Override
    public boolean joinChannel(long uid, String key, RoomInfo curRoomInfo) {
        return factory().joinChannel(uid, key, curRoomInfo);
    }

    @Override
    public void stopAudioMixing() {
        factory().stopAudioMixing();
    }

    @Override
    public void leaveChannel() {
        factory().leaveChannel();
    }

    @Override
    public void setRemoteMute(boolean mute) {
        factory().setRemoteMute(mute);
    }

    /**
     * 设置角色，上麦，下麦（调用）
     *
     * @param type 1为视频，0为音频
     * @param role CLIENT_ROLE_AUDIENCE: 听众 ，CLIENT_ROLE_BROADCASTER: 主播
     */
    @Override
    public void setRole(int type, int role) {
        factory().setRole(type, role);
    }

    /**
     * 设置是否能说话，静音,人自己的行为
     *
     * @param type 1为视频，0为音频
     * @param mute true：静音，false：不静音
     */
    @Override
    public int setMute(int type, boolean mute) {
        return factory().setMute(type, mute);
    }

    /**
     * 开/关本地视频流发送
     *
     * @return 0为成功
     */
    @Override
    public int muteLocalVideoStream(boolean enabled) {
        return factory().muteLocalVideoStream(enabled);
    }

    //音乐播放相关---------------begin--------------------------

    @Override
    public void adjustAudioMixingVolume(int volume) {
        factory().adjustAudioMixingVolume(volume);
    }

    @Override
    public void adjustRecordingSignalVolume(int volume) {
        factory().adjustRecordingSignalVolume(volume);
    }

    @Override
    public void resumeAudioMixing() {
        factory().resumeAudioMixing();
    }

    @Override
    public void pauseAudioMixing() {
        factory().pauseAudioMixing();
    }

    @Override
    public long getAudioMixingCurrentPosition() {
        return factory().getAudioMixingCurrentPosition();
    }

    @Override
    public long getAudioMixingDuration() {
        return factory().getAudioMixingDuration();
    }

    @Override
    public boolean isAudienceRole() {
        return factory().isAudienceRole();
    }

    @Override
    public boolean isRemoteMute() {
        return factory().isRemoteMute();
    }

    @Override
    public boolean isMute() {
        return factory().isMute();
    }

    /**
     * 获取当前禁麦状态
     */
    public int getCurrMuteStatus() {
        int muteStatus = MUTE_STATUS_NONE;
        if (isMute() && isLocalVideoMute()) {
            //声音流和视频流都禁了
            muteStatus = MUTE_STATUS_MUTE_ALL;
        } else if (isMute() && !isLocalVideoMute()) {
            //只禁声音流
            muteStatus = MUTE_STATUS_MUTE_AUDIO;
        } else if (!isMute() && isLocalVideoMute()) {
            //只禁视频流
            muteStatus = MUTE_STATUS_MUTE_VIDEO;
        }
        return muteStatus;
    }

    @Override
    public boolean isLocalVideoMute() {
        return false;
    }

    /**
     * 即构关闭指定用户的流，声网不使用
     *
     * @param uid 流id
     */
    @Override
    public void stopPlayingStream(String uid) {
        factory().stopPlayingStream(uid);
    }

    @Override
    public void setupLocalVideo(VideoCanvas videoCanvas) {
        factory().setupLocalVideo(videoCanvas);
    }

    @Override
    public void setupRemoteVideo(VideoCanvas videoCanvas) {
        factory().setupRemoteVideo(videoCanvas);
    }

    @Override
    public int startAudioMixing(String filePath, boolean loopback, int cycle) {
        return factory().startAudioMixing(filePath, loopback, cycle);
    }

    @Override
    public void updateTranscodingUsers() {
        factory().updateTranscodingUsers();
    }

    private BaseRtcEngine factory() {
        switch (audioOrganization) {
            case 1:
                return AgoraRtcEngine.get();
            case 2:
//               项目没即构
//                return ZegoRtcEngine.get();
                return AgoraRtcEngine.get();
            default:
                throw new IllegalArgumentException("参数错误");
        }
    }
}
