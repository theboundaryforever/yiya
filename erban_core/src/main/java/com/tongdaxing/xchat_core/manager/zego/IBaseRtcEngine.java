package com.tongdaxing.xchat_core.manager.zego;

import com.tongdaxing.xchat_core.manager.OnLoginCompletionListener;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import io.agora.rtc.mediaio.AgoraSurfaceView;
import io.agora.rtc.mediaio.IVideoSource;
import io.agora.rtc.video.VideoCanvas;

/**
 * 创建者      Created by Edwin
 * 创建时间    2018/11/4
 * 描述        音频相关的接口方法类
 * <p>
 * 更新者      Edwin
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author Edwin
 */
public interface IBaseRtcEngine {

    void initRtcEngine();

    /**
     * 引擎配置
     */
    void configEngine(int roomType, boolean isRoomOwner);

    /**
     * 设置本地视图
     *
     * @param roomOwnerView
     * @return
     */
    boolean setLocalView(AgoraSurfaceView roomOwnerView);

    int setVideoSource(IVideoSource var1);

    /**
     * 开启视频预览 true 开启 false 关闭
     *
     * @param startPreview
     */
    void startPreview(boolean startPreview);

    /**
     * 进去频道
     *
     * @param uid         用户的uid  （zego streamId）
     * @param key         token
     * @param curRoomInfo 房间信息
     */
    boolean joinChannel(long uid, String key, RoomInfo curRoomInfo);

    /**
     * 设置登录成功的回调
     */
    void setOnLoginCompletionListener(OnLoginCompletionListener listener);

    /**
     * 退出房间
     */
    void leaveChannel();

    /**
     * 禁闭喇叭
     */
    void setRemoteMute(boolean mute);

    /**
     * 设置角色，上麦，下麦（调用）
     *
     * @param type 1为视频、0为音频
     * @param role CLIENT_ROLE_AUDIENCE: 听众 ，CLIENT_ROLE_BROADCASTER: 主播
     */
    void setRole(int type, int role);

    /**
     * 设置是否能说话，静音,人自己的行为
     *
     * @param type 1为视频，0为音频
     * @param mute true：静音，false：不静音
     */
    int setMute(int type, boolean mute);

    /**
     * 开/关本地视频流发送
     *
     * @return 0为成功
     */
    int muteLocalVideoStream(boolean enabled);

    /**
     * 设置音乐播放的声音
     *
     * @param volume 音量大小
     */
    void adjustAudioMixingVolume(int volume);

    /**
     * 设置人声播放的声音
     *
     * @param volume 音量大小
     */
    void adjustRecordingSignalVolume(int volume);

    /**
     * 停止音乐播放
     */
    void stopAudioMixing();

    /**
     * 播放音乐
     *
     * @param filePath 文件路径
     */
    int startAudioMixing(String filePath, boolean loopback, int cycle);

    /**
     * 恢复播放
     */
    void resumeAudioMixing();

    /**
     * 暂停播放
     */
    void pauseAudioMixing();

    /**
     * 获取当前播放进度
     */
    long getAudioMixingCurrentPosition();

    /**
     * 获取整个文件的播放时间
     */
    long getAudioMixingDuration();

    /**
     * 获取用户身份状态
     */
    boolean isAudienceRole();

    /**
     * 获取是否关闭喇叭的声音的状态
     */
    boolean isRemoteMute();

    /**
     * 获取是否禁麦的状态
     */
    boolean isMute();

    /**
     * 是否开启视频采集
     */
    boolean isLocalVideoMute();

    /**
     * 关闭某个指定流数据
     *
     * @param uid 要禁止的用户的uid
     */
    void stopPlayingStream(String uid);

    void setupLocalVideo(VideoCanvas videoCanvas);

    void setupRemoteVideo(VideoCanvas videoCanvas);


    /**
     * 旁路推流  更新合流成员
     */
    void updateTranscodingUsers();
}
