package com.tongdaxing.xchat_core.manager;

import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.faceunity.FURenderer;
import com.juxiao.library_utils.log.LogUtil;
import com.juxiao.library_utils.storage.StorageType;
import com.juxiao.library_utils.storage.StorageUtil;
import com.tongdaxing.xchat_core.manager.zego.BaseRtcEngine;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.live.LiveTranscoding;
import io.agora.rtc.mediaio.AgoraDefaultRender;
import io.agora.rtc.mediaio.AgoraDefaultSource;
import io.agora.rtc.mediaio.AgoraSurfaceView;
import io.agora.rtc.mediaio.IVideoSource;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;

import static com.tongdaxing.xchat_core.manager.RtcEngineManager.AUDIO_UPDATE_INTERVAL;
import static io.agora.rtc.Constants.AUDIO_PROFILE_DEFAULT;
import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_HIGH_QUALITY;
import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_HIGH_QUALITY_STEREO;
import static io.agora.rtc.Constants.AUDIO_PROFILE_MUSIC_STANDARD_STEREO;
import static io.agora.rtc.Constants.AUDIO_RECORDING_QUALITY_LOW;
import static io.agora.rtc.Constants.AUDIO_SCENARIO_SHOWROOM;
import static io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE;

/**
 * 创建者      Created by dell
 * 创建时间    2018/11/8
 * 描述        声网引擎类
 *
 * @author Edwin
 */
public class AgoraRtcEngine extends BaseRtcEngine {

    public static final String LOG_FILE_NAME = "agora-rtc.log";
    public static final String TAG = "room_log ---> Agora";

    private static final int MSG_ROLE_CHANGED = 3;

    private static volatile AgoraRtcEngine sEngineManager;

    private RtcEngine mRtcEngine;
    private EngineEventHandler engineEventHandler;

    private LiveTranscoding mLiveTranscoding;


    private AgoraRtcEngine() {
        speakQueueMembersPosition = new ArrayList<>();
    }

    public static AgoraRtcEngine get() {
        if (sEngineManager == null) {
            synchronized (AgoraRtcEngine.class) {
                if (sEngineManager == null) {
                    sEngineManager = new AgoraRtcEngine();
                }
            }
        }
        return sEngineManager;
    }

    @Override
    public void initRtcEngine() {
        if (mRtcEngine == null) {
            try {
                if (engineEventHandler == null) {
                    engineEventHandler = new EngineEventHandler(this);
                }
                //560ac7a934fd42fd90fd95bc71c74152(旁路推流)
                //f96845fa53ebbb354757a0cec897df86
                mRtcEngine = RtcEngine.create(BasicConfig.INSTANCE.getAppContext(), "95f605309756499cb00cf5e7d61ab345", engineEventHandler);//App ID：95f605309756499cb00cf5e7d61ab345

                FURenderer.initFURenderer(BasicConfig.INSTANCE.getAppContext());
            } catch (Exception e) {
                LogUtil.i("setRole", "need to check rtc sdk init fatal error = " + e != null ? e.getMessage() : "");
            }
        }
    }

    @Override
    public void configEngine(int roomType, boolean isRoomOwner) {
        initRtcEngine();
        if (mRtcEngine != null) {
            setRole(roomType == RoomInfo.ROOMTYPE_VIDEO_LIVE ? 1 : 0, Constants.CLIENT_ROLE_AUDIENCE);
            mRtcEngine.enableAudioVolumeIndication(AUDIO_UPDATE_INTERVAL, 3);
            mRtcEngine.setDefaultAudioRoutetoSpeakerphone(true);
            mRtcEngine.setLogFile(getLogFilePath());
            if (roomType == RoomInfo.ROOMTYPE_VIDEO_LIVE) {//视频房
                //打开视频模式
                mRtcEngine.enableVideo();
                //设置频道模式为直播
                mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
                //开/关视频双流模式（发送端开启双流模式后，接收端可以选择接收大流还是小流）
                mRtcEngine.enableDualStreamMode(true);

                // 配置分辨率 帧率
                VideoEncoderConfiguration.ORIENTATION_MODE orientationMode = VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT;
                VideoEncoderConfiguration videoEncoderConfiguration;
                if (isRoomOwner) {
                    VideoEncoderConfiguration.VideoDimensions dimensions = new VideoEncoderConfiguration.VideoDimensions(848, 480);
                    videoEncoderConfiguration = new VideoEncoderConfiguration(dimensions, VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15, 1200, orientationMode);
                } else {
                    VideoEncoderConfiguration.VideoDimensions dimensions = new VideoEncoderConfiguration.VideoDimensions(128, 160);
                    videoEncoderConfiguration = new VideoEncoderConfiguration(dimensions, VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15, 200, orientationMode);
                }
                mRtcEngine.setVideoEncoderConfiguration(videoEncoderConfiguration);

                //打开与 Web SDK 的互通（仅在直播下适用）
                mRtcEngine.enableWebSdkInteroperability(true);

                //设置角色
                if (isRoomOwner) {
                    mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
                } else {
                    mRtcEngine.setClientRole(CLIENT_ROLE_AUDIENCE);
                }

                //非房主重置renderer和videoSource（房主会自定义renderer和videoSource）
                if (!isRoomOwner) {
                    //重置renderer和videoSource为默认
                    mRtcEngine.setLocalVideoRenderer(new AgoraDefaultRender());
                    setVideoSource(new AgoraDefaultSource());
                }

                if (isRoomOwner) {
                    initTranscoding(480, 640, 1200, 15);
                } else {
                    initTranscoding(128, 160, 200, 15);
                }
            } else {
                mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);  //设置频道模式为直播
                //语音房关闭视频模式
                mRtcEngine.disableVideo();
            }
        }
    }

    @Override
    public boolean setLocalView(AgoraSurfaceView roomOwnerView) {
        int i = mRtcEngine.setLocalVideoRenderer(roomOwnerView);
        int i1 = mRtcEngine.setupLocalVideo(new VideoCanvas(roomOwnerView, VideoCanvas.RENDER_MODE_HIDDEN, 0));
        return i != 0 && i1 != 0;
    }

    @Override
    public int setVideoSource(IVideoSource var1) {
        if (mRtcEngine != null) {
            return mRtcEngine.setVideoSource(var1);
        } else {
            return -1;
        }
    }

    @Override
    public boolean joinChannel(long uid, String key, RoomInfo curRoomInfo) {
        LogUtil.i(TAG, "joinChannel RoomId = " + curRoomInfo.getRoomId() + " ---> uid = " + uid);
        this.uid = uid;
        int quality = AUDIO_PROFILE_DEFAULT;
        int audioLevel = curRoomInfo.getAudioLevel();
        if (audioLevel == 1) {
            quality = AUDIO_PROFILE_MUSIC_STANDARD_STEREO;
        } else if (audioLevel == 2) {
            quality = AUDIO_PROFILE_MUSIC_HIGH_QUALITY;
        } else if (audioLevel >= 3) {
            quality = AUDIO_PROFILE_MUSIC_HIGH_QUALITY_STEREO;
        }
        this.isMute = false;
        this.isRemoteMute = false;

        if (mRtcEngine != null) {
            mRtcEngine.setAudioProfile(quality, AUDIO_SCENARIO_SHOWROOM);
            //创建并加入频道
            int joinChannel = mRtcEngine.joinChannel(key, String.valueOf(curRoomInfo.getRoomId()), null, (int) uid);
            LogUtil.i(TAG, "joinChannel ---> status = " + joinChannel);
            return joinChannel == 0;
        } else {
            return false;
        }
    }

    @Override
    public void setOnLoginCompletionListener(OnLoginCompletionListener listener) {
    }

    @Override
    public void stopAudioMixing() {
        if (mRtcEngine != null) {
            mRtcEngine.stopAudioMixing();
        }
    }

    /**
     * 该方法目前仅用于即构的处理，当前只是实现  不处理，外部勿调用
     *
     * @param uid 流id
     */
    @Deprecated
    @Override
    public void stopPlayingStream(String uid) {

    }

    @Override
    public void leaveChannel() {
        if (mRtcEngine != null) {
            stopAudioMixing();
            if (!TextUtils.isEmpty(AvRoomDataManager.get().getCndUrl())) {
                mRtcEngine.removePublishStreamUrl(AvRoomDataManager.get().getCndUrl());
            }
            int leaveChannel = mRtcEngine.leaveChannel();
            LogUtil.i(TAG, "leaveChannel ---> logoutRoom = " + leaveChannel);
        }
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }

        isAudienceRole = true;
        isMute = false;
        isRemoteMute = false;
        needRecord = false;
    }

    @Override
    public void setRemoteMute(boolean mute) {
        if (mRtcEngine != null) {
            int result = mRtcEngine.muteAllRemoteAudioStreams(mute);
            LogUtil.i(TAG, "setRemoteMute ---> mute = " + mute + " ---> result = " + result);
            if (result == 0) {
                isRemoteMute = mute;
            } else {
                LogUtil.i("setRemoteMute", "mute = " + mute + "  result = " + result);
            }
        }
    }

    /**
     * 设置角色，上麦，下麦（调用）
     *
     * @param type 1为视频、0为音频
     * @param role CLIENT_ROLE_AUDIENCE: 听众 ，CLIENT_ROLE_BROADCASTER: 主播
     */
    @Override
    public void setRole(int type, int role) {
        if (mRtcEngine != null) {
            int result = -1;
            if (role == Constants.CLIENT_ROLE_BROADCASTER &&
                    IMNetEaseManager.get().isImRoomConnection() && AvRoomDataManager.get().isOwnerOnMic()) {
                isAudienceRole = false;
                //是否静音
                if (isMute) {
                    if (type == 0) {
                        result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
                    } else if (type == 1) {
                        result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
                        //静音设置成功后才能设置为主播（避免本是静音的情况下上麦后有声音）
                        int muteResult = setMute(1, true);
                        if (muteResult != 0) {
                            mRtcEngine.setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
                            result = -1;
                        }
                    }
                } else {
                    result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
                }
            } else {
                isAudienceRole = true;
                result = mRtcEngine.setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
            }

//            if (result == 0 && type == 1) {
//                if (!TextUtils.isEmpty(AvRoomDataManager.get().getCndUrl())) {
//                    if (role == Constants.CLIENT_ROLE_BROADCASTER) {
//
//                        updateTranscodingUsers();
//                        // rtmp://vid-218.push.chinanetcenter.broadcastapp.agora.io/live/Stream_Name111
//                        int value = mRtcEngine.addPublishStreamUrl(AvRoomDataManager.get().getCndUrl(), false);
//                        LogUtil.i(TAG, "cnd_url: " + AvRoomDataManager.get().getCndUrl() + "\t" + value);
//                    } else {
//                        mRtcEngine.removePublishStreamUrl(AvRoomDataManager.get().getCndUrl());
//                    }
//                }
//            }
            LogUtil.i(TAG, "setRole ---> role = " + role + " ---> result = " + result + " ---> isAudienceRole = " + isAudienceRole);
        }
    }

    /**
     * 设置是否能说话，静音,人自己的行为
     *
     * @param type 1为视频，0为音频
     * @param mute true：静音，false：不静音
     */
    @Override
    public int setMute(int type, boolean mute) {
        int result = -1;
        if (mRtcEngine != null) {
            if (type == 1) {
                result = mRtcEngine.muteLocalAudioStream(mute);
            } else {
                result = mRtcEngine.setClientRole(mute ? Constants.CLIENT_ROLE_AUDIENCE : Constants.CLIENT_ROLE_BROADCASTER);
            }
            LogUtil.i(TAG, "setMute ---> mute（true：静音，false：不静音） = " + mute + " ---> result = " + result);
            if (result == 0) {
                isMute = mute;
            }
        }
        return result;
    }

    /**
     * 开/关本地视频流发送
     *
     * @return 0为成功
     */
    @Override
    public int muteLocalVideoStream(boolean mute) {
        int result = -1;
        if (mRtcEngine != null) {
            result = mRtcEngine.muteLocalVideoStream(mute);
            LogUtil.i(TAG, "enableLocalVideo ---> enabled = " + mute + " ---> result = " + result);
            if (result == 0) {
                isLocalVideoMute = mute;
            }
        }
        return result;
    }

    private Handler handler = new RtcEngineHandler(this);

    @Override
    public String getLogFilePath() {
        return StorageUtil.getDirectoryByDirType(StorageType.TYPE_LOG) + "/" + LOG_FILE_NAME;
    }

    private static class RtcEngineHandler extends Handler {

        private WeakReference<AgoraRtcEngine> mReference;

        RtcEngineHandler(AgoraRtcEngine manager) {
            mReference = new WeakReference<>(manager);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            AgoraRtcEngine rtcEngineManager = mReference.get();
            if (rtcEngineManager == null) {
                return;
            }
            if (msg.what == 0) {
                IMNetEaseManager.get().joinAvRoom();
                if (rtcEngineManager.needRecord) {
                    rtcEngineManager.mRtcEngine.startAudioRecording(Environment.getExternalStorageDirectory()
                            + File.separator + BasicConfig.INSTANCE.getAppContext().getPackageName()
                            + "/audio/" + System.currentTimeMillis() + ".aac", AUDIO_RECORDING_QUALITY_LOW);
                }
                if (AvRoomDataManager.get().isRoomOwner()) {
                    //是否主播
                    handlePublishStreamUrl(rtcEngineManager);
                }
            } else if (msg.what == 1) {
                IRtcEngineEventHandler.AudioVolumeInfo[] speakers = (IRtcEngineEventHandler.AudioVolumeInfo[]) msg.obj;
                RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
                if (roomInfo == null) {
                    return;
                }
                if (rtcEngineManager.speakQueueMembersPosition.size() > 0) {
                    rtcEngineManager.speakQueueMembersPosition.clear();
                }
                for (IRtcEngineEventHandler.AudioVolumeInfo speaker : speakers) {
                    // 0 代表的是自己,其他代表的是uid
                    long uid = speaker.uid == 0 ? rtcEngineManager.uid : speaker.uid;
                    int micPosition = AvRoomDataManager.get().getMicPosition(uid);
                    if (micPosition == Integer.MIN_VALUE) {
                        continue;
                    }
                    rtcEngineManager.speakQueueMembersPosition.add(micPosition);
                }
                IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                        new RoomEvent().setEvent(RoomEvent.SPEAK_STATE_CHANGE)
                                .setMicPositionList(rtcEngineManager.speakQueueMembersPosition));
            } else if (msg.what == 2) {
                Integer uid = (Integer) msg.obj;
            } else if (msg.what == 3) {
                int role = msg.arg1;
                if (role == Constants.CLIENT_ROLE_BROADCASTER) {
                    handlePublishStreamUrl(rtcEngineManager);
                }
            }
        }

        /**
         * 是否需要增加旁路推流
         *
         * @param rtcEngineManager
         */
        private void handlePublishStreamUrl(AgoraRtcEngine rtcEngineManager) {
            if (AvRoomDataManager.get().isCanAddPublishStreamUrl()) {
                rtcEngineManager.updateTranscodingUsers();
                // rtmp://vid-218.push.chinanetcenter.broadcastapp.agora.io/live/Stream_Name111
                int value = rtcEngineManager.mRtcEngine.addPublishStreamUrl(AvRoomDataManager.get().getCndUrl(), false);
                LogUtil.i(TAG, "cnd_url: " + AvRoomDataManager.get().getCndUrl() + "\t" + value);
            } else {
                rtcEngineManager.mRtcEngine.removePublishStreamUrl(AvRoomDataManager.get().getCndUrl());
                LogUtil.i(TAG, "remove—cnd_url: " + AvRoomDataManager.get().getCndUrl());
            }
        }
    }

    private static class EngineEventHandler extends IRtcEngineEventHandler {
        private WeakReference<AgoraRtcEngine> mReference;

        EngineEventHandler(AgoraRtcEngine manager) {
            mReference = new WeakReference<>(manager);
        }

        @Override
        public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
            super.onJoinChannelSuccess(channel, uid, elapsed);
            LogUtil.i(TAG, "onJoinChannelSuccess ---> channel = " + channel + " ---> uid = " + uid + " ---> elapsed = " + elapsed);
            if (mReference.get() != null) {
                mReference.get().handler.sendEmptyMessage(0);
            }

        }

        @Override
        public void onLastmileQuality(int quality) {
            super.onLastmileQuality(quality);
            if (quality >= 3) {
                IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                        new RoomEvent().setEvent(RoomEvent.RTC_ENGINE_NETWORK_BAD)
                );
            }
        }

        @Override
        public void onConnectionInterrupted() {
            super.onConnectionInterrupted();
            IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                    new RoomEvent().setEvent(RoomEvent.RTC_ENGINE_NETWORK_CLOSE)
            );
        }

        @Override
        public void onConnectionLost() {
            super.onConnectionLost();
            IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                    new RoomEvent().setEvent(RoomEvent.RTC_ENGINE_NETWORK_CLOSE)
            );
        }

        @Override
        public void onAudioVolumeIndication(AudioVolumeInfo[] speakers, int totalVolume) {
            super.onAudioVolumeIndication(speakers, totalVolume);
            AgoraRtcEngine manager = mReference.get();
            if (manager != null) {
                Message message = manager.handler.obtainMessage();
                message.what = 1;
                message.obj = speakers;
                manager.handler.sendMessage(message);
            }
        }

        @Override
        public void onUserMuteAudio(int uid, boolean muted) {
            super.onUserMuteAudio(uid, muted);
            AgoraRtcEngine manager = mReference.get();
            if (manager != null) {
                if (muted) {
                    Message message = manager.handler.obtainMessage();
                    message.what = 2;
                    message.obj = uid;
                    manager.handler.sendMessage(message);
                }
            }
        }

        @Override
        public void onAudioMixingFinished() {
            super.onAudioMixingFinished();
            IMNetEaseManager.get().getChatRoomEventObservable().onNext(
                    new RoomEvent().setEvent(RoomEvent.METHOD_ON_AUDIO_MIXING_FINISHED)
            );
        }

        @Override
        public void onFirstLocalAudioFrame(int elapsed) {
            super.onFirstLocalAudioFrame(elapsed);
            LogUtil.i(TAG, "onFirstLocalAudioFrame");
            IMNetEaseManager.get().getChatRoomEventObservable()
                    .onNext(new RoomEvent()
                            .setEvent(RoomEvent.CODE_FIRST_FRAME));
        }

        @Override
        public void onFirstRemoteAudioFrame(int uid, int elapsed) {
            super.onFirstRemoteAudioFrame(uid, elapsed);
            LogUtil.i(TAG, "onFirstRemoteAudioFrame --uid:" + uid);
            IMNetEaseManager.get().getChatRoomEventObservable()
                    .onNext(new RoomEvent()
                            .setEvent(RoomEvent.CODE_FIRST_FRAME));
        }

        @Override
        public void onFirstLocalVideoFrame(int width, int height, int elapsed) {
            super.onFirstLocalVideoFrame(width, height, elapsed);
            LogUtil.i(TAG, "onFirstLocalVideoFrame");
            IMNetEaseManager.get().getChatRoomEventObservable()
                    .onNext(new RoomEvent()
                            .setEvent(RoomEvent.CODE_FIRST_FRAME));
        }

        @Override
        public void onFirstRemoteVideoFrame(int uid, int width, int height, int elapsed) {
            LogUtil.i(TAG, "onFirstRemoteVideoFrame --uid:" + uid);
            super.onFirstRemoteVideoFrame(uid, width, height, elapsed);
            IMNetEaseManager.get().getChatRoomEventObservable()
                    .onNext(new RoomEvent()
                            .setEvent(RoomEvent.CODE_FIRST_FRAME));
        }

        @Override
        public void onUserJoined(int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);

        }

        @Override
        public void onUserOffline(int uid, int reason) {
            super.onUserOffline(uid, reason);
        }

        @Override
        public void onClientRoleChanged(int oldRole, int newRole) {
            super.onClientRoleChanged(oldRole, newRole);
            LogUtil.i(TAG, "onClientRoleChanged");
            AgoraRtcEngine manager = mReference.get();
            if (manager != null) {
                Message message = manager.handler.obtainMessage();
                message.what = MSG_ROLE_CHANGED;
                message.arg1 = newRole;
                manager.handler.sendMessage(message);
            }

        }
    }

    //音乐播放相关---------------begin--------------------------

    @Override
    public void adjustAudioMixingVolume(int volume) {
        if (mRtcEngine != null) {
            mRtcEngine.adjustAudioMixingVolume(volume);
        }
    }

    @Override
    public void adjustRecordingSignalVolume(int volume) {
        if (mRtcEngine != null) {
            mRtcEngine.adjustRecordingSignalVolume(volume);
        }
    }

    @Override
    public void resumeAudioMixing() {
        if (mRtcEngine != null) {
            mRtcEngine.resumeAudioMixing();
        }
    }

    @Override
    public void pauseAudioMixing() {
        if (mRtcEngine != null) {
            mRtcEngine.pauseAudioMixing();
        }
    }

    /**
     * 获取当前播放进度
     */
    @Override
    public long getAudioMixingCurrentPosition() {
        if (mRtcEngine != null) {
            return mRtcEngine.getAudioMixingCurrentPosition();
        }
        return 0;
    }

    /**
     * 获取整个文件的播放时间
     */
    @Override
    public long getAudioMixingDuration() {
        if (mRtcEngine != null) {
            return mRtcEngine.getAudioMixingDuration();
        }
        return 0;
    }

    @Override
    public int startAudioMixing(String filePath, boolean loopback, int cycle) {
        if (mRtcEngine != null) {
            mRtcEngine.stopAudioMixing();
            int result;
            try {
                result = mRtcEngine.startAudioMixing(filePath, false, false, 1);
            } catch (Exception e) {
                return -1;
            }
            return result;
        }
        return -1;
    }
    //音乐播放相关---------------end--------------------------

    @Override
    public void setupLocalVideo(VideoCanvas videoCanvas) {
        if (mRtcEngine != null) {
            mRtcEngine.setupLocalVideo(videoCanvas);
        }
    }

    @Override
    public void setupRemoteVideo(VideoCanvas videoCanvas) {
        if (mRtcEngine != null) {
            mRtcEngine.setupRemoteVideo(videoCanvas);
        }
    }

    public void startPreview(boolean startPreview) {
        LogUtil.i(TAG, "startPreview: " + startPreview);
        if (startPreview) {
            mRtcEngine.startPreview();
        } else {
            mRtcEngine.stopPreview();
        }
    }

    private void initTranscoding(int width, int height, int bit, int rate) {
        if (mLiveTranscoding == null) {
            mLiveTranscoding = new LiveTranscoding();
            mLiveTranscoding.height = height;
            mLiveTranscoding.videoBitrate = bit;
            mLiveTranscoding.width = width;
            mLiveTranscoding.videoFramerate = rate;
        }
    }

    /**
     * 旁路推流  更新合流成员
     * <p>
     * 上下麦
     */
    @Override
    public void updateTranscodingUsers() {
        ArrayList<LiveTranscoding.TranscodingUser> transcodingUsers;

        ArrayList<UserInfo> videoUsers = getAllVideoUser();
        transcodingUsers = cdnLayout(videoUsers, mLiveTranscoding.width, mLiveTranscoding.height);
        mLiveTranscoding.setUsers(transcodingUsers);
        mRtcEngine.setLiveTranscoding(mLiveTranscoding);
    }

    private ArrayList<UserInfo> getAllVideoUser() {
        ArrayList<UserInfo> users = new ArrayList<>();
        UserInfo user = new UserInfo();
        com.tongdaxing.xchat_core.user.bean.UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            user.uid = (int) userInfo.getUid();
            users.add(user);
        }
        return users;
    }

    private ArrayList<LiveTranscoding.TranscodingUser> cdnLayout(ArrayList<UserInfo> publishers, int canvasWidth, int canvasHeight) {
        ArrayList<LiveTranscoding.TranscodingUser> users = new ArrayList<>(publishers.size());
        int index = 0;
        float xIndex, yIndex;
        int viewWidth;
        int viewHEdge;

        if (publishers.size() <= 1)
            viewWidth = canvasWidth;
        else
            viewWidth = canvasWidth / 2;

        if (publishers.size() <= 2)
            viewHEdge = canvasHeight;
        else
            viewHEdge = canvasHeight / ((publishers.size() - 1) / 2 + 1);

        for (UserInfo entry : publishers) {
            xIndex = index % 2;
            yIndex = index / 2;
            LiveTranscoding.TranscodingUser tmpUser = new LiveTranscoding.TranscodingUser();
            tmpUser.uid = entry.uid;
            tmpUser.x = (int) ((xIndex) * viewWidth);
            tmpUser.y = (int) (viewHEdge * (yIndex));
            tmpUser.width = viewWidth;
            tmpUser.height = viewHEdge;
            tmpUser.zOrder = 0;
            tmpUser.audioChannel = 0;
            tmpUser.alpha = 1f;

            users.add(tmpUser);
            index++;
        }

        return users;
    }
}