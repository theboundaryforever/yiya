package com.tongdaxing.xchat_core.manager;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.Env;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_framework.BuildConfig;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.im.ICommonListener;
import com.tongdaxing.xchat_framework.im.IConnectListener;
import com.tongdaxing.xchat_framework.im.IMCallBack;
import com.tongdaxing.xchat_framework.im.IMError;
import com.tongdaxing.xchat_framework.im.IMErrorBean;
import com.tongdaxing.xchat_framework.im.IMKey;
import com.tongdaxing.xchat_framework.im.IMModelFactory;
import com.tongdaxing.xchat_framework.im.IMProCallBack;
import com.tongdaxing.xchat_framework.im.IMReportBean;
import com.tongdaxing.xchat_framework.im.IMSendRoute;
import com.tongdaxing.xchat_framework.im.SocketManager;
import com.tongdaxing.xchat_framework.util.util.Json;
import com.tongdaxing.xchat_framework.util.util.LogUtils;
import com.tongdaxing.xchat_framework.util.util.StringUtils;
import com.tongdaxing.xchat_framework.util.util.json.JsonParser;

import org.java_websocket.handshake.ServerHandshake;

import java.net.Proxy;
import java.net.URISyntaxException;
import java.util.List;

/**
 *
 */
public class ReUsedSocketManager {

    public static String TAG = "ReUsedSocketManager";
    private ICommonListener iCommonListener = null;
    private SocketManager socketManager = null;
    private static ReUsedSocketManager reUsedSocketManager = null;
    //发送
    public static final int SEND_ERROR = 0;
    private IConnectListener connectListener;
    private SocketManager.IMNoticeMsgListener imNoticeMsgListener;

    private static String RELEASE_WS_URL = "wss://im.lyy18.cn/";
    private static String RELEASE_18_WS_URL = "wss://pre.lyy18.cn:8819/";
    private static String DEBUG_WS_URL = "ws://beta.shenghong886.com:3006/";

    private static String socketUrl;

    private Proxy proxy = Proxy.NO_PROXY;


    public static void initIMSocket(Env.UriSetting uriSetting) {
        if (uriSetting == Env.UriSetting.Release) {//生产环境地址
            initReleaseSocket();
        } else if (uriSetting == Env.UriSetting.Test) {//测试环境地址
            initTestSocket();
        }
    }

    public static void initTestSocket() {
        socketUrl = DEBUG_WS_URL;
    }

    public static void initReleaseSocket() {
        if (BuildConfig.IS_RELEASE) {//正式服
            socketUrl = RELEASE_WS_URL;
        } else {//18服（预发布）
            socketUrl = RELEASE_18_WS_URL;
        }
    }

    private void imLogin(final long uid, final String ticket) {
        LogUtil.i(AvRoomDataManager.TAG, "imLogin ---> uid = " + uid);
        IMProCallBack imProCallBack = new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                int errno = imReportBean.getReportData().errno;
                LogUtil.i(AvRoomDataManager.TAG, "imLogin ---> errno = " + errno);
                if (errno != 0) {
                    if (!ReUsedSocketManager.get().isConnect()) {
                        ReUsedSocketManager.get().connect(uid, connectListener, 3000); //登录失败而且socket断开状态重连
                    } else {
                        if (errno != IMError.IM_ERROR_LOGIN_AUTH_FAIL && errno != IMError.IM_ERROR_GET_USER_INFO_FAIL) {
                            imLogin(uid, ticket);
                        }
                        if (imNoticeMsgListener != null)
                            imNoticeMsgListener.onLoginError(errno, imReportBean.getReportData().errmsg);
                    }
                } else {
                    if (imNoticeMsgListener != null) {
                        imNoticeMsgListener.onDisConntectIMLoginSuc();
                    }
                    onImLoginRenewEnterRoom();
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                LogUtil.i(AvRoomDataManager.TAG, "imLogin ---> errorCode = " + errorCode);
            }
        };
        ReUsedSocketManager.get().send(IMModelFactory.get().createLoginModel(ticket, String.valueOf(uid)), imProCallBack);
    }

    /**
     * im登录之后如果再房间，需要重新进入房间
     */
    private void onImLoginRenewEnterRoom() {
        LogUtil.i(AvRoomDataManager.TAG, "onImLogin ---> enterRoom");
        if (AvRoomDataManager.get().getRoomInfo() != null) {
            long roomUid = AvRoomDataManager.get().getRoomInfo().getUid();
            int roomType = AvRoomDataManager.get().getRoomInfo().getType();
            int reconnect = 1;
            boolean isCanConnectMic = AvRoomDataManager.get().getIsCanConnectMic();
            ReUsedSocketManager.get().enterWithOpenChatRoom(roomUid, roomType, null, reconnect, null, null, isCanConnectMic, new IMProCallBack() {
                @Override
                public void onSuccessPro(IMReportBean imReportBean) {
                    LogUtil.i(AvRoomDataManager.TAG, "onImLogin ---> onReconnection");
                    if (imNoticeMsgListener != null)
                        imNoticeMsgListener.onDisConnectEnterRoomSuc();
                }

                @Override
                public void onError(int errorCode, String errorMsg) {
                    LogUtil.i(AvRoomDataManager.TAG, "onImLogin ---> enterRoom error " + errorMsg);
                    if (imNoticeMsgListener != null) {
                        imNoticeMsgListener.onLoginError(errorCode, errorMsg);
                    }
                }
            });
        }
    }

    private void setServerMicInfo(String room_info, String member, List<Json> queueList) throws IllegalArgumentException {
        Gson gson = new Gson();
        RoomInfo extRoomInfo = gson.fromJson(room_info, RoomInfo.class);
        IMChatRoomMember chatRoomMember = gson.fromJson(member, IMChatRoomMember.class);
        if (chatRoomMember != null)
            AvRoomDataManager.get().setMOwnerMember(chatRoomMember);
        //同步房间信息
        if (extRoomInfo != null) {
            AvRoomDataManager.get().setRoomInfo(extRoomInfo);
        } else {
            throw new IllegalArgumentException("roomInfo must not null");
        }
        // 发送房间玩法，此时才有房间信息
        IMNetEaseManager.get().sendRoomRulesMessage();
        //同步麦序
        for (int i = 0; i < queueList.size(); i++) {
            Json json = queueList.get(i);
            int key = json.num("key");
            Json value = json.json_ok("value");
            IMChatRoomMember imChatRoomMember = null;
            if (value.has("member")) {
                imChatRoomMember = gson.fromJson(value.str("member"), IMChatRoomMember.class);
            }
            RoomMicInfo micInfo = gson.fromJson(value.str("mic_info"), RoomMicInfo.class);
            int onMicType = gson.fromJson(value.str("on_mic_type"), int.class);
            RoomQueueInfo roomQueueInfo = new RoomQueueInfo(micInfo, imChatRoomMember, onMicType);
            AvRoomDataManager.get().mMicQueueMemberMap.put(key, roomQueueInfo);
        }
    }


    //-------------------------------------------对外开放的方法-------------------------------------------------------------------


    public static ReUsedSocketManager get() {
        if (reUsedSocketManager == null) {
            synchronized (ReUsedSocketManager.class) {
                if (reUsedSocketManager == null) {
                    reUsedSocketManager = new ReUsedSocketManager();
                }
            }
        }
        return reUsedSocketManager;
    }


    /**
     * 注册断开回调函数  告诉业务层断开( code 告诉我们时超时导致还是网络导致断开还是手动断开)
     * 注册服务器单向推消息处理回调
     *
     * @param iCommonListener
     */
    public void setiCommonListener(ICommonListener iCommonListener) {
        this.iCommonListener = iCommonListener;
        if (iCommonListener != null && socketManager != null)
            socketManager.setiCommonListener(iCommonListener);
    }

    /**
     * 手动断开链接
     */
    public void disconnect() {
        if (socketManager != null) socketManager.disconnect();
    }

    public void destroy() {
        if (socketManager != null) socketManager.destroy();
    }


    /**
     * @return 链接状态
     */
    public boolean isConnect() {
        return socketManager != null && socketManager.isConnect();

    }


    /**
     * 链接socket
     */
    private void connect(long uid, IConnectListener iConnectListener, int delay) {
        if (socketManager != null)
            socketManager.destroy();
        socketManager = new SocketManager();

//        String socketUrl = BasicConfig.isDebug ? DEBUG_WS_URL : RELEASE_WS_URL + "?uid=" + uid;
        String socketUrl = this.socketUrl + "?uid=" + uid;
        LogUtil.i(TAG, "socketUrl: " + socketUrl);
        try {
            socketManager.setupSocketUri(socketUrl);
            socketManager.setProxy(proxy);//设置代理要在connect之前
            if (iCommonListener != null) socketManager.setiCommonListener(iCommonListener);
            socketManager.connect(iConnectListener, delay);
        } catch (URISyntaxException e) {
            if (iConnectListener != null) {
                iConnectListener.onError(e);
            }
        }
    }

    public void setImNoticeMsgListener(SocketManager.IMNoticeMsgListener imNoticeMsgListener) {
        this.imNoticeMsgListener = imNoticeMsgListener;

    }

    /**
     * 对服务器发送消息
     *
     * @param content    发送内容
     * @param imCallBack 结果的回调
     */
    public void send(Json content, @NonNull IMCallBack imCallBack) {
        LogUtils.d("request_info_im_send", content.toString());
        if (socketManager != null) {
            socketManager.send(content.toString(), imCallBack);
        } else {
            imCallBack.onError(-1, IMError.IM_MS_SEND_ERROR);
        }
    }


    /**
     * 登录回调之后初始化im，并登录
     */
    public void initIM() {
        final long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        final String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        final int reConnectTIme = 3000;
        connectListener = new IConnectListener() {
            @Override
            public void onSuccess(ServerHandshake serverHandshake) {
                LogUtil.i(AvRoomDataManager.TAG, "initIM ---> onOpen ---> HttpStatus = " + serverHandshake.getHttpStatus() + " HttpStatusMessage = " + serverHandshake.getHttpStatusMessage());
                imLogin(uid, ticket);
            }

            @Override
            public void onError(Exception e) {
                LogUtil.i(AvRoomDataManager.TAG, "initIM ---> onError ---> Exception = " + e.getMessage());
                LogUtil.i(AvRoomDataManager.TAG, e.getMessage());
                if (e.getMessage() == "Dubble connect!")
                    return;
                ReUsedSocketManager.get().connect(uid, connectListener, reConnectTIme); //断线重连延迟3秒执行
            }
        };
        connect(uid, connectListener, 0);
        setiCommonListener(new ICommonListener() {
            @Override
            public void onDisconnectCallBack(IMErrorBean err) {
                boolean isCloseSelf = err.getCloseReason() == SocketManager.CALL_BACK_CODE_SELFCLOSE;
                LogUtil.i(AvRoomDataManager.TAG, "initIM ---> onDisconnectCallBack ---> isCloseSelf = " + isCloseSelf + " err_code = " + err.getCode() + " reason = " + err.getReason());
                if (!isCloseSelf) { // 非手动关闭自动重连
                    ReUsedSocketManager.get().connect(uid, connectListener, reConnectTIme); // 断线重连延迟执行
                }
                if (imNoticeMsgListener != null)
                    imNoticeMsgListener.onDisConnection(isCloseSelf);

            }

            @Override
            public void onNoticeMessage(String message) {
                if (imNoticeMsgListener != null)
                    imNoticeMsgListener.onNotice(Json.parse(message));
            }
        });
    }

    public void enterWithOpenChatRoom(long roomUid, int roomType, String roomPwd, int reconnect, String avatar, String title, boolean canConnectMic, final IMProCallBack imProCallBack) {
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        ReUsedSocketManager.get().send(IMModelFactory.get().enterWithOpenChatRoom(roomUid, roomType, roomPwd, reconnect, avatar, title, canConnectMic, ticket), new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                IMNetEaseManager.get().setImRoomConnection(true);
                IMReportBean.ReportData reportData = imReportBean.getReportData();
                if (reportData.errno != 0) {
                    LogUtil.i(AvRoomDataManager.TAG, "enterRoom ---> send ---> onError --- > errno = " + reportData.errno);
                    if (imProCallBack != null) {
                        imProCallBack.onError(reportData.errno, reportData.errmsg);
                    }
                    return;
                }
                Json data = reportData.data;
                String room_info = data.str("room_info");
                String member = data.str("member");
                int pushCdn = data.num("push_cdn", 0);
                if (pushCdn == 1) {
                    //旁路推送获取cnd_url
                    List<String> pushRtmpList = data.sList("pushRtmpList");
                    if (!ListUtils.isListEmpty(pushRtmpList)) {
                        AvRoomDataManager.get().setCndUrl(pushRtmpList.get(0));
                        AvRoomDataManager.get().setEnterTimeStamp(System.currentTimeMillis());
                    }
                }

                List<Json> queueList = data.jlist("queue_list");
                //可能会解析错误
                try {
                    setServerMicInfo(room_info, member, queueList);
                } catch (Exception e) {
                    if (imProCallBack != null) {
                        imProCallBack.onError(-1101, e != null ? ("setServerMicInfo fail:" + e.getMessage()) : "setServerMicInfo fail");
                    }
                    return;

                }
                LogUtil.i(AvRoomDataManager.TAG, "enterRoom ---> send ---> onSuccessPro");
                if (imProCallBack != null) {
                    imProCallBack.onSuccessPro(imReportBean);
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                LogUtil.i(AvRoomDataManager.TAG, "enterRoom ---> send ---> onError ---> errorCode = " + errorCode);
                com.juxiao.library_utils.log.LogUtil.i(errorCode + ":" + errorMsg);
                if (imProCallBack != null) {
                    imProCallBack.onError(errorCode, errorMsg);
                }
            }
        });
    }

    /**
     * 更新麦序操作：加入队列  如果已经其他麦序位置上，会自动更新位置
     *
     * @param roomId
     * @param micPosition
     * @param uid
     * @param imCallBack
     */
    public void updateQueue(String roomId, int micPosition, long uid, final IMCallBack imCallBack) {
        LogUtil.i(AvRoomDataManager.TAG, "updateQueue ---> roomId = " + roomId + " --- micPosition = " + micPosition + " --- uid = " + uid);
        ReUsedSocketManager.get().send(IMModelFactory.get().createUpdateQueue(roomId, micPosition, uid), imCallBack);
    }

    /**
     * 退出房间
     *
     * @param roomId
     * @param imProCallBack
     */
    public void exitRoom(long roomId, IMProCallBack imProCallBack) {
        LogUtil.i(AvRoomDataManager.TAG, "exitRoom ---> roomId = " + roomId);
        send(IMModelFactory.get().createExitRoom(roomId), imProCallBack);
    }


    /**
     * 退出公聊房间
     *
     * @param roomId
     * @param imProCallBack
     */
    public void exitPublicRoom(long roomId, IMProCallBack imProCallBack) {
        LogUtil.i(AvRoomDataManager.TAG, "exitRoom ---> roomId = " + roomId);
        send(IMModelFactory.get().createExitPublicRoom(roomId), imProCallBack);
    }

    /**
     * 更新麦序操作：加入队列  如果已经其他麦序位置上，会自动更新位置
     *
     * @param roomId
     * @param micPosition
     * @param imSendCallBack
     */
    public void pollQueue(String roomId, int micPosition, final IMSendCallBack imSendCallBack) {
        LogUtil.i(AvRoomDataManager.TAG, "updateQueue ---> roomId = " + roomId + " --- micPosition = " + micPosition);
        send(IMModelFactory.get().createPollQueue(roomId, micPosition), imSendCallBack);
    }


    /**
     * 发送文本消息
     *
     * @param roomId
     * @param message
     * @param imSendCallBack
     */
    public void sendTxtMessage(String roomId, ChatRoomMessage message, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set(IMKey.member, new Gson().toJson(message.getImChatRoomMember()));
        if (StringUtils.isNotEmpty(message.getContent()))
            json.set(IMKey.content, message.getContent());
        send(IMModelFactory.get().createRequestData(IMSendRoute.sendText, json), imSendCallBack);
    }


    /**
     * 发送自定义消息
     *
     * @param roomId
     * @param message
     * @param imSendCallBack
     */
    public void sendCustomMessage(String roomId, ChatRoomMessage message, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set(IMKey.custom, message.getAttachment().toJson(true));
        if (message.getImChatRoomMember() != null)
            json.set(IMKey.member, JsonParser.toJson(message.getImChatRoomMember()));
        send(IMModelFactory.get().createRequestData(IMSendRoute.sendMessage, json), imSendCallBack);
    }

    /**
     * 发送自定义消息
     *
     * @param roomId
     * @param message
     * @param imSendCallBack
     */
    public void sendPublicMessage(String roomId, ChatRoomMessage message, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        json.set(IMKey.custom, message.getAttachment().toJson());
        if (message.getImChatRoomMember() != null)
            json.set(IMKey.member, message.getImChatRoomMember().toJson());
        send(IMModelFactory.get().createRequestData(IMSendRoute.sendPublicMsg, json), imSendCallBack);
    }

    /**
     * 进入公聊大厅
     *
     * @param roomId
     * @param imSendCallBack
     */
    public void enterChatHallMessage(String roomId, IMCallBack imSendCallBack) {
        Json json = new Json();
        json.set("room_id", roomId);
        send(IMModelFactory.get().createRequestData(IMSendRoute.enterPublicRoom, json), imSendCallBack);
    }

    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }
}