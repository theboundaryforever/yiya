package com.tongdaxing.xchat_core.manager.log;

import lombok.Data;

/**
 * @author Zhangsongzhou
 * @date 2019/5/30
 */
@Data
public class LogEvent {

    /**
     * 上传日志所需要制定的天数
     */
    public static final int DAY_TO_DAY = 1; //当天
    public static final int DAY_ONE_DAY_BEFORE = 2;
    public static final int DAY_TWO_DAY_BEFORE = 3;


    private int mDay = DAY_TO_DAY;

    public int getDay() {
        return mDay;
    }

    public LogEvent setDay(int mDay) {
        this.mDay = mDay;
        return this;
    }
}
