package com.tongdaxing.xchat_core.utils;

import android.os.Build;

import com.tongdaxing.xchat_framework.http_image.http.DefaultRequestParam;
import com.tongdaxing.xchat_framework.http_image.http.RequestParam;
import com.tongdaxing.xchat_framework.http_image.http.ResponseData;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.AppMetaDataUtil;
import com.tongdaxing.xchat_framework.util.util.NetworkUtils;
import com.tongdaxing.xchat_framework.util.util.TelephonyUtils;
import com.tongdaxing.xchat_framework.util.util.VersionUtil;
import com.tongdaxing.xchat_framework.util.util.log.MLog;


public class CommonParamUtil {

	public static RequestParam fillCommonParam() {
        DefaultRequestParam param = new DefaultRequestParam();
		try {
			param.put("os", "android");
			param.put("osVersion", Build.VERSION.RELEASE);
			param.put("XchatVersion", VersionUtil.getLocalVer(BasicConfig.INSTANCE.getAppContext()).getVersionNameWithoutSnapshot());
			param.put("ispType", String.valueOf(getIspType()));
			param.put("netType", String.valueOf(getNetworkType()));
			param.put("model", getPhoneModel());
			param.put("channel", AppMetaDataUtil.getChannelID(BasicConfig.INSTANCE.getAppContext()));
//			param.put("uid", String.valueOf(CoreManager.getAuthCore().getUserId()));
			param.put("imei", TelephonyUtils.getImei(BasicConfig.INSTANCE.getAppContext()));
		} catch (Throwable e) {
			MLog.error("CommonParamUtil", "fillCommonParam", e);
		}
		return param;
    }

    /**
     * 获取是否nettype字段
     *
     * @return
     */
	public static int getNetworkType() {
        if (NetworkUtils.getNetworkType(BasicConfig.INSTANCE.getAppContext()) == NetworkUtils.NET_WIFI) {
            return 2;
        } else {
            return 1;
        }
    }

    /**
     * 获取运营商字段
     *
     * @return
     */
	public static int getIspType() {
        String isp = NetworkUtils.getOperator(BasicConfig.INSTANCE.getAppContext());
        int ispType = 4;
        if (isp.equals(NetworkUtils.ChinaOperator.CMCC)) {
            ispType = 1;
        } else if (isp.equals(NetworkUtils.ChinaOperator.UNICOM)) {
            ispType = 2;
        } else if (isp.equals(NetworkUtils.ChinaOperator.CTL)) {
            ispType = 3;
        }

        return ispType;
    }

    /**
     * 获取手机型号
     *
     * @return
     */
	public static String getPhoneModel() {
        return Build.MODEL;
    }


    public static boolean isGameVoiceResponse(ResponseData response) {
        //默认是手有语音业务服务器返回结果
        if (response == null) {
            return true;
        }
        if (response.headers == null) {
            return false;
        }
        return GAME_VOICE_VAL.equals(response.headers.get(GAME_VOICE_KEY));
    }

    private static final String GAME_VOICE_KEY = "mgvoice";
    private static final String GAME_VOICE_VAL = "mgvoice";
}
