package com.tongdaxing.xchat_core.bills;

import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * Created by Seven on 2017/9/9.
 */

public interface IBillsCore extends IBaseCore {

    /*传12356*/
//    void getExpendBills(int pageNo,int pageSize,long time);

    void getGiftIncomeBills(int pageNo, int pageSize, long time);

    /**
     * @param pageNo
     * @param pageSize
     * @param time
     * @param coinType 1:钻石 ；2: 咿呀 （默认是钻石）
     */
    void getGiftIncomeBills(int pageNo, int pageSize, long time, int coinType);

    void getGiftExpendBills(int pageNo, int pageSize, long time);

    /**
     * @param pageNo
     * @param pageSize
     * @param time
     * @param coinType 1:钻石 ；2: 咿呀 （默认是钻石）
     */
    void getGiftExpendBills(int pageNo, int pageSize, long time, int coinType);

    void getWithdrawBills(int pageNo, int pageSize, long time);

    void getWithdrawBills(int pageNo, int pageSize, long time, int coinType);

    /**
     * 获取账单红包
     */
    void getWithdrawRedBills(int pageNo, int pageSize, long time);

    void getChatBills(int pageNo, int pageSize, long time);

    //    void getOrderExpendBills(int pageNo,int pageSize,long time);
    void getChargeBills(int pageNo, int pageSize, long time);

    /**
     * 获取红包记录列表
     */
    void getRedBagBills(int pageNo, int pageSize, long time);

//    void getIncomBills(int pageNo,int pageSize);
}
