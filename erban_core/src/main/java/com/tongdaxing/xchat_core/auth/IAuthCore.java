package com.tongdaxing.xchat_core.auth;

import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;
import com.tongdaxing.xchat_framework.util.util.Json;

/**
 * Created by chenran on 2017/2/13.
 */

public interface IAuthCore extends IBaseCore {
    AccountInfo getCurrentAccount();

    ThirdUserInfo getThirdUserInfo();

    void setThirdUserInfo(ThirdUserInfo thirdUserInfo);

    long getCurrentUid();

    String getTicket();

    boolean isLogin();

    void register(String validateStr, String phone, String sms_code, String password);

    void login(String account, String password, String validateStr);

    void autoLogin();

    void logout();

    void requestTicket();

    void requestSMSCode(String phone, int type);// Type取值：1注册短信；2登录短信；3找回或者修改密码短信  41.5.0版本登录

    void requestResetPsw(String phone, String sms_code, String newPsw);

    void wxLogin(String validateStr);

    void ThirdLogin(String validateStr, String openid, String unionid, String access_token, int type);

    void qqLogin(String validateStr);

    void isPhone(long uid);

    void binderPhone(String phone, String code);

    void ModifyBinderPhone(String phone, String code, String url);


    void getSMSCode(String phone);

    void getModifyPhoneSMSCode(String phone, String type);

    void accLogout(String access_token, HttpRequestCallBack<Json> callBack);

    void phoneLogin(String phone, String smsCode);
}
