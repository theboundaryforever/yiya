package com.tongdaxing.xchat_core;

import android.text.TextUtils;

import com.tongdaxing.xchat_framework.BuildConfig;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.pref.CommonPref;

/**
 * 请求接口接口地址
 */
public class UriProvider {

    public static void init(Env.UriSetting uriSetting) {
        if (uriSetting == Env.UriSetting.Release) {
            //生产环境地址
            initWebReleaseUri();
        } else if (uriSetting == Env.UriSetting.Test) {
            //测试环境地址
            initWebTestUri();
        }
    }

    public static void initDevUri() {
        JAVA_WEB_URL = "http://beta.shenghong886.com"; //域名：shenghong886.com
        IM_SERVER_URL = "http://beta.shenghong886.com";
    }

    public static void initWebReleaseUri() {
        if (BuildConfig.IS_RELEASE) {//正式服
            JAVA_WEB_URL = "https://www.lyy18.cn";
            IM_SERVER_URL = "https://www.lyy18.cn";
        } else {//18服（预发布）
            JAVA_WEB_URL = "https://pre.lyy18.cn";
            IM_SERVER_URL = "https://pre.lyy18.cn";
        }
    }

    public static String checkUpdata() {
        return IM_SERVER_URL.concat("/version/get");
    }

    private static String DEBUG_URL;

    public static void initDevUri(String url) {
        DEBUG_URL = url;
        JAVA_WEB_URL = url;
        IM_SERVER_URL = url;
    }

    private static void initWebTestUri() {
        int enviroment = CommonPref.instance(BasicConfig.INSTANCE.getAppContext()).getInt("enviroment");
        if (enviroment == 0) {
            initWebReleaseUri();//其他使用DEV的配置，就神曲这个跟新一下
        } else {
            if (!TextUtils.isEmpty(DEBUG_URL)) {
                initDevUri(DEBUG_URL);
            } else {
                initDevUri();
            }
        }
    }

    /**
     * 默认以下生产环境地址
     */
    public static String JAVA_WEB_URL = "https://www.lyy18.cn";
    public static String IM_SERVER_URL = "https://www.lyy18.cn";

    public static String HOME_RANKING_URL = "/mm/rank/index.html";

    public static String getGroupResourceBaseUri() {
        return JAVA_WEB_URL.concat("/app/service/resource/list");
    }

    /**
     * 注册接口
     */
    public static String getRegisterResourceUrl() {
        return IM_SERVER_URL.concat("/acc/signup");
    }


    /**
     * 1.5.0 版本登录入口
     *
     * @return
     */
    public static String getAccPhoneLoginUrl() {
        return IM_SERVER_URL.concat("/acc/phone/login");
    }


    /**
     * 登录接口
     */
    public static String getLoginResourceUrl() {
        return IM_SERVER_URL.concat("/oauth/token");
    }

    public static String getAccLogoutUrl() {
        return IM_SERVER_URL.concat("/acc/logout");
    }

    /**
     * 获取ticket
     */
    public static String getAuthTicket() {
        return IM_SERVER_URL.concat("/oauth/ticket");
    }

    /**
     * 获取短信验证码
     */
    public static String getSMSCode() {
        return IM_SERVER_URL.concat("/acc/sms");
    }

    /**
     * 找回，修改密码
     */
    public static String modifyPsw() {
        return IM_SERVER_URL.concat("/acc/pwd/reset");
    }

    /**
     * 登出
     */
    public static String logout() {
        return IM_SERVER_URL.concat("/acc/logout");
    }

    /**
     * 等级
     */
    public static String levelCharmGet() {
        return IM_SERVER_URL.concat("/level/charm/get");
    }

    /**
     * 金币
     */
    public static String levelExeperienceGet() {
        return IM_SERVER_URL.concat("/level/exeperience/get");
    }

    public static String getUserInfo() {
        return IM_SERVER_URL.concat("/user/v3/get");
    }

    public static String getUserInfoListUrl() {
        return IM_SERVER_URL.concat("/user/list");
    }


    public static String updateUserInfo() {
        return IM_SERVER_URL.concat("/user/update");
    }

    public static String addPhoto() {
        return IM_SERVER_URL.concat("/photo/upload");
    }

    public static String deletePhoto() {
        return IM_SERVER_URL.concat("/photo/delPhoto");
    }

    public static String giftWall() {
        return IM_SERVER_URL.concat("/giftwall/get");
    }

    //获取用户收到的神秘礼物列表
    public static String getMysteryGiftWall() {
        return IM_SERVER_URL.concat("/giftwall/listMystic");
    }

    public static String praise() {
        return IM_SERVER_URL.concat("/fans/like");
    }

    public static String deleteLike() {
        return IM_SERVER_URL.concat("/fans/fdelete");
    }

    public static String isLike() {
        return IM_SERVER_URL.concat("/fans/islike");
    }

    public static String getCheckUserFansAndBlack() {
        return IM_SERVER_URL.concat("/user/checkUserFansAndBlack");
    }

    public static String searchUserInfo() {
        return IM_SERVER_URL.concat("/search/user");
    }

    public static String getAllFans() {
        return IM_SERVER_URL.concat("/fans/following");

    }

    public static String getFansList() {
        return IM_SERVER_URL.concat("/fans/fanslist");

    }

    public static String getUserRoom() {
        return IM_SERVER_URL.concat("/userroom/get");
    }

    public static String userRoomIn() {
        return IM_SERVER_URL.concat("/userroom/in");
    }

    public static String userRoomOut() {
        return IM_SERVER_URL.concat("/userroom/out");
    }

    /**
     * 房间标签列表
     */
    public static String getRoomTagList() {
        return IM_SERVER_URL.concat("/room/tag/all");
    }

    public static String openRoom() {
        return IM_SERVER_URL.concat("/room/open");
    }

    public static String getRoomInfo() {
        return IM_SERVER_URL.concat("/room/get");
    }

    /**
     * userroom/get 获取房间信息
     */
    public static String getUserRoomInfo() {
        return IM_SERVER_URL.concat("/userroom/get");
    }

    /**
     * 房间时间统计
     *
     * @return
     */
    public static String roomStatistics() {
        return IM_SERVER_URL.concat("/basicusers/v3/record");
    }


    public static String updateRoomInfo() {
        return IM_SERVER_URL.concat("/room/update");
    }

    public static String updateByAdimin() {
        return IM_SERVER_URL.concat("/room/updateByAdmin");
    }

    public static String closeRoom() {
        return IM_SERVER_URL.concat("/room/close");
    }

    public static String getRoomConsumeList() {
        return IM_SERVER_URL.concat("/roomctrb/query");
    }

    public static String roomSearch() {
        return IM_SERVER_URL.concat("/search/room");
    }

    public static String getAuctionInfo() {
        return IM_SERVER_URL.concat("/auction/get");
    }

    public static String auctionStart() {
        return IM_SERVER_URL.concat("/auction/start");
    }

    //声网动态的appkey获取
    public static String getRoomAgoraKey() {
        return IM_SERVER_URL.concat("/agora/getKey");
    }

    /**
     * 用户参与竞拍报价
     */
    public static String auctionUp() {
        return IM_SERVER_URL.concat("/auctrival/up");
    }

    /**
     * 房主结束竞拍
     */
    public static String finishAuction() {
        return IM_SERVER_URL.concat("/auction/finish");
    }

    public static String weekAucionList() {
        return IM_SERVER_URL.concat("/weeklist/query");
    }

    public static String totalAuctionList() {
        return IM_SERVER_URL.concat("/sumlist/query");
    }

    /**
     * 获取订单列表
     */
    public static String getOrderList() {
        return IM_SERVER_URL.concat("/order/list");
    }

    /**
     * 完成订单
     */
    public static String finishOrder() {
        return IM_SERVER_URL.concat("/order/finish");
    }


    /**
     * 获取指定订单
     */
    public static String getOrder() {
        return IM_SERVER_URL.concat("/order/get");
    }

    /**
     * 获取房间列表
     */
    public static String getRoomList() {
        return IM_SERVER_URL.concat("/home/get");

    }

    /**
     * 获取房间列表
     */
    public static String getRoomListV2() {
        return IM_SERVER_URL.concat("/home/getV2");

    }

    /**
     * 获取分类标签内的房间列表
     */
    public static String getTagVideoRoom() {
        return IM_SERVER_URL.concat("/home/tagVideoRoom");

    }

    /**
     * 获取banner列表
     */
    public static String getBannerList() {
        return IM_SERVER_URL.concat("/banner/list");

    }

    /**
     * 获取礼物列表
     */
    public static String getGiftList() {
        return IM_SERVER_URL.concat("/gift/listV3");
    }

    /**
     * 获取神秘礼物列表
     */
    public static String getMysteryGiftList() {
        return IM_SERVER_URL.concat("/gift/listMystic");
    }

    /**
     * 送礼物新接口
     */
    public static String sendGiftV3() {

        return IM_SERVER_URL.concat("/gift/sendV3");
    }

    /**
     * 全麦送
     */
    public static String sendWholeGiftV3() {
        return IM_SERVER_URL.concat("/gift/sendWholeMicroV3");
    }

    /**
     * 获取表情列表
     */
    public static String getFaceList() {
        return IM_SERVER_URL.concat("/client/init");
    }

    /**
     * 客户端初始化
     *
     * @return --
     */
    public static String getInit() {
        return IM_SERVER_URL.concat("/client/init");
    }

    /**
     * 获取版本号
     */
    public static String getVersions() {
        return IM_SERVER_URL.concat("/appstore/check");
    }

    /**
     * 获取钱包信息
     */
    public static String getWalletInfos() {
        return IM_SERVER_URL.concat("/purse/query");
    }

    /**
     * 获取充值产品列表
     */
    public static String getChargeList() {
        return IM_SERVER_URL.concat("/chargeprod/list");
    }

    /**
     * 发起充值
     */
    public static String requestCharge() {
        return IM_SERVER_URL.concat("/charge/apply");
    }

    /**
     * 发起杉德充值
     */
    public static String requestChargeSD() {
        return IM_SERVER_URL.concat("/charge/sandpay/app/apply");
    }

    /**
     * 发起汇聚银行卡支付
     */
    public static String requestChargeBank() {
        return IM_SERVER_URL.concat("/charge/joinpay/app/mobilePay/apply");
    }

    /**
     * 发起汇聚充值
     */
    public static String requestChargeHJ() {
        return IM_SERVER_URL.concat("/charge/joinpay/app/apply");
    }

    /**
     * 发起汇潮充值
     */
    public static String requestChargeHC() {
        return IM_SERVER_URL.concat("/charge/ecpss/alipay/apply");
    }

    public static String requestCDKeyCharge() {
        return IM_SERVER_URL.concat("/redeemcode/use");
    }

    /**
     * 钻石兑换
     */
    public static String changeGold() {
        return IM_SERVER_URL.concat("/change/gold");
    }

    /**
     * 获取提现列表
     */
    public static String getWithdrawList() {
        return IM_SERVER_URL.concat("/withDraw/findList");
    }

    /**
     * 获取提现页用户信息
     */
    public static String getWithdrawInfo() {
        return IM_SERVER_URL.concat("/withDraw/exchange");
    }

    /**
     * 发起兑换
     */
    public static String requestExchange() {
        return IM_SERVER_URL.concat("/withDraw/withDrawCash");
    }

    /**
     * 发起兑换
     */
    public static String requestExchangeV2() {
        return IM_SERVER_URL.concat("/withDraw/v2/withDrawCash");
    }

    /**
     * 获取绑定支付宝验证码
     */
    public static String getSms() {
        return IM_SERVER_URL.concat("/withDraw/getSms");
    }

    /**
     * 获取绑定手机验证码
     */
    public static String getSmS() {
        return IM_SERVER_URL.concat("/withDraw/phoneCode");
    }


    /**
     * 检查微信提现验证码正确性
     *
     * @return
     */
    public static String checkCode() {
        return JAVA_WEB_URL.concat("/withDraw/checkCode");
    }

    /**
     * 获取绑定手机验证码
     */
    public static String getModifyPhoneSMS() {
        return IM_SERVER_URL.concat("/acc/sms");
    }

    /**
     * 绑定支付宝
     */

    public static String binder() {
        return IM_SERVER_URL.concat("/withDraw/bound");
    }

    /**
     * 绑定微信提现
     *
     * @return
     */
    public static String bindWeixinWithdraw() {
        return JAVA_WEB_URL.concat("/withDraw/boundWeixin");
    }

    /**
     * 绑定手机
     */
    public static String binderPhone() {
        return IM_SERVER_URL.concat("/withDraw/phone");
    }

    public static String modifyBinderPhone() {
        return IM_SERVER_URL.concat("/user/confirm");
    }

    public static String modifyBinderNewPhone() {
        return IM_SERVER_URL.concat("/user/replace");
    }


    /**
     * 提交反馈
     */
    public static String commitFeedback() {
        return IM_SERVER_URL.concat("/feedback");
    }

    public static String uploadLogFile() {
        return IM_SERVER_URL.concat("/client/log/upload");
    }


    /**
     * 微信登陆接口
     */
    public static String requestWXLogin() {
        return IM_SERVER_URL.concat("/acc/third/login");
    }

    /**
     * 获取order平均时长
     */
    public static String getAvgChattime() {
        return IM_SERVER_URL.concat("/basicorder/avgchattime");
    }

    /**
     * 是否绑定手机
     */
    public static String isPhones() {
        return IM_SERVER_URL.concat("/user/isBindPhone");
    }

    /**
     * 检测是否绑定过手机和设置过密码
     *
     * @return
     */
    public static String checkPwdV2() {
        return IM_SERVER_URL.concat("/user/v2/checkPwd");
    }

    /**
     * 绑定手机
     *
     * @return
     */
    public static String boundPhone() {
        return IM_SERVER_URL.concat("/user/boundPhone");
    }

    /**
     * 获取验证码
     *
     * @return
     */
    public static String getSendSms() {
        return IM_SERVER_URL.concat("/user/getSendSms");
    }

    public static String userSetPwd() {
        return IM_SERVER_URL.concat("/user/v2/setPwd");
    }


    /**
     * 检测是否绑定过手机
     */
    public static String checkPwd() {
        return IM_SERVER_URL.concat("/user/checkPwd");
    }

    /**
     * 修改登录密码
     */
    public static String modifyPwd() {
        return IM_SERVER_URL.concat("/user/modifyPwd");
    }

    /**
     * 设置登录密码
     */
    public static String setPassWord() {
        return IM_SERVER_URL.concat("/user/setPwd");
    }

    /**
     * 验证码
     */
    public static String checkSmsCode() {
        return IM_SERVER_URL.concat("/user/validateCode");
    }

    /**
     * 获取个人支出账单，包含礼物 充值 订单支出
     */
    public static String getAllBills() {
        return IM_SERVER_URL.concat("/personbill/list");
    }

    /**
     * 礼物，密聊，充值，提现账单查询（不包括红包）
     */
    public static String getBillRecord() {
        return IM_SERVER_URL.concat("/billrecord/get");
    }

    /**
     * 红包账单查询
     */
    public static String getPacketRecord() {
        return IM_SERVER_URL.concat("/packetrecord/get");
    }

    /**
     * 账单提现查询(红包提现)
     */
    public static String getPacketRecordDeposit() {
        return IM_SERVER_URL.concat("/packetrecord/deposit");
    }

    public static String getRedPacket() {
        return IM_SERVER_URL.concat("/statpacket/get");
    }

    /**
     * 新用户福利接口
     */
    public static String getNewUserWelfare() {
        return JAVA_WEB_URL.concat("/user/get/newUserGiftCart");
    }

    public static String getShareRedPacket() {
        return IM_SERVER_URL.concat("/usershare/save");
    }

    /**
     * 分享房间需要调用这个接口
     *
     * @return
     */
    public static String getShareRoom() {
        return IM_SERVER_URL.concat("/user/share/shareRoom");
    }


    /**
     * 是否第一次进入，获取红包弹窗
     */
    public static String getRedBagDialog() {
        return IM_SERVER_URL.concat("/packet/first");
    }

    /**
     * 获取红包弹窗活动类型
     */
    public static String getRedBagDialogType() {
        return IM_SERVER_URL.concat("/activity/query");
    }

    /**
     * 获取红包提现列表
     */
    public static String getRedBagList() {
        return IM_SERVER_URL.concat("/redpacket/list");
    }

    /**
     * 发起红包提现
     */
    public static String getRedWithdraw() {
        return IM_SERVER_URL.concat("/redpacket/withdraw");
    }

    /**
     * 红包提现列表
     */
    public static String getRedDrawList() {
        return IM_SERVER_URL.concat("/redpacket/drawlist");
    }

    /**
     * 首页排行列表
     */
    public static String getHomeRanking() {
        return IM_SERVER_URL.concat("/allrank/homeV2");
    }

    /**
     * 获取首页视频或语音列表
     */
    public static String getHomeList() {
        return IM_SERVER_URL.concat("/home/v2/room/list");
    }

    /**
     * 首页首页人气厅列表模块
     */
    public static String getHomePopularRoom() {
        return IM_SERVER_URL.concat("/home/v2/getindex");
    }

    /**
     * 语聊房获取排行榜前三
     *
     * @return
     */
    public static String getRank() {
        return IM_SERVER_URL.concat("/home/getRank");
    }


    /**
     * 获取首页tab数据
     */
    public static String getMainTabList() {
        return IM_SERVER_URL.concat("/room/tag/top");
    }

    /**
     * 获取首页热门数据
     */
    public static String getMainHotData() {
        return IM_SERVER_URL.concat("/home/v2/hotindex");
    }

    /**
     * 通过tag获取首页数据
     */
    public static String getMainDataByTab() {
        return IM_SERVER_URL.concat("/home/v2/tagindex");
    }

    /**
     * 首页头部广告
     */
    public static String getHomeHeadBanner() {
        return JAVA_WEB_URL.concat("/home/getIndexTopBanner");
    }

    /**
     * 首页提示萌币中心红点
     *
     * @return
     */
    public static String getHomeRemindState() {
        return JAVA_WEB_URL.concat("/mcoin/v1/getMissionCount");
    }

    /**
     * 获取首页推荐分类列表数据
     */
    public static String getMainDataByMenu() {
        return IM_SERVER_URL.concat("/room/tag/v3/classification");
    }

    public static String getLotteryActivityPage() {
        return IM_SERVER_URL.concat("/mm/luckdraw/index.html");
    }

    /**
     * 获取首页七天签到任务列表
     *
     * @return
     */
    public static String getSignInMissions() {
        return JAVA_WEB_URL.concat("/mcoin/v1/getCheckInMissions");
    }

    /**
     * 签到领取奖励
     *
     * @return
     */
    public static String postSignInMission() {
        return JAVA_WEB_URL.concat("/mcoin/v1/gainDailyMcoin");
    }

    /**
     * 获取首页种瓜弹窗
     *
     * @return
     */
    public static String getMelonPopup() {
        return JAVA_WEB_URL.concat("/seed/melon/homePopup");
    }

    public static String getConfigUrl() {
        return IM_SERVER_URL.concat("/client/configure");
    }


    // 获取用户落地页配置
    public static String getLoginindexconf() {
        return IM_SERVER_URL.concat("/client/get/loginindexconf");
    }

    /**
     * 排行榜
     */
    public static String getRankingList() {
        return IM_SERVER_URL.concat("/allrank/geth5");
    }

    public static String getRankingXCList() {
        return IM_SERVER_URL.concat("/stat/user/ctrb/list");
    }

    /**
     * 获取房间用户在线列表信息
     */
    public static String getRoomRankingList() {
        return IM_SERVER_URL.concat("/roomctrb/queryByType");
    }

    /**
     * 举报接口
     */
    public static String reportUserUrl() {
        return IM_SERVER_URL.concat("/user/report/save");
    }

    /**
     * 敏感词
     */
    public static String getSensitiveWord() {
        return IM_SERVER_URL.concat("/sensitiveWord/regex");
    }

    /**
     * 检查双方是否相互拉黑
     *
     * @return
     */
    public static String getCheckBlacklist() {
        return IM_SERVER_URL.concat("/user/blacklist/checkBothSides");
    }

    /**
     * 拉入私聊黑名单
     *
     * @return
     */
    public static String addUserBlackList() {
        return IM_SERVER_URL.concat("/user/blacklist/add");
    }

    /**
     * 移除私聊黑名单
     *
     * @return
     */
    public static String removeUserBlacklist() {
        return IM_SERVER_URL.concat("/user/blacklist/del");
    }

    /**
     * 获取私聊黑名单列表
     *
     * @return
     */
    public static String getUserBlacklist() {
        return IM_SERVER_URL.concat("/user/blacklist/list");
    }

    /**
     * 检查用户行为
     *
     * @return
     */
    public static String checkUserOperation() {
        return IM_SERVER_URL.concat("/user/checkUserOperation");
    }


    /**
     * 禁言
     */
    public static String getBannedType() {
        return IM_SERVER_URL.concat("/banned/checkBanned");
    }

    /* ------------------------------------------------ 砸蛋 --------------------------------------------*/

    /**
     * 开启、关闭猪猪大作战
     */
    public static String openOrClosePigFight() {
        return IM_SERVER_URL.concat("/room/game/openOrClosePigFight ");
    }

    /**
     * 猪猪大作战开始
     */
    public static String startPigFight() {
        return IM_SERVER_URL.concat("/room/game/startPigFight");
    }

    /**
     * 猪猪大作战规则说明
     */
    public static String getPigFightRuleList() {
        return IM_SERVER_URL.concat("/room/game/pigFightRule");
    }

    /**
     * 猪猪大作战最近8天记录
     *
     * @return
     */
    public static String getPigFightRecordList() {
        return IM_SERVER_URL.concat("/room/game/pigFightRecord");
    }

    /**
     * 获取砸蛋排行榜列表
     */
    public static String getPoundEggRank() {
        return IM_SERVER_URL.concat("/user/giftPurse/getRank");
    }

    /**
     * 获取砸蛋奖品
     */
    public static String getPoundEggRewords() {
        return IM_SERVER_URL.concat("/user/giftPurse/drawGiftList");
    }


    /**
     * 获取砸蛋中奖记录
     */
    public static String getPoundEggRewordRecord() {
        return IM_SERVER_URL.concat("/user/giftPurse/record");
    }

    /**
     * 免费砸蛋接口
     */
    public static String poundEggFree() {
        return IM_SERVER_URL.concat("/user/giftPurse/v2/doFreeDraw");
    }

    /**
     * 获取免费砸蛋次数接口
     */
    public static String getPoundEggFreeTimes() {
        return IM_SERVER_URL.concat("/user/giftPurse/v2/freeDrawTimes");
    }

    /**
     * 砸蛋接口
     *
     * @return
     */
    public static String poundEgg() {
        return IM_SERVER_URL.concat("/user/giftPurse/v2/draw");
    }

    /**
     * 获取砸蛋配置
     *
     * @return
     */
    public static String getPoundCfg() {
        return IM_SERVER_URL.concat("/user/giftPurse/v1/getDrawConf");
    }


    /**
     * 种豆规则说明
     *
     * @return
     */
    public static String getPlantBeanRuleList() {
        return IM_SERVER_URL.concat("/user/giftPurse/ruleDescription");
    }

    /* ------------------------------------------------ 砸蛋 --------------------------------------------*/


    //发现 -- 活动列表
    public static String getFindInfo() {
        return JAVA_WEB_URL.concat("/advertise/getList");
    }


    /**
     * 邀请上麦（视频房）
     */
    public static String inviteUpMicroOnVideoRoom() {
        return JAVA_WEB_URL.concat("/room/mic/passmic");
    }

    /**
     * 邀请用户进房
     */
    public static String inviteEnterRoom() {
        return JAVA_WEB_URL.concat("/room/mic/requestNoRoomUser");
    }

    /**
     * 锁麦，开麦操作
     */
    public static String operateMicroPhone() {
        return IM_SERVER_URL.concat("/room/mic/lockmic");
    }

    /**
     * 锁坑，开坑操作
     */
    public static String getlockMicroPhone() {
        return IM_SERVER_URL.concat("/room/mic/lockpos");
    }

    /**
     * 任务列表
     */
    public static String getTaskList() {
        return IM_SERVER_URL.concat("/duty/list");
    }

    /**
     * 领取任务奖励
     */
    public static String getTaskReward() {
        return IM_SERVER_URL.concat("/duty/achieve");
    }

    public static String reportPublic() {
        return IM_SERVER_URL.concat("/duty/fresh/public");
    }

    /**
     * 获取房间速配活动状态
     */
    public static String getRoomMatch() {
        return IM_SERVER_URL.concat("/room/game/getState");
    }

    /**
     * 提交房间速配活动确认选择结果
     */
    public static String postRoomMatchChoose() {
        return IM_SERVER_URL.concat("/room/game/choose");
    }

    /**
     * 提交房间速配活动显示的结果
     */
    public static String postRoomMatchConfirm() {
        return IM_SERVER_URL.concat("/room/game/confirm");
    }

    /**
     * 获取我的连麦状态接口
     */
    public static String getLianMicroStatus() {
        return IM_SERVER_URL.concat("/room/connectMic/getMyMicStatus");
    }

    /**
     * 取消连麦申请接口
     */
    public static String cancelLianMicroApply() {
        return IM_SERVER_URL.concat("/room/connectMic/cancel");
    }

    /**
     * 新手推荐弹框
     */
    public static String getNewUserRecommend() {
        return IM_SERVER_URL.concat("/room/rcmd/get");
    }

    /***
     * 萌新列表
     * @return
     */
    public static String getMengXin() {
        return IM_SERVER_URL.concat("/user/newUserList");
    }


    /**
     * PK活动接口模块
     */
    //发起保存一个PK
    public static String savePk() {
        return JAVA_WEB_URL.concat("/room/pkvote/save");
    }

    //取消一个PK
    public static String cancelPk() {
        return JAVA_WEB_URL.concat("/room/pkvote/cancel");
    }

    //获取一个PK结果
    public static String getPkResult() {
        return JAVA_WEB_URL.concat("/room/pkvote/get");
    }

    //获取PK历史
    public static String getPkHistoryList() {
        return JAVA_WEB_URL.concat("/room/pkvote/list");
    }

    //取消一个PK
    public static String sendPkVote() {
        return JAVA_WEB_URL.concat("/room/pkvote/vote");
    }

    // ---------------------------------------------------房间相关接口 ---------------------------------------

    //房间背景图片列表
    public static String getRoomBackList() {
        return JAVA_WEB_URL.concat("/room/bg/list");
    }


    // --------------------------------------------------- 消息相关接口 --------------------------------------

    //获取是否发送关注消息的接口
    public static String getFocusMsgSwitch() {
        return JAVA_WEB_URL.concat("/user/setting/v1/get");
    }

    //保存是否关注消息的状态接口
    public static String saveFocusMsgSwitch() {
        return JAVA_WEB_URL.concat("/user/setting/v1/save");
    }


    //----------------------------------------------------- 装扮商城相关接口 -----------------------------------
    //头饰接口
    public static String getHeadWearList() {
        return JAVA_WEB_URL.concat("/headwear/listMall");
    }

    public static String headWearList() {
        return JAVA_WEB_URL.concat("/headwear/list");
    }

    //我的头饰接口
    public static String getMyHeadWearList() {
        return JAVA_WEB_URL.concat("/headwear/user/list");
    }

    //购买头饰接口
    public static String purseHeadWear() {
        return JAVA_WEB_URL.concat("/headwear/purse");
    }

    //改变头饰使用状态
    public static String changeHeadWearState() {
        return JAVA_WEB_URL.concat("/headwear/use");
    }

    /**
     * 赠送座驾
     */
    public static String giftCarGive() {
        return IM_SERVER_URL.concat("/giftCar/give");
    }

    public static String giftHeadWearGive() {
        return IM_SERVER_URL.concat("/headwear/give");
    }

    //座驾接口
    public static String getCarList() {
        return JAVA_WEB_URL.concat("/giftCar/listMall");
    }

    public static String carList() {
        return JAVA_WEB_URL.concat("/giftCar/list");
    }

    //我的座驾接口
    public static String getMyCarList() {
        return JAVA_WEB_URL.concat("/giftCar/user/list");
    }

    //购买座驾
    public static String purseCar() {
        return JAVA_WEB_URL.concat("/giftCar/purse");
    }

    //改变座驾使用状态
    public static String changeCarState() {
        return JAVA_WEB_URL.concat("/giftCar/use");
    }


    //-------------------------------------------------- 客户端配置相关接口 ---------------------------------
    //获取审核模式状态
    public static String getClientChannel() {
        return JAVA_WEB_URL.concat("/client/channel");
    }

    //上报安全检测的结果
    public static String reportSafetyCheckResult() {
        return JAVA_WEB_URL.concat("/client/security/saveInfo");
    }

    public static String fetchRoomMembers() {
        return JAVA_WEB_URL.concat("/imroom/v1/fetchRoomMembers");
    }

    //-------------------------------------------------- 房间相关接口 ---------------------------------
    // 房间状态
    public static String liveStatus() {
        return JAVA_WEB_URL.concat("/room/onLiveStatus");
    }

    // 用户强制开播
    public static String forceOnLive() {
        return JAVA_WEB_URL.concat("/room/forceOnLive");
    }

    // 房间黑名单
    public static String addRoomBlackList() {
        return JAVA_WEB_URL.concat("/imroom/v1/markChatRoomBlackList");
    }

    public static String kickMember() {
        return JAVA_WEB_URL.concat("/imroom/v1/kickMember");
    }


    public static String markChatRoomManager() {
        return JAVA_WEB_URL.concat("/imroom/v1/markChatRoomManager");
    }

    //增加房间用户禁言接口
    public static String markChatRoomMute() {
        return JAVA_WEB_URL.concat("/imroom/v1/markChatRoomMute");
    }

    //查询房间用户禁言接口
    public static String fetchChatRoomMuteList() {
        return JAVA_WEB_URL.concat("/imroom/v1/fetchChatRoomMuteList");

    }


    //查询房间管理员
    public static String fetchRoomManagers() {
        return JAVA_WEB_URL.concat("/imroom/v1/fetchRoomManagers");
    }

    //查询房间黑名单列表
    public static String fetchRoomBlackList() {
        return JAVA_WEB_URL.concat("/imroom/v1/fetchRoomBlackList");
    }

    /**
     * 上报当前房间的hls地址
     */
    public static String reportHls() {
        return JAVA_WEB_URL.concat("/imroom/v1/reporthls");
    }

    /**
     * 检测用户
     */
    public static String checkPushAuth() {
        return JAVA_WEB_URL.concat("/imroom/v1/checkPushAuth");
    }

    public static String publicTitle() {
        return IM_SERVER_URL.concat("/imroom/v1/publicTitle");
    }

    /**
     * 申请上麦列表
     */
    public static String applyWaitList() {
        return JAVA_WEB_URL.concat("/room/mic/applyWaitList");
    }

    /**
     * 申请连麦
     */
    public static String applyLianMicro() {
        return JAVA_WEB_URL.concat("/room/connectMic/apply");
    }

    /**
     * 获取连麦列表
     */
    public static String getMicApplyList() {
        return JAVA_WEB_URL.concat("/room/connectMic/list");
    }

    /**
     * 同意连麦
     */
    public static String agreeMicroApply() {
        return JAVA_WEB_URL.concat("/room/connectMic/agree");
    }

    /**
     * 拒绝连麦
     */
    public static String rejectMicroApply() {
        return JAVA_WEB_URL.concat("/room/connectMic/reject");
    }

    /**
     * 设置连麦申请开关
     */
    public static String setRoomConnectMic() {
        return JAVA_WEB_URL.concat("/room/setRoomConnectMic");
    }

    public static String getLiveRoomFinish() {
        return JAVA_WEB_URL.concat("/room/endLive");
    }

    /**
     * 幸运宝箱倒计时
     *
     * @return
     */
    public static String getLuckyBoxDetail() {
        return JAVA_WEB_URL.concat("/luckyTreasure/getLuckyTreasureDetail");
    }

    /**
     * 获取免费弹幕
     *
     * @return
     */
    public static String getFreeBarrages() {
        return JAVA_WEB_URL.concat("/barrage/getFreeBarrage ");
    }

    /**
     * 发送弹幕 (Server→Client)
     *
     * @return
     */
    public static String getBarrageSend() {
        return JAVA_WEB_URL.concat("/imroom/v1/barrage/send");
    }

    /**
     * 魅力值pk邀请
     *
     * @return
     */
    public static String charmPKConfirm() {
        return JAVA_WEB_URL.concat("/roomCharm/savePK");
    }

    /**
     * 魅力值pk邀请确认
     *
     * @return
     */
    public static String agreeCharmPK() {
        return JAVA_WEB_URL.concat("/roomCharm/agree");
    }

    /**
     * 获取房间魅力值pk倒计时
     *
     * @return
     */
    public static String getCharmCountDown() {
        return JAVA_WEB_URL.concat("/roomCharm/getPkExpireSeconds");
    }

    /**
     * 获取房间常用语接口
     *
     * @return
     */
    public static String getUserRecommendWorlds() {
        return IM_SERVER_URL.concat("/room/v1/recommendWorlds");
    }

    /*------------------------------- 家族相关接口 -----------------------------------------*/

    /**
     * 申请退出
     */
    public static String applyExitTeam() {
        return JAVA_WEB_URL.concat("/family/applyExitTeam");
    }

    /**
     * 处理申请消息
     */
    public static String applyFamily() {
        return JAVA_WEB_URL.concat("/family/applyFamily");
    }

    /**
     * 申请加入家族
     */
    public static String applyJoinFamilyTeam() {
        return JAVA_WEB_URL.concat("/family/applyJoinFamilyTeam");
    }

    /**
     * 获取根据uid检测是否有加入家族
     */
    public static String checkFamilyJoin() {
        return JAVA_WEB_URL.concat("/family/checkFamilyJoin");
    }

    /**
     * 创建家族
     */
    public static String createFamilyTeam() {
        return JAVA_WEB_URL.concat("/family/createFamilyTeam");
    }

    /**
     * 编辑家族信息
     */
    public static String editFamilyTeam() {
        return JAVA_WEB_URL.concat("/family/editFamilyTeam");
    }

    /**
     * 根据家族Id获取家族信息
     */
    public static String getFamilyInfo() {
        return JAVA_WEB_URL.concat("/family/getFamilyInfo");
    }

    /**
     * 获取家族消息
     */
    public static String getFamilyMessage() {
        return JAVA_WEB_URL.concat("/family/getFamilyMessage");
    }

    /**
     * 获取加入家族成员信息
     */
    public static String getFamilyTeamJoin() {
        return JAVA_WEB_URL.concat("/family/getFamilyTeamJoin");
    }

    /**
     * 获取根据uid获取家族信息
     */
    public static String getJoinFamilyInfo() {
        return JAVA_WEB_URL.concat("/family/getJoinFamilyInfo");
    }

    /**
     * 获取家族列表
     */
    public static String getList() {
        return JAVA_WEB_URL.concat("/family/getList");
    }

    /**
     * 邀请他人权限
     */
    public static String invitationPermission() {
        return JAVA_WEB_URL.concat("/family/invitationPermission");
    }

    /**
     * 踢出家族
     */
    public static String kickOutTeam() {
        return JAVA_WEB_URL.concat("/family/kickOutTeam");
    }

    /**
     * 移除管理员
     */
    public static String removeAdmin() {
        return JAVA_WEB_URL.concat("/family/removeAdmin");
    }

    /**
     * 设置申请加入方式
     */
    public static String setApplyJoinMethod() {
        return JAVA_WEB_URL.concat("/family/setApplyJoinMethod");
    }

    /**
     * 设置禁言及解禁
     */
    public static String setBanned() {
        return JAVA_WEB_URL.concat("/family/setBanned");
    }

    /**
     * 设置消息提醒
     */
    public static String setMsgNotify() {
        return JAVA_WEB_URL.concat("/family/setMsgNotify");
    }

    /**
     * 设置管理员
     */
    public static String setupAdministrator() {
        return JAVA_WEB_URL.concat("/family/setupAdministrator");
    }

    /*------------------------------- 家族相关接口 -----------------------------------------*/


    //-------------------------------------------------- 房间关注相关接口 ---------------------------------

    //房间关注
    public static String roomAttention() {
        return JAVA_WEB_URL.concat("/room/attention/attentions");
    }

    //删除关注
    public static String deleteAttention() {
        return JAVA_WEB_URL.concat("/room/attention/delAttentions");
    }


    //检查是否关注
    public static String checkAttention() {
        return JAVA_WEB_URL.concat("/room/attention/checkAttentions");
    }

    //根据uid获取关注房间列表
    public static String getRoomAttentionById() {
        return JAVA_WEB_URL.concat("/room/attention/v3/getRoomAttentionByUid");
    }

    /***查询关注列表正在房间的好友*/
    public static String getRoomAttentionList(){
        return JAVA_WEB_URL.concat("/fans/followingAndInRoom");
    }

    //关注页面的推荐房间列表
    public static String getAttentionRecommendList() {
        return JAVA_WEB_URL.concat("/room/getRoomRecommendList");
    }

    //获取房间内排行榜榜首头像
    public static String getRoomRankFirstAvatar() {
        return JAVA_WEB_URL.concat("/roomctrb/v1/listRoomCtrbTop");
    }

    public static String getClosedInfo() {
        return JAVA_WEB_URL.concat("/room/getClosedInfo");
    }

    //-------------------------------------------红包相关接口---------------------------------------------------------------
    //公聊大厅发送红包接口
    public static String sendRedPackage() {
        return JAVA_WEB_URL.concat("/redpack/v1/sendRedPack");
    }

    //公聊大厅领取红包接口
    public static String receiveRedPackage() {
        return JAVA_WEB_URL.concat("/redpack/v1/getRedPack");
    }

    //公聊大厅领取红包记录接口
    public static String receiveRedPackageHistoryRecord() {
        return JAVA_WEB_URL.concat("/redpack/v1/redPackInfo");
    }

    //最新的发送红包记录
    public static String getSendRedPackageRecord() {
        return JAVA_WEB_URL.concat("/redpack/v1/redPackList");
    }

    //-------------------------------------------红包相关接口---------------------------------------------------------------


    //-------------------------------------------萌币相关接口---------------------------------------------------------------
    //萌币任务列表接口
    public static String getMengCoinTaskList() {
        return JAVA_WEB_URL.concat("/mcoin/v1/getInfo");
    }


    //公聊大厅领取红包记录接口
    public static String receiveMengCoinById() {
        return JAVA_WEB_URL.concat("/mcoin/v1/gainMcoin");
    }


    //-------------------------------------------萌币相关接口---------------------------------------------------------------

    /*------------------------------------------- 好友相关操作接口 -------------------------------------------------------*/

    /**
     * 获取我发起的申请列表
     */
    public static String applies() {
        return JAVA_WEB_URL.concat("/user/friend/v1/applies");
    }

    /**
     * 发送给我的申请列表
     */
    public static String sent() {
        return JAVA_WEB_URL.concat("/user/friend/v1/sent");
    }

    /**
     * 申请添加好友接口
     */
    public static String apply() {
        return JAVA_WEB_URL.concat("/user/friend/v1/apply");
    }

    /**
     * 检查好友状态接口
     */
    public static String checkStatus() {
        return JAVA_WEB_URL.concat("/user/friend/v1/getStatus");
    }

    /**
     * 同意或拒绝用户的申请
     */
    public static String agree() {
        return JAVA_WEB_URL.concat("/user/friend/v1/agree");
    }

    /**
     * 邀请进房列表
     */
    public static String activeList() {
        return JAVA_WEB_URL.concat("/room/mic/activeList");
    }

    /**
     * 获取用户守护榜数据
     */
    public static String guardianList() {
        return JAVA_WEB_URL.concat("/user/guardian/v1/rank/list");
    }

    /**
     * 获取提现白名单
     */
    public static String requestWithdrawStatus() {
        return JAVA_WEB_URL.concat("/withDraw/v1/getStatus");
    }

    /**
     * 获取首页推荐房
     */
    public static String getPopupRoom() {
        return JAVA_WEB_URL.concat("/home/getPopupRoom");
    }

    public static String reportStatisticsEvent() {
        return JAVA_WEB_URL.concat("/client/report/saveInfo");
    }

    /**
     * 获取关注房间列表
     *
     * @return
     */
    public static String getRoomAttention() {
        return JAVA_WEB_URL.concat("/room/attention/getRoomAttentionByUid");
    }

    /**
     * @return /home/videoRoom
     */
    public static String getHomeVideoRoom() {
        return JAVA_WEB_URL.concat("/home/v3/videoRoom");
    }

    /**根据首页标签ID获取首页数据*/
    public static String getHomeTalkRoom(){
        return JAVA_WEB_URL.concat("/home/v3/tagindex");
    }

    /**获取首页标签ID*/
    public static String getHomeTabRoom(){
        return JAVA_WEB_URL.concat("/home/homeChatRoom");
    }

    public static String getAccCheckQrCode() {
        return JAVA_WEB_URL.concat("/acc/qrCode/getUser");
    }

    public static String getAccQrCodeScan() {
        return JAVA_WEB_URL.concat("/acc/qrCode/scan");
    }


    /*----------------------------------------------群聊相关接口----------------------------------------------*/

    public static String buildNewGroup() {
        return JAVA_WEB_URL.concat("/family/createFamilyTeam");
    }

    /**
     * 获取群聊的列表数据
     */
    public static String getGroupList() {
        return JAVA_WEB_URL.concat("/family/getList");
    }

    /**
     * 获取热搜红娘
     */
    public static String getHotSearchMatchmaker() {
        return JAVA_WEB_URL.concat("/family/getHotMatchmaker");
    }

    public static String searchGroupList() {
        return JAVA_WEB_URL.concat("/family/quertByMatchmaker");
    }


    /**
     * 申请加入群聊
     */
    public static String applyJoinGroup() {
        return JAVA_WEB_URL.concat("/family/applyJoinFamilyTeam");
    }

    /**
     * 申请退出群聊
     */
    public static String applyExitGroup() {
        return JAVA_WEB_URL.concat("/family/applyExitTeam");
    }

    /**
     * 获取成员列表
     */
    public static String getMemberList() {
        return JAVA_WEB_URL.concat("/family/getFamilyTeamJoin");
    }

    /**
     * 获取主播协议页面url
     */
    public static String getBroadcastingAreementPage() {
        return JAVA_WEB_URL.concat("/front/agreement/index.html");
    }

    /**
     * 获取特惠礼物信息及该用户已经购买次数
     */
    public static String getGiftPreferential() {
        return JAVA_WEB_URL.concat("/giftPreferential/getGiftPreferentialV1");
    }

    /**
     * 获取特惠礼物信息及该用户已经购买次数
     */
    public static String getGiftPreferentialSetting() {
        return JAVA_WEB_URL.concat("/giftPreferential/getSettingInfo");
    }

    /**
     * 获取特惠礼物
     */
    public static String getPreferentialGiftList() {
        return JAVA_WEB_URL.concat("/gift/getPreferentialGiftList");
    }

    /**
     * 减少特惠礼物
     */
    public static String reducePreferentialGift() {
        return JAVA_WEB_URL.concat("/gift/reducePreferentialGift");
    }
}
