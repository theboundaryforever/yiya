package com.tongdaxing.xchat_core.msg;

import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * 创建者      Created by dell
 * 创建时间    2019/4/8
 * 描述        群聊用户信息
 * @author dell
 */
public class MemberInfo implements Parcelable, MultiItemEntity {

    public static final byte NORMAL_TYPE = 0;
    public static final byte TITLE_TYPE = 1;

    private long uid;
    private long erbanNo;
    private String nike;
    private String avatar;
    /** 角色状态 */
    private int roleStatus;
    /** 1-禁⾔言，0-解禁 */
    private int mute;

    private byte itemType = NORMAL_TYPE;
    private String title;

    public MemberInfo(byte itemType, String title) {
        this.itemType = itemType;
        this.title = title;
    }

    protected MemberInfo(Parcel in) {
        uid = in.readLong();
        erbanNo = in.readLong();
        nike = in.readString();
        avatar = in.readString();
        roleStatus = in.readInt();
        mute = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(uid);
        dest.writeLong(erbanNo);
        dest.writeString(nike);
        dest.writeString(avatar);
        dest.writeInt(roleStatus);
        dest.writeInt(mute);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MemberInfo> CREATOR = new Creator<MemberInfo>() {
        @Override
        public MemberInfo createFromParcel(Parcel in) {
            return new MemberInfo(in);
        }

        @Override
        public MemberInfo[] newArray(int size) {
            return new MemberInfo[size];
        }
    };

    public long getUid() {
        return uid;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public String getNike() {
        return nike;
    }

    public String getAvatar() {
        return avatar;
    }

    public int getRoleStatus() {
        return roleStatus;
    }

    public int getMute() {
        return mute;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    @Override
    public String toString() {
        return "MemberInfo{" +
                "uid=" + uid +
                ", erbanNo=" + erbanNo +
                ", nike='" + nike + '\'' +
                ", avatar='" + avatar + '\'' +
                ", roleStatus=" + roleStatus +
                ", mute=" + mute +
                '}';
    }
}
