package com.tongdaxing.xchat_core.msg;

import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * 创建者      Created by dell
 * 创建时间    2019/4/1
 * 描述        群聊信息
 * @author dell
 */
public class GroupInfo implements Parcelable, MultiItemEntity {

    public static final int NORMAL_TYPE = 0;
    public static final int TITLE_TYPE = 1;
    public static final int EMPTY_TYPE = 2;

    public static final byte ROLE_NONE = 0;
    public static final byte ROLE_OWNER = 1;
    public static final byte ROLE_MANAGER = 2;

    private int uid;
    private int roomId;
    private int member;
    private String nick;
    private int familyId;
    /**
     * 0.不是该群聊成员 1.族长2.管理员3.普通成员
     */
    private byte roleStatus;
    private String familyLogo;
    private String familyName;
    private String familyNotice;
    private List<GroupMemberInfo> familyUsersDTOS;

    /**
     * item 类型
     */
    private int type = NORMAL_TYPE;
    /**
     * item的标题
     */
    private String title;
    private boolean showDividerLine;

    @Override
    public int getItemType() {
        return type;
    }

    public GroupInfo(int type, String title, boolean showDividerLine) {
        this.type = type;
        this.title = title;
        this.showDividerLine = showDividerLine;
    }

    protected GroupInfo(Parcel in) {
        uid = in.readInt();
        roomId = in.readInt();
        member = in.readInt();
        nick = in.readString();
        familyId = in.readInt();
        roleStatus = in.readByte();
        familyLogo = in.readString();
        familyName = in.readString();
        familyNotice = in.readString();
        familyUsersDTOS = in.createTypedArrayList(GroupMemberInfo.CREATOR);
    }

    public static final Creator<GroupInfo> CREATOR = new Creator<GroupInfo>() {
        @Override
        public GroupInfo createFromParcel(Parcel in) {
            return new GroupInfo(in);
        }

        @Override
        public GroupInfo[] newArray(int size) {
            return new GroupInfo[size];
        }
    };

    public int getUid() {
        return uid;
    }

    public int getRoomId() {
        return roomId;
    }

    public int getMember() {
        return member;
    }

    public String getNick() {
        return nick;
    }

    public int getFamilyId() {
        return familyId;
    }

    public byte getRoleStatus() {
        return roleStatus;
    }

    public String getFamilyLogo() {
        return familyLogo;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getFamilyNotice() {
        return familyNotice;
    }

    public List<GroupMemberInfo> getMemberList() {
        return familyUsersDTOS;
    }

    public String getTitle() {
        return title;
    }

    public boolean isShowDividerLine() {
        return showDividerLine;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(uid);
        parcel.writeInt(roomId);
        parcel.writeInt(member);
        parcel.writeString(nick);
        parcel.writeInt(familyId);
        parcel.writeByte(roleStatus);
        parcel.writeString(familyLogo);
        parcel.writeString(familyName);
        parcel.writeString(familyNotice);
        parcel.writeTypedList(familyUsersDTOS);
    }

    @Override
    public String toString() {
        return "GroupInfo{" +
                "uid=" + uid +
                ", roomId=" + roomId +
                ", member=" + member +
                ", nick='" + nick + '\'' +
                ", familyId=" + familyId +
                ", roleStatus=" + roleStatus +
                ", familyLogo='" + familyLogo + '\'' +
                ", familyName='" + familyName + '\'' +
                ", familyNotice='" + familyNotice + '\'' +
                ", familyUsersDTOS=" + familyUsersDTOS +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", showDividerLine=" + showDividerLine +
                '}';
    }
}
