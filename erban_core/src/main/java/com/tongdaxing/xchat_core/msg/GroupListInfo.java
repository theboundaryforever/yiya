package com.tongdaxing.xchat_core.msg;

import java.util.List;

/**
 * 创建者      Created by dell
 * 创建时间    2019/4/1
 * 描述        群聊列表数据
 *
 * @author dell
 */
public class GroupListInfo {

    /**
     * 推荐的群聊列表
     */
    private List<GroupInfo> recommendFamilyList;
    /**
     * 加入的群聊列表
     */
    private List<GroupInfo> myFamilyList;

    public List<GroupInfo> getRecommendFamilyList() {
        return recommendFamilyList;
    }

    public List<GroupInfo> getMyFamilyList() {
        return myFamilyList;
    }

    @Override
    public String toString() {
        return "GroupListInfo{" +
                "familyList=" + recommendFamilyList +
                ", familyTeam=" + myFamilyList +
                '}';
    }
}
