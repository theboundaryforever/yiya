package com.tongdaxing.xchat_core.msg;

import android.support.annotation.NonNull;

import com.netease.nim.uikit.bean.FriendStateInfo;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.CallBack;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.tongdaxing.xchat_core.Constants.DEFAULT_PAGE_SIZE;
import static com.tongdaxing.xchat_core.Constants.PAGE_START;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/4
 * 描述        ${}
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 */
public class MsgModel extends BaseMvpModel {

    /**
     * 我的申请列表
     */
    public void applies(boolean isMy, int pageNum, int pageSize, OkHttpManager.MyCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();

        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        param.put("pageNum", String.valueOf(pageNum));
        param.put("pageSize", String.valueOf(pageSize));

        getRequest(isMy ? UriProvider.applies() : UriProvider.sent(), param, callBack);
    }

    /**
     * 申请添加好友接口
     */
    public void apply(long friendUid, HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();

        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("friendUid", String.valueOf(friendUid));

        getRequest(UriProvider.apply(), param, callBack);
    }

    /**
     * 检查好友状态
     *
     * 返回值 status 的状态 -1 双方未申请， 1 被申请人未同意， 2 好友， 3 本人未同意, 4 显示好友状态 此时彼此已经是好友
     */
    public void checkStatus(String friendUid, HttpRequestCallBack<FriendStateInfo> callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        param.put("friendUid", friendUid);

        getRequest(UriProvider.checkStatus(), param, callBack);
    }

    /**
     * 同意或拒绝好友
     *
     * @param status -1，申请人拒绝；2，申请人同意
     */
    public void agree(long friendId, int status, HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        param.put("friendId", String.valueOf(friendId));
        param.put("status", String.valueOf(status));

        getRequest(UriProvider.agree(), param, callBack);
    }

    /**
     * 创建群组
     */
    public void buildNewGroup(String cover, String groupName, String groupNotice,
                              HttpRequestCallBack<Object> callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();

        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        if (StringUtils.isNotEmpty(cover)) {
            param.put("logo", cover);
        }
        param.put("name", groupName);
        param.put("notice", groupNotice);

        postRequest(UriProvider.buildNewGroup(), param, callBack);
    }

    /**
     * 获取群组列表信息
     */
    public void getGroupList(final CallBack<List<GroupInfo>> callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();

        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        getRequest(UriProvider.getGroupList(), param, new HttpRequestCallBack<GroupListInfo>() {

            @Override
            public void onSuccess(String message, GroupListInfo response) {
                if (response != null) {
                    List<GroupInfo> groupList = new ArrayList<>();
                    if (ListUtils.isNotEmpty(response.getMyFamilyList())) {
                        GroupInfo groupInfo = new GroupInfo(GroupInfo.TITLE_TYPE, "我加入的群聊", false);
                        groupList.add(groupInfo);
                        groupList.addAll(response.getMyFamilyList());
                    } else {
                        GroupInfo groupInfo = new GroupInfo(GroupInfo.EMPTY_TYPE, "", false);
                        groupList.add(groupInfo);
                    }
                    if (ListUtils.isNotEmpty(response.getRecommendFamilyList())) {
                        GroupInfo groupInfo = new GroupInfo(GroupInfo.TITLE_TYPE, "为你推荐", true);
                        groupList.add(groupInfo);
                        groupList.addAll(response.getRecommendFamilyList());
                    }
                    callBack.onSuccess(groupList);
                } else {
                    callBack.onSuccess(null);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                callBack.onFail(msg);
            }
        });
    }

    public void getHotSearchMatchmaker(final CallBack<List<String>> callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();

        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        getRequest(UriProvider.getHotSearchMatchmaker(), param, new HttpRequestCallBack<List<UserInfo>>() {

            @Override
            public void onSuccess(String message, List<UserInfo> response) {
                if (ListUtils.isNotEmpty(response)) {
                    List<String> labelList = new ArrayList<>();
                    for (UserInfo userInfo : response) {
                        labelList.add(userInfo.getNick());
                    }
                    callBack.onSuccess(labelList);
                } else {
                    callBack.onSuccess(null);
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                callBack.onFail(msg);
            }
        });
    }

    public void searchGroupList(int pageNum, String content, HttpRequestCallBack<List<GroupInfo>> callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();

        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        param.put("pageSize", String.valueOf(DEFAULT_PAGE_SIZE));
        param.put("pageNum", String.valueOf(pageNum));
        param.put("keyword", content);

        getRequest(UriProvider.searchGroupList(), param, callBack);
    }

    /**
     * 申请加入家族
     */
    public void applyJoinGroup(int familyId, HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();

        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("familyId", String.valueOf(familyId));

        getRequest(UriProvider.applyJoinGroup(), param, callBack);
    }

    public void applyExitGroup(int familyId, HttpRequestCallBack callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();

        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());
        param.put("familyId", String.valueOf(familyId));

        getRequest(UriProvider.applyExitGroup(), param, callBack);
    }

    /**
     * 获取群聊成员列表
     */
    public void getMemberList(int familyId, final int pageNum, @NonNull final CallBack<List<MemberInfo>> callBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();

        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        param.put("ticket", CoreManager.getCore(IAuthCore.class).getTicket());

        param.put("pageSize", String.valueOf(DEFAULT_PAGE_SIZE));
        param.put("familyId", String.valueOf(familyId));
        param.put("pageNum", String.valueOf(pageNum));

        getRequest(UriProvider.getMemberList(), param, new HttpRequestCallBack<List<MemberInfo>>() {

            @Override
            public void onSuccess(String message, List<MemberInfo> response) {
                if (pageNum == PAGE_START) {
                    if (ListUtils.isNotEmpty(response)) {
                        MemberInfo memberInfo = new MemberInfo(MemberInfo.TITLE_TYPE, "群主");
                        response.add(0, memberInfo);
                        if (response.size() != PAGE_START) {
                            MemberInfo memberTitle = new MemberInfo(MemberInfo.TITLE_TYPE, "群成员");
                            response.add(2, memberTitle);
                        }
                        callBack.onSuccess(response);
                    } else {
                        callBack.onFail("没有更多数据");
                    }
                } else {
                    if (ListUtils.isNotEmpty(response)) {
                        callBack.onSuccess(response);
                    } else {
                        callBack.onFail("没有更多数据");
                    }
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                callBack.onFail("获取数据失败");
            }
        });
    }
}

