package com.tongdaxing.xchat_core.msg;

import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * 创建者      Created by dell
 * 创建时间    2019/4/1
 * 描述        群组成员信息
 *
 * @author dell
 */
public class GroupMemberInfo implements Parcelable, MultiItemEntity {

    public static final byte NORMAL_TYPE = 0;
    public static final byte ADD_MEMBER_TYPE = 1;

    private String avatar;
    private String nick;
    private int uid;

    private byte itemType = NORMAL_TYPE;

    public GroupMemberInfo(byte type) {
        this.itemType = type;
    }

    protected GroupMemberInfo(Parcel in) {
        avatar = in.readString();
        nick = in.readString();
        uid = in.readInt();
        itemType = in.readByte();
    }

    public static final Creator<GroupMemberInfo> CREATOR = new Creator<GroupMemberInfo>() {
        @Override
        public GroupMemberInfo createFromParcel(Parcel in) {
            return new GroupMemberInfo(in);
        }

        @Override
        public GroupMemberInfo[] newArray(int size) {
            return new GroupMemberInfo[size];
        }
    };

    public String getAvatar() {
        return avatar;
    }

    public String getNick() {
        return nick;
    }

    public int getUid() {
        return uid;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(avatar);
        parcel.writeString(nick);
        parcel.writeInt(uid);
        parcel.writeByte(itemType);
    }

    @Override
    public String toString() {
        return "GroupMemberInfo{" +
                "avatar='" + avatar + '\'' +
                ", nick='" + nick + '\'' +
                ", uid=" + uid +
                '}';
    }
}
