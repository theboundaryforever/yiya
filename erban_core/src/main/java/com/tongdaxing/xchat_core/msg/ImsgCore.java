package com.tongdaxing.xchat_core.msg;

import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * Created by zhouxiangfeng on 2017/3/11.
 */

public interface ImsgCore extends IBaseCore {


    void getMsgList();

}
