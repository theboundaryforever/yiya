package com.tongdaxing.xchat_core;

import java.io.File;

/**
 * <p> 常量集合 </p>
 * Created by Administrator on 2017/11/9.
 */
public class Constants {
    public static final String ERBAN_DIR_NAME = "com.yiya.mobile";

    //云信
    public static final String nimAppKey = "2385f7d1508c73e7bff1d4b00908c926"; //App Key：2385f7d1508c73e7bff1d4b00908c926

    /**
     * 百度统计
     */
    public static final String BAIDU_APPKEY = "e1afec7406";

    public static final String LOG_DIR = ERBAN_DIR_NAME + File.separator + "logs";

    public static final String KEY_MAIN_POSITION = "key_main_position";

    public static final int RESULT_OK = 200;

    public static final int PAGE_START = 1;
    public static final int PAGE_START_ZERO = 0;
    public static final int PAGE_SIZE = 10;
    public static final int DEFAULT_PAGE_SIZE = 20;
    public static final int PAGE_HOME_HOT_SIZE = 12;
    public static final int BILL_PAGE_SIZE = 50;

    public static final String HOME_TAB_INFO = "home_tab_info";
    public static final String KEY_USER_INFO = "key_user_info";

    public static final String KEY_HOME_HOT_LIST = "key_home_hot_list";
    public static final String KEY_HOME_NO_HOT_LIST = "key_home_no_hot_list";

    public static final int FAN_MAIN_PAGE_TYPE = 100;
    public static final int FAN_NO_MAIN_PAGE_TYPE = 101;
    public static final String KEY_PAGE_TYPE = "page_type";
    public static final String KEY_MAIN_TAB_LIST = "main_tab_list";

    public static final String KEY_POSITION = "position";

    public static final String CHARGE_HJ_WX = "WEIXIN_APP"; // 汇聚支付
    public static final String CHARGE_WX = "wx";
    public static final String CHARGE_HC_ALIPAY = "alipay_hc";// 汇潮支付
    public static final String CHARGE_ALIPAY = "alipay";

    public static final int CHARGE_TYPE_PPP = 0;// p++
    public static final int CHARGE_TYPE_HC = CHARGE_TYPE_PPP + 1;// 汇潮
    public static final int CHARGE_TYPE_HJ = CHARGE_TYPE_HC + 1;// 汇聚
    public static final int CHARGE_TYPE_BANK = CHARGE_TYPE_HJ + 1;// 银行卡支付

    public static final int PAGE_TYPE_AV_ROOM_ACTIVITY = 100;
    public static final int PAGE_TYPE_USER_INFO_ACTIVITY = 101;
    public static final java.lang.String KEY_ROOM_IS_SHOW_ONLINE = "is_show_online";
    public static final String KEY_ROOM_INFO = "key_room_info";

    /**
     * 房间相关Key设置
     */
    public static final String ROOM_UPDATE_KEY_POSTION = "micPosition";
    public static final String ROOM_UPDATE_KEY_UID = "micUid";
    public static final String ROOM_UPDATE_KEY_GENDER = "gender";

    public static final String KEY_CHAT_ROOM_INFO_ROOM = "roomInfo";
    public static final String KEY_CHAT_ROOM_INFO_MIC = "micQueue";

    public static final String ROOM_UID = "ROOM_UID";
    public static final String ROOM_TYPE = "ROOM_TYPE";
    public static final String ROOM_VIEW_TYPE = "ROOM_VIEW_TYPE";

    public static final String TYPE = "TYPE";

    public static final String KEY_PLANT_BEAN_ANIM = "KEY_PLANT_BEAN_ANIM"; // 种豆动画开关
    public static final String KEY_PLANT_BEAN_MSG = "KEY_PLANT_BEAN_MSG"; // 种豆消息开关

}
