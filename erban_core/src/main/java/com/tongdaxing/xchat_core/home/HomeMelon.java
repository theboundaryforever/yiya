package com.tongdaxing.xchat_core.home;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/5.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class HomeMelon implements Parcelable {

    /**
     * isPopUp : 0 是否弹窗 1弹 0不弹
     * seedMelonType : 0 种瓜类型 1 免费种瓜 2、前往种瓜
     * uid : 0
     */

    private int isPopUp;
    private int seedMelonType;
    private int uid;

    public int getIsPopUp() {
        return isPopUp;
    }

    public void setIsPopUp(int isPopUp) {
        this.isPopUp = isPopUp;
    }

    public int getSeedMelonType() {
        return seedMelonType;
    }

    public void setSeedMelonType(int seedMelonType) {
        this.seedMelonType = seedMelonType;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public boolean isPopup() {
        return isPopUp == 1;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("HomeMelon{");
        sb.append("isPopUp=").append(isPopUp);
        sb.append(", seedMelonType=").append(seedMelonType);
        sb.append(", uid=").append(uid);
        sb.append('}');
        return sb.toString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.isPopUp);
        dest.writeInt(this.seedMelonType);
        dest.writeInt(this.uid);
    }

    public HomeMelon() {
    }

    protected HomeMelon(Parcel in) {
        this.isPopUp = in.readInt();
        this.seedMelonType = in.readInt();
        this.uid = in.readInt();
    }

    public static final Creator<HomeMelon> CREATOR = new Creator<HomeMelon>() {
        @Override
        public HomeMelon createFromParcel(Parcel source) {
            return new HomeMelon(source);
        }

        @Override
        public HomeMelon[] newArray(int size) {
            return new HomeMelon[size];
        }
    };
}
