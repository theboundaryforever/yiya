package com.tongdaxing.xchat_core.home.mission;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/31.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class CheckInMissionInfo implements Parcelable {

    /**
     * mcoinNum : 0
     * picUrl : string
     */

    private int mcoinNum;
    private String picUrl;

    public int getMcoinNum() {
        return mcoinNum;
    }

    public void setMcoinNum(int mcoinNum) {
        this.mcoinNum = mcoinNum;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mcoinNum);
        dest.writeString(this.picUrl);
    }

    public CheckInMissionInfo() {
    }

    protected CheckInMissionInfo(Parcel in) {
        this.mcoinNum = in.readInt();
        this.picUrl = in.readString();
    }

    public static final Creator<CheckInMissionInfo> CREATOR = new Creator<CheckInMissionInfo>() {
        @Override
        public CheckInMissionInfo createFromParcel(Parcel source) {
            return new CheckInMissionInfo(source);
        }

        @Override
        public CheckInMissionInfo[] newArray(int size) {
            return new CheckInMissionInfo[size];
        }
    };

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CheckInMissionInfo{");
        sb.append("mcoinNum=").append(mcoinNum);
        sb.append(", picUrl='").append(picUrl).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
