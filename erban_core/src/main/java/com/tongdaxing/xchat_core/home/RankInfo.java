package com.tongdaxing.xchat_core.home;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/7/4
 */
public class RankInfo {


    private List<WealthRankBean> wealthRank;
    private List<CharmRankBean> charmRank;

    public List<WealthRankBean> getWealthRank() {
        return wealthRank;
    }

    public void setWealthRank(List<WealthRankBean> wealthRank) {
        this.wealthRank = wealthRank;
    }

    public List<CharmRankBean> getCharmRank() {
        return charmRank;
    }

    public void setCharmRank(List<CharmRankBean> charmRank) {
        this.charmRank = charmRank;
    }

    public static class WealthRankBean {
        /**
         * uid : 2907
         * erbanNo : 1718503
         * avatar : https://pic.lyy18.cn/FiSuVncmdkjuBUKyMma1vH-DaYd6?imageslim
         * nick : 夏季潮流
         * gender : 2
         * totalNum : 611108
         * experLevel : 50
         * charmLevel : 37
         * distance : 0
         */

        private int uid;
        private int erbanNo;
        private String avatar;
        private String nick;
        private int gender;
        private int totalNum;
        private int experLevel;
        private int charmLevel;
        private int distance;

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public int getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(int erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }

        public int getExperLevel() {
            return experLevel;
        }

        public void setExperLevel(int experLevel) {
            this.experLevel = experLevel;
        }

        public int getCharmLevel() {
            return charmLevel;
        }

        public void setCharmLevel(int charmLevel) {
            this.charmLevel = charmLevel;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }
    }

    public static class CharmRankBean {
        /**
         * uid : 4326
         * erbanNo : 7691327
         * avatar : https://pic.lyy18.cn/Fmnca6yOjSYS1MdNSmGSr3s6LZYK?imageslim
         * nick : 谧:弖
         * gender : 1
         * totalNum : 520150
         * experLevel : 35
         * charmLevel : 15
         * distance : 0
         */

        private int uid;
        private int erbanNo;
        private String avatar;
        private String nick;
        private int gender;
        private int totalNum;
        private int experLevel;
        private int charmLevel;
        private int distance;

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public int getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(int erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }

        public int getExperLevel() {
            return experLevel;
        }

        public void setExperLevel(int experLevel) {
            this.experLevel = experLevel;
        }

        public int getCharmLevel() {
            return charmLevel;
        }

        public void setCharmLevel(int charmLevel) {
            this.charmLevel = charmLevel;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }
    }
}
