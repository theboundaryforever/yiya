package com.tongdaxing.xchat_core.home;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author zhouxiangfeng
 * @date 2017/5/17
 */

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class HomeRoom extends RoomInfo implements MultiItemEntity {
    public static final int ITEM_TYPE_VIDEO = 1;

    /**
     * 性别 1:男 2：女 0 ：未知
     */
    private int gender;
    private int index;
    private String birth;
    private String province;
    private String city;
    private int age;
    private String region;
    private String userDesc;
    private int seqNo;
    public int itemType = 0;
    private int status  = 0;
    public String badge;
    public List<BannerInfo> bannerInfoList;
}