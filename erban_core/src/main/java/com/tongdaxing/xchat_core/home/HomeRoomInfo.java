package com.tongdaxing.xchat_core.home;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/1
 * 描述        ${}
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 *
 * @author dell
 */
public class HomeRoomInfo implements Parcelable {

    public long roomUid;
    public long roomId;
    public String title;
    public String avatar;
    public int onlineNum;
    public String nick;
    public int gender;

    public MemberInfo member;

    protected HomeRoomInfo(Parcel in) {
        roomUid = in.readLong();
        roomId = in.readLong();
        title = in.readString();
        avatar = in.readString();
        onlineNum = in.readInt();
        nick = in.readString();
        gender = in.readInt();
        member = in.readParcelable(MemberInfo.class.getClassLoader());
    }

    public static final Creator<HomeRoomInfo> CREATOR = new Creator<HomeRoomInfo>() {
        @Override
        public HomeRoomInfo createFromParcel(Parcel in) {
            return new HomeRoomInfo(in);
        }

        @Override
        public HomeRoomInfo[] newArray(int size) {
            return new HomeRoomInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(roomUid);
        parcel.writeLong(roomId);
        parcel.writeString(title);
        parcel.writeString(avatar);
        parcel.writeInt(onlineNum);
        parcel.writeString(nick);
        parcel.writeInt(gender);
        parcel.writeParcelable(member, i);
    }

    public static class MemberInfo implements Parcelable {

        public long uid;
        public long erbanNo;
        public String nick;
        public String avatar;
        public int gender;
        public String region;
        public String province;
        public String city;
        public String userDesc;
        public int age;

        protected MemberInfo(Parcel in) {
            uid = in.readLong();
            erbanNo = in.readLong();
            nick = in.readString();
            avatar = in.readString();
            gender = in.readInt();
            region = in.readString();
            province = in.readString();
            city = in.readString();
            userDesc = in.readString();
            age = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(uid);
            dest.writeLong(erbanNo);
            dest.writeString(nick);
            dest.writeString(avatar);
            dest.writeInt(gender);
            dest.writeString(region);
            dest.writeString(province);
            dest.writeString(city);
            dest.writeString(userDesc);
            dest.writeInt(age);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<MemberInfo> CREATOR = new Creator<MemberInfo>() {
            @Override
            public MemberInfo createFromParcel(Parcel in) {
                return new MemberInfo(in);
            }

            @Override
            public MemberInfo[] newArray(int size) {
                return new MemberInfo[size];
            }
        };

        public long getUid() {
            return uid;
        }

        public long getErbanNo() {
            return erbanNo;
        }

        public String getNick() {
            return nick;
        }

        public String getAvatar() {
            return avatar;
        }

        public int getGender() {
            return gender;
        }

        public String getRegion() {
            return region;
        }

        public String getProvince() {
            return province;
        }

        public String getCity() {
            return city;
        }

        public String getUserDesc() {
            return userDesc;
        }

        public int getAge() {
            return age;
        }
    }

    public long getRoomUid() {
        return roomUid;
    }

    public long getRoomId() {
        return roomId;
    }

    public String getTitle() {
        return title;
    }

    public String getAvatar() {
        return avatar;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public MemberInfo getMember() {
        return member;
    }

    public String getNick() {
        return nick;
    }

    public int getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "HomeRoomInfo{" +
                "roomUid=" + roomUid +
                ", roomId=" + roomId +
                ", title='" + title + '\'' +
                ", avatar='" + avatar + '\'' +
                ", onlineNum=" + onlineNum +
                ", member=" + member +
                '}';
    }
}
