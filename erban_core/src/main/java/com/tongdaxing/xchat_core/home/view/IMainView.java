package com.tongdaxing.xchat_core.home.view;

import android.view.View;

import com.tongdaxing.erban.libcommon.base.IMvpBaseView;
import com.tongdaxing.xchat_core.home.HomeMelon;
import com.tongdaxing.xchat_core.home.mission.CheckInMissionInfo;
import com.tongdaxing.xchat_core.home.mission.Missions;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.List;

public interface IMainView extends IMvpBaseView {
    /**
     * 退出房间
     */
    void onRoomExit();


    void onCheckBlacklist(View view, Json json);

    void showSignInMissionDialog(List<Missions.CheckInMissions> checkInMissions);

    void showMissionRewardDialog(CheckInMissionInfo checkInMissionInfo);

    void showMelonDialog(HomeMelon homeMelon);
}
