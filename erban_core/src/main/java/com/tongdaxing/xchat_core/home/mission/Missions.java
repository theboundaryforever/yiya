package com.tongdaxing.xchat_core.home.mission;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/29.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class Missions {

    /**
     * beginnerMissions : [{"completeDate":"2019-07-29T12:18:12.122Z","freebiesId":0,"freebiesType":0,"mcoinAmount":0,"missionId":0,"missionName":"string","missionStatus":0,"needDays":0,"picUrl":"string","scheme":"string"}]
     * checkInMissions : [{"completeDate":"2019-07-29T12:18:12.122Z","freebiesId":0,"freebiesType":0,"mcoinAmount":0,"missionId":0,"missionName":"string","missionStatus":0,"needDays":0,"picUrl":"string","scheme":"string"}]
     * continuousMissions : [{"completeDate":"2019-07-29T12:18:12.122Z","freebiesId":0,"freebiesType":0,"mcoinAmount":0,"missionId":0,"missionName":"string","missionStatus":0,"needDays":0,"picUrl":"string","scheme":"string"}]
     * dailyMissions : [{"completeDate":"2019-07-29T12:18:12.122Z","freebiesId":0,"freebiesType":0,"mcoinAmount":0,"missionId":0,"missionName":"string","missionStatus":0,"needDays":0,"picUrl":"string","scheme":"string"}]
     * mcoinNum : 0
     */

    private int mcoinNum;
    private List<BeginnerMissions> beginnerMissions;
    private List<CheckInMissions> checkInMissions;
    private List<ContinuousMissions> continuousMissions;
    private List<DailyMissions> dailyMissions;

    public int getMcoinNum() {
        return mcoinNum;
    }

    public void setMcoinNum(int mcoinNum) {
        this.mcoinNum = mcoinNum;
    }

    public List<BeginnerMissions> getBeginnerMissions() {
        return beginnerMissions;
    }

    public void setBeginnerMissions(List<BeginnerMissions> beginnerMissions) {
        this.beginnerMissions = beginnerMissions;
    }

    public List<CheckInMissions> getCheckInMissions() {
        return checkInMissions;
    }

    public void setCheckInMissions(List<CheckInMissions> checkInMissions) {
        this.checkInMissions = checkInMissions;
    }

    public List<ContinuousMissions> getContinuousMissions() {
        return continuousMissions;
    }

    public void setContinuousMissions(List<ContinuousMissions> continuousMissions) {
        this.continuousMissions = continuousMissions;
    }

    public List<DailyMissions> getDailyMissions() {
        return dailyMissions;
    }

    public void setDailyMissions(List<DailyMissions> dailyMissions) {
        this.dailyMissions = dailyMissions;
    }

    public static class BeginnerMissions implements Serializable {
        /**
         * completeDate : 2019-07-29T12:18:12.122Z
         * freebiesId : 0
         * freebiesType : 0
         * mcoinAmount : 0
         * missionId : 0
         * missionName : string
         * missionStatus : 0
         * needDays : 0
         * picUrl : string
         * scheme : string
         */

        private String completeDate;
        private int freebiesId;
        private int freebiesType;
        private int mcoinAmount;
        private int missionId;
        private String missionName;
        private int missionStatus;
        private int needDays;
        private String picUrl;
        private String scheme;

        public String getCompleteDate() {
            return completeDate;
        }

        public void setCompleteDate(String completeDate) {
            this.completeDate = completeDate;
        }

        public int getFreebiesId() {
            return freebiesId;
        }

        public void setFreebiesId(int freebiesId) {
            this.freebiesId = freebiesId;
        }

        public int getFreebiesType() {
            return freebiesType;
        }

        public void setFreebiesType(int freebiesType) {
            this.freebiesType = freebiesType;
        }

        public int getMcoinAmount() {
            return mcoinAmount;
        }

        public void setMcoinAmount(int mcoinAmount) {
            this.mcoinAmount = mcoinAmount;
        }

        public int getMissionId() {
            return missionId;
        }

        public void setMissionId(int missionId) {
            this.missionId = missionId;
        }

        public String getMissionName() {
            return missionName;
        }

        public void setMissionName(String missionName) {
            this.missionName = missionName;
        }

        public int getMissionStatus() {
            return missionStatus;
        }

        public void setMissionStatus(int missionStatus) {
            this.missionStatus = missionStatus;
        }

        public int getNeedDays() {
            return needDays;
        }

        public void setNeedDays(int needDays) {
            this.needDays = needDays;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public String getScheme() {
            return scheme;
        }

        public void setScheme(String scheme) {
            this.scheme = scheme;
        }
    }

    public static class CheckInMissions implements Parcelable {
        /**
         * completeDate : 2019-07-29T12:18:12.122Z
         * freebiesId : 0
         * freebiesType : 0
         * mcoinAmount : 0
         * missionId : 0
         * missionName : string
         * missionStatus : 0 任务状态，1，未完成；2，已完成；3，已领取
         * needDays : 0
         * picUrl : string
         * scheme : string
         */

        private String completeDate;
        private int freebiesId;
        private int freebiesType;
        private int mcoinAmount;
        private int missionId;
        private String missionName;
        private int missionStatus;
        private int needDays;
        private String picUrl;
        private String scheme;

        public String getCompleteDate() {
            return completeDate;
        }

        public void setCompleteDate(String completeDate) {
            this.completeDate = completeDate;
        }

        public int getFreebiesId() {
            return freebiesId;
        }

        public void setFreebiesId(int freebiesId) {
            this.freebiesId = freebiesId;
        }

        public int getFreebiesType() {
            return freebiesType;
        }

        public void setFreebiesType(int freebiesType) {
            this.freebiesType = freebiesType;
        }

        public int getMcoinAmount() {
            return mcoinAmount;
        }

        public void setMcoinAmount(int mcoinAmount) {
            this.mcoinAmount = mcoinAmount;
        }

        public int getMissionId() {
            return missionId;
        }

        public void setMissionId(int missionId) {
            this.missionId = missionId;
        }

        public String getMissionName() {
            return missionName;
        }

        public void setMissionName(String missionName) {
            this.missionName = missionName;
        }

        public int getMissionStatus() {
            return missionStatus;
        }

        public void setMissionStatus(int missionStatus) {
            this.missionStatus = missionStatus;
        }

        public int getNeedDays() {
            return needDays;
        }

        public void setNeedDays(int needDays) {
            this.needDays = needDays;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public String getScheme() {
            return scheme;
        }

        public void setScheme(String scheme) {
            this.scheme = scheme;
        }

        public boolean isSignIn() {
            return missionStatus != 1;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.completeDate);
            dest.writeInt(this.freebiesId);
            dest.writeInt(this.freebiesType);
            dest.writeInt(this.mcoinAmount);
            dest.writeInt(this.missionId);
            dest.writeString(this.missionName);
            dest.writeInt(this.missionStatus);
            dest.writeInt(this.needDays);
            dest.writeString(this.picUrl);
            dest.writeString(this.scheme);
        }

        public CheckInMissions() {
        }

        protected CheckInMissions(Parcel in) {
            this.completeDate = in.readString();
            this.freebiesId = in.readInt();
            this.freebiesType = in.readInt();
            this.mcoinAmount = in.readInt();
            this.missionId = in.readInt();
            this.missionName = in.readString();
            this.missionStatus = in.readInt();
            this.needDays = in.readInt();
            this.picUrl = in.readString();
            this.scheme = in.readString();
        }

        public static final Creator<CheckInMissions> CREATOR = new Creator<CheckInMissions>() {
            @Override
            public CheckInMissions createFromParcel(Parcel source) {
                return new CheckInMissions(source);
            }

            @Override
            public CheckInMissions[] newArray(int size) {
                return new CheckInMissions[size];
            }
        };
    }

    public static class ContinuousMissions implements Serializable {
        /**
         * completeDate : 2019-07-29T12:18:12.122Z
         * freebiesId : 0
         * freebiesType : 0
         * mcoinAmount : 0
         * missionId : 0
         * missionName : string
         * missionStatus : 0
         * needDays : 0
         * picUrl : string
         * scheme : string
         */

        private String completeDate;
        private int freebiesId;
        private int freebiesType;
        private int mcoinAmount;
        private int missionId;
        private String missionName;
        private int missionStatus;
        private int needDays;
        private String picUrl;
        private String scheme;

        public String getCompleteDate() {
            return completeDate;
        }

        public void setCompleteDate(String completeDate) {
            this.completeDate = completeDate;
        }

        public int getFreebiesId() {
            return freebiesId;
        }

        public void setFreebiesId(int freebiesId) {
            this.freebiesId = freebiesId;
        }

        public int getFreebiesType() {
            return freebiesType;
        }

        public void setFreebiesType(int freebiesType) {
            this.freebiesType = freebiesType;
        }

        public int getMcoinAmount() {
            return mcoinAmount;
        }

        public void setMcoinAmount(int mcoinAmount) {
            this.mcoinAmount = mcoinAmount;
        }

        public int getMissionId() {
            return missionId;
        }

        public void setMissionId(int missionId) {
            this.missionId = missionId;
        }

        public String getMissionName() {
            return missionName;
        }

        public void setMissionName(String missionName) {
            this.missionName = missionName;
        }

        public int getMissionStatus() {
            return missionStatus;
        }

        public void setMissionStatus(int missionStatus) {
            this.missionStatus = missionStatus;
        }

        public int getNeedDays() {
            return needDays;
        }

        public void setNeedDays(int needDays) {
            this.needDays = needDays;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public String getScheme() {
            return scheme;
        }

        public void setScheme(String scheme) {
            this.scheme = scheme;
        }
    }

    public static class DailyMissions implements Serializable {
        /**
         * completeDate : 2019-07-29T12:18:12.122Z
         * freebiesId : 0
         * freebiesType : 0
         * mcoinAmount : 0
         * missionId : 0
         * missionName : string
         * missionStatus : 0
         * needDays : 0
         * picUrl : string
         * scheme : string
         */

        private String completeDate;
        private int freebiesId;
        private int freebiesType;
        private int mcoinAmount;
        private int missionId;
        private String missionName;
        private int missionStatus;
        private int needDays;
        private String picUrl;
        private String scheme;

        public String getCompleteDate() {
            return completeDate;
        }

        public void setCompleteDate(String completeDate) {
            this.completeDate = completeDate;
        }

        public int getFreebiesId() {
            return freebiesId;
        }

        public void setFreebiesId(int freebiesId) {
            this.freebiesId = freebiesId;
        }

        public int getFreebiesType() {
            return freebiesType;
        }

        public void setFreebiesType(int freebiesType) {
            this.freebiesType = freebiesType;
        }

        public int getMcoinAmount() {
            return mcoinAmount;
        }

        public void setMcoinAmount(int mcoinAmount) {
            this.mcoinAmount = mcoinAmount;
        }

        public int getMissionId() {
            return missionId;
        }

        public void setMissionId(int missionId) {
            this.missionId = missionId;
        }

        public String getMissionName() {
            return missionName;
        }

        public void setMissionName(String missionName) {
            this.missionName = missionName;
        }

        public int getMissionStatus() {
            return missionStatus;
        }

        public void setMissionStatus(int missionStatus) {
            this.missionStatus = missionStatus;
        }

        public int getNeedDays() {
            return needDays;
        }

        public void setNeedDays(int needDays) {
            this.needDays = needDays;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public String getScheme() {
            return scheme;
        }

        public void setScheme(String scheme) {
            this.scheme = scheme;
        }
    }
}
