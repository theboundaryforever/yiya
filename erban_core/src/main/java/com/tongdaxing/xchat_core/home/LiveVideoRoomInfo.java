package com.tongdaxing.xchat_core.home;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/22
 */
public class LiveVideoRoomInfo {


    private List<RoomListBean> roomList;
    private List<MasterBannerBean> masterBanner;
    private List<NoticeBannerBean> noticeBanner;
    private List<HotChatRoomListBean> hotChatRoomList;


    public List<RoomListBean> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<RoomListBean> roomList) {
        this.roomList = roomList;
    }

    public List<MasterBannerBean> getMasterBanner() {
        return masterBanner;
    }

    public void setMasterBanner(List<MasterBannerBean> masterBanner) {
        this.masterBanner = masterBanner;
    }

    public List<NoticeBannerBean> getNoticeBanner() {
        return noticeBanner;
    }

    public void setNoticeBanner(List<NoticeBannerBean> noticeBanner) {
        this.noticeBanner = noticeBanner;
    }

    public List<HotChatRoomListBean> getHotChatRoomList() {
        return hotChatRoomList;
    }

    public void setHotChatRoomList(List<HotChatRoomListBean> hotChatRoomList) {
        this.hotChatRoomList = hotChatRoomList;
    }

    public static class RoomListBean {
        /**
         * uid : 2003
         * roomId : 10000571
         * title : he的房间
         * type : 6
         * avatar : https://pic.173ing.com/Fuw8VVgxyvboojZg15xsK5I5BS2A?imageslim
         * roomDesc :
         * onlineNum : 10
         * gender : 1
         * nick : he
         * erbanNo : 5828507
         * roomTag : 交友
         * tagId : 8
         * tagPict : https://pic.173ing.com/jiaoyou_biaoqian@3x.png
         * city : 广州
         */

        private int uid;
        private int roomId;
        private String title;
        private int type;
        private String avatar;
        private String roomDesc;
        private int onlineNum;
        private int gender;
        private String nick;
        private int erbanNo;
        private String roomTag;
        private int tagId;
        private String tagPict;
        private String city;


        private String badge;

        public String getBadge() {
            return badge;
        }

        public void setBadge(String badge) {
            this.badge = badge;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public int getRoomId() {
            return roomId;
        }

        public void setRoomId(int roomId) {
            this.roomId = roomId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getRoomDesc() {
            return roomDesc;
        }

        public void setRoomDesc(String roomDesc) {
            this.roomDesc = roomDesc;
        }

        public int getOnlineNum() {
            return onlineNum;
        }

        public void setOnlineNum(int onlineNum) {
            this.onlineNum = onlineNum;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(int erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getRoomTag() {
            return roomTag;
        }

        public void setRoomTag(String roomTag) {
            this.roomTag = roomTag;
        }

        public int getTagId() {
            return tagId;
        }

        public void setTagId(int tagId) {
            this.tagId = tagId;
        }

        public String getTagPict() {
            return tagPict;
        }

        public void setTagPict(String tagPict) {
            this.tagPict = tagPict;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }
    }

    public static class MasterBannerBean {
        /**
         * bannerName : 跳h5banner
         * bannerPic : https://pic.meibao8888.com/FuMJ9OjgmFNY_PWr2PRpBIrcotM8?imageslim
         * skipType : 3
         * skipUri : https://www.baidu.com
         */

        private String bannerName;
        private String bannerPic;
        private int skipType;
        private String skipUri;

        public String getBannerName() {
            return bannerName;
        }

        public void setBannerName(String bannerName) {
            this.bannerName = bannerName;
        }

        public String getBannerPic() {
            return bannerPic;
        }

        public void setBannerPic(String bannerPic) {
            this.bannerPic = bannerPic;
        }

        public int getSkipType() {
            return skipType;
        }

        public void setSkipType(int skipType) {
            this.skipType = skipType;
        }

        public String getSkipUri() {
            return skipUri;
        }

        public void setSkipUri(String skipUri) {
            this.skipUri = skipUri;
        }
    }

    public static class NoticeBannerBean {
        /**
         * bannerName : 跳h5banner
         * bannerPic : https://pic.meibao8888.com/FuMJ9OjgmFNY_PWr2PRpBIrcotM8?imageslim
         * skipType : 3
         * skipUri : https://www.baidu.com
         */

        private String bannerName;
        private String bannerPic;
        private int skipType;
        private String skipUri;

        public String getBannerName() {
            return bannerName;
        }

        public void setBannerName(String bannerName) {
            this.bannerName = bannerName;
        }

        public String getBannerPic() {
            return bannerPic;
        }

        public void setBannerPic(String bannerPic) {
            this.bannerPic = bannerPic;
        }

        public int getSkipType() {
            return skipType;
        }

        public void setSkipType(int skipType) {
            this.skipType = skipType;
        }

        public String getSkipUri() {
            return skipUri;
        }

        public void setSkipUri(String skipUri) {
            this.skipUri = skipUri;
        }
    }


    public static class HotChatRoomListBean {

        /**
         * uid : 3043
         * roomId : 10011380
         * title : assess的房间
         * type : 3
         * valid : true
         * operatorStatus : 1
         * avatar : https://pic.lyy18.cn/Fg1wNuxmUl_UJ0j38MWCfvHI3dd8?imageslim
         * roomDesc :
         * backPic :
         * onlineNum : 58
         * gender : 1
         * nick : assess
         * erbanNo : 7641877
         * roomTag : 交友
         * calcSumDataIndex : 0
         * tagId : 8
         * tagPict : https://pic.lyy18.cn/jiaoyou@3x.png
         * recomSeq : 100
         * isPermitRoom : 2
         * score : 17.407
         * isRecom : 0
         * count : 0
         */

        private int uid;
        private int roomId;
        private String title;
        private int type;
        private boolean valid;
        private int operatorStatus;
        private String avatar;
        private String roomDesc;
        private String backPic;
        private int onlineNum;
        private int gender;
        private String nick;
        private int erbanNo;
        private String roomTag;
        private int calcSumDataIndex;
        private int tagId;
        private String tagPict;
        private int recomSeq;
        private int isPermitRoom;
        private double score;
        private int isRecom;
        private int count;

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public int getRoomId() {
            return roomId;
        }

        public void setRoomId(int roomId) {
            this.roomId = roomId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }

        public int getOperatorStatus() {
            return operatorStatus;
        }

        public void setOperatorStatus(int operatorStatus) {
            this.operatorStatus = operatorStatus;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getRoomDesc() {
            return roomDesc;
        }

        public void setRoomDesc(String roomDesc) {
            this.roomDesc = roomDesc;
        }

        public String getBackPic() {
            return backPic;
        }

        public void setBackPic(String backPic) {
            this.backPic = backPic;
        }

        public int getOnlineNum() {
            return onlineNum;
        }

        public void setOnlineNum(int onlineNum) {
            this.onlineNum = onlineNum;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(int erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getRoomTag() {
            return roomTag;
        }

        public void setRoomTag(String roomTag) {
            this.roomTag = roomTag;
        }

        public int getCalcSumDataIndex() {
            return calcSumDataIndex;
        }

        public void setCalcSumDataIndex(int calcSumDataIndex) {
            this.calcSumDataIndex = calcSumDataIndex;
        }

        public int getTagId() {
            return tagId;
        }

        public void setTagId(int tagId) {
            this.tagId = tagId;
        }

        public String getTagPict() {
            return tagPict;
        }

        public void setTagPict(String tagPict) {
            this.tagPict = tagPict;
        }

        public int getRecomSeq() {
            return recomSeq;
        }

        public void setRecomSeq(int recomSeq) {
            this.recomSeq = recomSeq;
        }

        public int getIsPermitRoom() {
            return isPermitRoom;
        }

        public void setIsPermitRoom(int isPermitRoom) {
            this.isPermitRoom = isPermitRoom;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        public int getIsRecom() {
            return isRecom;
        }

        public void setIsRecom(int isRecom) {
            this.isRecom = isRecom;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }
}
