package com.tongdaxing.xchat_core.home;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

import lombok.Data;

/**
 * @author Zhangsongzhou
 * @date 2019/3/22
 */

@Data
public class RoomAttentionInfo implements MultiItemEntity {


    private int itemType;

    private List<Attentions> attentions;
    private List<RoomAttentionRecBean> roomAttentionRecList;
    private List<RoomAttentionRecBean> videoRoomList;


    @Data
    public static class Attentions {

        private int roomUid;
        private int type;
        private String title;
        private String avatar;
        private String roomTag;
        private String roomTagPic;
        private int onlineNum;
        private String city;

        private int uid;
        private String tagPict;
    }

    @Data
    public static class GuessesBean {
        private int roomUid;
        private int type;
        private String title;
        private String avatar;
        private String roomTag;
        private String roomTagPic;
        private int onlineNum;
        private String city;
    }

    @Data
    public static class RoomAttentionRecBean {

        private String avatar;
        private String nick;
        private int roomId;
        private String title;
        private int type;

        private int uid;
        private String tagPict;
        private int roomUid;

        private String roomTag;
        private String roomTagPic;
        private int onlineNum;
        private String city;
    }
}
