package com.tongdaxing.xchat_core.user.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import io.realm.RealmObject;
import lombok.EqualsAndHashCode;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/10/10.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
@EqualsAndHashCode(callSuper = false)
public class Medal extends RealmObject implements Serializable, Parcelable {

    private String picUrl;

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.picUrl);
    }

    public Medal() {
    }

    protected Medal(Parcel in) {
        this.picUrl = in.readString();
    }

    public static final Creator<Medal> CREATOR = new Creator<Medal>() {
        @Override
        public Medal createFromParcel(Parcel source) {
            return new Medal(source);
        }

        @Override
        public Medal[] newArray(int size) {
            return new Medal[size];
        }
    };
}
