package com.tongdaxing.xchat_core.user.bean;

import android.support.annotation.DrawableRes;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.tongdaxing.xchat_core.bean.RankingXCInfo;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/6/28.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MeInfo implements MultiItemEntity {

    public static final int TYPE_TOP = 0;
    public static final int TYPE_GIFT = 1;
    public static final int TYPE_NAME_VERIFY = 2;
    public static final int TYPE_BROADCASTER_VERIFY = 3;
    public static final int TYPE_FEEDBACK = 4;
    public static final int TYPE_INVITE = 5;
    public static final int TYPE_HOST_RECORD = 6;// 主播记录
    public static final int TYPE_ABOUT = 7;

    private UserInfo userInfo;
    private RankingXCInfo rankingXCInfo;
    private int resDrawable;
    private String title;

    private int itemType;

    public MeInfo(int itemType, @DrawableRes int resDrawable, String title) {
        this.itemType = itemType;
        this.resDrawable = resDrawable;
        this.title = title;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public RankingXCInfo getRankingXCInfo() {
        return rankingXCInfo;
    }

    public void setRankingXCInfo(RankingXCInfo rankingXCInfo) {
        this.rankingXCInfo = rankingXCInfo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getResDrawable() {
        return resDrawable;
    }

    public void setResDrawable(int resDrawable) {
        this.resDrawable = resDrawable;
    }
}
