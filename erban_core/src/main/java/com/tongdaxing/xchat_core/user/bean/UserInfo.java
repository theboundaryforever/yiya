package com.tongdaxing.xchat_core.user.bean;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by chenran on 2017/3/8.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserInfo extends RealmObject implements Serializable, Parcelable {
    public static final int USER_TYPE_COMMON = 1;
    public static final int USER_TYPE_OFFICIAL = 2;

    public static final String KEY_USER_INFO = "KEY_USER_INFO";
    public static final String KEY_USER_ID = "KEY_USER_ID";

    @PrimaryKey
    private long uid;
    //耳伴号
    private long erbanNo;
    // 昵称
    private String nick;
    //头像
    private String avatar;
    //性别 1:男 2：女 0 ：未知
    private int gender;
    //生日
    private long birth;
    //生日日期格式不存数据库
    private String birthStr;
    //签名
    private String signture;
    //声音展示文件
    private String userVoice;
    //声音时间
    private int voiceDura;
    //关注数
    private long followNum;
    //粉丝数
    private long fansNum;
    //人气值
    private long fortune;
    //1普通账号，2官方账号，3机器账号
    private int defUser;
    //地区
    private String region;
    //个人简介
    private String userDesc;
    //个人相册
    private RealmList<UserPhoto> privatePhoto;
    //财富等级
    private int experLevel;
    private String experLevelPic;
    //魅力等级
    private int charmLevel;
    private String charmLevelPic;
    //用户等级
    private int videoRoomExperLevel;
    //用户等级图片
    private String videoRoomExperLevelPic;

    private String phone;

    private String carUrl;

    private String carName;

    private String headwearUrl;

    /**
     * 是否靓号
     */
    private Boolean isPrettyErbanNo;
    /**
     * 用户勋章
     */
    private Medal medal;
    /**
     * 星座
     */
    private String constellation;

    private long createTime;

    private long tol;

    //显示萌新模块权限开关 1 有 0无
    private int findNewUsers;
    private String newUserPic;

    private String province;

    private String city;

    private int height;

    private int auditStatus;
    private int anchorStatus;
    private int age;

    private boolean isNewRegister;

    public UserInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.uid);
        dest.writeLong(this.erbanNo);
        dest.writeString(this.nick);
        dest.writeString(this.avatar);
        dest.writeInt(this.gender);
        dest.writeLong(this.birth);
        dest.writeString(this.birthStr);
        dest.writeString(this.signture);
        dest.writeString(this.userVoice);
        dest.writeInt(this.voiceDura);
        dest.writeLong(this.followNum);
        dest.writeLong(this.fansNum);
        dest.writeLong(this.fortune);
        dest.writeInt(this.defUser);
        dest.writeString(this.region);
        dest.writeString(this.userDesc);
        dest.writeList(this.privatePhoto);
        dest.writeInt(this.experLevel);
        dest.writeString(this.experLevelPic);
        dest.writeInt(this.charmLevel);
        dest.writeString(this.charmLevelPic);
        dest.writeInt(this.videoRoomExperLevel);
        dest.writeString(this.videoRoomExperLevelPic);
        dest.writeString(this.phone);
        dest.writeString(this.carUrl);
        dest.writeString(this.carName);
        dest.writeString(this.headwearUrl);
        dest.writeValue(this.isPrettyErbanNo);
        dest.writeParcelable(this.medal, flags);
        dest.writeString(this.constellation);
        dest.writeLong(this.createTime);
        dest.writeLong(this.tol);
        dest.writeInt(this.findNewUsers);
        dest.writeString(this.newUserPic);
        dest.writeString(this.province);
        dest.writeString(this.city);
        dest.writeInt(this.height);
        dest.writeInt(this.auditStatus);
        dest.writeInt(this.anchorStatus);
        dest.writeInt(this.age);
        dest.writeByte(this.isNewRegister ? (byte) 1 : (byte) 0);
    }

    protected UserInfo(Parcel in) {
        this.uid = in.readLong();
        this.erbanNo = in.readLong();
        this.nick = in.readString();
        this.avatar = in.readString();
        this.gender = in.readInt();
        this.birth = in.readLong();
        this.birthStr = in.readString();
        this.signture = in.readString();
        this.userVoice = in.readString();
        this.voiceDura = in.readInt();
        this.followNum = in.readLong();
        this.fansNum = in.readLong();
        this.fortune = in.readLong();
        this.defUser = in.readInt();
        this.region = in.readString();
        this.userDesc = in.readString();
        this.privatePhoto = new RealmList<>();
        in.readList(this.privatePhoto, UserPhoto.class.getClassLoader());
        this.experLevel = in.readInt();
        this.experLevelPic = in.readString();
        this.charmLevel = in.readInt();
        this.charmLevelPic = in.readString();
        this.videoRoomExperLevel = in.readInt();
        this.videoRoomExperLevelPic = in.readString();
        this.phone = in.readString();
        this.carUrl = in.readString();
        this.carName = in.readString();
        this.headwearUrl = in.readString();
        this.isPrettyErbanNo = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.medal = in.readParcelable(Medal.class.getClassLoader());
        this.constellation = in.readString();
        this.createTime = in.readLong();
        this.tol = in.readLong();
        this.findNewUsers = in.readInt();
        this.newUserPic = in.readString();
        this.province = in.readString();
        this.city = in.readString();
        this.height = in.readInt();
        this.auditStatus = in.readInt();
        this.anchorStatus = in.readInt();
        this.age = in.readInt();
        this.isNewRegister = in.readByte() != 0;
    }

    public static final Creator<UserInfo> CREATOR = new Creator<UserInfo>() {
        @Override
        public UserInfo createFromParcel(Parcel source) {
            return new UserInfo(source);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };
}