package com.tongdaxing.xchat_core.user.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/10/10.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MultiInfo implements MultiItemEntity {

    public static final int TYPE_PIC_URL = 0;
    public static final int TYPE_RES_ID = 1;

    private int itemType = TYPE_PIC_URL;

    private String picUrl;
    private int resId;

    public MultiInfo(int itemType) {
        this.itemType = itemType;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public MultiInfo setPicUrl(String picUrl) {
        this.picUrl = picUrl;
        return this;
    }

    public int getResId() {
        return resId;
    }

    public MultiInfo setResId(int resId) {
        this.resId = resId;
        return this;
    }
}
