package com.tongdaxing.xchat_core.user.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/6/28.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class MineCommonInfo implements MultiItemEntity {
    private String title;
    private int itemType;

    public MineCommonInfo(int itemType, String title) {
        this.itemType = itemType;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public int getItemType() {
        return itemType;
    }
}
