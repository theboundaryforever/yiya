package com.tongdaxing.xchat_core.user.bean;

import com.tongdaxing.xchat_core.room.bean.RoomInfo;

import lombok.Data;

@Data
public class NewUserBean {
    private long uid;
    private long erbanNo;
    private String phone;
    private long birth;
    private String nick;
    private int followNum;
    private int fansNum;
    private int gender;
    private String avatar;
    private String isNewUserPic;
    private int experLevel;
    private String experLevelPic;
    private int charmLevel;
    private String charmLevelPic;
    //用户等级图片
    private String videoRoomExperLevelPic;
    private int age;
    private int userStatus;// 0: 离线（刚刚来过） 1：在线不在房间（新人报道） 2：在线在房间（热聊中...）
    private RoomInfo userInRoom;
}
