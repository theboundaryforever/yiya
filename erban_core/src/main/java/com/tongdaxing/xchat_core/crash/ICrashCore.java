package com.tongdaxing.xchat_core.crash;

import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

/**
 * Created by chenran on 2017/4/2.
 */

public interface ICrashCore extends IBaseCore {
    void uploadCrashFile();
}
