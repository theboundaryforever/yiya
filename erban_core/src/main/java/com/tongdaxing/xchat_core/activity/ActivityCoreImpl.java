package com.tongdaxing.xchat_core.activity;

import com.netease.nimlib.sdk.msg.constant.MsgTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.tongdaxing.xchat_core.common.ICommonClient;
import com.tongdaxing.xchat_core.im.custom.bean.CustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.nim.LotteryAttachment;
import com.tongdaxing.xchat_core.im.message.IIMMessageCoreClient;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;

import java.util.List;

/**
 * Created by chenran on 2017/12/26.
 */

public class ActivityCoreImpl extends AbstractBaseCore implements IActivityCore {

    public ActivityCoreImpl() {
        CoreManager.addClient(this);
    }

    @Override
    public void requestLotteryActivity() {

    }
}
