package com.tongdaxing.xchat_core;


import com.tongdaxing.xchat_core.manager.ReUsedSocketManager;
import com.tongdaxing.xchat_framework.util.config.BasicConfig;
import com.tongdaxing.xchat_framework.util.util.pref.CommonPref;

/**
 * 环境配置类
 */
public class Env {

    public static final String PREF_SVC_SETTING = "PREF_SVC_SETTING";

    public static final String PREF_SVC_BROADCAST_SETTING = "PREF_SVC_BROADCAST_SETTING";

    public static final String PREF_URI_SETTING = "PREF_URI_SETTING";


    private static final Env mEnv = new Env();

    private Env() {

    }

    public static Env instance() {
        return mEnv;
    }

    public void init() {
        if (BasicConfig.INSTANCE.isDebuggable()) {
            UriProvider.init(UriSetting.Test);
            ReUsedSocketManager.initIMSocket(UriSetting.Test);
        } else {
            UriProvider.init(UriSetting.Release);
            ReUsedSocketManager.initIMSocket(UriSetting.Release);
        }
    }


    public enum UriSetting {
        Release, Test
    }
}
