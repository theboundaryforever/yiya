package com.tongdaxing.xchat_core.initial;

import java.io.Serializable;

import lombok.Data;

@Data
public class ClientConfigure implements Serializable {
    private String isExchangeAwards;
    private String timestamps;
    private String micInListOption;
    private String lottery_box_option;
    private String word_draw_niu_dan_enable_option;
    private String greenRoomIndex;
    private String lotteryBoxBigGift;
    private String mcoinOption;
    private String lishi_flag;// 1 禁用redpack
    private String lishi_show_text;
    private String send_pic_left_level;
    private int lucky_treasure_chest_flag = 1;//超级宝箱开关 默认0：开
    private int seed_melon_switch;//房间种瓜入口开关 默认0：关
    private int payChannel; //微信 1,ping++;2,汇聚
    private int alipayChannel; //支付宝 1,ping++;2,汇潮
    private int joinpay_mobilePay; //  汇聚收银台支付（银行卡），0关，1开
    private int gift_preferential_switch; //特惠礼物 开关 0关 1开
    private int gift_preferential_watting_time ; //特惠礼物 几秒后弹出dialog
}
