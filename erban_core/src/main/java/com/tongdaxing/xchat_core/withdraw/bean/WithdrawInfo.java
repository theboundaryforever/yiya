package com.tongdaxing.xchat_core.withdraw.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/7/25.
 */

public class WithdrawInfo implements Serializable {

    /*
        uid:90000
		alipayAccount:xxx //支付宝账号
		alipayAccountName:xxx //支付宝姓名
		diamondNum:钻石余额
		isNotBoundPhone:true//没有绑定手机，绑定了手机不返回该字段
		withDrawType 1微信2支付宝  历史提现状态
     */
    public long uid;
    public String alipayAccount;
    public double diamondNum;
    public String alipayAccountName;
    public boolean isNotBoundPhone;
    public boolean hasWx;
    public int withDrawType;
    public double redbeanNum;
    public String weixinName;
    public String phone;
}
