package com.tongdaxing.xchat_core.bean;

import com.tongdaxing.xchat_core.room.bean.WaitQueuePersonInfo;

/**
 * 房间消息实体
 */
public class VideoRoomMessage extends ChatRoomMessage {

    private IMChatRoomMember motherMember;
    private int needGold;
    private int position;
    private int type;
    private long roomUid;
    private WaitQueuePersonInfo waitQueuePersonInfo;

    public  WaitQueuePersonInfo getWaitQueuePersonInfo() {
        return waitQueuePersonInfo;
    }

    public void setWaitQueuePersonInfo(WaitQueuePersonInfo waitQueuePersonInfo) {
        this.waitQueuePersonInfo = waitQueuePersonInfo;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public IMChatRoomMember getMotherMember() {
        return motherMember;
    }

    public void setMotherMember(IMChatRoomMember motherMember) {
        this.motherMember = motherMember;
    }

    public int getNeedGold() {
        return needGold;
    }

    public void setNeedGold(int needGold) {
        this.needGold = needGold;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }
}
