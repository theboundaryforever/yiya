package com.tongdaxing.xchat_core.bean;

import lombok.Data;

@Data
public class RoomLiveStatus {
    String avatar;// 房间封面
    int phoneOnLive;// 手机端直播状态
    int sleepSeconds;// 需要睡眠的时间 这边需要
    String toast;
    int webOnLive;// web端直播状态
}
