package com.tongdaxing.xchat_core.bean;


import lombok.Data;

/**
 * <p>  房间麦序单个坑位信息实体，包含麦序状态，成员信息</p>
 *
 * @author jiahui
 * @date 2017/12/13
 */
@Data
public class RoomQueueInfo {
    /**
     * 坑位信息（是否所坑，开麦等）
     */
    public RoomMicInfo mRoomMicInfo;
    /**
     * 坑上人员信息
     */
    public IMChatRoomMember mChatRoomMember;

    public int onMicType;//连麦中类型，0 语音连麦  1 视频连麦  2其他(没数据)

    public RoomQueueInfo(RoomMicInfo roomMicInfo, IMChatRoomMember chatRoomMember, int onMicType) {
        mRoomMicInfo = roomMicInfo;
        mChatRoomMember = chatRoomMember;
        this.onMicType = onMicType;
    }

    public boolean isVideoMicroType() {
        return onMicType == 1;
    }

    public RoomQueueInfo(RoomMicInfo roomMicInfo, IMChatRoomMember chatRoomMember) {
        mRoomMicInfo = roomMicInfo;
        mChatRoomMember = chatRoomMember;
    }
}
