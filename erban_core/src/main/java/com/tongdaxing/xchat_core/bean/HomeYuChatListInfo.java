package com.tongdaxing.xchat_core.bean;

/**
 * @author Zhangsongzhou
 * @date 2019/3/23
 */
public class HomeYuChatListInfo {

    private boolean online;

    /**
     * uid : 2000
     * roomId : 10000910
     * title : 123456的房间
     * type : 3
     * operatorStatus : 1
     * avatar : https://pic.173ing.com/Fg09WwVxp5eBOopkhkT93Sz3F4gA?imageslim
     * roomDesc :
     * backPic :
     * onlineNum : 2
     * gender : 1
     * nick : 123456
     * erbanNo : 3969245
     * roomTag : 交友
     * calcSumDataIndex : 0
     * tagId : 8
     * tagPict : https://pic.173ing.com/jiaoyou_biaoqian@3x.png
     * recomSeq : 100
     * isPermitRoom : 2
     * score : 0.607
     * isRecom : 0
     * count : 0
     */

    private int uid;
    private int roomId;
    private String title;
    private int type;
    private int operatorStatus;
    private String avatar;
    private String roomDesc;
    private String backPic;
    private int onlineNum;
    private int gender;
    private String nick;
    private int erbanNo;
    private String roomTag;
    private int calcSumDataIndex;
    private int tagId;
    private String tagPict;
    private int recomSeq;
    private int isPermitRoom;
    private double score;
    private int isRecom;
    private int count;
    private String badge;//次标签
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public String getBackPic() {
        return backPic;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(int erbanNo) {
        this.erbanNo = erbanNo;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public int getCalcSumDataIndex() {
        return calcSumDataIndex;
    }

    public void setCalcSumDataIndex(int calcSumDataIndex) {
        this.calcSumDataIndex = calcSumDataIndex;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public int getRecomSeq() {
        return recomSeq;
    }

    public void setRecomSeq(int recomSeq) {
        this.recomSeq = recomSeq;
    }

    public int getIsPermitRoom() {
        return isPermitRoom;
    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getIsRecom() {
        return isRecom;
    }

    public void setIsRecom(int isRecom) {
        this.isRecom = isRecom;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }
}