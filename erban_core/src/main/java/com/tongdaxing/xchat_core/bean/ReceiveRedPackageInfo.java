package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

import lombok.Data;

/**
 * 文件描述：
 * 领取红包的人信息
 *
 * @auther：zdw
 * @data：2019/5/17
 */
@Data
public class ReceiveRedPackageInfo implements Serializable {
    private long uid;
    private int getCoin;//本人抢到的红包金额
    private String avatar;
    private String nick;
    private String showUnit;//单位
}
