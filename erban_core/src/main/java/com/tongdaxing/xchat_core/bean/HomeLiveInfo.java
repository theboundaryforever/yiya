package com.tongdaxing.xchat_core.bean;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/7/1
 */
public class HomeLiveInfo {


    /**
     * fontRoomList : []
     * roomList : []
     * goddessChatRoomTagId : 3
     * masterBanner : [{"bannerName":"周星","bannerPic":"https://pic.lyy18.cn/FoZidVAy3Mm4os3dc8Rwt2bHd4mv?imageslim","skipType":1,"skipUri":"http://beta.lyy18.cn/front/act05_20/index.html"},{"bannerName":"六&amp;ldquo;一&amp;rdquo;儿童节","bannerPic":"https://pic.lyy18.cn/FgJMpWAll4AfuXLJn_kk7etK1O4C?imageslim","skipType":3,"skipUri":"http://beta.lyy18.cn/front/act_cat/index.html"},{"bannerName":"aaaaa","bannerPic":"https://pic.lyy18.cn/FlG77JP9_uVHpP_rtsBRMrlR5WtW?imageslim","skipType":3,"skipUri":"https://beta.lyy18.cn/front/weekRank/index.html"},{"bannerName":"奇迹仲夏夜","bannerPic":"https://pic.lyy18.cn/Fh4ierdwPij4nRXttcEZI4lIe1Sc?imageslim","skipType":3,"skipUri":"https://beta.lyy18.cn/front/act07_01/index.html"},{"bannerName":"banner2","bannerPic":"https://pic.lyy18.cn/FiOqCUjKhTwb9MIZbLhG0R-wGW0k?imageslim","skipType":3,"skipUri":"https://beta.lyy18.cn/front/act04_23/index.html"},{"bannerName":"banner3","bannerPic":"https://pic.lyy18.cn/Fju949hG9HryuEfe6ZYXSNRCi3zo?imageslim","skipType":3,"skipUri":"https://beta.lyy18.cn/front/newLuckdraw/index.html"}]
     * videoRoomTagList : [{"id":12,"name":"舞蹈","pict":"https://pic.lyy18.cn/wudao@3x.png?imageslim","seq":12,"type":1,"status":true,"istop":false,"createTime":1557228892000,"description":"","tmpint":1,"tmpstr":"","children":"12","optPic":"https://pic.173ing.com/jiaoyou@3x.png","defPic":"https://pic.173ing.com/jiaoyoumoren@3x.png","isHot":0},{"id":7,"name":"娱乐","pict":"https://pic.lyy18.cn/yule@3x.png","seq":2,"type":1,"status":true,"istop":false,"createTime":1511155717000,"description":"","tmpint":1,"tmpstr":"","children":"7","optPic":"https://pic.173ing.com/yule@3x.png","defPic":"https://pic.173ing.com/yulemoren@3x.png","isHot":0}]
     * wealthRankVoList : [{"uid":3245,"erbanNo":3511420,"avatar":"https://pic.lyy18.cn/FmQYggTJpkKOEcIvhUVXpbiEod_E?imageslim","nick":"滴滴","gender":1,"totalNum":356680,"experLevel":28,"charmLevel":29,"distance":0},{"uid":3489,"erbanNo":1970368,"avatar":"https://pic.lyy18.cn/FoKcOyZOyxTXpA_qbyGb5P6FbfkN?imageslim","nick":"我怀疑你喜欢我，但是没有证据","gender":1,"totalNum":224923,"experLevel":35,"charmLevel":34,"distance":131757},{"uid":2926,"erbanNo":9949607,"avatar":"https://pic.lyy18.cn/Fl_wYXEGE06h-BIFWLNu0wLUjXYb?imageslim","nick":"女主播2","gender":2,"totalNum":141276,"experLevel":23,"charmLevel":28,"distance":83647}]
     * charmRankVoList : [{"uid":2907,"erbanNo":1718503,"avatar":"https://pic.lyy18.cn/FiSuVncmdkjuBUKyMma1vH-DaYd6?imageslim","nick":"wuchen季韩版潮流单鞋女鞋","gender":2,"totalNum":379752,"experLevel":50,"charmLevel":37,"distance":0},{"uid":2906,"erbanNo":2558937,"avatar":"https://pic.lyy18.cn/Fi5NhYXCUAVIo9l3u39KfUVQCs-k?imageslim","nick":"hellow@wuchen","gender":1,"totalNum":256320,"experLevel":42,"charmLevel":37,"distance":123432},{"uid":2930,"erbanNo":5204121,"avatar":"https://pic.lyy18.cn/FigD61qIBoDMesIO32QfLPHb7OmP?imageslim","nick":"聚效测试w\u2006s\u2006y","gender":2,"totalNum":133760,"experLevel":29,"charmLevel":19,"distance":122560}]
     */

    private int goddessChatRoomTagId;
    private List<RoomListBean> fontRoomList;
    private List<RoomListBean> roomList;
    private List<MasterBannerBean> masterBanner;
    private List<VideoRoomTagListBean> videoRoomTagList;
    private List<WealthRankVoListBean> wealthRankVoList;
    private List<CharmRankVoListBean> charmRankVoList;

    public int getGoddessChatRoomTagId() {
        return goddessChatRoomTagId;
    }

    public void setGoddessChatRoomTagId(int goddessChatRoomTagId) {
        this.goddessChatRoomTagId = goddessChatRoomTagId;
    }

    public List<RoomListBean> getFontRoomList() {
        return fontRoomList;
    }

    public void setFontRoomList(List<RoomListBean> fontRoomList) {
        this.fontRoomList = fontRoomList;
    }

    public List<RoomListBean> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<RoomListBean> roomList) {
        this.roomList = roomList;
    }

    public List<MasterBannerBean> getMasterBanner() {
        return masterBanner;
    }

    public void setMasterBanner(List<MasterBannerBean> masterBanner) {
        this.masterBanner = masterBanner;
    }

    public List<VideoRoomTagListBean> getVideoRoomTagList() {
        return videoRoomTagList;
    }

    public void setVideoRoomTagList(List<VideoRoomTagListBean> videoRoomTagList) {
        this.videoRoomTagList = videoRoomTagList;
    }

    public List<WealthRankVoListBean> getWealthRankVoList() {
        return wealthRankVoList;
    }

    public void setWealthRankVoList(List<WealthRankVoListBean> wealthRankVoList) {
        this.wealthRankVoList = wealthRankVoList;
    }

    public List<CharmRankVoListBean> getCharmRankVoList() {
        return charmRankVoList;
    }

    public void setCharmRankVoList(List<CharmRankVoListBean> charmRankVoList) {
        this.charmRankVoList = charmRankVoList;
    }

    public static class RoomListBean {
        /**
         * uid : 2003
         * roomId : 10000571
         * title : he的房间
         * type : 6
         * avatar : https://pic.173ing.com/Fuw8VVgxyvboojZg15xsK5I5BS2A?imageslim
         * roomDesc :
         * onlineNum : 10
         * gender : 1
         * nick : he
         * erbanNo : 5828507
         * roomTag : 交友
         * tagId : 8
         * tagPict : https://pic.173ing.com/jiaoyou_biaoqian@3x.png
         * city : 广州
         */

        private int uid;
        private int roomId;
        private String title;
        private int type;
        private String avatar;
        private String roomDesc;
        private int onlineNum;
        private int gender;
        private String nick;
        private int erbanNo;
        private String roomTag;
        private int tagId;
        private String tagPict;
        private String city;

        private String badge;

        public String getBadge() {
            return badge;
        }

        public void setBadge(String badge) {
            this.badge = badge;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public int getRoomId() {
            return roomId;
        }

        public void setRoomId(int roomId) {
            this.roomId = roomId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getRoomDesc() {
            return roomDesc;
        }

        public void setRoomDesc(String roomDesc) {
            this.roomDesc = roomDesc;
        }

        public int getOnlineNum() {
            return onlineNum;
        }

        public void setOnlineNum(int onlineNum) {
            this.onlineNum = onlineNum;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(int erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getRoomTag() {
            return roomTag;
        }

        public void setRoomTag(String roomTag) {
            this.roomTag = roomTag;
        }

        public int getTagId() {
            return tagId;
        }

        public void setTagId(int tagId) {
            this.tagId = tagId;
        }

        public String getTagPict() {
            return tagPict;
        }

        public void setTagPict(String tagPict) {
            this.tagPict = tagPict;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }
    }

    public static class MasterBannerBean {
        /**
         * bannerName : 周星
         * bannerPic : https://pic.lyy18.cn/FoZidVAy3Mm4os3dc8Rwt2bHd4mv?imageslim
         * skipType : 1
         * skipUri : http://beta.lyy18.cn/front/act05_20/index.html
         */

        private String bannerName;
        private String bannerPic;
        private int skipType;
        private String skipUri;

        public String getBannerName() {
            return bannerName;
        }

        public void setBannerName(String bannerName) {
            this.bannerName = bannerName;
        }

        public String getBannerPic() {
            return bannerPic;
        }

        public void setBannerPic(String bannerPic) {
            this.bannerPic = bannerPic;
        }

        public int getSkipType() {
            return skipType;
        }

        public void setSkipType(int skipType) {
            this.skipType = skipType;
        }

        public String getSkipUri() {
            return skipUri;
        }

        public void setSkipUri(String skipUri) {
            this.skipUri = skipUri;
        }
    }

    public static class VideoRoomTagListBean {
        /**
         * id : 12
         * name : 舞蹈
         * pict : https://pic.lyy18.cn/wudao@3x.png?imageslim
         * seq : 12
         * type : 1
         * status : true
         * istop : false
         * createTime : 1557228892000
         * description :
         * tmpint : 1
         * tmpstr :
         * children : 12
         * optPic : https://pic.173ing.com/jiaoyou@3x.png
         * defPic : https://pic.173ing.com/jiaoyoumoren@3x.png
         * isHot : 0
         */

        private int id;
        private String name;
        private String pict;
        private int seq;
        private int type;
        private boolean status;
        private boolean istop;
        private long createTime;
        private String description;
        private int tmpint;
        private String tmpstr;
        private String children;
        private String optPic;
        private String defPic;
        private int isHot;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPict() {
            return pict;
        }

        public void setPict(String pict) {
            this.pict = pict;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public boolean isIstop() {
            return istop;
        }

        public void setIstop(boolean istop) {
            this.istop = istop;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getTmpint() {
            return tmpint;
        }

        public void setTmpint(int tmpint) {
            this.tmpint = tmpint;
        }

        public String getTmpstr() {
            return tmpstr;
        }

        public void setTmpstr(String tmpstr) {
            this.tmpstr = tmpstr;
        }

        public String getChildren() {
            return children;
        }

        public void setChildren(String children) {
            this.children = children;
        }

        public String getOptPic() {
            return optPic;
        }

        public void setOptPic(String optPic) {
            this.optPic = optPic;
        }

        public String getDefPic() {
            return defPic;
        }

        public void setDefPic(String defPic) {
            this.defPic = defPic;
        }

        public int getIsHot() {
            return isHot;
        }

        public void setIsHot(int isHot) {
            this.isHot = isHot;
        }
    }

    public static class WealthRankVoListBean {
        /**
         * uid : 3245
         * erbanNo : 3511420
         * avatar : https://pic.lyy18.cn/FmQYggTJpkKOEcIvhUVXpbiEod_E?imageslim
         * nick : 滴滴
         * gender : 1
         * totalNum : 356680
         * experLevel : 28
         * charmLevel : 29
         * distance : 0
         */

        private int uid;
        private int erbanNo;
        private String avatar;
        private String nick;
        private int gender;
        private int totalNum;
        private int experLevel;
        private int charmLevel;
        private int distance;

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public int getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(int erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }

        public int getExperLevel() {
            return experLevel;
        }

        public void setExperLevel(int experLevel) {
            this.experLevel = experLevel;
        }

        public int getCharmLevel() {
            return charmLevel;
        }

        public void setCharmLevel(int charmLevel) {
            this.charmLevel = charmLevel;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }
    }

    public static class CharmRankVoListBean {
        /**
         * uid : 2907
         * erbanNo : 1718503
         * avatar : https://pic.lyy18.cn/FiSuVncmdkjuBUKyMma1vH-DaYd6?imageslim
         * nick : wuchen季韩版潮流单鞋女鞋
         * gender : 2
         * totalNum : 379752
         * experLevel : 50
         * charmLevel : 37
         * distance : 0
         */

        private int uid;
        private int erbanNo;
        private String avatar;
        private String nick;
        private int gender;
        private int totalNum;
        private int experLevel;
        private int charmLevel;
        private int distance;

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public int getErbanNo() {
            return erbanNo;
        }

        public void setErbanNo(int erbanNo) {
            this.erbanNo = erbanNo;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }

        public int getExperLevel() {
            return experLevel;
        }

        public void setExperLevel(int experLevel) {
            this.experLevel = experLevel;
        }

        public int getCharmLevel() {
            return charmLevel;
        }

        public void setCharmLevel(int charmLevel) {
            this.charmLevel = charmLevel;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }
    }
}
