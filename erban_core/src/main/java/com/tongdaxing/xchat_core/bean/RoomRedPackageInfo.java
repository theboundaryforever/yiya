package com.tongdaxing.xchat_core.bean;

import lombok.Data;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/1/8
 */
@Data
public class RoomRedPackageInfo {
    public static int REDPACKETTYPE_ALL = 1;// 所有人可领取
    public static int REDPACKETTYPE_OMMIC = 2;// 仅麦上用户可领取
    public static int REDPACKETDISPLAY_NO = 0;// 不通知全服
    public static int REDPACKETDISPLAY_ALL = 1;// 通知全服

    private String nick;
    private long redPackId;
    private int redpacketType;
}
