package com.tongdaxing.xchat_core.bean;

public class UserInRoom {

    private int uid;
    private int officeUser;
    private int roomId;
    private String title;
    private int type;
    private String meetingName;
    private boolean valid;
    private int operatorStatus;
    private String avatar;
    private String backPic;
    private long openTime;
    private int onlineNum;
    private int abChannelType;
    private String roomTag;
    private int tagId;
    private String tagPict;
    private int isPermitRoom;
    private boolean isExceptionClose;
    private int giftEffectSwitch;
    private int publicChatSwitch;
    private int factor;
    private int audioLevel;
    private int giftDrawEnable;
    private boolean exceptionClose;
    public void setUid(int uid) {
        this.uid = uid;
    }
    public int getUid() {
        return uid;
    }

    public void setOfficeUser(int officeUser) {
        this.officeUser = officeUser;
    }
    public int getOfficeUser() {
        return officeUser;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }
    public int getRoomId() {
        return roomId;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }

    public void setType(int type) {
        this.type = type;
    }
    public int getType() {
        return type;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }
    public String getMeetingName() {
        return meetingName;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
    public boolean getValid() {
        return valid;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }
    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public String getAvatar() {
        return avatar;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }
    public String getBackPic() {
        return backPic;
    }

    public void setOpenTime(long openTime) {
        this.openTime = openTime;
    }
    public long getOpenTime() {
        return openTime;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }
    public int getOnlineNum() {
        return onlineNum;
    }

    public void setAbChannelType(int abChannelType) {
        this.abChannelType = abChannelType;
    }
    public int getAbChannelType() {
        return abChannelType;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }
    public String getRoomTag() {
        return roomTag;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }
    public int getTagId() {
        return tagId;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }
    public String getTagPict() {
        return tagPict;
    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }
    public int getIsPermitRoom() {
        return isPermitRoom;
    }

    public void setIsExceptionClose(boolean isExceptionClose) {
        this.isExceptionClose = isExceptionClose;
    }
    public boolean getIsExceptionClose() {
        return isExceptionClose;
    }

    public void setGiftEffectSwitch(int giftEffectSwitch) {
        this.giftEffectSwitch = giftEffectSwitch;
    }
    public int getGiftEffectSwitch() {
        return giftEffectSwitch;
    }

    public void setPublicChatSwitch(int publicChatSwitch) {
        this.publicChatSwitch = publicChatSwitch;
    }
    public int getPublicChatSwitch() {
        return publicChatSwitch;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }
    public int getFactor() {
        return factor;
    }

    public void setAudioLevel(int audioLevel) {
        this.audioLevel = audioLevel;
    }
    public int getAudioLevel() {
        return audioLevel;
    }

    public void setGiftDrawEnable(int giftDrawEnable) {
        this.giftDrawEnable = giftDrawEnable;
    }
    public int getGiftDrawEnable() {
        return giftDrawEnable;
    }

    public void setExceptionClose(boolean exceptionClose) {
        this.exceptionClose = exceptionClose;
    }
    public boolean getExceptionClose() {
        return exceptionClose;
    }

}
