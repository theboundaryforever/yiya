package com.tongdaxing.xchat_core.bean;

public class HomeHotRoom {

    int abChannelType;
    String avatar;
    String backPic;
    String badge;
    int calcSumDataIndex;
    String city;
    int count;
    int erbanNo;
    Boolean exceptionClose;
    String exceptionCloseTime;
    int factor;
    int gender;
    int[] hideFace;
    String isExceptionClose;
    Boolean isNewHall;
    int isPermitRoom;
    int isRecom;
    Boolean isWaitingForRoom;
    String meetingName;
    String nick;
    int officeUser;
    int officialRoom;
    Boolean online;
    int onlineNum;
    String openTime;
    int operatorStatus;
    int recomSeq;
    String roomDesc;
    int roomId;
    String roomNotice;
    String roomPwd;
    String roomTag;
    int score;
    int seqNo;
    int status;
    int tagId;
    String tagPict;
    String title;
    int totalReceive;
    int type;
    int uid;
    Boolean valid;
    String waitingForRoomUrl;
    int weight;

    public int getAbChannelType() {
        return abChannelType;
    }

    public void setAbChannelType(int abChannelType) {
        this.abChannelType = abChannelType;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBackPic() {
        return backPic;
    }

    public void setBackPic(String backPic) {
        this.backPic = backPic;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public int getCalcSumDataIndex() {
        return calcSumDataIndex;
    }

    public void setCalcSumDataIndex(int calcSumDataIndex) {
        this.calcSumDataIndex = calcSumDataIndex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(int erbanNo) {
        this.erbanNo = erbanNo;
    }

    public Boolean getExceptionClose() {
        return exceptionClose;
    }

    public void setExceptionClose(Boolean exceptionClose) {
        this.exceptionClose = exceptionClose;
    }

    public String getExceptionCloseTime() {
        return exceptionCloseTime;
    }

    public void setExceptionCloseTime(String exceptionCloseTime) {
        this.exceptionCloseTime = exceptionCloseTime;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int[] getHideFace() {
        return hideFace;
    }

    public void setHideFace(int[] hideFace) {
        this.hideFace = hideFace;
    }

    public String getIsExceptionClose() {
        return isExceptionClose;
    }

    public void setIsExceptionClose(String isExceptionClose) {
        this.isExceptionClose = isExceptionClose;
    }

    public Boolean getNewHall() {
        return isNewHall;
    }

    public void setNewHall(Boolean newHall) {
        isNewHall = newHall;
    }

    public int getIsPermitRoom() {
        return isPermitRoom;
    }

    public void setIsPermitRoom(int isPermitRoom) {
        this.isPermitRoom = isPermitRoom;
    }

    public int getIsRecom() {
        return isRecom;
    }

    public void setIsRecom(int isRecom) {
        this.isRecom = isRecom;
    }

    public Boolean getWaitingForRoom() {
        return isWaitingForRoom;
    }

    public void setWaitingForRoom(Boolean waitingForRoom) {
        isWaitingForRoom = waitingForRoom;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getOfficeUser() {
        return officeUser;
    }

    public void setOfficeUser(int officeUser) {
        this.officeUser = officeUser;
    }

    public int getOfficialRoom() {
        return officialRoom;
    }

    public void setOfficialRoom(int officialRoom) {
        this.officialRoom = officialRoom;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public int getOnlineNum() {
        return onlineNum;
    }

    public void setOnlineNum(int onlineNum) {
        this.onlineNum = onlineNum;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public int getOperatorStatus() {
        return operatorStatus;
    }

    public void setOperatorStatus(int operatorStatus) {
        this.operatorStatus = operatorStatus;
    }

    public int getRecomSeq() {
        return recomSeq;
    }

    public void setRecomSeq(int recomSeq) {
        this.recomSeq = recomSeq;
    }

    public String getRoomDesc() {
        return roomDesc;
    }

    public void setRoomDesc(String roomDesc) {
        this.roomDesc = roomDesc;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getRoomNotice() {
        return roomNotice;
    }

    public void setRoomNotice(String roomNotice) {
        this.roomNotice = roomNotice;
    }

    public String getRoomPwd() {
        return roomPwd;
    }

    public void setRoomPwd(String roomPwd) {
        this.roomPwd = roomPwd;
    }

    public String getRoomTag() {
        return roomTag;
    }

    public void setRoomTag(String roomTag) {
        this.roomTag = roomTag;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getTagPict() {
        return tagPict;
    }

    public void setTagPict(String tagPict) {
        this.tagPict = tagPict;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTotalReceive() {
        return totalReceive;
    }

    public void setTotalReceive(int totalReceive) {
        this.totalReceive = totalReceive;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getWaitingForRoomUrl() {
        return waitingForRoomUrl;
    }

    public void setWaitingForRoomUrl(String waitingForRoomUrl) {
        this.waitingForRoomUrl = waitingForRoomUrl;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
