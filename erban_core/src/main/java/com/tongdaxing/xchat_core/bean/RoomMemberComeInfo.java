package com.tongdaxing.xchat_core.bean;

import lombok.Data;

/**
 * 房间人员进入消息
 */
@Data
public class RoomMemberComeInfo {
    private int experLevel;
    private String nickName;
    private String carImgUrl;
    private String carName;
    //用户等级
    private int videoRoomExperLevel;

    //用户等级图片
    private String videoRoomExperLevelPic;
}
