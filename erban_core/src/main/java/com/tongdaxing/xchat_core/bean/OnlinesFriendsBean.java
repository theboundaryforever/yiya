package com.tongdaxing.xchat_core.bean;

public class OnlinesFriendsBean {

        private int uid;
        private long erbanNo;
        private String nick;
        private String avatar;
        private int fansNum;
        private boolean valid;
        private int type;
        private int gender;
        private int operatorStatus;
        private UserInRoom userInRoom;
        private int experLevel;
        private int videoRoomExperLevel;
        private String videoRoomExperLevelPic;
        private boolean inRoom;
        public void setUid(int uid) {
            this.uid = uid;
        }
        public int getUid() {
            return uid;
        }

        public void setErbanNo(long erbanNo) {
            this.erbanNo = erbanNo;
        }
        public long getErbanNo() {
            return erbanNo;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }
        public String getNick() {
            return nick;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
        public String getAvatar() {
            return avatar;
        }

        public void setFansNum(int fansNum) {
            this.fansNum = fansNum;
        }
        public int getFansNum() {
            return fansNum;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }
        public boolean getValid() {
            return valid;
        }

        public void setType(int type) {
            this.type = type;
        }
        public int getType() {
            return type;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }
        public int getGender() {
            return gender;
        }

        public void setOperatorStatus(int operatorStatus) {
            this.operatorStatus = operatorStatus;
        }
        public int getOperatorStatus() {
            return operatorStatus;
        }

        public void setUserInRoom(UserInRoom userInRoom) {
            this.userInRoom = userInRoom;
        }
        public UserInRoom getUserInRoom() {
            return userInRoom;
        }

        public void setExperLevel(int experLevel) {
            this.experLevel = experLevel;
        }
        public int getExperLevel() {
            return experLevel;
        }

        public void setVideoRoomExperLevel(int videoRoomExperLevel) {
            this.videoRoomExperLevel = videoRoomExperLevel;
        }
        public int getVideoRoomExperLevel() {
            return videoRoomExperLevel;
        }

        public void setVideoRoomExperLevelPic(String videoRoomExperLevelPic) {
            this.videoRoomExperLevelPic = videoRoomExperLevelPic;
        }
        public String getVideoRoomExperLevelPic() {
            return videoRoomExperLevelPic;
        }

        public void setInRoom(boolean inRoom) {
            this.inRoom = inRoom;
        }
        public boolean getInRoom() {
            return inRoom;
        }
}
