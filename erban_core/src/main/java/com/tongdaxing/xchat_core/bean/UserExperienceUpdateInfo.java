package com.tongdaxing.xchat_core.bean;

import lombok.Data;

@Data
public class UserExperienceUpdateInfo {
    private String msg; // 消息文本
    private int uid;// 用户UID
    private int videoRoomLevelSeq; // 用户等级
    private String videoRoomLevelPic; // 用户等级图片
    private int experienceLevelSeq; // 财富等级
    private String experienceLevelPic; // 财富等级
    private int charmLevelSeq; // 魅力等级
    private String charmLevelPic; // 魅力等级
}
