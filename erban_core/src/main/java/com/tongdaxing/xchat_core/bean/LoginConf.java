package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/8/15
 *
 * @describe 着落页登陆配置
 */

@Data
public class LoginConf implements Serializable {
    private String channel;// 下载渠道
    private int gender;// 1 男 2 女
    private int pageCode;// 0视频页 1语聊页
    private int userType;// 0全部，1新，2老
}
