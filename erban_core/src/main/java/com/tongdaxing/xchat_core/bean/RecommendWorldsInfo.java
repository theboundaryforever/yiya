package com.tongdaxing.xchat_core.bean;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/18
 */
public class RecommendWorldsInfo {
    //{
    //    "code":200,
    //    "data":{
    //        "recommendWorlds":[
    //            {
    //                "text":"这软件怎么玩？",
    //                "color":"#C57EDF"
    //            },
    //            {
    //                "text":"大家好!",
    //                "color":"#7492E9"
    //            },
    //            {
    //                "text":"萌新求关注!",
    //                "color":"#C96C6C"
    //            },
    //            {
    //                "text":"这个怎么充值阿?",
    //                "color":"#C39B6B"
    //            },
    //            {
    //                "text":"有没有声音好听的？",
    //                "color":"#45B7A6"
    //            }
    //        ]
    //    },
    //    "message":"200:success"
    //}


    private List<RecommendWorldsBean> recommendWorlds;

    public List<RecommendWorldsBean> getRecommendWorlds() {
        return recommendWorlds;
    }

    public void setRecommendWorlds(List<RecommendWorldsBean> recommendWorlds) {
        this.recommendWorlds = recommendWorlds;
    }

    public static class RecommendWorldsBean {
        /**
         * text : 这软件怎么玩？
         * color : #C57EDF
         */

        private String text;
        private String color;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }
}
