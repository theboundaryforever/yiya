package com.tongdaxing.xchat_core.bean;

import com.tongdaxing.xchat_core.home.BannerInfo;

import java.io.Serializable;
import java.util.List;

public class HomeTalkInfo implements Serializable {

    private List<BannerInfo> banners;
    private List<HomeHotRoom> homeHotRoom;
    private List<SearchTagsBean> searchTags;

    public List<BannerInfo> getBannerInfoList() {
        return banners;
    }

    public void setBannerInfoList(List<BannerInfo> banners) {
        this.banners = banners;
    }

    public List<HomeHotRoom> getHomeHotRoomList() {
        return homeHotRoom;
    }

    public void setHomeHotRoomList(List<HomeHotRoom> homeHotRoom) {
        this.homeHotRoom = homeHotRoom;
    }
    public List<SearchTagsBean> getSearchTags() {
        return searchTags;
    }
    public void setSearchTags(List<SearchTagsBean> searchTags) {
        this.searchTags = searchTags;
    }
}
