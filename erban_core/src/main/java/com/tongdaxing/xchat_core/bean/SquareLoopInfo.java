package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/14.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class SquareLoopInfo implements Serializable {


    /**
     * room_id : 2
     * custom : {"first":15,"second":15,"data":{"msg":"才才","params":{"uid":4462,"avatar":"https://pic.lyy18.cn/Fj9gVgMOsw4s1mMKqSLa4NzW02Cs?imageslim","nick":"hd1563368217888","charmLevel":11,"experLevel":19,"redPackId":0},"server_msg_id":6}}
     * member : IMChatRoomMember(account=4462, nick=hd1563368217888, avatar=https://pic.lyy18.cn/Fj9gVgMOsw4s1mMKqSLa4NzW02Cs?imageslim, gender=1, headwear_url=, headwear_name=, car_name=, car_url=, exper_level=19, exper_level_pic=https://pic.lyy18.cn/exp_lv_19@2x.png, charm_level=11, charm_level_pic=https://pic.lyy18.cn/charm_lv_11@2x.png, video_room_exper_level=0, video_room_exper_level_pic=, create_time=0, age=0, province=null, city=null, is_online=false, is_mute=false, is_creator=false, is_new_user=false, is_manager=false, is_black_list=false, enter_time=0, online_num=0, timestamp=0, type=null, isDefaultInfo=false)
     * server_msg_id : 6
     */

    private String room_id;
    private Custom custom;
    private IMChatRoomMember member;
    private int server_msg_id;
    private String route;

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public Custom getCustom() {
        return custom;
    }

    public void setCustom(Custom custom) {
        this.custom = custom;
    }

    public IMChatRoomMember getMember() {
        return member;
    }

    public void setMember(IMChatRoomMember member) {
        this.member = member;
    }

    public int getServer_msg_id() {
        return server_msg_id;
    }

    public void setServer_msg_id(int server_msg_id) {
        this.server_msg_id = server_msg_id;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public static class Custom {
        /**
         * first : 15
         * second : 15
         * data : {"msg":"才才","params":{"uid":4462,"avatar":"https://pic.lyy18.cn/Fj9gVgMOsw4s1mMKqSLa4NzW02Cs?imageslim","nick":"hd1563368217888","charmLevel":11,"experLevel":19,"redPackId":0},"server_msg_id":6}
         */

        private int first;
        private int second;
        private Data data;

        public int getFirst() {
            return first;
        }

        public void setFirst(int first) {
            this.first = first;
        }

        public int getSecond() {
            return second;
        }

        public void setSecond(int second) {
            this.second = second;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public static class Data {
            /**
             * msg : 才才
             * params : {"uid":4462,"avatar":"https://pic.lyy18.cn/Fj9gVgMOsw4s1mMKqSLa4NzW02Cs?imageslim","nick":"hd1563368217888","charmLevel":11,"experLevel":19,"redPackId":0}
             * server_msg_id : 6
             */

            private String msg;
            private Params params;
            private int server_msg_id;
            private String html;
            private String loopHtml;

            public String getMsg() {
                return msg;
            }

            public void setMsg(String msg) {
                this.msg = msg;
            }

            public Params getParams() {
                return params;
            }

            public void setParams(Params params) {
                this.params = params;
            }

            public int getServer_msg_id() {
                return server_msg_id;
            }

            public void setServer_msg_id(int server_msg_id) {
                this.server_msg_id = server_msg_id;
            }

            public String getHtml() {
                return html;
            }

            public void setHtml(String html) {
                this.html = html;
            }

            public String getLoopHtml() {
                return loopHtml;
            }

            public void setLoopHtml(String loopHtml) {
                this.loopHtml = loopHtml;
            }

            public static class Params {
                /**
                 * uid : 4462
                 * avatar : https://pic.lyy18.cn/Fj9gVgMOsw4s1mMKqSLa4NzW02Cs?imageslim
                 * nick : hd1563368217888
                 * charmLevel : 11
                 * experLevel : 19
                 * redPackId : 0
                 */

                private int uid;
                private String avatar;
                private String nick;
                private int charmLevel;
                private int experLevel;
                private int redPackId;

                public int getUid() {
                    return uid;
                }

                public void setUid(int uid) {
                    this.uid = uid;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getNick() {
                    return nick;
                }

                public void setNick(String nick) {
                    this.nick = nick;
                }

                public int getCharmLevel() {
                    return charmLevel;
                }

                public void setCharmLevel(int charmLevel) {
                    this.charmLevel = charmLevel;
                }

                public int getExperLevel() {
                    return experLevel;
                }

                public void setExperLevel(int experLevel) {
                    this.experLevel = experLevel;
                }

                public int getRedPackId() {
                    return redPackId;
                }

                public void setRedPackId(int redPackId) {
                    this.redPackId = redPackId;
                }
            }
        }
    }
}
