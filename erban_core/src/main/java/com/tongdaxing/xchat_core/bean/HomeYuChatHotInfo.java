package com.tongdaxing.xchat_core.bean;

import java.util.List;

/**
 * @author Zhangsongzhou
 * @date 2019/3/23
 */
public class HomeYuChatHotInfo {


    /**
     * banners : []
     * homeIcons : []
     * hotRooms : [{"uid":33,"roomId":10008512,"title":"贺小梅0o的房间","type":3,"operatorStatus":1,"avatar":"https://pic.meibao8888.com/Fh0MaSLOor41KGEVstz3Ob8b__0R?imageslim","roomDesc":"","backPic":"","onlineNum":2,"gender":1,"nick":"贺小梅0o","erbanNo":7402847,"roomTag":"交友","calcSumDataIndex":0,"tagId":8,"tagPict":"https://pic.173ing.com/jiaoyou_biaoqian@3x.png","recomSeq":100,"isPermitRoom":2,"score":13.333333333,"isRecom":0,"count":0},{"uid":23,"roomId":10005138,"title":"123","type":4,"operatorStatus":1,"avatar":"https://pic.meibao8888.com/Fju-pTjSAGL-sP_Hn0SyXn9LVXgl?imageslim","roomDesc":"","backPic":"","onlineNum":1,"gender":2,"nick":"陈小吉","erbanNo":3367236,"roomTag":"交友","calcSumDataIndex":0,"tagId":8,"tagPict":"https://pic.173ing.com/jiaoyou_biaoqian@3x.png","recomSeq":100,"isPermitRoom":2,"score":6.666666666,"isRecom":0,"count":0}]
     * listRoom : []
     * listGreenRoom : []
     * roomTagList : [{"id":9,"name":"新秀","pict":"https://pic.173ing.com/xinxiu_biaoqian@3x.png","seq":10,"type":8,"status":true,"istop":false,"createTime":1511155717000,"description":"","tmpint":1,"tmpstr":"","children":"2,3,4,6,7,9","optPic":"https://pic.173ing.com/xinxiu@3x.png","defPic":"https://pic.173ing.com/xinxiumoren@3x.png"},{"id":8,"name":"交友","pict":"https://pic.173ing.com/jiaoyou_biaoqian@3x.png","seq":1,"type":1,"status":true,"istop":true,"createTime":1511155717000,"description":"","tmpint":1,"tmpstr":"","children":"8","optPic":"https://pic.173ing.com/jiaoyou@3x.png","defPic":"https://pic.173ing.com/jiaoyoumoren@3x.png"}]
     * viewType : 0
     */

    private int viewType;
    private List<?> banners;
    private List<?> homeIcons;
    private List<HomeYuChatListInfo> hotRooms;
    private List<HomeYuChatListInfo> listRoom;
    private List<?> listGreenRoom;
    private List<RoomTagListBean> roomTagList;


    public List<HomeYuChatListInfo> getHotRooms() {
        return hotRooms;
    }

    public void setHotRooms(List<HomeYuChatListInfo> hotRooms) {
        this.hotRooms = hotRooms;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public List<?> getBanners() {
        return banners;
    }

    public void setBanners(List<?> banners) {
        this.banners = banners;
    }

    public List<?> getHomeIcons() {
        return homeIcons;
    }

    public void setHomeIcons(List<?> homeIcons) {
        this.homeIcons = homeIcons;
    }


    public List<HomeYuChatListInfo> getListRoom() {
        return listRoom;
    }

    public void setListRoom(List<HomeYuChatListInfo> listRoom) {
        this.listRoom = listRoom;
    }

    public List<?> getListGreenRoom() {
        return listGreenRoom;
    }

    public void setListGreenRoom(List<?> listGreenRoom) {
        this.listGreenRoom = listGreenRoom;
    }

    public List<RoomTagListBean> getRoomTagList() {
        return roomTagList;
    }

    public void setRoomTagList(List<RoomTagListBean> roomTagList) {
        this.roomTagList = roomTagList;
    }


    public static class RoomTagListBean {
        /**
         * id : 9
         * name : 新秀
         * pict : https://pic.173ing.com/xinxiu_biaoqian@3x.png
         * seq : 10
         * type : 8
         * status : true
         * istop : false
         * createTime : 1511155717000
         * description :
         * tmpint : 1
         * tmpstr :
         * children : 2,3,4,6,7,9
         * optPic : https://pic.173ing.com/xinxiu@3x.png
         * defPic : https://pic.173ing.com/xinxiumoren@3x.png
         */

        private int id;
        private String name;
        private String pict;
        private int seq;
        private int type;
        private boolean status;
        private boolean istop;
        private long createTime;
        private String description;
        private int tmpint;
        private String tmpstr;
        private String children;
        private String optPic;
        private String defPic;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPict() {
            return pict;
        }

        public void setPict(String pict) {
            this.pict = pict;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public boolean isIstop() {
            return istop;
        }

        public void setIstop(boolean istop) {
            this.istop = istop;
        }

        public long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(long createTime) {
            this.createTime = createTime;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getTmpint() {
            return tmpint;
        }

        public void setTmpint(int tmpint) {
            this.tmpint = tmpint;
        }

        public String getTmpstr() {
            return tmpstr;
        }

        public void setTmpstr(String tmpstr) {
            this.tmpstr = tmpstr;
        }

        public String getChildren() {
            return children;
        }

        public void setChildren(String children) {
            this.children = children;
        }

        public String getOptPic() {
            return optPic;
        }

        public void setOptPic(String optPic) {
            this.optPic = optPic;
        }

        public String getDefPic() {
            return defPic;
        }

        public void setDefPic(String defPic) {
            this.defPic = defPic;
        }
    }
}
