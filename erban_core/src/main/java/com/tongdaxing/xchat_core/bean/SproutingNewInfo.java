package com.tongdaxing.xchat_core.bean;


import com.tongdaxing.xchat_core.home.BannerInfo;
import com.tongdaxing.xchat_core.user.bean.NewUserBean;

import java.util.List;

/**
 *  萌新列表
 * */
public class SproutingNewInfo {

    List<NewUserBean> newUsers;
    List<BannerInfo> banners;

    public List<NewUserBean> getNewUsers() {
        return newUsers;
    }

    public void setNewUsers(List<NewUserBean> newUsers) {
        this.newUsers = newUsers;
    }

    public List<BannerInfo> getBanners() {
        return banners;
    }

    public void setBanners(List<BannerInfo> banners) {
        this.banners = banners;
    }
}
