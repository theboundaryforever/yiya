package com.tongdaxing.xchat_core.bean;

/**首页标签*/
public class SearchTagsBean {

    String children;
    String createTime;
    String  defPic;
    String  description;
    int id;
    int isHot;
    Boolean istop;
    String name;
    String optPic;
    String pict;
    int seq;
    Boolean status;
    int  tmpint;
    String tmpstr;
    int  type;

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDefPic() {
        return defPic;
    }

    public void setDefPic(String defPic) {
        this.defPic = defPic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsHot() {
        return isHot;
    }

    public void setIsHot(int isHot) {
        this.isHot = isHot;
    }

    public Boolean getIstop() {
        return istop;
    }

    public void setIstop(Boolean istop) {
        this.istop = istop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOptPic() {
        return optPic;
    }

    public void setOptPic(String optPic) {
        this.optPic = optPic;
    }

    public String getPict() {
        return pict;
    }

    public void setPict(String pict) {
        this.pict = pict;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getTmpint() {
        return tmpint;
    }

    public void setTmpint(int tmpint) {
        this.tmpint = tmpint;
    }

    public String getTmpstr() {
        return tmpstr;
    }

    public void setTmpstr(String tmpstr) {
        this.tmpstr = tmpstr;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
