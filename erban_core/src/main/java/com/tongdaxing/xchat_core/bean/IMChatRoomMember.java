package com.tongdaxing.xchat_core.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.tongdaxing.xchat_framework.util.util.JavaUtil;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.io.Serializable;

import lombok.Data;

@Data
public class IMChatRoomMember implements Parcelable, Serializable {

    private long account;     // 成员账号
    private long erbanNo;

    /// 进入聊天室时提交
    private String nick;        // 聊天室内的昵称字段，预留字段，可从NimUserInfo中取，也可以由用户进聊天室时提交。
    private String avatar;      // 聊天室内的头像，预留字段，可从NimUserInfo中取avatar，可以由用户进聊天室时提交。
    private int gender;
    //头饰
    private String headwear_url;
    private String headwear_name;
    //座驾名称
    private String car_name;
    //座驾动画url
    private String car_url;
    private String is_new_user_pic;
    //财富等级
    private int exper_level;
    private String exper_level_pic;
    //魅力等级
    private int charm_level;
    private String charm_level_pic;
    //用户等级
    private int video_room_exper_level;
    //用户等级图片
    private String video_room_exper_level_pic;

    private long create_time;
    private int age;
    private String province;
    private String city;
    // 仅成员在线时有效
    private boolean is_online;   // 成员是否处于在线状态，仅特殊成员才可能离线，对游客/匿名用户而言只能是在线。
    private boolean is_mute;    // 是禁言用户
    private boolean is_creator;
    //是否是萌新
    private boolean is_new_user;
    private boolean is_manager;

    private boolean is_black_list;// 是否在黑名单中
    private boolean is_first_charge;//是否符合首充特惠
    private boolean is_on_mic;//是否在麦上
    private String medal_url;//专属勋章

    // 进入聊天室的时间点,对于离线成员该字段为空
    private long enter_time;
    private int online_num;//在线人数用于退出和进入消息
    private long timestamp;//在线人数时间戳用与判断在线人数更新的及时性

    private MemberType type;    // 成员类型：主要分为游客和非游客，非游客又分成受限用户、普通用户、创建者、管理员;

    private boolean isDefaultInfo;//是否是默认创建的信息，非服务器返回的信息

    public IMChatRoomMember() {
    }

    public IMChatRoomMember(com.netease.nimlib.sdk.chatroom.model.ChatRoomMember member) {
        this();
        if (member != null) {

            setAccount(member.getAccount());
            setMemberType(member.getMemberType());
            setNick(member.getNick());
            setAvatar(member.getAvatar());

            setIs_online(member.isOnline());
            setIs_black_list(member.isInBlackList());
            setIs_mute(member.isMuted());
            setEnter_time(member.getEnterTime());
        }
    }

    public boolean isIs_manager() {
        return is_manager;
    }

    public void setIs_manager(boolean is_manager) {
        this.is_manager = is_manager;
    }

    /**
     * 获取成员帐号
     *
     * @return 成员account
     */
    public String getAccount() {
        return String.valueOf(account);
    }

    /**
     * 获取成员类型
     * 成员类型：主要分为游客和非游客。
     * 非游客又分成受限用户、普通用户、创建者、管理员;
     *
     * @return 成员类型
     */
    public MemberType getMemberType() {
        return type;
    }

    /**
     * 获取昵称
     * 可从NimUserInfo中取，也可以由用户进聊天室时提交。
     *
     * @return 昵称
     */
    public String getNick() {
        return nick;
    }

    /**
     * 获取头像
     * 可从NimUserInfo中取avatar，可以由用户进聊天室时提交。
     *
     * @return 头像
     */
    public String getAvatar() {
        return avatar;
    }


    /**
     * 获取进入聊天室时间
     * 对于离线成员该字段为空
     *
     * @return 进入聊天室时间
     */
    public long getEnter_time() {
        return enter_time;
    }

    /**
     * 判断用户是否处于在线状态
     * 仅特殊成员才可能离线，对游客/匿名用户而言只能是在线。
     *
     * @return 是否在线
     */
    public boolean isIs_online() {
        return is_online;
    }

    /**
     * 判断用户是否在黑名单中
     *
     * @return 是否在黑名单中
     */
    public boolean isIs_black_list() {
        return is_black_list;
    }

    /**
     * 判断用户是否被禁言
     *
     * @return 是否被禁言
     */
    public boolean isIs_mute() {
        return is_mute;
    }


    /**
     * 设置用户帐号
     *
     * @param account 用户帐号
     */
    public void setAccount(String account) {
        this.account = JavaUtil.str2long(account);
    }

    /**
     * 设置成员类型
     *
     * @param type 成员类型
     */
    public void setMemberType(MemberType type) {
        this.type = type;
    }

    /**
     * 设置成员昵称
     *
     * @param nick 昵称
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * 设置成员头像
     *
     * @param avatar 头像
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


    /**
     * 设置在线状态
     *
     * @param is_online 在线状态
     */
    public void setIs_online(boolean is_online) {
        this.is_online = is_online;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 设置进入聊天室时间
     *
     * @param enter_time 进入聊天室时间
     */
    public void setEnter_time(long enter_time) {
        this.enter_time = enter_time;
    }

    /**
     * 设置是否在黑名单中
     *
     * @param is_black_list 是否设置为黑名单
     */
    public void setIs_black_list(boolean is_black_list) {
        this.is_black_list = is_black_list;
    }

    /**
     * 设置是否禁言
     *
     * @param is_mute 是否禁言
     */
    public void setIs_mute(boolean is_mute) {
        this.is_mute = is_mute;
    }


    public int getExperLevel() {
        return exper_level;
    }

    public void setExperLevel(int experLevel) {
        this.exper_level = experLevel;
    }

    public int getCharmLevel() {
        return charm_level;
    }

    public void setCharmLevel(int charmLevel) {
        this.charm_level = charmLevel;
    }

    public boolean isIs_new_user() {
        return is_new_user;
    }

    public void setIs_new_user(boolean is_new_user) {
        this.is_new_user = is_new_user;
    }

    public MemberType getType() {
        return type;
    }

    public void setType(MemberType type) {
        this.type = type;
    }

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public boolean getIs_new_user() {
        return is_new_user;
    }

    public String getHeadwear_url() {
        return headwear_url;
    }

    public void setHeadwear_url(String headwear_url) {
        this.headwear_url = headwear_url;
    }

    public String getCar_url() {
        return car_url;
    }

    public void setCar_url(String car_url) {
        this.car_url = car_url;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getOnline_num() {
        return online_num < 0 ? 0 : online_num;
    }

    public void setOnline_num(int online_num) {
        this.online_num = online_num;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDefaultInfo() {
        return isDefaultInfo;
    }

    public void setDefaultInfo(boolean defaultInfo) {
        isDefaultInfo = defaultInfo;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public String getNew_user_pic() {
        return is_new_user_pic;
    }

    public void setNew_user_pic(String is_new_user_pic) {
        this.is_new_user_pic = is_new_user_pic;
    }

    public Json toJson() {
        Json jsonObject = new Json();
        jsonObject.set("account", account);
        jsonObject.set("erbanNo", erbanNo);
        jsonObject.set("nick", nick);
        jsonObject.set("avatar", avatar);
        jsonObject.set("gender", gender);
        jsonObject.set("exper_level_pic", exper_level_pic);
        jsonObject.set("charm_level_pic", charm_level_pic);
        jsonObject.set("is_new_user_pic", is_new_user_pic);
        return jsonObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.account);
        dest.writeLong(this.erbanNo);
        dest.writeString(this.nick);
        dest.writeString(this.avatar);
        dest.writeInt(this.gender);
        dest.writeString(this.headwear_url);
        dest.writeString(this.headwear_name);
        dest.writeString(this.car_name);
        dest.writeString(this.car_url);
        dest.writeString(this.is_new_user_pic);
        dest.writeInt(this.exper_level);
        dest.writeString(this.exper_level_pic);
        dest.writeInt(this.charm_level);
        dest.writeString(this.charm_level_pic);
        dest.writeInt(this.video_room_exper_level);
        dest.writeString(this.video_room_exper_level_pic);
        dest.writeLong(this.create_time);
        dest.writeInt(this.age);
        dest.writeString(this.province);
        dest.writeString(this.city);
        dest.writeByte(this.is_online ? (byte) 1 : (byte) 0);
        dest.writeByte(this.is_mute ? (byte) 1 : (byte) 0);
        dest.writeByte(this.is_creator ? (byte) 1 : (byte) 0);
        dest.writeByte(this.is_new_user ? (byte) 1 : (byte) 0);
        dest.writeByte(this.is_manager ? (byte) 1 : (byte) 0);
        dest.writeByte(this.is_black_list ? (byte) 1 : (byte) 0);
        dest.writeByte(this.is_first_charge ? (byte) 1 : (byte) 0);
        dest.writeString(this.medal_url);
        dest.writeLong(this.enter_time);
        dest.writeInt(this.online_num);
        dest.writeLong(this.timestamp);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeByte(this.isDefaultInfo ? (byte) 1 : (byte) 0);
    }

    protected IMChatRoomMember(Parcel in) {
        this.account = in.readLong();
        this.erbanNo = in.readLong();
        this.nick = in.readString();
        this.avatar = in.readString();
        this.gender = in.readInt();
        this.headwear_url = in.readString();
        this.headwear_name = in.readString();
        this.car_name = in.readString();
        this.car_url = in.readString();
        this.is_new_user_pic = in.readString();
        this.exper_level = in.readInt();
        this.exper_level_pic = in.readString();
        this.charm_level = in.readInt();
        this.charm_level_pic = in.readString();
        this.video_room_exper_level = in.readInt();
        this.video_room_exper_level_pic = in.readString();
        this.create_time = in.readLong();
        this.age = in.readInt();
        this.province = in.readString();
        this.city = in.readString();
        this.is_online = in.readByte() != 0;
        this.is_mute = in.readByte() != 0;
        this.is_creator = in.readByte() != 0;
        this.is_new_user = in.readByte() != 0;
        this.is_manager = in.readByte() != 0;
        this.is_black_list = in.readByte() != 0;
        this.is_first_charge = in.readByte() != 0;
        this.medal_url = in.readString();
        this.enter_time = in.readLong();
        this.online_num = in.readInt();
        this.timestamp = in.readLong();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : MemberType.values()[tmpType];
        this.isDefaultInfo = in.readByte() != 0;
    }

    public static final Creator<IMChatRoomMember> CREATOR = new Creator<IMChatRoomMember>() {
        @Override
        public IMChatRoomMember createFromParcel(Parcel source) {
            return new IMChatRoomMember(source);
        }

        @Override
        public IMChatRoomMember[] newArray(int size) {
            return new IMChatRoomMember[size];
        }
    };
}