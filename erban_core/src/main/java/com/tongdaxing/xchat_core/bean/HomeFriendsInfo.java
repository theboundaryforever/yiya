package com.tongdaxing.xchat_core.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/3.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class HomeFriendsInfo implements MultiItemEntity {

    public static final int TYPE_ENTER_ROOM = 0;
    public static final int TYPE_EMPTY = 1;
    public static final int TYPE_ITEM = 2;
    public static final int TYPE_SQUARE = 3;

    private int itemType = TYPE_ITEM;

    private HomeYuChatListInfo homeYuChatListInfo;
    private List<SquareLoopInfo> squareLoopInfoList;

    public HomeFriendsInfo() {
    }

    public HomeFriendsInfo(int itemType) {
        this.itemType = itemType;
    }

    public static List<HomeFriendsInfo> generateList() {
        List<HomeFriendsInfo> list = new ArrayList<>();
        list.add(new HomeFriendsInfo(TYPE_ENTER_ROOM));
        list.add(new HomeFriendsInfo(TYPE_EMPTY));
        return list;
    }

    public HomeYuChatListInfo getHomeYuChatListInfo() {
        return homeYuChatListInfo;
    }

    public HomeFriendsInfo setHomeYuChatListInfo(HomeYuChatListInfo homeYuChatListInfo) {
        this.homeYuChatListInfo = homeYuChatListInfo;
        return this;
    }

    public List<SquareLoopInfo> getSquareLoopInfoList() {
        return squareLoopInfoList;
    }

    public HomeFriendsInfo setSquareLoopInfoList(List<SquareLoopInfo> squareLoopInfoList) {
        this.squareLoopInfoList = squareLoopInfoList;
        return this;
    }

    @Override
    public int getItemType() {
        return itemType;
    }
}
