package com.tongdaxing.xchat_core.bean;

/**
 * 文件描述：
 *
 * @auther：zwk
 * @data：2019/3/5
 */
public class RoomCharmInfo {
    private int value;
    private boolean withHat;
    private int type;//2：毒帽子


    public RoomCharmInfo() {
    }

    public RoomCharmInfo(int value, boolean withHat) {
        this.value = value;
        this.withHat = withHat;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isWithHat() {
        return withHat;
    }

    public void setWithHat(boolean withHat) {
        this.withHat = withHat;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
