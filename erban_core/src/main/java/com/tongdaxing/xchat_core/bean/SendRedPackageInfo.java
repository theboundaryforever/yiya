package com.tongdaxing.xchat_core.bean;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * 文件描述：
 * 红包信息的通用实体类
 *
 * @auther：zwk
 * @data：2019/1/7
 */
@Data
public class SendRedPackageInfo implements Serializable {
    private long redPackId;//红包id
    private long getCoin;//本人抢到的红包金额
    private double totalCoin;//红包总额
    private String senderNick;//红包发送人的昵称
    private String senderAvatar;//红包发送人的头像
    private String showUnit;//单位
    private List<ReceiveRedPackageInfo> hisList;
}
