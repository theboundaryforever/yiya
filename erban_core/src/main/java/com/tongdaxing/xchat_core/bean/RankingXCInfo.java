package com.tongdaxing.xchat_core.bean;

import java.util.List;

import lombok.Data;

/**
 * @author Zhangsongzhou
 * @date 2019/3/26
 */

@Data
public class RankingXCInfo {

    private long total;

    private List<ListBean> list;

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    @Data
    public static class ListBean {
        /**
         * ctrbUid : 2001
         * nick : cgy2
         * avatar : https://pic.173ing.com/FhRAtHbYRghw-fR6U3Q788nKKsls?imageslim
         * gender : 1
         * experLevel : 0
         * charmLevel : 0
         * goldSum : 10
         */

        private int rankingAvatarId = -1;

        private int ctrbUid;
        private String nick;
        private String avatar;
        private int gender;// 1是男 2是女
        private int experLevel;
        private String experLevelPic;
        private int charmLevel;
        private String charmLevelPic;
        private int goldSum = -1;
        private int sumGold = -1;
        //用户等级
        private int videoRoomExperLevel;

        //用户等级图片
        private String videoRoomExperLevelPic;
    }
}
