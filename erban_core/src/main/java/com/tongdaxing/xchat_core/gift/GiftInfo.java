package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Created by chenran on 2017/7/27.
 */

@Data
public class GiftInfo implements Serializable {
    private int giftId;
    private int giftType;
    private String giftName;
    private int goldPrice;
    private String giftUrl;
    private int seqNo;
    private boolean hasGifPic;
    private String gifUrl;
    private String gifFile;
    private boolean hasVggPic;
    private String vggUrl;
    private boolean hasLatest;//是否是最新礼物
    private boolean hasTimeLimit;//是否是限时礼物
    private boolean hasEffect;//是否有特效
    private boolean isLuckyGift;//是否是幸运礼物
    private String  luckyGiftTips;// 幸运礼物提示
    private int userGiftPurseNum;//抽奖礼物数量
    private int giftNum;//砸蛋砸中礼物数
    private boolean displayable = false;//在giftType为4时生效
    private boolean isNobleGift;// 是否是贵族礼物
    private List<String> subPicUrls;
}
