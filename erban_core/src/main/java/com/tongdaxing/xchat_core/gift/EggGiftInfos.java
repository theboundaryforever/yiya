package com.tongdaxing.xchat_core.gift;

import java.util.List;

import lombok.Data;

@Data
public class EggGiftInfos {
    List<EggGiftInfo> giftList;
}
