package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;

import lombok.Data;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/10/11.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
@Data
public class GiftSendInfo implements Serializable {

    /**
     * giftId : 300
     * giftPurseAmount : 0
     * goldCost : 1
     * displayable : true
     */

    private int giftId;
    private int giftPurseAmount;
    private int goldCost;
    private boolean displayable;

}
