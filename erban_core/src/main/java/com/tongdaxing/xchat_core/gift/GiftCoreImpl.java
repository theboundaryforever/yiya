package com.tongdaxing.xchat_core.gift;

import com.juxiao.library_utils.log.LogUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.DemoCache;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_core.common.ICommonClient;
import com.tongdaxing.xchat_core.im.custom.bean.GiftAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.IMCustomAttachment;
import com.tongdaxing.xchat_core.im.custom.bean.LotteryBoxAttachment;
import com.tongdaxing.xchat_core.im.room.IIMRoomCore;
import com.tongdaxing.xchat_core.im.room.IIMRoomCoreClient;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.pay.IPayCore;
import com.tongdaxing.xchat_core.pay.IPayCoreClient;
import com.tongdaxing.xchat_core.pk.IPKCoreClient;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.AbstractBaseCore;
import com.tongdaxing.xchat_framework.coremanager.CoreEvent;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.result.ServiceResult;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.im.IMReportRoute;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author chenran
 * @date 2017/7/27
 * 军圣说userGiftPurseNum、isNobleGift都不需要了。只需要判断displayable就行了
 */

public class GiftCoreImpl extends AbstractBaseCore implements IGiftCore {

    private GiftListInfo giftListInfo;
    private static List<IMCustomAttachment> giftQueue;

    public GiftCoreImpl() {
        CoreManager.addClient(this);
        giftListInfo = DemoCache.readGiftList();
        requestGiftInfos();
    }

    @Override
    public void requestGiftInfos() {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        params.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");

        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo != null){
            params.put("giftScreen", roomInfo.getType() + "");
        }

        OkHttpManager.getInstance().getRequest(UriProvider.getGiftList(), params, new OkHttpManager.MyCallBack<ServiceResult<GiftListInfo>>() {

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(ServiceResult<GiftListInfo> response) {
                if (response.isSuccess()) {
                    giftListInfo = response.getData();
                    DemoCache.saveGiftList(giftListInfo);// 写入本地缓存
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift);
                }
            }
        });
    }

    @Override
    public List<GiftInfo> getGiftInfosByType(int type) {
        if (giftListInfo != null && giftListInfo.getGift() != null && giftListInfo.getGift().size() > 0) {
            List<GiftInfo> giftInfos = new ArrayList<>();
            for (int i = 0; i < giftListInfo.getGift().size(); i++) {
                GiftInfo giftInfo = giftListInfo.getGift().get(i);
                if (giftInfo.getGiftType() == type) {
                    if (giftInfo.isDisplayable()) {
                        giftInfos.add(giftInfo);
                    }
                }
            }
            return giftInfos;
        } else {// 礼物信息为空 则获取一次礼物信息
            requestGiftInfos();
        }
        return new ArrayList<>();
    }

    @Override
    public List<GiftInfo> getGiftInfosAll() {
        if (giftListInfo != null && giftListInfo.getGift() != null && giftListInfo.getGift().size() > 0) {
            List<GiftInfo> giftInfos = new ArrayList<>();
            for (int i = 0; i < giftListInfo.getGift().size(); i++) {
                GiftInfo giftInfo = giftListInfo.getGift().get(i);
                if (giftInfo.isDisplayable()) {
                    giftInfos.add(giftInfo);
                }
            }
            return giftInfos;
        } else {// 礼物信息为空 则获取一次礼物信息
            requestGiftInfos();
        }
        return new ArrayList<>();
    }

    @Override
    public List<GiftInfo> getGiftInfosByType2And3() {
        if (giftListInfo != null && giftListInfo.getGift() != null && giftListInfo.getGift().size() > 0) {
            List<GiftInfo> giftInfos = new ArrayList<>();
            for (int i = 0; i < giftListInfo.getGift().size(); i++) {
                GiftInfo giftInfo = giftListInfo.getGift().get(i);
                if (giftInfo.getGiftType() == 2 || giftInfo.getGiftType() == 3) {
                    if (giftInfo.isDisplayable()) {
                        giftInfos.add(giftInfo);
                    }
                }
            }
            return giftInfos;
        } else {// 礼物信息为空 则获取一次礼物信息
            requestGiftInfos();
        }
        return new ArrayList<>();
    }

    @Override
    public List<GiftInfo> getGiftInfosByType2And4() { // 当前礼物只有2 3 4类型，2为普通礼物 3为神秘礼物 4为活动礼物
        if (giftListInfo != null && giftListInfo.getGift() != null && giftListInfo.getGift().size() > 0) {
            List<GiftInfo> giftInfos = new ArrayList<>();
            for (int i = 0; i < giftListInfo.getGift().size(); i++) {
                GiftInfo giftInfo = giftListInfo.getGift().get(i);
                if (giftInfo.getGiftType() == 2 || giftInfo.getGiftType() == 4) {
                    if (giftInfo.isDisplayable()) {
                        giftInfos.add(giftInfo);
                    }
                }
            }
            return giftInfos;
        } else {// 礼物信息为空 则获取一次礼物信息
            requestGiftInfos();
        }
        return new ArrayList<>();
    }

    @Override
    public void onReceiveChatRoomMessages(List<ChatRoomMessage> chatRoomMessageList) {
        if (chatRoomMessageList != null && chatRoomMessageList.size() > 0) {
            for (ChatRoomMessage msg : chatRoomMessageList) {
                if (IMReportRoute.sendMessageReport.equalsIgnoreCase(msg.getRoute())) {
                    IMCustomAttachment attachment = (IMCustomAttachment) msg.getAttachment();
                    if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                            || attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT
                            || attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT)
                    parseChatRoomAttachMent(attachment);
                }
            }
        }
    }

    @CoreEvent(coreClientClass = IIMRoomCoreClient.class)
    public void onSendRoomMessageSuccess(ChatRoomMessage msg) {
        if (IMReportRoute.sendMessageReport.equalsIgnoreCase(msg.getRoute())) {
            IMCustomAttachment attachment = (IMCustomAttachment) msg.getAttachment();
            if (attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT
                    || attachment.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT)
            parseChatRoomAttachMent(attachment);
        }
    }

    private static void parseChatRoomAttachMent(IMCustomAttachment attachMent) {
        if (attachMent.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT) {
            GiftAttachment giftAttachment = (GiftAttachment) attachMent;
            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_RECIEVE_GIFT_MSG, giftAttachment.getGiftRecieveInfo());
        } else if (attachMent.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT) {
            GiftAttachment multiGiftAttachment = (GiftAttachment) attachMent;
            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_MULTI_GIFT_MSG, multiGiftAttachment.getGiftRecieveInfo());
        } else if (attachMent.getFirst() == IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_SUPER_GIFT) {
            GiftAttachment giftAttachment = (GiftAttachment) attachMent;
            CoreManager.notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_SUPER_GIFT_MSG, giftAttachment.getGiftRecieveInfo());
        }
    }

    private void sendGiftMessage(GiftReceiveInfo giftReceiveInfo) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo == null || giftReceiveInfo == null) return;
        long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

        GiftAttachment giftAttachment = new GiftAttachment(IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_GIFT, IMCustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_GIFT);
        giftAttachment.setUid(myUid + "");
        giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
        ChatRoomMessage message = new ChatRoomMessage();
        message.setRoom_id(roomInfo.getRoomId() + "");
        message.setAttachment(giftAttachment);
        message.setImChatRoomMember(AvRoomDataManager.get().getMOwnerMember());
        CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
    }


    private void sendMultiGiftMessage(GiftReceiveInfo giftReceiveInfo, int price) {
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo != null && giftReceiveInfo != null) {
            long myUid = CoreManager.getCore(IAuthCore.class).getCurrentUid();

            GiftAttachment giftAttachment = new GiftAttachment(IMCustomAttachment.CUSTOM_MSG_HEADER_TYPE_MULTI_GIFT, IMCustomAttachment.CUSTOM_MSG_SUB_TYPE_SEND_MULTI_GIFT);
            giftAttachment.setUid(myUid + "");
            giftAttachment.setGiftRecieveInfo(giftReceiveInfo);
            ChatRoomMessage message = new ChatRoomMessage();
            message.setRoom_id(roomInfo.getRoomId() + "");
            message.setAttachment(giftAttachment);
            message.setImChatRoomMember(AvRoomDataManager.get().getMOwnerMember());
            CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
        }
    }


    @Override
    public void sendLotteryMeg(EggGiftInfo giftInfo, int count) {
        if (giftInfo == null)
            return;
        RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo == null) {
            LogUtil.i("sendLotteryMeg", "roomInfo is null");
            return;
        }

        LotteryBoxAttachment lotteryBoxAttachment = new LotteryBoxAttachment(IMCustomAttachment.CUSTOM_MSG_LOTTERY_BOX, IMCustomAttachment.CUSTOM_MSG_LOTTERY_BOX);
        String nick = "";
        UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
        if (userInfo != null) {
            nick = userInfo.getNick();
        }
        lotteryBoxAttachment.setNick(nick);
        lotteryBoxAttachment.setCount(count);
        lotteryBoxAttachment.setGiftName(giftInfo.getGiftName());
        lotteryBoxAttachment.setGoldPrice(giftInfo.getGoldPrice());
        lotteryBoxAttachment.setUid(CoreManager.getCore(IAuthCore.class).getCurrentUid());
        ChatRoomMessage message = new ChatRoomMessage();
        message.setRoom_id(roomInfo.getRoomId() + "");
        message.setAttachment(lotteryBoxAttachment);
        CoreManager.getCore(IIMRoomCore.class).sendMessage(message);
    }

    @Override
    public void sendRoomGift(int giftId, final long targetUid, long roomUid, int roomType, final int giftNum) {
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("giftId", giftId + "");
        params.put("targetUid", targetUid + "");
        params.put("uid", uid + "");
        params.put("ticket", ticket);
        params.put("roomUid", roomUid + "");
        params.put("giftNum", giftNum + "");
        if (targetUid == roomUid) {
            params.put("type", "1");
        } else {
            params.put("type", "3");
        }
        params.put("roomType", String.valueOf(roomType));
        OkHttpManager.getInstance().doPostRequest(UriProvider.sendGiftV3(), params, new OkHttpManager.MyCallBack<ServiceResult<GiftReceiveInfo>>() {

            @Override
            public void onError(Exception e) {
                notifyClients(IPayCoreClient.class, IPayCoreClient.METHOD_ON_WALLET_INFO_UPDATE, e.getMessage());
                notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_GIFT_FAIL, e.getMessage());
            }

            @Override
            public void onResponse(ServiceResult<GiftReceiveInfo> response) {
                if (response.isSuccess()) {
                    GiftReceiveInfo data = response.getData();
                    CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());
                    notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_GIFT, targetUid);
                    sendGiftMessage(data);
                    List<GiftInfo> giftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType2And3();
                    boolean needRefresh = false;
                    if (giftInfos != null) {
                        for (int i = 0; i < giftInfos.size(); i++) {
                            GiftInfo giftInfo = giftInfos.get(i);
                            if (giftInfo == null) {
                                continue;
                            }
                            if (giftInfo.getGiftId() == data.getGiftId()) {
                                giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
                                giftInfo.setDisplayable(data.isDisplayable());
                                needRefresh = true;
                            }

                        }
                    }
                    if (needRefresh) {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift);
                    }
                } else if (response.getCode() == 2103) {
                    notifyClients(ICommonClient.class, ICommonClient.METHOD_ON_RECIEVE_NEED_RECHARGE);
                } else if (response.getCode() == 8000) {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_PAST_DUE);
                    requestGiftInfos();
                } else if (response.getCode() == 8001) {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_MYSTERY_NOT_ENOUGH);
                } else {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_SEND_FAIL, response.getCode(), response.getMessage());
                }
            }
        });
    }


    @Override
    public void sendRoomMultiGift(int giftId, final List<Long> targetUids, long roomUid, int roomType, final int giftNum, final int goldPrice) {
        if (targetUids == null || targetUids.size() <= 0) {
            return;
        }
        Map<String, String> params = CommonParamUtil.getDefaultParam();
        long uid = CoreManager.getCore(IAuthCore.class).getCurrentUid();
        String ticket = CoreManager.getCore(IAuthCore.class).getTicket();
        params.put("giftId", giftId + "");
        params.put("uid", uid + "");
        params.put("ticket", ticket);
        params.put("roomUid", roomUid + "");
        params.put("giftNum", giftNum + "");
        params.put("roomType", roomType + "");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < targetUids.size(); i++) {
            long targetUid = targetUids.get(i);
            sb.append(targetUid + "");
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        params.put("targetUids", sb.toString());

        OkHttpManager.getInstance().doPostRequest(UriProvider.sendWholeGiftV3(), params, new OkHttpManager.MyCallBack<ServiceResult<GiftReceiveInfo>>() {

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onResponse(ServiceResult<GiftReceiveInfo> response) {
                if (response.isSuccess()) {
                    GiftReceiveInfo data = response.getData();
                    int price = goldPrice * giftNum * targetUids.size();
                    CoreManager.getCore(IPayCore.class).minusGold(data.getUseGiftPurseGold());

                    sendMultiGiftMessage(data, price);

                    List<GiftInfo> giftInfos = CoreManager.getCore(IGiftCore.class).getGiftInfosByType2And3();
                    boolean needRefresh = false;

                    for (int i = 0; i < giftInfos.size(); i++) {
                        GiftInfo giftInfo = giftInfos.get(i);
                        if (giftInfo == null) {
                            continue;
                        }
                        if (giftInfo.getGiftId() == data.getGiftId()) {
                            giftInfo.setUserGiftPurseNum(data.getUserGiftPurseNum());
                            giftInfo.setDisplayable(data.isDisplayable());
                            needRefresh = true;
                        }

                    }
                    notifyClients(IPKCoreClient.class, IPKCoreClient.METHOD_ON_PK_MULTI_GIFT, targetUids);
                    if (needRefresh) {
                        notifyClients(IGiftCoreClient.class, IGiftCoreClient.refreshFreeGift);
                    }
                } else if (response.getCode() == 2103) {
                    notifyClients(ICommonClient.class, ICommonClient.METHOD_ON_RECIEVE_NEED_RECHARGE);
                } else if (response.getCode() == 8000) {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_PAST_DUE);
                    requestGiftInfos();
                } else if (response.getCode() == 8001) {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_MYSTERY_NOT_ENOUGH);
                } else {
                    notifyClients(IGiftCoreClient.class, IGiftCoreClient.METHOD_ON_GIFT_SEND_FAIL, response.getCode(), response.getMessage());
                }
            }
        });
    }

    @Override
    public GiftInfo findGiftInfoById(int giftId) {
        GiftInfo giftInfo = null;
        if (giftListInfo != null && !ListUtils.isListEmpty(giftListInfo.getGift())) {
            for (int i = 0; i < giftListInfo.getGift().size(); i++) {
                if (giftListInfo.getGift().get(i).getGiftId() == giftId) {
                    giftInfo = giftListInfo.getGift().get(i);
                    return giftInfo;
                }
            }
        }
        return giftInfo;
    }
}