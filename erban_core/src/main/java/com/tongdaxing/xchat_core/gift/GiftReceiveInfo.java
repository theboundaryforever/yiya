package com.tongdaxing.xchat_core.gift;

import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * Created by chenran on 2017/7/28.
 */

@Data
public class GiftReceiveInfo implements Serializable {
    /********* 旧 **********/
    private long uid;

    public long getUid() {
        return sendUser.getUid();
    }

    private long targetUid;

    public long getTargetUid() {
        return targetUsers.get(0).getUid();
    }

    private int giftId;

    public int getGiftId() {
        return gift.getGiftId();
    }

    //    private int giftNum;
    private String targetNick;

    public String getTargetNick() {
        return targetUsers.get(0).getNick();
    }

    private String targetAvatar;

    public String getTargetAvatar() {
        return targetUsers.get(0).getAvatar();
    }

    private String nick;

    public String getNick() {
        return sendUser.getNick();
    }

    private String avatar;

    public String getAvatar() {
        return sendUser.getAvatar();
    }

    private int personCount;
    //    private String roomId;
    private String userNo;

    public String getUserNo() {
        return String.valueOf(sendUser.getErbanNo());
    }

    private int userGiftPurseNum;

    public int getUserGiftPurseNum() {
        return gift.getUserGiftPurseNum();
    }

    private int useGiftPurseGold;

    public int getUseGiftPurseGold() {
        return gift.getGoldPrice();
    }

    private long giftSendTime;

    public long getGiftSendTime() {
        return sendTimestamps;
    }
    //    private long comboId;// 礼物连击 为0 则非连击 不为0 则为每组连击的标记（时间戳）
//    private long comboCount;
//    private long comboFrequencyCount;
//    private int roomType;

    private List<Long> targetUids;

    public List<Long> getTargetUids() {
        ArrayList<Long> list = new ArrayList<>();
        for (UserInfo userInfo : targetUsers) {
            list.add(userInfo.getUid());
        }
        return list;
    }

    private boolean displayable = false;
    private GiftInfo giftInfo;

    public GiftInfo getGiftInfo() {
        return gift;
    }

    /********* 旧 **********/

    private UserInfo sendUser;

    private List<UserInfo> targetUsers;

    private Boolean sendAll;

    private String roomId;

    private Long roomUid;

    private int roomType;

    private int giftNum;

    private GiftInfo gift;

    private Long sendTimestamps;

    private long comboId;

    private long comboCount;

    private long comboFrequencyCount;



}
