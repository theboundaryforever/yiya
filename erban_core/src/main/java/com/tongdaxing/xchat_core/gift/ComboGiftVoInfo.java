package com.tongdaxing.xchat_core.gift;

import lombok.Data;

/**
 * Created by Administrator on 2019/4/7.
 * 礼物连击展示层bean
 */
@Data
public class ComboGiftVoInfo {
    /**
     * LOADING :  加载中
     * DISAPPEARING :   消失中
     * DISAPPEARED  :   已消失
     */
    public enum GiftAnim {
        LOADING, DISAPPEARING, DISAPPEARED
    }

    public ComboGiftVoInfo() {
    }

    private long uid;
    private long giftId;
    private long startTime;

    private int currentNum;
    private String nick;
    private String avatar;
    private String content;
    private String giftUrl;
    private String comboId;
    private long comboCount;
    private long comboFrequencyCount;

    private GiftAnim giftAnim;

    public long getComboFrequencyCount() {
        return comboFrequencyCount;
    }

    public void setComboFrequencyCount(long comboFrequencyCount) {
        this.comboFrequencyCount = comboFrequencyCount;
    }

    public long getComboCount() {
        return comboCount;
    }

    public void setComboCount(long comboCount) {
        this.comboCount = comboCount;
    }

    public String getComboId() {
        return comboId;
    }

    public void setComboId(String comboId) {
        this.comboId = comboId;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getGiftId() {
        return giftId;
    }

    public void setGiftId(long giftId) {
        this.giftId = giftId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public int getCurrentNum() {
        return currentNum;
    }

    public void setCurrentNum(int currentNum) {
        this.currentNum = currentNum;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public GiftAnim getGiftAnim() {
        return giftAnim;
    }

    public void setGiftAnim(GiftAnim giftAnim) {
        this.giftAnim = giftAnim;
    }
}
