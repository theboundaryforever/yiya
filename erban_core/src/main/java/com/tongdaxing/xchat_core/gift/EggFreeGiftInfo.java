package com.tongdaxing.xchat_core.gift;

import java.io.Serializable;
import java.util.List;

/**
 * 砸蛋的礼物
 * Created by zwk on 2018/9/13.
 */
public class EggFreeGiftInfo implements Serializable {
    private int freeDrawGiftTimes;
    private List<EggGiftInfo> freeDrawGiftList;

    public int getFreeDrawGiftTimes() {
        return freeDrawGiftTimes;
    }

    public void setFreeDrawGiftTimes(int freeDrawGiftTimes) {
        this.freeDrawGiftTimes = freeDrawGiftTimes;
    }

    public List<EggGiftInfo> getFreeDrawGiftList() {
        return freeDrawGiftList;
    }

    public void setFreeDrawGiftList(List<EggGiftInfo> freeDrawGiftList) {
        this.freeDrawGiftList = freeDrawGiftList;
    }
}
