package com.tongdaxing.xchat_core.gift;

import com.tongdaxing.xchat_core.bean.ChatRoomMessage;
import com.tongdaxing.xchat_framework.coremanager.IBaseCore;

import java.util.List;

/**
 * Created by chenran on 2017/7/27.
 */

public interface IGiftCore extends IBaseCore {

    int MAGIC_LIST = 5;
    int LUCKY_LIST = 6;

    List<GiftInfo> getGiftInfosByType(int type);

    /**
     * 获取所有礼物
     * @return
     */
    List<GiftInfo> getGiftInfosAll();

    /**
     * 获取普通礼物和神秘礼物
     * @return
     */
    List<GiftInfo> getGiftInfosByType2And3();

    /**
     * 获取普通礼物和库存礼物
     * @return
     */
    List<GiftInfo> getGiftInfosByType2And4();

    void sendLotteryMeg(EggGiftInfo giftInfo, int count);

    void sendRoomMultiGift(int giftId, List<Long> targetUids, long roomUid, int roomType, int giftNum, int goldPrice);

    void sendRoomGift(int giftId, long targetUid, long roomUid, int roomType, int giftNum);

    GiftInfo findGiftInfoById(int giftId);

    void requestGiftInfos();

    void onReceiveChatRoomMessages(List<ChatRoomMessage> chatRoomMessageList);
}