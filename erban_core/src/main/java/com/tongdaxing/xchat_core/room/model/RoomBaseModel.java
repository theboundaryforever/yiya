package com.tongdaxing.xchat_core.room.model;

import android.text.TextUtils;
import android.util.SparseArray;

import com.netease.nimlib.sdk.NIMChatRoomSDK;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.chatroom.constant.MemberQueryType;
import com.netease.nimlib.sdk.chatroom.constant.MemberType;
import com.netease.nimlib.sdk.chatroom.model.ChatRoomInfo;
import com.netease.nimlib.sdk.util.Entry;
import com.tongdaxing.erban.libcommon.listener.CallBack;
import com.tongdaxing.erban.libcommon.net.exception.ErrorThrowable;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.erban.libcommon.net.rxnet.RxNet;
import com.tongdaxing.erban.libcommon.net.rxnet.callback.HttpRequestCallBack;
import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomMicInfo;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_core.manager.IMNetEaseManager;
import com.tongdaxing.xchat_core.manager.ReUsedSocketManager;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;
import com.tongdaxing.xchat_core.room.bean.RoomInfo;
import com.tongdaxing.xchat_core.user.IUserCore;
import com.tongdaxing.xchat_core.user.bean.UserInfo;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.im.IMProCallBack;
import com.tongdaxing.xchat_framework.im.IMReportBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * <p>房间相关操作model </p>
 *
 * @author jiahui
 * @date 2017/12/19
 */
public class RoomBaseModel extends BaseMvpModel {
    /**
     * 一页房间人数
     */
    protected static final int ROOM_MEMBER_SIZE = 50;


    /**
     * 获取固定成员列表(操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 请求数量
     */
    public Single<List<IMChatRoomMember>> queryNormalList(int limit, long time) {
        return fetchRoomMembers(MemberQueryType.NORMAL, time, limit);
    }

    /**
     * 获取最新固定在线人数(操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 请求数量
     */
    public Single<List<IMChatRoomMember>> queryOnlineList(int limit) {
        return fetchRoomMembers(MemberQueryType.ONLINE_NORMAL, 0, limit);
    }


    /**
     * 获取在线游客列表 (操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 获取数量大小
     * @param time  从当前时间开始查找
     */
    public Single<List<IMChatRoomMember>> queryGuestList(int limit, long time) {
        return fetchRoomMembers(MemberQueryType.GUEST, time, limit);
    }

    private Single<List<IMChatRoomMember>> fetchRoomMembers(final MemberQueryType memberType,
                                                            final long time, final int limit) {
        final RoomInfo roomInfo = AvRoomDataManager.get().getRoomInfo();
        if (roomInfo == null)
            return Single.error(new ErrorThrowable(ErrorThrowable.ROOM_INFO_NULL_ERROR));
        return Single.create(
                new SingleOnSubscribe<List<com.netease.nimlib.sdk.chatroom.model.ChatRoomMember>>() {
                    @Override
                    public void subscribe(SingleEmitter<List<com.netease.nimlib.sdk.chatroom.model.ChatRoomMember>> e) throws Exception {
                        executeNIMClient(NIMClient.syncRequest(NIMChatRoomSDK.getChatRoomService()
                                .fetchRoomMembers(String.valueOf(roomInfo.getRoomId()), memberType, time, limit)), e);
                    }
                }).map(new Function<List<com.netease.nimlib.sdk.chatroom.model.ChatRoomMember>, List<IMChatRoomMember>>() {
            @Override
            public List<IMChatRoomMember> apply(List<com.netease.nimlib.sdk.chatroom.model.ChatRoomMember> chatRoomMembers) throws Exception {
                List<IMChatRoomMember> members = new ArrayList<>();
                if (chatRoomMembers != null && chatRoomMembers.size() > 0) {
                    for (int i = 0; i < chatRoomMembers.size(); i++) {
                        IMChatRoomMember chatRoomMember = new IMChatRoomMember(chatRoomMembers.get(i));
                        members.add(chatRoomMember);
                    }
                }
                return members;
            }
        }).subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io());
    }

    /**
     * 获取管理员列表 (操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit -
     * @return --
     */
    public Single<List<IMChatRoomMember>> queryManagerList(int limit) {
        return queryNormalList(limit, 0)
                .map(new Function<List<IMChatRoomMember>, List<IMChatRoomMember>>() {
                    @Override
                    public List<IMChatRoomMember> apply(List<IMChatRoomMember> chatRoomMemberList) throws Exception {
                        List<IMChatRoomMember> managerList = null;
                        if (!ListUtils.isListEmpty(chatRoomMemberList)) {
                            managerList = new ArrayList<>();
                            for (IMChatRoomMember chatRoomMember : chatRoomMemberList) {
                                if (chatRoomMember.getMemberType() == MemberType.ADMIN) {
                                    managerList.add(chatRoomMember);
                                }
                            }
                        }
                        return managerList;
                    }
                });
    }


    /**
     * 获取最新黑名单列表 (操作在子线程，需要在主线程操作的自己转)
     *
     * @param limit 数量
     * @return
     */
    public Single<List<IMChatRoomMember>> queryBlackList(int limit) {
        return queryNormalList(limit, 0)
                .map(new Function<List<IMChatRoomMember>, List<IMChatRoomMember>>() {
                    @Override
                    public List<IMChatRoomMember> apply(List<IMChatRoomMember> chatRoomMemberList) throws Exception {
                        List<IMChatRoomMember> blackList = null;
                        if (!ListUtils.isListEmpty(chatRoomMemberList)) {
                            blackList = new ArrayList<>();
                            for (IMChatRoomMember chatRoomMember : chatRoomMemberList) {
                                if (chatRoomMember.isIs_black_list()) {
                                    blackList.add(chatRoomMember);
                                }
                            }
                        }
                        return blackList;
                    }
                });
    }


    /**
     * 设置管理员
     *
     * @param roomId
     * @param account
     * @param mark    true:设置管理员 ,false：移除管理员
     */
    public void markManagerList(long roomId, String account, boolean mark, OkHttpManager.MyCallBack callBack) {
        IMNetEaseManager.get().markManager(account, mark, callBack);
    }


//    /**
//     * @param account 被操作的用户uid
//     * @param is_add  1添加，0移除
//     */
//    public void markBlackList(String roomId, String account, boolean is_add, OkHttpManager.MyCallBack myCallBack) {
//        IMNetEaseManager.get().addRoomBlackList(roomId, account, is_add, myCallBack);
//    }


    /**
     * 邀请上麦
     *
     * @param micUid   上麦用户uid
     * @param position
     * @return Single<ChatRoomMessage>
     */
    public void inviteMicroPhone(long micUid, int position) {
        IMNetEaseManager.get().inviteMicroPhoneBySdk(micUid, position);
    }


    public void upMicroPhone(final int micPosition, final String uId, final String roomId,
                             boolean isInviteUpMic, final CallBack<String> callBack) {
        upMicroPhone(micPosition, uId, roomId, isInviteUpMic, callBack, false);
    }


    /**
     * 上麦
     *
     * @param micPosition
     * @param uId           要上麦的用户id
     * @param roomId
     * @param isInviteUpMic 是否是主动的
     * @param callBack
     */
    public void upMicroPhone(final int micPosition, final String uId, final String roomId,
                             boolean isInviteUpMic, final CallBack<String> callBack, boolean needCloseMic) {
        RoomQueueInfo roomQueueInfo = AvRoomDataManager.get().mMicQueueMemberMap.get(micPosition);

        if (roomQueueInfo == null) {
            return;
        }
        IMChatRoomMember chatRoomMember = roomQueueInfo.mChatRoomMember;
        final RoomMicInfo roomMicInfo = roomQueueInfo.mRoomMicInfo;

        //坑上没人且没锁
        if (roomMicInfo != null
                && ((!roomMicInfo.isMicLock() || AvRoomDataManager.get().isRoomOwner(uId)
                || AvRoomDataManager.get().isRoomAdmin(uId))
                || isInviteUpMic)
                && chatRoomMember == null) {
            final UserInfo userInfo = CoreManager.getCore(IUserCore.class).getCacheLoginUserInfo();
            if (userInfo != null) {
//                //先看下这个用户是否在麦上
//                if (AvRoomDataManager.get().isOnMic(userInfo.getUid())) {
//                    int position = AvRoomDataManager.get().getMicPosition(userInfo.getUid());
//
//                    if (position == -1)
//                        return;
//                    //下麦
//                    downMicroPhone(position, new CallBack<String>() {
//                        @Override
//                        public void onSuccess(String data) {
//                            updateQueueEx(micPosition, roomId, callBack, userInfo,-1);
//                        }
//
//                        @Override
//                        public void onFail(int code, String error) {
//                            if (callBack != null) {
//                                callBack.onFail(-1, "上麦导致下麦失败");
//                            }
//                        }
//                    });
//                } else {
                updateQueueEx(micPosition, roomId, callBack, userInfo, -1);
//                }
            }
        }
    }

    /**
     * 下麦
     *
     * @param micPosition
     * @param callBack
     */
    public void downMicroPhone(int micPosition, final CallBack<String> callBack) {
        IMNetEaseManager.get().downMicroPhoneBySdk(micPosition, callBack);

    }
//
//    public void updateQueueEx(int micPosition, String roomId, final CallBack<String> callBack, UserInfo userInfo) {
//        updateQueueEx(micPosition, roomId, callBack, userInfo, -1);
//    }

    //
    public void updateQueueEx(int micPosition, String roomId, final CallBack<String> callBack, UserInfo userInfo, int type) {
        updataQueueExBySdk(micPosition, roomId, callBack, userInfo.getUid(), true);
    }
//
//
//    public void updataQueueExBySdk(int micPosition, String roomId, final CallBack<String> callBack,long  uid) {
//        updataQueueExBySdk(micPosition, roomId, callBack, uid, true);
//    }

    public void updataQueueExBySdk(int micPosition, String roomId, final CallBack<String> callBack, long uid, boolean isTransient) {
        //防止房主掉麦
        if (micPosition == -1) {
            String currentUid = String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid());
            if (!AvRoomDataManager.get().isRoomOwner(currentUid)) {
                return;
            }
        }
        ReUsedSocketManager.get().updateQueue(roomId, micPosition, uid, new IMProCallBack() {
            @Override
            public void onSuccessPro(IMReportBean imReportBean) {
                if (imReportBean == null || imReportBean.getReportData() == null) {
                    if (callBack != null) {
                        callBack.onFail(-1, "数据异常！");
                    }
                    return;
                }
                if (imReportBean.getReportData().errno == 0) {
                    if (callBack != null) {
                        callBack.onSuccess("上麦成功");
                    }
                } else {
                    if (callBack != null) {
                        callBack.onFail(imReportBean.getReportData().errno, "上麦失败:" + imReportBean.getReportData().errmsg);
                    }
                }
            }

            @Override
            public void onError(int errorCode, String errorMsg) {
                if (callBack != null) {
                    callBack.onFail(errorCode, "上麦失败:" + errorMsg);
                }
            }
        });
    }

    public void getMembers(int index, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> imDefaultParamsMap = IMNetEaseManager.get().getImDefaultParamsMap();
        if (imDefaultParamsMap == null)
            return;

        imDefaultParamsMap.put("limit", "20");
        imDefaultParamsMap.put("start", index + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.fetchRoomMembers(), imDefaultParamsMap, myCallBack);
    }

    public List<OnlineChatMember> memberToOnlineMember(int page, List<IMChatRoomMember> imChatRoomMembers, List<OnlineChatMember> oldList) {
        List<OnlineChatMember> onlineChatMembers = new ArrayList<>();
        for (IMChatRoomMember member : imChatRoomMembers) {
            onlineChatMembers.add(new OnlineChatMember(member, false, member.isIs_manager(),
                    AvRoomDataManager.get().isRoomOwner(member.getAccount())));

        }

        if (page > 1 && !ListUtils.isListEmpty(oldList)) {
            onlineChatMembers.addAll(0, oldList);
        }
        return onlineChatMembers;

    }

    public List<OnlineChatMember> memberToOnlineMember(List<IMChatRoomMember> imChatRoomMembers, boolean filterOnMic, List<OnlineChatMember> oldList) {


        Set<String> micUIds = new HashSet<>();


        //排除麦上的用户
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                micUIds.add(account);
            }

        }
        Set<String> newListUids = new HashSet<>();

        List<OnlineChatMember> onlineChatMembers = new ArrayList<>();


        for (IMChatRoomMember member : imChatRoomMembers) {
            String account = member.getAccount();
            boolean onMic = micUIds.contains(account);
            //过滤麦上用户
            newListUids.add(account);
            if (onMic && filterOnMic) {
                continue;
            }
            onlineChatMembers.add(new OnlineChatMember(member,
                    onMic,
                    member.isIs_manager(),
                    AvRoomDataManager.get().isRoomOwner(account)));

        }


        if (!ListUtils.isListEmpty(oldList)) {
            //排重
            List<OnlineChatMember> repeatData = new ArrayList<>();
            for (OnlineChatMember onlineChatMember : oldList) {
                IMChatRoomMember chatRoomMember = onlineChatMember.chatRoomMember;
                if (chatRoomMember == null)
                    continue;
                String account = chatRoomMember.getAccount();
                if (newListUids.contains(account))
                    repeatData.add(onlineChatMember);
            }
            oldList.removeAll(repeatData);


            onlineChatMembers.addAll(0, oldList);
        }


//        //房主-管理-游客排序,用户身份有可能会变化，需要重新排序
//        Collections.sort(onlineChatMembers, new Comparator<OnlineChatMember>() {
//            @Override
//            public int compare(OnlineChatMember o1, OnlineChatMember o2) {
//                if (o1.isRoomOwer)
//                    return -1;
//                if (o2.isRoomOwer)
//                    return 1;
//
//                if (o1.isAdmin == o2.isAdmin) {
//                    return -1;
//                } else {
//                    if (o1.isAdmin) {
//                        return -1;
//                    } else {
//                        return 1;
//                    }
//                }
//            }
//        });

        return onlineChatMembers;
    }


    /**
     * 查询房间管理员列表
     *
     * @param myCallBack
     */
    public void fetchRoomManagers(OkHttpManager.MyCallBack myCallBack) {
        OkHttpManager.getInstance().doPostRequest(UriProvider.fetchRoomManagers(), IMNetEaseManager.get().getImDefaultParamsMap(), myCallBack);
    }

    /**
     * 查询房间黑名单列表
     *
     * @param start
     * @param limit
     * @param myCallBack
     */
    public void fetchRoomBlackList(int start, int limit, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> imDefaultParamsMap = IMNetEaseManager.get().getImDefaultParamsMap();
        if (imDefaultParamsMap == null)
            return;
        imDefaultParamsMap.put("limit", limit + "");
        imDefaultParamsMap.put("start", start + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.fetchRoomBlackList(), imDefaultParamsMap, myCallBack);
    }
}
