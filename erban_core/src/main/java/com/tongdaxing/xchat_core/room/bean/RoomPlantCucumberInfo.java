package com.tongdaxing.xchat_core.room.bean;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/5.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomPlantCucumberInfo {

    private long time;//时间戳
    private long goldPool;//金币池

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getGoldPool() {
        return goldPool;
    }

    public void setGoldPool(long goldPool) {
        this.goldPool = goldPool;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RoomPlantCucumberInfo{");
        sb.append("time=").append(time);
        sb.append(", goldPool=").append(goldPool);
        sb.append('}');
        return sb.toString();
    }
}
