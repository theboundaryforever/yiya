package com.tongdaxing.xchat_core.room.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ComboGift {
    private long comboId;// 礼物连击 为0 则非连击 不为0 则为每组连击的标记（时间戳）
    private long comboCount;
    private long comboFrequencyCount;
}
