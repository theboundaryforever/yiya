package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/9/9
 *
 * @describe 特惠礼物
 */

@Data
public class DiscountGift {
    private int giftId;
    private int giftNum;
}
