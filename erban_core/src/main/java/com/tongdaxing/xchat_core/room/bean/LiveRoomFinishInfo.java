package com.tongdaxing.xchat_core.room.bean;

/**
 * @author Zhangsongzhou
 * @date 2019/6/11
 */
public class LiveRoomFinishInfo {


    /**
     * uid : 2922
     * roomId : 2984
     * liveTime : 0:0:53
     * redBeanNum : null
     * watchNum : 0
     * fansNum : 0
     */

    private int uid;
    private int roomId;
    private String liveTime;
    private double redBeanNum;
    private int watchNum;
    private int fansNum;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getLiveTime() {
        return liveTime;
    }

    public void setLiveTime(String liveTime) {
        this.liveTime = liveTime;
    }

    public double getRedBeanNum() {
        return redBeanNum;
    }

    public void setRedBeanNum(double redBeanNum) {
        this.redBeanNum = redBeanNum;
    }

    public int getWatchNum() {
        return watchNum;
    }

    public void setWatchNum(int watchNum) {
        this.watchNum = watchNum;
    }

    public int getFansNum() {
        return fansNum;
    }

    public void setFansNum(int fansNum) {
        this.fansNum = fansNum;
    }
}
