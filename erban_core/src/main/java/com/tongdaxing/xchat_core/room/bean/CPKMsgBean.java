package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

/**
 * @author zdw
 * @data 2019/7/23
 *
 * @describe 魅力值PK消息bean
 */

@Data
public class CPKMsgBean {
    public static final int CPKMSG_TYPE_NORMAL = 1;// 普通消息
    public static final int CPKMSG_TYPE_START = 2;// 开始

    private String message;// 文本内容
    private int type; // 1是普通消息 2是开始
    private int expireSeconds;// 比赛总时长
    private long roomId;// 比赛总时长
}
