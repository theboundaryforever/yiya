package com.tongdaxing.xchat_core.room.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.tongdaxing.xchat_core.user.bean.UserInfo;

import java.io.Serializable;
import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/6/10.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomMicroApplyInfo implements Serializable {

    private int total;//申请人数
    private List<MicroApplyInfo> applyList;//申请列表
    private long timestamp;
    private long applyUid;//申请用户id
    private UserInfo userInfo;


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<MicroApplyInfo> getApplyList() {
        return applyList;
    }

    public void setApplyList(List<MicroApplyInfo> applyList) {
        this.applyList = applyList;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public long getApplyUid() {
        return applyUid;
    }

    public void setApplyUid(long applyUid) {
        this.applyUid = applyUid;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RoomMicroApplyInfo{");
        sb.append("total=").append(total);
        sb.append(", applyList=").append(applyList);
        sb.append(", timestamp=").append(timestamp);
        sb.append(", applyUid=").append(applyUid);
        sb.append(", userInfo=").append(userInfo);
        sb.append('}');
        return sb.toString();
    }

    public static class MicroApplyInfo implements Parcelable, MultiItemEntity {

        public static final int MICRO_APPLY_NORMAL = 0;
        public static final int MICRO_APPLY_CONNECTING = 1;

        /**
         * avatar : string
         * experLevel : 0
         * gender : 0
         * nick : string
         * score : 0
         * type : 0
         * uid : 0
         */

        private String avatar;
        private int experLevel;
        private int gender;
        private String nick;
        private int score;
        private int type;
        private int uid;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getExperLevel() {
            return experLevel;
        }

        public void setExperLevel(int experLevel) {
            this.experLevel = experLevel;
        }

        public int getGender() {
            return gender;
        }

        public void setGender(int gender) {
            this.gender = gender;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public boolean isAudioType() {
            return type == 0;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("MicroApplyInfo{");
            sb.append("avatar='").append(avatar).append('\'');
            sb.append(", experLevel=").append(experLevel);
            sb.append(", gender=").append(gender);
            sb.append(", nick='").append(nick).append('\'');
            sb.append(", score=").append(score);
            sb.append(", type=").append(type);
            sb.append(", uid=").append(uid);
            sb.append('}');
            return sb.toString();
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.avatar);
            dest.writeInt(this.experLevel);
            dest.writeInt(this.gender);
            dest.writeString(this.nick);
            dest.writeInt(this.score);
            dest.writeInt(this.type);
            dest.writeInt(this.uid);
        }

        public MicroApplyInfo() {
        }

        protected MicroApplyInfo(Parcel in) {
            this.avatar = in.readString();
            this.experLevel = in.readInt();
            this.gender = in.readInt();
            this.nick = in.readString();
            this.score = in.readInt();
            this.type = in.readInt();
            this.uid = in.readInt();
        }

        public static final Parcelable.Creator<MicroApplyInfo> CREATOR = new Parcelable.Creator<MicroApplyInfo>() {
            @Override
            public MicroApplyInfo createFromParcel(Parcel source) {
                return new MicroApplyInfo(source);
            }

            @Override
            public MicroApplyInfo[] newArray(int size) {
                return new MicroApplyInfo[size];
            }
        };

        @Override
        public int getItemType() {
            return score == 0 ? MICRO_APPLY_CONNECTING : MICRO_APPLY_NORMAL;
        }
    }
}
