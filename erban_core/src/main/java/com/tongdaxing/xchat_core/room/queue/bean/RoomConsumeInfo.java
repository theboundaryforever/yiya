package com.tongdaxing.xchat_core.room.queue.bean;

import java.io.Serializable;

/**
 * Created by chenran on 2017/10/2.
 */

public class RoomConsumeInfo implements Serializable {

    private long uid;
    private String nick;
    private int gender;
    private String avatar;
    private String region;
    private String province;
    private String city;
    private long goldNum;
    /**
     * 等级值
     */
    private int experLevel;
    /**
     * 魅力值
     */
    private int charmLevel;

    private int age;

    private int height;

    private String salary;

    /**
     * 排麦标识 1 排麦用户
     */
    private int micIcon;

    /*
     * 金币标识 1 音符大于50
     */
    private int glodIcon;

    private boolean isChecked;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getGoldNum() {
        return goldNum;
    }

    public void setGoldNum(long goldNum) {
        this.goldNum = goldNum;
    }

    public int getExperLevel() {
        return experLevel;
    }

    public void setExperLevel(int experLevel) {
        this.experLevel = experLevel;
    }

    public int getCharmLevel() {
        return charmLevel;
    }

    public void setCharmLevel(int charmLevel) {
        this.charmLevel = charmLevel;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public int getMicIcon() {
        return micIcon;
    }

    public void setMicIcon(int micIcon) {
        this.micIcon = micIcon;
    }

    public int getGlodIcon() {
        return glodIcon;
    }

    public void setGlodIcon(int glodIcon) {
        this.glodIcon = glodIcon;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public String toString() {
        return "RoomConsumeInfo{" +
                "uid=" + uid +
                ", nick='" + nick + '\'' +
                ", gender=" + gender +
                ", avatar='" + avatar + '\'' +
                ", region='" + region + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", goldNum=" + goldNum +
                ", experLevel=" + experLevel +
                ", charmLevel=" + charmLevel +
                ", age=" + age +
                ", height=" + height +
                ", salary='" + salary + '\'' +
                ", micIcon=" + micIcon +
                ", glodIcon=" + glodIcon +
                '}';
    }
}

//public class RoomConsumeInfo implements Serializable{
//    private long uid;
//    private String nick;
//    private String avatar;
//    private int gender;
//    private long ctrbUid;
//    private long sumGold;
//    //财富等级
//    private int experLevel;
//    //魅力等级
//
//    public int getCharmLevel() {
//        return charmLevel;
//    }
//
//    public void setCharmLevel(int charmLevel) {
//        this.charmLevel = charmLevel;
//    }
//
//    private int charmLevel;
//
//    public int getExperLevel() {
//        return experLevel;
//    }
//
//    public void setExperLevel(int experLevel) {
//        this.experLevel = experLevel;
//    }
//
//    public long getUid() {
//        return uid;
//    }
//
//    public void setUid(long uid) {
//        this.uid = uid;
//    }
//
//    public String getNick() {
//        return nick;
//    }
//
//    public void setNick(String nick) {
//        this.nick = nick;
//    }
//
//    public String getAvatar() {
//        return avatar;
//    }
//
//    public void setAvatar(String avatar) {
//        this.avatar = avatar;
//    }
//
//    public int getGender() {
//        return gender;
//    }
//
//    public void setGender(int gender) {
//        this.gender = gender;
//    }
//
//    public long getCtrbUid() {
//        return ctrbUid;
//    }
//
//    public void setCtrbUid(long ctrbUid) {
//        this.ctrbUid = ctrbUid;
//    }
//
//    public long getSumGold() {
//        return sumGold;
//    }
//
//    public void setSumGold(long sumGold) {
//        this.sumGold = sumGold;
//    }
//
//    @Override
//    public String toString() {
//        return "RoomConsumeInfo{" +
//                "uid=" + uid +
//                ", nick='" + nick + '\'' +
//                ", avatar='" + avatar + '\'' +
//                ", gender=" + gender +
//                ", ctrbUid=" + ctrbUid +
//                ", sumGold=" + sumGold +
//                '}';
//    }
//}
