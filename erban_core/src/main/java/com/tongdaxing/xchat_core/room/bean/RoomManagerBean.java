package com.tongdaxing.xchat_core.room.bean;

import com.tongdaxing.xchat_core.bean.IMChatRoomMember;

public class RoomManagerBean {
    private IMChatRoomMember member;

    public IMChatRoomMember getMember() {
        return member;
    }

    public void setMember(IMChatRoomMember member) {
        this.member = member;
    }
}
