package com.tongdaxing.xchat_core.room.presenter;

import android.support.annotation.Nullable;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tongdaxing.erban.libcommon.base.AbstractMvpPresenter;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.bean.IMChatRoomMember;
import com.tongdaxing.xchat_core.bean.RoomQueueInfo;
import com.tongdaxing.xchat_core.manager.AvRoomDataManager;
import com.tongdaxing.xchat_core.room.model.RoomInviteModel;
import com.tongdaxing.xchat_core.room.view.IRoomInviteView;
import com.tongdaxing.xchat_framework.im.IMKey;
import com.tongdaxing.xchat_framework.util.util.Json;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>  </p>
 *
 * @author jiahui
 * @date 2017/12/21
 */
public class RoomInvitePresenter extends AbstractMvpPresenter<IRoomInviteView> {

    private RoomInviteModel mRoomInviteModel;
    private Gson gson;

    public RoomInvitePresenter() {
        mRoomInviteModel = new RoomInviteModel();
        gson = new Gson();
    }


    /**
     * 查询成员列表
     *
     * @param index
     * @param page
     */
    public void requestChatMemberByPage(int index, final int page) {


        mRoomInviteModel.getMembers(index, new OkHttpManager.MyCallBack<Json>() {
            @Override
            public void onError(Exception e) {

                if (getMvpView() != null)
                    getMvpView().onRequestChatMemberByPageFail("网络异常", page);
            }

            @Override
            public void onResponse(Json response) {
                if (response.num(IMKey.errno) != 0) {
                    if (getMvpView() != null)
                        getMvpView().onRequestChatMemberByPageFail(response.str(IMKey.errmsg, "网络异常"), page);
                    return;
                }

                String data = response.str("data");
                List<IMChatRoomMember> imChatRoomMembers = null;
                try {
                    imChatRoomMembers = gson.fromJson(data, new TypeToken<List<IMChatRoomMember>>() {
                    }.getType());
                } catch (Exception e) {

                }
                List<IMChatRoomMember> onlineChatMembers = getFilterOnMicList(imChatRoomMembers);
                if (onlineChatMembers == null) return;


                if (getMvpView() != null) {
                    getMvpView().onRequestMemberByPageSuccess(onlineChatMembers, page);
                }
            }
        });

//
//        mRoomInviteModel.getPageMembers(page, time)
//                .map(new Function<List<IMChatRoomMember>, List<IMChatRoomMember>>() {
//                    @Override
//                    public List<IMChatRoomMember> apply(List<IMChatRoomMember> chatRoomMemberList) throws Exception {
//                        SparseArray<RoomQueueInfo> queueInfoSparseArray = AvRoomDataManager.get().mMicQueueMemberMap;
//                        int size;
//                        if (queueInfoSparseArray == null || (size = queueInfoSparseArray.size()) <= 0) {
//                            return chatRoomMemberList;
//                        }
//                        //移除麦上人员
//                        IMChatRoomMember chatRoomMember;
//                        ListIterator<IMChatRoomMember> iterator = chatRoomMemberList.listIterator();
//                        for (; iterator.hasNext(); ) {
//                            chatRoomMember = iterator.next();
//                            for (int i = 0; i < size; i++) {
//                                RoomQueueInfo roomQueueInfo = queueInfoSparseArray.valueAt(i);
//                                if (roomQueueInfo.mChatRoomMember != null
//                                        && Objects.equals(chatRoomMember.getAccount(), roomQueueInfo.mChatRoomMember.getAccount())) {
//                                    iterator.remove();
//                                }
//                            }
//                        }
//                        return chatRoomMemberList;
//                    }
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<List<IMChatRoomMember>>() {
//                    @Override
//                    public void accept(List<IMChatRoomMember> chatRoomMemberList) throws Exception {
//                        if (getMvpView() != null) {
//                            getMvpView().onRequestMemberByPageSuccess(chatRoomMemberList, page);
//                        }
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        throwable.printStackTrace();
//                        if (getMvpView() != null) {
//                            getMvpView().onRequestChatMemberByPageFail(throwable.getMessage(), page);
//                        }
//                    }
//                });
    }

    @Nullable
    private List<IMChatRoomMember> getFilterOnMicList(List<IMChatRoomMember> imChatRoomMembers) {
        List<IMChatRoomMember> onlineChatMembers = new ArrayList<>();

        if (imChatRoomMembers == null)
            return null;
        Set<String> micUIds = new HashSet<>();

        //排除麦上的用户
        SparseArray<RoomQueueInfo> mMicQueueMemberMap = AvRoomDataManager.get().mMicQueueMemberMap;
        for (int i = 0; i < mMicQueueMemberMap.size(); i++) {
            RoomQueueInfo roomQueueInfo = mMicQueueMemberMap.valueAt(i);
            if (roomQueueInfo.mChatRoomMember != null) {
                String account = roomQueueInfo.mChatRoomMember.getAccount();
                micUIds.add(account);
            }

        }

        for (int i = 0; i < imChatRoomMembers.size(); i++) {
            IMChatRoomMember imChatRoomMember = imChatRoomMembers.get(i);
            if (imChatRoomMember == null || micUIds.contains(imChatRoomMember.getAccount()))
                continue;
            onlineChatMembers.add(imChatRoomMember);
        }
        return onlineChatMembers;
    }
}
