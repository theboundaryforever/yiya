package com.tongdaxing.xchat_core.room.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/9/4.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class BannerEvent implements MultiItemEntity {

    public static final int TYPE_LUCKY_BOX = 0;
    public static final int TYPE_PLANT_CUCUMBER = 1;

    private int itemType;
    private RoomLuckyBoxInfo roomLuckyBoxInfo;
    private RoomPlantCucumberInfo roomPlantCucumberInfo;

    public BannerEvent(int itemType) {
        this.itemType = itemType;
    }

    public RoomLuckyBoxInfo getRoomLuckyBoxInfo() {
        return roomLuckyBoxInfo;
    }

    public void setRoomLuckyBoxInfo(RoomLuckyBoxInfo roomLuckyBoxInfo) {
        this.roomLuckyBoxInfo = roomLuckyBoxInfo;
    }

    public RoomPlantCucumberInfo getRoomPlantCucumberInfo() {
        return roomPlantCucumberInfo;
    }

    public void setRoomPlantCucumberInfo(RoomPlantCucumberInfo roomPlantCucumberInfo) {
        this.roomPlantCucumberInfo = roomPlantCucumberInfo;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

}
