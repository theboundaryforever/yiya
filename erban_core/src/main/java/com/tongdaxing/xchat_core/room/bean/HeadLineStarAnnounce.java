package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/9/3
 *
 * @describe 头条之星公告
 */

@Data
public class HeadLineStarAnnounce {
    private String time;// 时间戳；
    private int giftId;// 礼物id
    private String giftName;// 礼物名
    private String giftPic;// 礼物图片
    private long uid;// uid
}
