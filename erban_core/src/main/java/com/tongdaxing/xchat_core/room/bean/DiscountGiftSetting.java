package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/9/9
 *
 * @describe
 */

@Data
public class DiscountGiftSetting {
    private int isOpen; //  是否打开特惠礼物标志（1:是，0：否）
    private int maxBuyCount; //  最多购买次数
    private int userBuyCount; //  用户已经购买次数
    private int waitSecord; //  特惠礼物延迟弹出时间（秒）
    private int userRead; //  1:已经看过，0：还没看
}
