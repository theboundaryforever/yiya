package com.tongdaxing.xchat_core.room.bean;

import java.io.Serializable;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/26.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomLuckyBoxAddGold implements Serializable {

    private String msg;//消息文本
    private long golds;//金币数
    private long roomId;//幸运房间id
    private long roomUid;//房主id
    private int roomType;//房间类型
    private String roomOwnerNick;//房主昵称

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getGolds() {
        return golds;
    }

    public void setGolds(long golds) {
        this.golds = golds;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public long getRoomUid() {
        return roomUid;
    }

    public void setRoomUid(long roomUid) {
        this.roomUid = roomUid;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public String getRoomOwnerNick() {
        return roomOwnerNick;
    }

    public void setRoomOwnerNick(String roomOwnerNick) {
        this.roomOwnerNick = roomOwnerNick;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RoomLuckyBoxAddGold{");
        sb.append("msg='").append(msg).append('\'');
        sb.append(", golds=").append(golds);
        sb.append(", roomId=").append(roomId);
        sb.append(", roomUid=").append(roomUid);
        sb.append(", roomType=").append(roomType);
        sb.append(", roomOwnerNick='").append(roomOwnerNick).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
