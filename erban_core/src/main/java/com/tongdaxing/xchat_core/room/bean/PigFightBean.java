package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

@Data
public class PigFightBean {
    private int winPigNum;
    private String html;
}
