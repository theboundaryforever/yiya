package com.tongdaxing.xchat_core.room.bean;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author zdw
 * @data 2019/7/22
 *
 * @describe 魅力值PK结果
 */

@Data
public class CPKResultBean implements Serializable {
    public static final int CPKRESULT_WIN = 1;// 获胜
    public static final int CPKRESULT_DRAW = 2;// 平局

    List<String> avatarList;

    /**
     * 1. 获胜，2.平局
     */
    private int type;

    /**
     * 获胜者用户昵称
     */
    private String winNickName;

    /**
     * 获胜者用户ID
     */
    private long winUid;

    /**
     * 房间ID
     */
    private long roomId;

    /**
     * pkId
     */
    private long id;
}
