package com.tongdaxing.xchat_core.room.model;

import android.text.TextUtils;

import com.tongdaxing.erban.libcommon.utils.ListUtils;
import com.tongdaxing.xchat_core.room.bean.OnlineChatMember;

import java.util.List;
import java.util.Objects;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * <p> 轰趴房用户列表网络处理 </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */

public class HomePartyUserListModel extends RoomBaseModel {

    public Single<List<OnlineChatMember>> onMemberDownUpMic(final String account,
                                                            final boolean isUpMic,
                                                            final List<OnlineChatMember> dataList) {
        if (TextUtils.isEmpty(account)) return Single.error(new Throwable("account 不能为空"));
        return Single.create(
                new SingleOnSubscribe<List<OnlineChatMember>>() {
                    @Override
                    public void subscribe(SingleEmitter<List<OnlineChatMember>> e) throws Exception {
                        if (ListUtils.isListEmpty(dataList)) e.onSuccess(dataList);
                        int size = dataList.size();
                        for (int i = 0; i < size; i++) {
                            OnlineChatMember onlineChatMember = dataList.get(i);
                            if (onlineChatMember.chatRoomMember != null
                                    && Objects.equals(onlineChatMember.chatRoomMember.getAccount(), account)) {
                                onlineChatMember.isOnMic = isUpMic;
                            }
                        }
                        e.onSuccess(dataList);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<List<OnlineChatMember>> onUpdateMemberManager(final String account,
                                                                final boolean isRemoveManager,
                                                                final List<OnlineChatMember> dataList) {
        return Single.create(
                new SingleOnSubscribe<List<OnlineChatMember>>() {
                    @Override
                    public void subscribe(SingleEmitter<List<OnlineChatMember>> e) throws Exception {
                        if (ListUtils.isListEmpty(dataList)) e.onSuccess(dataList);
                        int size = dataList.size();
                        for (int i = 0; i < size; i++) {
                            OnlineChatMember onlineChatMember = dataList.get(i);
                            if (onlineChatMember.chatRoomMember != null
                                    && Objects.equals(onlineChatMember.chatRoomMember.getAccount(), account)) {
                                onlineChatMember.isAdmin = !isRemoveManager;
                            }
                        }
                        e.onSuccess(dataList);
                    }
                })
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
