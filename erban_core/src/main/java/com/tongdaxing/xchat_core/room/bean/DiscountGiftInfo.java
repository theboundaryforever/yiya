package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/9/6
 *
 * @describe 特惠礼物bean
 */

@Data
public class DiscountGiftInfo {
    private int discount;//  礼物折扣
    private String discountPrice;// 礼物折扣价
    private String discountPriceRMB;// 礼物折扣价(元)
    private int giftId;// 礼物ID
    private String giftName ; // 礼物名称
    private int goldPrice ; // 礼物原价
    private String goldPriceRMB ; // 礼物原价(元)
    private boolean hasEffect ; // 是否有特效
    private boolean hasVggPic ; // 是否有vgg动画
    private int id ; // ID
    private String picUrl ; // 礼物图片
    private int userBuyCount ; // 用户已经购买次数
    private String vggUrl ; // vgg动画链接
    private int maxBuyCount;// 最多购买次数
}
