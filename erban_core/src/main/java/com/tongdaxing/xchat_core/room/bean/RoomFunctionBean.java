package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

@Data
public class RoomFunctionBean {
    private RoomFunctionEnum functionType = RoomFunctionEnum.ROOM_QUIT;
    private String title;
    private int imgRes;
    private String imgUrl;
    private boolean tagEnable;// 小红点显示与否
}
