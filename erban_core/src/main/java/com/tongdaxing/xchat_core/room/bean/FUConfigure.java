package com.tongdaxing.xchat_core.room.bean;

import java.io.Serializable;

import lombok.Data;

@Data
public class FUConfigure implements Serializable {
    // 美颜设置
    float mBeautyColorLevel = 0.0f;// 美白
    float mBeautyBlurLevel = 0.0f;// 磨皮
    float mBeautyCheekThin = 0.0f;// 瘦脸
    float mBeautyEnlargeEye = 0.0f;// 大眼

    // 滤镜设置
    String filterName;
    float filterLevel;
}
