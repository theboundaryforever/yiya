package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/9/3
 *
 * @describe 头条之星分数超过两万
 */

@Data
public class HeadLineStarLimit {
    private String time;// 时间戳；
    private String nick;// 昵称
    private String avatar;// 头像
    private int outpaceNum;// 数量
    private long uid;// 房主uid
    private int roomType;// 房间类型
}
