package com.tongdaxing.xchat_core.room.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/8/26.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class PigRecordInfo implements Parcelable {

    private int winingPidNum;

    public int getWiningPidNum() {
        return winingPidNum;
    }

    public void setWiningPidNum(int winingPidNum) {
        this.winingPidNum = winingPidNum;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.winingPidNum);
    }

    public PigRecordInfo() {
    }

    protected PigRecordInfo(Parcel in) {
        this.winingPidNum = in.readInt();
    }

    public static final Creator<PigRecordInfo> CREATOR = new Creator<PigRecordInfo>() {
        @Override
        public PigRecordInfo createFromParcel(Parcel source) {
            return new PigRecordInfo(source);
        }

        @Override
        public PigRecordInfo[] newArray(int size) {
            return new PigRecordInfo[size];
        }
    };
}
