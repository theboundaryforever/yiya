package com.tongdaxing.xchat_core.room.model;

import com.netease.nim.uikit.common.util.string.StringUtil;
import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_core.manager.BaseMvpModel;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;
import com.tongdaxing.xchat_framework.util.util.StringUtils;

import java.util.Map;

/**
 * <p> 房间设置 </p>
 *
 * @author jiahui
 * @date 2017/12/15
 */
public class RoomSettingModel extends BaseMvpModel {

    public RoomSettingModel() {

    }

    public void requestTagAll(String ticket, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("ticket", ticket);
        OkHttpManager.getInstance().doPostRequest(UriProvider.getRoomTagList(), param, myCallBack);
    }

    /**
     * 更新房间设置信息
     *
     * @param title
     * @param desc
     * @param pwd
     * @param label 标签名字
     * @param tagId 标签id
     */
    public void updateRoomInfo(int roomType, String title, String desc, String pwd, String label, String avatar,
                               int tagId, long uid, String ticket, String backPic, int giftEffect, int isBlockGiftMsg, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        if (StringUtils.isNotEmpty(title))
            param.put("title", title);
        if (desc != null) {
            param.put("roomDesc", desc);
        }
        if (pwd != null) {
            param.put("roomPwd", pwd);
        }
        if (!StringUtil.isEmpty(label)) {
            param.put("roomTag", label);
        }
        if (StringUtils.isNotEmpty(label))
            param.put("roomTag", label);
        if (StringUtils.isNotEmpty(backPic))
            param.put("backPic", backPic);

        if (StringUtils.isNotEmpty(backPic))
            param.put("avatar", avatar);

        param.put("type", String.valueOf(roomType));
//        param.put("tagId", tagId + "");
        param.put("uid", uid + "");
        param.put("ticket", ticket);
        param.put("giftEffectSwitch", giftEffect + "");
        param.put("isBlockGiftMsg", String.valueOf(isBlockGiftMsg));
        OkHttpManager.getInstance().doPostRequest(UriProvider.updateRoomInfo(), param, myCallBack);
    }

    /**
     * 更新房间设置信息
     *
     * @param roomType
     * @param avatar
     */
    public void updateRoomInfo(int roomType, String avatar, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        if (StringUtils.isNotEmpty(avatar))
            param.put("avatar", avatar);

        param.put("type", String.valueOf(roomType));
        param.put("uid", String.valueOf(CoreManager.getCore(IAuthCore.class).getCurrentUid()));
        OkHttpManager.getInstance().doPostRequest(UriProvider.updateRoomInfo(), param, myCallBack);
    }

    /**
     * 更新房间设置信息
     *
     * @param title
     * @param desc
     * @param pwd
     * @param label 标签名字
     * @param tagId 标签id
     */
    public void updateByAdmin(int roomType, long roomUid, String title, String desc, String pwd, String label,
                              int tagId, long uid, String ticket, String backPic, int giftEffect, int isBlockGiftMsg, OkHttpManager.MyCallBack myCallBack) {
        Map<String, String> param = CommonParamUtil.getDefaultParam();
        param.put("roomUid", roomUid + "");
        if (StringUtils.isNotEmpty(title))
            param.put("title", title);
        if (desc != null) {
            param.put("roomDesc", desc);
        }
        if (pwd != null) {
            param.put("roomPwd", pwd);
        }
        if (StringUtils.isNotEmpty(label))
            param.put("roomTag", label);
        if (StringUtils.isNotEmpty(backPic))
            param.put("backPic", backPic);

        param.put("type", String.valueOf(roomType));
//        param.put("tagId", tagId + "");
        param.put("uid", uid + "");
        param.put("ticket", ticket);
        param.put("giftEffectSwitch", giftEffect + "");
        param.put("isBlockGiftMsg", String.valueOf(isBlockGiftMsg));
        OkHttpManager.getInstance().doPostRequest(UriProvider.updateByAdimin(), param, myCallBack);
    }

}
