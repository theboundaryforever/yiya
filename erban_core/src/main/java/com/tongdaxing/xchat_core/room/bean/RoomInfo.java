package com.tongdaxing.xchat_core.room.bean;


import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhouxiangfeng
 * @date 2017/5/24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoomInfo implements Serializable {
    public static final int ROOMTYPE_HOME_PARTY = 3;
    public static final int ROOMTYPE_VIDEO_LIVE = 6;//视频房

    private long uid;
    private int officeUser;//官方账号与非官方账号
    private long roomId;
    private String title;
    private String nick;
    private String avatar;
    private int type;
    private String roomNotice;
    private String roomDesc;
    private String backPic;
    private String backPicUrl;// 房间背景
    private String playInfo;//房间玩法
    private int giftEffectSwitch;//小礼物特效
    private int publicChatSwitch;//公屏开关 0，关闭过滤；1开启过滤
    private List<Integer> hideFace;
    //音效质量等级 0为AUDIO_PROFILE_DEFAULT、1为AUDIO_PROFILE_MUSIC_STANDARD_STEREO、2为AUDIO_PROFILE_MUSIC_HIGH_QUALITY、3
    // 为AUDIO_PROFILE_MUSIC_HIGH_QUALITY_STEREO
    private int audioLevel;
    private boolean valid;//房间是否开启，是否正在直播
    private int isPermitRoom;
    private int giftDrawEnable;//是否砸蛋厅 1，是；2，否
    private int charmEnable;// 房间魅力值开关  0  魅力值开关：1，关闭；2开启
    private int openPigFight;// 猪猪大作战UI开关 0，关闭；1开启
    private int audioChannel;//1.声网 2.即构
    private long erbanNo;
    public String roomPwd;
    private String roomTag;
    private String roomTagUrl;
    public int tagId;
    public String tagPict;
    private int onlineNum;//房间在线人数
    private int isCanConnectMic;//是否开启连麦 1，开启
    private int roomUserOnLiveClient;//特殊字段只有进房才返回，房主直播客户端 0 web端；1手机端；
    private Long receiveSum;// 本场收礼总数
    private long headlineStarDistance;// 头条之星距离上一名
    private String key;// 声网key
    private List<GameBean> roomGameConfig;
    private int isBlockGiftMsg;//是否屏蔽公屏礼物信息 （1：关 2：开）

    public boolean isBlockGiftMsg() {
        return isBlockGiftMsg == 2;
    }

    @Data
    public static class GameBean implements Serializable {
        public static final int GAME_TYPE_DS = 1;
        public static final int GAME_TYPE_PIG = 2;
        public static final int GAME_GAME_OFF = 0;
        public static final int GAME_GAME_ON = 1;

        private int id; // 1 DS 2猪战
        private int isOpen;// 底部工具栏是否显示 isopen=1表示开启、0表示关闭
    }
}