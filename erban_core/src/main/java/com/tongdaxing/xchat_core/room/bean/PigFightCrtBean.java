package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

@Data
public class PigFightCrtBean {
    private int uid;
    private int type;// 0是关闭 1是开启
    private long roomId;
    private String timestamps;
}
