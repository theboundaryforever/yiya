package com.tongdaxing.xchat_core.room.bean;

public enum RoomFunctionEnum {
    ROOM_REPORT,// 举报
    ROOM_SETTING,// 设置
    ROOM_MINIMIZE,// 最小化
    ROOM_PUBLIC_ENABLE,// 公屏消息处理
    ROOM_QUIT, // 退出
    ROOM_SHARE, // 分享
    ROOM_FEEDBACK,// 反馈
    ROOM_PLAY_MUSIC,// 音乐播放
    ROOM_RED_PACKET,// 红包
    ROOM_SPEAK,//发言
    ROOM_REVERSAL,//翻转
    ROOM_SOUND,//静音
    ROOM_MIRRORING,//镜像
    ROOM_CPK,//魅力值PK
    ROOM_PLANT_BEAN,//种豆消息
    ROOM_PIG_FIGHT,//种豆消息
    ROOM_PRIVATE_CHAT,//私聊
}
