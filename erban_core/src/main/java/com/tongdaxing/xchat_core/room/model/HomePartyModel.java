package com.tongdaxing.xchat_core.room.model;

import com.tongdaxing.erban.libcommon.net.rxnet.OkHttpManager;
import com.tongdaxing.xchat_core.UriProvider;
import com.tongdaxing.xchat_core.auth.IAuthCore;
import com.tongdaxing.xchat_framework.coremanager.CoreManager;
import com.tongdaxing.xchat_framework.http_image.util.CommonParamUtil;

import java.util.Map;

/**
 * <p> 轰趴房model层：数据获取 </p>
 *
 * @author jiahui
 * @date 2017/12/8
 */
public class HomePartyModel extends RoomBaseModel {

    public HomePartyModel() {
    }

    public void lockMicroPhone(int micPosition, String roomUid, String ticker,OkHttpManager.MyCallBack myCallBack) {
        operateMicroPhone(micPosition,"1",roomUid,ticker,myCallBack);
    }

    /**
     * 释放该麦坑的锁
     *
     * @param micPosition
     */
    public void unLockMicroPhone(int micPosition, String roomUid, String ticker,OkHttpManager.MyCallBack myCallBack) {
        operateMicroPhone(micPosition,"0",roomUid,ticker,myCallBack);
    }

    /**
     * 操作麦坑的锁
     * @param micPosition
     * @param state 1：锁，0：不锁
     * @param roomUid
     * @param ticker
     */
    private void operateMicroPhone(int micPosition, String state, String roomUid, String ticker, OkHttpManager.MyCallBack myCallBack){
        Map<String,String> param = CommonParamUtil.getDefaultParam();
        param.put("position", micPosition + "");
        param.put("state", state);
        param.put("roomUid", roomUid);
        param.put("ticket",ticker);
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.getlockMicroPhone(), param, myCallBack);
    }


    /**
     * 开麦
     *
     * @param micPosition
     * @param roomUid
     */
    public void openMicroPhone(int micPosition, long roomUid,OkHttpManager.MyCallBack myCallBack) {
        openOrCloseMicroPhone(micPosition, 0, roomUid, CoreManager.getCore(IAuthCore.class).getTicket(), myCallBack);
    }

    /**
     * 闭麦
     *
     * @param micPosition
     * @param roomUid
     */
    public void closeMicroPhone(int micPosition, long roomUid,OkHttpManager.MyCallBack myCallBack) {
        openOrCloseMicroPhone(micPosition, 1, roomUid, CoreManager.getCore(IAuthCore.class).getTicket(), myCallBack);
    }

    /**
     * 开闭麦接口
     *
     * @param micPosition
     * @param state       1:闭麦，0：开麦
     * @param roomUid
     * @param ticket
     */
    private void openOrCloseMicroPhone(int micPosition, int state, long roomUid, String ticket, OkHttpManager.MyCallBack myCallBack) {
        Map<String,String> param = CommonParamUtil.getDefaultParam();
        param.put("position", micPosition + "");
        param.put("state", state + "");
        param.put("roomUid", roomUid + "");
        param.put("ticket",ticket);
        param.put("uid", CoreManager.getCore(IAuthCore.class).getCurrentUid() + "");
        OkHttpManager.getInstance().doPostRequest(UriProvider.operateMicroPhone(),param,myCallBack);
    }

    /**
     * 当前用户领取某个房间的对应id的红包接口
     * @param roomid
     * @param redPackId
     * @param myCallBack
     */
    public void receiveRoomRedPakcageToMyself(long roomid ,long redPackId ,OkHttpManager.MyCallBack myCallBack){
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("uid",CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        params.put("ticket",CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomid",roomid+"");
        params.put("redPackId",redPackId+"");
        OkHttpManager.getInstance().doPostRequest(UriProvider.receiveRedPackage(),params,myCallBack);
    }

    /**
     * 获取对应房间未领取完的红包发送记录
     * @param roomid
     * @param myCallBack
     */
    public void getRoomSendRedPackageRecord(String roomid ,OkHttpManager.MyCallBack myCallBack){
        Map<String,String> params = CommonParamUtil.getDefaultParam();
        params.put("uid",CoreManager.getCore(IAuthCore.class).getCurrentUid()+"");
        params.put("ticket",CoreManager.getCore(IAuthCore.class).getTicket());
        params.put("roomid",roomid);
        OkHttpManager.getInstance().getRequest(UriProvider.getSendRedPackageRecord(),params,myCallBack);
    }

}
