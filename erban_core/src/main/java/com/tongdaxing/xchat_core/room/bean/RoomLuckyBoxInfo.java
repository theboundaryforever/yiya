package com.tongdaxing.xchat_core.room.bean;

import java.io.Serializable;
import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/9.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomLuckyBoxInfo implements Serializable {

    /**
     * activate : true
     * activateTime : 0
     * currentTreasure : 0
     * nextTreasure : 0
     * recordList : [{"createTime":"2019-07-09T07:59:58.182Z","goldNum":0,"nick":"string","roomUid":0,"roomUserGoldNum":0,"roomUserNick":"string","uid":0}]
     */

    private boolean activate;
    private int activateTime;
    private int currentTreasure;
    private int nextTreasure;
    private List<RecordList> recordList;

    public boolean isActivate() {
        return activate;
    }

    public void setActivate(boolean activate) {
        this.activate = activate;
    }

    public int getActivateTime() {
        return activateTime;
    }

    public void setActivateTime(int activateTime) {
        this.activateTime = activateTime;
    }

    public int getCurrentTreasure() {
        return currentTreasure;
    }

    public void setCurrentTreasure(int currentTreasure) {
        this.currentTreasure = currentTreasure;
    }

    public int getNextTreasure() {
        return nextTreasure;
    }

    public void setNextTreasure(int nextTreasure) {
        this.nextTreasure = nextTreasure;
    }

    public List<RecordList> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<RecordList> recordList) {
        this.recordList = recordList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RoomLuckyBoxInfo{");
        sb.append("activate=").append(activate);
        sb.append(", activateTime=").append(activateTime);
        sb.append(", currentTreasure=").append(currentTreasure);
        sb.append(", nextTreasure=").append(nextTreasure);
        sb.append(", recordList=").append(recordList);
        sb.append('}');
        return sb.toString();
    }

    public static class RecordList {
        /**
         * createTime : 2019-07-09T07:59:58.182Z
         * goldNum : 0
         * nick : string
         * roomUid : 0
         * roomUserGoldNum : 0
         * roomUserNick : string
         * uid : 0
         */

        private String createTime;
        private int goldNum;
        private String nick;
        private int roomUid;
        private int roomUserGoldNum;
        private String roomUserNick;
        private int uid;

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getGoldNum() {
            return goldNum;
        }

        public void setGoldNum(int goldNum) {
            this.goldNum = goldNum;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public int getRoomUid() {
            return roomUid;
        }

        public void setRoomUid(int roomUid) {
            this.roomUid = roomUid;
        }

        public int getRoomUserGoldNum() {
            return roomUserGoldNum;
        }

        public void setRoomUserGoldNum(int roomUserGoldNum) {
            this.roomUserGoldNum = roomUserGoldNum;
        }

        public String getRoomUserNick() {
            return roomUserNick;
        }

        public void setRoomUserNick(String roomUserNick) {
            this.roomUserNick = roomUserNick;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("RecordList{");
            sb.append("createTime='").append(createTime).append('\'');
            sb.append(", goldNum=").append(goldNum);
            sb.append(", nick='").append(nick).append('\'');
            sb.append(", roomUid=").append(roomUid);
            sb.append(", roomUserGoldNum=").append(roomUserGoldNum);
            sb.append(", roomUserNick='").append(roomUserNick).append('\'');
            sb.append(", uid=").append(uid);
            sb.append('}');
            return sb.toString();
        }
    }
}
