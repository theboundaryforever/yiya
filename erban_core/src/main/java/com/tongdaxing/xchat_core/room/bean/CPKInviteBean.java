package com.tongdaxing.xchat_core.room.bean;

import java.io.Serializable;

import lombok.Data;

/**
 * @author zdw
 * @data 2019/7/22
 *
 * @describe 魅力值PK结果
 */

@Data
public class CPKInviteBean implements Serializable {

    private String nick;
    private Long uid;
    private int pkId;

    /**
     * 房间ID
     */
    private Long roomId;
}
