package com.tongdaxing.xchat_core.room.bean;

import java.io.Serializable;
import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/10.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomLuckyTips implements Serializable {

    private String msg;//消息文本
    private String nick;//幸运儿昵称
    private long golds;//金币数
    private String roomUserNick;//主播昵称
    private long roomId;//房间id
    private String avatar;//头像
    private String html;//文案
    private List<Long> notShowUids;//排除ids

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public long getGolds() {
        return golds;
    }

    public void setGolds(long golds) {
        this.golds = golds;
    }

    public String getRoomUserNick() {
        return roomUserNick;
    }

    public void setRoomUserNick(String roomUserNick) {
        this.roomUserNick = roomUserNick;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<Long> getNotShowUids() {
        return notShowUids;
    }

    public void setNotShowUids(List<Long> notShowUids) {
        this.notShowUids = notShowUids;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RoomLuckyTips{");
        sb.append("msg='").append(msg).append('\'');
        sb.append(", nick='").append(nick).append('\'');
        sb.append(", golds=").append(golds);
        sb.append(", roomUserNick='").append(roomUserNick).append('\'');
        sb.append(", roomId=").append(roomId);
        sb.append(", avatar='").append(avatar).append('\'');
        sb.append(", html='").append(html).append('\'');
        sb.append(", notShowUids=").append(notShowUids);
        sb.append('}');
        return sb.toString();
    }
}
