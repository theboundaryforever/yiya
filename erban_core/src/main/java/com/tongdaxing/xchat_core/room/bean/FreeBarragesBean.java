package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

@Data
public class FreeBarragesBean {
    private int barrageNum;
}
