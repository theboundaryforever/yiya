package com.tongdaxing.xchat_core.room.bean;

import com.tongdaxing.xchat_core.home.LiveVideoRoomInfo;

import java.io.Serializable;
import java.util.List;

public class RoomClosedBean implements Serializable {
    private String avater;
    private long erbanNo;
    private String title;
    private List<LiveVideoRoomInfo.RoomListBean> recommends;
    private boolean attentions;
    private long roomId;
    private long uid;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getAvater() {
        return avater;
    }

    public void setAvater(String avater) {
        this.avater = avater;
    }

    public long getErbanNo() {
        return erbanNo;
    }

    public void setErbanNo(long erbanNo) {
        this.erbanNo = erbanNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<LiveVideoRoomInfo.RoomListBean> getRecommends() {
        return recommends;
    }

    public void setRecommends(List<LiveVideoRoomInfo.RoomListBean> recommends) {
        this.recommends = recommends;
    }

    public boolean isAttentions() {
        return attentions;
    }

    public void setAttentions(boolean attentions) {
        this.attentions = attentions;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    @Override
    public String toString() {
        return "RoomClosedBean{" +
                "avater='" + avater + '\'' +
                ", erbanNo=" + erbanNo +
                ", title='" + title + '\'' +
                ", recommends=" + recommends +
                '}';
    }
}
