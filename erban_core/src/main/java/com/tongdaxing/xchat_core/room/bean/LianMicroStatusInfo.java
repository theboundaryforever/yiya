package com.tongdaxing.xchat_core.room.bean;


import java.io.Serializable;

import lombok.Data;

@Data
public class LianMicroStatusInfo implements Serializable {

    public static final int STATUS_CLOSE = 0, STATUS_NO_CONNECTION = 1, STATUS_CONNECTING = 2, STATUS_CONNECTED = 3;

    private String avatar;//主播头像
    private int connectNum;//连接人数
    private int status;//0、主播关闭连麦申请 1、未连接 2、连接中 3已连接
    private String message;
    private int type;//连麦类型 0：语音 1：视频

    public boolean isAudioMicroType() {
        return type == 0;
    }

}
