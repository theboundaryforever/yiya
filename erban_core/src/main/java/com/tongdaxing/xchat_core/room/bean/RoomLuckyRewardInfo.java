package com.tongdaxing.xchat_core.room.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ProjectName:
 * Description:
 * Created by BlackWhirlwind on 2019/7/11.
 * Modify by:
 * Modify time:
 * Modify remark:
 */
public class RoomLuckyRewardInfo implements Serializable, Parcelable {

    public static final Parcelable.Creator<RoomLuckyRewardInfo> CREATOR = new Parcelable.Creator<RoomLuckyRewardInfo>() {
        @Override
        public RoomLuckyRewardInfo createFromParcel(Parcel source) {
            return new RoomLuckyRewardInfo(source);
        }

        @Override
        public RoomLuckyRewardInfo[] newArray(int size) {
            return new RoomLuckyRewardInfo[size];
        }
    };
    private String msg;//消息文本
    private String roomUserNick;//主播昵称
    private String nick;//幸运儿昵称
    private List<Prize> prizeList;

    public RoomLuckyRewardInfo() {
    }

    protected RoomLuckyRewardInfo(Parcel in) {
        this.msg = in.readString();
        this.roomUserNick = in.readString();
        this.nick = in.readString();
        this.prizeList = new ArrayList<Prize>();
        in.readList(this.prizeList, Prize.class.getClassLoader());
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRoomUserNick() {
        return roomUserNick;
    }

    public void setRoomUserNick(String roomUserNick) {
        this.roomUserNick = roomUserNick;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public List<Prize> getPrizeList() {
        return prizeList;
    }

    public void setPrizeList(List<Prize> prizeList) {
        this.prizeList = prizeList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.msg);
        dest.writeString(this.roomUserNick);
        dest.writeString(this.nick);
        dest.writeList(this.prizeList);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RoomLuckyRewardInfo{");
        sb.append("msg='").append(msg).append('\'');
        sb.append(", roomUserNick='").append(roomUserNick).append('\'');
        sb.append(", nick='").append(nick).append('\'');
        sb.append(", prizeList=").append(prizeList);
        sb.append('}');
        return sb.toString();
    }

    public static class Prize implements Parcelable {

        private String pic;//图片地址
        private String desc;//奖品描述

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Prize{");
            sb.append("pic='").append(pic).append('\'');
            sb.append(", desc='").append(desc).append('\'');
            sb.append('}');
            return sb.toString();
        }

        public static final Creator<Prize> CREATOR = new Creator<Prize>() {
            @Override
            public Prize createFromParcel(Parcel source) {
                return new Prize(source);
            }

            @Override
            public Prize[] newArray(int size) {
                return new Prize[size];
            }
        };

        public Prize() {
        }

        protected Prize(Parcel in) {
            this.pic = in.readString();
            this.desc = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.pic);
            dest.writeString(this.desc);
        }
    }


}
