package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/9/3
 *
 * @describe  头条之星距离上一名
 */

@Data
public class HeadLineStarDistance {
    private String timestamps;// 时间戳；
    private long roomId;// 房间id
    private long distance;// 距离上一名
    private long roomUid;// 房主uid
    private int roomType;// 房间类型
}
