package com.tongdaxing.xchat_core.room.bean;

import java.util.List;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/7/25
 *
 * @describe 种豆配置
 */

@Data
public class DrawCfg {
    public static int FREE_DRAW_DISABLE = 0;
    public static int FREE_DRAW_ENABLE = 1;

    private int freeDrawTimes;// 免费种豆次数
    private int freeDrawEnable;// 免费种豆是否开启 1开启  0关闭
    private List<DrawConfList> drawConfList;

    @Data
    public class DrawConfList{
        private int drawType;
        private int drawCount;
    }
}
