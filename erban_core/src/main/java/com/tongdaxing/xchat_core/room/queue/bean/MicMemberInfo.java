package com.tongdaxing.xchat_core.room.queue.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chenran on 2017/10/25.
 */

public class MicMemberInfo implements Parcelable {
    private long uid;
    private String avatar;
    private String nick;
    private int micPosition;
    private boolean isRoomOwnner;
    private boolean isSelect = false;//礼物用户选择中状态判断

    public MicMemberInfo() {

    }

    protected MicMemberInfo(Parcel in) {
        uid = in.readLong();
        avatar = in.readString();
        nick = in.readString();
        micPosition = in.readInt();
        isRoomOwnner = in.readByte() != 0;
        isSelect = in.readByte() != 0;
    }

    public static final Creator<MicMemberInfo> CREATOR = new Creator<MicMemberInfo>() {
        @Override
        public MicMemberInfo createFromParcel(Parcel in) {
            return new MicMemberInfo(in);
        }

        @Override
        public MicMemberInfo[] newArray(int size) {
            return new MicMemberInfo[size];
        }
    };

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getMicPosition() {
        return micPosition;
    }

    public void setMicPosition(int micPosition) {
        this.micPosition = micPosition;
    }

    public boolean isRoomOwnner() {
        return isRoomOwnner;
    }

    public void setRoomOwnner(boolean roomOwnner) {
        isRoomOwnner = roomOwnner;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(uid);
        dest.writeString(avatar);
        dest.writeString(nick);
        dest.writeInt(micPosition);
        dest.writeByte((byte) (isRoomOwnner ? 1 : 0));
        dest.writeByte((byte) (isSelect ? 1 : 0));
    }
}
