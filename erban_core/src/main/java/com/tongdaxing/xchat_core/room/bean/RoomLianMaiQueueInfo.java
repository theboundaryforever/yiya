package com.tongdaxing.xchat_core.room.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.tongdaxing.xchat_core.room.queue.bean.RoomConsumeInfo;

import java.util.List;

/**
 * 创建者      Created by dell
 * 创建时间    2019/3/8
 * 描述        ${}
 *
 * 更新者      dell
 * 更新时间    ${}
 * 更新描述    ${}
 */
public class RoomLianMaiQueueInfo implements Parcelable {

    private List<RoomConsumeInfo> roomMicQueue;

    protected RoomLianMaiQueueInfo(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RoomLianMaiQueueInfo> CREATOR = new Creator<RoomLianMaiQueueInfo>() {
        @Override
        public RoomLianMaiQueueInfo createFromParcel(Parcel in) {
            return new RoomLianMaiQueueInfo(in);
        }

        @Override
        public RoomLianMaiQueueInfo[] newArray(int size) {
            return new RoomLianMaiQueueInfo[size];
        }
    };

    public List<RoomConsumeInfo> getRoomMicQueue() {
        return roomMicQueue;
    }

    @Override
    public String toString() {
        return "RoomQueueInfo{" +
                "roomMicQueue=" + roomMicQueue +
                '}';
    }
}