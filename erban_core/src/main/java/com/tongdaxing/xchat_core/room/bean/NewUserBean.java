package com.tongdaxing.xchat_core.room.bean;

import lombok.Data;

/**
 * @author zdw
 * @date 2019/9/2
 *
 * @describe 萌新用户列表
 */

@Data
public class NewUserBean {
    //头像
    private String avatar;
    //性别 1:男 2：女 0 ：未知
    private int gender;
    //财富等级
    private int experLevel;
    private String experLevelPic;
    //魅力等级
    private int charmLevel;
    private String charmLevelPic;
    //用户等级
    private int videoRoomExperLevel;
    //用户等级图片
    private String videoRoomExperLevelPic;
    private int status;
}
